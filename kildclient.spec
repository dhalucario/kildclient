Name:      kildclient
Version:   3.2.1
Release:   1%{dist}
Summary:   A powerful MUD client with a built-in Perl interpreter
Summary(pt_BR): Um poderoso cliente de MUD com um interpretador Perl embutido
License:   GPLv2+
Group:     Amusements/Games
Icon:      kildclient.xpm
URL:       https://www.kildclient.org

Source:    https://download.sourceforge.net/%{name}/%{name}-%{version}.tar.gz
BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
BuildRequires: glib2-devel >= 2.32.0
BuildRequires: gtk3-devel >= 3.22.0
BuildRequires: gtkspell3-devel
BuildRequires: gnutls-devel >= 2.10.0
BuildRequires: perl-devel >= 5.8.3
BuildRequires: perl-ExtUtils-Embed
BuildRequires: zlib-devel
BuildRequires: gettext >= 0.14.5
BuildRequires: desktop-file-utils
Requires: desktop-file-utils
Requires: perl-JSON


%description
KildClient is a MUD Client written with the GTK+ windowing toolkit. It
supports many common features of other clients, such as triggers,
gags, aliases, macros, timers, and much more. But its main feature is
the built-in Perl interpreter. You can at any moment execute Perl
statements and functions to do things much more powerful than simply
sending text the mud. Perl statements can also be run, for example, as
the action of a trigger, allowing you to do complex things. Some
built-in functions of KildClient allow interaction with the world,
such as sending commands to it.

KildClient's ANSI support is extensive: it supports not only the
common 16 colors, but also support underlined text (singly and
doubly), text in italics, text striked through, reverse video and
"hidden" text. It also supports vt100's line-drawing characters, and
xterm's escape sequences for a 256-color mode. All these features make
KildClient one of the clients with the most features for displaying
the mud output.

KildClient supports the MCCP (Mud Client Compression Protocol)
protocol, versions 1 and 2, to reduce the necessary bandwidth.

KildClient allows connection through SOCKS4/5 and HTTP proxy servers.

KildClient supports the GMCP (also called ATCP2) and MSDP protocols
for out-of-band communications between server and client.

%description -l pt_BR
KildClient é um cliente de MUD escrito com a biblioteca gráfica GTK.
Ele suporta muitos recursos comuns aos outros clientes, como triggers,
gags, aliases, macros, temporizadores e muito mais. Mas seu principal
recurso é um interpretador Perl embutido. A qualquer momento, você
pode executar declarações Perl e funções para fazer coisas muito mais
poderosas que simplesmente enviar texto para o MUD. Declarações Perl
podem também ser executadas, por exemplo, como ação de um trigger, lhe
permitindo fazer coisas mais complexas. Algumas funções nativas do
KildClient permitem interação com o mundo, como enviar comandos para
ele.

O suporte ANSI do KildClient é extenso: ele suporta não apenas as 16
cores comuns, mas também suporta texto sublinhado (simples e duplo),
texto em itálico, texto tachado, texto em vídeo reverso e texto
"escondido". Ele suporta os caracteres para desenho de linhas do
vt100, e as seqüências do xterm para um mode com 256 cores. Todos
esses recursos fazem o KildClient um dos cliente com mais recursos
para mostrar o texto do mud.

O KildClient suporta o protocolo MCCP (Mud Client Compression
Protocol, Protocolo de Compressão para Clientes de MUD), versões 1 e
2, para reduzir a banda necessária.

O KildClient suporta conexão através de servidores proxy SOCKS4/5
e HTTP.

O KildClient suporta os protocols GMCP (também chamado ATCP2) e MSDP
para comunicação fora de banda entre cliente e servidor.


%prep
%setup -q

%build
%configure
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install
%find_lang %{name}
desktop-file-install --vendor "fedora" --delete-original \
  --dir $RPM_BUILD_ROOT%{_datadir}/applications          \
  $RPM_BUILD_ROOT%{_datadir}/applications/kildclient.desktop

%clean
rm -rf $RPM_BUILD_ROOT


%files -f %{name}.lang
%defattr(-, root, root, -)
%{_bindir}/kildclient
%doc %{_datadir}/doc/%{name}
%{_mandir}/man6/kildclient.*
%{_datadir}/%{name}
%{_datadir}/pixmaps/kildclient.png
%{_datadir}/pixmaps/kildclient.xpm
%{_datadir}/applications/fedora-kildclient.desktop


%changelog
* Tue Aug 22 2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br> - 3.2.1-1
- New version: 3.2.1.

* Mon Jan 12 2018 Eduardo M Kalinowski <eduardo@kalinowski.com.br> - 3.2.0-2
- desktop-file-utils is required instead of gvfs.

* Sun Dec 24 2017 Eduardo M Kalinowski <eduardo@kalinowski.com.br> - 3.2.0-1
- gtk+3 >= 3.22.0 is now required.
- gvfs is now required.

* Sun Dec 04 2016 Eduardo M Kalinowski <eduardo@kalinowski.com.br> - 3.1.0-1
- New version: 3.1.0.
- Changed homepage and download URLs to use https.
- perl-gettext is no longer necessary.

* Sun Dec 21 2014 Eduardo M Kalinowski <eduardo@kalinowski.com.br> - 3.0.1-1
- New version: 3.0.1.
- Require gtk3-devel >= 3.12.0.

* Tue Jul  8 2014 Eduardo M Kalinowski <eduardo@kalinowski.com.br> - 3.0.0-1
- New version: 3.0.0.
- Updated homepage to the new URL.
- Added required Perl modules.
- glib >= 2.32.0 is now required.
- gtk+3 >= 3.10.0 is now required.
- gtkspell 3 is now required
- gnutls-devel >= 2.10.0 is now required.

* Sat May 28 2011 Eduardo M Kalinowski <eduardo@kalinowski.com.br> - 2.11.1-1
- New version: 2.11.1.
- Fixed minimum required version of gnutls.

* Sun Feb 13 2011 Eduardo M Kalinowski <eduardo@kalinowski.com.br> - 2.11.0-1
- New version: 2.11.0.

* Sun Jun 13 2010 Eduardo M Kalinowski <eduardo@kalinowski.com.br> - 2.10.0-1
- Require glib >= 2.14.0, gtk >= 2.18.0, but does not require libglade
  anymore.
- Added zlib-devel as build requirement. -lz is also specified when linking.

* Sat May 23 2009 Eduardo M Kalinowski <eduardo@kalinowski.com.br> - 2.9.0-1
- New version: 2.9.0.

* Tue Jan 27 2009 Eduardo M Kalinowski <eduardo@kalinowski.com.br> - 2.8.1-1
- New version: 2.8.1.

* Sat Jan 03 2009 Eduardo M Kalinowski <eduardo@kalinowski.com.br> - 2.8.0-1
- New version: 2.8.0.
- Small changes in description to remove an occurrence of 'the the' and
  advertise proxy support.

* Tue May 13 2008 Eduardo M Kalinowski <ekalin@gmail.com> - 2.7.0-1
- New version: 2.7.0.

* Tue Jan  8 2008 Eduardo M Kalinowski <ekalin@gmail.com> - 2.6.0-5
- Removed manual Requires.
- Removed unnecessary %%doc of manpage in %%files.
- Fixed %%files so that %%{_datadir}/%%{name} is owned by the package.
- Corrected call to desktop-file-install.

* Fri Dec 28 2007 Eduardo M Kalinowski <ekalin@gmail.com> - 2.6.0-4
- Removed Vendor & Packager.
- Added dist tag.
- Added full URL in Source.
- Changed the BuildRoot to use mktemp.
- Support for SMP build flags.
- Removed the use of %%makeinstall; standard make is called.
- %%find_lang is now used.
- Added call to desktop-file-install.

* Thu Dec 20 2007 Eduardo M Kalinowski <ekalin@gmail.com> - 2.6.0-3aei
- Included an icon.
- Changed group to Amusements/Games.
- Removed cleaning in the prep stage, and added cleaning in the install
  stage.
- Specified the language for the .mo files.
- Minor tweaks in descriptive fields.

* Wed Dec 12 2007 Eduardo M Kalinowski <ekalin@gmail.com> - 2.6.0-2aei
- Changed gtk+2-devel to gtk2-devel and gtk+2 to gtk2.
- Added some files that were missing.

* Wed Nov 28 2007 Eduardo M Kalinowski <ekalin@gmail.com>
- New version: 2.6.0.

* Mon May 21 2007 Eduardo M Kalinowski <ekalin@bol.com.br>
- New version: 2.5.1.

* Sat Aug 12 2006 Eduardo M Kalinowski <ekalin@bol.com.br>
- New version: 2.5.0.
- Requiring GTK+ version 2.8.X now.

* Sun Mar 26 2006 Eduardo M Kalinowski <ekalin@bol.com.br>
- New version: 2.4.1.

* Fri Feb 17 2006 Eduardo M Kalinowski <ekalin@bol.com.br>
- New version: 2.4.0.
- Added gtkspell requirement.

* Wed Dec 21 2005 Eduardo M Kalinowski <ekalin@bol.com.br>
- New version: 2.3.0.
- Added Requires and BuildRequires for libgnutls.
- Updated required version of gtk+ and libglade.

* Sat Oct 01 2005 Eduardo M Kalinowski <ekalin@bol.com.br>
- Updated version to 2.2.2.
- Removed intltool dependency.

* Tue Sep 20 2005 Eduardo M Kalinowski <ekalin@bol.com.br>
- Updated version to 2.2.1.

* Mon Sep 19 2005 Eduardo M Kalinowski <ekalin@bol.com.br>
- Updated version to 2.2.0.
- Changed the description to describe some new features.

* Thu Aug 25 2005 Eduardo M Kalinowski <ekalin@bol.com.br>
- Updated version to 2.1.0.

* Wed Jul 27 2005 Eduardo M Kalinowski <ekalin@bol.com.br>
- Updated version to 2.0.0.
- Removed dependency of libvte, which is not used anymore.

* Sun Jul 03 2005 Eduardo M Kalinowski <ekalin@bol.com.br>
- Updated version to 1.3.2.

* Tue May 31 2005 Eduardo M Kalinowski <ekalin@bol.com.br>
- Updated version to 1.3.1.
- Added a requirement for glib >= 2.6.0.

* Fri May 27 2005 Eduardo M Kalinowski <ekalin@bol.com.br>
- Updated for version 1.3.0 of the program.
- Added perl-gettext as a requirement.

* Wed Apr 20 2005 Eduardo M Kalinowski <ekalin@bol.com.br>
- Updated for version 1.2.0 of the program.
- Mentioned MCCP in the description.
- Added intltool to BuildRequires

* Fri Mar 25 2005 Eduardo M Kalinowski <ekalin@bol.com.br>
- Updated for version 1.1.2 of the program.

* Fri Mar 11 2005 Eduardo M Kalinowski <ekalin@bol.com.br>
- Updated for version 1.1.1 of the program.

* Mon Feb 28 2005 Eduardo M Kalinowski <ekalin@bol.com.br>
- Included man file in package

* Sun Feb 20 2005 Alexandre Erwin Ittner <aittner@netuno.com.br>
- New version: 1.1.0

* Thu Jan 13 2005 Alexandre Erwin Ittner <aittner@netuno.com.br>
- RPM for Conectiva 10 (but should work on any distro)
