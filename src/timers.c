/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include "libintl-wrapper.h"
#include <locale.h>
#include <gtk/gtk.h>

#include "kildclient.h"
#include "perlscript.h"


/*************************
 * File global variables *
 *************************/


/***********************
 * Function prototypes *
 ***********************/



gboolean
run_timer(gpointer data)
{
  Timer *timer = (Timer *) data;
  World *world = timer->for_world;

  if (world->disable_timers) {
    return TRUE;
  }

  world_for_perl = world;
  parse_commands(world, timer->action);
  we_timer_update_timer(world, timer);
  world_for_perl = currentWorld;

  if (timer->count != -1 && --timer->count <= 0) {
    if (timer->temporary) {
      we_timer_delete_timer(world, timer);
      world->timers = g_slist_remove(world->timers,
                                     timer);
      if (!timer->owner_plugin) {
        --world->timer_pos;
      }
      free_timer(timer, NULL);
    } else {
      timer->enabled = FALSE;
    }

    return FALSE;
  }

  return TRUE;
}


void
start_timers(World *world)
{
  GSList *timerptr = world->timers;
  Timer  *timer;

  while (timerptr) {
    timer = (Timer *) timerptr->data;

    /* Timers of plugins loaded from the script file have already
       been started. */
    if (timer->enabled && !timer->function_id) {
      timer->function_id = g_timeout_add(timer->interval * 1000,
                                         run_timer,
                                         timer);
    }

    timerptr = timerptr->next;
  }
}


void
remove_timer(World *world, GSList *timeritem)
{
  Timer *timer = (Timer *) timeritem->data;

  we_timer_delete_timer(world, timer);

  world->timers = g_slist_remove_link(world->timers, timeritem);
  if (!timer->owner_plugin) {
    --world->timer_pos;
  }
  free_timer(timer, NULL);
  g_slist_free(timeritem);
}


gboolean
move_timer(World *world, gint old_pos, gint new_pos)
{
  GSList   *timeritem;
  gpointer  timer;

  timeritem = g_slist_nth(world->timers, old_pos);
  if (!timeritem) {
    return FALSE;
  }
  timer = timeritem->data;

  world->timers = g_slist_delete_link(world->timers, timeritem);
  world->timers = g_slist_insert(world->timers, timer, new_pos);

  we_timer_delete_timer(world, timer);
  we_timer_insert_timer(world, timer, new_pos);

  return TRUE;
}


void
free_timer(Timer *timer, gpointer data)
{
  if (timer->enabled) {
    g_source_remove(timer->function_id);
  }
  g_free(timer->action);

  g_free(timer);
}


void
list_timers(World *world, Plugin *plugin)
{
  guint   i;
  guint   interval_width;
  guint   repeat_width;
  guint   total_width;
  guint   field_width;
  GSList *timerptr;
  Timer  *timer;

  /* The rows argument is not used. */
  ansitextview_get_size(world->gui, &field_width, &total_width);

  /* If the screen is really narrow, we can do nothing. */
  if (total_width < 29) {
    total_width = 29;
  }

  /* Calculate maximum size necessary for the interval and repeat
     count fields */
  interval_width = 5;
  repeat_width   = 5;
  timerptr = world->timers;
  while (timerptr) {
    guint this_int_width;
    guint this_rep_width;

    timer = (Timer *) timerptr->data;

    this_int_width = (guint) log10(timer->interval) + 5;
    if (this_int_width > interval_width) {
      interval_width = this_int_width;
    }
    this_rep_width = (guint) log10(timer->count) + 1;
    if (this_rep_width > repeat_width) {
      repeat_width = this_rep_width;
    }

    timerptr = timerptr->next;
  }

  field_width = total_width - 13 - interval_width - 1 - repeat_width - 1;

  ansitextview_append_string(world->gui, _("Num Ena Temp Int "));
  for (i = 0; i < interval_width - 3; ++i) {
    ansitextview_append_string(world->gui, " ");
  }
  ansitextview_append_string(world->gui, _("Count "));
  for (i = 0; i < repeat_width - 5; ++i) {
    ansitextview_append_string(world->gui, " ");
  }
  ansitextview_append_string_nl(world->gui, _("Action"));
  ansitextview_append_string(world->gui, "--- --- ---- ");
  for (i = 0; i < interval_width; ++i) {
    ansitextview_append_string(world->gui, "-");
  }
  ansitextview_append_string(world->gui, " ");
  for (i = 0; i < repeat_width; ++i) {
    ansitextview_append_string(world->gui, "-");
  }
  ansitextview_append_string(world->gui, " ");
  for (i = 0; i < field_width; ++i) {
    ansitextview_append_string(world->gui, "-");
  }
  ansitextview_append_string(world->gui, "\n");

  i = 0;
  timerptr = world->timers;
  while (timerptr) {
    timer = (Timer *) timerptr->data;

    if (timer->owner_plugin == plugin) {
      ansitextview_append_stringf(world->gui,"%3d %3.3s %4.4s %*.3f %*d %-*.*s\n",
                                  i,
                                  timer->enabled ? _("y") : _("n"),
                                  timer->temporary ? _("y") : _("n"),
                                  interval_width, timer->interval,
                                  repeat_width, timer->count,
                                  field_width, field_width, timer->action);
    }

    ++i;
    timerptr = timerptr->next;
  }
}


void
save_timer(GString *str, Timer *timer)
{
  g_string_append(str, "    <timer ");
  if (timer->name) {
    g_string_append_printf(str, "name=\"%s\" ", timer->name);
  }
  g_string_append_printf(str,
                         "interval=\"%f\" count=\"%d\" enabled=\"%d\" temporary=\"%d\">",
                         timer->interval,
                         timer->count,
                         timer->enabled,
                         timer->temporary);
  kc_g_string_append_escaped(str, "%s</timer>\n", timer->action);
}
