/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include "libintl-wrapper.h"
#include <gtk/gtk.h>
#include <gmodule.h>

#include "simocombobox.h"

#include "kildclient.h"
#include "perlscript.h"


/***********************
 * Function prototypes *
 ***********************/


GObject *
get_we_protocols_tab(GtkBuilder *ui_builder)
{
  GObject *panel;

  panel = gtk_builder_get_object(ui_builder, "we_panel_protocols");

  return panel;
}


void
fill_we_protocols_tab(World *world, GtkBuilder *ui_builder)
{
  GObject *rad;

  if (!world) {
    return;
  }

  if (world->mccp_behavior == MCCP_DISABLE) {
    rad = gtk_builder_get_object(ui_builder, "radMCCPDisable");
  } else if (world->mccp_behavior == MCCP_ALWAYS) {
    rad = gtk_builder_get_object(ui_builder, "radMCCPAlways");
  } else {
    rad = gtk_builder_get_object(ui_builder, "radMCCPAfterConnect");
  }

  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rad), TRUE);
}



void
update_we_protocols_parameters(World *world, GtkBuilder *ui_builder)
{
  GtkToggleButton *radMCCPAfterConnect;
  GtkToggleButton *radMCCPAlways;
  GtkToggleButton *radMCCPDisable;

  radMCCPAfterConnect
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "radMCCPAfterConnect"));
  radMCCPAlways
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "radMCCPAlways"));
  radMCCPDisable
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "radMCCPDisable"));

  if (gtk_toggle_button_get_active(radMCCPAfterConnect)) {
    world->mccp_behavior = MCCP_AFTER_CONNECT;
  } else if (gtk_toggle_button_get_active(radMCCPAlways)) {
    world->mccp_behavior = MCCP_ALWAYS;
  } else if (gtk_toggle_button_get_active(radMCCPDisable)) {
    world->mccp_behavior = MCCP_DISABLE;
  }
}
