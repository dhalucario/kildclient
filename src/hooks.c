/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include "libintl-wrapper.h"
#include <locale.h>
#include <gtk/gtk.h>

#include "kildclient.h"
#include "perlscript.h"


/*************************
 * File global variables *
 *************************/


/***********************
 * Function prototypes *
 ***********************/
static void     execute_hook_full(World *world, const gchar *event,
                                  const char *hookdata, gboolean immediately);
static void     save_hook_list(GString *str, GSList *hookptr);



gboolean
connect_hook(World       *world,
             const gchar *event,
             int          pos,
             gchar       *action,
             gchar       *name,
             gboolean     enabled)
{
  GSList **hook_list;
  Hook    *hook;
  gint    *hook_pos;

  hook_list = get_hook_list_for_writing(world, event, &hook_pos);
  if (!hook_list) {
    ansitextview_append_stringf(world->gui,
                                _("hook: Invalid event '%s'\n"),
                                event);
    return FALSE;
  }

  if (pos == -1) {
    hook = g_new0(Hook, 1);
    if (world->plugins_being_defined) {
      hook->owner_plugin = world->plugins_being_defined->data;
      *hook_list = g_slist_append(*hook_list, hook);
    } else {
      *hook_list = g_slist_insert(*hook_list, hook, *hook_pos);
    }
  } else {
    hook = (Hook *) g_slist_nth_data(*hook_list, pos);
    if (!hook) {
      ansitextview_append_string_nl(world->gui, _("No such hook"));
      return FALSE;
    }
  }

  if (action) {
    g_free(hook->action);
    hook->action = action;
  }
  if (name) {
    g_free(hook->name);
    hook->name = name;
  }
  hook->enabled = enabled;

  if (pos == -1) {
    if (!world->plugins_being_defined) {
      we_hook_insert_hook(world, event, hook, (*hook_pos)++);
    } else {
      we_hook_insert_hook(world, event, hook, g_slist_length(*hook_list));
    }
  } else {
    we_hook_update_hook(world, event, hook);
  }

  return TRUE;
}


void
execute_hook(World *world, const gchar *event, const char *hookdata)
{
  execute_hook_full(world, event, hookdata, FALSE);
}


void
execute_hook_immediately(World *world,
                         const gchar *event, const char *hookdata)
{
  execute_hook_full(world, event, hookdata, TRUE);
}



static
void
execute_hook_full(World *world, const gchar *event,
                  const char *hookdata, gboolean immediately)
{
  /* If immediately is true, then instead of queuing commands, they are
     executed immediately. This is necessary when data is passed to the
     hooks via variables ($hookdata and $serverdata), because if the
     command is not executed immediately the variable may be overwritten
     if more data from the server triggers another execution. However,
     care must be taken so that immediate execution is only requested
     from the main thread. */
  GSList *hookptr = NULL;
  Hook   *hook;
  SV     *Phookdata;

  get_hook_list_for_reading(world, event, &hookptr, NULL);
  if (!hookptr) {
    return;
  }

  world_for_perl = world;

  if (hookdata) {
    GRAB_PERL(world);
    Phookdata = get_sv("hookdata", TRUE);
    SvUTF8_on(Phookdata);
    sv_setpv(Phookdata, hookdata);
    RELEASE_PERL(world);
  }

  while (hookptr) {
    hook = (Hook *) hookptr->data;
    if (hook->enabled) {
      if (immediately) {
        parse_command_line_immediately(world, hook->action);
      } else {
        parse_commands(world, hook->action);
      }
    }
    hookptr = hookptr->next;
  }

  world_for_perl = currentWorld;
}


void
list_hooks(World *world, const gchar *event, Plugin *plugin)
{
  GSList *hooklist;
  Hook   *hook;
  guint   total_width;
  guint   field_width;
  guint   i;

  hooklist = get_hook_list_or_warn_if_nonexistent(world, event);
  if (!hooklist) {
    return;
  }

  /* The rows argument is not used. */
  ansitextview_get_size(world->gui, &field_width, &total_width);

  /* If the screen is really narrow, we can do nothing. */
  if (total_width < 14) {
    total_width = 14;
  }
  field_width = total_width - 9;

  ansitextview_append_stringf(world->gui, _("Hooks for '%s':\n"), event);
  ansitextview_append_string_nl(world->gui, _("Num Ena Action"));
  ansitextview_append_string(world->gui, "--- --- ");
  for (i = 0; i < field_width; ++i) {
    ansitextview_append_string(world->gui, "-");
  }
  ansitextview_append_string(world->gui, "\n");

  i = 0;
  while (hooklist) {
    hook = (Hook *) hooklist->data;
    if (hook->owner_plugin == plugin) {
      ansitextview_append_stringf(world->gui,
                                  "%3d %-3.3s %-*.*s\n",
                                  i,
                                  hook->enabled ? _("y") : _("n"),
                                  field_width, field_width, hook->action);
    }
    hooklist = hooklist->next;
    ++i;
  }
}


void
list_hook(World *world, const gchar *event, int pos)
{
  GSList *hooklist;
  Hook   *hook;

  hooklist = get_hook_list_or_warn_if_nonexistent(world, event);

  hook = (Hook *) g_slist_nth_data(hooklist, pos);
  if (!hook) {
    ansitextview_append_string_nl(world->gui, _("No such hook"));
    return;
  }

  ansitextview_append_stringf(world->gui,
                              _("Hook number %d for event %s\n"
                                "  Name:    %s\n"
                                "  Action:  %s\n"
                                "  Enabled: %s\n"),
                              pos,
                              event,
                              hook->name ? hook->name : _("Not set"),
                              hook->action,
                              hook->enabled ? _("yes") : _("no"));
}


gboolean
delete_hook(World *world, const gchar *event, int pos, GSList *hook)
{
  /* Can receive either the list item poiting to the hook itself
   * (in this case, pos is ignored),
   * or, if hook is NULL, the position of the hook to delete.
   */
  GSList **hooklist;
  gint    *hook_pos;

  hooklist = get_hook_list_for_writing(world, event, &hook_pos);
  if (!hooklist) {
    return FALSE;
  }

  if (hook == NULL) {
    hook = g_slist_nth(*hooklist, pos);
  }
  if (!hook) {
    return FALSE;
  }

  we_hook_delete_hook(world, event, (Hook *) hook->data);

  *hooklist = g_slist_remove_link(*hooklist,
                                  hook);
  if (!((Hook *) hook->data)->owner_plugin) {
    --(*hook_pos);
  }
  free_hook((Hook *) hook->data, NULL);
  g_slist_free(hook);

  return TRUE;
}


gboolean
move_hook(World *world, const gchar *event, gint old_pos, gint new_pos)
{
  GSList  **hooklist;
  gint     *hook_pos;
  GSList   *hookitem;
  gpointer  hook;

  hooklist = get_hook_list_for_writing(world, event, &hook_pos);
  if (!hooklist) {
    return FALSE;
  }

  if (new_pos < 0 || new_pos >= *hook_pos) {
    new_pos = *hook_pos - 1;
  }

  hookitem = g_slist_nth(*hooklist, old_pos);
  if (!hookitem) {
    return FALSE;
  }
  hook = hookitem->data;

  *hooklist = g_slist_delete_link(*hooklist, hookitem);
  *hooklist = g_slist_insert(*hooklist, hook, new_pos);

  we_hook_delete_hook(world, event, hook);
  we_hook_insert_hook(world, event, hook, new_pos);

  return TRUE;
}


void
free_hook(Hook *hook, gpointer data)
{
  g_free(hook->name);
  g_free(hook->action);

  g_free(hook);
}


void
save_hooks(GString *str, World *world)
{
  g_string_append_printf(str, "  <hooksv2>\n");

  if (world->hooks.OnConnect) {
    g_string_append_printf(str, "    <hooklistv2 for=\"OnConnect\">\n");
    save_hook_list(str, world->hooks.OnConnect);
  }

  if (world->hooks.OnDisconnect) {
    g_string_append_printf(str, "    <hooklistv2 for=\"OnDisconnect\">\n");
    save_hook_list(str, world->hooks.OnDisconnect);
  }

  if (world->hooks.OnReceivedText) {
    g_string_append_printf(str, "    <hooklistv2 for=\"OnReceivedText\">\n");
    save_hook_list(str, world->hooks.OnReceivedText);
  }

  if (world->hooks.OnSentCommand) {
    g_string_append_printf(str, "    <hooklistv2 for=\"OnSentCommand\">\n");
    save_hook_list(str, world->hooks.OnSentCommand);
  }

  if (world->hooks.OnGetFocus) {
    g_string_append_printf(str, "    <hooklistv2 for=\"OnGetFocus\">\n");
    save_hook_list(str, world->hooks.OnGetFocus);
  }

  if (world->hooks.OnLoseFocus) {
    g_string_append_printf(str, "    <hooklistv2 for=\"OnLoseFocus\">\n");
    save_hook_list(str, world->hooks.OnLoseFocus);
  }

  if (world->hooks.OnCloseConnected) {
    g_string_append_printf(str, "    <hooklistv2 for=\"OnCloseConnected\">\n");
    save_hook_list(str, world->hooks.OnCloseConnected);
  }

  if (world->hooks.OnServerData) {
    g_string_append_printf(str, "    <hooklistv2 for=\"OnServerData\">\n");
    save_hook_list(str, world->hooks.OnServerData);
  }

  g_string_append_printf(str, "  </hooksv2>\n");
}


GSList **
get_hook_list_for_writing(World        *world,
                          const gchar  *event,
                          gint        **poscounter)
{
  if (strcmp(event, "OnConnect") == 0) {
    *poscounter = &world->hooks.OnConnect_pos;
    return &world->hooks.OnConnect;
  }
  if (strcmp(event, "OnDisconnect") == 0) {
    *poscounter = &world->hooks.OnDisconnect_pos;
    return &world->hooks.OnDisconnect;
  }
  if (strcmp(event, "OnReceivedText") == 0) {
    *poscounter = &world->hooks.OnReceivedText_pos;
    return &world->hooks.OnReceivedText;
  }
  if (strcmp(event, "OnSentCommand") == 0) {
    *poscounter = &world->hooks.OnSentCommand_pos;
    return &world->hooks.OnSentCommand;
  }
  if (strcmp(event, "OnGetFocus") == 0) {
    *poscounter = &world->hooks.OnGetFocus_pos;
    return &world->hooks.OnGetFocus;
  }
  if (strcmp(event, "OnLoseFocus") == 0) {
    *poscounter = &world->hooks.OnGetFocus_pos;
    return &world->hooks.OnLoseFocus;
  }
  if (strcmp(event, "OnCloseConnected") == 0) {
    *poscounter = &world->hooks.OnCloseConnected_pos;
    return &world->hooks.OnCloseConnected;
  }
  if (strcmp(event, "OnServerData") == 0) {
    *poscounter = &world->hooks.OnServerData_pos;
    return &world->hooks.OnServerData;
  }

  return NULL;
}


gboolean
get_hook_list_for_reading(World        *world,
                          const gchar  *event,
                          GSList      **list,
                          gint         *pos)
{
  gint    *found_pos;
  GSList **found_list = get_hook_list_for_writing(world, event, &found_pos);

  if (!found_list) {
    *list = NULL;
    return FALSE;
  }

  *list = *found_list;
  if (pos) {
    *pos = *found_pos;
  }

  return TRUE;
}


GSList *
get_hook_list_or_warn_if_nonexistent(World *world, const gchar *event)
{
  GSList *hooklist;

  if (!get_hook_list_for_reading(world, event, &hooklist, NULL)) {
    ansitextview_append_stringf(world->gui,
                                _("hook: Invalid event '%s'\n"),
                                event);
    return NULL;
  }

  return hooklist;
}


GtkTreeModel *
get_hook_model(World *world, const gchar *event)
{
  if (strcmp(event, "OnConnect") == 0) {
    return world->hooks.OnConnect_model;
  }
  if (strcmp(event, "OnDisconnect") == 0) {
    return world->hooks.OnDisconnect_model;
  }
  if (strcmp(event, "OnReceivedText") == 0) {
    return world->hooks.OnReceivedText_model;
  }
  if (strcmp(event, "OnSentCommand") == 0) {
    return world->hooks.OnSentCommand_model;
  }
  if (strcmp(event, "OnGetFocus") == 0) {
    return world->hooks.OnGetFocus_model;
  }
  if (strcmp(event, "OnLoseFocus") == 0) {
    return world->hooks.OnLoseFocus_model;
  }
  if (strcmp(event, "OnCloseConnected") == 0) {
    return world->hooks.OnCloseConnected_model;
  }
  if (strcmp(event, "OnServerData") == 0) {
    return world->hooks.OnServerData_model;
  }

  return NULL;
}


static
void
save_hook_list(GString *str, GSList *hookptr)
{
  Hook *hook;

  while (hookptr) {
    hook = (Hook *) hookptr->data;
    if (!hook->owner_plugin) {
      save_hook(str, hook);
    }
    hookptr = hookptr->next;
  }
  g_string_append(str, "    </hooklistv2>\n");
}


void
save_hook(GString *str, Hook *hook)
{
  g_string_append(str, "      <hookv2 ");
  if (hook->name) {
    kc_g_string_append_escaped(str, "name=\"%s\" ", hook->name);
  }
  kc_g_string_append_escaped(str, "enabled=\"%d\">%s</hookv2>\n",
                             hook->enabled, hook->action);
}


gboolean
are_there_enabled_server_data_hooks(World *world)
{
  GSList *iter = world->hooks.OnServerData;
  while (iter != NULL) {
    Hook *hook = (Hook *) iter->data;
    if (hook->enabled) {
      return TRUE;
    }
    iter = iter->next;
  }

  return FALSE;
}


void
mainthread_op_execute_on_received_text_hook(HookExecution *hook_execution)
{
  execute_hook_immediately(hook_execution->world, "OnReceivedText",
                           hook_execution->data);
}

