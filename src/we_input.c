/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include "libintl-wrapper.h"
#include <gtk/gtk.h>
#include <gmodule.h>
#include <gtkspell/gtkspell.h>

#include "simocombobox.h"

#include "kildclient.h"
#include "perlscript.h"


/***********************
 * Function prototypes *
 ***********************/
static void fill_spell_languages_combo(GtkBuilder *ui_builder);
static void fill_spell_checking(World *world, GtkBuilder *ui_builder);

/* XML UI signals */
G_MODULE_EXPORT void multi_line_toggled_cb(GtkToggleButton *button,
                                           gpointer         data);
G_MODULE_EXPORT void input_size_changed_cb(GtkSpinButton *button,
                                           gpointer       data);
G_MODULE_EXPORT void entryfont_set_cb(GtkFontButton *font_btn,
                                      gpointer data);
G_MODULE_EXPORT void flood_prevention_toggled_cb(GtkToggleButton *chkbox,
                                                 gpointer data);



GObject *
get_we_input_tab(GtkBuilder *ui_builder)
{
  GObject         *panel;

  panel = gtk_builder_get_object(ui_builder, "we_panel_input");

  fill_spell_languages_combo(ui_builder);

  return panel;
}


static
void
fill_spell_languages_combo(GtkBuilder *ui_builder)
{
  GtkComboBoxText *cmbSpellLanguage;
  GList           *languages;
  GList           *i;

  cmbSpellLanguage
    = GTK_COMBO_BOX_TEXT(gtk_builder_get_object(ui_builder,
                                                "cmbSpellLanguage"));

  languages = gtk_spell_checker_get_language_list();
  for (i = languages; i != NULL; i = i->next) {
    gtk_combo_box_text_append_text(cmbSpellLanguage, i->data);
  }

  g_list_free(languages);
}


void
fill_we_input_tab(World *world, GtkBuilder *ui_builder)
{
  GtkToggleButton *chkRepeatCommands;
  GtkToggleButton *chkCmdEcho;
  GtkToggleButton *chkNeverHideInput;
  GtkToggleButton *chkStoreCommands;
  GtkSpinButton   *spnCommandsToSave;
  GtkEntry        *txtCommandSeparator;
  GObject         *radSingleLine;
  GObject         *radMultiLine;
  GtkSpinButton   *spnNLines;
  GtkToggleButton *chkAutoCompletion;
  GtkSpinButton   *spnMinPrefix;
  GtkFontChooser  *entryfont_btn;
  GObject         *chkFloodPrevention;
  GtkWidget       *spnMaxCommands;
  GtkEntry        *txtPreventionCommand;

  if (!world) {
    return;
  }

  /* Get widgets */
  chkRepeatCommands
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "chkRepeatCommands"));
  chkCmdEcho
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "chkCmdEcho"));
  chkNeverHideInput
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "chkNeverHideInput"));
  chkStoreCommands
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "chkStoreCommands"));
  spnCommandsToSave
    = GTK_SPIN_BUTTON(gtk_builder_get_object(ui_builder, "spnCommandsToSave"));
  radSingleLine
    = gtk_builder_get_object(ui_builder, "radSingleLine");
  radMultiLine
    = gtk_builder_get_object(ui_builder, "radMultiLine");
  chkAutoCompletion
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "chkAutoCompletion"));
  spnMinPrefix
    = GTK_SPIN_BUTTON(gtk_builder_get_object(ui_builder, "spnMinPrefix"));
  spnNLines
    = GTK_SPIN_BUTTON(gtk_builder_get_object(ui_builder, "spnNLines"));
  txtCommandSeparator
    = GTK_ENTRY(gtk_builder_get_object(ui_builder, "txtCommandSeparator"));
  entryfont_btn
    = GTK_FONT_CHOOSER(gtk_builder_get_object(ui_builder, "entryfont_btn"));
  chkFloodPrevention   = gtk_builder_get_object(ui_builder,
                                                "chkFloodPrevention");
  spnMaxCommands
    = GTK_WIDGET(gtk_builder_get_object(ui_builder, "spnMaxCommands"));
  txtPreventionCommand
    = GTK_ENTRY(gtk_builder_get_object(ui_builder, "txtPreventionCommand"));

  gtk_toggle_button_set_active(chkRepeatCommands, world->repeat_commands);
  gtk_toggle_button_set_active(chkCmdEcho, world->cmd_echo);
  gtk_toggle_button_set_active(chkNeverHideInput, world->never_hide_input);
  gtk_toggle_button_set_active(chkStoreCommands, world->store_commands);
  gtk_spin_button_set_value(spnCommandsToSave, world->commands_to_save);
  gtk_entry_set_text(txtCommandSeparator, world->command_separator);

  gtk_spin_button_set_value(spnNLines, world->input_n_lines_saved);
  set_input_line_controls(world, radSingleLine, radMultiLine, spnNLines);

  gtk_toggle_button_set_active(chkAutoCompletion, world->autocompletion);
  gtk_spin_button_set_value(spnMinPrefix, world->autocompl_minprefix);

  fill_spell_checking(world, ui_builder);

  gtk_font_chooser_set_font(entryfont_btn, world->entryfont);

  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(chkFloodPrevention),
                               world->flood_prevention);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spnMaxCommands),
                            world->max_equal_commands);
  gtk_widget_set_sensitive(spnMaxCommands, world->flood_prevention);
  gtk_widget_set_sensitive(GTK_WIDGET(txtPreventionCommand),
                           world->flood_prevention);
  if (world->flood_prevention_command) {
    gtk_entry_set_text(txtPreventionCommand, world->flood_prevention_command);
  } else {
    gtk_entry_set_text(txtPreventionCommand, "");
  }
}


static
void
fill_spell_checking(World *world, GtkBuilder *ui_builder)
{
  GtkToggleButton *chkSpell;
  GtkComboBox     *cmbSpellLanguage;

  chkSpell
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "chkSpell"));
  cmbSpellLanguage
    = GTK_COMBO_BOX(gtk_builder_get_object(ui_builder, "cmbSpellLanguage"));

  gtk_toggle_button_set_active(chkSpell, world->spell);
  if (world->spell_language) {
    GtkTreeModel *model;
    GtkTreeIter   i;
    gboolean      has_more;
    gboolean      found = FALSE;
    gint          index;

    model = gtk_combo_box_get_model(cmbSpellLanguage);
    index = 0;
    has_more = gtk_tree_model_get_iter_first(model, &i);
    while (has_more) {
      gchar *language;

      gtk_tree_model_get(model, &i, 0, &language, -1);
      if (strcmp(language, world->spell_language) == 0) {
        g_free(language);
        found = TRUE;
        break;
      }

      g_free(language);
      ++index;
      has_more = gtk_tree_model_iter_next(model, &i);
    }

    if (found) {
      gtk_combo_box_set_active(cmbSpellLanguage, index);
    } else {
      gtk_combo_box_set_active(cmbSpellLanguage, -1);
    }
  } else {
    gtk_combo_box_set_active(cmbSpellLanguage, -1);
  }
}



void
update_we_input_parameters(World *world, GtkBuilder *ui_builder)
{
  GtkToggleButton *chkRepeatCommands;
  GtkToggleButton *chkCmdEcho;
  GtkToggleButton *chkNeverHideInput;
  GtkToggleButton *chkFloodPrevention;
  GtkSpinButton   *spnMaxCommands;
  GtkEntry        *txtPreventionCommand;
  GtkToggleButton *chkStoreCommands;
  GtkSpinButton   *spnCommandsToSave;
  GtkToggleButton *chkAutoCompletion;
  GtkSpinButton   *spnMinPrefix;
  GtkToggleButton *chkSpell;
  GtkComboBoxText *cmbSpellLanguage;
  GtkEntry        *txtCommandSeparator;

  GtkEntryCompletion *completion;
  gchar              *new_language;
  const gchar        *new_separator;

  /* Get widgets */
  chkRepeatCommands
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "chkRepeatCommands"));
  chkCmdEcho
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "chkCmdEcho"));
  chkNeverHideInput
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "chkNeverHideInput"));
  chkStoreCommands
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "chkStoreCommands"));
  spnCommandsToSave
    = GTK_SPIN_BUTTON(gtk_builder_get_object(ui_builder, "spnCommandsToSave"));
  chkAutoCompletion
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "chkAutoCompletion"));
  spnMinPrefix
    = GTK_SPIN_BUTTON(gtk_builder_get_object(ui_builder, "spnMinPrefix"));
  chkSpell
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "chkSpell"));
  cmbSpellLanguage
    = GTK_COMBO_BOX_TEXT(gtk_builder_get_object(ui_builder,
                                                "cmbSpellLanguage"));
  txtCommandSeparator
    = GTK_ENTRY(gtk_builder_get_object(ui_builder, "txtCommandSeparator"));
  chkFloodPrevention
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "chkFloodPrevention"));
  spnMaxCommands
    = GTK_SPIN_BUTTON(gtk_builder_get_object(ui_builder, "spnMaxCommands"));
  txtPreventionCommand
    = GTK_ENTRY(gtk_builder_get_object(ui_builder, "txtPreventionCommand"));

  world->repeat_commands =
    gtk_toggle_button_get_active(chkRepeatCommands);
  world->cmd_echo =
    gtk_toggle_button_get_active(chkCmdEcho);
  world->never_hide_input =
    gtk_toggle_button_get_active(chkNeverHideInput);
  if (world->gui) {
    simo_combo_box_set_visibility(world->gui->cmbEntry, TRUE);
  }

  world->store_commands =
    gtk_toggle_button_get_active(chkStoreCommands);
  world->commands_to_save =
    gtk_spin_button_get_value_as_int(spnCommandsToSave);

  world->autocompletion =
    gtk_toggle_button_get_active(chkAutoCompletion);
  world->autocompl_minprefix =
    gtk_spin_button_get_value_as_int(spnMinPrefix);
  if (world->gui) {
    completion = simo_combo_box_get_completion(world->gui->cmbEntry);
    if (world->autocompletion) {
      gtk_entry_completion_set_minimum_key_length(completion,
                                                  world->autocompl_minprefix);
    } else {
      gtk_entry_completion_set_minimum_key_length(completion,
                                                  9999);
    }
  }

  world->spell = gtk_toggle_button_get_active(chkSpell);
  g_free(world->spell_language);
  new_language = gtk_combo_box_text_get_active_text(cmbSpellLanguage);
  if (new_language != NULL && strcmp(new_language, "") != 0) {
    world->spell_language = new_language;
  } else {
    g_free(new_language);
    world->spell_language = NULL;
  }
  if (world->gui) {
    worldgui_configure_spell(world->gui);
  }

  g_strlcpy(world->command_separator,
            gtk_entry_get_text(txtCommandSeparator),
            sizeof(world->command_separator));

  world->flood_prevention =
    gtk_toggle_button_get_active(chkFloodPrevention);
  world->max_equal_commands =
    gtk_spin_button_get_value_as_int(spnMaxCommands);

  new_separator = gtk_entry_get_text(txtPreventionCommand);
  if (!world->flood_prevention_command
      || strcmp(world->flood_prevention_command, new_separator) != 0) {
    g_free(world->flood_prevention_command);
    world->flood_prevention_command = g_strdup(new_separator);
  }
}


void
multi_line_toggled_cb(GtkToggleButton *button, gpointer data)
{
  World           *world = (World *) data;
  GtkToggleButton *radSingleLine;

  /* Avoid infinite loop */
  if (world->updating_controls) {
    return;
  }

  radSingleLine = GTK_TOGGLE_BUTTON(gtk_builder_get_object(world->ui_builder,
                                                           "radSingleLine"));

  /* If there is a gui, setting the size generates a signal that is caught,
     and world->input_n_lines is updated in that handler. */
  if (gtk_toggle_button_get_active(radSingleLine)) {
    if (world->gui) {
      simo_combo_box_set_n_lines(world->gui->cmbEntry, 1);
    } else {
      world->input_n_lines = 1;
    }
  } else {
    if (world->gui) {
      simo_combo_box_set_n_lines(world->gui->cmbEntry,
                                 world->input_n_lines_saved);
    } else {
      world->input_n_lines = world->input_n_lines_saved;
    }
  }
}


void
input_size_changed_cb(GtkSpinButton *button, gpointer data)
{
  World           *world = (World *) data;
  GtkToggleButton *radMultiLine;

  /* Avoid infinite loop */
  if (world->updating_controls) {
    return;
  }

  radMultiLine  = GTK_TOGGLE_BUTTON(gtk_builder_get_object(world->ui_builder,
                                                           "radMultiLine"));

  world->input_n_lines_saved = gtk_spin_button_get_value_as_int(button);
  if (world->input_n_lines_saved > 10) {
    world->input_n_lines_saved = 10;
  }
  if (world->input_n_lines_saved < 2) {
    world->input_n_lines_saved = 2;
  }

  if (world->gui) {
    simo_combo_box_set_n_lines(world->gui->cmbEntry,
                               world->input_n_lines_saved);
  } else {
    world->input_n_lines = world->input_n_lines_saved;
  }
  gtk_toggle_button_set_active(radMultiLine, TRUE);
}


void
entryfont_set_cb(GtkFontButton *font_btn, gpointer data)
{
  World    *world = (World *) data;
  WorldGUI *gui   = world->gui;

  free(world->entryfont);
  world->entryfont
    = g_strdup(gtk_font_chooser_get_font(GTK_FONT_CHOOSER(font_btn)));

  if (gui) {
    configure_css(gui, world);
  }
}


void
flood_prevention_toggled_cb(GtkToggleButton *chkbox, gpointer data)
{
  World       *world = (World *) data;
  GtkWidget   *spnMaxCommands;
  GtkWidget   *txtPreventionCommand;
  gboolean     enabled;

  /* Get widgets */
  spnMaxCommands       = GTK_WIDGET(gtk_builder_get_object(world->ui_builder,
                                                           "spnMaxCommands"));
  txtPreventionCommand = GTK_WIDGET(gtk_builder_get_object(world->ui_builder,
                                                           "txtPreventionCommand"));
  enabled = gtk_toggle_button_get_active(chkbox);

  gtk_widget_set_sensitive(spnMaxCommands, enabled);
  gtk_widget_set_sensitive(txtPreventionCommand, enabled);
}


void
set_input_line_controls(World         *world,
                        GObject       *radSingleLine,
                        GObject       *radMultiLine,
                        GtkSpinButton *spnNLines)
{
  /* If the controls have not been passed as argument. */
  if (!radSingleLine) {
    if (!world->dlgEditWorld) {
      return;
    }

    radSingleLine = gtk_builder_get_object(world->ui_builder, "radSingleLine");
    radMultiLine  = gtk_builder_get_object(world->ui_builder, "radMultiLine");
    spnNLines     = GTK_SPIN_BUTTON(gtk_builder_get_object(world->ui_builder,
                                                           "spnNLines"));
  }

  world->updating_controls = TRUE;
  if (world->input_n_lines == 1) {
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radSingleLine), TRUE);
  } else {
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radMultiLine), TRUE);
    gtk_spin_button_set_value(spnNLines, world->input_n_lines);
  }
  world->updating_controls = FALSE;
}
