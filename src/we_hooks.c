/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include "libintl-wrapper.h"
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

#include "kildclient.h"
#include "perlscript.h"


/***********************
 * Function prototypes *
 ***********************/
static void          create_we_hooks_models(World *world);
static GtkTreeModel *create_we_hooks_model(GSList *hookptr);
static void          we_hook_configure_view(GtkTreeView *view);
static void          we_hook_combo_changed_cb(GtkComboBoxText *combo,
                                              gpointer         data);
static void          we_hook_save_currently_selected(GtkTreeModel     *model,
                                                     GtkTreeSelection *selection);
static void          we_hook_add_cb(GtkButton *button, gpointer data);
static void          we_hook_edit_cb(GtkButton *button, gpointer data);
static void          we_hook_delete_cb(GtkButton *button, gpointer data);
static void          we_hook_view_row_dblclick_cb(GtkTreeView        *view,
                                                  GtkTreePath        *path,
                                                  GtkTreeViewColumn  *col,
                                                  gpointer            data);
static gboolean      edit_hook(World *world, const gchar *event, Hook *hook);
static gboolean      we_hook_view_keypress_cb(GtkWidget   *widget,
                                              GdkEventKey *evt,
                                              gpointer     data);
static void          we_hook_move_cb(GtkButton *button, gpointer data);
static void          we_hook_export_cb(GtkButton *button, gpointer data);



GObject *
get_we_hooks_tab(World *world)
{
  GtkBuilder       *ui_builder;
  gchar            *objects[] = { "we_panel_generic_graphical_editor", NULL };
  GError           *error = NULL;
  GObject          *panel;
  GObject          *viewObjects;
  GtkLabel         *lblType;
  GObject          *btnAdd;
  GObject          *btnEdit;
  GObject          *btnDelete;
  GObject          *btnUp;
  GObject          *btnDown;
  GObject          *btnExport;
  GtkTreeSelection *selection;

  GtkWidget       *label;
  GtkComboBoxText *cmbWhichHook;
  GtkWidget       *hboxWhich;
  GtkBox          *vbox_int;

  /* Create the dialog */
  ui_builder = gtk_builder_new();
  if (!gtk_builder_add_objects_from_resource(ui_builder,
                                             "/ekalin/kildclient/kildclient.ui",
                                             objects,
                                             &error)) {
    g_warning(_("Error loading UI from XML file: %s"), error->message);
    g_error_free(error);
    return NULL; /* This will cause warnings in the calling function, but
                    there is need to abort the program because of this. */
  }

  panel = gtk_builder_get_object(ui_builder,
                                 "we_panel_generic_graphical_editor");
  /* So it's not destroyed when the builder is destroyed */
  g_object_ref(panel);

  viewObjects = gtk_builder_get_object(ui_builder, "viewObjects");
  g_object_set_data(viewObjects, "forworld", world);
  g_signal_connect(viewObjects, "row-activated",
                   G_CALLBACK(we_hook_view_row_dblclick_cb), world);
  g_signal_connect(viewObjects, "key-press-event",
                   G_CALLBACK(we_hook_view_keypress_cb), world);

  lblType = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblGEType"));
  gtk_label_set_markup(lblType,
                       "<span weight=\"bold\">Hooks</span>");

  btnAdd = gtk_builder_get_object(ui_builder, "btnGEAdd");
  g_signal_connect(btnAdd, "clicked",
                   G_CALLBACK(we_hook_add_cb), viewObjects);
  btnEdit = gtk_builder_get_object(ui_builder, "btnGEEdit");
  g_signal_connect(btnEdit, "clicked",
                   G_CALLBACK(we_hook_edit_cb), viewObjects);
  btnDelete = gtk_builder_get_object(ui_builder, "btnGEDelete");
  g_signal_connect(btnDelete, "clicked",
                   G_CALLBACK(we_hook_delete_cb), viewObjects);

  btnUp = gtk_builder_get_object(ui_builder, "btnGEUp");
  g_object_set_data(btnUp, "isup", GINT_TO_POINTER(TRUE));
  g_signal_connect(btnUp, "clicked",
                   G_CALLBACK(we_hook_move_cb), viewObjects);
  btnDown = gtk_builder_get_object(ui_builder, "btnGEDown");
  g_object_set_data(btnDown, "isup", GINT_TO_POINTER(FALSE));
  g_signal_connect(btnDown, "clicked",
                   G_CALLBACK(we_hook_move_cb), viewObjects);
  btnExport = gtk_builder_get_object(ui_builder, "btnGEExport");

  /* The manually added combo for selecting which hook */
  label = gtk_label_new_with_mnemonic(_("_Event:"));
  cmbWhichHook = GTK_COMBO_BOX_TEXT(gtk_combo_box_text_new());
  g_signal_connect(G_OBJECT(cmbWhichHook), "changed",
                   G_CALLBACK(we_hook_combo_changed_cb), viewObjects);
  gtk_label_set_mnemonic_widget(GTK_LABEL(label), GTK_WIDGET(cmbWhichHook));
  hboxWhich = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 8);
  gtk_box_pack_start(GTK_BOX(hboxWhich), label, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(hboxWhich), GTK_WIDGET(cmbWhichHook),
                     FALSE, FALSE, 0);

  gtk_combo_box_text_append_text(cmbWhichHook, "OnConnect");
  gtk_combo_box_text_append_text(cmbWhichHook, "OnDisconnect");
  gtk_combo_box_text_append_text(cmbWhichHook, "OnReceivedText");
  gtk_combo_box_text_append_text(cmbWhichHook, "OnSentCommand");
  gtk_combo_box_text_append_text(cmbWhichHook, "OnGetFocus");
  gtk_combo_box_text_append_text(cmbWhichHook, "OnLoseFocus");
  gtk_combo_box_text_append_text(cmbWhichHook, "OnCloseConnected");
  gtk_combo_box_text_append_text(cmbWhichHook, "OnServerData");

  vbox_int
    = GTK_BOX(gtk_builder_get_object(ui_builder,
                                     "vbox_int_generic_graphical_editor"));
  gtk_box_pack_start(vbox_int, hboxWhich, FALSE, FALSE, 0);
  gtk_box_reorder_child(vbox_int, hboxWhich, 0);

  create_we_hooks_models(world);

  we_hook_configure_view(GTK_TREE_VIEW(viewObjects));
  selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(viewObjects));
  gtk_tree_selection_set_mode(selection, GTK_SELECTION_MULTIPLE);
  g_object_set_data(G_OBJECT(selection), "btnEdit",   btnEdit);
  g_object_set_data(G_OBJECT(selection), "btnDelete", btnDelete);
  g_object_set_data(G_OBJECT(selection), "btnUp",     btnUp);
  g_object_set_data(G_OBJECT(selection), "btnDown",   btnDown);
  g_object_set_data(G_OBJECT(selection), "btnExport", btnExport);
  g_signal_connect(G_OBJECT(selection), "changed",
                   G_CALLBACK(we_guied_selection_changed_cb), NULL);

  /* Select first, will cause a model to be associated with the view */
  gtk_combo_box_set_active(GTK_COMBO_BOX(cmbWhichHook), 0);

  gtk_builder_connect_signals(ui_builder, world);
  /* The callback for the Export buttons in the Hooks tab is a
     different one - so we disconnect the handler from the XML UI file
     and connect the correct one. */
  g_signal_handlers_disconnect_by_func(btnExport, we_guied_export_cb, world);
  g_signal_connect(btnExport, "clicked",
                   G_CALLBACK(we_hook_export_cb), viewObjects);


  g_object_unref(ui_builder);
  return panel;
}


static
void
create_we_hooks_models(World *world)
{
  world->hooks.OnConnect_model = create_we_hooks_model(world->hooks.OnConnect);
  g_object_set_data(G_OBJECT(world->hooks.OnConnect_model),
                    "forevent", "OnConnect");
  g_object_set_data(G_OBJECT(world->hooks.OnConnect_model),
                    "selected_list", &world->selected_OnConnect_hooks);
  world->hooks.OnConnect_model_filter = GTK_TREE_MODEL_FILTER(gtk_tree_model_filter_new(world->hooks.OnConnect_model, NULL));
  g_object_unref(G_OBJECT(world->hooks.OnConnect_model));
  gtk_tree_model_filter_set_visible_func(GTK_TREE_MODEL_FILTER(world->hooks.OnConnect_model_filter),
                                         we_guied_plugin_filter_func,
                                         world,
                                         NULL);

  world->hooks.OnDisconnect_model = create_we_hooks_model(world->hooks.OnDisconnect);
  g_object_set_data(G_OBJECT(world->hooks.OnDisconnect_model),
                    "forevent", "OnDisconnect");
  g_object_set_data(G_OBJECT(world->hooks.OnDisconnect_model),
                    "selected_list", &world->selected_OnDisconnect_hooks);
  world->hooks.OnDisconnect_model_filter = GTK_TREE_MODEL_FILTER(gtk_tree_model_filter_new(world->hooks.OnDisconnect_model, NULL));
  g_object_unref(G_OBJECT(world->hooks.OnDisconnect_model));
  gtk_tree_model_filter_set_visible_func(GTK_TREE_MODEL_FILTER(world->hooks.OnDisconnect_model_filter),
                                         we_guied_plugin_filter_func,
                                         world,
                                         NULL);

  world->hooks.OnReceivedText_model = create_we_hooks_model(world->hooks.OnReceivedText);
  g_object_set_data(G_OBJECT(world->hooks.OnReceivedText_model),
                    "forevent", "OnReceivedText");
  g_object_set_data(G_OBJECT(world->hooks.OnReceivedText_model),
                    "selected_list", &world->selected_OnReceivedText_hooks);
  world->hooks.OnReceivedText_model_filter = GTK_TREE_MODEL_FILTER(gtk_tree_model_filter_new(world->hooks.OnReceivedText_model, NULL));
  g_object_unref(G_OBJECT(world->hooks.OnReceivedText_model));
  gtk_tree_model_filter_set_visible_func(GTK_TREE_MODEL_FILTER(world->hooks.OnReceivedText_model_filter),
                                         we_guied_plugin_filter_func,
                                         world,
                                         NULL);

  world->hooks.OnSentCommand_model = create_we_hooks_model(world->hooks.OnSentCommand);
  g_object_set_data(G_OBJECT(world->hooks.OnSentCommand_model),
                    "forevent", "OnSentCommand");
  g_object_set_data(G_OBJECT(world->hooks.OnSentCommand_model),
                    "selected_list", &world->selected_OnSentCommand_hooks);
  world->hooks.OnSentCommand_model_filter = GTK_TREE_MODEL_FILTER(gtk_tree_model_filter_new(world->hooks.OnSentCommand_model, NULL));
  g_object_unref(G_OBJECT(world->hooks.OnSentCommand_model));
  gtk_tree_model_filter_set_visible_func(GTK_TREE_MODEL_FILTER(world->hooks.OnSentCommand_model_filter),
                                         we_guied_plugin_filter_func,
                                         world,
                                         NULL);

  world->hooks.OnGetFocus_model = create_we_hooks_model(world->hooks.OnGetFocus);
  g_object_set_data(G_OBJECT(world->hooks.OnGetFocus_model),
                    "forevent", "OnGetFocus");
  g_object_set_data(G_OBJECT(world->hooks.OnGetFocus_model),
                    "selected_list", &world->selected_OnGetFocus_hooks);
  world->hooks.OnGetFocus_model_filter = GTK_TREE_MODEL_FILTER(gtk_tree_model_filter_new(world->hooks.OnGetFocus_model, NULL));
  g_object_unref(G_OBJECT(world->hooks.OnGetFocus_model));
  gtk_tree_model_filter_set_visible_func(GTK_TREE_MODEL_FILTER(world->hooks.OnGetFocus_model_filter),
                                         we_guied_plugin_filter_func,
                                         world,
                                         NULL);

  world->hooks.OnLoseFocus_model = create_we_hooks_model(world->hooks.OnLoseFocus);
  g_object_set_data(G_OBJECT(world->hooks.OnLoseFocus_model),
                    "forevent", "OnLoseFocus");
  g_object_set_data(G_OBJECT(world->hooks.OnLoseFocus_model),
                    "selected_list", &world->selected_OnLoseFocus_hooks);
  world->hooks.OnLoseFocus_model_filter = GTK_TREE_MODEL_FILTER(gtk_tree_model_filter_new(world->hooks.OnLoseFocus_model, NULL));
  g_object_unref(G_OBJECT(world->hooks.OnLoseFocus_model));
  gtk_tree_model_filter_set_visible_func(GTK_TREE_MODEL_FILTER(world->hooks.OnLoseFocus_model_filter),
                                         we_guied_plugin_filter_func,
                                         world,
                                         NULL);

  world->hooks.OnCloseConnected_model = create_we_hooks_model(world->hooks.OnCloseConnected);
  g_object_set_data(G_OBJECT(world->hooks.OnCloseConnected_model),
                    "forevent", "OnCloseConnected");
  g_object_set_data(G_OBJECT(world->hooks.OnCloseConnected_model),
                    "selected_list", &world->selected_OnCloseConnected_hooks);
  world->hooks.OnCloseConnected_model_filter = GTK_TREE_MODEL_FILTER(gtk_tree_model_filter_new(world->hooks.OnCloseConnected_model, NULL));
  g_object_unref(G_OBJECT(world->hooks.OnCloseConnected_model));
  gtk_tree_model_filter_set_visible_func(GTK_TREE_MODEL_FILTER(world->hooks.OnCloseConnected_model_filter),
                                         we_guied_plugin_filter_func,
                                         world,
                                         NULL);

  world->hooks.OnServerData_model = create_we_hooks_model(world->hooks.OnServerData);
  g_object_set_data(G_OBJECT(world->hooks.OnServerData_model),
                    "forevent", "OnServerData");
  g_object_set_data(G_OBJECT(world->hooks.OnServerData_model),
                    "selected_list", &world->selected_OnServerData_hooks);
  world->hooks.OnServerData_model_filter = GTK_TREE_MODEL_FILTER(gtk_tree_model_filter_new(world->hooks.OnServerData_model, NULL));
  g_object_unref(G_OBJECT(world->hooks.OnServerData_model));
  gtk_tree_model_filter_set_visible_func(GTK_TREE_MODEL_FILTER(world->hooks.OnServerData_model_filter),
                                         we_guied_plugin_filter_func,
                                         world,
                                         NULL);
}


static
GtkTreeModel *
create_we_hooks_model(GSList *hookptr)
{
  GtkListStore *store;
  GtkTreeIter   iter;
  Hook         *hook;

  store = gtk_list_store_new(WE_GUIED_N_COLS,
                             G_TYPE_POINTER);
  while (hookptr) {
    hook = (Hook *) hookptr->data;
    gtk_list_store_append(store, &iter);
    gtk_list_store_set(store, &iter,
                       WE_GUIED_POINTER, hook,
                       -1);

    hookptr = hookptr->next;
  }

  return GTK_TREE_MODEL(store);
}


static
void
we_hook_configure_view(GtkTreeView *view)
{
  GtkCellRenderer   *renderer;
  GtkTreeViewColumn *column;

  /* Column 1 - Enabled? */
  renderer = gtk_cell_renderer_toggle_new();
  g_object_set(G_OBJECT(renderer), "activatable", TRUE, NULL);
  g_object_set_data(G_OBJECT(renderer), "column",
                    GINT_TO_POINTER(HOOK_ENABLED));
  g_signal_connect(G_OBJECT(renderer), "toggled",
                   G_CALLBACK(we_guied_bool_col_toggled_cb), view);
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Enabled"));
  gtk_tree_view_column_pack_start(column, renderer, FALSE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_bool_func,
                                          GINT_TO_POINTER(HOOK_ENABLED),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 2 - Name */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Name"));
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_text_func,
                                          GINT_TO_POINTER(HOOK_NAME),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 3 - Action */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Action"));
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_text_func,
                                          GINT_TO_POINTER(HOOK_ACTION),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 4 - Plugin */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Plugin"));
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_text_func,
                                          GINT_TO_POINTER(OBJECT_PLUGIN),
                                          NULL);
  gtk_tree_view_append_column(view, column);
}


static
void
we_hook_combo_changed_cb(GtkComboBoxText *combo,
                         gpointer         data)
{
  GtkTreeView       *view;
  World             *world;
  gchar             *newevent;
  GtkTreeModel      *oldmodel;
  GtkTreeModel      *newmodel;
  GSList           **selected_list;
  GtkTreeSelection  *selection;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");
  oldmodel = gtk_tree_view_get_model(view);
  if (oldmodel) {
    /* Save selected rows */
    we_hook_save_currently_selected(oldmodel,
                                    gtk_tree_view_get_selection(view));
  }

  newevent = gtk_combo_box_text_get_active_text(combo);
  newmodel = get_hook_model(world, newevent);
  gtk_tree_view_set_model(view, newmodel);
  selected_list = g_object_get_data(G_OBJECT(newmodel), "selected_list");
  if (*selected_list) {
    /* Restore selected rows */
    GSList *listiter;

    selection = gtk_tree_view_get_selection(view);
    listiter = *selected_list;
    while (listiter) {
      GtkTreePath *path = gtk_tree_row_reference_get_path(listiter->data);
      gtk_tree_selection_select_path(selection, path);
      listiter = listiter->next;
    }
  }

  gtk_tree_view_columns_autosize(view);
  g_free(newevent);
}


static
void
we_hook_save_currently_selected(GtkTreeModel     *model,
                                GtkTreeSelection *selection)
{
  GSList **selected_list;
  GList   *curr_selected;

  selected_list = g_object_get_data(G_OBJECT(model), "selected_list");
  g_slist_foreach(*selected_list, (GFunc) gtk_tree_row_reference_free, NULL);
  g_slist_free(*selected_list);
  *selected_list = NULL;

  curr_selected = gtk_tree_selection_get_selected_rows(selection, NULL);
  if (curr_selected) {
    GList *listiter;

    listiter = curr_selected;
    while (listiter) {
      *selected_list = g_slist_append(*selected_list,
                                      gtk_tree_row_reference_new(model,
                                                                 listiter->data));
      listiter = listiter->next;
    }

    g_list_foreach(curr_selected, (GFunc) gtk_tree_path_free, NULL);
    g_list_free(curr_selected);
  }
}


static
void
we_hook_add_cb(GtkButton *button, gpointer data)
{
  World         *world;
  GtkTreeView   *view;
  GtkTreeModel  *model;
  gchar         *event;
  GSList       **hook_list;
  gint          *hookpos;
  Hook          *hook;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");
  model = gtk_tree_view_get_model(view);
  event = g_object_get_data(G_OBJECT(model), "forevent");

  hook = g_new0(Hook, 1);
  hook->enabled = 1;

  if (edit_hook(world, event, hook)) {
    hook_list = get_hook_list_for_writing(world, event, &hookpos);
    *hook_list = g_slist_insert(*hook_list,
                                hook,
                                *hookpos);
    we_hook_insert_hook(world, event, hook, (*hookpos)++);
  } else {
    g_free(hook);
  }
}

static
void
we_hook_edit_cb(GtkButton *button, gpointer data)
{
  /* Note that the Edit button is only active when exactly one row
     is selected. */
  World            *world;
  GtkTreeView      *view;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  GList            *selected;
  GtkTreeIter       iter;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");
  selection = gtk_tree_view_get_selection(view);
  selected = gtk_tree_selection_get_selected_rows(selection, &model);
  if (selected) {
    Hook  *hook;
    gchar *event;

    gtk_tree_model_get_iter(model, &iter, selected->data);
    gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &hook, -1);
    event = g_object_get_data(G_OBJECT(model), "forevent");

    if (edit_hook(world, event, hook)) {
      we_hook_update_hook(world, event, hook);
    }

    g_list_foreach(selected, (GFunc) gtk_tree_path_free, NULL);
    g_list_free(selected);
  }
}


static
void
we_hook_delete_cb(GtkButton *button, gpointer data)
{
  World            *world;
  GtkTreeView      *view;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  GList            *selected;
  GtkTreeIter       iter;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");
  selection = gtk_tree_view_get_selection(view);
  selected = gtk_tree_selection_get_selected_rows(selection, &model);
  if (selected) {
    Hook      *hook;
    GSList    *hooklist;
    gint       pos;
    gchar     *event;
    GtkWidget *msgdlg;
    gint       n;

    n = gtk_tree_selection_count_selected_rows(selection);
    msgdlg = we_guied_confirm_delete_dialog_new(GTK_WINDOW(world->dlgEditWorld),
                                                ngettext("hook", "hooks", n),
                                                n);

    if (!world->confirm_delete
        || gtk_dialog_run(GTK_DIALOG(msgdlg)) == GTK_RESPONSE_YES) {
      GList *listiter = g_list_last(selected);

      event = g_object_get_data(G_OBJECT(model), "forevent");
      get_hook_list_for_reading(world, event, &hooklist, NULL);
      while (listiter) {
        gtk_tree_model_get_iter(model, &iter, listiter->data);
        gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &hook, -1);
        pos = g_slist_index(hooklist, hook);
        delete_hook(world, event, pos, NULL);

        listiter = listiter->prev;
      }
    }
    gtk_widget_destroy(msgdlg);

    g_list_foreach(selected, (GFunc) gtk_tree_path_free, NULL);
    g_list_free(selected);
  }
}


static
void
we_hook_view_row_dblclick_cb(GtkTreeView        *view,
                             GtkTreePath        *path,
                             GtkTreeViewColumn  *col,
                             gpointer            data)
{
  World        *world;
  GtkTreeModel *model;
  GtkTreeIter   iter;

  world = (World *) data;
  model = gtk_tree_view_get_model(view);

  if (gtk_tree_model_get_iter(model, &iter, path)) {
    Hook  *hook;
    gchar *event;

    gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &hook, -1);
    event = g_object_get_data(G_OBJECT(model), "forevent");

    if (edit_hook(world, event, hook)) {
      we_hook_update_hook(world, event, hook);
    }
  }
}


static
gboolean
edit_hook(World *world, const gchar *event, Hook *hook)
{
  GtkBuilder      *ui_builder;
  gchar           *objects[] = { "dlgEditHook", NULL };
  GError          *error = NULL;
  GtkWidget       *dlgEditHook;
  GtkLabel        *lblEvent;
  GtkEntry        *txtName;
  GtkEntry        *txtAction;
  GtkToggleButton *chkEnabled;
  const gchar     *newname;
  const gchar     *newaction;

  ui_builder = gtk_builder_new();
  if (!gtk_builder_add_objects_from_resource(ui_builder,
                                             "/ekalin/kildclient/kildclient.ui",
                                             objects,
                                             &error)) {
    g_warning(_("Error loading UI from XML file: %s"), error->message);
    g_error_free(error);
    return FALSE;
  }

  dlgEditHook = GTK_WIDGET(gtk_builder_get_object(ui_builder, "dlgEditHook"));
  gtk_window_set_transient_for(GTK_WINDOW(dlgEditHook),
                               GTK_WINDOW(world->dlgEditWorld));
  gtk_dialog_set_default_response(GTK_DIALOG(dlgEditHook),
                                  GTK_RESPONSE_OK);

  lblEvent    = GTK_LABEL(gtk_builder_get_object(ui_builder,
                                                 "lblHookEvent"));
  txtName     = GTK_ENTRY(gtk_builder_get_object(ui_builder,
                                                 "txtHookName"));
  txtAction   = GTK_ENTRY(gtk_builder_get_object(ui_builder,
                                                 "txtHookAction"));
  chkEnabled  = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder,
                                                         "chkHookEnabled"));

  /* Only display note when editing a plugin's trigger */
  if (!hook->owner_plugin) {
    GObject *label;

    label = gtk_builder_get_object(ui_builder, "lblNoteHook");
    gtk_widget_destroy(GTK_WIDGET(label));
    label = gtk_builder_get_object(ui_builder, "lblNoteTextHook");
    gtk_widget_destroy(GTK_WIDGET(label));
  }

  /* We don't need it anymore */
  g_object_unref(ui_builder);

  /* Fill-in values */
  gtk_label_set_text(lblEvent, event);
  if (hook->name) {
    gtk_entry_set_text(txtName, hook->name);
  }
  if (hook->action) {
    gtk_entry_set_text(txtAction, hook->action);
  }
  gtk_toggle_button_set_active(chkEnabled,  hook->enabled);

  /* Run the dialog until the input is valid or cancelled */
  gtk_widget_show_all(dlgEditHook);
  while (1) {
    if (gtk_dialog_run(GTK_DIALOG(dlgEditHook)) == GTK_RESPONSE_OK) {
      /* Validate */
      newaction = gtk_entry_get_text(txtAction);
      if (strcmp(newaction, "") == 0) {
        kc_error_dialog(GTK_WINDOW(dlgEditHook),
                        _("You must specify the action."));
        continue;
      }

      /* Update values */
      newname = gtk_entry_get_text(txtName);
      if (!hook->name || strcmp(newname, hook->name) != 0) {
        g_free(hook->name);
        if (strcmp(newname, "") != 0) {
          hook->name = g_strdup(newname);
        } else {
          hook->name = NULL;
        }
      }

      if (!hook->action || strcmp(newaction, hook->action) != 0) {
        g_free(hook->action);
        hook->action = g_strdup(newaction);
      }

      hook->enabled = gtk_toggle_button_get_active(chkEnabled);

      /* We've finished successfully */
      gtk_widget_destroy(dlgEditHook);
      return TRUE;
    } else {
      /* Cancel pressed */
      gtk_widget_destroy(dlgEditHook);
      return FALSE;
    }
  }
}


static
gboolean
we_hook_view_keypress_cb(GtkWidget   *widget,
                         GdkEventKey *evt,
                         gpointer     data)
{
  if (evt->keyval == GDK_KEY_Delete || evt->keyval == GDK_KEY_KP_Delete) {
    we_hook_delete_cb(NULL, widget);
    return TRUE;
  }

  return FALSE;
}


static
void
we_hook_move_cb(GtkButton *button, gpointer data)
{
  GtkTreeView      *view;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  GList            *selected;
  GtkTreeIter       iter;
  World            *world;
  gboolean          is_up;
  gint              new_pos;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");
  selection = gtk_tree_view_get_selection(view);
  is_up = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(button), "isup"));
  selected = gtk_tree_selection_get_selected_rows(selection, &model);
  if (selected) {
    GSList *hooklist;
    gint    hook_pos;
    Hook   *hook;
    gchar  *event;
    gint    pos;

    gtk_tree_model_get_iter(model, &iter, selected->data);
    gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &hook, -1);
    event = g_object_get_data(G_OBJECT(model), "forevent");
    get_hook_list_for_reading(world, event, &hooklist, &hook_pos);
    pos = g_slist_index(hooklist, hook);
    new_pos = pos;
    if (is_up) {
      if (pos != 0) {
        new_pos = pos - 1;
        move_hook(world, event, pos, new_pos);
      }
    } else {
      if (pos != hook_pos - 1) {
        new_pos = pos + 1;
        move_hook(world, event, pos, new_pos);
      }
    }
    /* Reselect the moved item */
    gtk_tree_model_iter_nth_child(model, &iter, NULL, new_pos);
    gtk_tree_selection_select_iter(selection, &iter);

    g_list_foreach(selected, (GFunc) gtk_tree_path_free, NULL);
    g_list_free(selected);
  }
}


static
void
we_hook_export_cb(GtkButton *button, gpointer data)
{
  GtkTreeView  *view;
  GtkTreeModel *model;
  World        *world;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");
  model = gtk_tree_view_get_model(view);

  we_hook_save_currently_selected(model, gtk_tree_view_get_selection(view));

  g_object_set_data(G_OBJECT(button), "preselect",
                    g_object_get_data(G_OBJECT(model), "forevent"));
  we_guied_export_cb(button, world);
}


void
we_hook_update_hook(World *world, const gchar *event, Hook *hook_arg)
{
  /* Called by the Perl functions when an hook is changed, so that
     the display is updated. */
  GtkTreeIter   iter;
  GtkTreePath  *path;
  GtkTreeModel *model;
  Hook         *hook;
  gboolean      success;

  model = get_hook_model(world, event);
  if (!model) {
    return;
  }

  success = gtk_tree_model_get_iter_first(model, &iter);
  while (success) {
    gtk_tree_model_get(model, &iter,
                       WE_GUIED_POINTER, &hook, -1);
    if (hook == hook_arg) {
      path = gtk_tree_model_get_path(model, &iter);
      gtk_tree_model_row_changed(model, path, &iter);
      gtk_tree_path_free(path);
      return;
    }
    success = gtk_tree_model_iter_next(model, &iter);
  }
}


void
we_hook_insert_hook(World *world, const gchar *event, Hook *hook, gint pos)
{
  /* Called by the Perl functions when an hook is inserted, so that
     the display is updated. */
  GtkTreeModel *model;
  GtkTreeIter   iter;

  model = get_hook_model(world, event);
  if (!model) {
    return;
  }

  gtk_list_store_insert(GTK_LIST_STORE(model), &iter, pos);
  gtk_list_store_set(GTK_LIST_STORE(model), &iter,
                     WE_GUIED_POINTER, hook,
                     -1);
}


void
we_hook_delete_hook(World *world, const gchar *event, Hook *hook_arg)
{
  /* Called by the Perl functions when an hook is deleted, so that
     the display is updated. */
  GtkTreeIter   iter;
  GtkTreeModel *model;
  Hook         *hook;
  gboolean      success;

  model = get_hook_model(world, event);
  if (!model) {
    return;
  }

  success = gtk_tree_model_get_iter_first(model, &iter);
  while (success) {
    gtk_tree_model_get(model, &iter,
                       WE_GUIED_POINTER, &hook, -1);
    if (hook == hook_arg) {
      gtk_list_store_remove(GTK_LIST_STORE(model), &iter);
      return;
    }
    success = gtk_tree_model_iter_next(model, &iter);
  }
}
