/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include "libintl-wrapper.h"
#include <gtk/gtk.h>

#include "kildclient.h"
#include "perlscript.h"


/***********************
 * Function prototypes *
 ***********************/
/* XML UI signals */
G_MODULE_EXPORT void color_changed_cb(GtkColorButton *btn, gpointer data);
G_MODULE_EXPORT void set_default_colors_cb(GtkButton *btn, gpointer data);



GObject *
get_we_colors_tab(World *world, GtkBuilder *ui_builder)
{
  GObject *panel;
  GObject *colorBtnAnsi;
  GObject *colorBtnDeffore;
  GObject *colorBtnDefbold;
  GObject *colorBtnDefback;
  GObject *colorBtnDefboldback;
  gchar    btnname[18]; /* 18 is enough to hold the button names */
  int      i;

  panel = gtk_builder_get_object(ui_builder, "we_panel_colors");

  /* Ansi colors category */
  for (i = 0; i < 16; ++i) {
    sprintf(btnname, "colorBtnAnsi[%d]", i);
    colorBtnAnsi = gtk_builder_get_object(ui_builder, btnname);
    gtk_color_chooser_set_rgba(GTK_COLOR_CHOOSER(colorBtnAnsi),
                               &world->ansicolors[i]);
    g_object_set_data(colorBtnAnsi, "colorptr", &world->ansicolors[i]);
  }

  /* Default colors category */
  colorBtnDeffore = gtk_builder_get_object(ui_builder, "colorBtnDeffore");
  gtk_color_chooser_set_rgba(GTK_COLOR_CHOOSER(colorBtnDeffore),
                             world->deffore);
  g_object_set_data(colorBtnDeffore, "colorptr", world->deffore);

  colorBtnDefbold = gtk_builder_get_object(ui_builder, "colorBtnDefbold");
  gtk_color_chooser_set_rgba(GTK_COLOR_CHOOSER(colorBtnDefbold),
                             world->defbold);
  g_object_set_data(colorBtnDefbold, "colorptr", world->defbold);

  colorBtnDefback = gtk_builder_get_object(ui_builder, "colorBtnDefback");
  gtk_color_chooser_set_rgba(GTK_COLOR_CHOOSER(colorBtnDefback),
                             world->defback);
  g_object_set_data(colorBtnDefback, "colorptr", world->defback);

  colorBtnDefboldback = gtk_builder_get_object(ui_builder,
                                               "colorBtnDefboldback");
  gtk_color_chooser_set_rgba(GTK_COLOR_CHOOSER(colorBtnDefboldback),
                             world->defboldback);
  g_object_set_data(colorBtnDefboldback, "colorptr", world->defboldback);

  return panel;
}


void
color_changed_cb(GtkColorButton *btn, gpointer data)
{
  World    *world = (World *) data;
  WorldGUI *gui   = world->gui;
  GdkRGBA  *color = (GdkRGBA *) g_object_get_data(G_OBJECT(btn), "colorptr");

  gtk_color_chooser_get_rgba(GTK_COLOR_CHOOSER(btn), color);
  if (gui) {
    ansitextview_update_color_tags(gui, gui->world);
  }
}


void
set_default_colors_cb(GtkButton *btn, gpointer data)
{
  World           *world = (World *) data;
  WorldGUI        *gui   = world->gui;
  GtkBuilder      *ui_builder;
  GtkColorChooser *colorBtnAnsi;
  GtkColorChooser *colorBtnDeffore;
  GtkColorChooser *colorBtnDefbold;
  GtkColorChooser *colorBtnDefback;
  GtkColorChooser *colorBtnDefboldback;
  gchar            btnname[18]; /* 18 is enough to hold the button names */
  int              i;

  ui_builder = world->ui_builder;

  colorBtnDeffore
    = GTK_COLOR_CHOOSER(gtk_builder_get_object(ui_builder, "colorBtnDeffore"));
  colorBtnDefbold
    = GTK_COLOR_CHOOSER(gtk_builder_get_object(ui_builder, "colorBtnDefbold"));
  colorBtnDefback
    = GTK_COLOR_CHOOSER(gtk_builder_get_object(ui_builder, "colorBtnDefback"));
  colorBtnDefboldback
    = GTK_COLOR_CHOOSER(gtk_builder_get_object(ui_builder, "colorBtnDefboldback"));

  memcpy(world->deffore, &deffore, sizeof(GdkRGBA));
  gtk_color_chooser_set_rgba(colorBtnDeffore, world->deffore);

  memcpy(world->defbold, &defbold, sizeof(GdkRGBA));
  gtk_color_chooser_set_rgba(colorBtnDefbold, world->defbold);

  memcpy(world->defback, &defback, sizeof(GdkRGBA));
  gtk_color_chooser_set_rgba(colorBtnDefback, world->defback);

  memcpy(world->defboldback, &defboldback, sizeof(GdkRGBA));
  gtk_color_chooser_set_rgba(colorBtnDefboldback, world->defboldback);

  memcpy(world->ansicolors, &defansi, 16*sizeof(GdkRGBA));
  for (i = 0; i < 16; ++i) {
    sprintf(btnname, "colorBtnAnsi[%d]", i);
    colorBtnAnsi = GTK_COLOR_CHOOSER(gtk_builder_get_object(ui_builder,
                                                            btnname));
    gtk_color_chooser_set_rgba(colorBtnAnsi, &world->ansicolors[i]);
  }

  if (gui) {
    ansitextview_update_color_tags(gui, gui->world);
  }
}
