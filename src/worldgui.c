/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "libintl-wrapper.h"
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#ifdef __MINGW32__
#  include <windows.h>
#  include <shellapi.h>
#endif

#include "kcircularqueue.h"
#include "simocombobox.h"

#include "kildclient.h"
#include "ansi.h"
#include "perlscript.h"


/******************
 * File variables *
 ******************/
static gboolean   hovering_over_link = FALSE;
static GdkCursor *hand_cursor = NULL;
static GdkCursor *regular_cursor = NULL;


/***********************
 * Function prototypes *
 ***********************/
static void      create_css_provider(WorldGUI *gui);
static void      combo_changed_cb(SimoComboBox *combo, gpointer data);
static void      combo_poped_cb(SimoComboBox *combo, gpointer data);
static void      combo_closed_cb(SimoComboBox *combo, gpointer data);
static void      n_lines_changed_cb(SimoComboBox *combo, gpointer data);
static void      activate_cb(SimoComboBox *cmb, gpointer data);
static gboolean  keypress_cb(GtkWidget   *widget,
                             GdkEventKey *evt,
                             gpointer    data);
static void      text_changed_cb(SimoComboBox *cmb, gpointer data);
static void      cursor_moved_cb(GtkTextBuffer *buffer,
                                 GtkTextMark   *mark,
                                 gpointer       data);
static gboolean  completion_function(GtkEntryCompletion *completion,
                                     const gchar        *key,
                                     GtkTreeIter        *iter,
                                     gpointer            user_data);
static gboolean  completion_match_selected_cb(GtkEntryCompletion *widget,
                                              GtkTreeModel       *model,
                                              GtkTreeIter        *iter,
                                              gpointer            data);
static void      tab_close_cb(GtkWidget *widget, gpointer data);
static gboolean  textview_button_press_cb(GtkWidget      *text_view,
                                          GdkEventButton *event,
                                          gpointer        data);
static gboolean  act_if_link(WorldGUI       *gui,
                             GtkTextIter    *iter,
                             GtkWidget      *widget,
                             GdkEventButton *event);
static gboolean  textview_motion_notify_event(GtkWidget      *widget,
                                              GdkEventMotion *event,
                                              gpointer        data);
static void      set_cursor_if_appropriate(GtkTextView *text_view,
                                           WorldGUI    *gui,
                                           gint         x,
                                           gint         y);
static gboolean  worldgui_query_tooltip_cb(GtkWidget  *widget,
                                           gint        x,
                                           gint        y,
                                           gboolean    keyboard_mode,
                                           GtkTooltip *tooltip,
                                           gpointer    data);
static gchar    *worldgui_get_tooltip_text(WorldGUI    *gui,
                                           GtkTextView *text_view,
                                           gint         win_x,
                                           gint         win_y);
static void      textview_size_allocate_cb(GtkWidget     *widget,
                                           GtkAllocation *allocation,
                                           gpointer       data);
static void      find_incremental_cb(GtkEditable *widget, gpointer data);
static void      do_find(WorldGUI *gui);
static void      close_search_box_cb(GtkButton *widget, gpointer data);
static gboolean  search_box_keypress_cb(GtkWidget   *widget,
                                        GdkEventKey *evt,
                                        gpointer     data);
static void      scrolled_win_size_allocate_cb(GtkWidget     *widget,
                                               GtkAllocation *allocation,
                                               gpointer       data);
static void      scrolled_win_value_changed_cb(GtkAdjustment *adjustment,
                                               gpointer       data);
static gboolean  set_split_to_zero(gpointer data);
/* XML UI Signals */
G_MODULE_EXPORT void menu_popup_url_copy(GtkMenuItem *menu, gchar *url);



WorldGUI *
world_gui_new(gboolean is_for_mud)
{
  /* is_for_mud determines the kind of GUI we're creating.
     If TRUE, it is used for really displaying MUD output, and it
     will be included in the notebook.
     If FALSE, it is a KCWin gui, and it has less features.
  */
  WorldGUI      *gui;
  GtkWidget     *frame;
  GtkAdjustment *verticalAdjust;

  GtkTreeViewColumn *column;
  GtkCellRenderer   *renderer;

  GtkTextIter end_iter;


  /* First, create the cursors if this is the first run */
  if (!hand_cursor) {
    hand_cursor = gdk_cursor_new_for_display(gdk_display_get_default(),
                                             GDK_HAND2);
    regular_cursor = gdk_cursor_new_for_display(gdk_display_get_default(),
                                                GDK_XTERM);
  }

  gui = g_new0(WorldGUI, 1);

  create_css_provider(gui);

  gui->vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
  g_object_set_data(G_OBJECT(gui->vbox), "gui", gui);

  gui->scrolled_win = GTK_SCROLLED_WINDOW(gtk_scrolled_window_new(NULL, NULL));
  gtk_scrolled_window_set_policy(gui->scrolled_win,
                                 GTK_POLICY_AUTOMATIC,
                                 GTK_POLICY_ALWAYS);
  /* With the default (on), scroll-to-end does not work with GTK+ >= 3.13 */
  g_object_set(gtk_widget_get_settings(GTK_WIDGET(gui->scrolled_win)),
               "gtk-enable-animations", FALSE,
               NULL);

  gui->txtView = GTK_TEXT_VIEW(gtk_text_view_new());
  gtk_widget_set_can_focus(GTK_WIDGET(gui->txtView), FALSE);
  add_css_provider_and_class(GTK_WIDGET(gui->txtView),
                             gui->css_provider, "mud-output");
  gtk_text_view_set_editable(gui->txtView, FALSE);
  gui->txtBuffer = gtk_text_view_get_buffer(gui->txtView);
  /* Store a mark at the end, to allow scrolling to it */
  gtk_text_buffer_get_iter_at_offset(gui->txtBuffer, &end_iter, -1);
  gui->txtmark_end = gtk_text_buffer_create_mark(gui->txtBuffer,
                                                 "end",
                                                 &end_iter,
                                                 FALSE);
  gtk_container_add(GTK_CONTAINER(gui->scrolled_win), GTK_WIDGET(gui->txtView));

  if (is_for_mud) {
    g_signal_connect(G_OBJECT(gui->scrolled_win), "size-allocate",
                     G_CALLBACK(scrolled_win_size_allocate_cb), gui);
    verticalAdjust = gtk_scrolled_window_get_vadjustment(gui->scrolled_win);
    g_signal_connect(G_OBJECT(verticalAdjust), "value-changed",
                     G_CALLBACK(scrolled_win_value_changed_cb), gui);

    /* Second TextView for split screen */
    gui->scrwinScroll = GTK_SCROLLED_WINDOW(gtk_scrolled_window_new(NULL, NULL));
    gtk_scrolled_window_set_policy(gui->scrwinScroll,
                                   GTK_POLICY_AUTOMATIC,
                                   GTK_POLICY_ALWAYS);
    gui->txtViewScroll
      = GTK_TEXT_VIEW(gtk_text_view_new_with_buffer(gui->txtBuffer));
    gtk_widget_set_can_focus(GTK_WIDGET(gui->txtViewScroll), FALSE);
    add_css_provider_and_class(GTK_WIDGET(gui->txtViewScroll),
                               gui->css_provider, "mud-output");
    gtk_text_view_set_editable(gui->txtViewScroll, FALSE);
    gtk_container_add(GTK_CONTAINER(gui->scrwinScroll),
                      GTK_WIDGET(gui->txtViewScroll));

    /* Create split pane */
    gui->split_pane = GTK_PANED(gtk_paned_new(GTK_ORIENTATION_VERTICAL));
    gtk_paned_pack1(gui->split_pane, GTK_WIDGET(gui->scrwinScroll), FALSE, TRUE);
    gtk_paned_pack2(gui->split_pane, GTK_WIDGET(gui->scrolled_win), TRUE, FALSE);
    /* For some reason, we cannot set to 0 directly. */
    g_idle_add(set_split_to_zero, gui);

    gtk_box_pack_start(GTK_BOX(gui->vbox), GTK_WIDGET(gui->split_pane),
                       TRUE, TRUE, 0);
  } else {
    /* No split screen. However, we create the text view so that the
       functions that configure it do not fail. */
    gui->txtViewScroll
      = GTK_TEXT_VIEW(gtk_text_view_new_with_buffer(gui->txtBuffer));
    gtk_box_pack_start(GTK_BOX(gui->vbox), GTK_WIDGET(gui->scrolled_win),
                       TRUE, TRUE, 0);
  }

  if (is_for_mud) {
    /* Search box */
    GtkWidget *btnClose;
    GtkWidget *label;

    gui->search_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 6);
    gtk_container_set_border_width(GTK_CONTAINER(gui->search_box), 2);
    g_object_set(G_OBJECT(gui->search_box), "no-show-all", TRUE, NULL);

    btnClose = gtk_button_new_from_icon_name("window-close",
                                             GTK_ICON_SIZE_BUTTON);
    gtk_button_set_relief(GTK_BUTTON(btnClose), GTK_RELIEF_NONE);
    g_signal_connect(G_OBJECT(btnClose), "clicked",
                     G_CALLBACK(close_search_box_cb), gui);
    gtk_box_pack_start(GTK_BOX(gui->search_box), btnClose, FALSE, FALSE, 0);

    label = gtk_label_new_with_mnemonic(_("_Find:"));
    gtk_box_pack_start(GTK_BOX(gui->search_box), label, FALSE, FALSE, 0);

    gui->txtSearchTerm = gtk_entry_new();
    gtk_label_set_mnemonic_widget(GTK_LABEL(label), gui->txtSearchTerm);
    g_signal_connect(G_OBJECT(gui->txtSearchTerm), "changed",
                     G_CALLBACK(find_incremental_cb), gui);
    g_signal_connect(G_OBJECT(gui->txtSearchTerm), "activate",
                     G_CALLBACK(menu_findnext_activate_cb), NULL);
    g_signal_connect(G_OBJECT(gui->txtSearchTerm), "key-press-event",
                     G_CALLBACK(search_box_keypress_cb), gui);
    gtk_box_pack_start(GTK_BOX(gui->search_box), gui->txtSearchTerm,
                       FALSE, FALSE, 0);

    gui->btnFindNext = gtk_button_new_with_mnemonic(_("Find n_ext"));
    gtk_button_set_relief(GTK_BUTTON(gui->btnFindNext), GTK_RELIEF_NONE);
    g_signal_connect(G_OBJECT(gui->btnFindNext), "clicked",
                     G_CALLBACK(find_next_cb), gui);
    gtk_box_pack_start(GTK_BOX(gui->search_box), gui->btnFindNext,
                       FALSE, FALSE, 0);

    gui->lblSearchInfo = GTK_LABEL(gtk_label_new(NULL));
    gtk_box_pack_start(GTK_BOX(gui->search_box),
                       GTK_WIDGET(gui->lblSearchInfo),
                       FALSE, FALSE, 0);

    gtk_box_pack_start(GTK_BOX(gui->vbox), gui->search_box, FALSE, FALSE, 0);
  }

  /* Command area */
  gui->commandArea = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 4);
  gtk_container_set_border_width(GTK_CONTAINER(gui->commandArea), 2);

  gui->btnClear = gtk_button_new_from_icon_name("edit-clear",
                                                GTK_ICON_SIZE_MENU);
  g_signal_connect(G_OBJECT(gui->btnClear), "clicked",
                   G_CALLBACK(clear_button_cb), gui);
  gtk_box_pack_start(GTK_BOX(gui->commandArea), gui->btnClear,
                     FALSE, FALSE, 0);

  gtk_widget_set_tooltip_text(gui->btnClear,
                              _("Click to clear the command input area."));

  if (is_for_mud) {
    /* The full-featured SimoComboBox */
    gui->cmbEntry = SIMO_COMBO_BOX(simo_combo_box_new());
    add_css_provider_and_class(simo_combo_box_get_single_line_widget(gui->cmbEntry),
                               gui->css_provider, "input");
    add_css_provider_and_class(simo_combo_box_get_multi_line_widget(gui->cmbEntry),
                               gui->css_provider, "input");
    g_signal_connect(G_OBJECT(gui->cmbEntry), "size-changed",
                     G_CALLBACK(n_lines_changed_cb), gui);
    g_signal_connect(G_OBJECT(gui->cmbEntry), "clicked",
                     G_CALLBACK(combo_changed_cb), gui);
    g_signal_connect(G_OBJECT(gui->cmbEntry), "popup-displayed",
                     G_CALLBACK(combo_poped_cb), gui);
    g_signal_connect(G_OBJECT(gui->cmbEntry), "popup-closed",
                     G_CALLBACK(combo_closed_cb), gui);
    g_signal_connect(G_OBJECT(gui->cmbEntry), "activate",
                     G_CALLBACK(activate_cb), gui);
    g_signal_connect(G_OBJECT(simo_combo_box_get_multi_line_widget(SIMO_COMBO_BOX(gui->cmbEntry))),
                     "key-press-event",
                     G_CALLBACK(keypress_cb), gui);
    g_signal_connect(G_OBJECT(simo_combo_box_get_single_line_widget(SIMO_COMBO_BOX(gui->cmbEntry))),
                     "key-press-event",
                     G_CALLBACK(keypress_cb), gui);
    g_signal_connect(G_OBJECT(gui->cmbEntry), "changed",
                     G_CALLBACK(text_changed_cb), gui);
    g_signal_connect(G_OBJECT(gui->cmbEntry), "mark-set",
                     G_CALLBACK(cursor_moved_cb), gui);
    gtk_box_pack_start(GTK_BOX(gui->commandArea), GTK_WIDGET(gui->cmbEntry),
                       TRUE, TRUE, 0);
    /* Configure renderer for combo column */
    renderer = gtk_cell_renderer_text_new();
    column   = gtk_tree_view_column_new();
    gtk_tree_view_column_pack_start(column, renderer, TRUE);
    gtk_tree_view_column_set_cell_data_func(column,
                                            renderer,
                                            (GtkTreeCellDataFunc) completion_cell_data_function,
                                            NULL,
                                            NULL);
    simo_combo_box_set_combo_column(gui->cmbEntry, column);
  } else {
    /* Just a GtkEntry */
    GtkWidget *widget = gtk_entry_new();
    add_css_provider_and_class(widget, gui->css_provider, "input");
    gtk_box_pack_start(GTK_BOX(gui->commandArea), widget, TRUE, TRUE, 0);
    gui->txtEntry = GTK_ENTRY(widget);
  }
  gtk_box_pack_start(GTK_BOX(gui->vbox), gui->commandArea, FALSE, FALSE, 0);

  /* Set initial colors and state */
  gui->ta_state.fg_color = gui->saved_ta_state.fg_color
    = ANSI_DEFAULT_COLOR_IDX;
  gui->ta_state.bg_color = gui->saved_ta_state.bg_color
    = ANSI_DEFAULT_COLOR_IDX;
  gui->ta_state.underline = PANGO_UNDERLINE_NONE;

  /* Create tag for text with attributes */
  gui->ta.underline_tag = gtk_text_buffer_create_tag(gui->txtBuffer,
                                                     NULL,
                                                     "underline",
                                                     PANGO_UNDERLINE_SINGLE,
                                                     NULL);
  gui->ta.dblunderline_tag = gtk_text_buffer_create_tag(gui->txtBuffer,
                                                        NULL,
                                                        "underline",
                                                        PANGO_UNDERLINE_DOUBLE,
                                                        NULL);
  gui->ta.strike_tag = gtk_text_buffer_create_tag(gui->txtBuffer,
                                                  NULL,
                                                  "strikethrough",
                                                  TRUE,
                                                  NULL);
  gui->ta.italics_tag = gtk_text_buffer_create_tag(gui->txtBuffer,
                                                   NULL,
                                                   "style",
                                                   PANGO_STYLE_ITALIC,
                                                   NULL);

  /* Initialize structures in TextAppearance */
  gui->ta.rgb_fore_tags = g_hash_table_new(NULL, NULL);
  gui->ta.rgb_back_tags = g_hash_table_new(NULL, NULL);

  /* We don't know yet which characters are supported. */
  gui->ta.sup_geom_shapes   = -1;
  gui->ta.sup_block         = -1;
  gui->ta.sup_control       = -1;
  gui->ta.sup_l1_supplement = -1;
  gui->ta.sup_box_drawing   = -1;
  gui->ta.sup_misc_tech     = -1;
  gui->ta.sup_math          = -1;
  gui->ta.sup_greek         = -1;

  if (is_for_mud) {   /* Special features */
    GtkWidget *btnClose;

    /* Notebook tab */
    gui->hboxTab = GTK_BOX(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 4));
    gui->lblNotebook = GTK_LABEL(gtk_label_new(_("No world")));
    gtk_box_pack_start(gui->hboxTab, GTK_WIDGET(gui->lblNotebook),
                       FALSE, FALSE, 0);

    btnClose = gtk_button_new_from_icon_name("window-close",
                                             GTK_ICON_SIZE_BUTTON);
    /* Make the button smaller. (Taken from gedit code) */
    gtk_widget_set_focus_on_click(btnClose, FALSE);
    gtk_button_set_relief(GTK_BUTTON(btnClose), GTK_RELIEF_NONE);

    g_signal_connect(G_OBJECT(btnClose), "clicked",
                     G_CALLBACK(tab_close_cb), gui);
    gtk_box_pack_start(GTK_BOX(gui->hboxTab), btnClose,
                       FALSE, FALSE, 0);

    /* Detect resizes */
    g_signal_connect(G_OBJECT(gui->txtView), "size-allocate",
                     G_CALLBACK(textview_size_allocate_cb), gui);

    /* Create a mark to hold the start of the current line */
    gui->txtmark_linestart = gtk_text_buffer_create_mark(gui->txtBuffer,
                                                         "linestart",
                                                         &end_iter,
                                                         TRUE);
    /* Create tag for URLs */
    gui->txttag_url = gtk_text_buffer_create_tag(gui->txtBuffer,
                                                 NULL,
                                                 "underline",
                                                 PANGO_UNDERLINE_SINGLE,
                                                 NULL);
    /* URLs support */
    g_signal_connect(gui->txtView, "button-press-event",
                     G_CALLBACK(textview_button_press_cb), gui);
    g_signal_connect(gui->txtViewScroll, "button-press-event",
                     G_CALLBACK(textview_button_press_cb), gui);
    g_signal_connect(gui->txtView, "motion-notify-event",
                     G_CALLBACK(textview_motion_notify_event), gui);
    g_signal_connect(gui->txtViewScroll, "motion-notify-event",
                     G_CALLBACK(textview_motion_notify_event), gui);

    /* Tooltips */
    g_signal_connect(G_OBJECT(gui->txtView), "query-tooltip",
                     G_CALLBACK(worldgui_query_tooltip_cb), gui);
    g_signal_connect(G_OBJECT(gui->txtViewScroll), "query-tooltip",
                     G_CALLBACK(worldgui_query_tooltip_cb), gui);

    /* Status bar */
    gui->statusbar_box = GTK_BOX(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 4));

    gui->lblStatus = GTK_LABEL(gtk_label_new(_("KildClient ready")));
    add_css_provider_and_class(GTK_WIDGET(gui->lblStatus),
                               gui->css_provider,
                               "status-bar");
    gtk_widget_set_halign(GTK_WIDGET(gui->lblStatus), GTK_ALIGN_START);
    gtk_label_set_single_line_mode(gui->lblStatus, TRUE);
    gtk_label_set_ellipsize(gui->lblStatus, PANGO_ELLIPSIZE_END);
    gtk_box_pack_start(gui->statusbar_box, GTK_WIDGET(gui->lblStatus),
                       TRUE, TRUE, 0);

    gui->lblLines = GTK_LABEL(gtk_label_new(_("0 lines")));
    gtk_widget_set_halign(GTK_WIDGET(gui->lblLines), GTK_ALIGN_START);
    gtk_label_set_single_line_mode(gui->lblLines, TRUE);
    frame = gtk_frame_new(NULL);
    gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_IN);
    gtk_container_add(GTK_CONTAINER(frame), GTK_WIDGET(gui->lblLines));
    gtk_box_pack_start(gui->statusbar_box, frame,
                       FALSE, FALSE, 0);

    gui->lblTime = GTK_LABEL(gtk_label_new(NULL));
    gtk_widget_set_halign(GTK_WIDGET(gui->lblTime), GTK_ALIGN_START);
    gtk_label_set_single_line_mode(gui->lblTime, TRUE);
    frame = gtk_frame_new(NULL);
    gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_IN);
    gtk_container_add(GTK_CONTAINER(frame), GTK_WIDGET(gui->lblTime));
    gtk_box_pack_start(gui->statusbar_box, frame,
                       FALSE, FALSE, 0);

    gtk_box_pack_start(GTK_BOX(gui->vbox), GTK_WIDGET(gui->statusbar_box),
                       FALSE, FALSE, 0);

    /* Clear lines at the beginning of the buffer to keep it to a
       reasonable size. */
    gui->prune_timeout_id = g_timeout_add(1000,
                                          ansitextview_prune_extra_lines,
                                          gui);
  }

  return gui;
}


void
create_css_provider(WorldGUI *gui)
{
  gui->css_provider = gtk_css_provider_new();
  configure_css(gui, default_world);
}


void
world_gui_size_textview(WorldGUI *gui)
{
  gint char_height;
  gint char_width;

  ansitextview_get_char_size(gui, &char_height, &char_width);
  gtk_widget_set_size_request(GTK_WIDGET(gui->txtView),
                              80 * char_width,
                              24 * char_height);
}


void
worldgui_determine_supported_chars(WorldGUI *gui)
{
  PangoContext         *context;
  PangoFontDescription *fontdesc;
  PangoFont            *font;
  PangoCoverage        *coverage;

  if (gui->ta.sup_geom_shapes != -1) {
    return;     /* There are cached values */
  }

  context = gtk_widget_get_pango_context(GTK_WIDGET(gui->txtView));
  fontdesc = pango_font_description_from_string(gui->world->terminalfont);
  font = pango_context_load_font(context, fontdesc);
  coverage = pango_font_get_coverage(font,
                                     pango_context_get_language(context));

  /* U+25C6 BLACK DIAMOND */
  gui->ta.sup_geom_shapes   = pango_coverage_get(coverage, 0x25C6);
  /* U+2592 MEDIUM SHADE */
  gui->ta.sup_block         = pango_coverage_get(coverage, 0x2592);
  /* U+2409 SYMBOL FOR HORIZONTAL TABULATION */
  gui->ta.sup_control       = pango_coverage_get(coverage, 0x2409);
  /* U+00B0 DEGREE SIGN */
  gui->ta.sup_l1_supplement = pango_coverage_get(coverage, 0x00B0);
  /* U+2518 BOX DRAWINGS LIGHT UP AND LEFT */
  gui->ta.sup_box_drawing   = pango_coverage_get(coverage, 0x2518);
  /* U+23BA HORIZONTAL SCAN LINE-1 */
  gui->ta.sup_misc_tech     = pango_coverage_get(coverage, 0x23BA);
  /* U+2264 LESS-THAN OR EQUAL TO */
  gui->ta.sup_math          = pango_coverage_get(coverage, 0x2264);
  /* U+03C0 GREEK SMALL LETTER PI */
  gui->ta.sup_greek         = pango_coverage_get(coverage, 0x03C0);

  pango_font_description_free(fontdesc);
  g_object_unref(coverage);
}


void
free_world_gui(WorldGUI *gui)
{
  g_object_unref(gui->css_provider);

  if (gui->prune_timeout_id) {
    g_source_remove(gui->prune_timeout_id);
  }

  /* The tags themselves are supposed to be destroyed when the TextBuffer
     is destroyed, which happens when the widget is destroyed. */
  g_free(gui->ta.ansi_fore_tags);
  g_free(gui->ta.ansi_back_tags);
  g_hash_table_destroy(gui->ta.rgb_fore_tags);
  g_hash_table_destroy(gui->ta.rgb_back_tags);

  if (gui->line_times) {
    k_circular_queue_free(gui->line_times);
  }

  g_free(gui);
}


static
void
combo_changed_cb(SimoComboBox *combo, gpointer data)
{
  GtkTreeIter  current;
  World       *world;
  gchar       *command;

  world = ((WorldGUI *) data)->world;

  if (simo_combo_box_get_active_iter(combo, &current)) {
    world->current_cmd = current;
    command = cmdhistory_get_command(world->cmd_list, &current);
    simo_combo_box_set_text(combo, command);
    world->cmd_just_selected_from_combo = TRUE;
    g_free(command);
  }
}


static
void
combo_poped_cb(SimoComboBox *combo, gpointer data)
{
  World *world = ((WorldGUI *) data)->world;

  gtk_list_store_set(GTK_LIST_STORE(world->cmd_list), &world->current_cmd,
                     CMDLIST_COL_TENTATIVE,
                     simo_combo_box_get_text(combo),
                     -1);
  world->combo_is_poped = TRUE;
}


static
void
combo_closed_cb(SimoComboBox *combo, gpointer data)
{
  World *world = ((WorldGUI *) data)->world;

  world->combo_is_poped = FALSE;
}


static
void
n_lines_changed_cb(SimoComboBox *combo, gpointer data)
{
  World *world = ((WorldGUI *) data)->world;

  world->input_n_lines = simo_combo_box_get_n_lines(combo);
  set_input_line_controls(world, NULL, NULL, NULL);
}


static
void
activate_cb(SimoComboBox *cmb, gpointer data)
{
  World *world = ((WorldGUI *) data)->world;

  if (world) {
    const gchar *str;

    str = simo_combo_box_get_text(cmb);
    parse_commands(world, str);

    /* Ignore empty commands and passwords */
    if (strcmp(str, "") != 0 && !world->noecho) {
      add_recent_command(world, str);
    }
  }
  if (world->noecho || !world->repeat_commands)
    simo_combo_box_set_text(cmb, "");
  else
    simo_combo_box_select_region(cmb, 0, -1);

  if (world->itime_reset_activate) {
    world->last_command_time = time(NULL);
  }
  world->cmd_position_changed = FALSE;
}


static
gboolean
keypress_cb(GtkWidget   *widget,
            GdkEventKey *evt,
            gpointer     data)
{
  WorldGUI      *gui = (WorldGUI *) data;
  World         *world = gui->world;
  GtkAdjustment *adjustment;
  int            direction = 1;
  gdouble        newval;

  if (world->print_next_keycode) {
    if (gtk_accelerator_valid(evt->keyval, evt->state)) {
      world->keycode_to_print = gtk_accelerator_name(evt->keyval, evt->state);
      ansitextview_append_stringf(world->gui,
                                  "Key code: %s\n",
                                  world->keycode_to_print);


      world->print_next_keycode = FALSE;
      gtk_main_quit();
    }

    return TRUE;
  }

  switch (evt->keyval) {
  case GDK_KEY_Left:
  case GDK_KEY_KP_Left:
    if (evt->state & GDK_MOD1_MASK) { /* Alt+left */
      menu_perl_run_cb(NULL, "$world->prev");
      return TRUE;
    }
    break;

  case GDK_KEY_Right:
  case GDK_KEY_KP_Right:
    if (evt->state & GDK_MOD1_MASK) { /* Alt+right */
      menu_perl_run_cb(NULL, "$world->next");
      return TRUE;
    }
    break;

  case GDK_KEY_Tab:
  case GDK_KEY_ISO_Left_Tab:
    if (evt->state & GDK_CONTROL_MASK) {
      if (evt->state & GDK_SHIFT_MASK) { /* Ctrl+Shift+Tab */
        menu_perl_run_cb(NULL, "$world->prev");
      } else {                           /* Ctrl+Tab */
        menu_perl_run_cb(NULL, "$world->next");
      }
      return TRUE;
    }
    break;

  case '1': case '2': case '3': case '4': case '5':
  case '6': case '7': case '8': case '9':
    if (evt->state & GDK_MOD1_MASK) { /* Alt+digit */
      guint newpage = evt->keyval - '1';

      if (newpage < g_list_length(open_worlds)) {
        set_focused_world(newpage);
      }
      return TRUE;
    }
    break;

  case GDK_KEY_Up:
  case GDK_KEY_KP_Up:
    direction = 0;
    /* Fall through */

  case GDK_KEY_Down:
  case GDK_KEY_KP_Down:
    /* Do nothing if option to ignore these keys is active. */
    if (world->ignore_up_down_keys) {
      return FALSE;
    }
    /* Do nothing if arrows have been used to move around the multi-line
       input bar */
    if (world->input_n_lines > 1 && world->cmd_position_changed) {
      return FALSE;
    }
    prev_or_next_command(world, direction);
    return TRUE;

  case GDK_KEY_Page_Up:
  case GDK_KEY_KP_Page_Up:
    direction = -1;
    /* Fall through */

  case GDK_KEY_Page_Down:
  case GDK_KEY_KP_Page_Down:
    /* If the scrollback window is visible, move that */
    if (gtk_paned_get_position(gui->split_pane) > 0) {
      adjustment = gtk_scrolled_window_get_vadjustment(gui->scrwinScroll);
    } else {
      adjustment = gtk_scrolled_window_get_vadjustment(gui->scrolled_win);
    }
    newval = gtk_adjustment_get_value(adjustment)
      + direction*gtk_adjustment_get_page_increment(adjustment)/2;
    if (newval
        > (gtk_adjustment_get_upper(adjustment) - gtk_adjustment_get_page_size(adjustment))) {
      newval = gtk_adjustment_get_upper(adjustment) - gtk_adjustment_get_page_size(adjustment);
    }
    gtk_adjustment_set_value(adjustment, newval);
    return TRUE;

  case GDK_KEY_End:
  case GDK_KEY_KP_End:
    if (evt->state & GDK_CONTROL_MASK) {
      adjustment = gtk_scrolled_window_get_vadjustment(gui->scrolled_win);
      gtk_adjustment_set_value(adjustment,
                               gtk_adjustment_get_upper(adjustment) - gtk_adjustment_get_page_size(adjustment));
      return TRUE;
    }
    break;
  }

  return check_macros(world, evt->keyval, evt->state);
}


static
void
text_changed_cb(SimoComboBox *cmb, gpointer data)
{
  World *world = ((WorldGUI *) data)->world;
  const gchar *command;

  if (world->gui->execute_changed_signal && !world->combo_is_poped) {
    if (!world->cmd_just_selected_from_combo) {
      command = simo_combo_box_get_text(cmb);
      insert_recent_command(world, command);
      /* Set the pointer to point to the new command */
      gtk_tree_model_get_iter_first(world->cmd_list, &world->current_cmd);
    } else {
      insert_recent_command(world, "");
    }

    world->gui->execute_changed_signal = FALSE;
    world->cmd_has_been_edited = TRUE;
    world->cmd_just_selected_from_combo = FALSE;
    if (world->saved_command_find_search) {
      g_free(world->saved_command_find_search);
      world->saved_command_find_search = NULL;
    }
  }
}


static
void
cursor_moved_cb(GtkTextBuffer *buffer,
                GtkTextMark   *mark,
                gpointer       data)
{
  WorldGUI    *gui = (WorldGUI *) data;
  const gchar *name;

  name = gtk_text_mark_get_name(mark);
  if (!name || strcmp(name, "insert") != 0) {
    return;
  }

  if (gui->world) {
    gui->world->cmd_position_changed = TRUE;
  }
}


void
completion_cell_data_function(GtkCellLayout   *cell_layout,
                              GtkCellRenderer *renderer,
                              GtkTreeModel    *model,
                              GtkTreeIter     *iter,
                              gpointer         user_data)
{
  /* Used in the combo box and completion display. If there is a tentative
     command, displays it, otherwise displays the normal text. */
  gchar *command;

  command = cmdhistory_get_command(model, iter);
  g_object_set(renderer, "text", command, NULL);
  g_free(command);
}


static
gboolean
completion_function(GtkEntryCompletion *completion,
                    const gchar        *key,
                    GtkTreeIter        *iter,
                    gpointer            user_data)
{
  GtkTreeModel *model;
  gchar        *command;
  gchar        *normalized_command;
  gchar        *case_normalized_command;
  gboolean      ret = FALSE;

  model = (GtkTreeModel *) user_data;

  command = cmdhistory_get_command(model, iter);

  if (!command) {
    return FALSE;
  }

  normalized_command      = g_utf8_normalize(command, -1, G_NORMALIZE_ALL);
  case_normalized_command = g_utf8_casefold(normalized_command, -1);

  if (strncmp(key, case_normalized_command, strlen(key)) == 0) {
    ret = TRUE;
  }

  g_free(case_normalized_command);
  g_free(normalized_command);
  g_free(command);

  return ret;
}


static
gboolean
completion_match_selected_cb(GtkEntryCompletion *widget,
                             GtkTreeModel       *model,
                             GtkTreeIter        *iter,
                             gpointer            user_data)
{
  World *world = (World *) user_data;
  gchar *command;

  command = cmdhistory_get_command(model, iter);
  simo_combo_box_set_text(world->gui->cmbEntry, command);
  simo_combo_box_set_position(world->gui->cmbEntry, -1);
  g_free(command);

  return TRUE;
}


static
void
tab_close_cb(GtkWidget *widget, gpointer data)
{
  WorldGUI *gui   = (WorldGUI *) data;
  World    *world = gui->world;

  if (world->connected) {
    GtkWidget *msgdlg;
    gint       response;

    msgdlg = gtk_message_dialog_new(GTK_WINDOW(wndMain),
                                    GTK_DIALOG_MODAL,
                                    GTK_MESSAGE_QUESTION,
                                    GTK_BUTTONS_NONE,
                                    _("Are you sure you want to close this world?"));
    gtk_window_set_title(GTK_WINDOW(msgdlg), _("Really close?"));
    gtk_dialog_add_buttons(GTK_DIALOG(msgdlg),
                           _("Keep _open"), GTK_RESPONSE_NO,
                           _("_Close"), GTK_RESPONSE_YES,
                           NULL);

    gtk_widget_show_all(msgdlg);
    response = gtk_dialog_run(GTK_DIALOG(msgdlg));
    gtk_widget_destroy(msgdlg);

    if (response != GTK_RESPONSE_YES) {
      return;
    }
  }

  remove_world(world, TRUE);
}



void
clear_button_cb(GtkButton *button, gpointer data)
{
  gchar    *last_path;
  WorldGUI *gui = (WorldGUI *) data;
  World    *world = gui->world;

  if (world) {
    simo_combo_box_clear_text(gui->cmbEntry);

    /* So that up key recalls the previous command */
    last_path = g_strdup_printf("%d", world->cmd_list_size - 1);
    gtk_tree_model_get_iter_from_string(world->cmd_list,
                                        &world->current_cmd,
                                        last_path);
  } else {
    gtk_entry_set_text(gui->txtEntry, "");
  }

  gtk_widget_grab_focus(GTK_WIDGET(gui->cmbEntry));
}


void
configure_gui(WorldGUI *gui, World *world)
{
  GtkEntryCompletion *completion;
  GtkCellRenderer    *renderer;

  configure_css(gui, world);

  gtk_text_view_set_wrap_mode(gui->txtView,
                              world->wrap
                                ? GTK_WRAP_WORD_CHAR
                                : GTK_WRAP_CHAR);
  gtk_text_view_set_indent(gui->txtView, -world->wrap_indent);
  gtk_text_view_set_wrap_mode(gui->txtViewScroll,
                              world->wrap
                                ? GTK_WRAP_WORD_CHAR
                                : GTK_WRAP_CHAR);
  gtk_text_view_set_indent(gui->txtViewScroll, -world->wrap_indent);

  /* If this GUI is for a MUD, and not for a KCWin */
  if (gui->world) {
    simo_combo_box_set_model(gui->cmbEntry, world->cmd_list);
    completion = simo_combo_box_get_completion(gui->cmbEntry);
    if (world->autocompletion) {
      gtk_entry_completion_set_minimum_key_length(completion,
                                                  world->autocompl_minprefix);
    } else {
      gtk_entry_completion_set_minimum_key_length(completion,
                                                  9999);
    }
    /* Configure rendererer for completion */
    renderer = gtk_cell_renderer_text_new();
    simo_combo_box_set_completion_renderer(gui->cmbEntry, renderer);
    simo_combo_box_set_completion_cell_func(gui->cmbEntry,
                                            renderer,
                                            completion_cell_data_function,
                                            NULL,
                                            NULL);
    gtk_entry_completion_set_match_func(completion,
                                        completion_function,
                                        world->cmd_list,
                                        NULL);
    /* Correct behaviour when a completion is selected */
    g_signal_connect(G_OBJECT(completion), "match-selected",
                     G_CALLBACK(completion_match_selected_cb), world);

    /* Set up structure to hold time of each line's arrival */
    gui->line_times = k_circular_queue_new(sizeof(time_t),
                                           world->buffer_lines);

    /* Size of input box */
    simo_combo_box_set_n_lines(gui->cmbEntry, world->input_n_lines);

    /* Spell checking. */
    worldgui_configure_spell(gui);

    /* Tooltips */
    gtk_widget_set_has_tooltip(GTK_WIDGET(gui->txtView),
                               gui->world->use_tooltips);
    gtk_widget_set_has_tooltip(GTK_WIDGET(gui->txtViewScroll),
                               gui->world->use_tooltips);
  }
}


void
configure_css(WorldGUI *gui, World *world)
{
  gchar *terminalfont;
  gchar *entryfont;
  gchar *statusfont;

  terminalfont = pango_font_to_css(world->terminalfont);
  entryfont    = pango_font_to_css(world->entryfont);
  statusfont   = pango_font_to_css(world->statusfont);

  gchar *css = g_strdup_printf(
    ".mud-output text {"
    "  background-color: %s;"
    "  color: %s;"
    "}"
    ".mud-output {"
    "  %s;"
    "}"

    ".mud-output text selection {"
    "  background-color: #94c9ed;"
    "  color: #000000;"
    "}"

    ".input {"
    "  %s;"
    "}"

    ".status-bar {"
    "  %s;"
    "}",
    gdk_rgba_to_string(world->defback),
    gdk_rgba_to_string(world->deffore),
    terminalfont,
    entryfont,
    statusfont);

  gtk_css_provider_load_from_data(gui->css_provider, css, -1, NULL);

  g_free(terminalfont);
  g_free(entryfont);
  g_free(statusfont);
  g_free(css);
}


void
worldgui_configure_spell(WorldGUI *gui)
{
  GError *error = NULL;

  simo_combo_box_set_spell(gui->cmbEntry,
                           gui->world->spell, gui->world->spell_language,
                           &error);
  if (error) {
    ansitextview_append_stringf(gui,_("Error setting spell checker: %s\n"), error->message);
    g_error_free(error);
  }
}


static
gboolean
textview_button_press_cb(GtkWidget      *text_view,
                         GdkEventButton *event,
                         gpointer        data)
{
  WorldGUI       *gui;
  GtkTextIter     iter;
  gint            x, y;

  gui = (WorldGUI *) data;

  if (event->button != GDK_BUTTON_PRIMARY
      && event->button != GDK_BUTTON_SECONDARY) {
    return FALSE;
  }

  gtk_text_view_window_to_buffer_coords(GTK_TEXT_VIEW(text_view),
                                        GTK_TEXT_WINDOW_WIDGET,
                                        event->x, event->y, &x, &y);

  gtk_text_view_get_iter_at_location(GTK_TEXT_VIEW(text_view), &iter, x, y);

  return act_if_link(gui, &iter, text_view, event);
}


static
gboolean
act_if_link(WorldGUI *gui, GtkTextIter *iter,
            GtkWidget *widget, GdkEventButton *event)
{
  GtkTextIter  start;
  GtkTextIter  end;
  gchar       *matched_url;

  if (!gtk_text_iter_has_tag(iter, gui->txttag_url)) {
    return FALSE;
  }

  start = end = *iter;
  if (!gtk_text_iter_backward_to_tag_toggle(&start, gui->txttag_url)) {
    /* Not found must mean we are at the tag boundary */
    start = *iter;
  }
  if (!gtk_text_iter_forward_to_tag_toggle(&end, gui->txttag_url)) {
    /* Not found must mean we are at the tag boundary */
    end = *iter;
  }
  matched_url = gtk_text_buffer_get_text(gui->txtBuffer, &start, &end, FALSE);

  if (event->button == GDK_BUTTON_PRIMARY) {
    menu_url_open(NULL, matched_url);
  } else {
    GtkBuilder *ui_builder;
    GError     *error = NULL;
    GtkMenu    *mnuPopupURL;

    ui_builder = gtk_builder_new();
    if (!gtk_builder_add_from_resource(ui_builder,
                                       "/ekalin/kildclient/mnuPopupURL.ui",
                                       &error)) {
      g_warning(_("Error loading UI from XML file: %s"), error->message);
      g_error_free(error);
      return TRUE;
    }

    mnuPopupURL = GTK_MENU(gtk_builder_get_object(ui_builder, "mnuPopupURL"));
    gtk_widget_show_all(GTK_WIDGET(mnuPopupURL));
    gtk_builder_connect_signals(ui_builder, matched_url);

    gtk_menu_attach_to_widget(mnuPopupURL, widget, NULL);
    gtk_menu_popup_at_pointer(mnuPopupURL, (GdkEvent *) event);
    g_object_unref(ui_builder);
  }

  return TRUE;
}


#ifndef __MINGW32__
void
menu_url_open(GtkMenuItem *menu, char *url)
{
  GError *err = NULL;

  gtk_show_uri_on_window(GTK_WINDOW(wndMain), url, GDK_CURRENT_TIME, &err);
  if (err != NULL) {
    fprintf(stderr, _("Error opening URL: %s\n"),
            err->message);
    g_error_free(err);
  }

  g_free(url);
}
#else /* defined __MINGW32__ */
void
menu_url_open(GtkMenuItem *menu, char *url)
{
  ShellExecute(NULL, "open", url, NULL, NULL, SW_SHOWNORMAL);

  g_free(url);
}
#endif


void
menu_popup_url_copy(GtkMenuItem *menu, char *url)
{
  GdkDisplay   *display;
  GtkClipboard *clipboard;

  display = gtk_widget_get_display(GTK_WIDGET(menu));

  clipboard = gtk_clipboard_get_for_display(display, GDK_SELECTION_PRIMARY);
  gtk_clipboard_set_text(clipboard, url, -1);

  clipboard = gtk_clipboard_get_for_display(display, GDK_SELECTION_CLIPBOARD);
  gtk_clipboard_set_text(clipboard, url, -1);

  g_free(url);
}


static
gboolean
textview_motion_notify_event(GtkWidget      *widget,
                             GdkEventMotion *event,
                             gpointer        data)
{
  WorldGUI    *gui = (WorldGUI *) data;
  GtkTextView *text_view = GTK_TEXT_VIEW(widget);
  gint         x, y;

  /*
   * Update the cursor image if the pointer moved.
   */
  gtk_text_view_window_to_buffer_coords(text_view,
                                        GTK_TEXT_WINDOW_WIDGET,
                                        event->x, event->y, &x, &y);

  set_cursor_if_appropriate(text_view, gui, x, y);

  return FALSE;
}


static
void
set_cursor_if_appropriate(GtkTextView *text_view,
                          WorldGUI    *gui,
                          gint         x,
                          gint         y)
{
  GtkTextIter    iter;
  gboolean       hovering = FALSE;

  gtk_text_view_get_iter_at_location(text_view, &iter, x, y);
  hovering = gtk_text_iter_has_tag(&iter, gui->txttag_url);

  if (hovering != hovering_over_link) {
    hovering_over_link = hovering;

    if (hovering_over_link) {
      gdk_window_set_cursor(gtk_text_view_get_window(text_view,
                                                     GTK_TEXT_WINDOW_TEXT),
                            hand_cursor);
    } else {
      gdk_window_set_cursor(gtk_text_view_get_window(text_view,
                                                     GTK_TEXT_WINDOW_TEXT),
                            regular_cursor);
    }
  }
}


static
gboolean
worldgui_query_tooltip_cb(GtkWidget  *widget,
                          gint        win_x,
                          gint        win_y,
                          gboolean    keyboard_mode,
                          GtkTooltip *tooltip,
                          gpointer    data)
{
  WorldGUI      *gui       = (WorldGUI *) data;
  GtkTextView   *text_view = GTK_TEXT_VIEW(widget);
  GtkTextIter    iter;
  gint           tooltip_line_top_y;
  gint           x, y;
  GtkAllocation  alloc;
  GdkRectangle   rect;
  gchar         *text;

  /* No world yet? */
  if (!gui->world) {
    return FALSE;
  }

  if (keyboard_mode) {
    return FALSE;
  }

  /* Determine are for which the tooltip is valid - this corresponds
     to the area of the buffer line. */
  gtk_text_view_window_to_buffer_coords(text_view,
                                        GTK_TEXT_WINDOW_WIDGET,
                                        win_x, win_y, &x, &y);
  gtk_text_view_get_line_at_y(text_view, &iter, y, NULL);
  gtk_text_view_get_line_yrange(text_view, &iter,
                                &tooltip_line_top_y,
                                &rect.height);
  gtk_text_view_buffer_to_window_coords(text_view,
                                        GTK_TEXT_WINDOW_WIDGET,
                                        0, tooltip_line_top_y,
                                        &rect.x, &rect.y);
  gtk_widget_get_allocation(widget, &alloc);
  rect.width = alloc.width;

  gtk_tooltip_set_tip_area(tooltip, &rect);

  /* Get the tooltip text */
  text = worldgui_get_tooltip_text(gui, text_view, x, y);
  if (text == NULL) {
    return FALSE;
  }

  gtk_tooltip_set_text(tooltip, text);
  g_free(text);

  return TRUE;
}


static
gchar *
worldgui_get_tooltip_text(WorldGUI    *gui,
                          GtkTextView *text_view,
                          gint         x,
                          gint         y)
{
  GtkTextIter  iter;
  gchar       *tip;

  gtk_text_view_get_iter_at_location(text_view, &iter, x, y);
  if (gtk_text_iter_has_tag(&iter, gui->txttag_url)) {
    tip = g_strdup(_("Click to open link; right-click for more options."));
  } else {
    gint   tooltip_line_number;
    size_t len;
#ifdef HAVE_LOCALTIME_R
    struct tm time_tm;
#endif
    struct tm *time_tmp;

    if (k_circular_queue_size(gui->line_times) == 0) {
      return NULL;
    }

    tip = g_malloc(MAX_BUFFER * sizeof(gchar));

    tooltip_line_number = gtk_text_iter_get_line(&iter);

    g_snprintf(tip, MAX_BUFFER,
               "Line %ld, ",
               tooltip_line_number + gui->world->deleted_lines + 1);
    len = strlen(tip);

#ifdef HAVE_LOCALTIME_R
    localtime_r(&k_circular_queue_nth(gui->line_times,
                                      time_t,
                                      tooltip_line_number),
                &time_tm);
    time_tmp = &time_tm;
#else
    time_tmp = localtime(&k_circular_queue_nth(gui->line_times,
                                               time_t,
                                               tooltip_line_number));
#endif

    strftime(tip + len, MAX_BUFFER - len, "%c", time_tmp);
  }

  return tip;
}


static
void
textview_size_allocate_cb(GtkWidget     *widget,
                          GtkAllocation *allocation,
                          gpointer       data)
{
  WorldGUI     *gui   = (WorldGUI *) data;
  World        *world = gui->world;
  GdkRectangle  rect;

  gtk_text_view_get_visible_rect(gui->txtView, &rect);

  /* Some checks to prevent send_naws_size() from being called when
     world is invalid. */
  if (world && world->gui && world->gui == gui
      && world->connected && world->use_naws == TRUE) {
    /* See if the size has changed. */
    if (rect.height != world->last_naws_size.height
        || rect.width != world->last_naws_size.width) {
      send_naws_size(world);
      world->last_naws_size = rect;
    }
  }

  /* Dealing with the wrap column:
     If the user shrinks the window so that the view is too narrow to
     show wrap_column columns, then we wrap normally. If it's wider,
     we want to keep the wrap at wrap_column, and show blank white
     space for the rest. To do thath, we pad the view's right-margin
     with whatever space is left over. */
  if (world) {
    if (!world->use_wrap_column || world->wrap_column < 40) {
      /* No wrap column set */
      gtk_text_view_set_right_margin(gui->txtView, 0);
      gtk_text_view_set_right_margin(gui->txtViewScroll, 0);
    } else {
      gint char_width, char_height;
      gint wrap_width;
      gint margin;

      ansitextview_get_char_size(gui, &char_height, &char_width);
      wrap_width = char_width * (world->wrap_column + 1);
      margin = rect.width > wrap_width ? (rect.width - wrap_width) : 0;
      gtk_text_view_set_right_margin(gui->txtView, margin);
      gtk_text_view_set_right_margin(gui->txtViewScroll, margin);
    }
  }
}


static
void
find_incremental_cb(GtkEditable *widget, gpointer data)
{
  WorldGUI *gui = (WorldGUI *) data;

  do_find(gui);
}


void
find_next_cb(GtkButton *button, gpointer data)
{
  WorldGUI    *gui = (WorldGUI *) data;
  GtkTextIter  search_start;

  gtk_text_buffer_get_iter_at_mark(gui->txtBuffer,
                                   &search_start,
                                   gui->txtmark_next_search_start);
  gtk_text_buffer_move_mark(gui->txtBuffer,
                            gui->txtmark_search_start,
                            &search_start);
  do_find(gui);
}


static
void
do_find(WorldGUI *gui)
{
  const gchar *text;
  GtkTextIter  search_start;
  GtkTextIter  match_start;
  GtkTextIter  match_end;

  gtk_label_set_text(gui->lblSearchInfo, NULL);

  text = gtk_entry_get_text(GTK_ENTRY(gui->txtSearchTerm));
  if (strcmp(text, "") == 0) {
    /* Reset search when the text is blanked. Either if the user clears
       everything, or when the Find menu is used. */
    gtk_text_buffer_get_start_iter(gui->txtBuffer, &search_start);
    gtk_text_buffer_move_mark(gui->txtBuffer,
                              gui->txtmark_search_start,
                              &search_start);
    gtk_text_buffer_select_range(gui->txtBuffer, &search_start, &search_start);
    return;
  }

  gtk_text_buffer_get_iter_at_mark(gui->txtBuffer,
                                   &search_start,
                                   gui->txtmark_search_start);

  if (gtk_text_iter_forward_search(&search_start,
                                   text,
                                   0,
                                   &match_start,
                                   &match_end,
                                   NULL)) {
    gtk_text_buffer_select_range(gui->txtBuffer, &match_start, &match_end);
    /* If split pane is active, scroll that plane */
    if (gtk_paned_get_position(gui->split_pane) > 0) {
      gtk_text_view_scroll_to_iter(gui->txtViewScroll, &match_start,
                                   0, FALSE, 0, 0);
    } else {
      gtk_text_view_scroll_to_iter(gui->txtView, &match_start,
                                   0, FALSE, 0, 0);
    }

    /* Mark point at which Find Next will start */
    gtk_text_iter_forward_char(&match_start);
    gtk_text_buffer_move_mark(gui->txtBuffer,
                              gui->txtmark_next_search_start,
                              &match_start);
  } else {
    gtk_label_set_text(gui->lblSearchInfo, _("Not found"));
  }
}


static
void
close_search_box_cb(GtkButton *widget, gpointer data)
{
  WorldGUI *gui = (WorldGUI *) data;

  g_object_set(G_OBJECT(gui->search_box), "no-show-all", TRUE, NULL);
  gtk_widget_hide(GTK_WIDGET(gui->search_box));
  gtk_widget_grab_focus(GTK_WIDGET(gui->cmbEntry));
}


static
gboolean
search_box_keypress_cb(GtkWidget   *widget,
                       GdkEventKey *evt,
                       gpointer     data)
{
  if (evt->keyval == GDK_KEY_Escape) {
    close_search_box_cb(NULL, data);
    return TRUE;
  }

  return FALSE;
}


static
void
scrolled_win_size_allocate_cb(GtkWidget     *widget,
                              GtkAllocation *allocation,
                              gpointer       data)
{
  /* When the bottom TextView (the main one) is resized, we want its
     top part to shrink/grow and the bottom part to remain displaying
     what it was displaying before.

     To do that, the value of (upper - (value + page_size)) of the
     ScrolledWindow's Adjustment must remain the same. This value
     corresponds to the size of the part of the TextView that is not
     displayed and is below the displayed part.

     upper never changes while scrolling, so we only need to make sure
     that (value + page_size) remains constant. To do that, so we
     store this value at the end of the function so that in the next
     call the previous value is know so that the new position of value
     is calculated.

     Whenever value changes, this stored value is also updated.
  */
  WorldGUI      *gui = (WorldGUI *) data;
  GtkAdjustment *verticalAdjust;
  double         page_size, upper;
  double         newValue;

  verticalAdjust
    = gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(widget));
  upper = gtk_adjustment_get_upper(verticalAdjust);
  page_size = gtk_adjustment_get_page_size(verticalAdjust);

  /* First call */
  //if (gui->last_scroll_info == -1) {
  //  gui->last_scroll_info = page_size;
  //  return;
  //}

  newValue = gui->last_scroll_info - page_size;
  if (newValue < 0) {
    newValue = 0;
  }
  if (newValue > (upper - page_size)) {
    newValue = upper - page_size;
  }

  gtk_adjustment_set_value(verticalAdjust, newValue);

  gui->last_scroll_info = newValue + page_size;
}


static
void
scrolled_win_value_changed_cb(GtkAdjustment *adjustment,
                              gpointer       data)
{
  WorldGUI *gui = (WorldGUI *) data;

  gui->last_scroll_info = gtk_adjustment_get_value(adjustment)
                          + gtk_adjustment_get_page_size(adjustment);
}


static
gboolean
set_split_to_zero(gpointer data)
{
  WorldGUI *gui = (WorldGUI *) data;

  gtk_paned_set_position(gui->split_pane, 0);
  return FALSE;
}
