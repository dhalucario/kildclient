/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include <ctype.h>
#include "libintl-wrapper.h"
#include <locale.h>
#include <gtk/gtk.h>

#include "kildclient.h"
#include "perlscript.h"


/***********************************************
 * Function prototypes - Test trigger function *
 ***********************************************/
static GtkWidget *create_test_triggers_dialog(GtkWindow *parent, World *world);
/* XML UI signals */
G_MODULE_EXPORT void test_triggers_cb(GtkMenuItem *widget, gpointer data);
G_MODULE_EXPORT void test_trigger_send_cb(GtkWidget *widget, gpointer data);



Trigger *
new_trigger(void)
{
  Trigger *trigger;

  trigger = g_new0(Trigger, 1);

  trigger->high_fg_color  = -1;
  trigger->high_bg_color  = -1;
  trigger->high_italic    = -1;
  trigger->high_strike    = -1;
  trigger->high_underline = -1;

  return trigger;
}


void
remove_trigger(World *world, GSList *triggeritem)
{
  Trigger *trigger = (Trigger *) triggeritem->data;

  /* Necessary when free_trigger dereferences trigger->pattern_re */
  if (world->perl_interpreter) {
    GRAB_PERL(world);
  }

  we_trigger_delete_trigger(world, trigger);

  world->triggers = g_slist_remove_link(world->triggers, triggeritem);
  if (!trigger->owner_plugin) {
    --world->trigger_pos;
  }
  free_trigger(trigger, NULL);
  g_slist_free(triggeritem);

  if (world->perl_interpreter) {
    RELEASE_PERL(world);
  }
}


gboolean
move_trigger(World *world, gint old_pos, gint new_pos)
{
  GSList   *triggeritem;
  gpointer  trigger;

  triggeritem = g_slist_nth(world->triggers, old_pos);
  if (!triggeritem) {
    return FALSE;
  }
  trigger = triggeritem->data;

  world->triggers = g_slist_delete_link(world->triggers, triggeritem);
  world->triggers = g_slist_insert(world->triggers, trigger, new_pos);

  we_trigger_delete_trigger(world, trigger);
  we_trigger_insert_trigger(world, trigger, new_pos);

  return TRUE;
}


void
free_trigger(Trigger *trigger, gpointer data)
{
  g_free(trigger->name);
  g_free(trigger->pattern);
  g_free(trigger->action);
  SvREFCNT_dec(trigger->pattern_re);

  g_free(trigger);
}


void
list_triggers(World *world, Plugin *plugin)
{
  guint    i;
  guint    total_width;
  guint    field_width;
  GSList  *trigptr;
  Trigger *trigger;

  /* The rows argument is not used. */
  ansitextview_get_size(world->gui, &field_width, &total_width);

  /* If the screen is really narrow, we can do nothing. */
  if (total_width < 49) {
    total_width = 49;
  }
  field_width = (total_width - 34)/2;

  ansitextview_append_stringf(world->gui,
                              _("Num Gag GLo Ena KeE ReW IgC Sty %-*.*s %-*.*s\n"),
                              field_width, field_width, _("Pattern"),
                              field_width, field_width, _("Action"));
  ansitextview_append_string(world->gui, "--- --- --- --- --- --- --- --- ");
  for (i = 0; i < field_width; ++i) {
    ansitextview_append_string(world->gui, "-");
  }
  ansitextview_append_string(world->gui, " ");
  for (i = 0; i < field_width; ++i) {
    ansitextview_append_string(world->gui, "-");
  }
  ansitextview_append_string(world->gui, "\n");

  i = 0;
  trigptr = world->triggers;
  while (trigptr) {
    trigger = (Trigger *) trigptr->data;

    if (trigger->owner_plugin == plugin) {
      ansitextview_append_stringf(world->gui,
                                  "%3d %-3.3s %-3.3s %-3.3s %-3.3s %-3.3s %-3.3s %-3.3s %-*.*s %-*.*s\n",
                                  i,
                                  trigger->gag_output    ? _("y") : _("n"),
                                  trigger->gag_log       ? _("y") : _("n"),
                                  trigger->enabled       ? _("y") : _("n"),
                                  trigger->keepexecuting ? _("y") : _("n"),
                                  trigger->rewriter      ? _("y") : _("n"),
                                  trigger->ignore_case   ? _("y") : _("n"),
                                  trigger->highlight     ? _("y") : _("n"),
                                  field_width, field_width,
                                  trigger->pattern,
                                  field_width, field_width,
                                  trigger->action ? trigger->action : "");
    }
    ++i;
    trigptr = trigptr->next;
  }
}


void
trigger_precompute_res(World *world)
{
  GSList  *triggerptr = world->triggers;
  Trigger *trigger;

  GRAB_PERL(world);

  while (triggerptr) {
    trigger = (Trigger *) triggerptr->data;

    /* Plugins loaded from a script file already have the precomputed RE */
    if (!trigger->pattern_re) {
      trigger->pattern_re = precompute_re(trigger->pattern,
                                          trigger->ignore_case);
    }

    triggerptr = triggerptr->next;
  }

  /* Precompute a special pattern to substitute $1, $2, etc in trigger
     actions. */
  if (!world->trigger_action_re) {
    world->trigger_action_re = precompute_re("\\$([0-9]+)", FALSE);
  }

  RELEASE_PERL(world);
}


void
save_trigger(GString *str, Trigger *trigger)
{
  g_string_append(str, "    <trigger ");
  if (trigger->name) {
    kc_g_string_append_escaped(str, "name=\"%s\" ", trigger->name);
  }
  g_string_append_printf(str,
                         "enabled=\"%d\" keepexecuting=\"%d\" rewriter=\"%d\" ignorecase=\"%d\"%s%s>\n",
                         trigger->enabled,
                         trigger->keepexecuting,
                         trigger->rewriter,
                         trigger->ignore_case,
                         trigger->gag_output ? " gag=\"1\"" : "",
                         trigger->gag_log    ? " gaglog=\"1\"": "");
  kc_g_string_append_escaped(str,
                             "      <pattern>%s</pattern>\n", trigger->pattern);
  if (trigger->action) {
    kc_g_string_append_escaped(str,
                               "      <action>%s</action>\n", trigger->action);
  }
  g_string_append_printf(str, "      <highlight enabled=\"%d\" target=\"%d\" fg=\"%d\" bg=\"%d\" italics=\"%d\" strike=\"%d\" underline=\"%d\"/>\n",
                         trigger->highlight, trigger->high_target,
                         trigger->high_fg_color, trigger->high_bg_color,
                         trigger->high_italic, trigger->high_strike, trigger->high_underline);
  g_string_append(str, "    </trigger>\n");
}


/*
 * Test trigger function.
 */
void
test_triggers_cb(GtkMenuItem *widget, gpointer data)
{
  if (currentWorld) {
    open_test_triggers_dialog(currentWorld);
  }
}


void
open_test_triggers_dialog(World *world)
{
  if (!world->dlgTestTriggers) {
    world->dlgTestTriggers =
      create_test_triggers_dialog(GTK_WINDOW(wndMain), world);
  }

  gtk_widget_show_all(world->dlgTestTriggers);
  gtk_window_present(GTK_WINDOW(world->dlgTestTriggers));
}


static
GtkWidget *
create_test_triggers_dialog(GtkWindow *parent, World *world)
{
  GError           *error = NULL;
  GObject          *dlg;

  /* Create the dialog */
  if (!world->ui_builder) {
    world->ui_builder = gtk_builder_new();
  }
  if (!gtk_builder_add_from_resource(world->ui_builder,
                                     "/ekalin/kildclient/dlgTestTriggers.ui",
                                     &error)) {
    g_warning(_("Error loading UI from XML file: %s"), error->message);
    g_error_free(error);
    return NULL; /* This will cause warnings in the calling function, but
                    there is need to abort the program because of this. */
  }
  dlg = gtk_builder_get_object(world->ui_builder, "dlgTestTriggers");
  gtk_window_set_transient_for(GTK_WINDOW(dlg), GTK_WINDOW(wndMain));

  /* Signals */
  gtk_builder_connect_signals(world->ui_builder, world);

  return GTK_WIDGET(dlg);
}


void
test_trigger_send_cb(GtkWidget *widget, gpointer data)
{
  World         *world = (World *) data;
  GtkEntry      *txtLine;
  GtkLabel      *lblMatches;
  GtkTextView   *txtCommands;
  GtkEntry      *txtOutput;
  GtkLabel      *lblGagLog;
  GtkTextBuffer *bufCommands;
  gboolean       disable_triggers;
  gchar         *line;
  gchar         *stripped;
  gint           n_matches;
  GSList        *trigger_response = NULL;
  gsize          output_len;
  gboolean       gag_output = FALSE;
  gboolean       gag_log = FALSE;
  gchar         *n_matches_str;
  GSList        *cmdptr;
  GtkTextIter    iter;

  txtLine     = GTK_ENTRY(gtk_builder_get_object(world->ui_builder,
                                                 "txtLine"));
  lblMatches  = GTK_LABEL(gtk_builder_get_object(world->ui_builder,
                                                 "lblMatches"));
  txtOutput   = GTK_ENTRY(gtk_builder_get_object(world->ui_builder,
                                                 "txtOutput"));
  txtCommands = GTK_TEXT_VIEW(gtk_builder_get_object(world->ui_builder,
                                                     "txtCommands"));
  lblGagLog   = GTK_LABEL(gtk_builder_get_object(world->ui_builder,
                                                 "lblGagLog"));
  bufCommands = gtk_text_view_get_buffer(txtCommands);

  line     = g_strdup(gtk_entry_get_text(txtLine));
  stripped = strip_ansi(line, strlen(line));

  /* We need the triggers to be tried. */
  disable_triggers = world->disable_triggers;
  world->disable_triggers = FALSE;

  match_triggers(world, &stripped, &line,
                 &output_len, &gag_output, &gag_log,
                 &n_matches, &trigger_response);

  world->disable_triggers = disable_triggers;

  /* Show output */
  n_matches_str = g_strdup_printf("%d", n_matches);
  gtk_label_set_text(lblMatches, n_matches_str);
  g_free(n_matches_str);

  gtk_text_buffer_set_text(bufCommands, "", 0);
  gtk_text_buffer_get_end_iter(bufCommands, &iter);
  cmdptr = trigger_response;
  while (cmdptr) {
    gtk_text_buffer_insert(bufCommands, &iter, (gchar *) cmdptr->data, -1);
    gtk_text_buffer_insert(bufCommands, &iter, "\n", 1);
    cmdptr = cmdptr->next;
  }
  g_slist_foreach(trigger_response, (GFunc) g_free, NULL);
  g_slist_free(trigger_response);

  if (!gag_output) {
    gtk_entry_set_text(txtOutput, (gchar *) line);
  } else {
    gtk_entry_set_text(txtOutput, "");
  }

  gtk_label_set_text(lblGagLog, gag_log ? _("No") : _("Yes"));

  g_free(line);
  g_free(stripped);
}
