/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include <ctype.h>
#include <locale.h>
#include "libintl-wrapper.h"
#include <gtk/gtk.h>

/* Perl includes */
#include <EXTERN.h>
#include <perl.h>
#include <XSUB.h>

#include "kildclient.h"
#include "perlscript.h"


/***********************
 * Function prototypes *
 ***********************/
static int  find_next_command_separator(World *world, const char *cmdline);
static void process_aliases(World      *world,
                            const char *cmdline,
                            int         start,
                            int         end);
static void process_command(World *world, const char *cmd, int len);



void
parse_commands(World *world, const char *cmdline)
{
  parse_commands_full(world, cmdline, NULL, NULL, FALSE);
}


void
parse_commands_with_matches_and_notify(World      *world,
                                       const char *cmdline,
                                       AV         *matches)
{
  parse_commands_full(world, cmdline, matches, NULL, TRUE);
}


void
parse_commands_full(World       *world,
                    const gchar *cmdline,
                    AV          *matches,
                    const gchar *colorline,
                    gboolean     notify)
{
  MainThreadOperation *operation = g_new0(MainThreadOperation, 1);
  operation->action              = PARSE_COMMANDS;
  operation->commands.commands   = g_strdup(cmdline);
  operation->commands.matches    = matches;
  operation->commands.colorline  = g_strdup(colorline);
  operation->commands.notify     = notify;

  queue_mainthread_operation(world, operation);
}


void
mainthread_op_parse_commands(World *world, MainThreadOperation *operation)
{
  gboolean needs_perl = operation->commands.colorline
                        || operation->commands.matches;

  if (needs_perl) {
    GRAB_PERL(world);
  }

  if (operation->commands.colorline) {
    SV *Pcolorline;

    Pcolorline = get_sv("colorline", TRUE);
    SvUTF8_on(Pcolorline);
    sv_setpv(Pcolorline, operation->commands.colorline);
  }

  if (operation->commands.matches) {
    perl_set_arg_array(operation->commands.matches);
  }

  parse_command_line_immediately(world, operation->commands.commands);

  if (needs_perl) {
    RELEASE_PERL(world);
  }

  /* Notify waiting match trigger thread */
  if (operation->commands.notify) {
    g_mutex_lock(&world->wait_command_mutex);
    world->command_executed = TRUE;
    g_cond_signal(&world->wait_command_cond);
    g_mutex_unlock(&world->wait_command_mutex);
  }
}


void
parse_command_line_immediately(World *world, const char *cmdline)
{
  int      totallen;
  int      seppos;
  int      start;
  int      end;
  gboolean quit;
  int      cmdseplen;

  totallen = strlen(cmdline);
  cmdseplen = strlen(world->command_separator);

  start = 0;
  quit = FALSE;
  do {
    seppos = find_next_command_separator(world, cmdline + start);
    if (seppos == -1) {
      end = totallen;
      quit = TRUE;
    } else {
      seppos += start;
      end = seppos;
      while (isspace(cmdline[end-1]))
        --end;
    }

    process_aliases(world, cmdline, start, end);

    start = seppos + cmdseplen;
    while (isspace(cmdline[start]))
      ++start;
  } while (!quit);
}


static
int
find_next_command_separator(World *world, const char *cmdline)
{
  int  pos            = 0;
  int  len            = strlen(cmdline);
  char quote          = 0;
  int  cmdseplen      = strlen(world->command_separator);
  gboolean use_quotes = FALSE;

  if (cmdline[0] == world->perl_character
      && len > 1 && cmdline[1] != world->perl_character) {
    use_quotes = TRUE;
  }

  while (pos < len) {
    if (use_quotes && (cmdline[pos] == '\"' || cmdline[pos] == '\'')) {
      if (quote) {
        if (cmdline[pos] == quote && cmdline[pos-1] != '\\') {
          quote = 0;
        }
      } else {
        if (pos == 0 || cmdline[pos-1] != '\\') {
          quote = cmdline[pos];
        }
      }

    } else if (cmdline[pos] == world->command_separator[0]) {
      if (!quote && pos != (len - (cmdseplen-1))
          && strncmp(cmdline + pos, world->command_separator,
                     cmdseplen) == 0) {
        return pos;
      }
    }

    ++pos;
  }

  return -1;
}


static
void
process_aliases(World *world, const char *cmdline, int start, int end)
{
  char  cmd[MAX_BUFFER + 1];
  int   len = end - start;
  char *result;

  if (len >= MAX_BUFFER) {
    ansitextview_append_string_nl(world->gui,
                                  _("Warning: Trying to send a very long command line. Perhaps a recursive alias definition?"));
    process_command(world, cmdline + start, len);
    return;
  }

  memcpy(cmd, cmdline + start, len);
  cmd[len] = '\0';

  result = substitute_aliases(world, cmd);
  if (result) {
    parse_command_line_immediately(world, result);
    g_free(result);
  } else {
    process_command(world, cmd, len);
  }
}


static
void
process_command(World *world, const char *cmd, int len)
{
  if (*cmd != world->perl_character) {
    send_to_world(world, cmd, len);
  } else {
    if (*(cmd + 1) == world->perl_character) {
      send_to_world(world, cmd + 1, len - 1);
    } else {
      SV *commands;
      SV *error;

      GRAB_PERL(world);
      commands = newSVpv(cmd + 1, len - 1);
      SvUTF8_on(commands);
      eval_sv(commands, TRUE | G_EVAL);
      error = get_sv("@", TRUE);
      if (SvTRUE(error)) {
        ansitextview_append_stringf(world->gui,
                                    "Perl Error: %s", SvPV_nolen(error));
      }
      RELEASE_PERL(world);
    }
  }
}
