/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include "kildclient.h"
#include "perlscript.h"


/*************************
 * File global variables *
 *************************/


/***********************
 * Function prototypes *
 ***********************/
static void     do_process_line_operation(World                *world,
                                          ProcessLineOperation *operation);
static void     process_recv_complete_line(World                *world,
                                           ProcessLineOperation *operation);
static void     process_recv_incomplete_line(World                *world,
                                             ProcessLineOperation *operation);
static void     match_rewriter_trigger(World    *world,
                                       Trigger  *trigger,
                                       gchar   **line,
                                       gchar   **colorline,
                                       gsize    *output_len,
                                       SV       *Pcolorline,
                                       gint     *n_matches,
                                       gint      trigger_number);
static gboolean verify_gag_trigger(Trigger      *trigger,
                                   const gchar  *line,
                                   gboolean     *gag_output,
                                   gboolean     *gag_log,
                                   GSList      **highlights);
static gboolean match_normal_trigger(World        *world,
                                     Trigger      *trigger,
                                     const gchar  *colorline,
                                     const gchar  *line,
                                     gint         *n_matches,
                                     gint          trigger_number,
                                     GSList      **trigger_response);
static void     output_line(World       *world,
                            gboolean     gag_output,
                            gboolean     gag_log,
                            const gchar *colorline,
                            const gchar *line,
                            gsize        len,
                            GSList      *highlights);



gpointer
process_line_thread(gpointer data)
{
  World *world = (World *) data;

  g_async_queue_ref(world->process_line_operations);
  g_async_queue_ref(world->ansiparse_operations);
  g_async_queue_ref(world->mainthread_operations);

  while (1) {
    ProcessLineOperation *operation
      = (ProcessLineOperation *) g_async_queue_pop(world->process_line_operations);

    if (world->to_be_destructed) {
      g_async_queue_unref(world->process_line_operations);
      g_async_queue_unref(world->ansiparse_operations);
      g_async_queue_unref(world->mainthread_operations);

      free_process_line_operation(operation);
      return NULL;
    }

    do_process_line_operation(world, operation);
    free_process_line_operation(operation);
  }
}


void
queue_process_line_operation(World *world, ProcessLineOperation *operation)
{
  g_async_queue_push(world->process_line_operations, operation);
}


void
free_process_line_operation(gpointer data)
{
  ProcessLineOperation *operation = (ProcessLineOperation *) data;

  if (operation->action != DUMMY_PROCESS_LINE) {
    g_free(operation->line);
  }

  g_free(operation);
}


static
void
do_process_line_operation(World *world, ProcessLineOperation *operation)
{
  switch (operation->action) {
  case PROCESS_COMPLETE_LINE:
    process_recv_complete_line(world, operation);
    break;

  case PROCESS_INCOMPLETE_LINE:
    process_recv_incomplete_line(world, operation);
    break;

  case DUMMY_PROCESS_LINE:
    /* Do nothing */
    break;
  }
}


static
void
process_recv_complete_line(World  *world, ProcessLineOperation *operation)
{
  gchar              *stripped;
  gint                dummy;
  gboolean            gag_output = FALSE;
  gboolean            gag_log    = FALSE;
  AnsiParseOperation *ansi_operation;

  stripped = strip_ansi(operation->line, operation->len);

  match_triggers(world,
                 &stripped, &operation->line, &operation->len,
                 &gag_output, &gag_log,
                 &dummy, NULL);

  g_free(stripped);

  /* The line is printed and written to the log file in match_triggers. */

  if (!gag_output) {
    MainThreadOperation *mainthread_op = g_new(MainThreadOperation, 1);

    mainthread_op->action = EXECUTE_ON_RECEIVED_TEXT;
    mainthread_op->hook_execution = g_new(HookExecution, 1);
    mainthread_op->hook_execution->world = world;
    mainthread_op->hook_execution->data = g_strdup(operation->line);
    queue_mainthread_operation(world, mainthread_op);
  }

  /* Final actions after line is parsed. */
  ansi_operation = g_new(AnsiParseOperation, 1);
  ansi_operation->action = FINALIZE_LINE;
  queue_ansiparse_operation(world, ansi_operation);
}


static
void
process_recv_incomplete_line(World *world, ProcessLineOperation *operation)
{
  AnsiParseOperation *ansi_operation = g_new(AnsiParseOperation, 1);
  ansi_operation->action = APPEND_LINE;
  ansi_operation->line.line = g_strdup(operation->line);
  ansi_operation->line.line_len = operation->len;
  queue_ansiparse_operation(world, ansi_operation);

  /* Final actions after line is parsed. */
  ansi_operation = g_new(AnsiParseOperation, 1);
  ansi_operation->action = FINALIZE_LINE;
  queue_ansiparse_operation(world, ansi_operation);
}


void
match_triggers(World     *world,
               gchar    **line,
               gchar    **colorline,
               gsize     *output_len,
               gboolean  *gag_output,
               gboolean  *gag_log,
               gint      *n_matches,
               GSList   **trigger_response)
{
  /* This function matches triggers against the line.

     If trigger_response is NULL, then it executes triggers, and also
     writes the line to the screen and/or log file if appropriate.

     If trigger_response points to a GSList, then this function does
     not do anything really, but stores the commands that would be
     executed in the list, for use in the Test Triggers function. */
  GSList      *trigptr;
  Trigger     *trigger;
  SV          *Pcolorline;
  int          i;
  GSList      *highlights = NULL;

  *n_matches  = 0;

  if (world->disable_triggers) {
    if (!trigger_response) {
      AnsiParseOperation *operation = g_new(AnsiParseOperation, 1);
      operation->action = APPEND_LINE;
      operation->line.line = g_strdup(*colorline);
      operation->line.line_len = *output_len;
      queue_ansiparse_operation(world, operation);

      write_log_line(world, *line, NULL);
    }

    return;
  }

  GRAB_PERL(world);
  /*
   * First pass: rewriter triggers
   */
  Pcolorline = get_sv("colorline", TRUE);
  SvUTF8_on(Pcolorline);
  trigptr = world->triggers;
  i = 0;
  while (trigptr) {
    trigger = (Trigger *) trigptr->data;

    if (trigger->rewriter) {
      match_rewriter_trigger(world, trigger,
                             line, colorline, output_len, Pcolorline,
                             n_matches, i);
    }

    trigptr = trigptr->next;
    ++i;
  }

  /*
   * Second pass: see if one of the triggers is a gag
   */
  trigptr = world->triggers;
  while (trigptr) {
    trigger = (Trigger *) trigptr->data;

    if (!trigger->rewriter) {
      if (verify_gag_trigger(trigger, *line,
                             gag_output, gag_log,
                             &highlights)
          && !trigger->keepexecuting) {
        break;
      }
    }

    trigptr = trigptr->next;
  }

  if (!trigger_response) {
    output_line(world, *gag_output, *gag_log,
                *colorline, *line, *output_len, highlights);
  }

  /*
   * Third pass: run normal triggers
   */
  world_for_perl = world;
  trigptr = world->triggers;
  i = 0;
  while (trigptr) {
    trigger = (Trigger *) trigptr->data;

    if (!trigger->rewriter) {
      if (match_normal_trigger(world, trigger,
                               *colorline, *line,
                               n_matches, i,
                               trigger_response)
          && !trigger->keepexecuting) {
        break;
      }
    }

    trigptr = trigptr->next;
    ++i;
  }
  world_for_perl = currentWorld;

  RELEASE_PERL(world);
}


static
void
match_rewriter_trigger(World    *world,
                       Trigger  *trigger,
                       gchar   **line,
                       gchar   **colorline,
                       gsize    *output_len,
                       SV       *Pcolorline,
                       gint     *n_matches,
                       gint      trigger_number)
{
  AV    *matches;
  gchar *tmp;

  if (!trigger->enabled
      || !perl_match(*line,
                     trigger->pattern_re,
                     NULL, NULL, 0, &matches)) {
    return;
  }

  ++(*n_matches);

  if (debug_matches) {
    fprintf(stderr,
            "**Trigger %d '%s' (rewriter) matched for world %s\n"
            "  Line: %s"
            "  Pattern: %s\n"
            "  Action: %s\n",
            trigger_number,
            trigger->name ? trigger->name : "",
            world->name,
            *line,
            trigger->pattern,
            trigger->action);
  }

  if (trigger->action) {
    parse_commands_full(world, trigger->action, matches, *colorline, TRUE);
  }

  /* We need to wait for the command to finish so we can get the
     new line. */
  RELEASE_PERL(world);
  g_mutex_lock(&world->wait_command_mutex);
  while (!world->command_executed) {
    g_cond_wait(&world->wait_command_cond, &world->wait_command_mutex);
  }

  g_free(*colorline);
  tmp = SvPV(Pcolorline, *output_len);
  *colorline = g_strdup(tmp);
  g_free(*line);
  *line = strip_ansi(*colorline, *output_len);

  world->command_executed = FALSE;
  g_mutex_unlock(&world->wait_command_mutex);
  GRAB_PERL(world);
}


static
gboolean
verify_gag_trigger(Trigger      *trigger,
                   const gchar  *line,
                   gboolean     *gag_output,
                   gboolean     *gag_log,
                   GSList      **highlights)
{
  gint match_start;
  gint match_end;

  if (!trigger->enabled
      || !perl_match(line,
                     trigger->pattern_re,
                     &match_start, &match_end,
                     trigger->high_target,
                     NULL)) {
    return FALSE;
  }

  *gag_output |= trigger->gag_output;
  *gag_log    |= trigger->gag_log;

  if (*gag_output && *gag_log) {
    return TRUE;
  }

  if (trigger->highlight) {
    HighlightApplication *highlight = g_new(HighlightApplication, 1);
    highlight->trigger     = trigger;
    highlight->match_start = match_start;
    highlight->match_end   = match_end;

    *highlights = g_slist_append(*highlights, highlight);
  }

  return TRUE;
}


static
gboolean
match_normal_trigger(World        *world,
                     Trigger      *trigger,
                     const gchar  *colorline,
                     const gchar  *line,
                     gint         *n_matches,
                     gint          trigger_number,
                     GSList      **trigger_response)
{
  AV *matches;

  if (!trigger->enabled
      || !perl_match(line,
                     trigger->pattern_re,
                     NULL, NULL, 0, &matches)) {
    return FALSE;
  }

  ++(*n_matches);

  if (debug_matches) {
    fprintf(stderr,
            "**Trigger %d '%s' matched for world %s\n"
            "  Line: %s"
            "  Pattern: %s\n"
            "  Action: %s\n",
            trigger_number,
            trigger->name ? trigger->name : "",
            world->name,
            line,
            trigger->pattern,
            trigger->action ? trigger->action : "");
  }

  if (trigger->action) {
    /* Substitute $1, $2, etc. for matched parts. */
    gchar *action = perl_substitute(trigger->action,
                                    world->trigger_action_re,
                                    "$__M[$1]",
                                    FALSE,
                                    TRUE);
    if (!trigger_response) {
      if (action) {
        parse_commands_full(world, action, matches, colorline, FALSE);
        g_free(action);
      } else {
        parse_commands_full(world, trigger->action,
                            matches, colorline, FALSE);
      }
    } else {
      if (action) {
        *trigger_response = g_slist_append(*trigger_response, action);
      } else {
        *trigger_response = g_slist_append(*trigger_response,
                                           g_strdup(trigger->action));
      }
    }
  }

  return TRUE;
}


static
void
output_line(World       *world,
            gboolean     gag_output,
            gboolean     gag_log,
            const gchar *colorline,
            const gchar *line,
            gsize        len,
            GSList      *highlights)
{
  if (!gag_output) {
    AnsiParseOperation *operation = g_new(AnsiParseOperation, 1);
    operation->action = APPEND_LINE;
    operation->line.line = g_strdup(colorline);
    operation->line.line_len = len;
    queue_ansiparse_operation(world, operation);

    operation = g_new(AnsiParseOperation, 1);
    operation->action = APPLY_HIGHLIGHTS_AND_MOVE_MARK;
    operation->highlights = highlights;
    queue_ansiparse_operation(world, operation);
  }

  if (!gag_log) {
    write_log_line(world, line, NULL);
  }
}
