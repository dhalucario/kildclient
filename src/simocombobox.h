/*
 * SimoComboBox is meant to be an alternative combobox to the
 * standard GtkComboBox.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Author:
 * Todd A. Fisher (toddf@simosoftware.com)
 *
 */

#ifndef __SIMO_COMBO_BOX_H
#define __SIMO_COMBO_BOX_H

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define SIMO_TYPE_COMBO_BOX             (simo_combo_box_get_type())
#define SIMO_COMBO_BOX(obj)             (G_TYPE_CHECK_INSTANCE_CAST((obj),\
                                         SIMO_TYPE_COMBO_BOX, SimoComboBox))
#define SIMO_IS_COMBO_BOX(obj)          (G_TYPE_CHECK_INSTANCE_TYPE((obj),\
                                         SIMO_TYPE_COMBO_BOX))
#define SIMO_COMBO_BOX_CLASS(vtable)    (G_TYPE_CHECK_CLASS_CAST((vtable), \
                                         SIMO_TYPE_COMBO_BOX,\
                                         SimoComboBoxClass))
#define SIMO_IS_COMBO_BOX_CLASS(vtable) (G_TYPE_CHECK_CLASS_TYPE((vtable),\
                                         SIMO_TYPE_COMBO_BOX))
#define SIMO_COMBO_BOX_GET_CLASS(inst)  (G_TYPE_INSTANCE_GET_CLASS((inst),\
                                         SIMO_TYPE_COMBO_BOX,\
                                         SimoComboBoxClass))

typedef struct _SimoComboBox        SimoComboBox;
typedef struct _SimoComboBoxClass   SimoComboBoxClass;
typedef struct _SimoComboBoxPrivate SimoComboBoxPrivate;

struct _SimoComboBox
{
  GtkBox parent_instance;

  /*< private >*/
  SimoComboBoxPrivate *priv;
};

struct _SimoComboBoxClass
{
  GtkBoxClass parent_class;

  /* signals */
  void (* clicked)         (SimoComboBox *combo_box);
  void (* popup_displayed) (SimoComboBox *combo_box);
  void (* popup_closed)    (SimoComboBox *combo_box);
  void (* changed)         (SimoComboBox *combo_box);
  void (* activate)        (SimoComboBox *combo_box);
  void (* size_changed)    (SimoComboBox *combo_box);
  void (* mark_set)        (SimoComboBox *combo_box,
                            GtkTextMark  *mark);
};


/* construction */
GType               simo_combo_box_get_type(void);
GtkWidget          *simo_combo_box_new(void);

/* NOTE: if you construct your own model you can only use insert_seperator if
   column index 0 is GDK_TYPE_PIXBUF and index 1 is of G_TYPE_INT
   when index 1 is 1 the seperator is displayed and the text is hidden
   when index 1 is 0 the seperator is hidden and the text is displayed */
GtkWidget          *simo_combo_box_new_with_model(GtkTreeModel *model);

GtkTreeModel       *simo_combo_box_get_model(SimoComboBox *combo_box);
void                simo_combo_box_set_model(SimoComboBox *combo_box,
                                             GtkTreeModel *model);
void                simo_combo_box_set_combo_column(SimoComboBox      *combo_box,
                                                    GtkTreeViewColumn *column);

const gchar        *simo_combo_box_get_text(SimoComboBox *combo_box);
void                simo_combo_box_set_text(SimoComboBox *combo_box,
                                            const gchar  *text);
void                simo_combo_box_clear_text(SimoComboBox *combo_box);
void                simo_combo_box_set_position(SimoComboBox *combo_box,
                                                gint          position);
gboolean            simo_combo_box_get_selection_bounds(SimoComboBox *combo_box,
                                                        gint         *start,
                                                        gint         *end);
void                simo_combo_box_select_region(SimoComboBox *combo_box,
                                                 gint          start,
                                                 gint          end);
void                simo_combo_box_delete_selection(SimoComboBox *combo_box);
void                simo_combo_box_cut_clipboard(SimoComboBox *combo_box);
void                simo_combo_box_copy_clipboard(SimoComboBox *combo_box);
void                simo_combo_box_paste_clipboard(SimoComboBox *combo_box);
void                simo_combo_box_insert_seperator(SimoComboBox *combo_box,
                                                    GtkTreeIter  *iter);

gboolean            simo_combo_box_get_active_iter(SimoComboBox *combo_box,
                                                   GtkTreeIter  *iter);
void                simo_combo_box_set_active_iter(SimoComboBox *combo_box,
                                                   GtkTreeIter  *iter);

gboolean            simo_combo_box_get_n_lines(SimoComboBox *combo_box);
void                simo_combo_box_set_n_lines(SimoComboBox *combo_box,
                                               gint          n_lines);

void                simo_combo_box_set_visibility(SimoComboBox *combo_box,
                                                  gboolean      visible);

GtkWidget          *simo_combo_box_get_single_line_widget(SimoComboBox *combo);
GtkWidget          *simo_combo_box_get_multi_line_widget(SimoComboBox *combo);

/* Spell checking */
gboolean            simo_combo_box_set_spell(SimoComboBox  *combo_box,
                                             gboolean       use_spell,
                                             gchar         *language,
                                             GError       **error);

/* For completion: Either set_completion_text_column to get an
   automatic renderer, or set_completion_renderer to create your own. */
GtkEntryCompletion *simo_combo_box_get_completion(SimoComboBox *combo_box);
void                simo_combo_box_set_completion_text_column(SimoComboBox *combo_box,
                                                              int           col_id);
void                simo_combo_box_set_completion_renderer(SimoComboBox    *combo_box,
                                                           GtkCellRenderer *renderer);
void                simo_combo_box_set_completion_cell_func(SimoComboBox          *combo_box,
                                                            GtkCellRenderer       *renderer,
                                                            GtkCellLayoutDataFunc  func,
                                                            gpointer               user_data,
                                                            GDestroyNotify         destroy);

/* programmatic control */
void                 simo_combo_box_popup(SimoComboBox *combo_box);

G_END_DECLS

#endif /* __SIMO_COMBO_BOX_H__ */
