/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include "libintl-wrapper.h"
#include <gtk/gtk.h>
#include <gmodule.h>

#include "simocombobox.h"

#include "kildclient.h"
#include "perlscript.h"


/***********************
 * Function prototypes *
 ***********************/
/* XML UI signals */
G_MODULE_EXPORT void we_log_start_cb(GtkWidget *widget, gpointer data);
G_MODULE_EXPORT void we_log_stop_cb(GtkWidget *widget, gpointer data);
G_MODULE_EXPORT void select_logfile_cb(gpointer data, GtkWidget *widget);
G_MODULE_EXPORT void revert_logformat_cb(gpointer data, GtkWidget *widget);



GObject *
get_we_logging_tab(GtkBuilder *ui_builder, World *world)
{
  GObject *panel;

  panel = gtk_builder_get_object(ui_builder, "we_panel_logging");

  if (world == default_world) {
    /* These parts make no sense for the default world, so let's hide them */
    GObject *component;

    component = gtk_builder_get_object(ui_builder, "label_log_status");
    g_object_set(component, "no-show-all", TRUE, NULL);
    component = gtk_builder_get_object(ui_builder, "vbox_int_log_status");
    g_object_set(component, "no-show-all", TRUE, NULL);
  }

  return panel;
}


void
fill_we_logging_tab(World *world, GtkBuilder *ui_builder)
{
  GtkLabel        *lblLogStatus;
  GtkWidget       *btnLogStart;
  GtkWidget       *btnLogStop;
  GObject         *txtLogFile;
  GtkToggleButton *chkLogAuto;
  GtkToggleButton *chkLogAddTime;
  GtkEntry        *txtLogTimeFormat;

  if (!world) {
    return;
  }

  if (!ui_builder) {
    /* Called from Perl's XS_logfile. In this case there is no ui_builder,
       and the World Editor might not have been created yet. */
    if (!world->dlgEditWorld) {
      return;
    }

    ui_builder = world->ui_builder;
  }

  lblLogStatus     = GTK_LABEL(gtk_builder_get_object(ui_builder,
                                                      "lblLogStatus"));
  btnLogStart      = GTK_WIDGET(gtk_builder_get_object(ui_builder,
                                                       "btnLogStart"));
  btnLogStop       = GTK_WIDGET(gtk_builder_get_object(ui_builder,
                                                       "btnLogStop"));
  txtLogFile       = gtk_builder_get_object(ui_builder, "txtLogFile");
  chkLogAuto       = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder,
                                                              "chkLogAuto"));
  chkLogAddTime    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder,
                                                              "chkLogAddTime"));
  txtLogTimeFormat = GTK_ENTRY(gtk_builder_get_object(ui_builder,
                                                      "txtLogTimeFormat"));

  if (world->connected) {
    if (world->log_file) {
      gchar *msg;

      msg = g_strdup_printf(_("Logging to file %s."), world->log_actual_file);
      gtk_label_set_text(lblLogStatus, msg);
      g_free(msg);
      gtk_widget_set_sensitive(btnLogStart, FALSE);
      gtk_widget_set_sensitive(btnLogStop, TRUE);
    } else {
      gtk_label_set_text(lblLogStatus, _("Logging is disabled."));
      gtk_widget_set_sensitive(btnLogStart, TRUE);
      gtk_widget_set_sensitive(btnLogStop, FALSE);
    }
  } else {
    gtk_label_set_text(lblLogStatus, _("Not connected."));
    gtk_widget_set_sensitive(btnLogStart, FALSE);
    gtk_widget_set_sensitive(btnLogStop, FALSE);
  }

  if (world->log_file_name) {
    gtk_entry_set_text(GTK_ENTRY(txtLogFile), world->log_file_name);
  } else {
    gtk_entry_set_text(GTK_ENTRY(txtLogFile), "");
  }

  gtk_toggle_button_set_active(chkLogAuto, world->log_autostart);
  gtk_toggle_button_set_active(chkLogAddTime, world->log_add_time);
  if (world->log_timeformat) {
    gtk_entry_set_text(txtLogTimeFormat, world->log_timeformat);
  } else {
    gtk_entry_set_text(txtLogTimeFormat, "");
  }

  /* Store pointer to dlgEditWorld so it can be used as a parent
     in the select_logfile_cb callback */
  g_object_set_data(txtLogFile, "dlgEditWorld", world->dlgEditWorld);
}


void
update_we_logging_parameters(World *world, GtkBuilder *ui_builder)
{
  GtkEntry        *txtLogFile;
  GtkToggleButton *chkLogAuto;
  GtkToggleButton *chkLogAddTime;
  GtkEntry        *txtLogTimeFormat;
  const gchar     *filename;
  const gchar     *format;

  txtLogFile       = GTK_ENTRY(gtk_builder_get_object(ui_builder, "txtLogFile"));
  chkLogAuto       = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder,
                                                            "chkLogAuto"));
  chkLogAddTime    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder,
                                                            "chkLogAddTime"));
  txtLogTimeFormat = GTK_ENTRY(gtk_builder_get_object(ui_builder,
                                                    "txtLogTimeFormat"));

  g_free(world->log_file_name);
  filename = gtk_entry_get_text(txtLogFile);
  if (filename && *filename) {
    world->log_file_name = g_strdup(filename);
  } else {
    world->log_file_name = NULL;
  }

  world->log_autostart = gtk_toggle_button_get_active(chkLogAuto);
  world->log_add_time  = gtk_toggle_button_get_active(chkLogAddTime);

  format = gtk_entry_get_text(txtLogTimeFormat);
  if (!world->log_timeformat || strcmp(world->log_timeformat, format) != 0) {
    g_free(world->log_timeformat);
    world->log_timeformat = g_strdup(format);
  }
}


void
we_log_start_cb(GtkWidget *widget, gpointer data)
{
  World           *world = (World *) data;
  gint             lines = 0;
  gchar           *errmsg = NULL;
  GtkToggleButton *chkLogIncludePrevious;
  GtkSpinButton   *spnLogPreviousLines;

  /* Copy parameters in the World structure */
  update_we_logging_parameters(world, world->ui_builder);

  /* Has the user asked for inclusion of lines in the window buffer? */
  chkLogIncludePrevious
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(world->ui_builder, "chkLogIncludePrevious"));
  spnLogPreviousLines
    = GTK_SPIN_BUTTON(gtk_builder_get_object(world->ui_builder, "spnLogPreviousLines"));

  if (gtk_toggle_button_get_active(chkLogIncludePrevious)) {
    lines = (int) gtk_spin_button_get_value(spnLogPreviousLines);
  }

  if (start_log(world, lines, &errmsg)) {
    fill_we_logging_tab(world, world->ui_builder);
  } else {
    kc_error_dialog_with_title(GTK_WINDOW(world->dlgEditWorld),
                               _("Logging not started"),
                               "%s", errmsg);
    g_free(errmsg);
  }
}


void
we_log_stop_cb(GtkWidget *widget, gpointer data)
{
  World      *world = (World *) data;

  update_we_logging_parameters(world, world->ui_builder);
  stop_log(world);
  fill_we_logging_tab(world, NULL);
}


void
select_logfile_cb(gpointer data, GtkWidget *widget)
{
  GtkEntry    *txtLogFile = GTK_ENTRY(data);
  GtkWindow   *dlgEditWorld;
  GtkWidget   *filedlg;
  const gchar *filename;

  dlgEditWorld = GTK_WINDOW(g_object_get_data(G_OBJECT(txtLogFile),
                                              "dlgEditWorld"));

  filedlg = gtk_file_chooser_dialog_new(_("Select log file"),
                                        dlgEditWorld,
                                        GTK_FILE_CHOOSER_ACTION_SAVE,
                                        _("_Cancel"),
                                        GTK_RESPONSE_CANCEL,
                                        _("_OK"),
                                        GTK_RESPONSE_OK,
                                        NULL);
  gtk_dialog_set_default_response(GTK_DIALOG(filedlg), GTK_RESPONSE_OK);
  filename = gtk_entry_get_text(txtLogFile);
  if (filename && *filename) {
    gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(filedlg), filename);
  }

  if (gtk_dialog_run(GTK_DIALOG(filedlg)) == GTK_RESPONSE_OK) {
    gchar *new_file;

    new_file = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(filedlg));
    gtk_entry_set_text(txtLogFile, new_file);
    g_free(new_file);
  }

  gtk_widget_destroy(filedlg);
}


void
revert_logformat_cb(gpointer data, GtkWidget *widget)
{
  GtkEntry *txtLogTimeFormat = GTK_ENTRY(data);

  gtk_entry_set_text(txtLogTimeFormat, DEFAULT_LOGTIME_FORMAT);
}
