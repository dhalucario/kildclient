/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include <ctype.h>
#include <sys/stat.h>
#include "libintl-wrapper.h"
#include <gtk/gtk.h>

#include "kildclient.h"
#include "perlscript.h"


/***********************
 * Function prototypes *
 ***********************/
static GtkTreeModel *get_charsets_model(void);
static char         *get_file_name_for_world(const char *name);
/* XML UI signals */
G_MODULE_EXPORT void confirm_delete_changed_cb(GtkToggleButton *chkbox,
                                               gpointer         data);
G_MODULE_EXPORT void show_plugin_toggled_cb(GtkToggleButton *button,
                                            gpointer         data);


/*************************
 * File global variables *
 *************************/
static const gchar *charsets[] = {
    "ISO-8859-1",
    "UTF-8",
    "ANSI_X3.110-1983",
    "ANSI_X3.4-1968",
    "ASMO_449",
    "BIG5",
    "BIG5HKSCS",
    "BS_4730",
    "CP1250",
    "CP1251",
    "CP1252",
    "CP1253",
    "CP1254",
    "CP1255",
    "CP1256",
    "CP1257",
    "CP1258",
    "CP737",
    "CP775",
    "CP949",
    "CSA_Z243.4-1985-1",
    "CSA_Z243.4-1985-2",
    "CSN_369103",
    "CWI",
    "DEC-MCS",
    "DIN_66003",
    "DS_2089",
    "EBCDIC-AT-DE",
    "EBCDIC-AT-DE-A",
    "EBCDIC-CA-FR",
    "EBCDIC-DK-NO",
    "EBCDIC-DK-NO-A",
    "EBCDIC-ES",
    "EBCDIC-ES-A",
    "EBCDIC-ES-S",
    "EBCDIC-FI-SE",
    "EBCDIC-FI-SE-A",
    "EBCDIC-FR",
    "EBCDIC-IS-FRISS",
    "EBCDIC-IT",
    "EBCDIC-PT",
    "EBCDIC-UK",
    "EBCDIC-US",
    "ECMA-CYRILLIC",
    "ES",
    "ES2",
    "EUC-JP",
    "EUC-KR",
    "EUC-TW",
    "GB18030",
    "GB2312",
    "GBK",
    "GB_1988-80",
    "GOST_19768-74",
    "GREEK-CCITT",
    "GREEK7",
    "GREEK7-OLD",
    "HP-ROMAN8",
    "IBM037",
    "IBM038",
    "IBM1004",
    "IBM1026",
    "IBM1047",
    "IBM256",
    "IBM273",
    "IBM274",
    "IBM275",
    "IBM277",
    "IBM278",
    "IBM280",
    "IBM281",
    "IBM284",
    "IBM285",
    "IBM290",
    "IBM297",
    "IBM420",
    "IBM423",
    "IBM424",
    "IBM437",
    "IBM500",
    "IBM850",
    "IBM851",
    "IBM852",
    "IBM855",
    "IBM857",
    "IBM860",
    "IBM861",
    "IBM862",
    "IBM863",
    "IBM864",
    "IBM865",
    "IBM866",
    "IBM868",
    "IBM869",
    "IBM870",
    "IBM871",
    "IBM874",
    "IBM875",
    "IBM880",
    "IBM891",
    "IBM903",
    "IBM904",
    "IBM905",
    "IBM918",
    "IEC_P27-1",
    "INIS",
    "INIS-8",
    "INIS-CYRILLIC",
    "ISIRI-3342",
    "ISO-8859-10",
    "ISO-8859-13",
    "ISO-8859-14",
    "ISO-8859-15",
    "ISO-8859-16",
    "ISO-8859-2",
    "ISO-8859-3",
    "ISO-8859-4",
    "ISO-8859-5",
    "ISO-8859-6",
    "ISO-8859-7",
    "ISO-8859-8",
    "ISO-8859-9",
    "ISO-IR-197",
    "ISO-IR-90",
    "ISO_10367-BOX",
    "ISO_2033-1983",
    "ISO_5427",
    "ISO_5427-EXT",
    "ISO_5428",
    "ISO_6937",
    "IT",
    "JIS_C6220-1969-RO",
    "JIS_C6229-1984-B",
    "JOHAB",
    "JUS_I.B1.002",
    "KOI-8",
    "KOI8-R",
    "KOI8-U",
    "KSC5636",
    "LATIN-GREEK",
    "LATIN-GREEK-1",
    "MAC-IS",
    "MAC-UK",
    "MACINTOSH",
    "MSZ_7795.3",
    "NATS-DANO",
    "NATS-SEFI",
    "NC_NC00-10",
    "NF_Z_62-010",
    "NF_Z_62-010_(1973)",
    "NF_Z_62-010_1973",
    "NS_4551-1",
    "NS_4551-2",
    "PT",
    "PT2",
    "SEN_850200_B",
    "SEN_850200_C",
    "SJIS",
    "T.61-8BIT",
    "TIS-620",
    "VISCII",
    "WIN-SAMI-2",
    NULL,
};



GObject *
get_we_advanced_tab(World *world, GtkBuilder *ui_builder)
{
  GObject         *panel;
  GtkCellRenderer *renderer;
  GtkComboBox     *cmbCharset;

  panel = gtk_builder_get_object(ui_builder, "we_panel_advanced");

  if (world == default_world) {
    // These components don't make sense for the default world
    GtkWidget *widget;

    widget = GTK_WIDGET(gtk_builder_get_object(ui_builder, "label_file"));
    gtk_widget_destroy(widget);
    widget = GTK_WIDGET(gtk_builder_get_object(ui_builder, "vbox_int_file"));
    gtk_widget_destroy(widget);
  }

  cmbCharset = GTK_COMBO_BOX(gtk_builder_get_object(ui_builder, "cmbCharset"));
  gtk_combo_box_set_model(cmbCharset, get_charsets_model());
  renderer = gtk_cell_renderer_text_new();
  gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(cmbCharset), renderer, TRUE);
  gtk_combo_box_set_active(cmbCharset, 0);
  gtk_cell_layout_set_attributes(GTK_CELL_LAYOUT(cmbCharset),
                                 renderer,
                                 "text", 0,
                                 NULL);

  return panel;
}


void
fill_we_advanced_tab(World *world, GtkBuilder *ui_builder)
{
  GtkEntry        *txtWorldFile;
  GtkComboBox     *cmbCharset;
  GtkToggleButton *chkConfirmDelete;
  GtkToggleButton *chkShowPlugin;
  GtkToggleButton *chkKeepAlive;
  GtkToggleButton *chkIgnoreUpDown;
  GtkEntryBuffer  *txtPerlCharacter;

  if (!world) {
    return;
  }

  cmbCharset
    = GTK_COMBO_BOX(gtk_builder_get_object(ui_builder, "cmbCharset"));
  chkConfirmDelete
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "chkConfirmDelete"));
  chkShowPlugin
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "chkShowPlugin"));
  chkKeepAlive
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "chkKeepAlive"));
  chkIgnoreUpDown
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "chkIgnoreUpDown"));
  txtPerlCharacter
    = gtk_entry_get_buffer(GTK_ENTRY(gtk_builder_get_object(ui_builder,
                                                            "txtPerlChar")));

  if (world != default_world) {
    txtWorldFile = GTK_ENTRY(gtk_builder_get_object(ui_builder, "txtWorldFile"));
    if (world->file)
      gtk_entry_set_text(txtWorldFile, world->file);
  }

  if (world->charset_index != -1) {
    gtk_combo_box_set_active(cmbCharset, world->charset_index);
  } else {
    GtkTreeIter   iter;
    GtkTreeModel *model;
    gchar        *charset;

    model = get_charsets_model();
    gtk_tree_model_get_iter_first(model, &iter);
    do {
      gtk_tree_model_get(model, &iter,
                         0, &charset,
                         -1);
      if (strcmp(charset, world->charset) == 0) {
        gtk_combo_box_set_active_iter(cmbCharset, &iter);
        world->charset_index = gtk_combo_box_get_active(cmbCharset);
        break;
      }
    } while (gtk_tree_model_iter_next(model, &iter));
  }

  gtk_toggle_button_set_active(chkConfirmDelete, world->confirm_delete);
  gtk_toggle_button_set_active(chkShowPlugin,    world->show_plugin_items);
  gtk_toggle_button_set_active(chkKeepAlive,     world->keep_alive);
  gtk_toggle_button_set_active(chkIgnoreUpDown,  world->ignore_up_down_keys);

  gtk_entry_buffer_set_text(txtPerlCharacter, &world->perl_character, 1);
}


void
update_we_advanced_parameters(World *world, gboolean *newworld,
                              GtkBuilder *ui_builder)
{
  GtkEntry        *txtWorldFile;
  GtkComboBox     *cmbCharset;
  GtkToggleButton *chkKeepAlive;
  const gchar     *newval;
  gchar           *newfile;
  gchar           *newcharset;
  GtkTreeIter      iter;
  GtkToggleButton *chkIgnoreUpDown;
  GtkEntry        *txtPerlCharacter;

  cmbCharset      = GTK_COMBO_BOX(gtk_builder_get_object(ui_builder,
                                                         "cmbCharset"));
  chkKeepAlive    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder,
                                                             "chkKeepAlive"));
  chkIgnoreUpDown = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder,
                                                             "chkIgnoreUpDown"));
  txtPerlCharacter
    = GTK_ENTRY(gtk_builder_get_object(ui_builder, "txtPerlChar"));

  if (world != default_world) {
    txtWorldFile = GTK_ENTRY(gtk_builder_get_object(ui_builder,
                                                    "txtWorldFile"));
    newval = gtk_entry_get_text(txtWorldFile);
    if (strcmp(newval, "") == 0) {
      newfile = get_file_name_for_world(world->name);
    } else {
      newfile = g_strdup(newval);
    }
    if (!world->file || strcmp(world->file, newval) != 0) {
      g_free(world->file);
      world->file = newfile;
      *newworld = TRUE;
    } else {
      g_free(newfile);
    }
  }

  gtk_combo_box_get_active_iter(cmbCharset, &iter);
  gtk_tree_model_get(get_charsets_model(), &iter,
                     0, &newcharset,
                     -1);
  if (!world->charset || strcmp(world->charset, newcharset) != 0) {
    g_free(world->charset);
    world->charset = newcharset;
  } else {
    g_free(newcharset);
  }
  world->charset_index = gtk_combo_box_get_active(cmbCharset);

  world->keep_alive = gtk_toggle_button_get_active(chkKeepAlive);

  world->ignore_up_down_keys = gtk_toggle_button_get_active(chkIgnoreUpDown);

  world->perl_character = gtk_entry_get_text(txtPerlCharacter)[0];
}


void
confirm_delete_changed_cb(GtkToggleButton *chkbox,
                          gpointer         data)
{
  World *world = (World *) data;

  world->confirm_delete = gtk_toggle_button_get_active(chkbox);
}


void
show_plugin_toggled_cb(GtkToggleButton *button, gpointer data)
{
  World *world = (World *) data;

  world->show_plugin_items = gtk_toggle_button_get_active(button);

  gtk_tree_model_filter_refilter(world->trigger_model_filter);
  gtk_tree_model_filter_refilter(world->alias_model_filter);
  gtk_tree_model_filter_refilter(world->macro_model_filter);
  gtk_tree_model_filter_refilter(world->timer_model_filter);

  gtk_tree_model_filter_refilter(world->hooks.OnConnect_model_filter);
  gtk_tree_model_filter_refilter(world->hooks.OnDisconnect_model_filter);
  gtk_tree_model_filter_refilter(world->hooks.OnReceivedText_model_filter);
  gtk_tree_model_filter_refilter(world->hooks.OnSentCommand_model_filter);
  gtk_tree_model_filter_refilter(world->hooks.OnGetFocus_model_filter);
  gtk_tree_model_filter_refilter(world->hooks.OnLoseFocus_model_filter);
  gtk_tree_model_filter_refilter(world->hooks.OnCloseConnected_model_filter);
  gtk_tree_model_filter_refilter(world->hooks.OnServerData_model_filter);
}


static
GtkTreeModel *
get_charsets_model(void)
{
  static GtkListStore *model = NULL;

  if (!model) {
    GtkTreeIter iter;
    int         i;

    model = gtk_list_store_new(1, G_TYPE_STRING);

    i = 0;
    while (charsets[i]) {
    gtk_list_store_append(model, &iter);
    gtk_list_store_set(model, &iter,
                       0, charsets[i],
                       -1);
    ++i;
    }
  }

  return GTK_TREE_MODEL(model);
}


static
char *
get_file_name_for_world(const char *name)
{
  const char *kilddir;
  char        file[PATH_MAX];
  int         i;
  int         pos;
  int         startpos;
  struct stat stinfo;

  kilddir = get_kildclient_directory_path();
  /* +1 to start after the / that will be added later */
  startpos = strlen(kilddir) + 1;

  i = 0;
  while (1) {
    sprintf(file, "%s/%s_%d.wrl", kilddir, name, i);
    /* Convert the name into something allowed in file names */
    pos = startpos;
    while (file[pos]) {
      if (!isalnum(file[pos]) && file[pos] != '.' && file[pos] != '_')
        file[pos] = '_';
      ++pos;
    }

    if (stat(file, &stinfo) == -1)
      break;
    ++i;
  }

  return (g_strdup(file));
}
