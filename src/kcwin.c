/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include "libintl-wrapper.h"
#include <locale.h>
#include <gtk/gtk.h>

/* Perl includes */
#include <EXTERN.h>
#include <perl.h>
#include <XSUB.h>

#include "kildclient.h"
#include "perlscript.h"


/*****************
 * Useful macros *
 *****************/
#define FETCHGUIVARS SV        *self_ref;   \
                     HV        *self;       \
                     SV       **guiptrsv;   \
                     WorldGUI  *gui
#define FETCHGUI     self_ref = ST(0);                                \
                     self     = (HV*) SvRV(self_ref);                 \
                     guiptrsv = hv_fetch(self, "_GUIPTR", 7, FALSE); \
                     gui      = INT2PTR(WorldGUI *, SvIV(*guiptrsv))


/***********************
 * Function prototypes *
 ***********************/


void
XS_KCWin__new(pTHX_ CV *perlcv)
{
  /* For internal use only. Returns C pointers to a window and some
     widgets, later these are converted to Gtk-Perl objects. */
  GtkWidget *window;
  WorldGUI  *gui;
  dMARK;
  dAX;

  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_default_size(GTK_WINDOW(window), 450, 170);

  gui = world_gui_new(FALSE);
  configure_gui(gui, world_for_perl);
  ansitextview_update_color_tags(gui, world_for_perl);
  gtk_container_add(GTK_CONTAINER(window), gui->vbox);

  XST_mIV(0, PTR2IV(window));
  XST_mIV(1, PTR2IV(gui->vbox));
  XST_mIV(2, PTR2IV(gui->scrolled_win));
  XST_mIV(3, PTR2IV(gui->txtView));
  XST_mIV(4, PTR2IV(gui->txtBuffer));
  XST_mIV(5, PTR2IV(gui->commandArea));
  XST_mIV(6, PTR2IV(gui->btnClear));
  XST_mIV(7, PTR2IV(gui->txtEntry));
  XST_mIV(8, PTR2IV(gui));

  XSRETURN(9);
}


void
XS_KCWin_feed(pTHX_ CV *perlcv)
{
  STRLEN  len;
  gchar  *str;
  FETCHGUIVARS;
  dXSARGS;

  if (items < 2) {
    warn(_("%s: Too few arguments"), "KCWin::feed");
    XSRETURN_EMPTY;
  }

  FETCHGUI;

  str = SvPV(ST(1), len);

  ansitextview_append_ansi_string_immediately(gui, str, len);

  XSRETURN_EMPTY;
}


void
XS_KCWin__destroy(pTHX_ CV *perlcv)
{
  FETCHGUIVARS;
  dMARK;
  dAX;

  FETCHGUI;

  free_world_gui(gui);

  XSRETURN_EMPTY;
}
