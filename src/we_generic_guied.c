/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include "libintl-wrapper.h"
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

#include "kildclient.h"
#include "perlscript.h"


/********************7
 * Type definitions *
 ********************/
typedef void (*UpdateFunc)();


/***********************
 * Function prototypes *
 ***********************/
static void          we_guied_do_export(World       *world,
                                        const gchar *file,
                                        GtkBuilder  *ui_builder);
static void          we_guied_export_selected_hooks(GString      *str,
                                                    GSList       *list,
                                                    const gchar  *name,
                                                    GtkTreeModel *model,
                                                    GtkBuilder   *ui_builder);
static void          we_guied_do_import(World *world, const gchar *filename);
static void          we_guied_try_import(GSList      **oldlist,
                                         GSList       *newlist,
                                         gint         *pos,
                                         UpdateFunc    update_func,
                                         World        *world,
                                         const gchar  *evt_name);
static void          we_guied_try_import_vars(World *world, World *newworld);
static gboolean     *we_guied_get_pointer_to_bool_value(gpointer object,
                                                        gint column);
/* XML UI signals */
G_MODULE_EXPORT void we_guied_import_cb(GtkButton *button, gpointer data);



void
we_guied_export_cb(GtkButton *button, gpointer data)
{
  World           *world = (World *) data;
  GtkBuilder      *ui_builder;
  gchar           *objects[] = { "ExtraFilechooser", NULL };
  GError          *error = NULL;
  GtkWidget       *ExtraFilechooser;
  GtkToggleButton *preselect;
  GtkWidget  *     filechooser;

  ui_builder = gtk_builder_new();
  if (!gtk_builder_add_objects_from_resource(ui_builder,
                                             "/ekalin/kildclient/kildclient.ui",
                                             objects,
                                             &error)) {
    g_warning(_("Error loading UI from XML file: %s"), error->message);
    g_error_free(error);
    return;
  }

  ExtraFilechooser = GTK_WIDGET(gtk_builder_get_object(ui_builder,
                                                       "ExtraFilechooser"));
  preselect
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder,
                                               g_object_get_data(G_OBJECT(button),
                                                                 "preselect")));
  gtk_toggle_button_set_active(preselect, TRUE);

  filechooser = gtk_file_chooser_dialog_new(_("Export"),
                                            GTK_WINDOW(world->dlgEditWorld),
                                            GTK_FILE_CHOOSER_ACTION_SAVE,
                                            _("_Cancel"),
                                            GTK_RESPONSE_CANCEL,
                                            _("_Save"),
                                            GTK_RESPONSE_YES,
                                            NULL);
  gtk_file_chooser_set_extra_widget(GTK_FILE_CHOOSER(filechooser),
                                    ExtraFilechooser);

  gtk_widget_show_all(GTK_WIDGET(filechooser));
  if (gtk_dialog_run(GTK_DIALOG(filechooser)) == GTK_RESPONSE_YES) {
    gchar *filename;

    gtk_widget_hide(filechooser);
    filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(filechooser));
    we_guied_do_export(world, filename, ui_builder);
    g_free(filename);
  }
  gtk_widget_destroy(filechooser);
  g_object_unref(ui_builder);
}


static
void
we_guied_do_export(World *world, const gchar *file, GtkBuilder *ui_builder)
{
  GtkTreeSelection *selection;
  GtkTreeModel     *model;
  GList            *selected;
  GList            *listiter;
  GtkTreeIter       iter;
  GString          *file_contents;
  GError           *error = NULL;

  file_contents = g_string_sized_new(1024);

  /* Header */
  g_string_append(file_contents, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
              "<!DOCTYPE kcworld-export SYSTEM \"kcworld.dtd\">\n\n"
              "<kcworld-export>\n");

  /* Triggers */
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "Trigger")))) {
    selection = gtk_tree_view_get_selection(world->viewTrigger);
    selected  = gtk_tree_selection_get_selected_rows(selection, &model);
    if (selected) {
      g_string_append(file_contents, "  <triggers>\n");

      listiter = selected;
      while (listiter) {
        Trigger *trigger;

        gtk_tree_model_get_iter(model, &iter, listiter->data);
        gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &trigger, -1);
        save_trigger(file_contents, trigger);

        listiter = listiter->next;
      }
      g_list_foreach(selected, (GFunc) gtk_tree_path_free, NULL);
      g_list_free(selected);

      g_string_append(file_contents, "  </triggers>\n");
    }
  }

  /* Aliases */
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "Alias")))) {
    selection = gtk_tree_view_get_selection(world->viewAlias);
    selected  = gtk_tree_selection_get_selected_rows(selection, &model);
    if (selected) {
      g_string_append(file_contents, "  <aliases>\n");

      listiter = selected;
      while (listiter) {
        Alias *alias;

        gtk_tree_model_get_iter(model, &iter, listiter->data);
        gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &alias, -1);
        save_alias(file_contents, alias);

        listiter = listiter->next;
      }
      g_list_foreach(selected, (GFunc) gtk_tree_path_free, NULL);
      g_list_free(selected);

      g_string_append(file_contents, "  </aliases>\n");
    }
  }

  /* Macros */
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "Macro")))) {
    selection = gtk_tree_view_get_selection(world->viewMacro);
    selected  = gtk_tree_selection_get_selected_rows(selection, &model);
    if (selected) {
      g_string_append(file_contents, "  <macros>\n");

      listiter = selected;
      while (listiter) {
        Macro *macro;

        gtk_tree_model_get_iter(model, &iter, listiter->data);
        gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &macro, -1);
        save_macro(file_contents, macro);

        listiter = listiter->next;
      }
      g_list_foreach(selected, (GFunc) gtk_tree_path_free, NULL);
      g_list_free(selected);

      g_string_append(file_contents, "  </macros>\n");
    }
  }

  /* Timers */
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "Timer")))) {
    selection = gtk_tree_view_get_selection(world->viewTimer);
    selected  = gtk_tree_selection_get_selected_rows(selection, &model);
    if (selected) {
      g_string_append(file_contents, "  <timers>\n");

      listiter = selected;
      while (listiter) {
        Timer *timer;

        gtk_tree_model_get_iter(model, &iter, listiter->data);
        gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &timer, -1);
        save_timer(file_contents, timer);

        listiter = listiter->next;
      }
      g_list_foreach(selected, (GFunc) gtk_tree_path_free, NULL);
      g_list_free(selected);

      g_string_append(file_contents, "  </timers>\n");
    }
  }

  /* Permanent Variables */
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "Var")))) {
    selection = gtk_tree_view_get_selection(world->viewVar);
    selected  = gtk_tree_selection_get_selected_rows(selection, &model);
    if (selected) {
      g_string_append(file_contents, "  <variables>\n");

      listiter = selected;
      while (listiter) {
        gchar *name;

        gtk_tree_model_get_iter(model, &iter, listiter->data);
        gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &name, -1);
        g_string_append_printf(file_contents,
                               "    <variable>%s</variable>\n",
                               name);

        listiter = listiter->next;
      }
      g_list_foreach(selected, (GFunc) gtk_tree_path_free, NULL);
      g_list_free(selected);

      g_string_append(file_contents, "  </variables>\n");
    }
  }

  /* Hooks */
  g_string_append(file_contents, "  <hooksv2>\n");
  we_guied_export_selected_hooks(file_contents,
                                 world->selected_OnConnect_hooks,
                                 "OnConnect",
                                 GTK_TREE_MODEL(world->hooks.OnConnect_model_filter),
                                 ui_builder);
  we_guied_export_selected_hooks(file_contents,
                                 world->selected_OnDisconnect_hooks,
                                 "OnDisconnect",
                                 GTK_TREE_MODEL(world->hooks.OnDisconnect_model_filter),
                                 ui_builder);
  we_guied_export_selected_hooks(file_contents,
                                 world->selected_OnReceivedText_hooks,
                                 "OnReceivedText",
                                 GTK_TREE_MODEL(world->hooks.OnReceivedText_model_filter),
                                 ui_builder);
  we_guied_export_selected_hooks(file_contents,
                                 world->selected_OnSentCommand_hooks,
                                 "OnSentCommand",
                                 GTK_TREE_MODEL(world->hooks.OnSentCommand_model_filter),
                                 ui_builder);
  we_guied_export_selected_hooks(file_contents,
                                 world->selected_OnGetFocus_hooks,
                                 "OnGetFocus",
                                 GTK_TREE_MODEL(world->hooks.OnGetFocus_model_filter),
                                 ui_builder);
  we_guied_export_selected_hooks(file_contents,
                                 world->selected_OnLoseFocus_hooks,
                                 "OnLoseFocus",
                                 GTK_TREE_MODEL(world->hooks.OnLoseFocus_model_filter),
                                 ui_builder);
  we_guied_export_selected_hooks(file_contents,
                                 world->selected_OnCloseConnected_hooks,
                                 "OnCloseConnected",
                                 GTK_TREE_MODEL(world->hooks.OnCloseConnected_model_filter),
                                 ui_builder);
  we_guied_export_selected_hooks(file_contents,
                                 world->selected_OnServerData_hooks,
                                 "OnServerData",
                                 GTK_TREE_MODEL(world->hooks.OnServerData_model_filter),
                                 ui_builder);
  g_string_append(file_contents, "  </hooksv2>\n");

  g_string_append(file_contents, "</kcworld-export>\n");


  if (!g_file_set_contents(file,
                           file_contents->str, file_contents->len,
                           &error)) {
    kc_error_dialog(GTK_WINDOW(world->dlgEditWorld),
                    _("Could not open file '%s': %s"),
                    file, error->message);
    g_error_free(error);
  }
  g_string_free(file_contents, TRUE);
}


static
void
we_guied_export_selected_hooks(GString      *str,
                               GSList       *list,
                               const gchar  *name,
                               GtkTreeModel *model,
                               GtkBuilder   *ui_builder)
{
  GSList      *listiter;
  GtkTreeIter  iter;
  Hook        *hook;

  if (!gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, name)))) {
    return;
  }

  if (list) {
    GtkTreePath *path;

    g_string_append_printf(str, "    <hooklistv2 for=\"%s\">\n", name);

    listiter = list;
    while (listiter) {
      if (gtk_tree_row_reference_valid(listiter->data)) {
        path = gtk_tree_row_reference_get_path(listiter->data);
        gtk_tree_model_get_iter(model, &iter, path);
        gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &hook, -1);
        save_hook(str, hook);
      }
      listiter = listiter->next;
    }

    g_string_append(str, "    </hooklistv2>\n");
  }
}


void
we_guied_import_cb(GtkButton *button, gpointer data)
{
  World     *world = (World *) data;
  GtkWidget *filechooser;

  filechooser = gtk_file_chooser_dialog_new(_("Import"),
                                            GTK_WINDOW(world->dlgEditWorld),
                                            GTK_FILE_CHOOSER_ACTION_OPEN,
                                            _("_Cancel"),
                                            GTK_RESPONSE_CANCEL,
                                            _("_Open"),
                                            GTK_RESPONSE_YES,
                                            NULL);
  if (gtk_dialog_run(GTK_DIALOG(filechooser)) == GTK_RESPONSE_YES) {
    gchar *filename;

    gtk_widget_hide(filechooser);
    filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(filechooser));
    we_guied_do_import(world, filename);
    g_free(filename);
  }
  gtk_widget_destroy(filechooser);
}


static
void
we_guied_do_import(World *world, const gchar *filename)
{
  World    *newworld;
  gboolean  success;
  GError   *error = NULL;

  newworld = create_new_world(FALSE);
  success = load_world_from_file(filename, newworld, FALSE, &error);
  if (!success) {
    kc_error_dialog(GTK_WINDOW(world->dlgEditWorld),
                    _("Could not import file '%s': %s"),
                    filename, error->message);
    g_error_free(error);
    free_world(newworld);
    return;
  }

  we_guied_try_import(&world->triggers,
                      newworld->triggers,
                      &world->trigger_pos,
                      we_trigger_insert_trigger,
                      world,
                      NULL);
  we_guied_try_import(&world->aliases,
                      newworld->aliases,
                      &world->alias_pos,
                      (UpdateFunc) we_alias_insert_alias,
                      world,
                      NULL);
  we_guied_try_import(&world->macros,
                      newworld->macros,
                      &world->macro_pos,
                      (UpdateFunc) we_macro_insert_macro,
                      world,
                      NULL);
  we_guied_try_import(&world->timers,
                      newworld->timers,
                      &world->timer_pos,
                      (UpdateFunc) we_timer_insert_timer,
                      world,
                      NULL);
  we_guied_try_import_vars(world, newworld);
  we_guied_try_import(&world->hooks.OnConnect,
                      newworld->hooks.OnConnect,
                      &world->hooks.OnConnect_pos,
                      (UpdateFunc) we_hook_insert_hook,
                      world,
                      "OnConnect");
  we_guied_try_import(&world->hooks.OnDisconnect,
                      newworld->hooks.OnDisconnect,
                      &world->hooks.OnDisconnect_pos,
                      (UpdateFunc) we_hook_insert_hook,
                      world,
                      "OnDisconnect");
  we_guied_try_import(&world->hooks.OnReceivedText,
                      newworld->hooks.OnReceivedText,
                      &world->hooks.OnReceivedText_pos,
                      (UpdateFunc) we_hook_insert_hook,
                      world,
                      "OnReceivedText");
  we_guied_try_import(&world->hooks.OnSentCommand,
                      newworld->hooks.OnSentCommand,
                      &world->hooks.OnSentCommand_pos,
                      (UpdateFunc) we_hook_insert_hook,
                      world,
                      "OnSentCommand");
  we_guied_try_import(&world->hooks.OnGetFocus,
                      newworld->hooks.OnGetFocus,
                      &world->hooks.OnGetFocus_pos,
                      (UpdateFunc) we_hook_insert_hook,
                      world,
                      "OnGetFocus");
  we_guied_try_import(&world->hooks.OnLoseFocus,
                      newworld->hooks.OnLoseFocus,
                      &world->hooks.OnLoseFocus_pos,
                      (UpdateFunc) we_hook_insert_hook,
                      world,
                      "OnLoseFocus");
  we_guied_try_import(&world->hooks.OnCloseConnected,
                      newworld->hooks.OnCloseConnected,
                      &world->hooks.OnCloseConnected_pos,
                      (UpdateFunc) we_hook_insert_hook,
                      world,
                      "OnCloseConnected");
  we_guied_try_import(&world->hooks.OnServerData,
                      newworld->hooks.OnServerData,
                      &world->hooks.OnServerData_pos,
                      (UpdateFunc) we_hook_insert_hook,
                      world,
                      "OnServerData");

  /* This can only be done after the importing because it uses a Perl
     interpreter. The ones that have already the precomputed RE's are
     not changed. */
  if (world->connected_at_least_once) {
    trigger_precompute_res(world);
    alias_precompute_res(world);
  }

  /* Since we used the lists directly, we set them to NULL to avoid
     freeing them. */
  newworld->triggers               = NULL;
  newworld->aliases                = NULL;
  newworld->macros                 = NULL;
  newworld->timers                 = NULL;
  newworld->permanent_variables    = NULL;
  newworld->hooks.OnConnect        = NULL;
  newworld->hooks.OnDisconnect     = NULL;
  newworld->hooks.OnReceivedText   = NULL;
  newworld->hooks.OnSentCommand    = NULL;
  newworld->hooks.OnGetFocus       = NULL;
  newworld->hooks.OnLoseFocus      = NULL;
  newworld->hooks.OnCloseConnected = NULL;
  newworld->hooks.OnServerData     = NULL;
  free_world(newworld);
}


static
void
we_guied_try_import(GSList      **oldlist,
                    GSList       *newlist,
                    gint         *pos,
                    UpdateFunc    update_func,
                    World        *world,
                    const gchar  *evt_name)
{
  if (newlist) {
    GSList *last;
    GSList *listiter;
    gint    first_pos;
    gint    i;

    if (!*oldlist) { /* No items */
      *oldlist = newlist;
      *pos = g_slist_length(*oldlist);
      last = NULL;
      first_pos = 0;
    } else {
      if (!*pos) { /* Only plugin items */
        last = *oldlist;
        first_pos = 0;
        *pos = g_slist_length(newlist);
        *oldlist = g_slist_concat(newlist, *oldlist);
      } else { /* Has normal items (and perhaps plugin ones) */
        GSList *last_normal;
        GSList *new_last;

        /* We insert the new list in the middle */
        last_normal = g_slist_nth(*oldlist, *pos - 1);
        first_pos = *pos;
        *pos += g_slist_length(newlist);
        last = last_normal->next;
        last_normal->next = newlist;
        new_last = g_slist_last(newlist);
        new_last->next = last;
      }
    }

    /* Show new items in GUI editor */
    for (listiter = newlist, i = first_pos;
         listiter != last;
         listiter = listiter->next, ++i) {
      if (evt_name) {
        update_func(world, evt_name, listiter->data, i);
      } else {
        update_func(world, listiter->data, i);
      }
    }
  }
}


static
void
we_guied_try_import_vars(World *world, World *newworld)
{
  GSList *listiter;

  listiter = newworld->permanent_variables;
  while (listiter) {
    if (!g_slist_find_custom(world->permanent_variables,
                             listiter->data,
                             (GCompareFunc) strcmp)) {
      world->permanent_variables =
        g_slist_append(world->permanent_variables, listiter->data);
      we_var_insert_var(world, listiter->data, -1);
    }
    listiter = listiter->next;
  }
}


void
we_guied_bool_func(GtkTreeViewColumn *column,
                   GtkCellRenderer   *renderer,
                   GtkTreeModel      *model,
                   GtkTreeIter       *iter,
                   gpointer           data)
{
  gpointer   object;
  gint       op;
  gboolean  *value;

  gtk_tree_model_get(model, iter, WE_GUIED_POINTER, &object, -1);

  op = GPOINTER_TO_INT(data);
  value = we_guied_get_pointer_to_bool_value(object, op);

  g_object_set(renderer, "active", *value, NULL);
}


void
we_guied_text_func(GtkTreeViewColumn *column,
                   GtkCellRenderer   *renderer,
                   GtkTreeModel      *model,
                   GtkTreeIter       *iter,
                   gpointer           data)
{
  gpointer      object;
  gint          op;
  gchar        *value = NULL;
  static gchar  printed_value[100];
  gchar        *keycodestr;

  gtk_tree_model_get(model, iter, WE_GUIED_POINTER, &object, -1);

  op = GPOINTER_TO_INT(data);
  switch (op) {
  case PLUGIN_NAME:
    value = ((Plugin *) object)->name;
    break;

  case PLUGIN_VERSION:
    value = ((Plugin *) object)->version;
    break;

  case PLUGIN_AUTHOR:
    value = ((Plugin *) object)->author;
    break;

  case PLUGIN_DESCRIPTION:
    value = ((Plugin *) object)->description;
    break;

  case TRIGGER_PATTERN:
    value = ((Trigger *) object)->pattern;
    break;

  case TRIGGER_ACTION:
    if (((Trigger *) object)->action) {
      value = ((Trigger *) object)->action;
    } else {
      value = "";
    }
    break;

  case TRIGGER_NAME:
    if (((Trigger *) object)->name) {
      value = ((Alias *) object)->name;
    } else {
      value = "";
    }
    break;

  case ALIAS_PATTERN:
    value = ((Alias *) object)->pattern;
    break;

  case ALIAS_SUBSTITUTION:
    value = ((Alias *) object)->substitution;
    break;

  case ALIAS_NAME:
    if (((Alias *) object)->name) {
      value = ((Alias *) object)->name;
    } else {
      value = "";
    }
    break;

  case TIMER_NAME:
    if (((Timer *) object)->name) {
      value = ((Timer *) object)->name;
    } else {
      value = "";
    }
    break;

  case TIMER_ACTION:
    value = ((Timer *) object)->action;
    break;

  case TIMER_INTERVAL:
    g_snprintf(printed_value, 100, "%.3f", ((Timer *) object)->interval);
    value = printed_value;
    break;

  case TIMER_COUNT:
    g_snprintf(printed_value, 100, "%d", ((Timer *) object)->count);
    value = printed_value;
    break;

  case MACRO_NAME:
    if (((Macro *) object)->name) {
      value = ((Macro *) object)->name;
    } else {
      value = "";
    }
    break;

  case MACRO_ACTION:
    value = ((Macro *) object)->action;
    break;

  case MACRO_KEY:
    keycodestr = gtk_accelerator_name(((Macro *) object)->keyval,
                                      ((Macro *) object)->modifiers);
    g_strlcpy(printed_value, keycodestr, 100);
    g_free(keycodestr);
    value = printed_value;
    break;

  case HOOK_NAME:
    if (((Hook *) object)->name) {
      value = ((Hook *) object)->name;
    } else {
      value = "";
    }
    break;

  case HOOK_ACTION:
    value = ((Hook *) object)->action;
    break;

  case PERMVAR_NAME:
    value = (gchar *) object;
    break;

  case OBJECT_PLUGIN:
    if (((GeneralObject *) object)->owner_plugin) {
      value = ((GeneralObject *) object)->owner_plugin->name;
    } else {
      value = "";
    }
    break;
  }

  g_object_set(renderer, "text", value, NULL);
}


gboolean
we_guied_plugin_filter_func(GtkTreeModel *model,
                            GtkTreeIter  *iter,
                            gpointer      data)
{
  World    *world = (World *) data;
  gpointer  object;

  if (world->show_plugin_items) {       /* Display all objects */
    return TRUE;
  }

  gtk_tree_model_get(model, iter, WE_GUIED_POINTER, &object, -1);
  if (!object) {
    return FALSE;
  }

  if (((GeneralObject *) object)->owner_plugin) {
    return FALSE;
  }
  return TRUE;
}


void
we_guied_bool_col_toggled_cb(GtkCellRendererToggle *renderer,
                             gchar                 *pathstr,
                             gpointer               data)
{
  GtkTreeView      *view;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  GtkTreeIter       iter;
  GtkTreePath      *path;
  gint              column;
  gpointer          object;
  gboolean         *value;

  view      = (GtkTreeView *) data;
  model     = gtk_tree_view_get_model(view);
  selection = gtk_tree_view_get_selection(view);

  gtk_tree_model_get_iter_from_string(model, &iter, pathstr);
  /* Only toggle if we click in the active row, otherwise just select
     that row. */
  if (gtk_tree_selection_iter_is_selected(selection, &iter)) {
    gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &object, -1);
    column = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(renderer),
                                               "column"));
    value = we_guied_get_pointer_to_bool_value(object, column);

    /* Update */
    *value = !*value;
    path = gtk_tree_path_new_from_string(pathstr);
    gtk_tree_model_row_changed(model, path, &iter);
    gtk_tree_path_free(path);

    /* Recompute REs if Ignore Case is toggled */
    if (column == TRIGGER_ICASE) {
      World   *world   = g_object_get_data(G_OBJECT(view),
                                           "forworld");
      Trigger *trigger = (Trigger *) object;
      if (world->perl_interpreter) {
        GRAB_PERL(world);
        if (trigger->pattern_re) {
          SvREFCNT_dec(trigger->pattern_re);
        }
        trigger->pattern_re = precompute_re(trigger->pattern,
                                            trigger->ignore_case);
        RELEASE_PERL(world);
      }
    }
    if (column == ALIAS_ICASE) {
      World *world = g_object_get_data(G_OBJECT(view),
                                       "forworld");
      Alias *alias = (Alias *) object;
      if (world->perl_interpreter) {
        GRAB_PERL(world);
        if (alias->pattern_re) {
          SvREFCNT_dec(alias->pattern_re);
        }
        alias->pattern_re = precompute_re(alias->pattern,
                                          alias->ignore_case);
        RELEASE_PERL(world);
      }
    }

    /* Turn timer on or off if necessary */
    if (column == TIMER_ENABLED) {
      Timer *timer = (Timer *) object;
      if (timer->for_world->connected) {
        if (*value) {
          timer->function_id = g_timeout_add(timer->interval * 1000,
                                             run_timer,
                                             timer);
        } else {
          g_source_remove(timer->function_id);
        }
      }
    }
  }
}


static
gboolean *
we_guied_get_pointer_to_bool_value(gpointer object, gint column)
{
  gboolean *value = NULL;

  switch (column) {
  case PLUGIN_ENABLED:
    value = &((Plugin *) object)->enabled;
    break;

  case TRIGGER_ENABLED:
    value = &((Trigger *) object)->enabled;
    break;

  case TRIGGER_GAG:
    value = &((Trigger *) object)->gag_output;
    break;

  case TRIGGER_GAGLOG:
    value = &((Trigger *) object)->gag_log;
    break;

  case TRIGGER_KEEPEXEC:
    value = &((Trigger *) object)->keepexecuting;
    break;

  case TRIGGER_REWRITER:
    value = &((Trigger *) object)->rewriter;
    break;

  case TRIGGER_ICASE:
    value = &((Trigger *) object)->ignore_case;
    break;

  case TRIGGER_HIGHLIGHT:
    value = &((Trigger *) object)->highlight;
    break;

  case ALIAS_ENABLED:
    value = &((Alias *) object)->enabled;
    break;

  case ALIAS_PERL_EVAL:
    value = &((Alias *) object)->perl_eval;
    break;

  case ALIAS_ICASE:
    value = &((Alias *) object)->ignore_case;
    break;

  case TIMER_ENABLED:
    value = &((Timer *) object)->enabled;
    break;

  case TIMER_TEMPORARY:
    value = &((Timer *) object)->temporary;
    break;

  case MACRO_ENABLED:
    value = &((Macro *) object)->enabled;
    break;

  case HOOK_ENABLED:
    value = &((Hook *) object)->enabled;
    break;
  }

  return value;
}


void
we_guied_selection_changed_cb(GtkTreeSelection *selection,
                              gpointer          data)
{
  GtkWidget *btnEdit;
  GtkWidget *btnDelete;
  GtkWidget *btnUp;
  GtkWidget *btnDown;
  GtkWidget *btnExport;
  int        n;

  btnEdit   = g_object_get_data(G_OBJECT(selection), "btnEdit");
  btnDelete = g_object_get_data(G_OBJECT(selection), "btnDelete");
  btnUp     = g_object_get_data(G_OBJECT(selection), "btnUp");
  btnDown   = g_object_get_data(G_OBJECT(selection), "btnDown");
  btnExport = g_object_get_data(G_OBJECT(selection), "btnExport");

  n = gtk_tree_selection_count_selected_rows(selection);
  gtk_widget_set_sensitive(btnEdit,   n == 1);
  gtk_widget_set_sensitive(btnUp,     n == 1);
  gtk_widget_set_sensitive(btnDown,   n == 1);
  gtk_widget_set_sensitive(btnDelete, n >= 1);
  gtk_widget_set_sensitive(btnExport, n >= 1);
}


GtkWidget *
we_guied_confirm_delete_dialog_new(GtkWindow   *parent,
                                   const gchar *objname,
                                   int          n)
{
  GtkWidget *msgdlg;

  msgdlg = gtk_message_dialog_new(parent,
                                  GTK_DIALOG_MODAL,
                                  GTK_MESSAGE_QUESTION,
                                  GTK_BUTTONS_NONE,
                                  ngettext("Do you really want to delete the %1$s?",
                                           "Do you really want to delete %2$d %1$s?",
                                           n),
                                  objname, n);
  gtk_window_set_title(GTK_WINDOW(msgdlg), _("Confirm deletion"));
  gtk_dialog_add_buttons(GTK_DIALOG(msgdlg),
                         _("Do _not delete"), GTK_RESPONSE_NO,
                         _("_Delete"), GTK_RESPONSE_YES,
                         NULL);

  return msgdlg;
}
