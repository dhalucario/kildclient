/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include "libintl-wrapper.h"
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

#include "kildclient.h"
#include "perlscript.h"


/***********************
 * Function prototypes *
 ***********************/
static GtkTreeModel *create_we_vars_model(World *world);
static void          we_var_configure_view(GtkTreeView *view,
                                           GtkTreeModel *model);
static void          we_var_add_cb(GtkButton *button, gpointer data);
static void          we_var_edit_cb(GtkButton *button, gpointer data);
static void          we_var_delete_cb(GtkButton *button, gpointer data);
static void          we_var_view_row_dblclick_cb(GtkTreeView        *view,
                                                 GtkTreePath        *path,
                                                 GtkTreeViewColumn  *col,
                                                 gpointer            data);
static gchar        *edit_var(World *world, const gchar *var);
static gboolean      we_var_view_keypress_cb(GtkWidget   *widget,
                                             GdkEventKey *evt,
                                             gpointer     data);
static void          we_var_move_cb(GtkButton *button, gpointer data);



GObject *
get_we_vars_tab(World *world)
{
  GtkBuilder       *ui_builder;
  gchar            *objects[] = { "we_panel_generic_graphical_editor", NULL };
  GError           *error = NULL;
  GObject          *panel;
  GtkLabel         *lblType;
  GObject          *btnAdd;
  GObject          *btnEdit;
  GObject          *btnDelete;
  GObject          *btnUp;
  GObject          *btnDown;
  GObject          *btnExport;
  GtkTreeSelection *selection;

  /* Create the dialog */
  ui_builder = gtk_builder_new();
  if (!gtk_builder_add_objects_from_resource(ui_builder,
                                             "/ekalin/kildclient/kildclient.ui",
                                             objects,
                                             &error)) {
    g_warning(_("Error loading UI from XML file: %s"), error->message);
    g_error_free(error);
    return NULL; /* This will cause warnings in the calling function, but
                    there is need to abort the program because of this. */
  }

  panel = gtk_builder_get_object(ui_builder,
                                 "we_panel_generic_graphical_editor");
  /* So it's not destroyed when the builder is destroyed */
  g_object_ref(panel);

  world->viewVar = GTK_TREE_VIEW(gtk_builder_get_object(ui_builder,
                                                      "viewObjects"));
  g_object_set_data(G_OBJECT(world->viewVar), "forworld", world);
  g_signal_connect(G_OBJECT(world->viewVar), "row-activated",
                   G_CALLBACK(we_var_view_row_dblclick_cb), world);
  g_signal_connect(G_OBJECT(world->viewVar), "key-press-event",
                   G_CALLBACK(we_var_view_keypress_cb), world);

  lblType = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblGEType"));
  gtk_label_set_markup(lblType,
                       "<span weight=\"bold\">Permanent variables</span>");

  btnAdd = gtk_builder_get_object(ui_builder, "btnGEAdd");
  g_signal_connect(btnAdd, "clicked",
                   G_CALLBACK(we_var_add_cb), world->viewVar);
  btnEdit = gtk_builder_get_object(ui_builder, "btnGEEdit");
  g_signal_connect(btnEdit, "clicked",
                   G_CALLBACK(we_var_edit_cb), world->viewVar);
  btnDelete = gtk_builder_get_object(ui_builder, "btnGEDelete");
  g_signal_connect(btnDelete, "clicked",
                   G_CALLBACK(we_var_delete_cb), world->viewVar);
  btnUp = gtk_builder_get_object(ui_builder, "btnGEUp");
  g_object_set_data(btnUp, "isup", GINT_TO_POINTER(TRUE));
  g_signal_connect(btnUp, "clicked",
                   G_CALLBACK(we_var_move_cb), world->viewVar);
  btnDown = gtk_builder_get_object(ui_builder, "btnGEDown");
  g_object_set_data(btnDown, "isup", GINT_TO_POINTER(FALSE));
  g_signal_connect(btnDown, "clicked",
                   G_CALLBACK(we_var_move_cb), world->viewVar);

  btnExport = gtk_builder_get_object(ui_builder, "btnGEExport");
  g_object_set_data(btnExport, "preselect", "Var");

  world->permvars_model = create_we_vars_model(world);

  we_var_configure_view(world->viewVar, world->permvars_model);
  selection = gtk_tree_view_get_selection(world->viewVar);
  gtk_tree_selection_set_mode(selection, GTK_SELECTION_MULTIPLE);
  g_object_set_data(G_OBJECT(selection), "btnEdit",   btnEdit);
  g_object_set_data(G_OBJECT(selection), "btnDelete", btnDelete);
  g_object_set_data(G_OBJECT(selection), "btnUp",     btnUp);
  g_object_set_data(G_OBJECT(selection), "btnDown",   btnDown);
  g_object_set_data(G_OBJECT(selection), "btnExport", btnExport);
  g_signal_connect(G_OBJECT(selection), "changed",
                   G_CALLBACK(we_guied_selection_changed_cb), NULL);

  gtk_builder_connect_signals(ui_builder, world);
  g_object_unref(ui_builder);
  return panel;
}


static
GtkTreeModel *
create_we_vars_model(World *world)
{
  GtkListStore *store;
  GtkTreeIter   iter;
  GSList       *varptr;
  gchar        *var;

  store = gtk_list_store_new(WE_GUIED_N_COLS,
                             G_TYPE_POINTER);

  varptr = world->permanent_variables;
  while (varptr) {
    var = (gchar *) varptr->data;
    gtk_list_store_append(store, &iter);
    gtk_list_store_set(store, &iter,
                       WE_GUIED_POINTER, var,
                       -1);

    varptr = varptr->next;
  }

  return GTK_TREE_MODEL(store);
}


static
void
we_var_configure_view(GtkTreeView *view, GtkTreeModel *model)
{
  GtkCellRenderer   *renderer;
  GtkTreeViewColumn *column;

  gtk_tree_view_set_model(view, model);
  gtk_tree_view_set_headers_visible(view, FALSE);
  g_object_unref(model);

  /* Column 1 - Name */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_text_func,
                                          GINT_TO_POINTER(PERMVAR_NAME),
                                          NULL);
  gtk_tree_view_append_column(view, column);


  gtk_tree_view_columns_autosize(view);
}


static
void
we_var_add_cb(GtkButton *button, gpointer data)
{
  World       *world;
  GtkTreeView *view;
  gchar       *var;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");

  var = edit_var(world, NULL);
  if (var) {
    world->permanent_variables = g_slist_append(world->permanent_variables,
                                                var);
    we_var_insert_var(world, var, -1);
  }
}


static
void
we_var_edit_cb(GtkButton *button, gpointer data)
{
  /* Note that the Edit button is only active when exactly one row
     is selected. */
  World            *world;
  GtkTreeView      *view;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  GList            *selected;
  GtkTreeIter       iter;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");
  selection = gtk_tree_view_get_selection(view);
  selected = gtk_tree_selection_get_selected_rows(selection, &model);
  if (selected) {
    gchar *var;
    gchar *newvar;

    gtk_tree_model_get_iter(model, &iter, selected->data);
    gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &var, -1);

    newvar = edit_var(world, var);
    if (newvar) {
      GSList *item;

      we_var_update_var(world, var, newvar);
      item = g_slist_find(world->permanent_variables, var);
      g_free(var);
      item->data = newvar;
    }

    g_list_foreach(selected, (GFunc) gtk_tree_path_free, NULL);
    g_list_free(selected);
  }
}


static
void
we_var_delete_cb(GtkButton *button, gpointer data)
{
  World            *world;
  GtkTreeView      *view;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  GList            *selected;
  GtkTreeIter       iter;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");
  selection = gtk_tree_view_get_selection(view);
  selected = gtk_tree_selection_get_selected_rows(selection, &model);
  if (selected) {
    gchar     *var;
    GtkWidget *msgdlg;
    gint       n;

    n = gtk_tree_selection_count_selected_rows(selection);
    msgdlg = gtk_message_dialog_new(GTK_WINDOW(world->dlgEditWorld),
                                    GTK_DIALOG_MODAL,
                                    GTK_MESSAGE_QUESTION,
                                    GTK_BUTTONS_NONE,
                                    ngettext("Do you really want to make the variable temporary?",
                                             "Do you really want to make %d variables temporary?",
                                             n),
                                    n);
    gtk_window_set_title(GTK_WINDOW(msgdlg), _("Confirm deletion"));
    gtk_dialog_add_buttons(GTK_DIALOG(msgdlg),
                           _("Keep _permanent"), GTK_RESPONSE_NO,
                           _("Make _temporary"), GTK_RESPONSE_YES,
                           NULL);

    if (!world->confirm_delete
        || gtk_dialog_run(GTK_DIALOG(msgdlg)) == GTK_RESPONSE_YES) {
      GList *listiter = g_list_last(selected);

      while (listiter) {
        gtk_tree_model_get_iter(model, &iter, listiter->data);
        gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &var, -1);

        we_var_delete_var(world, var);
        world->permanent_variables
          = g_slist_remove(world->permanent_variables, var);
        g_free(var);

        listiter = listiter->prev;
      }
    }
    gtk_widget_destroy(msgdlg);

    g_list_foreach(selected, (GFunc) gtk_tree_path_free, NULL);
    g_list_free(selected);
  }
}


static
void
we_var_view_row_dblclick_cb(GtkTreeView       *view,
                            GtkTreePath       *path,
                            GtkTreeViewColumn *col,
                            gpointer           data)
{
  World        *world;
  GtkTreeModel *model;
  GtkTreeIter   iter;

  world = (World *) data;
  model = gtk_tree_view_get_model(view);

  if (gtk_tree_model_get_iter(model, &iter, path)) {
    gchar *var;
    gchar *newvar;

    gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &var, -1);

    newvar = edit_var(world, var);
    if (newvar) {
      GSList *item;

      we_var_update_var(world, var, newvar);
      item = g_slist_find(world->permanent_variables, var);
      g_free(var);
      item->data = newvar;
    }
  }
}


static
gchar *
edit_var(World *world, const gchar *var)
{
  GtkBuilder      *ui_builder;
  gchar           *objects[] = { "dlgEditVar", NULL };
  GError          *error = NULL;
  GtkWidget       *dlgEditVar;
  GtkEntry        *txtName;
  const gchar     *givenname;
  gchar           *newname;

  ui_builder = gtk_builder_new();
  if (!gtk_builder_add_objects_from_resource(ui_builder,
                                             "/ekalin/kildclient/kildclient.ui",
                                             objects,
                                             &error)) {
    g_warning(_("Error loading UI from XML file: %s"), error->message);
    g_error_free(error);
    return NULL;
  }
  dlgEditVar = GTK_WIDGET(gtk_builder_get_object(ui_builder, "dlgEditVar"));
  gtk_window_set_transient_for(GTK_WINDOW(dlgEditVar),
                               GTK_WINDOW(world->dlgEditWorld));
  gtk_dialog_set_default_response(GTK_DIALOG(dlgEditVar), GTK_RESPONSE_OK);

  txtName    = GTK_ENTRY(gtk_builder_get_object(ui_builder, "txtVarName"));

  /* We don't need it anymore */
  g_object_unref(ui_builder);

  /* Fill-in values */
  if (var) {
    gtk_entry_set_text(txtName, var);
  }

  /* Run the dialog until the input is valid or cancelled */
  gtk_widget_show_all(dlgEditVar);
  while (1) {
    if (gtk_dialog_run(GTK_DIALOG(dlgEditVar)) == GTK_RESPONSE_OK) {
      /* Validate */
      givenname = gtk_entry_get_text(txtName);
      if (strcmp(givenname, "") == 0) {
        kc_error_dialog(GTK_WINDOW(dlgEditVar),
                        _("You must specify a variable name."));
        continue;
      }

      if (givenname[0] != '$' && givenname[0] != '@' && givenname[0] != '%') {
        kc_error_dialog(GTK_WINDOW(dlgEditVar),
                        _("Only scalar ($), array (@) and hash (%%) variables can be made permanent."));
        continue;
      }

      /* If we are adding (var == NULL) a new variable, or when editing
         but a different name is typed, verify if there's already a
         variable with that name. If we are editing and just press OK,
         ignore this check for the user convenience. */
      if ((!var || strcmp(var, givenname) != 0) &&
          g_slist_find_custom(world->permanent_variables,
                              givenname,
                              (GCompareFunc) strcmp)) {
        kc_error_dialog(GTK_WINDOW(dlgEditVar),
                        _("%s is already permanent."), givenname);
        continue;
      }

      /* We must make the copy before gtk_widget_destroy is called */
      newname = g_strdup(givenname);
      gtk_widget_destroy(dlgEditVar);
      return newname;
    } else {
      /* Cancel pressed */
      gtk_widget_destroy(dlgEditVar);
      return NULL;
    }
  }
}


static
gboolean
we_var_view_keypress_cb(GtkWidget   *widget,
                        GdkEventKey *evt,
                        gpointer     data)
{
  if (evt->keyval == GDK_KEY_Delete || evt->keyval == GDK_KEY_KP_Delete) {
    we_var_delete_cb(NULL, widget);
    return TRUE;
  }

  return FALSE;
}


static
void
we_var_move_cb(GtkButton *button, gpointer data)
{
  GtkTreeView      *view;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  GList            *selected;
  GtkTreeIter       iter;
  World            *world;
  gboolean          is_up;
  gint              new_pos;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");
  selection = gtk_tree_view_get_selection(view);
  is_up = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(button), "isup"));
  selected = gtk_tree_selection_get_selected_rows(selection, &model);
  if (selected) {
    gchar *var;
    gint   pos;

    gtk_tree_model_get_iter(model, &iter, selected->data);
    gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &var, -1);
    pos = g_slist_index(world->permanent_variables, var);
    new_pos = pos;
    if (is_up) {
      if (pos != 0) {
        new_pos = pos - 1;
        move_var(world, pos, new_pos);
      }
    } else {
      if (pos != g_slist_length(world->permanent_variables) - 1) {
        new_pos = pos + 1;
        move_var(world, pos, new_pos);
      }
    }
    /* Reselect the moved item */
    gtk_tree_model_iter_nth_child(model, &iter, NULL, new_pos);
    gtk_tree_selection_select_iter(selection, &iter);

    g_list_foreach(selected, (GFunc) gtk_tree_path_free, NULL);
    g_list_free(selected);
  }
}


void
we_var_update_var(World *world, const gchar *old_var, const gchar *new_var)
{
  /* Called by the Perl functions when an var is changed, so that
     the display is updated. */
  GtkTreeIter  iter;
  gchar       *var;
  gboolean     success;

  if (!world->permvars_model) {
    return;
  }

  success = gtk_tree_model_get_iter_first(world->permvars_model, &iter);
  while (success) {
    gtk_tree_model_get(world->permvars_model, &iter,
                       WE_GUIED_POINTER, &var, -1);
    if (var == old_var) {
      gtk_list_store_set(GTK_LIST_STORE(world->permvars_model), &iter,
                         WE_GUIED_POINTER, new_var, -1);
      return;
    }
    success = gtk_tree_model_iter_next(world->permvars_model, &iter);
  }
}


void
we_var_insert_var(World *world, const gchar *var, gint pos)
{
  /* Called by the Perl functions when an var is inserted, so that
     the display is updated. */
  GtkTreeIter iter;

  if (!world->permvars_model) {
    return;
  }

  if (pos < 0) {
    gtk_list_store_append(GTK_LIST_STORE(world->permvars_model), &iter);
  } else {
    gtk_list_store_insert(GTK_LIST_STORE(world->permvars_model), &iter, pos);
  }
  gtk_list_store_set(GTK_LIST_STORE(world->permvars_model), &iter,
                     WE_GUIED_POINTER, var,
                     -1);
}


void
we_var_delete_var(World *world, const gchar *var_arg)
{
  /* Called by the Perl functions when an var is deleted, so that
     the display is updated. */
  GtkTreeIter  iter;
  gchar       *var;
  gboolean     success;

  if (!world->permvars_model) {
    return;
  }

  success = gtk_tree_model_get_iter_first(world->permvars_model, &iter);
  while (success) {
    gtk_tree_model_get(world->permvars_model, &iter,
                       WE_GUIED_POINTER, &var, -1);
    if (var == var_arg) {
      gtk_list_store_remove(GTK_LIST_STORE(world->permvars_model), &iter);
      return;
    }
    success = gtk_tree_model_iter_next(world->permvars_model, &iter);
  }
}
