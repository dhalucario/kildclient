/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include <unistd.h>
#ifndef __MINGW32__
#  include <glob.h>
#else
#  include <windows.h>
#endif
#include "libintl-wrapper.h"
#include <gtk/gtk.h>

#include "kildclient.h"
#include "perlscript.h"


/********************
 * Type definitions *
 ********************/
enum {
  COLUMN_NAME,
  COLUMN_FILE,
  COLUMN_INDEX,
  N_COLUMNS,
};


/*************************
 * File global variables *
 *************************/
GtkWidget       *dlgSelectWorld;
GtkToggleButton *radManually;
GtkEntry        *txtHost;
GtkEntry        *txtPort;
GtkTreeView     *lstWorlds;


/***********************
 * Function prototypes *
 ***********************/
static GtkWidget    *create_world_selector_dlg(GtkWindow* parent);
static GtkTreeStore *create_saved_worlds_model(GtkTreeIter **last_open_iter);
static void          fill_saved_worlds_model(GtkTreeStore  *model,
                                             GtkTreeIter  **last_open_iter);
static void          add_saved_worlds_model_entry_from_file(GtkTreeStore *model,
                                                            const gchar  *file,

                                                            GtkTreeIter **last_open_iter);
static void          add_saved_worlds_model_entries(GtkTreeStore *model,
                                                    World        *world,
                                                    GtkTreeIter  *worlditer);
static gint          sort_by_name(GtkTreeModel *model,
                                  GtkTreeIter  *a,
                                  GtkTreeIter  *b,
                                  gpointer      data);
static char         *try_find_world_from_name(const gchar *name);
static char         *compare_world_from_name(const gchar *file,
                                             const gchar *name);
static void          list_selection_changed_cb(GtkTreeSelection *selection,
                                               gpointer data);
static void          add_edit_buttons_cb(GtkButton *btn, gpointer data);
/* XML UI signals */
G_MODULE_EXPORT void set_manually_cb(GtkEditable *widget,
                                     gpointer user);
G_MODULE_EXPORT void list_row_activated_cb(gpointer           data,
                                           GtkTreeView       *treeview,
                                           GtkTreePath       *treepath,
                                           GtkTreeViewColumn *column);
G_MODULE_EXPORT void delete_button_cb(GtkButton *btn, gpointer data);



World *
get_world_to_connect()
{
  GtkWidget *dlgWorldSelector;
  World     *world = NULL;
  gboolean   success;

  dlgWorldSelector = create_world_selector_dlg(GTK_WINDOW(wndMain));
  gtk_widget_show_all(dlgWorldSelector);

  success = FALSE;
  while (!success) {
    if (gtk_dialog_run(GTK_DIALOG(dlgWorldSelector)) == GTK_RESPONSE_OK) {
      world = create_new_world(TRUE);
      world->connected = FALSE;

      if (gtk_toggle_button_get_active(radManually)) {
        create_world_from_parameters(world,
                                     g_strdup(gtk_entry_get_text(txtHost)),
                                     g_strdup(gtk_entry_get_text(txtPort)),
                                     NULL,
                                     NULL);
        success = TRUE;
      } else {
        GtkTreeSelection *selection;
        GtkTreeModel     *model;
        GtkTreeIter       iter;

        selection = gtk_tree_view_get_selection(lstWorlds);
        if (gtk_tree_selection_get_selected(selection, &model, &iter)) {
          char   *file;
          gint    charidx;
          GError *error = NULL;

          gtk_tree_model_get(model, &iter,
                             COLUMN_FILE, &file,
                             COLUMN_INDEX, &charidx,
                             -1);
          success = load_world_from_file(file, world, FALSE, &error);
          if (!success) {
            kc_error_dialog(GTK_WINDOW(wndMain),
                            _("Could not load world from file '%s': %s"),
                            file, error->message);
            g_error_free(error);
            free_world(world);
            world = NULL;
          } else {
            if (charidx == -1) {
              world->character_used = NULL;
            } else {
              GtkTreePath         *path;
              GtkTreeRowReference *ref;

              path = gtk_tree_path_new_from_indices(charidx, -1);
              ref  = gtk_tree_row_reference_new(world->logon_characters, path);

              world->character_used = ref;
              gtk_tree_path_free(path);
            }
          }
        }
      }
    } else {    /* Response was not OK */
      /* Considered successful, returns NULL and the application quits */
      success = TRUE;
    }
  }
  gtk_widget_destroy(dlgWorldSelector);

  return world;
}


static
GtkWidget*
create_world_selector_dlg(GtkWindow* parent)
{
  GtkBuilder *ui_builder;
  GError     *error = NULL;
  GObject    *radFromList;
  GObject    *btnNew;
  GObject    *btnEdit;

  GtkTreeStore      *model;
  GtkCellRenderer   *renderer;
  GtkTreeViewColumn *column;
  GtkTreeSelection  *selection;
  GtkTreeIter       *last_open_iter = NULL;

  ui_builder = gtk_builder_new();
  if (!gtk_builder_add_from_resource(ui_builder,
                                     "/ekalin/kildclient/dlgSelectWorld.ui",
                                     &error)) {
    g_warning(_("Error loading UI from XML file: %s"), error->message);
    g_error_free(error);
    exit(1); /* No point in continuing */
  }

  dlgSelectWorld = GTK_WIDGET(gtk_builder_get_object(ui_builder,
                                                     "dlgSelectWorld"));
  gtk_window_set_transient_for(GTK_WINDOW(dlgSelectWorld), parent);
  gtk_dialog_set_default_response(GTK_DIALOG(dlgSelectWorld), GTK_RESPONSE_OK);

  /* Set global variables pointing to widgets */
  radFromList = gtk_builder_get_object(ui_builder, "radFromList");
  radManually = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder,
                                                         "radManually"));
  txtHost = GTK_ENTRY(gtk_builder_get_object(ui_builder, "txtHost"));
  txtPort = GTK_ENTRY(gtk_builder_get_object(ui_builder, "txtPort"));

  /* Prepare TreeView with the Worlds */
  model = create_saved_worlds_model(&last_open_iter);
  lstWorlds = GTK_TREE_VIEW(gtk_builder_get_object(ui_builder,
                                                   "lstWorlds"));
  gtk_tree_view_set_model(lstWorlds, GTK_TREE_MODEL(model));
  g_object_unref(G_OBJECT(model));
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new_with_attributes(NULL,
                                                    renderer,
                                                    "text",
                                                    COLUMN_NAME,
                                                    NULL);
  gtk_tree_view_append_column(lstWorlds, column);

  /* Pre select last used world */
  selection = gtk_tree_view_get_selection(lstWorlds);
  if (last_open_iter) {
    GtkTreePath *path;

    gtk_tree_selection_select_iter(selection, last_open_iter);
    path = gtk_tree_model_get_path(GTK_TREE_MODEL(model), last_open_iter);
    gtk_tree_view_scroll_to_cell(lstWorlds,
                                 path, NULL,
                                 FALSE, 0, 0);
    gtk_tree_path_free(path);
  }

  /* Connect signals that can't be specified in the XML UI file */
  g_signal_connect(G_OBJECT(selection), "changed",
                   G_CALLBACK(list_selection_changed_cb), radFromList);
  btnNew = gtk_builder_get_object(ui_builder, "btnNew");
  g_signal_connect(btnNew, "clicked",
                   G_CALLBACK(add_edit_buttons_cb), GINT_TO_POINTER(0));
  btnEdit = gtk_builder_get_object(ui_builder, "btnEdit");
  g_signal_connect(btnEdit, "clicked",
                   G_CALLBACK(add_edit_buttons_cb), GINT_TO_POINTER(1));

  gtk_builder_connect_signals(ui_builder, NULL);
  g_object_unref(ui_builder);

  return dlgSelectWorld;
}


static
GtkTreeStore *
create_saved_worlds_model(GtkTreeIter **last_open_iter)
{
  GtkTreeStore    *model;
  GtkTreeSortable *sortable;

  model = gtk_tree_store_new(N_COLUMNS,
                             G_TYPE_STRING,
                             G_TYPE_STRING,
                             G_TYPE_INT);

  fill_saved_worlds_model(model, last_open_iter);

  /* Sort the list */
  sortable = GTK_TREE_SORTABLE(model);
  gtk_tree_sortable_set_sort_func(sortable, COLUMN_NAME,
                                  sort_by_name,
                                  NULL,
                                  NULL);
  gtk_tree_sortable_set_sort_column_id(sortable, COLUMN_NAME,
                                       GTK_SORT_ASCENDING);

  return model;
}


#ifndef __MINGW32__
static
void
fill_saved_worlds_model(GtkTreeStore *model, GtkTreeIter **last_open_iter)
{
  const char *kilddir;
  char       *globstr;
  glob_t      globbuf;

  kilddir = get_kildclient_directory_path();;
  globstr = g_strdup_printf("%s/*.wrl", kilddir);

  if (glob(globstr, GLOB_ERR | GLOB_NOSORT, NULL, &globbuf) == 0) {
    int i;
    for (i = 0; i < globbuf.gl_pathc; ++i) {
      add_saved_worlds_model_entry_from_file(model,
                                             globbuf.gl_pathv[i],
                                             last_open_iter);
    }
  }

  g_free(globstr);
  globfree(&globbuf);
}
#else
static
void
fill_saved_worlds_model(GtkTreeStore *model, GtkTreeIter **last_open_iter)
{
  const char     *kilddir;
  char           *globstr;
  char           *file;
  WIN32_FIND_DATA FindFileData;
  HANDLE          hFind = INVALID_HANDLE_VALUE;

  /* Mental note: Find{First,Next}File only return the file name, without
     path. Windows sucks. */

  kilddir = get_kildclient_directory_path();
  globstr = g_strdup_printf("%s\\*.wrl", kilddir);

  hFind = FindFirstFile(globstr, &FindFileData);
  if (hFind != INVALID_HANDLE_VALUE) {
    file = g_strdup_printf("%s\\%s", kilddir, FindFileData.cFileName);
    add_saved_worlds_model_entry_from_file(model, file, last_open_iter);
    g_free(file);

    while (FindNextFile(hFind, &FindFileData)) {
      file = g_strdup_printf("%s\\%s", kilddir, FindFileData.cFileName);
      add_saved_worlds_model_entry_from_file(model, file, last_open_iter);
      g_free(file);
    }

    FindClose(hFind);
  }

  g_free(globstr);
}
#endif


static
void
add_saved_worlds_model_entry_from_file(GtkTreeStore *model,
                                       const gchar  *file,
                                       GtkTreeIter **last_open_iter)
{
  GtkTreeIter  worlditer;
  World       *world;
  GError      *error = NULL;

  /* We do not call create_new_world because we don't care about
     the default parameters, we only want the name and file */
  world = g_new0(World, 1);
  world->logon_characters = GTK_TREE_MODEL(gtk_list_store_new(N_LOGON_COLUMNS,
                                                              G_TYPE_STRING,
                                                              G_TYPE_STRING));

  if (!load_world_from_file(file, world, TRUE, &error)) {
    fprintf(stderr, "Error loading %s: %s\n",
            file, error->message);
    g_error_free(error);
    return;
  }

  add_saved_worlds_model_entries(model, world, &worlditer);

  /* See if this was the last opened world */
  if (!(*last_open_iter) &&
      globalPrefs.last_open_world &&
      strcmp(world->name, globalPrefs.last_open_world) == 0) {
    *last_open_iter = g_new(GtkTreeIter, 1);
    **last_open_iter = worlditer;
  }

  g_free(world->name);
  g_free(world->file);
  g_object_unref(world->logon_characters);
  g_free(world);
}


static
void
add_saved_worlds_model_entries(GtkTreeStore *model,
                               World        *world,
                               GtkTreeIter  *worlditer)
{
  /* Fills worlditer with the iter pointing to the world. */
  gint n_chars;

  n_chars = gtk_tree_model_iter_n_children(world->logon_characters, NULL);

  /* Add world main entry */
  gtk_tree_store_append(model, worlditer, NULL);
  gtk_tree_store_set(model, worlditer,
                     COLUMN_NAME, world->name,
                     COLUMN_FILE, world->file,
                     COLUMN_INDEX, n_chars ? 0 : -1,
                     -1);

  /* Add sub-entries for each of the characters */
  if (n_chars > 0) {
    GtkTreeIter chariter;
    GtkTreeIter subworlditer;
    gint        idx;

    gtk_tree_model_get_iter_first(world->logon_characters, &chariter);
    idx = 0;
    do {
      gchar *character;

      gtk_tree_model_get(world->logon_characters, &chariter,
                         LOGON_CHAR, &character,
                         -1);

      gtk_tree_store_append(model, &subworlditer, worlditer);
      gtk_tree_store_set(model, &subworlditer,
                         COLUMN_NAME, character,
                         COLUMN_FILE, world->file,
                         COLUMN_INDEX, idx,
                         -1);

      g_free(character);
      ++idx;
    } while (gtk_tree_model_iter_next(world->logon_characters, &chariter));

    /* Add an entry for logging without Auto-logon */
    gtk_tree_store_append(model, &subworlditer, worlditer);
    gtk_tree_store_set(model, &subworlditer,
                       COLUMN_NAME, "No auto-login",
                       COLUMN_FILE, world->file,
                       COLUMN_INDEX, -1,
                       -1);
  }
}


static
gint
sort_by_name(GtkTreeModel *model,
             GtkTreeIter  *a,
             GtkTreeIter  *b,
             gpointer      data)
{
  GtkTreePath *path_a;
  GtkTreePath *path_b;
  gchar       *name_a;
  gchar       *name_b;
  gint         ret;

  path_a = gtk_tree_model_get_path(model, a);
  path_b = gtk_tree_model_get_path(model, b);

  /* The check to see if path_a and path_b are not NULL is done to avoid
     a Gtk Warning that appears when we add something to a model already
     sorted. This might be a Gtk bug. */
  /* Only top-level entries are sorted */
  if (path_a && path_b && gtk_tree_path_get_depth(path_a) > 1) {
    ret = gtk_tree_path_compare(path_a, path_b);
  } else {
    gtk_tree_model_get(model, a, COLUMN_NAME, &name_a, -1);
    gtk_tree_model_get(model, b, COLUMN_NAME, &name_b, -1);
    ret = g_utf8_collate(name_a, name_b);
    g_free(name_a);
    g_free(name_b);
  }

  gtk_tree_path_free(path_a);
  gtk_tree_path_free(path_b);
  return ret;
}


World *
get_world_from_cmdline(const gchar *cmdline)
{
  gchar  *world_file;
  World  *world = NULL;
  static GRegex *parameters_regex = NULL;

  world_file = try_find_world_from_name(cmdline);

  if (world_file) {
    GError *error = NULL;

    world = create_new_world(TRUE);
    if (load_world_from_file(world_file,
                             world,
                             FALSE,
                             &error)) {
      g_free(world_file);
    } else {
      g_free(world_file);
      fprintf(stderr, _("Could not open world '%s': %s\n"),
              cmdline, error->message);
      g_error_free(error);
      free_world(world);
    }
  } else {
    GMatchInfo *matchinfo;
    gchar      *host;
    gchar      *port;
    gchar      *user;
    gchar      *password;

    GError *error = NULL;
    if (parameters_regex == NULL) {
      parameters_regex = g_regex_new(
        "^(telnet://)?"
          "((?<user>[a-zA-Z0-9_.+$!*'()\"-]+)"
           ":(?<password>[a-zA-Z0-9_.+$!*'()\"-]+)"
           "@)?"
          "(?<host>[a-zA-Z0-9.-]+)"
          "(:(?<port>\\d+))?"
          "/?$",
        G_REGEX_NO_AUTO_CAPTURE,
        0,
        &error);
      if (error) {
        fputs(error->message, stderr);
        g_error_free(error);
      }
    }

    if (g_regex_match(parameters_regex,
                      cmdline,
                      0,
                      &matchinfo)) {
      host     = g_match_info_fetch_named(matchinfo, "host");
      port     = g_match_info_fetch_named(matchinfo, "port");
      user     = g_match_info_fetch_named(matchinfo, "user");
      password = g_match_info_fetch_named(matchinfo, "password");

      g_match_info_free(matchinfo);
      world = create_new_world(TRUE);
      create_world_from_parameters(world, host, port, user, password);
    } else {
      g_match_info_free(matchinfo);
      fprintf(stderr, _("Invalid argument '%s'\n"), cmdline);
    }
  }

  return world;
}


#ifndef __MINGW32__
static
char *
try_find_world_from_name(const gchar *name)
{
  const gchar *kilddir;
  gchar       *globstr;
  glob_t       globbuf;
  char        *result = NULL;

  kilddir = get_kildclient_directory_path();
  globstr = g_strdup_printf("%s/*.wrl", kilddir);

  if (glob(globstr, GLOB_ERR | GLOB_NOSORT, 0, &globbuf) == 0) {
    int i;
    for (i = 0; !result && i < globbuf.gl_pathc; ++i) {
      result = compare_world_from_name(globbuf.gl_pathv[i], name);
    }
  }

  g_free(globstr);
  globfree(&globbuf);
  return result;
}
#else
static
char *
try_find_world_from_name(const gchar *name)
{
  const gchar     *kilddir;
  gchar           *globstr;
  gchar           *file;
  WIN32_FIND_DATA  FindFileData;
  HANDLE           hFind = INVALID_HANDLE_VALUE;
  char            *result = NULL;

  kilddir = get_kildclient_directory_path();
  globstr = g_strdup_printf("%s\\*.wrl", kilddir);

  hFind = FindFirstFile(globstr, &FindFileData);
  if (hFind != INVALID_HANDLE_VALUE) {
    file = g_strdup_printf("%s\\%s", kilddir, FindFileData.cFileName);
    result = compare_world_from_name(file, name);
    g_free(file);

    while (!result && FindNextFile(hFind, &FindFileData)) {
      file = g_strdup_printf("%s\\%s", kilddir, FindFileData.cFileName);
      result = compare_world_from_name(file, name);
      g_free(file);
    }

    FindClose(hFind);
  }

  g_free(globstr);
  return result;
}

#endif


static
char *
compare_world_from_name(const gchar *file, const gchar *name)
{
  /* We do not call create_new_world because we don't care about
     the default parameters, we only want the name and file */
  World *world = g_new0(World, 1);
  char  *result = NULL;

  if (load_world_from_file(file, world, TRUE, NULL)) {
    if (strcmp(world->name, name) == 0) {
      result = world->file;
    } else {
      g_free(world->file);
    }
    g_free(world->name);
    g_free(world);
  }

  return result;
}


void
set_manually_cb(GtkEditable *widget,
                gpointer user)
{
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radManually),
                               TRUE);
}


static
void
list_selection_changed_cb(GtkTreeSelection *selection,
                          gpointer data)
{
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(data), TRUE);
}


void
list_row_activated_cb(gpointer           data,
                      GtkTreeView       *treeview,
                      GtkTreePath       *treepath,
                      GtkTreeViewColumn *column)
{
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(data), TRUE);
  gtk_dialog_response(GTK_DIALOG(dlgSelectWorld), GTK_RESPONSE_OK);
}


static
void
add_edit_buttons_cb(GtkButton *btn, gpointer data)
{
  World        *world = NULL;
  GtkTreeIter   iter;
  GtkTreeModel *model;
  gboolean      newworld;

  if (GPOINTER_TO_INT(data)) {
    GtkTreeSelection *selection;
    char             *file;
    GError           *error = NULL;

    selection = gtk_tree_view_get_selection(lstWorlds);
    if (!gtk_tree_selection_get_selected(selection, &model, &iter))
      return;

    gtk_tree_model_get(model, &iter,
                       COLUMN_FILE, &file,
                       -1);

    world = create_new_world(FALSE);

    if (!load_world_from_file(file, world, FALSE, &error)) {
      kc_error_dialog(GTK_WINDOW(wndMain),
                      _("Could not load world from file '%s': %s"),
                      file, error->message);
      g_error_free(error);
      g_free(world);
      return;
    }

    make_startup_plugin_list_for_editing(world);
  } else {
    model = gtk_tree_view_get_model(lstWorlds);
  }

  if (edit_world(&world, GTK_WINDOW(dlgSelectWorld), &newworld, TRUE)) {
    GtkTreeIter worlditer;

    save_world_to_file(world);

    /* Remove entry, because it will be added again. */
    if (!newworld) {
      GtkTreeIter parent;
      GtkTreeIter child;

      if (!gtk_tree_model_iter_parent(model, &parent, &iter)) {
        parent = iter;
      }

      /* Remove the children */
      if (gtk_tree_model_iter_children(model, &child, &parent)) {
        while (gtk_tree_store_remove(GTK_TREE_STORE(model), &child))
          ;
      }

      gtk_tree_store_remove(GTK_TREE_STORE(model), &parent);
    }

    add_saved_worlds_model_entries(GTK_TREE_STORE(model),
                                   world,
                                   &worlditer);
  }
  free_world(world);
}


void
delete_button_cb(GtkButton *btn, gpointer data)
{
  GtkTreeSelection *selection;
  GtkTreeModel     *model;
  GtkTreeIter       iter;
  GtkTreeIter       parent;
  char             *name;
  char             *file;
  GtkWidget        *msgdlg;
  gchar            *msg;

  selection = gtk_tree_view_get_selection(lstWorlds);
  if (!gtk_tree_selection_get_selected(selection, &model, &iter))
    return;

  /* Make sure we point to the parent */
  if (!gtk_tree_model_iter_parent(model, &parent, &iter)) {
    parent = iter;
  }

  gtk_tree_model_get(model, &parent,
                     COLUMN_NAME, &name,
                     COLUMN_FILE, &file,
                     -1);

  msg = g_strdup_printf("world '%s'", name);
  msgdlg = we_guied_confirm_delete_dialog_new(GTK_WINDOW(wndMain),
                                              msg, 1);
  g_free(msg);

  if (gtk_dialog_run(GTK_DIALOG(msgdlg)) == GTK_RESPONSE_YES) {
    GtkTreeIter  child;
    char        *otherfile;

    unlink(file);

    /* four characters for the dot and extension, 1 for null terminator */
    otherfile = malloc(strlen(file) + 5);
    strcpy(otherfile, file);
    strcat(otherfile, ".hst");
    unlink(otherfile);

    strcpy(otherfile, file);
    strcat(otherfile, ".var");
    unlink(otherfile);
    g_free(otherfile);

    /* Remove the children */
    if (gtk_tree_model_iter_children(model, &child, &parent)) {
      while (gtk_tree_store_remove(GTK_TREE_STORE(model), &child))
        ;
    }

    gtk_tree_store_remove(GTK_TREE_STORE(model), &parent);
  }
  gtk_widget_destroy(msgdlg);

  g_free(file);
}


gint
disconnected_msgbox(gchar *msg, gboolean can_offline)
{
  gint resp;
  GtkWidget *dlg;

  dlg = gtk_message_dialog_new(GTK_WINDOW(wndMain),
                               GTK_DIALOG_MODAL
                               | GTK_DIALOG_DESTROY_WITH_PARENT,
                               GTK_MESSAGE_QUESTION,
                               GTK_BUTTONS_NONE,
                               "%s", msg);
  gtk_window_set_title(GTK_WINDOW(dlg), _("Disconnected"));
  if (can_offline) {
    gtk_dialog_add_buttons(GTK_DIALOG(dlg),
                           _("_Reconnect"), RESP_RECONNECT,
                           _("Connect to _another world"), RESP_OTHER,
                           _("_Offline"), RESP_OFFLINE,
                           _("_Close"),GTK_RESPONSE_CLOSE,
                           NULL);
  } else {
    gtk_dialog_add_buttons(GTK_DIALOG(dlg),
                           _("_Reconnect"), RESP_RECONNECT,
                           _("Connect to _another world"), RESP_OTHER,
                           _("_Close"), GTK_RESPONSE_CLOSE,
                           NULL);
  }

  gtk_widget_show_all(dlg);
  resp = gtk_dialog_run(GTK_DIALOG(dlg));
  gtk_widget_destroy(dlg);
  return resp;
}
