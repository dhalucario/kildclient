/*
 * SimoComboBox is meant to be an alternative combobox to the
 * standard GtkComboBox.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Author:
 * Todd A. Fisher (toddf@simosoftware.com)
 */

#include <string.h>

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <gtkspell/gtkspell.h>

#include "simocombobox.h"



enum {
  CLICKED,
  POPUP_DISPLAYED,
  POPUP_CLOSED,
  CHANGED,
  ACTIVATE,
  SIZE_CHANGED,
  MARK_SET,
  LAST_SIGNAL,
};

enum {
  PROP_0,
  PROP_MODEL,
  PROP_MAX_LINES,
};


struct _SimoComboBoxPrivate {
  /*** Model with the stored lines ***/
  GtkTreeModel       *model;

  /*** Main widget - Single line Entry ***/
  GtkWidget          *entry;
  GtkEntryCompletion *completion;

  /*** Main widget - Multi line Entry ***/
  GtkWidget          *scrolled_window;   /* holds TextView */
  GtkWidget          *txtview;
  GtkTextBuffer      *txtbuffer;
  GtkSpellChecker    *gtkspell;
  /* Return value of get_text for multi line widget - so that the
     user does not need to free it. */
  gchar              *ml_text;

  /*** Main widget - Popup button and height arrows ***/
  GtkWidget          *popup_button;  /* button to display popup window */
  GtkWidget          *vbox_arrows;

  /*** Popup window ***/
  GtkWidget          *popup_window;  /* the window that is dropped down when
                                        the button is clicked */
  GdkCursor          *hand;

  GtkTreeView        *treeview;
  GtkTreeViewColumn  *column;
  gint                visible_column;
  GtkTreePath        *current_path;    /* path to the active cell, can be NULL
                                          if no items are currently selected */
  /*** Properties ***/
  gint                n_lines;
  gint                max_lines;

  /*** IDs of signal handlers ***/
  gulong              arrow_sig;              /* arrow button sig */
  gulong              single_changed_id;
  gulong              multi_changed_id;

  /*** Size calculation ***/
  /* Line height for size calculation */
  gint                cached_line_height;
};


static GtkBinClass *parent_class = NULL;
static guint        combo_box_signals[LAST_SIGNAL] = {0, };


static void     simo_combo_box_class_init(SimoComboBoxClass *klass);
static void     simo_combo_box_init(SimoComboBox *combo_box);
static void     simo_combo_box_create_model(SimoComboBox *combo_box);
static void     simo_combo_box_create_widget(SimoComboBox *combo_box);
static void     simo_combo_box_create_popup_window(SimoComboBox *combo_box);
static void     simo_combo_box_finalize(GObject *object);

static void     simo_combo_box_get_property(GObject    *object,
                                            guint       param_id,
                                            GValue     *value,
                                            GParamSpec *pspec);
static void     simo_combo_box_set_property(GObject      *object,
                                            guint         param_id,
                                            const GValue *value,
                                            GParamSpec   *pspec);

static void     simo_combo_box_get_preferred_height(GtkWidget *widget,
                                                    gint      *minimum_height,
                                                    gint      *natural_height);
static void     simo_combo_box_get_preferred_width(GtkWidget *widget,
                                                   gint      *minimum_width,
                                                   gint      *natural_width);
static void     simo_combo_box_size_request(GtkWidget      *widget,
                                            GtkRequisition *requisition);
static gint     simo_combo_box_get_line_height(SimoComboBox *combo_box);
static void     simo_combo_box_size_allocate(GtkWidget     *widget,
                                             GtkAllocation *allocation);

static void     simo_combo_box_grab_focus_signal(GtkWidget *widget);

static gboolean simo_combo_box_update_pressed(GtkWidget      *widget,
                                              GdkEventButton *event,
                                              SimoComboBox   *combo_box);
static void     simo_combo_box_row_activated(GtkTreeView       *treeview,
                                             GtkTreePath       *arg1,
                                             GtkTreeViewColumn *arg2,
                                             SimoComboBox      *combo);
static void     simo_combo_box_row_deleted(GtkTreeModel *model,
                                           GtkTreePath  *path,
                                           SimoComboBox *combo);
static gboolean simo_combo_box_treeview_release_event(GtkWidget      *widget,
                                                      GdkEventButton *event,
                                                      SimoComboBox   *combo_box);
static gboolean simo_combo_box_treeview_motion_event(GtkWidget      *widget,
                                                     GdkEventMotion *event,
                                                     SimoComboBox   *combo_box);
static gboolean simo_combo_box_key_release_event(GtkWidget    *widget,
                                                 GdkEventKey  *event,
                                                 SimoComboBox *combo_box);

static gboolean simo_combo_box_arrow_up_cb(GtkWidget      *widget,
                                           GdkEventButton *event,
                                           gpointer        data);
static gboolean simo_combo_box_arrow_down_cb(GtkWidget      *widget,
                                             GdkEventButton *event,
                                             gpointer        data);

static void     simo_combo_box_text_changed_cb(GtkEditable *editable,
                                               gpointer     data);
static void     simo_combo_box_mark_set_cb(GtkTextBuffer *textbuffer,
                                           GtkTextIter   *location,
                                           GtkTextMark   *mark,
                                           gpointer       data);
static void     simo_combo_box_entry_activate_cb(GtkEditable *editable,
                                                 gpointer     data);
static gboolean simo_combo_box_txtview_keypress_cb(GtkWidget   *widget,
                                                   GdkEventKey *evt,
                                                   gpointer     data);

static int      get_screen_height(GtkWidget *widget);

static void     simo_combo_box_popup_close(SimoComboBox *combo_box);

static gchar   *get_gvalue_as_string(GValue *value);



GType
simo_combo_box_get_type(void)
{
  static GType combo_box_type = 0;

  if (!combo_box_type) {
    static const GTypeInfo combo_box_info = {
      sizeof (SimoComboBoxClass),
      NULL, /* base_init */
      NULL, /* base_finalize */
      (GClassInitFunc) simo_combo_box_class_init,
      NULL, /* class_finalize */
      NULL, /* class_data */
      sizeof (SimoComboBox),
      0,
      (GInstanceInitFunc) simo_combo_box_init
    };

    combo_box_type = g_type_register_static(GTK_TYPE_BOX,
                                            "SimoComboBox",
                                            &combo_box_info,
                                            (GTypeFlags) 0);
  }

  return combo_box_type;
}


static
void
simo_combo_box_class_init(SimoComboBoxClass *klass)
{
  GObjectClass      *g_object_class;
  GtkWidgetClass    *widget_class;

  g_object_class = G_OBJECT_CLASS (klass);
  widget_class   = (GtkWidgetClass *) klass;

  parent_class                       = g_type_class_peek_parent((GObjectClass *) klass);
  g_object_class->finalize           = simo_combo_box_finalize;
  g_object_class->set_property       = simo_combo_box_set_property;
  g_object_class->get_property       = simo_combo_box_get_property;
  widget_class->size_allocate        = simo_combo_box_size_allocate;
  widget_class->get_preferred_height = simo_combo_box_get_preferred_height;
  widget_class->get_preferred_width  = simo_combo_box_get_preferred_width;
  widget_class->grab_focus           = simo_combo_box_grab_focus_signal;

  klass->clicked = NULL;
  klass->popup_displayed = NULL;

  /* signals */
  combo_box_signals[CLICKED] = g_signal_new("clicked",
                                            G_OBJECT_CLASS_TYPE(widget_class),
                                            G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                                            G_STRUCT_OFFSET(SimoComboBoxClass, clicked),
                                            NULL, NULL,
                                            g_cclosure_marshal_VOID__VOID,
                                            G_TYPE_NONE, 0);
  combo_box_signals[POPUP_DISPLAYED] = g_signal_new("popup-displayed",
                                                    G_OBJECT_CLASS_TYPE(widget_class),
                                                    G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                                                    G_STRUCT_OFFSET(SimoComboBoxClass, popup_displayed),
                                                    NULL, NULL,
                                                    g_cclosure_marshal_VOID__VOID,
                                                    G_TYPE_NONE, 0);
  combo_box_signals[POPUP_CLOSED] = g_signal_new("popup-closed",
                                                 G_OBJECT_CLASS_TYPE(widget_class),
                                                 G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                                                 G_STRUCT_OFFSET(SimoComboBoxClass, popup_closed),
                                                 NULL, NULL,
                                                 g_cclosure_marshal_VOID__VOID,
                                                 G_TYPE_NONE, 0);
  combo_box_signals[CHANGED] = g_signal_new("changed",
                                            G_OBJECT_CLASS_TYPE(widget_class),
                                            G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                                            G_STRUCT_OFFSET(SimoComboBoxClass, changed),
                                            NULL, NULL,
                                            g_cclosure_marshal_VOID__VOID,
                                            G_TYPE_NONE, 0);
  combo_box_signals[ACTIVATE] = g_signal_new("activate",
                                            G_OBJECT_CLASS_TYPE(widget_class),
                                            G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                                            G_STRUCT_OFFSET(SimoComboBoxClass, activate),
                                            NULL, NULL,
                                            g_cclosure_marshal_VOID__VOID,
                                            G_TYPE_NONE, 0);
  combo_box_signals[SIZE_CHANGED] = g_signal_new("size_changed",
                                            G_OBJECT_CLASS_TYPE(widget_class),
                                            G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                                            G_STRUCT_OFFSET(SimoComboBoxClass, size_changed),
                                            NULL, NULL,
                                            g_cclosure_marshal_VOID__VOID,
                                            G_TYPE_NONE, 0);
  combo_box_signals[MARK_SET] = g_signal_new("mark_set",
                                             G_OBJECT_CLASS_TYPE(widget_class),
                                             G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
                                             G_STRUCT_OFFSET(SimoComboBoxClass, mark_set),
                                             NULL, NULL,
                                             g_cclosure_marshal_VOID__OBJECT,
                                             G_TYPE_NONE,
                                             1,
                                             GTK_TYPE_TEXT_MARK);

  /* properties */
  g_object_class_install_property(g_object_class,
                                  PROP_MODEL,
                                  g_param_spec_object("model",
                                                      "ComboBox model",
                                                      "The model for the combo box",
                                                      GTK_TYPE_TREE_MODEL,
                                                      G_PARAM_READWRITE));

  g_object_class_install_property(g_object_class,
                                  PROP_MAX_LINES,
                                  g_param_spec_int("max_lines",
                                                   "Maximum lines",
                                                   "Max number of lines allowed in the multi-line text editing widget",
                                                   1,
                                                   G_MAXINT,
                                                   10,
                                                   G_PARAM_READWRITE));
}


static
void
simo_combo_box_init(SimoComboBox *combo_box)
{
  SimoComboBoxPrivate *mthis;

  mthis = g_new0(SimoComboBoxPrivate, 1);
  combo_box->priv = mthis;

  mthis->n_lines = 1;
  mthis->max_lines = 10;

  simo_combo_box_create_model(combo_box);
  simo_combo_box_create_widget(combo_box);
  simo_combo_box_create_popup_window(combo_box);
}


static
void
simo_combo_box_create_model(SimoComboBox *combo_box)
{
  SimoComboBoxPrivate *mthis = combo_box->priv;

  mthis->model = GTK_TREE_MODEL(gtk_tree_store_new(1, G_TYPE_STRING));
}


static
void
simo_combo_box_create_widget(SimoComboBox *combo_box)
{
  SimoComboBoxPrivate *mthis = combo_box->priv;
  GtkWidget           *arrow;
  GtkWidget           *arrow_evtbox;

  /* Single line input box */
  mthis->entry = gtk_entry_new();
  gtk_widget_set_name(mthis->entry, "combo_entry");
  mthis->single_changed_id
    = g_signal_connect(G_OBJECT(mthis->entry), "changed",
                       G_CALLBACK(simo_combo_box_text_changed_cb), combo_box);
  g_signal_connect(G_OBJECT(mthis->entry), "activate",
                   G_CALLBACK(simo_combo_box_entry_activate_cb), combo_box);

  mthis->completion = gtk_entry_completion_new();
  gtk_entry_set_completion(GTK_ENTRY(mthis->entry), mthis->completion);
  g_object_unref(mthis->completion);
  gtk_entry_completion_set_model(mthis->completion, mthis->model);

  /* Multi line input box */
  mthis->txtview = gtk_text_view_new();
  gtk_text_view_set_pixels_above_lines(GTK_TEXT_VIEW(mthis->txtview), 2);
  gtk_text_view_set_left_margin(GTK_TEXT_VIEW(mthis->txtview), 2);
  gtk_text_view_set_right_margin(GTK_TEXT_VIEW(mthis->txtview), 2);
  gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(mthis->txtview),
                              GTK_WRAP_WORD_CHAR);
  g_signal_connect(G_OBJECT(mthis->txtview), "key-press-event",
                   G_CALLBACK(simo_combo_box_txtview_keypress_cb), combo_box);

  mthis->txtbuffer
    = gtk_text_view_get_buffer(GTK_TEXT_VIEW(mthis->txtview));
  mthis->multi_changed_id =
    g_signal_connect(G_OBJECT(mthis->txtbuffer), "changed",
                     G_CALLBACK(simo_combo_box_text_changed_cb), combo_box);
  g_signal_connect(G_OBJECT(mthis->txtbuffer), "mark-set",
                   G_CALLBACK(simo_combo_box_mark_set_cb), combo_box);

  mthis->scrolled_window = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(mthis->scrolled_window),
                                 GTK_POLICY_NEVER,
                                 GTK_POLICY_AUTOMATIC);
  gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(mthis->scrolled_window),
                                      GTK_SHADOW_IN);
  g_object_set(G_OBJECT(mthis->scrolled_window), "no-show-all", TRUE, NULL);
  gtk_container_add(GTK_CONTAINER(mthis->scrolled_window), mthis->txtview);

  /* Popup button */
  arrow = gtk_image_new_from_icon_name("pan-down-symbolic", GTK_ICON_SIZE_BUTTON);
  gtk_widget_set_name(arrow, "combo_arrow");

  mthis->popup_button = gtk_toggle_button_new();
  gtk_widget_set_name(mthis->popup_button, "combo_button");
  gtk_widget_set_focus_on_click(mthis->popup_button, FALSE);
  mthis->arrow_sig =
    g_signal_connect_swapped(G_OBJECT(mthis->popup_button), "toggled",
                             G_CALLBACK(simo_combo_box_popup), combo_box);
  gtk_container_add(GTK_CONTAINER(mthis->popup_button), arrow);

  /* Arrows for changing size */
  mthis->vbox_arrows = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
  gtk_box_set_homogeneous(GTK_BOX(mthis->vbox_arrows), TRUE);
  arrow = gtk_image_new_from_icon_name("pan-up-symbolic", GTK_ICON_SIZE_BUTTON);
  gtk_widget_set_valign(arrow, GTK_ALIGN_START);
  arrow_evtbox = gtk_event_box_new();
  g_signal_connect(G_OBJECT(arrow_evtbox), "button-press-event",
                   G_CALLBACK(simo_combo_box_arrow_up_cb), combo_box);
  gtk_container_add(GTK_CONTAINER(arrow_evtbox), arrow);
  gtk_box_pack_start(GTK_BOX(mthis->vbox_arrows), arrow_evtbox, TRUE, TRUE, 0);

  arrow = gtk_image_new_from_icon_name("pan-down-symbolic", GTK_ICON_SIZE_BUTTON);
  gtk_widget_set_valign(arrow, GTK_ALIGN_END);
  arrow_evtbox = gtk_event_box_new();
  g_signal_connect(G_OBJECT(arrow_evtbox), "button-press-event",
                   G_CALLBACK(simo_combo_box_arrow_down_cb), combo_box);
  gtk_container_add(GTK_CONTAINER(arrow_evtbox), arrow);
  gtk_box_pack_start(GTK_BOX(mthis->vbox_arrows), arrow_evtbox,
                     TRUE, TRUE, 0);

  gtk_box_pack_start(GTK_BOX(combo_box), mthis->entry, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(combo_box), mthis->scrolled_window, TRUE, TRUE, 0);
  gtk_box_pack_end(GTK_BOX(combo_box), mthis->vbox_arrows, FALSE, FALSE, 0);
  gtk_box_pack_end(GTK_BOX(combo_box), mthis->popup_button, FALSE, FALSE, 0);
}


static
void
simo_combo_box_create_popup_window(SimoComboBox *combo_box)
{
  SimoComboBoxPrivate *mthis = combo_box->priv;
  GtkCellRenderer     *cell;
  GtkWidget           *frame;
  GtkWidget           *popup_scroll;
  int                  mask;

  mthis->hand = gdk_cursor_new_for_display(gdk_display_get_default(),
                                           GDK_HAND2);

  mthis->visible_column = 0;

  /* Build the Tree View */
  mthis->treeview = GTK_TREE_VIEW(gtk_tree_view_new_with_model(mthis->model));
  g_object_set(G_OBJECT(mthis->treeview),
               "fixed-height-mode", FALSE,
               "headers-visible", FALSE,
               "reorderable", FALSE,
               NULL);

  g_signal_connect(G_OBJECT(mthis->treeview), "row-activated",
                   G_CALLBACK(simo_combo_box_row_activated), combo_box);
  g_signal_connect(G_OBJECT(mthis->treeview), "button-release-event",
                   G_CALLBACK(simo_combo_box_treeview_release_event),
                   combo_box);
  g_signal_connect(G_OBJECT(mthis->treeview), "key-release-event",
                   G_CALLBACK(simo_combo_box_key_release_event), combo_box);
  g_signal_connect(G_OBJECT(mthis->treeview), "motion-notify-event",
                   G_CALLBACK(simo_combo_box_treeview_motion_event), combo_box);

  mthis->column = gtk_tree_view_column_new();
  cell = gtk_cell_renderer_text_new();
  g_object_set(cell, "ellipsize", PANGO_ELLIPSIZE_END, NULL);
  gtk_tree_view_column_pack_start(mthis->column, cell, TRUE);
  gtk_tree_view_column_add_attribute(mthis->column, cell, "text", 0);
  gtk_tree_view_column_set_expand(mthis->column, TRUE);
  gtk_tree_view_column_set_sizing(mthis->column, GTK_TREE_VIEW_COLUMN_FIXED);

  gtk_tree_view_append_column(mthis->treeview, mthis->column);

  popup_scroll = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(popup_scroll),
                                 GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
  gtk_container_set_border_width(GTK_CONTAINER(popup_scroll), 0);

  gtk_container_add(GTK_CONTAINER(popup_scroll), GTK_WIDGET(mthis->treeview));

  frame = gtk_frame_new(NULL);
  gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_OUT);
  gtk_container_add(GTK_CONTAINER(frame), popup_scroll);

  // build the popup window
  mthis->popup_window = gtk_window_new(GTK_WINDOW_POPUP);
  gtk_widget_set_name(mthis->popup_window, "white");
  // set up the popup window with the appropriate attributes so that it works correctly as a popup window
  gtk_window_set_type_hint(GTK_WINDOW(mthis->popup_window),
                           GDK_WINDOW_TYPE_HINT_MENU);
  gtk_window_set_keep_above(GTK_WINDOW(mthis->popup_window), TRUE);
  gtk_window_set_decorated(GTK_WINDOW(mthis->popup_window), FALSE);
  gtk_window_set_resizable(GTK_WINDOW(mthis->popup_window), TRUE);
  gtk_container_set_border_width(GTK_CONTAINER(mthis->popup_window), 0);
  gtk_widget_realize(mthis->popup_window);
  gdk_window_set_cursor(gtk_widget_get_window(mthis->popup_window),
                        mthis->hand);
  mask = gdk_window_get_events(gtk_widget_get_window(mthis->popup_window));
  gdk_window_set_events(gtk_widget_get_window(mthis->popup_window),
                        mask | GDK_BUTTON_PRESS_MASK
                        | GDK_BUTTON_RELEASE_MASK
                        | GDK_KEY_PRESS_MASK
                        | GDK_KEY_RELEASE_MASK);

  gtk_container_add(GTK_CONTAINER(mthis->popup_window), frame);

}


GtkWidget *
simo_combo_box_new()
{
  return g_object_new(simo_combo_box_get_type(), NULL);
}


GtkWidget *
simo_combo_box_new_with_model(GtkTreeModel *model)
{
  SimoComboBox *cb = (SimoComboBox *) g_object_new(simo_combo_box_get_type(),
                                                   NULL);

  simo_combo_box_set_model(cb, model);

  return (GtkWidget *) cb;
}


static
void
simo_combo_box_finalize(GObject *object)
{
  SimoComboBoxPrivate *mthis;
  mthis = SIMO_COMBO_BOX(object)->priv;

  // run parent finalize
  G_OBJECT_CLASS(parent_class)->finalize(object);

  if (mthis->current_path) {
    gtk_tree_path_free(mthis->current_path);
  }

  g_object_unref(mthis->hand);

  g_free(mthis->ml_text);

  g_free(mthis);
}


static
void
simo_combo_box_get_property(GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  SimoComboBox *combo_box = SIMO_COMBO_BOX(object);

  switch (prop_id) {
  case PROP_MODEL:
    g_value_set_object(value, combo_box->priv->model);
    break;

  case PROP_MAX_LINES:
    g_value_set_int(value, combo_box->priv->max_lines);
    break;

  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    break;
  }
}


static
void
simo_combo_box_set_property(GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  SimoComboBox *combo_box = SIMO_COMBO_BOX(object);

  switch (prop_id) {
  case PROP_MODEL: {
    GtkTreeModel *model = g_value_get_object(value);
    simo_combo_box_set_model(combo_box, model);
  }
    break;

  case PROP_MAX_LINES:
    combo_box->priv->max_lines = g_value_get_int(value);
    break;

  default:
    break;
  }
}


static
void
simo_combo_box_get_preferred_height(GtkWidget *widget,
                                    gint      *minimum_height,
                                    gint      *natural_height)
{
  GtkRequisition req;

  simo_combo_box_size_request(widget, &req);
  *minimum_height = req.height;
  *natural_height = req.height;
}


static
void
simo_combo_box_get_preferred_width(GtkWidget *widget,
                                   gint      *minimum_width,
                                   gint      *natural_width)
{
  GtkRequisition req;

  simo_combo_box_size_request(widget, &req);
  *minimum_width = req.width;
  *natural_width = req.width;
}


static
void
simo_combo_box_size_request(GtkWidget *widget, GtkRequisition *req)
{
  SimoComboBox *combo_box = SIMO_COMBO_BOX(widget);
  GtkRequisition breq, ereq, areq;

  if (combo_box->priv->n_lines > 1) {
    gtk_widget_get_preferred_size(combo_box->priv->scrolled_window, NULL, &ereq);
  } else {
    gtk_widget_get_preferred_size(combo_box->priv->entry, NULL, &ereq);
  }
  gtk_widget_get_preferred_size(combo_box->priv->popup_button, NULL, &breq);
  gtk_widget_get_preferred_size(combo_box->priv->vbox_arrows, NULL, &areq);

  ereq.height = breq.height;

  if (combo_box->priv->n_lines > 1) {
    req->height = simo_combo_box_get_line_height(combo_box) * combo_box->priv->n_lines + 8;
  } else {
    req->height = breq.height;
  }
  req->width  = ereq.width + breq.width + areq.width;

  gtk_widget_set_size_request(combo_box->priv->entry, ereq.width, ereq.height);
  gtk_widget_set_size_request(combo_box->priv->scrolled_window, ereq.width, ereq.height);
}


static
gint
simo_combo_box_get_line_height(SimoComboBox *combo_box)
{
  if (combo_box->priv->cached_line_height == 0) {
    PangoLayout    *layout;
    PangoRectangle  logical_rect;

    layout
      = gtk_widget_create_pango_layout(GTK_WIDGET(combo_box->priv->txtview),
                                       "W");
    pango_layout_get_pixel_extents(layout, NULL, &logical_rect);
    combo_box->priv->cached_line_height = logical_rect.height;
    g_object_unref(layout);
  }

  return combo_box->priv->cached_line_height;
}


static
void
simo_combo_box_size_allocate(GtkWidget *widget, GtkAllocation *allocation)
{
  SimoComboBox  *combo;
  GtkAllocation  ealloc;
  GtkAllocation  combobtnAlloc;
  GtkAllocation  vboxarrowsAlloc;

  g_return_if_fail(SIMO_IS_COMBO_BOX(widget));
  g_return_if_fail(allocation != NULL);

  combo = SIMO_COMBO_BOX(widget);

  GTK_WIDGET_CLASS(parent_class)->size_allocate(widget, allocation);

  gtk_widget_get_allocation(combo->priv->popup_button, &combobtnAlloc);
  gtk_widget_get_allocation(combo->priv->vbox_arrows, &vboxarrowsAlloc);

  ealloc.x = allocation->x;
  ealloc.y = allocation->y;
  ealloc.width = allocation->width
    - combobtnAlloc.width
    - vboxarrowsAlloc.width;
  ealloc.height = allocation->height;

  if (combo->priv->n_lines > 1) {
    gtk_widget_size_allocate(combo->priv->scrolled_window, &ealloc);
  } else {
    gtk_widget_size_allocate(combo->priv->entry, &ealloc);
  }
}


static
void
simo_combo_box_grab_focus_signal(GtkWidget *widget)
{
  SimoComboBox *combo_box = SIMO_COMBO_BOX(widget);

  if (combo_box->priv->n_lines > 1) {
    gtk_widget_grab_focus(combo_box->priv->txtview);
  } else {
    gtk_widget_grab_focus(combo_box->priv->entry);
  }
}


GtkTreeModel *
simo_combo_box_get_model(SimoComboBox *combo_box)
{
  return combo_box->priv->model;
}


void
simo_combo_box_set_model(SimoComboBox *combo_box, GtkTreeModel *model)
{
  if (combo_box->priv->model) {
    g_object_unref(combo_box->priv->model);
  }

  combo_box->priv->model = model;

  gtk_tree_view_set_model(combo_box->priv->treeview, combo_box->priv->model);
  if (combo_box->priv->model) {
    g_object_ref(combo_box->priv->model);
    g_signal_connect(combo_box->priv->model, "row-deleted",
                     G_CALLBACK(simo_combo_box_row_deleted), combo_box);

    gtk_entry_completion_set_model(combo_box->priv->completion,
                                   combo_box->priv->model);
  }
}


void
simo_combo_box_set_combo_column(SimoComboBox      *combo_box,
                                GtkTreeViewColumn *column)
{
  gtk_tree_view_remove_column(combo_box->priv->treeview,
                              combo_box->priv->column);
  gtk_tree_view_append_column(combo_box->priv->treeview, column);
  combo_box->priv->column = column;
}


const gchar *
simo_combo_box_get_text(SimoComboBox *combo_box)
{
  if (combo_box->priv->n_lines > 1) {
    GtkTextIter start;
    GtkTextIter end;

    g_free(combo_box->priv->ml_text);
    gtk_text_buffer_get_bounds(combo_box->priv->txtbuffer, &start, &end);
    combo_box->priv->ml_text = gtk_text_buffer_get_text(combo_box->priv->txtbuffer,
                                                        &start,
                                                        &end,
                                                        FALSE);
    return combo_box->priv->ml_text;
  } else {
    return gtk_entry_get_text(GTK_ENTRY(combo_box->priv->entry));
  }
}


void
simo_combo_box_set_text(SimoComboBox *combo_box, const gchar *text)
{
  if (combo_box->priv->n_lines > 1) {
    gtk_text_buffer_set_text(combo_box->priv->txtbuffer, text, -1);
  } else {
    gtk_entry_set_text(GTK_ENTRY(combo_box->priv->entry), text);
  }
}


void
simo_combo_box_clear_text(SimoComboBox *combo_box)
{
  simo_combo_box_set_text(combo_box, "");
}


void
simo_combo_box_set_position(SimoComboBox *combo_box, gint position)
{
  if (combo_box->priv->n_lines > 1) {
    GtkTextIter iter;

    gtk_text_buffer_get_iter_at_offset(combo_box->priv->txtbuffer,
                                       &iter, position);
    gtk_text_buffer_place_cursor(combo_box->priv->txtbuffer, &iter);
  } else {
    gtk_editable_set_position(GTK_EDITABLE(combo_box->priv->entry), position);
  }
}


gboolean
simo_combo_box_get_selection_bounds(SimoComboBox *combo_box,
                                    gint         *start,
                                    gint         *end)
{
  if (combo_box->priv->n_lines > 1) {
    GtkTextIter start_iter;
    GtkTextIter end_iter;
    gboolean    retval;

    retval = gtk_text_buffer_get_selection_bounds(combo_box->priv->txtbuffer,
                                                  &start_iter, &end_iter);
    if (start) {
      *start = gtk_text_iter_get_offset(&start_iter);
    }
    if (end) {
      *end = gtk_text_iter_get_offset(&end_iter);
    }
    return retval;
  } else {
    return gtk_editable_get_selection_bounds(GTK_EDITABLE(combo_box->priv->entry),
                                             start, end);
  }
}


void
simo_combo_box_select_region(SimoComboBox *combo_box,
                             gint          start,
                             gint          end)
{
  if (combo_box->priv->n_lines > 1) {
    GtkTextIter start_iter;
    GtkTextIter end_iter;

    gtk_text_buffer_get_iter_at_offset(combo_box->priv->txtbuffer,
                                       &start_iter, start);
    gtk_text_buffer_get_iter_at_offset(combo_box->priv->txtbuffer,
                                       &end_iter, end);
    gtk_text_buffer_select_range(combo_box->priv->txtbuffer,
                                 &start_iter, &end_iter);
  } else {
    gtk_editable_select_region(GTK_EDITABLE(combo_box->priv->entry),
                               start, end);
  }
}


void
simo_combo_box_delete_selection(SimoComboBox *combo_box)
{
  if (combo_box->priv->n_lines > 1) {
    gtk_text_buffer_delete_selection(combo_box->priv->txtbuffer, FALSE, TRUE);
  } else {
    gtk_editable_delete_selection(GTK_EDITABLE(combo_box->priv->entry));
  }
}


void
simo_combo_box_cut_clipboard(SimoComboBox *combo_box)
{
  if (combo_box->priv->n_lines > 1) {
    GdkDisplay   *display;
    GtkClipboard *clipboard;

    display = gtk_widget_get_display(GTK_WIDGET(combo_box));

    clipboard = gtk_clipboard_get_for_display(display,
                                              GDK_SELECTION_CLIPBOARD);
    gtk_text_buffer_cut_clipboard(combo_box->priv->txtbuffer, clipboard, TRUE);
  } else {
    gtk_editable_cut_clipboard(GTK_EDITABLE(combo_box->priv->entry));
  }
}


void
simo_combo_box_copy_clipboard(SimoComboBox *combo_box)
{
  if (combo_box->priv->n_lines > 1) {
    GdkDisplay   *display;
    GtkClipboard *clipboard;

    display = gtk_widget_get_display(GTK_WIDGET(combo_box));

    clipboard = gtk_clipboard_get_for_display(display,
                                              GDK_SELECTION_CLIPBOARD);
    gtk_text_buffer_copy_clipboard(combo_box->priv->txtbuffer, clipboard);
  } else {
    gtk_editable_copy_clipboard(GTK_EDITABLE(combo_box->priv->entry));
  }
}


void
simo_combo_box_paste_clipboard(SimoComboBox *combo_box)
{
  if (combo_box->priv->n_lines > 1) {
    GdkDisplay   *display;
    GtkClipboard *clipboard;

    display = gtk_widget_get_display(GTK_WIDGET(combo_box));

    clipboard = gtk_clipboard_get_for_display(display,
                                              GDK_SELECTION_CLIPBOARD);
    gtk_text_buffer_paste_clipboard(combo_box->priv->txtbuffer, clipboard,
                                    NULL, TRUE);
  } else {
    gtk_editable_paste_clipboard(GTK_EDITABLE(combo_box->priv->entry));
  }
}


gboolean
simo_combo_box_get_active_iter(SimoComboBox *combo_box, GtkTreeIter *iter)
{
  if (combo_box->priv->current_path) {
    return gtk_tree_model_get_iter(combo_box->priv->model, iter,
                                   combo_box->priv->current_path);
  } else {
    return FALSE;
  }
}


void
simo_combo_box_set_active_iter(SimoComboBox *combo_box, GtkTreeIter *iter)
{
  SimoComboBoxPrivate *mthis = combo_box->priv;
  GValue               value = { 0 };
  gchar               *text  = NULL;

  if (mthis->current_path) {
    gtk_tree_path_free(mthis->current_path);
  }

  mthis->current_path = gtk_tree_model_get_path(mthis->model, iter);
  if (mthis->current_path) {
    gtk_tree_model_get_value(mthis->model, iter, mthis->visible_column, &value);
  }

  if (G_IS_VALUE(&value)) {
    text = get_gvalue_as_string(&value);
  }

  if (text) {
    gtk_entry_set_text(GTK_ENTRY(mthis->entry), text);
    g_free(text);
  } else {
    gtk_entry_set_text(GTK_ENTRY(mthis->entry), "");
  }

  g_value_unset(&value);
}


gint
simo_combo_box_get_n_lines(SimoComboBox *combo_box)
{
  return combo_box->priv->n_lines;
}


void
simo_combo_box_set_n_lines(SimoComboBox *combo_box, gint n_lines)
{
  gint        start;
  gint        end;
  GtkTextIter start_iter;
  GtkTextIter end_iter;

  if (n_lines == combo_box->priv->n_lines || n_lines <= 0) {
    return;
  }

  if (combo_box->priv->n_lines == 1
      || (combo_box->priv->n_lines == 2 && n_lines == 1)) {
    if (n_lines > 1) {
      g_object_set(G_OBJECT(combo_box->priv->entry), "no-show-all", TRUE, NULL);
      g_object_set(G_OBJECT(combo_box->priv->scrolled_window),
                   "no-show-all", FALSE, NULL);

      gtk_widget_hide(combo_box->priv->entry);
      gtk_widget_show_all(combo_box->priv->scrolled_window);

      /* Copy text and selected part */
      g_signal_handler_block(G_OBJECT(combo_box->priv->entry),
                             combo_box->priv->single_changed_id);
      gtk_text_buffer_set_text(combo_box->priv->txtbuffer,
                               gtk_entry_get_text(GTK_ENTRY(combo_box->priv->entry)),
                               -1);

      gtk_editable_get_selection_bounds(GTK_EDITABLE(combo_box->priv->entry),
                                        &start, &end);
      gtk_text_buffer_get_iter_at_offset(combo_box->priv->txtbuffer,
                                         &start_iter, start);
      gtk_text_buffer_get_iter_at_offset(combo_box->priv->txtbuffer,
                                         &end_iter, end);
      gtk_text_buffer_select_range(combo_box->priv->txtbuffer,
                                   &start_iter, &end_iter);

      g_signal_handler_unblock(G_OBJECT(combo_box->priv->entry),
                               combo_box->priv->single_changed_id);
    } else {
      gchar *text;

      g_object_set(G_OBJECT(combo_box->priv->entry),
                   "no-show-all", FALSE, NULL);
      g_object_set(G_OBJECT(combo_box->priv->scrolled_window),
                   "no-show-all", TRUE, NULL);

      gtk_widget_hide(combo_box->priv->scrolled_window);
      gtk_widget_show(combo_box->priv->entry);

      /* Copy text and selected part */
      g_signal_handler_block(G_OBJECT(combo_box->priv->txtbuffer),
                             combo_box->priv->multi_changed_id);
      gtk_text_buffer_get_bounds(combo_box->priv->txtbuffer,
                                 &start_iter, &end_iter);
      text = gtk_text_buffer_get_text(combo_box->priv->txtbuffer,
                                      &start_iter, &end_iter,
                                      FALSE);
      gtk_entry_set_text(GTK_ENTRY(combo_box->priv->entry), text);
      g_free(text);

      gtk_text_buffer_get_selection_bounds(combo_box->priv->txtbuffer,
                                           &start_iter, &end_iter);
      start = gtk_text_iter_get_offset(&start_iter);
      end   = gtk_text_iter_get_offset(&end_iter);
      gtk_editable_select_region(GTK_EDITABLE(combo_box->priv->entry),
                                 start, end);
      g_signal_handler_unblock(G_OBJECT(combo_box->priv->txtbuffer),
                               combo_box->priv->multi_changed_id);
    }
  }

  combo_box->priv->n_lines = n_lines;
  gtk_widget_queue_resize(GTK_WIDGET(combo_box));
  g_signal_emit(combo_box, combo_box_signals[SIZE_CHANGED], 0);
}


void
simo_combo_box_set_visibility(SimoComboBox *combo_box, gboolean visible)
{
  gtk_entry_set_visibility(GTK_ENTRY(combo_box->priv->entry), visible);
}


GtkWidget *
simo_combo_box_get_single_line_widget(SimoComboBox *combo)
{
  return combo->priv->entry;
}


GtkWidget *
simo_combo_box_get_multi_line_widget(SimoComboBox *combo)
{
  return combo->priv->txtview;
}


gboolean
simo_combo_box_set_spell(SimoComboBox  *combo,
                         gboolean       use_spell,
                         gchar         *language,
                         GError       **error)
{
  if (combo->priv->gtkspell) {   /* We already have one */
    if (!use_spell) {
      gtk_spell_checker_detach(combo->priv->gtkspell);
      combo->priv->gtkspell = NULL;
      return TRUE;
    } else {
      return gtk_spell_checker_set_language(combo->priv->gtkspell,
                                            language, error);
    }
  } else { /* We don't have one */
    if (use_spell) {
      gboolean success;

      combo->priv->gtkspell = gtk_spell_checker_new();
      gtk_spell_checker_attach(combo->priv->gtkspell,
                               GTK_TEXT_VIEW(combo->priv->txtview));
      success = gtk_spell_checker_set_language(combo->priv->gtkspell,
                                               language, error);
      if (!success) {
        return FALSE;
      }
      return TRUE;
    } else {
      return TRUE;
    }
  }
}


GtkEntryCompletion *
simo_combo_box_get_completion(SimoComboBox *combo_box)
{
  return combo_box->priv->completion;
}


void
simo_combo_box_set_completion_text_column(SimoComboBox *combo_box, int col_id)
{
  combo_box->priv->visible_column = col_id;
  gtk_entry_completion_set_text_column(combo_box->priv->completion, col_id);
}


void
simo_combo_box_set_completion_renderer(SimoComboBox    *combo_box,
                                       GtkCellRenderer *renderer)
{
  gtk_cell_layout_clear(GTK_CELL_LAYOUT(combo_box->priv->completion));
  gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(combo_box->priv->completion),
                             renderer,
                             TRUE);
}


void
simo_combo_box_set_completion_cell_func(SimoComboBox          *combo_box,
                                        GtkCellRenderer       *renderer,
                                        GtkCellLayoutDataFunc  func,
                                        gpointer               user_data,
                                        GDestroyNotify         destroy)
{
  gtk_cell_layout_set_cell_data_func(GTK_CELL_LAYOUT(combo_box->priv->completion),
                                     renderer,
                                     func,
                                     user_data,
                                     destroy);
}


static
gboolean
simo_combo_box_update_pressed(GtkWidget      *widget,
                              GdkEventButton *event,
                              SimoComboBox   *combo_box)

{
  gdouble x, y;
  gint x1, x2, y1, y2;
  gint xoffset, yoffset;
  GtkAllocation alloc;

  if (event->button != 1) {
    return FALSE;
  }

  x = event->x_root;
  y = event->y_root;
  gdk_window_get_root_origin(gtk_widget_get_window(widget), &xoffset, &yoffset);

  gtk_widget_get_allocation(widget, &alloc);

  xoffset += alloc.x;
  yoffset += alloc.y;

  x1 = alloc.x + xoffset;
  y1 = alloc.y + yoffset;
  x2 = x1 + alloc.width;
  y2 = y1 + alloc.height;

  if (x > x1 && x < x2 && y > y1 && y < y2) {
    return TRUE;
  }

  simo_combo_box_popup_close(combo_box);

  return TRUE;
}


static
void
simo_combo_box_row_activated(GtkTreeView       *treeview,
                             GtkTreePath       *path,
                             GtkTreeViewColumn *col,
                             SimoComboBox      *combo)
{
  GtkTreeIter iter;

  gtk_tree_model_get_iter(combo->priv->model, &iter, path);
  simo_combo_box_set_active_iter(combo, &iter);

  simo_combo_box_popup_close(combo);
}


static
void
simo_combo_box_row_deleted(GtkTreeModel *model,
                           GtkTreePath  *path,
                           SimoComboBox *combo)
{
  if (combo->priv->current_path) {
    if (!gtk_tree_path_compare(path, combo->priv->current_path)) {
      gtk_tree_path_free(combo->priv->current_path);
      combo->priv->current_path = NULL;
    }
  }
}


static
gboolean
simo_combo_box_treeview_release_event(GtkWidget      *widget,
                                      GdkEventButton *event,
                                      SimoComboBox   *combo_box)
{
  GtkTreePath       *path;
  GtkTreeViewColumn *col;
  GtkAllocation      alloc;

  gtk_widget_get_allocation(widget, &alloc);

  if ((event->x > alloc.x)
      && (event->x < (alloc.x + alloc.width))
      && (event->y > alloc.y)
      && (event->y < (alloc.y + alloc.height))) {
    if (gtk_tree_view_get_path_at_pos(combo_box->priv->treeview,
                                      (gint) event->x, (gint) event->y,
                                       &path, &col, NULL, NULL)) {
      gtk_tree_view_row_activated(combo_box->priv->treeview, path, col);
      g_signal_emit(combo_box, combo_box_signals[CLICKED], 0);
    }
  }

  return FALSE;
}


static
gboolean
simo_combo_box_treeview_motion_event(GtkWidget      *widget,
                                     GdkEventMotion *event,
                                     SimoComboBox   *combo)
{
  GtkTreePath *path;

  if (gtk_tree_view_get_path_at_pos(combo->priv->treeview,
                                    (gint) event->x, (gint) event->y,
                                     &path, NULL, NULL, NULL)) {
    GtkTreeIter iter;
    if (gtk_tree_model_get_iter(combo->priv->model, &iter, path)) {
      GtkTreeSelection *selection
        = gtk_tree_view_get_selection(combo->priv->treeview);
      gtk_tree_selection_select_iter(selection, &iter);
    }
    gtk_tree_path_free(path);
  }

  return FALSE;
}


static
gboolean
simo_combo_box_key_release_event(GtkWidget    *widget,
                                 GdkEventKey  *event,
                                 SimoComboBox *combo_box)
{
  if(event->keyval == GDK_KEY_Escape) {
    simo_combo_box_popup_close(combo_box);
  }

  return FALSE;
}


static
gboolean
simo_combo_box_arrow_up_cb(GtkWidget      *widget,
                           GdkEventButton *event,
                           gpointer        data)
{
  SimoComboBox *combo_box = (SimoComboBox *) data;

  if (event->button != 1) {
    return FALSE;
  }

  if (combo_box->priv->n_lines == combo_box->priv->max_lines) {
    return TRUE;
  }

  simo_combo_box_set_n_lines(combo_box, combo_box->priv->n_lines + 1);
  if (combo_box->priv->n_lines == 2) {
    gtk_widget_grab_focus(combo_box->priv->txtview);
  }

  return TRUE;
}


static
gboolean
simo_combo_box_arrow_down_cb(GtkWidget      *widget,
                             GdkEventButton *event,
                             gpointer        data)
{
  SimoComboBox *combo_box = (SimoComboBox *) data;

  if (event->button != 1) {
    return FALSE;
  }

  if (combo_box->priv->n_lines == 1) {
    return TRUE;
  }

  simo_combo_box_set_n_lines(combo_box, combo_box->priv->n_lines - 1);
  if (combo_box->priv->n_lines == 1) {
    gtk_widget_grab_focus(combo_box->priv->entry);
  }

  return TRUE;
}


static
void
simo_combo_box_text_changed_cb(GtkEditable *editable, gpointer data)
{
  g_signal_emit(SIMO_COMBO_BOX(data), combo_box_signals[CHANGED], 0);
}


static
void
simo_combo_box_mark_set_cb(GtkTextBuffer *textbuffer,
                           GtkTextIter   *location,
                           GtkTextMark   *mark,
                           gpointer       data)
{
  g_signal_emit(SIMO_COMBO_BOX(data),
                combo_box_signals[MARK_SET],
                0,
                mark);
}


static
void
simo_combo_box_entry_activate_cb(GtkEditable *editable, gpointer data)
{
  g_signal_emit(SIMO_COMBO_BOX(data), combo_box_signals[ACTIVATE], 0);
}


static
gboolean
simo_combo_box_txtview_keypress_cb(GtkWidget   *widget,
                                   GdkEventKey *evt,
                                   gpointer     data)
{
  if ((evt->keyval == GDK_KEY_Return || evt->keyval == GDK_KEY_KP_Enter)
      && !(evt->state & GDK_MOD1_MASK)
      && !(evt->state & GDK_SHIFT_MASK)
      && !(evt->state & GDK_CONTROL_MASK)) {
    g_signal_emit(SIMO_COMBO_BOX(data), combo_box_signals[ACTIVATE], 0);
    return TRUE;
  }

  return FALSE;
}


void
simo_combo_box_popup(SimoComboBox *combo_box)
{
  gint bx, by;
  gint width, height;
  gint screen_height;
  GtkWidget *widget;
  GtkAllocation alloc;
  SimoComboBoxPrivate *mthis;

  g_return_if_fail(SIMO_IS_COMBO_BOX(combo_box));
  widget = GTK_WIDGET(combo_box);
  mthis = combo_box->priv;

  g_signal_emit(combo_box, combo_box_signals[POPUP_DISPLAYED], 0);

  g_signal_connect(G_OBJECT(mthis->popup_window), "button-release-event",
                   G_CALLBACK(simo_combo_box_update_pressed), combo_box);

  // Determine size of popup window
  if (mthis->n_lines > 1) {
    width = gtk_widget_get_allocated_width(mthis->scrolled_window);
  } else {
    width = gtk_widget_get_allocated_width(mthis->entry);
  }

  gtk_widget_get_allocation(mthis->popup_button, &alloc);
  width += alloc.width;

  height = 130; // Arbitrary

  // Determine popup window position
  gtk_widget_get_allocation(widget, &alloc);
  gdk_window_get_origin(gtk_widget_get_window(widget), &bx, &by);
  bx += alloc.x;
  by += alloc.y + alloc.height;

   // Display above the text box if there isn't space below
  screen_height = get_screen_height(widget);
  if (by + height > screen_height) {
    by -= (alloc.height + height);
  }

  gtk_tree_view_column_set_max_width(mthis->column, width);
  gtk_window_resize(GTK_WINDOW(mthis->popup_window), width, height);
  gtk_window_move(GTK_WINDOW(mthis->popup_window), bx, by);
  gtk_widget_show_all(mthis->popup_window);

  g_signal_handler_block(G_OBJECT(mthis->popup_button), mthis->arrow_sig);

  if (!gtk_widget_has_focus(GTK_WIDGET(mthis->treeview))) {
    gtk_widget_set_can_default(GTK_WIDGET(mthis->treeview), TRUE);
    gtk_widget_grab_focus(GTK_WIDGET(mthis->treeview));
  }

  gtk_grab_add(mthis->popup_window);

  // set toggle state on buttons
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mthis->popup_button), TRUE);
}


static
int
get_screen_height(GtkWidget *widget)
{
  GdkWindow    *window;
  GdkDisplay   *display;
  GdkMonitor   *monitor;
  GdkRectangle  rectangle;

  window = gtk_widget_get_window(widget);
  display = gdk_display_get_default();
  monitor = gdk_display_get_monitor_at_window(display, window);

  gdk_monitor_get_geometry(monitor, &rectangle);
  return rectangle.height;
}


static
void
simo_combo_box_popup_close(SimoComboBox *combo_box)
{
  g_signal_emit(combo_box, combo_box_signals[POPUP_CLOSED], 0);

  gtk_grab_remove(combo_box->priv->popup_window);

  g_signal_handlers_disconnect_by_func(G_OBJECT(combo_box->priv->popup_window),
                                       G_CALLBACK(simo_combo_box_update_pressed),
                                       combo_box);

  // set toggle state on buttons
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(combo_box->priv->popup_button),
                               FALSE);
  g_signal_handler_unblock(G_OBJECT(combo_box->priv->popup_button),
                           combo_box->priv->arrow_sig);

  gtk_widget_hide(GTK_WIDGET(combo_box->priv->popup_window));
  // the label should have focus after the combobox is clicked
  // make sure our widget has focus after being clicked on
  if (gtk_widget_get_can_focus(GTK_WIDGET(combo_box))) {
    if (gtk_widget_get_can_focus(combo_box->priv->entry)) {
      gtk_widget_grab_focus(GTK_WIDGET(combo_box->priv->entry));
    }
  }
}


static
gchar *
get_gvalue_as_string(GValue *value)
{
  GType type = G_VALUE_TYPE(value);

  switch (type) {
  case G_TYPE_BOOLEAN:
    return g_strdup_printf("%d", (int) g_value_get_boolean(value));
    break;
  case G_TYPE_INT:
    return g_strdup_printf("%d", g_value_get_int(value));
    break;
  case G_TYPE_FLOAT:
    return g_strdup_printf("%f", g_value_get_float(value));
    break;
  case G_TYPE_DOUBLE:
    return g_strdup_printf("%f", g_value_get_double(value));
    break;
  case G_TYPE_STRING:
    return g_value_dup_string(value);
    break;
  }
  return NULL;
}
