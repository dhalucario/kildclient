/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */


#ifndef __NET_H
#define __NET_H


/***************************
 * Telnet protocol defines *
 ***************************/
#define TELNET_IAC               255        /* 0xFF, 0377 */
#define TELNET_WILL              251        /* 0xFB, 0373 */
#define TELNET_WONT              252        /* 0xFC, 0374 */
#define TELNET_DO                253        /* 0xFD, 0375 */
#define TELNET_DONT              254        /* 0xFE, 0376 */
#define TELNET_SB                250        /* 0xFA, 0372 */
#define TELNET_SE                240        /* 0xF0, 0360 */
#define TELNET_GA                249        /* 0xF9, 0371 */

#define TELNET_OPT_ECHO          1          /* D'oh */
#define TELNET_OPT_NAWS          31         /* 0x1f, 0037 */

#define TELNET_OPT_TERMINAL_TYPE 24         /* 0x18, 0030 */
#define TELNET_TERMTYPE_IS       0
#define TELNET_TERMTYPE_SEND     1

#define TELNET_OPT_COMPRESS      85         /* 0x55, 0125, "U" */
#define TELNET_OPT_COMPRESS2     86         /* 0x56, 0126, "V" */

#define TELNET_OPT_GMCP          201        /* 0xC9, 0311 */
#define TELNET_OPT_MSDP          69         /* 0x45, 0105, "E" */


/**************
 * Data types *
 **************/
struct resolve_data_s
{
  World           *world;
  gchar           *host;
  gchar           *port;
  int              ai_family;
  struct addrinfo *addrinfo;
  gboolean       (*callback)(gpointer);
  int              ret;
};


/*********************************
 * Function prototypes - general *
 *********************************/
gpointer resolve_name_thread(gpointer data);
gboolean continue_connecting_cb(gpointer data);
gboolean perform_tls_handshake(World *world);
gboolean connected_to(World *world);
void     process_telnet_iac(World *world, int pos, int end);
void     start_mccp(World *world, int pos, int end);
void     end_mccp(World *world);

/* Send and receive data */
ssize_t kc_recv(World *world, void *buf, size_t s);
ssize_t kc_send(World *world, const void *buf, size_t s);


/*******************************
 * Function prototypes - proxy *
 *******************************/
void     proxy_info_free(proxy_settings *s);
void     proxy_info_copy(proxy_settings *source, proxy_settings *dest);
gboolean proxy_socks4_resolved_real_host_cb(gpointer data);
gboolean proxy_socks4_connected_to(World *world);
gboolean proxy_socks5_connected_to(World *world);
gboolean proxy_http_connected_to(World *world);




#endif
