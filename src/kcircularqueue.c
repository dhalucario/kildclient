/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#include <stdlib.h>
#include <string.h>
#include "kcircularqueue.h"


KCircularQueue *
k_circular_queue_new(size_t el_size, unsigned int how_many)
{
  KCircularQueue *q;

  q                 = (KCircularQueue *) calloc(sizeof(KCircularQueue), 1);
  q->data           = malloc(el_size * how_many);
  q->el_size        = el_size;
  q->allocated_size = how_many;
  q->free_until_end = how_many;

  return q;
}


void
k_circular_queue_free(KCircularQueue *q)
{
  free(q->data);
  free(q);
}


void
k_circular_queue_push_copies(KCircularQueue *q,
                             const void     *val,
                             unsigned int    n)
{
  /* Make sure there is enough space. */
  if ((q->allocated_size - q->size) < n) {
    _k_circular_queue_realloc(q, q->size + n);
  }

  /* Elements that are added after the last one until the end of the array. */
  while (q->free_until_end && n) {
    memcpy(q->data + q->first_free*q->el_size, val, q->el_size);
    ++q->size;
    if (--q->free_until_end == 0) {
      q->first_free = 0;
    } else {
      ++q->first_free;
    }
    --n;
  }

  /* Elements added starting at the first position of the array. */
  while (n) {
    memcpy(q->data + q->first_free*q->el_size, val, q->el_size);
    ++q->size;
    ++q->first_free;
    --n;
  }
}


void
k_circular_queue_pop(KCircularQueue *q, unsigned int n)
{
  q->size  -= n;
  if (q->first + n >= q->allocated_size) {
    unsigned int in_end = q->allocated_size - q->first;
    q->first = n - in_end;
    q->free_until_end = q->allocated_size - q->size - q->first;
  } else {
    q->first += n;
  }
}



/*************************************************************
 * Private functions                                         *
 *************************************************************/
void
_k_circular_queue_realloc(KCircularQueue *q, unsigned int new_size)
{
  char         *newdata;
  unsigned int  till_end;

  newdata = malloc(q->el_size * new_size);

  till_end = q->allocated_size - q->first;
  memcpy(newdata, q->data + q->first*q->el_size, till_end*q->el_size);
  if (till_end < q->size) {
    memcpy(newdata + till_end*q->el_size,
           q->data,
           (q->size - till_end)*q->el_size);
  }

  q->allocated_size = new_size;
  q->first          = 0;
  q->first_free     = q->size;
  q->free_until_end = q->allocated_size - q->size;

  free(q->data);
  q->data = newdata;
}


const void *
_k_circular_queue_nth(KCircularQueue *q, unsigned int i)
{
  unsigned int pos;

  pos = q->first + i;
  if (pos >= q->allocated_size) {
    unsigned int in_end = q->allocated_size - q->first;
    pos = i - in_end;
  }

  return q->data + pos*q->el_size;
}
