/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <stdlib.h>
#include <string.h>
#include "libintl-wrapper.h"
#include <glib.h>
#include <glib/gstdio.h>
#include <gtk/gtk.h>
#include <gmodule.h>
#ifdef __MINGW32__
#  include <windows.h>
#endif

#include "kildclient.h"
#include "perlscript.h"


/***********************
 * Function prototypes *
 ***********************/


#ifndef __MINGW32__
void
play_sound(const gchar *file)
{
  char *to_run;

  to_run = g_strdup_printf(globalPrefs.audio_player_command, file);

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-result"
  system(to_run);
#pragma GCC diagnostic pop
  g_free(to_run);
}
#else /* defined __MINGW32__ */
void
play_sound(const gchar *file)
{
  PlaySound(file, NULL, SND_FILENAME | SND_ASYNC);
}
#endif /* defined __MINGW32__ */
