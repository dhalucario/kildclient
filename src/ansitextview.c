/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include <time.h>
#include <stdarg.h>
#include <sys/types.h>
#include "libintl-wrapper.h"
#include <locale.h>
#include <gtk/gtk.h>

#include "kcircularqueue.h"

#include "ansi.h"
#include "kildclient.h"
#include "perlscript.h"


/***********************
 * Function prototypes *
 ***********************/
static void        ansitextview_append_string_internal(WorldGUI    *gui,
                                                       const gchar *str,
                                                       gboolean     add_nl);
static void        ansitextview_append_formatted(WorldGUI    *gui,
                                                 GtkTextIter *iter,
                                                 const gchar *text,
                                                 TAState     *ta_state,
                                                 GtkTextTag  *extra_tag);





void
queue_ansiparse_operation(World *world, AnsiParseOperation *operation)
{
  g_async_queue_push(world->ansiparse_operations, operation);
}


void
ansitextview_append_string(WorldGUI    *gui,
                           const gchar *str)
{
  /*
   * Append str (a null-terminated string) to the Buffer, in the default
   * color.
   */

  ansitextview_append_string_internal(gui, str, FALSE);
}


void
ansitextview_append_string_nl(WorldGUI    *gui,
                              const gchar *str)
{
  /*
   * Append str (a null-terminated string) to the Buffer, in the default
   * color, and a newline.
   */

  ansitextview_append_string_internal(gui, str, TRUE);
}


void
ansitextview_append_stringf(WorldGUI    *gui,
                            const gchar *format,
                            ...)
{
  /*
   * Formats a string using printf syntax and outputs it.
   */
  va_list  ap;
  gchar   *str;

  va_start(ap, format);
  str = g_strdup_vprintf(format, ap);
  ansitextview_append_string_internal(gui, str, FALSE);
  g_free(str);
}


void
ansitextview_append_stringf_nl(WorldGUI    *gui,
                               const gchar *format,
                               ...)
{
  /*
   * Formats a string using printf syntax and outputs it.
   */
  va_list  ap;
  gchar   *str;

  va_start(ap, format);
  str = g_strdup_vprintf(format, ap);
  ansitextview_append_string_internal(gui, str, TRUE);
  g_free(str);
}


void
ansitextview_append_string_with_fgcolor(WorldGUI    *gui,
                                        const gchar *str,
                                        gint         fg_color)
{
  /*
   * Append str (a null-terminated string) to the Buffer, using the
   * specified foreground color.
   */
  MainThreadOperation *operation;

  operation = g_new(MainThreadOperation, 1);
  operation->action = INSERT_AT_END;
  operation->text_insert.use_ta = FALSE;
  operation->text_insert.extra_tag = gui->ta.ansi_fore_tags[fg_color];
  operation->text_insert.text = g_strdup(str);
  queue_mainthread_operation(gui->world, operation);

  operation = g_new(MainThreadOperation, 1);
  operation->action = MOVE_LINESTART_MARK;
  queue_mainthread_operation(gui->world, operation);

  operation = g_new(MainThreadOperation, 1);
  operation->action = TEXT_ADDED;
  queue_mainthread_operation(gui->world, operation);
}


void
ansitextview_append_ansi_string_immediately(WorldGUI *gui,
                                            gchar *str,
                                            gsize len)
{
  /* For KCWin: Appends the string without queueing the operations,
   * but rather executing them immediately. */
  insert_ansi_string(gui, str, len,
                     INSERT_AT_END, process_mainthread_operation);
}


void
ansitextview_get_char_size(WorldGUI *gui,
                           gint     *char_height,
                           gint     *char_width)
{
  PangoLayout    *layout;
  PangoRectangle  logical_rect;

  layout = gtk_widget_create_pango_layout(GTK_WIDGET(gui->txtView), "W");
  pango_layout_get_pixel_extents(layout, NULL, &logical_rect);
  *char_height = logical_rect.height;
  *char_width  = logical_rect.width;

  g_object_unref(layout);
}


void
ansitextview_get_size(WorldGUI *gui, guint *lines, guint *cols)
{
  GdkRectangle rect;
  gint         char_height;
  gint         char_width;

  /* Get the size of the text view */
  gtk_text_view_get_visible_rect(gui->txtView, &rect);

  ansitextview_get_char_size(gui, &char_height, &char_width);
  *lines = rect.height / char_height;
  *cols  = rect.width / char_width;
}


void
ansitextview_update_color_tags(WorldGUI *gui, World *world)
{
  int i;

  if (!world) {
    fprintf(stderr, "BUG: update_color_tag without world.\n");
    return;
  }

  /* First time called, the tags have not been created yet. */
  if (!gui->ta.ansi_fore_tags) {
    /* Allocate space for colors */
    gui->ta.ansi_fore_tags = g_new0(GtkTextTag *, ANSI_TABLE_SIZE);
    gui->ta.ansi_back_tags = g_new0(GtkTextTag *, ANSI_TABLE_SIZE);
    for (i = 0; i < ANSI_TABLE_SIZE; ++i) {
      gui->ta.ansi_fore_tags[i] = gtk_text_buffer_create_tag(gui->txtBuffer,
                                                          NULL,
                                                          NULL);
      gui->ta.ansi_back_tags[i] = gtk_text_buffer_create_tag(gui->txtBuffer,
                                                          NULL,
                                                          NULL);
    }
  }

  /* Update normal foreground/background colors */
  for (i = 0; i < ANSI_N_COLORS; ++i) {
    g_object_set(G_OBJECT(gui->ta.ansi_fore_tags[i]),
                 "foreground-rgba", &world->ansicolors[i],
                 NULL);
    g_object_set(G_OBJECT(gui->ta.ansi_back_tags[i]),
                 "background-rgba", &world->ansicolors[i],
                 NULL);
  }
  for (i = ANSI_BOLD_BASE_IDX; i < (ANSI_BOLD_BASE_IDX + ANSI_N_COLORS); ++i) {
    g_object_set(G_OBJECT(gui->ta.ansi_fore_tags[i]),
                 "foreground-rgba", &world->ansicolors[i - ANSI_TABLE_OFFSET],
                 NULL);
    g_object_set(G_OBJECT(gui->ta.ansi_back_tags[i]),
                 "background-rgba", &world->ansicolors[i - ANSI_TABLE_OFFSET],
                 NULL);
  }

  /* Update default colors */
  g_object_set(G_OBJECT(gui->ta.ansi_fore_tags[ANSI_DEFAULT_COLOR_IDX]),
               "foreground-rgba", world->deffore,
               NULL);
  g_object_set(G_OBJECT(gui->ta.ansi_fore_tags[ANSI_BOLD_BASE_IDX + ANSI_DEFAULT_COLOR_IDX]),
               "foreground-rgba", world->defbold,
               NULL);

  g_object_set(G_OBJECT(gui->ta.ansi_back_tags[ANSI_DEFAULT_COLOR_IDX]),
               "background-rgba", world->defback,
               NULL);
  g_object_set(G_OBJECT(gui->ta.ansi_back_tags[ANSI_BOLD_BASE_IDX + ANSI_DEFAULT_COLOR_IDX]),
               "background-rgba", world->defboldback,
               NULL);

  /* Update default colors for reverse video */
  g_object_set(G_OBJECT(gui->ta.ansi_fore_tags[ANSI_REVERSE_DEFAULT_IDX]),
               "foreground-rgba", world->defback,
               NULL);
  g_object_set(G_OBJECT(gui->ta.ansi_fore_tags[ANSI_REVERSE_BOLD_DEFAULT_IDX]),
               "foreground-rgba", world->defboldback,
               NULL);

  g_object_set(G_OBJECT(gui->ta.ansi_back_tags[ANSI_REVERSE_DEFAULT_IDX]),
               "background-rgba", world->deffore,
               NULL);
  g_object_set(G_OBJECT(gui->ta.ansi_back_tags[ANSI_REVERSE_BOLD_DEFAULT_IDX]),
               "background-rgba", world->defbold,
               NULL);

  /* Update use bold state */
  for (i = ANSI_BOLD_BASE_IDX;
       i < (ANSI_BOLD_BASE_IDX + ANSI_N_COLORS + 1);
       ++i) {
    g_object_set(G_OBJECT(gui->ta.ansi_fore_tags[i]),
                 "weight", (world->usebold
                            ? PANGO_WEIGHT_BOLD
                            : PANGO_WEIGHT_NORMAL),
                 NULL);
  }
  g_object_set(G_OBJECT(gui->ta.ansi_fore_tags[ANSI_REVERSE_BOLD_DEFAULT_IDX]),
               "weight", (world->usebold
                          ? PANGO_WEIGHT_BOLD
                          : PANGO_WEIGHT_NORMAL),
               NULL);

  /* Update colors */
  configure_css(gui, world);
}


gboolean
ansitextview_prune_extra_lines(gpointer data)
{
  WorldGUI *gui = (WorldGUI *) data;
  gint      lines;

  if (!gui->world) {
    return TRUE;
  }

  lines = gtk_text_buffer_get_line_count(gui->txtBuffer);
  if (lines > gui->world->buffer_lines) {
    GtkTextIter start;
    GtkTextIter end;
    int         to_del;

    to_del = lines - gui->world->buffer_lines;

    gtk_text_buffer_get_start_iter(gui->txtBuffer, &start);
    gtk_text_buffer_get_iter_at_line(gui->txtBuffer, &end, to_del);
    gtk_text_buffer_delete(gui->txtBuffer, &start, &end);

    /* Remove times of the deleted lines */
    if (gui->line_times) {
      k_circular_queue_pop(gui->line_times, to_del);
      gui->world->deleted_lines += to_del;
    }
  }

  return TRUE;
}


gchar *
strip_ansi(const gchar *original, gsize len)
{
  /* This is to be called on complete strings, so parsing always
     starts in DATA mode */
  ANSIFSMState  state = ANSIFSM_DATA;
  gchar        *stripped;
  int           original_pos;
  int           stripped_pos;

  /* In the worst case, the result after processing will have the
     same size + a null string terminator. */
  stripped = g_malloc(len + 1);
  original_pos = 0;
  stripped_pos = 0;

  while (original_pos < len) {
    switch (state) {
    case ANSIFSM_DATA:
      if (original[original_pos] == ANSI_ESC) {
        state = ANSIFSM_ESC;
      } else {
        stripped[stripped_pos++] = original[original_pos];
      }
      break;

    case ANSIFSM_ESC:
      if (original[original_pos] == '[') {
        state = ANSIFSM_ANSISEQ;
      } else {
        stripped[stripped_pos++] = ANSI_ESC;        /* Add ESC again */
        stripped[stripped_pos++] = original[original_pos];
        state = ANSIFSM_DATA;
      }
      break;

    case ANSIFSM_ANSISEQ:
      if (isalpha(original[original_pos])) {
        state = ANSIFSM_DATA;
      }
      break;

    default:
      break;
    }
    ++original_pos;
  }

  stripped[stripped_pos] = '\0';

  return stripped;
}


void
ansitextview_op_insert_at_end(WorldGUI *gui, MainThreadOperation *operation)
{
  GtkTextIter end_iter;
  TAState     *ta = NULL;

  if (operation->text_insert.use_ta) {
    ta = &operation->text_insert.ta_state;
  }

  gtk_text_buffer_get_iter_at_offset(gui->txtBuffer,
                                     &end_iter, -1);
  ansitextview_append_formatted(gui, &end_iter,
                                operation->text_insert.text,
                                ta, operation->text_insert.extra_tag);
}


void
ansitextview_op_insert_at_mark(WorldGUI *gui, MainThreadOperation *operation)
{
  GtkTextIter  iter;
  TAState     *ta = NULL;

  if (operation->text_insert.use_ta) {
    ta = &operation->text_insert.ta_state;
  }

  gtk_text_buffer_get_iter_at_mark(gui->txtBuffer,
                                   &iter,
                                   gui->txtmark_linestart);
  ansitextview_append_formatted(gui, &iter,
                                operation->text_insert.text,
                                ta, operation->text_insert.extra_tag);
  gtk_text_buffer_move_mark(gui->txtBuffer,
                            gui->txtmark_linestart, &iter);
}


void
ansitextview_op_text_added(WorldGUI *gui, MainThreadOperation *operation)
{
  GtkAdjustment *adjustment;
  gint           lines;
  static gchar   msg[MAX_BUFFER];
  int            n;

  /* Update count of lines */
  lines = gtk_text_buffer_get_line_count(gui->txtBuffer);
  if (gui->lblLines) {
    gint totallines = gui->world->deleted_lines + lines;
    sprintf(msg, ngettext("%d line", "%d lines", totallines), totallines);
    gtk_label_set_text(gui->lblLines, msg);
  }

  /* Add time that these lines were processed */
  if (gui->line_times) {
    int    existing;
    time_t now;

    existing = k_circular_queue_size(gui->line_times);
    now = time(NULL);

    if (existing) {
      /* This last line that existed probably had something added.
         Update its time, unless this is the first line ever. */
      k_circular_queue_nth(gui->line_times, time_t, existing - 1) = now;
    }

    /* Add new times */
    n = lines - existing;
    if (n > 0) {
      k_circular_queue_push_copies(gui->line_times, &now, n);
    }
  }

  /* Adjust unread markers */
  if (gui->world && !gui->world->has_focus && !gui->world->has_unread_text) {
    gui->world->has_unread_text = TRUE;
    gtk_label_set_markup(gui->lblNotebook, gui->world->new_text_name);
    if (worlds_with_new_text != -1) {
      ++worlds_with_new_text;

      if (!window_has_focus) {
        adjust_window_title();
      }
    }
  }

  /* Scroll to end if option is marked or we are at the bottom */
  adjustment = gtk_scrolled_window_get_vadjustment(gui->scrolled_win);
  if ((gui->world && gui->world->scrollOutput)
      || gtk_adjustment_get_value(adjustment)
         == gtk_adjustment_get_upper(adjustment) - gtk_adjustment_get_page_size(adjustment)) {
    gtk_text_view_scroll_to_mark(gui->txtView,
                                 gui->txtmark_end,
                                 0,
                                 TRUE,
                                 0.5,
                                 1);
  }
}


void
ansitextview_op_clear_pending_line(WorldGUI            *gui,
                                   MainThreadOperation *operation)
{
  GtkTextIter  line_start;
  GtkTextIter  line_end;

  gtk_text_buffer_get_iter_at_mark(gui->txtBuffer, &line_start,
                                   gui->txtmark_linestart);
  gtk_text_buffer_get_iter_at_mark(gui->txtBuffer, &line_end,
                                   gui->txtmark_end);
  gtk_text_buffer_delete(gui->txtBuffer, &line_start, &line_end);
}


void
ansitextview_op_move_linestart_mark(WorldGUI *gui, MainThreadOperation *operation)
{
  GtkTextIter end;

  gtk_text_buffer_get_iter_at_offset(gui->txtBuffer, &end, -1);
  gtk_text_buffer_move_mark(gui->txtBuffer, gui->txtmark_linestart, &end);
}


void
ansitextview_op_apply_highlight(WorldGUI *gui, MainThreadOperation *operation)
{
  Trigger     *trigger;
  GtkTextIter  start;
  GtkTextIter  end;
  int          i;

  trigger = operation->highlight->trigger;
  gtk_text_buffer_get_iter_at_offset(gui->txtBuffer,
                                     &end, -1);
  gtk_text_iter_backward_line(&end);
  start = end;

  if (trigger->high_target != -1) {
    gtk_text_iter_set_line_offset(&start, operation->highlight->match_start);
    gtk_text_iter_set_line_offset(&end,   operation->highlight->match_end);
  } else {    /* The whole line */
    gtk_text_iter_forward_to_line_end(&end);
  }

  if (trigger->high_fg_color != -1) {
    /* Remove previous tags */
    for (i = 0; i < ANSI_TABLE_SIZE; ++i) {
      gtk_text_buffer_remove_tag(gui->txtBuffer,
                                 gui->ta.ansi_fore_tags[i],
                                 &start, &end);
    }
    gtk_text_buffer_apply_tag(gui->txtBuffer,
                              gui->ta.ansi_fore_tags[trigger->high_fg_color],
                              &start, &end);
  }

  if (trigger->high_bg_color != -1) {
    /* Remove previous tags */
    for (i = 0; i < ANSI_TABLE_SIZE; ++i) {
      gtk_text_buffer_remove_tag(gui->txtBuffer,
                                 gui->ta.ansi_back_tags[i],
                                 &start, &end);
    }
    gtk_text_buffer_apply_tag(gui->txtBuffer,
                              gui->ta.ansi_back_tags[trigger->high_bg_color],
                              &start, &end);
  }

  if (trigger->high_italic != -1) {
    gtk_text_buffer_remove_tag(gui->txtBuffer,
                               gui->ta.italics_tag,
                               &start, &end);
    if (trigger->high_italic) {
      gtk_text_buffer_apply_tag(gui->txtBuffer,
                                gui->ta.italics_tag,
                                &start, &end);
    }
  }

  if (trigger->high_strike != -1) {
    gtk_text_buffer_remove_tag(gui->txtBuffer,
                               gui->ta.strike_tag,
                               &start, &end);
    if (trigger->high_strike) {
      gtk_text_buffer_apply_tag(gui->txtBuffer,
                                gui->ta.strike_tag,
                                &start, &end);
    }
  }

  if (trigger->high_underline != -1) {
    gtk_text_buffer_remove_tag(gui->txtBuffer,
                               gui->ta.underline_tag,
                               &start, &end);
    gtk_text_buffer_remove_tag(gui->txtBuffer,
                               gui->ta.dblunderline_tag,
                               &start, &end);
    if (trigger->high_underline == 1) {
      gtk_text_buffer_apply_tag(gui->txtBuffer,
                                gui->ta.underline_tag,
                                &start, &end);
    }
    if (trigger->high_underline == 2) {
      gtk_text_buffer_apply_tag(gui->txtBuffer,
                                gui->ta.dblunderline_tag,
                                &start, &end);
    }
  }
}


static
void
ansitextview_append_string_internal(WorldGUI    *gui,
                                    const gchar *str,
                                    gboolean     add_nl)
{
  /*
   * Does the bulk work of ansitextview_append_string() and
   * ansitextview_append_string_nl(): Those functions just call this one
   * specifying whether a newline is to be added or not.
   */
  int str_length;
  MainThreadOperation *operation;

  operation = g_new(MainThreadOperation, 1);
  operation->action = INSERT_AT_MARK;
  operation->text_insert.use_ta = FALSE;
  operation->text_insert.extra_tag = NULL;

  str_length = strlen(str);
  operation->text_insert.text = g_strndup(str, str_length + 1);
  if (add_nl) {
    operation->text_insert.text[str_length] = '\n';
  }

  queue_mainthread_operation(gui->world, operation);

  operation = g_new(MainThreadOperation, 1);
  operation->action = TEXT_ADDED;
  queue_mainthread_operation(gui->world, operation);
}


static
void
ansitextview_append_formatted(WorldGUI    *gui,
                              GtkTextIter *iter,
                              const gchar *text,
                              TAState     *ta_state,
                              GtkTextTag  *extra_tag)
{
  gint        start_offset;
  GtkTextIter start_iter;
  gint        fg_color_idx;
  gint        bg_color_idx;

  start_offset = gtk_text_iter_get_offset(iter);
  gtk_text_buffer_insert(gui->txtBuffer, iter,
                         text, -1);

  gtk_text_buffer_get_iter_at_offset(gui->txtBuffer, &start_iter, start_offset);

  /* Any extra tag passed. */
  if (extra_tag) {
    gtk_text_buffer_apply_tag(gui->txtBuffer, extra_tag, &start_iter, iter);
  }

  if (ta_state == NULL) {
    return;
  }

  /* Foreground & background */
  fg_color_idx = ta_state->fg_base_idx + ta_state->fg_color;
  bg_color_idx = ta_state->bg_base_idx + ta_state->bg_color;

  if (ta_state->foreground_color_tag) {
    gtk_text_buffer_apply_tag(gui->txtBuffer,
                              ta_state->foreground_color_tag,
                              &start_iter,
                              iter);
  } else {
    gint idx_to_use;

    if (ta_state->reverse ^ ta_state->hidden) {
      if (bg_color_idx == ANSI_DEFAULT_COLOR_IDX) {
        idx_to_use = ANSI_REVERSE_DEFAULT_IDX;
      } else if (bg_color_idx == ANSI_DEFAULT_BOLD_COLOR_IDX) {
        idx_to_use = ANSI_REVERSE_BOLD_DEFAULT_IDX;
      } else {
        idx_to_use = bg_color_idx;
      }
    } else {
      idx_to_use = fg_color_idx;
    }
    gtk_text_buffer_apply_tag(gui->txtBuffer,
                              gui->ta.ansi_fore_tags[idx_to_use],
                              &start_iter,
                              iter);
  }

  if (ta_state->background_color_tag) {
    gtk_text_buffer_apply_tag(gui->txtBuffer,
                              ta_state->background_color_tag,
                              &start_iter,
                              iter);
  } else {
    gint idx_to_use;

    if (!ta_state->reverse) {
      idx_to_use = bg_color_idx;
    } else {
      if (fg_color_idx == ANSI_DEFAULT_COLOR_IDX) {
        idx_to_use = ANSI_REVERSE_DEFAULT_IDX;
      } else if (fg_color_idx == ANSI_DEFAULT_BOLD_COLOR_IDX) {
        idx_to_use = ANSI_REVERSE_BOLD_DEFAULT_IDX;
      } else {
        idx_to_use = fg_color_idx;
      }
    }
    gtk_text_buffer_apply_tag(gui->txtBuffer,
                              gui->ta.ansi_back_tags[idx_to_use],
                              &start_iter,
                              iter);
  }

  /* Special attributes */
  if (ta_state->underline == PANGO_UNDERLINE_SINGLE) {
    gtk_text_buffer_apply_tag(gui->txtBuffer, gui->ta.underline_tag,
                              &start_iter, iter);
  } else if (ta_state->underline == PANGO_UNDERLINE_DOUBLE) {
    gtk_text_buffer_apply_tag(gui->txtBuffer, gui->ta.dblunderline_tag,
                              &start_iter, iter);
  }
  if (ta_state->strike) {
    gtk_text_buffer_apply_tag(gui->txtBuffer, gui->ta.strike_tag,
                              &start_iter, iter);
  }
  if (ta_state->italics) {
    gtk_text_buffer_apply_tag(gui->txtBuffer, gui->ta.italics_tag,
                              &start_iter, iter);
  }
}
