/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include "libintl-wrapper.h"
#include <gtk/gtk.h>

#include "kildclient.h"
#include "perlscript.h"


/***********************
 * Function prototypes *
 ***********************/
/* XML UI signals */
G_MODULE_EXPORT void terminalfont_set_cb(GtkFontButton *font_btn,
                                         gpointer data);
G_MODULE_EXPORT void use_bold_changed_cb(GtkToggleButton *chkbox,
                                         gpointer data);
G_MODULE_EXPORT void wrap_changed_cb(GtkToggleButton *widget,
                                     gpointer data);
G_MODULE_EXPORT void wrap_indent_changed_cb(GtkSpinButton *widget,
                                            gpointer data);
G_MODULE_EXPORT void wrap_column_changed_cb(GtkWidget *widget,
                                            gpointer data);
G_MODULE_EXPORT void scroll_output_changed_cb(GtkToggleButton *widget,
                                              gpointer         data);
G_MODULE_EXPORT void use_tooltips_changed_cb(GtkToggleButton *widget,
                                             gpointer         data);
static void fill_font_section(World *world, GtkBuilder *ui_builder);
static void fill_word_wrap_section(World *world, GtkBuilder *ui_builder);
static void fill_scrolling_section(World *world, GtkBuilder *ui_builder);
static void fill_tooltip_section(World *world, GtkBuilder *ui_builder);
static void fill_name_display_section(World *world, GtkBuilder *ui_builder);
static void update_scrolling_parameters(World *world, GtkBuilder *ui_builder);
static void update_name_display_parameters(World *world,
                                           GtkBuilder *ui_builder);


/*************************
 * File global variables *
 *************************/



GObject *
get_we_mainwindow_tab(GtkBuilder *ui_builder)
{
  GObject *panel;

  panel = gtk_builder_get_object(ui_builder, "we_panel_mainwindow");

  return panel;
}


void
fill_we_mainwindow_tab(World *world, GtkBuilder *ui_builder)
{
  fill_font_section(world, ui_builder);
  fill_word_wrap_section(world, ui_builder);
  fill_scrolling_section(world, ui_builder);
  fill_tooltip_section(world, ui_builder);
  fill_name_display_section(world, ui_builder);
}


static
void
fill_font_section(World *world, GtkBuilder *ui_builder)
{
  GtkFontChooser  *terminalfont_btn;
  GtkToggleButton *chkUseBold;

  terminalfont_btn
    = GTK_FONT_CHOOSER(gtk_builder_get_object(ui_builder, "terminalfont_btn"));
  chkUseBold
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "chkUseBold"));

  gtk_font_chooser_set_font(terminalfont_btn, world->terminalfont);
  gtk_toggle_button_set_active(chkUseBold, world->usebold);
}


static
void
fill_word_wrap_section(World *world, GtkBuilder *ui_builder)
{
  GtkToggleButton *chkUseWrap;
  GtkSpinButton   *spnWrapIndent;
  GtkToggleButton *radWrapFullWidth;
  GtkToggleButton *radWrapColumn;
  GtkSpinButton   *spnWrapColumn;

  chkUseWrap
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "chkUseWrap"));
  spnWrapIndent
    = GTK_SPIN_BUTTON(gtk_builder_get_object(ui_builder, "spnWrapIndent"));
  radWrapFullWidth
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "radWrapFullWidth"));
  radWrapColumn
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "radWrapColumn"));
  spnWrapColumn
    = GTK_SPIN_BUTTON(gtk_builder_get_object(ui_builder, "spnWrapColumn"));

  gtk_toggle_button_set_active(chkUseWrap, world->wrap);
  gtk_spin_button_set_value(spnWrapIndent, world->wrap_indent);
  if (world->use_wrap_column) {
    gtk_toggle_button_set_active(radWrapColumn, TRUE);
  } else {
    gtk_toggle_button_set_active(radWrapFullWidth, TRUE);
  }
  gtk_spin_button_set_value(spnWrapColumn, world->wrap_column);
}


static
void
fill_scrolling_section(World *world, GtkBuilder *ui_builder)
{
  GtkToggleButton *chkScrollOutput;
  GtkSpinButton   *txtBufferLines;

  chkScrollOutput
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "chkScrollOutput"));
  txtBufferLines
    = GTK_SPIN_BUTTON(gtk_builder_get_object(ui_builder, "txtBufferLines"));

  gtk_toggle_button_set_active(chkScrollOutput, world->scrollOutput);
  gtk_spin_button_set_value(txtBufferLines, world->buffer_lines);
}


static
void
fill_tooltip_section(World *world, GtkBuilder *ui_builder)
{
  GtkToggleButton *chkUseTooltips;

  chkUseTooltips
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "chkUseTooltips"));

  gtk_toggle_button_set_active(chkUseTooltips, world->use_tooltips);
}


static
void
fill_name_display_section(World *world, GtkBuilder *ui_builder)
{
  GtkToggleButton *radNameDisplayWorld;
  GtkToggleButton *radNameDisplayWorldChar;
  GtkToggleButton *radNameDisplayCharWorld;
  GtkToggleButton *radNameDisplayCustom;
  GtkEntry        *txtNameDisplayCustom;

  radNameDisplayWorld
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "radNameDisplayWorld"));
  radNameDisplayWorldChar
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "radNameDisplayWorldChar"));
  radNameDisplayCharWorld
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "radNameDisplayCharWorld"));
  radNameDisplayCustom
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "radNameDisplayCustom"));
  txtNameDisplayCustom
    = GTK_ENTRY(gtk_builder_get_object(ui_builder, "txtNameDisplayCustom"));

  switch (world->name_display_style) {
  case NAME_DISPLAY_WORLD:
    gtk_toggle_button_set_active(radNameDisplayWorld, TRUE);
    break;

  case NAME_DISPLAY_WORLD_CHAR:
    gtk_toggle_button_set_active(radNameDisplayWorldChar, TRUE);
    break;

  case NAME_DISPLAY_CHAR_WORLD:
    gtk_toggle_button_set_active(radNameDisplayCharWorld, TRUE);
    break;

  case NAME_DISPLAY_CUSTOM:
    gtk_toggle_button_set_active(radNameDisplayCustom, TRUE);
    break;
  }
  gtk_entry_set_text(txtNameDisplayCustom, world->name_display_pattern);
}


void
terminalfont_set_cb(GtkFontButton *font_btn, gpointer data)
{
  World    *world = (World *) data;
  WorldGUI *gui   = world->gui;

  free(world->terminalfont);
  world->terminalfont
    = g_strdup(gtk_font_chooser_get_font(GTK_FONT_CHOOSER(font_btn)));

  if (gui) {
    configure_css(gui, world);

    /* If the font has changed, we don't know what the new font supports. */
    gui->ta.sup_geom_shapes   = -1;
    gui->ta.sup_block         = -1;
    gui->ta.sup_control       = -1;
    gui->ta.sup_l1_supplement = -1;
    gui->ta.sup_box_drawing   = -1;
    gui->ta.sup_misc_tech     = -1;
    gui->ta.sup_math          = -1;
    gui->ta.sup_greek         = -1;
  }
}


void
use_bold_changed_cb(GtkToggleButton *chkbox, gpointer data)
{
  World    *world = (World *) data;
  WorldGUI *gui   = world->gui;

  world->usebold = gtk_toggle_button_get_active(chkbox);
  if (gui) {
    ansitextview_update_color_tags(gui, gui->world);
  }
}


void
wrap_changed_cb(GtkToggleButton *button, gpointer data)
{
  World    *world = (World *) data;
  WorldGUI *gui   = world->gui;

  world->wrap = gtk_toggle_button_get_active(button);
  if (gui) {
    gtk_text_view_set_wrap_mode(gui->txtView,
                                world->wrap
                                  ? GTK_WRAP_WORD_CHAR
                                  : GTK_WRAP_CHAR);
    gtk_text_view_set_wrap_mode(gui->txtViewScroll,
                                world->wrap
                                  ? GTK_WRAP_WORD_CHAR
                                  : GTK_WRAP_CHAR);
  }
}


void
wrap_indent_changed_cb(GtkSpinButton *widget, gpointer data)
{
  World    *world = (World *) data;
  WorldGUI *gui   = world->gui;

  world->wrap_indent = gtk_spin_button_get_value_as_int(widget);
  if (gui) {
    gtk_text_view_set_indent(gui->txtView, -world->wrap_indent);
    gtk_text_view_set_indent(gui->txtViewScroll, -world->wrap_indent);
  }
}


void
wrap_column_changed_cb(GtkWidget *widget, gpointer data)
{
  World           *world = (World *) data;
  GtkToggleButton *radWrapFullWidth;
  GtkSpinButton   *spnWrapColumn;

  radWrapFullWidth
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(world->ui_builder,
                                               "radWrapFullWidth"));
  spnWrapColumn = GTK_SPIN_BUTTON(gtk_builder_get_object(world->ui_builder,
                                                         "spnWrapColumn"));

  if (gtk_toggle_button_get_active(radWrapFullWidth)) {
    world->use_wrap_column = FALSE;
  } else {
    world->use_wrap_column = TRUE;
  }

  world->wrap_column = gtk_spin_button_get_value(spnWrapColumn);

  if (world->gui) {
    gtk_widget_queue_resize(GTK_WIDGET(world->gui->txtView));
  }
}


void
scroll_output_changed_cb(GtkToggleButton *widget, gpointer data)
{
  World *world = (World *) data;

  world->scrollOutput = gtk_toggle_button_get_active(widget);
}


void
use_tooltips_changed_cb(GtkToggleButton *button, gpointer data)
{
  World *world = (World *) data;

  world->use_tooltips = gtk_toggle_button_get_active(button);

  if (world->gui) {
    gtk_widget_set_has_tooltip(GTK_WIDGET(world->gui->txtView),
                               world->use_tooltips);
    gtk_widget_set_has_tooltip(GTK_WIDGET(world->gui->txtViewScroll),
                               world->use_tooltips);
  }
}


void
update_we_mainwindow_parameters(World *world, GtkBuilder *ui_builder)
{
  update_scrolling_parameters(world, ui_builder);
  update_name_display_parameters(world, ui_builder);
}


static
void
update_scrolling_parameters(World *world, GtkBuilder *ui_builder)
{
  GtkSpinButton   *txtBufferLines;

  txtBufferLines
    = GTK_SPIN_BUTTON(gtk_builder_get_object(ui_builder, "txtBufferLines"));

  world->buffer_lines = gtk_spin_button_get_value_as_int(txtBufferLines);
}


static
void
update_name_display_parameters(World *world, GtkBuilder *ui_builder)
{
  GtkToggleButton *radNameDisplayWorld;
  GtkToggleButton *radNameDisplayWorldChar;
  GtkToggleButton *radNameDisplayCharWorld;
  GtkToggleButton *radNameDisplayCustom;
  GtkEntry        *txtNameDisplayCustom;

  const gchar *newval;

  radNameDisplayWorld
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "radNameDisplayWorld"));
  radNameDisplayWorldChar
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "radNameDisplayWorldChar"));
  radNameDisplayCharWorld
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "radNameDisplayCharWorld"));
  radNameDisplayCustom
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "radNameDisplayCustom"));
  txtNameDisplayCustom
    = GTK_ENTRY(gtk_builder_get_object(ui_builder, "txtNameDisplayCustom"));

  if (gtk_toggle_button_get_active(radNameDisplayWorld)) {
    world->name_display_style = NAME_DISPLAY_WORLD;
  } else if (gtk_toggle_button_get_active(radNameDisplayWorldChar)) {
    world->name_display_style = NAME_DISPLAY_WORLD_CHAR;
  } else if (gtk_toggle_button_get_active(radNameDisplayCharWorld)) {
    world->name_display_style = NAME_DISPLAY_CHAR_WORLD;
  } else if (gtk_toggle_button_get_active(radNameDisplayCustom)) {
    world->name_display_style = NAME_DISPLAY_CUSTOM;
  }

  newval = gtk_entry_get_text(txtNameDisplayCustom);
  if (!world->name_display_pattern
      || strcmp(world->name_display_pattern, newval) != 0) {
    g_free(world->name_display_pattern);
    world->name_display_pattern = g_strdup(newval);
  }
}
