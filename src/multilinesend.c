/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include "libintl-wrapper.h"
#include <locale.h>
#include <gtk/gtk.h>
#include <gmodule.h>

#include "kildclient.h"
#include "perlscript.h"



/***********************
 * Function prototypes *
 ***********************/
static GtkWidget *create_multi_line_send_dialog(GtkWindow *parent,
                                                World *world);
static gboolean   multi_line_send_timer_cb(gpointer data);
static gchar     *get_text_buffer_line(GtkTextBuffer *buffer,
                                       int line,
                                       int nlines,
                                       gboolean *last);
static gchar     *get_file_line(FILE *fp, gboolean *last);
static void       send_line(gchar *line, MLSendData  *context);
static void       string_free(gpointer string, gpointer user_data);
/* XML UI signals */
G_MODULE_EXPORT void multi_line_send_cb(GtkMenuItem *widget, gpointer data);
G_MODULE_EXPORT void start_multi_line_send_cb(GtkButton *btn, gpointer data);
G_MODULE_EXPORT void clear_multi_line_send_dialog_cb(GtkWidget *widget,
                                                     gpointer   data);



void
multi_line_send_cb(GtkMenuItem *widget, gpointer data)
{
  if (!currentWorld->dlgMLSend) {
    currentWorld->dlgMLSend
      = create_multi_line_send_dialog(GTK_WINDOW(wndMain), currentWorld);
  }

  gtk_widget_show_all(currentWorld->dlgMLSend);
  gtk_window_present(GTK_WINDOW(currentWorld->dlgMLSend));
}


static
GtkWidget *
create_multi_line_send_dialog(GtkWindow *parent, World *world)
{
  GError        *error = NULL;
  GtkWidget     *dlg;
  GtkWidget     *text;
  GtkWidget     *txtInitial;
  GtkSpinButton *spnDelay;
  GtkSpinButton *spnLinesATime;

  PangoLayout          *layout;
  gint                  char_width;
  PangoRectangle        logical_rect;
  GtkRequisition        requisition;

  /* Create the dialog */
  if (!world->ui_builder) {
    world->ui_builder = gtk_builder_new();
  }
  if (!gtk_builder_add_from_resource(world->ui_builder,
                                     "/ekalin/kildclient/dlgMLSend.ui",
                                     &error)) {
    g_warning(_("Error loading UI from XML file: %s"), error->message);
    g_error_free(error);
    return NULL; /* This will cause warnings in the calling function, but
                    there is need to abort the program because of this. */
  }
  dlg = GTK_WIDGET(gtk_builder_get_object(world->ui_builder, "dlgMLSend"));
  gtk_window_set_transient_for(GTK_WINDOW(dlg), GTK_WINDOW(wndMain));

  /* Configure widgets to use input font */
  txtInitial = GTK_WIDGET(gtk_builder_get_object(world->ui_builder,
                                                 "txtInitial"));
  add_css_provider_and_class(txtInitial, world->gui->css_provider, "input");

  text = GTK_WIDGET(gtk_builder_get_object(world->ui_builder, "txtFinal"));
  add_css_provider_and_class(text, world->gui->css_provider, "input");

  text = GTK_WIDGET(gtk_builder_get_object(world->ui_builder, "txtStart"));
  add_css_provider_and_class(text, world->gui->css_provider, "input");

  text = GTK_WIDGET(gtk_builder_get_object(world->ui_builder, "txtEnd"));
  add_css_provider_and_class(text, world->gui->css_provider, "input");

  /* Default values */
  spnDelay = GTK_SPIN_BUTTON(gtk_builder_get_object(world->ui_builder,
                                                    "spnMLSDelay"));
  gtk_spin_button_set_value(spnDelay, globalPrefs.multi_cmd_delay);
  spnLinesATime
    = GTK_SPIN_BUTTON(gtk_builder_get_object(world->ui_builder,
                                             "spnLinesATime"));
  gtk_spin_button_set_value(spnLinesATime, globalPrefs.multi_cmd_group_size);

  /* Connect signals */
  gtk_builder_connect_signals(world->ui_builder, world);

  /* Set width of entries to be slightly larger than 80 columns. */
  layout = gtk_widget_create_pango_layout(txtInitial, "W");
  pango_layout_get_pixel_extents(layout, NULL, &logical_rect);
  char_width = logical_rect.width;
  /* This hack allows the initial size to be set, but without preventing
     the window from getting smaller. */
  gtk_widget_set_size_request(txtInitial, 81 * char_width, -1);
  gtk_widget_get_preferred_size(dlg, NULL, &requisition);
  gtk_widget_set_size_request(txtInitial, -1, -1);
  gtk_window_resize(GTK_WINDOW(dlg), requisition.width, requisition.height);

  g_object_unref(layout);

  return dlg;
}


void
start_multi_line_send_cb(GtkButton *btn, gpointer data)
{
  World           *world = (World *) data;
  GtkBuilder      *ui_builder;
  GtkTextView     *txtInitial;
  GtkTextView     *txtFinal;
  GtkFileChooser  *txtFile;
  GtkEntry        *txtStart;
  GtkEntry        *txtEnd;
  GtkSpinButton   *spnDelay;
  GtkSpinButton   *spnLinesATime;
  GtkToggleButton *chkDontClose;
  gdouble          delay;
  MLSendData      *mlcontext;

  ui_builder = world->ui_builder;

  txtInitial
    = GTK_TEXT_VIEW(gtk_builder_get_object(ui_builder, "txtInitial"));
  txtFinal = GTK_TEXT_VIEW(gtk_builder_get_object(ui_builder, "txtFinal"));
  txtFile  = GTK_FILE_CHOOSER(gtk_builder_get_object(ui_builder, "txtFile"));
  txtStart = GTK_ENTRY(gtk_builder_get_object(ui_builder, "txtStart"));
  txtEnd   = GTK_ENTRY(gtk_builder_get_object(ui_builder, "txtEnd"));
  spnDelay
    = GTK_SPIN_BUTTON(gtk_builder_get_object(ui_builder, "spnMLSDelay"));
  spnLinesATime
    = GTK_SPIN_BUTTON(gtk_builder_get_object(ui_builder, "spnLinesATime"));
  chkDontClose
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "chkDontClose"));

  delay = gtk_spin_button_get_value(spnDelay);

  mlcontext = g_new0(MLSendData, 1);
  mlcontext->state       = ML_TRY_INITIALBUFFER;
  mlcontext->world       = world;
  mlcontext->textInitial = gtk_text_view_get_buffer(txtInitial);
  mlcontext->textFinal   = gtk_text_view_get_buffer(txtFinal);
  mlcontext->file        = gtk_file_chooser_get_filename(txtFile);
  mlcontext->textStart   = g_strdup(gtk_entry_get_text(txtStart));
  mlcontext->textEnd     = g_strdup(gtk_entry_get_text(txtEnd));
  mlcontext->linesatime  = gtk_spin_button_get_value_as_int(spnLinesATime);
  mlcontext->dont_close  = gtk_toggle_button_get_active(chkDontClose);

  if (!mlcontext->dont_close) {
    gtk_widget_hide(world->dlgMLSend);
  }

  do_multi_line_send(mlcontext, delay);
}


void
do_multi_line_send(MLSendData *mlcontext, gdouble delay)
{
  /* Send the first line immediately */
  if (multi_line_send_timer_cb(mlcontext)) {
    if (delay) {
      g_timeout_add_full(G_PRIORITY_HIGH,
                         delay * 1000,
                         multi_line_send_timer_cb,
                         mlcontext,
                         NULL);
    } else {    /* If delay is zero, send as fast as possible */
      while (multi_line_send_timer_cb(mlcontext))
        ;
    }
  }
}


static
gboolean
multi_line_send_timer_cb(gpointer data)
{
  MLSendData  *context = (MLSendData *) data;
  int          last = FALSE;
  FILE        *fp;
  gchar       *line;
  int          sent_lines;

  sent_lines = 0;
  while (sent_lines < context->linesatime) {
    switch (context->state) {
    case ML_TRY_INITIALBUFFER:
      if (context->textInitial
          && gtk_text_buffer_get_char_count(context->textInitial)) {
        context->state = ML_INITIALBUFFER;
        context->nlines = gtk_text_buffer_get_line_count(context->textInitial);
        context->currline = 0;
      } else {
        context->state = ML_TRY_INITIALLINES;
      }
      break;

    case ML_TRY_INITIALLINES:
      if (context->linesInitial) {
        context->state  = ML_INITIALLINES;
        context->list_iterator = context->linesInitial;
      } else {
        context->state = ML_TRY_FILE;
      }
      break;

    case ML_TRY_FILE:
      if (context->file) {
        fp = fopen(context->file, "r");
        if (fp) {
          context->state = ML_FILECONTENTS;
          context->fp = fp;
          break;
        }
      }
      context->state = ML_TRY_FINALBUFFER;
      break;

    case ML_TRY_FINALBUFFER:
      if (context->textFinal
          && gtk_text_buffer_get_char_count(context->textFinal)) {
        context->state = ML_FINALBUFFER;
        context->nlines = gtk_text_buffer_get_line_count(context->textFinal);
        context->currline = 0;
      } else {
        context->state = ML_TRY_FINALLINES;
      }
      break;

    case ML_TRY_FINALLINES:
      if (context->linesFinal) {
        context->state = ML_FINALLINES;
        context->list_iterator = context->linesFinal;
      } else {
        context->state = ML_FINISH;
      }
      break;

    case ML_INITIALBUFFER:
      line = get_text_buffer_line(context->textInitial,
                                  context->currline,
                                  context->nlines,
                                  &last);
      send_line(line, context);
      g_free(line);
      ++sent_lines;
      if (last) {
        context->state = ML_TRY_FILE;
      } else {
        ++context->currline;
      }
      break;

    case ML_FINALBUFFER:
      line = get_text_buffer_line(context->textFinal,
                                  context->currline,
                                  context->nlines,
                                  &last);
      send_line(line, context);
      g_free(line);
      ++sent_lines;
      if (last) {
        context->state = ML_FINISH;
      } else {
        ++context->currline;
      }
      break;

    case ML_INITIALLINES:
      line = (gchar *) context->list_iterator->data;
      send_line(line, context);
      ++sent_lines;
      context->list_iterator = context->list_iterator->next;
      if (!context->list_iterator) {
        context->state = ML_TRY_FILE;
      }
      break;

    case ML_FINALLINES:
      line = (gchar *) context->list_iterator->data;
      send_line(line, context);
      ++sent_lines;
      context->list_iterator = context->list_iterator->next;
      if (!context->list_iterator) {
        context->state = ML_FINISH;
      }
      break;

    case ML_FILECONTENTS:
      line = get_file_line(context->fp, &last);
      send_line(line, context);
      ++sent_lines;
      if (last) {
        context->state = ML_TRY_FINALBUFFER;
        fclose(context->fp);
      }
      break;

    case ML_FINISH:
      if (context->textInitial && !context->dont_close) {
        clear_multi_line_send_dialog_cb(GTK_WIDGET(context->world->dlgMLSend),
                                        context->world);
      }
      g_free(context->textStart);
      g_free(context->textEnd);
      g_free(context->file);
      if (context->linesInitial) {
        g_slist_foreach(context->linesInitial, string_free, NULL);
        g_slist_free(context->linesInitial);
      }
      if (context->linesFinal) {
        g_slist_foreach(context->linesFinal, string_free, NULL);
        g_slist_free(context->linesFinal);
      }
      g_free(context);
      return FALSE;
    }
  }

  return TRUE;
}


void
clear_multi_line_send_dialog_cb(GtkWidget *widget, gpointer data)
{
  World          *world = (World *) data;
  GtkBuilder     *ui_builder;
  GtkWidget      *dlgMLSend;
  GtkTextView    *txtInitial;
  GtkFileChooser *txtFile;
  GtkTextView    *txtFinal;
  GtkEntry       *txtStart;
  GtkEntry       *txtEnd;
  GtkTextBuffer  *buffer;

  ui_builder = world->ui_builder;
  dlgMLSend  = GTK_WIDGET(gtk_builder_get_object(ui_builder, "dlgMLSend"));
  txtInitial = GTK_TEXT_VIEW(gtk_builder_get_object(ui_builder, "txtInitial"));
  txtFile    = GTK_FILE_CHOOSER(gtk_builder_get_object(ui_builder, "txtFile"));
  txtFinal   = GTK_TEXT_VIEW(gtk_builder_get_object(ui_builder, "txtFinal"));
  txtStart   = GTK_ENTRY(gtk_builder_get_object(ui_builder, "txtStart"));
  txtEnd     = GTK_ENTRY(gtk_builder_get_object(ui_builder, "txtEnd"));

  buffer = gtk_text_view_get_buffer(txtInitial);
  gtk_text_buffer_set_text(buffer, "", 0);
  gtk_file_chooser_unselect_all(txtFile);
  buffer = gtk_text_view_get_buffer(txtFinal);
  gtk_text_buffer_set_text(buffer, "", 0);
  gtk_entry_set_text(txtStart, "");
  gtk_entry_set_text(txtEnd, "");

  gtk_widget_hide(dlgMLSend);
}


static
gchar *
get_text_buffer_line(GtkTextBuffer *buffer,
                     int line,
                     int nlines,
                     gboolean *last)
{
  GtkTextIter  start;
  GtkTextIter  end;
  gchar       *to_send;

  gtk_text_buffer_get_iter_at_line(buffer, &start, line);
  if (line == nlines - 1) {
    gtk_text_buffer_get_end_iter(buffer, &end);
    *last = TRUE;
  } else {
    gtk_text_buffer_get_iter_at_line(buffer, &end, line+1);
    *last = FALSE;
  }
  to_send = gtk_text_buffer_get_text(buffer, &start, &end, FALSE);

  return to_send;
}


static
gchar *
get_file_line(FILE *fp, gboolean *last)
{
  static char line[MAX_BUFFER];
  int         c;

  if (!fgets(line, MAX_BUFFER, fp)) {
    *last = TRUE;
    line[0] = '\0';
    return line;
  }

  c = fgetc(fp);
  if (c != -1) {
    ungetc(c, fp);
    *last = FALSE;
  } else {
    *last = TRUE;
  }

  return line;
}


static
void
send_line(gchar *line, MLSendData *context)
{
  int len;

  /* Remove final newline */
  len = strlen(line);
  if (line[len-1] == '\n') {
    line[len - 1] = '\0';
    --len;
    if (line[len-1] == '\r') {
      line[len - 1] = '\0';
      --len;
    }
  }

  send_to_world_no_check(context->world,
                         context->textStart, strlen(context->textStart),
                         NULL, 0,
                         FALSE,
                         FALSE);
  send_to_world_no_check(context->world,
                         line, len,
                         NULL, 0,
                         FALSE,
                         FALSE);
  send_to_world_no_check(context->world,
                         context->textEnd, strlen(context->textEnd),
                         NULL, 0,
                         TRUE,
                         FALSE);
}


static
void
string_free(gpointer string, gpointer user_data)
{
  g_free(string);
}
