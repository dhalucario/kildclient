/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include "libintl-wrapper.h"
#include <locale.h>
#include <gtk/gtk.h>

/* Perl includes */
#include <EXTERN.h>
#include <perl.h>
#include <XSUB.h>

#include "kildclient.h"
#include "perlscript.h"


/*************************
 * File global variables *
 *************************/


/***********************
 * Function prototypes *
 ***********************/


void
load_permanent_variables(World *world)
{
  gchar *variables_file;

  if (world->file) {
    variables_file = g_strdup_printf("%s.var", world->file);
    perl_script_import_file(variables_file);
    g_free(variables_file);
  }
}


void
save_permanent_variables(World *world)
{
  gchar           *variables_file;
  GSList          *iter;
  gchar           *varname;
  gchar           *command;
  SV              *retval;
  GString         *file_contents;
  GError          *error = NULL;

  if (!world->file) {
    return;
  }

  variables_file = g_strdup_printf("%s.var", world->file);
  if (!world->permanent_variables) {
    /* We don't care if it doesn't exist */
    unlink(variables_file);
    g_free(variables_file);
    return;
  }

  file_contents = g_string_sized_new(512);

  GRAB_PERL(world);

  iter = world->permanent_variables;
  while (iter) {
    varname = (char *) iter->data;

    if (varname[0] == '$') {
      command = g_strdup_printf("$__serialized = Data::Dumper->Dump([%s], ['%s']);",
                                varname, varname+1);
    } else {
      command = g_strdup_printf("$__serialized = Data::Dumper->Dump([\\%s], ['*%s']);",
                                varname, varname+1);
    }

    eval_pv(command, TRUE);
    retval = get_sv("__serialized", FALSE);
    g_string_append(file_contents, SvPV_nolen(retval));

    g_free(command);

    iter = iter->next;
  }

  RELEASE_PERL(world);

  if (!g_file_set_contents(variables_file,
                           file_contents->str, file_contents->len,
                           &error)) {
    kc_error_dialog(GTK_WINDOW(wndMain),
                    _("Could not open file '%s': %s\n"
                      "Variables were not saved."),
                    variables_file, error->message);
    g_error_free(error);
  }
  g_free(variables_file);
  g_string_free(file_contents, TRUE);
}


gboolean
move_var(World *world, gint old_pos, gint new_pos)
{
  GSList   *varitem;
  gpointer  var;

  varitem = g_slist_nth(world->permanent_variables, old_pos);
  if (!varitem) {
    return FALSE;
  }
  var = varitem->data;

  world->permanent_variables
    = g_slist_delete_link(world->permanent_variables, varitem);
  world->permanent_variables
    = g_slist_insert(world->permanent_variables, var, new_pos);

  we_var_delete_var(world, var);
  we_var_insert_var(world, var, new_pos);

  return TRUE;
}


void
free_permanent_variable(gchar *variable, gpointer data)
{
  g_free(variable);
}

