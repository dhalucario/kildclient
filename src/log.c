/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include "libintl-wrapper.h"
#include <locale.h>
#include <gtk/gtk.h>

#include "kildclient.h"
#include "perlscript.h"


/*************************
 * File global variables *
 *************************/


/***********************
 * Function prototypes *
 ***********************/
static void get_log_filename(World  *world,
                             gchar  *actualfile,
                             time_t *curr_time);
static void make_directories(const gchar* path);



void
write_log_line(World *world, const char *line, time_t *line_time)
{
  int         len;
  static char timestr[MAX_TIME_STR];

  if (!world->log_file) {
    return;
  }

  g_mutex_lock(&world->write_log_line_mutex);

  if (world->log_add_time) {
    time_t     currtime;
    struct tm *brokentime;

    if (line_time) {
      brokentime = localtime(line_time);
    } else {
      currtime = time(NULL);
      brokentime = localtime(&currtime);
    }
    strftime(timestr, MAX_TIME_STR,
             world->log_timeformat,
             brokentime);

    fprintf(world->log_file, "%s", timestr);
  }

  len = strlen(line);
  if (!len) {
    g_mutex_unlock(&world->write_log_line_mutex);
    return;
  }

  fputs(line, world->log_file);

  /* Finish always with a NL character */
  if (line[len - 1] != '\n') {
    fputc('\n', world->log_file);
  }

  g_mutex_unlock(&world->write_log_line_mutex);
}


void
start_log_and_display_result(World *world)
{
  gchar *errmsg = NULL;

  if (start_log(world, 0, &errmsg)) {
    ansitextview_append_stringf_nl(world->gui,
                                   _("Logging to file %s."),
                                   world->log_actual_file);
  } else {
    ansitextview_append_string_nl(world->gui, errmsg);
    g_free(errmsg);
  }
}


gboolean
start_log(World *world, gint lines, gchar **errmsg)
{
  time_t       curr_time;
  static char  actualfile[PATH_MAX+1];
  static char  timestr[MAX_TIME_STR];

  if (!world->log_file_name) {
    *errmsg = g_strdup(_("File name not specified."));
    return FALSE;
  }

  /* Get file name after substitutions */
  time(&curr_time);
  get_log_filename(world, actualfile, &curr_time);

  make_directories(actualfile);

  if ((world->log_file = fopen(actualfile, "a")) != NULL) {
    world->log_actual_file = g_strdup(actualfile);

    /* Use unbuffered output */
    setbuf(world->log_file, NULL);

    strcpy(timestr, ctime(&curr_time));
    /* Remove final nl */
    timestr[strlen(timestr) - 1] = '\0';
    fprintf(world->log_file, "*** LOG STARTED AT %s ***\n", timestr);

    /* Copy lines from the buffer, in case it was asked */
    if (lines) {
      gint         first_line;
      gint         last_line;
      gint         line;
      GtkTextIter  start_iter;
      GtkTextIter  end_iter;
      gchar       *text;
      gboolean     stop;

      last_line  = gtk_text_buffer_get_line_count(world->gui->txtBuffer);
      first_line = last_line - lines - 1;
      if (first_line < 0) {
        first_line = 0;
      }

      line = first_line;
      stop = FALSE;
      gtk_text_buffer_get_iter_at_line(world->gui->txtBuffer,
                                       &start_iter, first_line);
      while (1) {
        end_iter = start_iter;
        if (!gtk_text_iter_forward_line(&end_iter)) {
          stop = TRUE;
        }

        text = gtk_text_buffer_get_text(world->gui->txtBuffer,
                                        &start_iter, &end_iter,
                                        FALSE);
        write_log_line(world, text, &k_circular_queue_nth(world->gui->line_times,
                                                          time_t,
                                                          line));
        g_free(text);

        if (stop) {
          break;
        } else {
          start_iter = end_iter;
          ++line;
        }
      }
    }

    return TRUE;
  } else {
    *errmsg = g_strdup_printf(_("Could not open log file: %s."),
                              g_strerror(errno));
    return FALSE;
  }
}


void
stop_log(World *world)
{
  time_t      curr_time;
  static char timestr[MAX_TIME_STR];

  if (world->log_file) {
    time(&curr_time);
    strcpy(timestr, ctime(&curr_time));
    /* Remove final nl */
    timestr[strlen(timestr) - 1] = '\0';
    fprintf(world->log_file, "*** LOG ENDED AT %s ***\n\n",
            timestr);
    fclose(world->log_file);
    world->log_file = NULL;
    g_free(world->log_actual_file);
    world->log_actual_file = NULL;
  }
}


static
void
get_log_filename(World *world, gchar *actualfile, time_t *curr_time)
{
  gchar         *character;
  gchar         *tmp;
  struct tm     *brokentime;

  /* Get character name used */
  if (world->character_used == NULL || world->connection_style == NONE) {
    character = g_strdup("undefined");
  } else {
    GtkTreePath *path;
    GtkTreeIter  iter;

    path = gtk_tree_row_reference_get_path(world->character_used);
    if (!path) {
      gtk_tree_row_reference_free(world->character_used);
      world->character_used = NULL;
      character = g_strdup("undefined");
    }

    gtk_tree_model_get_iter(world->logon_characters,
                            &iter, path);
    gtk_tree_model_get(world->logon_characters, &iter,
                       LOGON_CHAR, &character,
                       -1);
  }

  /* Substitute KC-specific escapes */
  tmp = replace_kc_escapes(world->log_file_name, world->name, character);
  g_free(character);

  /* Substitute date/time values. */
  brokentime = localtime(curr_time);
  strftime(actualfile, PATH_MAX, tmp, brokentime);

  g_free(tmp);
}


static
void
make_directories(const gchar* path)
{
  gchar *dir = g_path_get_dirname(path);
  g_mkdir_with_parents(dir, 0755);
  g_free(dir);
}
