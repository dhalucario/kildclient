/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */


#ifndef __ANSI_H
#define __ANSI_H


/************************************
 * ANSI Escape Sequences - Integers *
 ************************************/
#define ANSI_FIRST_FG_COLOR   30
#define ANSI_LAST_FG_COLOR    37
#define ANSI_FIRST_BG_COLOR   40
#define ANSI_LAST_BG_COLOR    47
#define ANSI_FG_DEFAULT       39
#define ANSI_BG_DEFAULT       49

#define ANSI_RESET            0
#define ANSI_SET_FG_BOLD      1
#define ANSI_BOLD_OFF         22
#define ANSI_SET_BG_LIGHT     5
#define ANSI_BOLD_BG_OFF      25
#define ANSI_SET_UNDERLINE    4
#define ANSI_SET_DBLUNDERLINE 21
#define ANSI_UNDERLINE_OFF    24
#define ANSI_SET_STRIKE       9
#define ANSI_STRIKE_OFF       29
#define ANSI_SET_ITALICS      3
#define ANSI_ITALICS_OFF      23
#define ANSI_SET_REVERSE      7
#define ANSI_REVERSE_OFF      27
#define ANSI_SET_HIDDEN       8
#define ANSI_HIDDEN_OFF       28
/* Support for xterm's 256 color cube */
#define XTERM256_SET_FORE    38
#define XTERM256_SET_BACK    48
#define XTERM256_SECOND_CODE 5


/********************
 * For ANSI parsing *
 ********************/
#define ANSI_ESC 0x1b   /* 033, 27 dec */

#define ANSI_TABLE_SIZE               20
#define ANSI_N_COLORS_HIGHLIGHT       17
#define ANSI_N_COLORS                 8
#define ANSI_NORMAL_BASE_IDX          0
#define ANSI_BOLD_BASE_IDX            9
#define ANSI_DEFAULT_COLOR_IDX        8
#define ANSI_DEFAULT_BOLD_COLOR_IDX   17
#define ANSI_BLACK_IDX                0
#define ANSI_REVERSE_DEFAULT_IDX      18
#define ANSI_REVERSE_BOLD_DEFAULT_IDX 19

/* This offset represents the difference in the way the tags and the ansi
   colors are stored. For the tags, 0-7 hold the basic colors, 8 the default
   color, 9-16 the bold colors and 17 the default bold; for colors
   ansicolors have the basic and bold colors in sequence in 0-15, and
   defaults are stored separatedly. */
#define ANSI_TABLE_OFFSET 1


/***********************************
 * ANSI Escape Sequences - Strings *
 ***********************************/

/* Basic building blocks for colors */
#define ANSI_START "\033["
#define ANSI_COLOR_END   "m"
#define ANSI_SEP         ";"

#define ANSI_ATTRIBS_OFF "0"
#define ANSI_BOLD_ON     "1"
#define ANSI_BLINK_ON    "5"  /* Actually causes light background */

#define ANSI_FG_BLACK    "30"
#define ANSI_FG_RED      "31"
#define ANSI_FG_GREEN    "32"
#define ANSI_FG_YELLOW   "33"
#define ANSI_FG_BLUE     "34"
#define ANSI_FG_MAGENTA  "35"
#define ANSI_FG_CYAN     "36"
#define ANSI_FG_WHITE    "37"

#define ANSI_BG_BLACK    "40"
#define ANSI_BG_RED      "41"
#define ANSI_BG_GREEN    "42"
#define ANSI_BG_YELLOW   "43"
#define ANSI_BG_BLUE     "44"
#define ANSI_BG_MAGENTA  "45"
#define ANSI_BG_CYAN     "46"
#define ANSI_BG_WHITE    "47"

/* Codes to set colors */
#define ANSI_RESET_DEFAULT  ANSI_START ANSI_ATTRIBS_OFF ANSI_COLOR_END

#define ANSI_SET_FG_BLACK   ANSI_START ANSI_ATTRIBS_OFF ANSI_SEP \
                            ANSI_FG_BLACK   ANSI_COLOR_END
#define ANSI_SET_FG_RED     ANSI_START ANSI_ATTRIBS_OFF ANSI_SEP \
                            ANSI_FG_RED     ANSI_COLOR_END
#define ANSI_SET_FG_GREEN   ANSI_START ANSI_ATTRIBS_OFF ANSI_SEP \
                            ANSI_FG_GREEN   ANSI_COLOR_END
#define ANSI_SET_FG_BLUE    ANSI_START ANSI_ATTRIBS_OFF ANSI_SEP \
                            ANSI_FG_BLUE    ANSI_COLOR_END
#define ANSI_SET_FG_YELLOW  ANSI_START ANSI_ATTRIBS_OFF ANSI_SEP \
                            ANSI_FG_YELLOW  ANSI_COLOR_END
#define ANSI_SET_FG_MAGENTA ANSI_START ANSI_ATTRIBS_OFF ANSI_SEP \
                            ANSI_FG_MAGENTA ANSI_COLOR_END
#define ANSI_SET_FG_CYAN    ANSI_START ANSI_ATTRIBS_OFF ANSI_SEP \
                            ANSI_FG_CYAN    ANSI_COLOR_END
#define ANSI_SET_FG_WHITE   ANSI_START ANSI_ATTRIBS_OFF ANSI_SEP \
                            ANSI_FG_WHITE   ANSI_COLOR_END

#define ANSI_SET_BG_BLACK   ANSI_START ANSI_BG_BLACK   ANSI_COLOR_END
#define ANSI_SET_BG_RED     ANSI_START ANSI_BG_RED     ANSI_COLOR_END
#define ANSI_SET_BG_GREEN   ANSI_START ANSI_BG_GREEN   ANSI_COLOR_END
#define ANSI_SET_BG_BLUE    ANSI_START ANSI_BG_BLUE    ANSI_COLOR_END
#define ANSI_SET_BG_YELLOW  ANSI_START ANSI_BG_YELLOW  ANSI_COLOR_END
#define ANSI_SET_BG_MAGENTA ANSI_START ANSI_BG_MAGENTA ANSI_COLOR_END
#define ANSI_SET_BG_CYAN    ANSI_START ANSI_BG_CYAN    ANSI_COLOR_END
#define ANSI_SET_BG_WHITE   ANSI_START ANSI_BG_WHITE   ANSI_COLOR_END

#define ANSI_SET_FG_BLACK_BOLD   ANSI_START ANSI_BOLD_ON ANSI_SEP \
                                 ANSI_FG_BLACK   ANSI_COLOR_END
#define ANSI_SET_FG_RED_BOLD     ANSI_START ANSI_BOLD_ON ANSI_SEP \
                                 ANSI_FG_RED     ANSI_COLOR_END
#define ANSI_SET_FG_GREEN_BOLD   ANSI_START ANSI_BOLD_ON ANSI_SEP \
                                 ANSI_FG_GREEN   ANSI_COLOR_END
#define ANSI_SET_FG_BLUE_BOLD    ANSI_START ANSI_BOLD_ON ANSI_SEP \
                                 ANSI_FG_BLUE    ANSI_COLOR_END
#define ANSI_SET_FG_YELLOW_BOLD  ANSI_START ANSI_BOLD_ON ANSI_SEP \
                                 ANSI_FG_YELLOW  ANSI_COLOR_END
#define ANSI_SET_FG_MAGENTA_BOLD ANSI_START ANSI_BOLD_ON ANSI_SEP \
                                 ANSI_FG_MAGENTA ANSI_COLOR_END
#define ANSI_SET_FG_CYAN_BOLD    ANSI_START ANSI_BOLD_ON ANSI_SEP \
                                 ANSI_FG_CYAN    ANSI_COLOR_END
#define ANSI_SET_FG_WHITE_BOLD   ANSI_START ANSI_BOLD_ON ANSI_SEP \
                                 ANSI_FG_BOLD    ANSI_COLOR_END



#endif
