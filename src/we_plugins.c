/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include "libintl-wrapper.h"
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

#include "kildclient.h"
#include "perlscript.h"


/*****************************************
 * Function prototypes - Startup Plugins *
 *****************************************/
static void     configure_startup_view(GtkTreeView *view, World *world);
/* XML UI signals */
G_MODULE_EXPORT void     we_splugin_add_cb(gpointer data, GtkButton *button);
G_MODULE_EXPORT void     we_splugin_remove_cb(gpointer data, GtkButton *button);
G_MODULE_EXPORT void     we_splugin_move_cb(gpointer data, GtkButton *button);
G_MODULE_EXPORT gboolean we_splugin_view_keypress_cb(GtkWidget   *widget,
                                                     GdkEventKey *evt,
                                                     gpointer     data);

/**************************************************
 * Function prototypes - Currently Loaded Plugins *
 **************************************************/
static GtkTreeModel *create_we_lplugins_model(World *world);
static void          configure_loaded_view(GtkTreeView  *view,
                                           GtkTreeModel *model);
static void          we_lplugin_ena_toggled_cb(GtkCellRendererToggle *renderer,
                                               gchar                 *pathstr,
                                               gpointer               data);
/* XML UI signals */
G_MODULE_EXPORT void we_lplugin_load_cb(gpointer data, GtkButton *button);
G_MODULE_EXPORT void we_lplugin_help_cb(gpointer data, GtkButton *button);


/*********************************
 * Function prototypes - Generic *
 *********************************/
static GtkWidget *we_plugin_get_load_filechooser(GtkWindow *parent);



GObject *
get_we_plugins_tab(GtkBuilder *ui_builder, World *world)
{
  GObject *panel;

  GObject *lstLoadedPlugins;
  GObject *btnLPluginLoad;

  GObject *lstStartupPlugins;
  GObject *btnSPluginUp;
  GObject *btnSPluginDown;

  panel = gtk_builder_get_object(ui_builder, "we_panel_plugins");

  /* Currently loaded plugins part */
  lstLoadedPlugins = gtk_builder_get_object(ui_builder, "lstLoadedPlugins");
  g_object_set_data(lstLoadedPlugins, "forworld", world);

  btnLPluginLoad = gtk_builder_get_object(ui_builder, "btnLPluginLoad");
  if (!world->connected) {
    gtk_widget_set_sensitive(GTK_WIDGET(btnLPluginLoad), FALSE);
  }

  world->plugin_model = create_we_lplugins_model(world);
  configure_loaded_view(GTK_TREE_VIEW(lstLoadedPlugins), world->plugin_model);


  /* Startup plugins part */
  lstStartupPlugins = gtk_builder_get_object(ui_builder, "lstStartupPlugins");
  g_object_set_data(lstStartupPlugins, "forworld", world);

  btnSPluginUp = gtk_builder_get_object(ui_builder, "btnSPluginUp");
  g_object_set_data(btnSPluginUp, "isup", GINT_TO_POINTER(TRUE));
  btnSPluginDown = gtk_builder_get_object(ui_builder, "btnSPluginDown");
  g_object_set_data(btnSPluginDown, "isup", GINT_TO_POINTER(FALSE));

  configure_startup_view(GTK_TREE_VIEW(lstStartupPlugins), world);

  return panel;
}


/* Startup plugins */
static
void
configure_startup_view(GtkTreeView *view, World *world)
{
  GtkCellRenderer   *renderer;
  GtkTreeViewColumn *column;

  gtk_tree_view_set_model(view, world->startup_plugins);

  if (world->connected) {
    /* Column 1 - Name */
    renderer = gtk_cell_renderer_text_new();
    column = gtk_tree_view_column_new();
    gtk_tree_view_column_set_resizable(column, TRUE);
    gtk_tree_view_column_set_expand(column, TRUE);
    gtk_tree_view_column_set_title(column, _("Name"));
    gtk_tree_view_column_pack_start(column, renderer, TRUE);
    gtk_tree_view_column_add_attribute(column,
                                       renderer,
                                       "text", SPLUGIN_NAME);
    gtk_tree_view_append_column(view, column);

    /* Column 2 - Description */
    renderer = gtk_cell_renderer_text_new();
    column = gtk_tree_view_column_new();
    gtk_tree_view_column_set_resizable(column, TRUE);
    gtk_tree_view_column_set_expand(column, TRUE);
    gtk_tree_view_column_set_title(column, _("Description"));
    gtk_tree_view_column_pack_start(column, renderer, TRUE);
    gtk_tree_view_column_add_attribute(column,
                                       renderer,
                                       "text", SPLUGIN_DESCRIPTION);
    gtk_tree_view_append_column(view, column);

    /* Column 3 - Author */
    renderer = gtk_cell_renderer_text_new();
    column = gtk_tree_view_column_new();
    gtk_tree_view_column_set_resizable(column, TRUE);
    gtk_tree_view_column_set_expand(column, TRUE);
    gtk_tree_view_column_set_title(column, _("Author"));
    gtk_tree_view_column_pack_start(column, renderer, TRUE);
    gtk_tree_view_column_add_attribute(column,
                                       renderer,
                                       "text", SPLUGIN_AUTHOR);
    gtk_tree_view_append_column(view, column);
  }

  /* Column 4 - File */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_column_set_title(column, _("File"));
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_column_add_attribute(column,
                                     renderer,
                                     "text", SPLUGIN_FILE);
  gtk_tree_view_append_column(view, column);


  gtk_tree_view_columns_autosize(view);
}


void
we_splugin_add_cb(gpointer data, GtkButton *button)
{
  World       *world;
  GtkTreeView *view;
  GtkWidget   *filechooser;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");

  filechooser = we_plugin_get_load_filechooser(GTK_WINDOW(world->dlgEditWorld));

  if (gtk_dialog_run(GTK_DIALOG(filechooser)) == GTK_RESPONSE_YES) {
    gchar  *filename;
    GError *error = NULL;

    gtk_widget_hide(filechooser);
    filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(filechooser));
    if (world->connected) {
      add_startup_plugin(world, filename, &error);
    } else {
      GtkTreeIter iter;

      gtk_list_store_append(GTK_LIST_STORE(world->startup_plugins), &iter);
      gtk_list_store_set(GTK_LIST_STORE(world->startup_plugins), &iter,
                         SPLUGIN_FILE, filename,
                         -1);

    }
    g_free(filename);
    if (error) {
      kc_error_dialog_with_title(GTK_WINDOW(world->dlgEditWorld),
                                 _("Error loading plugin"),
                                 "%s", error->message);
      g_error_free(error);
    }
  }
  gtk_widget_destroy(filechooser);
}


void
we_splugin_remove_cb(gpointer data, GtkButton *button)
{
  GtkTreeView      *view;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  GtkTreeIter       iter;

  view = (GtkTreeView *) data;
  selection = gtk_tree_view_get_selection(view);
  if (gtk_tree_selection_get_selected(selection, &model, &iter)) {
    gtk_list_store_remove(GTK_LIST_STORE(model), &iter);
  }
}


void
we_splugin_move_cb(gpointer data, GtkButton *button)
{
  GtkTreeView      *view;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  GtkTreeIter       iter;
  GtkTreeIter       other_iter;
  gboolean          is_up;

  view = (GtkTreeView *) data;
  selection = gtk_tree_view_get_selection(view);
  is_up = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(button), "isup"));
  if (gtk_tree_selection_get_selected(selection, &model, &iter)) {
    if (is_up) {
      GtkTreePath *path;

      path = gtk_tree_model_get_path(model, &iter);
      if (gtk_tree_path_prev(path)) {
        gtk_tree_model_get_iter(model, &other_iter, path);
        gtk_list_store_swap(GTK_LIST_STORE(model), &iter, &other_iter);
      }
      gtk_tree_path_free(path);
    } else {
      other_iter = iter;
      if (gtk_tree_model_iter_next(model, &other_iter)) {
        gtk_list_store_swap(GTK_LIST_STORE(model), &iter, &other_iter);
      }
    }
  }
}


gboolean
we_splugin_view_keypress_cb(GtkWidget   *widget, /* The treeview */
                            GdkEventKey *evt,
                            gpointer     data)
{
  if (evt->keyval == GDK_KEY_Delete || evt->keyval == GDK_KEY_KP_Delete) {
    we_splugin_remove_cb(widget, NULL);
    return TRUE;
  }

  return FALSE;
}


/* Currently loaded plugins */
static
GtkTreeModel *
create_we_lplugins_model(World *world)
{
  GtkListStore *store;
  GtkTreeIter   iter;
  GSList       *pluginptr;
  Plugin       *plugin;

  store = gtk_list_store_new(WE_GUIED_N_COLS,
                             G_TYPE_POINTER);

  pluginptr = world->plugins;
  while (pluginptr) {
    plugin = (Plugin *) pluginptr->data;
    gtk_list_store_append(store, &iter);
    gtk_list_store_set(store, &iter,
                       WE_GUIED_POINTER, plugin,
                       -1);

    pluginptr = pluginptr->next;
  }

  return GTK_TREE_MODEL(store);
}


static
void
configure_loaded_view(GtkTreeView *view, GtkTreeModel *model)
{
  GtkCellRenderer   *renderer;
  GtkTreeViewColumn *column;

  gtk_tree_view_set_model(view, model);
  g_object_unref(model);

  /* Column 1 - Enabled? */
  renderer = gtk_cell_renderer_toggle_new();
  g_object_set(G_OBJECT(renderer), "activatable", TRUE, NULL);
  g_signal_connect(G_OBJECT(renderer), "toggled",
                   G_CALLBACK(we_lplugin_ena_toggled_cb), view);
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Enabled"));
  gtk_tree_view_column_pack_start(column, renderer, FALSE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_bool_func,
                                          GINT_TO_POINTER(PLUGIN_ENABLED),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 2 - Name */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Name"));
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_text_func,
                                          GINT_TO_POINTER(PLUGIN_NAME),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 3 - Version */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Version"));
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_text_func,
                                          GINT_TO_POINTER(PLUGIN_VERSION),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 4 - Author */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Author"));
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_text_func,
                                          GINT_TO_POINTER(PLUGIN_AUTHOR),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 5 - Description */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Description"));
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_text_func,
                                          GINT_TO_POINTER(PLUGIN_DESCRIPTION),
                                          NULL);
  gtk_tree_view_append_column(view, column);


  gtk_tree_view_columns_autosize(view);
}

static
void
we_lplugin_ena_toggled_cb(GtkCellRendererToggle *renderer,
                          gchar                 *pathstr,
                          gpointer               data)
{
  GtkTreeView      *view;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  GtkTreeIter       iter;
  World            *world;
  Plugin           *plugin;

  view      = (GtkTreeView *) data;
  model     = gtk_tree_view_get_model(view);
  selection = gtk_tree_view_get_selection(view);
  world     = (World *) g_object_get_data(G_OBJECT(view), "forworld");
  gtk_tree_model_get_iter_from_string(model, &iter, pathstr);
  if (gtk_tree_selection_iter_is_selected(selection, &iter)) {
    gchar *cmd;

    gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &plugin, -1);
    if (plugin->enabled) {
      cmd = g_strdup_printf("$::world->displugin('%s');", plugin->name);
    } else {
      cmd = g_strdup_printf("$::world->enaplugin('%s');", plugin->name);
    }
    GRAB_PERL(world);
    eval_pv(cmd, TRUE);
    RELEASE_PERL(world);
    g_free(cmd);
  }
}


void
we_lplugin_load_cb(gpointer data, GtkButton *button)
{
  World        *world;
  GtkTreeView  *view;
  GtkWidget    *filechooser;
  GtkWidget    *chkAtStartup;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");

  filechooser = we_plugin_get_load_filechooser(GTK_WINDOW(world->dlgEditWorld));

  chkAtStartup = gtk_check_button_new_with_mnemonic(_("Always load this plugin at _startup"));
  gtk_file_chooser_set_extra_widget(GTK_FILE_CHOOSER(filechooser),
                                    chkAtStartup);

  if (gtk_dialog_run(GTK_DIALOG(filechooser)) == GTK_RESPONSE_YES) {
    gchar  *filename;
    GError *error = NULL;

    gtk_widget_hide(filechooser);
    filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(filechooser));
    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(chkAtStartup))) {
      add_startup_plugin(world, filename, &error);
    }
    if (error == NULL) {
      load_plugin(world, filename, &error);
    }
    if (error) {
      kc_error_dialog_with_title(GTK_WINDOW(world->dlgEditWorld),
                                 _("Error loading plugin"),
                                 "%s", error->message);
      g_error_free(error);
    }
  }
  gtk_widget_destroy(filechooser);
}


void
we_lplugin_help_cb(gpointer data, GtkButton *button)
{
  World            *world;
  GtkTreeView      *view;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  GtkTreeIter       iter;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");
  selection = gtk_tree_view_get_selection(view);
  if (gtk_tree_selection_get_selected(selection, &model, &iter)) {
    Plugin    *plugin;
    gchar     *cmd;
    GtkWidget *msgdlg;

    gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &plugin, -1);
    cmd = g_strdup_printf("%s::help();", plugin->name);
    GRAB_PERL(world);
    eval_pv(cmd, FALSE);
    RELEASE_PERL(world);
    g_free(cmd);

    /* Show message telling where the help appears. */
    if (!globalPrefs.no_plugin_help_msg) {
      GtkWidget *chk;
      msgdlg = gtk_message_dialog_new(GTK_WINDOW(world->dlgEditWorld),
                                      GTK_DIALOG_MODAL,
                                      GTK_MESSAGE_INFO,
                                      GTK_BUTTONS_OK,
                                      _("The help is displayed in main window."));
      gtk_window_set_title(GTK_WINDOW(msgdlg), _("KildClient information"));
      chk = gtk_check_button_new_with_mnemonic(_("_Do not display this message again"));
      gtk_box_pack_start(GTK_BOX(gtk_dialog_get_content_area(GTK_DIALOG(msgdlg))),
                         chk,
                         FALSE, FALSE, 4);

      gtk_widget_show_all(msgdlg);
      gtk_dialog_run(GTK_DIALOG(msgdlg));

      if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(chk))) {
        globalPrefs.no_plugin_help_msg = TRUE;
      }

      gtk_widget_destroy(msgdlg);
    }
  }
}


void
we_lplugin_insert_plugin(World *world, Plugin *plugin)
{
  /* Called by the Perl functions when an plugin is inserted, so that
     the display is updated. */
  GtkTreeIter iter;

  if (!world->plugin_model) {
    return;
  }

  gtk_list_store_append(GTK_LIST_STORE(world->plugin_model), &iter);
  gtk_list_store_set(GTK_LIST_STORE(world->plugin_model), &iter,
                     WE_GUIED_POINTER, plugin,
                     -1);
}


void
we_lplugin_update_plugin(World *world, Plugin *plugin_arg)
{
  /* Called by the Perl functions when an plugin is changed, so that
     the display is updated. */
  GtkTreeIter  iter;
  GtkTreePath *path;
  Plugin      *plugin;
  gboolean     success;

  if (!world->plugin_model) {
    return;
  }

  success = gtk_tree_model_get_iter_first(world->plugin_model, &iter);
  while (success) {
    gtk_tree_model_get(world->plugin_model, &iter,
                       WE_GUIED_POINTER, &plugin, -1);
    if (plugin == plugin_arg) {
      path = gtk_tree_model_get_path(world->plugin_model, &iter);
      gtk_tree_model_row_changed(world->plugin_model, path, &iter);
      gtk_tree_path_free(path);
      return;
    }
    success = gtk_tree_model_iter_next(world->plugin_model, &iter);
  }
}


static
GtkWidget *
we_plugin_get_load_filechooser(GtkWindow *parent)
{
  const gchar *plugindir;
  GtkWidget   *filechooser;

  filechooser = gtk_file_chooser_dialog_new(_("Load plugin"),
                                           parent,
                                           GTK_FILE_CHOOSER_ACTION_OPEN,
                                           _("_Cancel"),
                                           GTK_RESPONSE_CANCEL,
                                           _("_Open"),
                                           GTK_RESPONSE_YES,
                                           NULL);
  plugindir = get_kildclient_installed_file("plugins");
  gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(filechooser),
                                      plugindir);
  gtk_file_chooser_add_shortcut_folder(GTK_FILE_CHOOSER(filechooser),
                                       plugindir,
                                       NULL);
#ifndef __MINGW32__
  plugindir = g_strdup_printf("%s/plugins",
                              get_kildclient_directory_path());
#else
  plugindir = g_strdup_printf("%s\\plugins",
                              get_kildclient_directory_path());
#endif
  gtk_file_chooser_add_shortcut_folder(GTK_FILE_CHOOSER(filechooser),
                                       plugindir,
                                       NULL);

  return filechooser;
}
