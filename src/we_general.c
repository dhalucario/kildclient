/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include "libintl-wrapper.h"
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

#include "kildclient.h"
#include "perlscript.h"


/***********************
 * Function prototypes *
 ***********************/
static gboolean edit_char(World        *world,
                          GtkTreeModel *model,
                          GtkTreeIter  *iter);
/* XML UI signals */
G_MODULE_EXPORT void     we_general_add_edit_char_cb(GtkWidget *widget,
                                                     gpointer data);
G_MODULE_EXPORT void     we_general_delete_char_cb(GtkWidget *widget,
                                                   gpointer data);
G_MODULE_EXPORT void     we_general_move_char_cb(gpointer data,
                                                 GtkWidget *widget);
G_MODULE_EXPORT void     we_general_view_row_dblclick_cb(GtkTreeView  *view,
                                                         GtkTreePath  *path,
                                                         GtkTreeViewColumn *col,
                                                         gpointer     data);
G_MODULE_EXPORT gboolean we_general_view_keypress_cb(GtkWidget   *widget,
                                                     GdkEventKey *evt,
                                                     gpointer     data);



GObject   *
get_we_general_tab(World *world, GtkBuilder *ui_builder)
{
  GObject           *panel;
  GtkTreeView       *viewCharacters;
  GtkCellRenderer   *renderer;
  GtkTreeViewColumn *column;

  panel = gtk_builder_get_object(ui_builder, "we_panel_general");

  /* Configure view */
  viewCharacters
    = GTK_TREE_VIEW(gtk_builder_get_object(ui_builder, "viewCharacters"));
  gtk_tree_view_set_model(viewCharacters, world->logon_characters);
  renderer = gtk_cell_renderer_text_new();
  gtk_tree_view_insert_column_with_attributes(viewCharacters,
                                              0,
                                              _("Character"),
                                              renderer,
                                              "text", LOGON_CHAR,
                                              NULL);

  renderer = gtk_cell_renderer_text_new();
  g_object_set(G_OBJECT(renderer), "text", "******", NULL);
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_title(column, _("Password"));
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_append_column(viewCharacters, column);

  return panel;
}


void
fill_we_general_tab(World *world, GtkBuilder *ui_builder)
{
  GtkEntry          *txtName;
  GtkEntry          *txtHost;
  GtkEntry          *txtPort;
  GtkWidget         *chkTLS;
  GtkComboBox       *cmbProxyType;
  GtkEntry          *txtProxyServer;
  GtkEntry          *txtProxyPort;
  GtkEntry          *txtProxyUser;
  GtkEntry          *txtProxyPassword;
  GtkComboBox       *cmbConnStyle;
  GObject           *btnAdd;
  GObject           *btnEdit;
  GObject           *btnUp;
  GObject           *btnDown;

  if (!world) {
    return;
  }

  txtName          = GTK_ENTRY(gtk_builder_get_object(ui_builder, "txtName"));
  txtHost          = GTK_ENTRY(gtk_builder_get_object(ui_builder, "txtHost"));
  txtPort          = GTK_ENTRY(gtk_builder_get_object(ui_builder, "txtPort"));
  chkTLS           = GTK_WIDGET(gtk_builder_get_object(ui_builder, "chkTLS"));
  cmbProxyType     = GTK_COMBO_BOX(gtk_builder_get_object(ui_builder,
                                                          "cmbProxyType"));
  txtProxyServer   = GTK_ENTRY(gtk_builder_get_object(ui_builder,
                                                      "txtProxyServer"));
  txtProxyPort     = GTK_ENTRY(gtk_builder_get_object(ui_builder,
                                                      "txtProxyPort"));
  txtProxyUser     = GTK_ENTRY(gtk_builder_get_object(ui_builder,
                                                      "txtProxyUser"));
  txtProxyPassword = GTK_ENTRY(gtk_builder_get_object(ui_builder,
                                                      "txtProxyPassword"));
  cmbConnStyle     = GTK_COMBO_BOX(gtk_builder_get_object(ui_builder,
                                                          "cmbConnStyle"));
  btnAdd           = gtk_builder_get_object(ui_builder, "btnCharAdd");
  btnEdit          = gtk_builder_get_object(ui_builder, "btnCharEdit");
  btnUp            = gtk_builder_get_object(ui_builder, "btnCharUp");
  btnDown          = gtk_builder_get_object(ui_builder, "btnCharDown");

  /* General connection settings */
  if (world->name) {
    gtk_entry_set_text(txtName, world->name);
  }
  if (world->host) {
    gtk_entry_set_text(txtHost, world->host);
  }
  if (world->port) {
    gtk_entry_set_text(txtPort, world->port);
  }
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(chkTLS), world->use_tls);

  /* Proxy */
  /* -1 is "global settings in enumeration */
  gtk_combo_box_set_active(cmbProxyType, world->proxy.type + 1);
  if (world->proxy.server) {
    gtk_entry_set_text(txtProxyServer, world->proxy.server);
  } else {
    gtk_entry_set_text(txtProxyServer, "");
  }
  if (world->proxy.port) {
    gtk_entry_set_text(txtProxyPort, world->proxy.port);
  } else {
    gtk_entry_set_text(txtProxyPort, "");
  }
  if (world->proxy.user) {
    gtk_entry_set_text(txtProxyUser, world->proxy.user);
  } else {
    gtk_entry_set_text(txtProxyUser, "");
  }
  if (world->proxy.password) {
    gtk_entry_set_text(txtProxyPassword, world->proxy.password);
  } else {
    gtk_entry_set_text(txtProxyPassword, "");
  }

  /* Auto-login*/
  gtk_combo_box_set_active(cmbConnStyle, world->connection_style);

  /* Information used in the callbacks */
  g_object_set_data(btnAdd, "isnew", GINT_TO_POINTER(TRUE));
  g_object_set_data(btnEdit, "isnew", GINT_TO_POINTER(FALSE));
  g_object_set_data(btnUp, "isup", GINT_TO_POINTER(TRUE));
  g_object_set_data(btnDown, "isup", GINT_TO_POINTER(FALSE));
}


void
update_we_general_parameters(World *world, GtkBuilder *ui_builder)
{
  const gchar     *newval;
  GtkEntry        *txtName;
  GtkEntry        *txtHost;
  GtkEntry        *txtPort;
  GtkComboBox     *cmbProxyType;
  GtkEntry        *txtProxyServer;
  GtkEntry        *txtProxyPort;
  GtkEntry        *txtProxyUser;
  GtkEntry        *txtProxyPassword;
  GtkComboBox     *cmbConnStyle;
  GtkToggleButton *chkTLS;

  txtName          = GTK_ENTRY(gtk_builder_get_object(ui_builder, "txtName"));
  txtHost          = GTK_ENTRY(gtk_builder_get_object(ui_builder, "txtHost"));
  txtPort          = GTK_ENTRY(gtk_builder_get_object(ui_builder, "txtPort"));
  cmbProxyType     = GTK_COMBO_BOX(gtk_builder_get_object(ui_builder,
                                                          "cmbProxyType"));
  txtProxyServer   = GTK_ENTRY(gtk_builder_get_object(ui_builder,
                                                      "txtProxyServer"));
  txtProxyPort     = GTK_ENTRY(gtk_builder_get_object(ui_builder,
                                                      "txtProxyPort"));
  txtProxyUser     = GTK_ENTRY(gtk_builder_get_object(ui_builder,
                                                      "txtProxyUser"));
  txtProxyPassword = GTK_ENTRY(gtk_builder_get_object(ui_builder,
                                                      "txtProxyPassword"));
  cmbConnStyle     = GTK_COMBO_BOX(gtk_builder_get_object(ui_builder,
                                                          "cmbConnStyle"));

  /* General settings */
  newval = gtk_entry_get_text(txtName);
  if (!world->name || strcmp(world->name, newval) != 0) {
    g_free(world->name);
    world->name = g_strdup(newval);
  }

  newval = gtk_entry_get_text(txtHost);
  if (!world->host || strcmp(world->host, newval) != 0) {
    g_free(world->host);
    world->host = g_strdup(newval);
  }

  newval = gtk_entry_get_text(txtPort);
  if (!world->port || strcmp(world->port, newval) != 0) {
    g_free(world->port);
    world->port = g_strdup(newval);
  }

  /* Proxy */
  /* -1 is "global settings" in enumeration */
  world->proxy.type = gtk_combo_box_get_active(cmbProxyType) - 1;

  newval = gtk_entry_get_text(txtProxyServer);
  if (!world->proxy.server || strcmp(world->proxy.server, newval) != 0) {
    g_free(world->proxy.server);
    world->proxy.server = g_strdup(newval);
  }

  newval = gtk_entry_get_text(txtProxyPort);
  if (!world->proxy.port || strcmp(world->proxy.port, newval) != 0) {
    g_free(world->proxy.port);
    world->proxy.port = g_strdup(newval);
  }

  newval = gtk_entry_get_text(txtProxyUser);
  if (!world->proxy.user || strcmp(world->proxy.user, newval) != 0) {
    g_free(world->proxy.user);
    world->proxy.user = g_strdup(newval);
  }

  newval = gtk_entry_get_text(txtProxyPassword);
  if (!world->proxy.password || strcmp(world->proxy.password, newval) != 0) {
    g_free(world->proxy.password);
    world->proxy.password = g_strdup(newval);
  }

  /* Auto-logon */
  world->connection_style = gtk_combo_box_get_active(cmbConnStyle);

  chkTLS = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "chkTLS"));
  world->use_tls = gtk_toggle_button_get_active(chkTLS);
}


void
we_general_add_edit_char_cb(GtkWidget *widget, gpointer data)
{
  World            *world = (World *) data;
  gboolean          is_new;
  GtkTreeView      *view;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  GtkTreeIter       iter;

  view = GTK_TREE_VIEW(gtk_builder_get_object(world->ui_builder,
                                              "viewCharacters"));
  model = gtk_tree_view_get_model(view);

  is_new = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(widget), "isnew"));
  if (is_new) {
    gtk_list_store_append(GTK_LIST_STORE(model), &iter);
  } else {
    selection = gtk_tree_view_get_selection(view);
    if (!gtk_tree_selection_get_selected(selection, NULL, &iter)) {
      return;
    }
  }

  if (!edit_char(world, model, &iter) && is_new) {
    gtk_list_store_remove(GTK_LIST_STORE(model), &iter);
  }
}


void
we_general_view_row_dblclick_cb(GtkTreeView        *view,
                                GtkTreePath        *path,
                                GtkTreeViewColumn  *col,
                                gpointer            data)
{
  World        *world;
  GtkTreeModel *model;
  GtkTreeIter   iter;

  world = (World *) data;
  model = gtk_tree_view_get_model(view);

  if (gtk_tree_model_get_iter(model, &iter, path)) {
    edit_char(world, model, &iter);
  }
}


static
gboolean
edit_char(World *world, GtkTreeModel *model, GtkTreeIter *iter)
{
  GtkBuilder  *ui_builder;
  gchar       *objects[] = { "dlgEditChar", NULL };
  GError      *error = NULL;
  GtkWidget   *dlgEditChar;
  GtkEntry    *txtName;
  GtkEntry    *txtPassword;
  gchar       *name = NULL;
  gchar       *password = NULL;
  const gchar *newname;
  const gchar *newpassword;

  ui_builder = gtk_builder_new();
  if (!gtk_builder_add_objects_from_resource(ui_builder,
                                             "/ekalin/kildclient/kildclient.ui",
                                             objects,
                                             &error)) {
    g_warning(_("Error loading UI from XML file: %s"), error->message);
    g_error_free(error);
    return FALSE;
  }

  dlgEditChar = GTK_WIDGET(gtk_builder_get_object(ui_builder, "dlgEditChar"));
  gtk_window_set_transient_for(GTK_WINDOW(dlgEditChar),
                               GTK_WINDOW(world->dlgEditWorld));
  gtk_dialog_set_default_response(GTK_DIALOG(dlgEditChar), GTK_RESPONSE_OK);

  txtName     = GTK_ENTRY(gtk_builder_get_object(ui_builder, "txtCharName"));
  txtPassword = GTK_ENTRY( gtk_builder_get_object(ui_builder,
                                                  "txtCharPassword"));

  /* We don't need it anymore */
  g_object_unref(ui_builder);

  /* Fill-in values */
  gtk_tree_model_get(model, iter,
                     LOGON_CHAR, &name,
                     LOGON_PASS, &password,
                     -1);
  if (name) {
    gtk_entry_set_text(txtName, name);
    g_free(name);
  }
  if (password) {
    gtk_entry_set_text(txtPassword, password);
    g_free(password);
  }

  /* Run the dialog until the input is valid or cancelled */
  gtk_widget_show_all(dlgEditChar);
  while (1) {
    if (gtk_dialog_run(GTK_DIALOG(dlgEditChar)) == GTK_RESPONSE_OK) {
      /* Validate */
      newname = gtk_entry_get_text(txtName);
      if (strcmp(newname, "") == 0) {
        kc_error_dialog(GTK_WINDOW(dlgEditChar),
                        _("You must specify the name."));
        continue;
      }

      newpassword = gtk_entry_get_text(txtPassword);
      if (strcmp(newpassword, "") == 0) {
        kc_error_dialog(GTK_WINDOW(dlgEditChar),
                        _("You must specify the password."));
        continue;
      }

      /* Update values */
      gtk_list_store_set(GTK_LIST_STORE(model), iter,
                         LOGON_CHAR, newname,
                         LOGON_PASS, newpassword,
                         -1);

      /* We've finished successfully */
      gtk_widget_destroy(dlgEditChar);
      return TRUE;
    } else {
      /* Cancel pressed */
      gtk_widget_destroy(dlgEditChar);
      return FALSE;
    }
  }
}


void
we_general_delete_char_cb(GtkWidget *widget, gpointer data)
{
  World            *world = (World *) data;
  GtkTreeView      *view;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  GtkTreeIter       iter;

  view = GTK_TREE_VIEW(gtk_builder_get_object(world->ui_builder,
                                              "viewCharacters"));
  selection = gtk_tree_view_get_selection(view);
  if (gtk_tree_selection_get_selected(selection, &model, &iter)) {
    GtkWidget *msgdlg;

    msgdlg = we_guied_confirm_delete_dialog_new(GTK_WINDOW(world->dlgEditWorld),
                                                _("character"), 1);

    if (!world->confirm_delete
        || gtk_dialog_run(GTK_DIALOG(msgdlg)) == GTK_RESPONSE_YES) {
      gtk_list_store_remove(GTK_LIST_STORE(model), &iter);
    }
    gtk_widget_destroy(msgdlg);
  }
}


gboolean
we_general_view_keypress_cb(GtkWidget   *widget,
                            GdkEventKey *evt,
                            gpointer     data)
{
  if (evt->keyval == GDK_KEY_Delete || evt->keyval == GDK_KEY_KP_Delete) {
    we_general_delete_char_cb(widget, data);
    return TRUE;
  }

  return FALSE;
}


void
we_general_move_char_cb(gpointer data, GtkWidget *widget)
{
  GtkTreeView      *view;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  GtkTreeIter       iter;
  GtkTreeIter       other_iter;
  gboolean          is_up;

  view = (GtkTreeView *) data;
  selection = gtk_tree_view_get_selection(view);
  is_up = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(widget), "isup"));
  if (gtk_tree_selection_get_selected(selection, &model, &iter)) {
    if (is_up) {
      GtkTreePath *path;

      path = gtk_tree_model_get_path(model, &iter);
      if (gtk_tree_path_prev(path)) {
        gtk_tree_model_get_iter(model, &other_iter, path);
        gtk_list_store_swap(GTK_LIST_STORE(model), &iter, &other_iter);
      }
      gtk_tree_path_free(path);
    } else {
      other_iter = iter;
      if (gtk_tree_model_iter_next(model, &other_iter)) {
        gtk_list_store_swap(GTK_LIST_STORE(model), &iter, &other_iter);
      }
    }
  }
}
