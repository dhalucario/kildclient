/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include "libintl-wrapper.h"
#include <locale.h>
#include <gtk/gtk.h>

#include "kildclient.h"
#include "perlscript.h"


/*************************
 * File global variables *
 *************************/


/***********************
 * Function prototypes *
 ***********************/



char *
substitute_aliases(World *world, const char *cmd)
{
  GSList *aliasptr;
  Alias  *alias;
  char   *result;
  int     i;

  if (world->disable_aliases) {
    return NULL;
  }

  GRAB_PERL(world);

  aliasptr = world->aliases;
  i = 0;
  while (aliasptr) {
    alias = (Alias *) aliasptr->data;
    if (alias->enabled && (result = perl_substitute(cmd,
                                                    alias->pattern_re,
                                                    alias->substitution,
                                                    alias->perl_eval,
                                                    FALSE))) {
      if (debug_matches) {
        fprintf(stderr,
                "**Alias %d '%s' matched for world %s\n"
                "  Command: %s\n"
                "  Pattern: %s\n"
                "  Substitution: %s\n"
                "  Eval as Perl: %d\n"
                "  Result: %s\n",
                i,
                alias->name ? alias->name : "",
                world->name,
                cmd,
                alias->pattern,
                alias->substitution,
                alias->perl_eval,
                result);
      }

      RELEASE_PERL(world);
      return result;
    }
    aliasptr = aliasptr->next;
    ++i;
  }

  RELEASE_PERL(world);

  return NULL;
}


void
remove_alias(World *world, GSList *aliasitem)
{
  Alias *alias = (Alias *) aliasitem->data;

  /* Necessary when free_alias dereferences alias->pattern_re */
  if (world->perl_interpreter) {
    GRAB_PERL(world);
  }

  we_alias_delete_alias(world, alias);

  world->aliases = g_slist_remove_link(world->aliases, aliasitem);
  if (!alias->owner_plugin) {
    --world->alias_pos;
  }
  free_alias(alias, NULL);
  g_slist_free(aliasitem);

  if (world->perl_interpreter) {
    RELEASE_PERL(world);
  }
}


gboolean
move_alias(World *world, gint old_pos, gint new_pos)
{
  GSList   *aliasitem;
  gpointer  alias;

  aliasitem = g_slist_nth(world->aliases, old_pos);
  if (!aliasitem) {
    return FALSE;
  }
  alias = aliasitem->data;

  world->aliases = g_slist_delete_link(world->aliases, aliasitem);
  world->aliases = g_slist_insert(world->aliases, alias, new_pos);

  we_alias_delete_alias(world, alias);
  we_alias_insert_alias(world, alias, new_pos);

  return TRUE;
}


void
free_alias(Alias *alias, gpointer data)
{
  g_free(alias->name);
  g_free(alias->pattern);
  g_free(alias->substitution);
  SvREFCNT_dec(alias->pattern_re);

  g_free(alias);
}


void
list_aliases(World *world, Plugin *plugin)
{
  guint   i;
  guint   total_width;
  guint   field_width;
  GSList *aliasptr;
  Alias  *alias;

  /* The rows argument is not used. */
  ansitextview_get_size(world->gui, &field_width, &total_width);

  /* If the screen is really narrow, we can do nothing. */
  if (total_width < 30) {
    total_width = 30;
  }
  field_width = (total_width - 18)/2;

  ansitextview_append_stringf(world->gui,
                              _("Num Eval Ena IgC %-*.*s %-*.*s\n"),
                              field_width, field_width, _("Pattern"),
                              field_width, field_width, _("Substitution"));
  ansitextview_append_string(world->gui, "--- ---- --- --- ");
  for (i = 0; i < field_width; ++i) {
    ansitextview_append_string(world->gui, "-");
  }
  ansitextview_append_string(world->gui, " ");
  for (i = 0; i < field_width; ++i) {
    ansitextview_append_string(world->gui, "-");
  }
  ansitextview_append_string(world->gui, "\n");

  i = 0;
  aliasptr = world->aliases;
  while (aliasptr) {
    alias = (Alias *) aliasptr->data;

    if (alias->owner_plugin == plugin) {
      ansitextview_append_stringf(world->gui,
                                  "%3d %-4.4s %-3.3s %-3.3s %-*.*s %-*.*s\n",
                                  i,
                                  alias->perl_eval   ? _("y") : _("n"),
                                  alias->enabled     ? _("y") : _("n"),
                                  alias->ignore_case ? _("y") : _("n"),
                                  field_width, field_width,
                                  alias->pattern,
                                  field_width, field_width,
                                  alias->substitution);
    }

    ++i;
    aliasptr = aliasptr->next;
  }
}


void
alias_precompute_res(World *world)
{
  GSList *aliasptr = world->aliases;
  Alias  *alias;

  GRAB_PERL(world);

  while (aliasptr) {
    alias = (Alias *) aliasptr->data;

    /* Aliases loaded from a script file already have the precomputed RE */
    if (!alias->pattern_re) {
      alias->pattern_re = precompute_re(alias->pattern, alias->ignore_case);
    }

    aliasptr = aliasptr->next;
  }

  RELEASE_PERL(world);
}


void
save_alias(GString *str, Alias *alias)
{
  g_string_append(str, "    <alias ");
  if (alias->name) {
    kc_g_string_append_escaped(str, "name=\"%s\" ", alias->name);
  }
  g_string_append_printf(str,
                         "perleval=\"%d\" enabled=\"%d\" ignorecase=\"%d\">\n",
                         alias->perl_eval,
                         alias->enabled,
                         alias->ignore_case);
  kc_g_string_append_escaped(str,
                             "      <pattern>%s</pattern>\n"
                             "      <substitution>%s</substitution>\n",
                             alias->pattern,
                             alias->substitution);
  g_string_append(str, "    </alias>\n");
}
