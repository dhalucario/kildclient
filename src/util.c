/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <stdarg.h>
#include "libintl-wrapper.h"
#include <gtk/gtk.h>

#include "kildclient.h"


/***********************
 * Function prototypes *
 ***********************/
static void kc_error_dialog_internal(GtkWindow   *parent,
                                     const gchar *title,
                                     const gchar *format,
                                     va_list      args);



void
add_css_provider_and_class(GtkWidget      *widget,
                           GtkCssProvider *provider,
                           const gchar    *class)
{
  GtkStyleContext *context;

  context = gtk_widget_get_style_context(widget);
  gtk_style_context_add_provider(context,
                                 GTK_STYLE_PROVIDER(provider),
                                 GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
  gtk_style_context_add_class(context, class);
}


gchar *
pango_font_to_css(const gchar *pango_font)
{
  PangoFontDescription *font;
  GString              *s;
  PangoFontMask         set;

  font = pango_font_description_from_string(pango_font);
  s = g_string_new (NULL);

  set = pango_font_description_get_set_fields(font);
  if (set & PANGO_FONT_MASK_FAMILY) {
    g_string_append(s, "font-family: ");
    g_string_append(s, pango_font_description_get_family(font));
    g_string_append(s, "; ");
  }

  if (set & PANGO_FONT_MASK_STYLE) {
    switch (pango_font_description_get_style(font)) {
    case PANGO_STYLE_NORMAL:
      g_string_append(s, "font-style: normal; ");
      break;
    case PANGO_STYLE_OBLIQUE:
      g_string_append(s, "font-style: oblique; ");
      break;
    case PANGO_STYLE_ITALIC:
      g_string_append(s, "font-style: italic; ");
      break;
    }
  }

  if (set & PANGO_FONT_MASK_VARIANT) {
    switch (pango_font_description_get_variant(font)) {
    case PANGO_VARIANT_NORMAL:
      g_string_append(s, "font-variant: normal; ");
      break;
    case PANGO_VARIANT_SMALL_CAPS:
      g_string_append(s, "font-variant: small-caps; ");
      break;
    default:
      // Other attributes are not supported
      break;
    }
  }

  if (set & PANGO_FONT_MASK_WEIGHT) {
    switch (pango_font_description_get_weight(font)) {
    case PANGO_WEIGHT_THIN:
      g_string_append(s, "font-weight: 100; ");
      break;
    case PANGO_WEIGHT_ULTRALIGHT:
      g_string_append(s, "font-weight: 200; ");
      break;
    case PANGO_WEIGHT_LIGHT:
    case PANGO_WEIGHT_SEMILIGHT:
      g_string_append(s, "font-weight: 300; ");
      break;
    case PANGO_WEIGHT_BOOK:
    case PANGO_WEIGHT_NORMAL:
      g_string_append(s, "font-weight: 400; ");
      break;
    case PANGO_WEIGHT_MEDIUM:
      g_string_append(s, "font-weight: 500; ");
      break;
    case PANGO_WEIGHT_SEMIBOLD:
      g_string_append(s, "font-weight: 600; ");
      break;
    case PANGO_WEIGHT_BOLD:
      g_string_append(s, "font-weight: 700; ");
      break;
    case PANGO_WEIGHT_ULTRABOLD:
      g_string_append(s, "font-weight: 800; ");
      break;
    case PANGO_WEIGHT_HEAVY:
    case PANGO_WEIGHT_ULTRAHEAVY:
      g_string_append(s, "font-weight: 900; ");
      break;
    }
  }

  if (set & PANGO_FONT_MASK_STRETCH) {
    switch (pango_font_description_get_stretch(font)) {
    case PANGO_STRETCH_ULTRA_CONDENSED:
      g_string_append(s, "font-stretch: ultra-condensed; ");
      break;
    case PANGO_STRETCH_EXTRA_CONDENSED:
      g_string_append(s, "font-stretch: extra-condensed; ");
      break;
    case PANGO_STRETCH_CONDENSED:
      g_string_append(s, "font-stretch: condensed; ");
      break;
    case PANGO_STRETCH_SEMI_CONDENSED:
      g_string_append(s, "font-stretch: semi-condensed; ");
      break;
    case PANGO_STRETCH_NORMAL:
      g_string_append(s, "font-stretch: normal; ");
      break;
    case PANGO_STRETCH_SEMI_EXPANDED:
      g_string_append(s, "font-stretch: semi-expanded; ");
      break;
    case PANGO_STRETCH_EXPANDED:
      g_string_append(s, "font-stretch: expanded; ");
      break;
    case PANGO_STRETCH_EXTRA_EXPANDED:
      g_string_append(s, "font-stretch: extra-expanded; ");
      break;
    case PANGO_STRETCH_ULTRA_EXPANDED:
      g_string_append(s, "font-stretch: ultra-expanded; ");
      break;
    }
  }
  if (set & PANGO_FONT_MASK_SIZE) {
    g_string_append_printf(s, "font-size: %dpt",
                           pango_font_description_get_size (font) / PANGO_SCALE);
  }

  pango_font_description_free(font);
  return g_string_free(s, FALSE);
}


void
kc_error_dialog(GtkWindow *parent, const gchar *format, ...)
{
  va_list args;

  va_start(args, format);
  kc_error_dialog_internal(parent, _("KildClient error"), format, args);
  va_end(args);
}


void
kc_error_dialog_with_title(GtkWindow   *parent,
                           const gchar *title,
                           const gchar *format, ...)
{
  va_list args;

  va_start(args, format);
  kc_error_dialog_internal(parent, title, format, args);
  va_end(args);
}


static
void
kc_error_dialog_internal(GtkWindow   *parent,
                         const gchar *title,
                         const gchar *format,
                         va_list      args)
{
  gchar     *msg;
  GtkWidget *dlg;

  msg = g_strdup_vprintf(format, args);

  dlg = gtk_message_dialog_new(parent,
                               GTK_DIALOG_MODAL,
                               GTK_MESSAGE_ERROR,
                               GTK_BUTTONS_OK,
                               "%s", msg);
  gtk_window_set_title(GTK_WINDOW(dlg), title);

  gtk_dialog_run(GTK_DIALOG(dlg));

  gtk_widget_destroy(dlg);
  g_free(msg);
}
