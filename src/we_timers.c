/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include "libintl-wrapper.h"
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

#include "kildclient.h"
#include "perlscript.h"


/***********************
 * Function prototypes *
 ***********************/
static GtkTreeModel *create_we_timers_model(World *world);
static void          we_timer_configure_view(GtkTreeView *view,
                                             GtkTreeModel *model);
static void          we_timer_add_cb(GtkButton *button, gpointer data);
static void          we_timer_edit_cb(GtkButton *button, gpointer data);
static void          we_timer_delete_cb(GtkButton *button, gpointer data);
static void          we_timer_view_row_dblclick_cb(GtkTreeView        *view,
                                                   GtkTreePath        *path,
                                                   GtkTreeViewColumn  *col,
                                                   gpointer            data);
static gboolean      edit_timer(World *world, Timer *timer, gboolean editing);
static void          we_timer_move_cb(GtkButton *button, gpointer data);
static gboolean      we_timer_view_keypress_cb(GtkWidget   *widget,
                                               GdkEventKey *evt,
                                               gpointer     data);



GObject *
get_we_timers_tab(World *world)
{
  GtkBuilder       *ui_builder;
  gchar            *objects[] = { "we_panel_generic_graphical_editor", NULL };
  GError           *error = NULL;
  GObject          *panel;
  GtkLabel         *lblType;
  GObject          *btnAdd;
  GObject          *btnEdit;
  GObject          *btnDelete;
  GObject          *btnUp;
  GObject          *btnDown;
  GObject          *btnExport;
  GtkTreeSelection *selection;

  /* Create the dialog */
  ui_builder = gtk_builder_new();
  if (!gtk_builder_add_objects_from_resource(ui_builder,
                                             "/ekalin/kildclient/kildclient.ui",
                                             objects,
                                             &error)) {
    g_warning(_("Error loading UI from XML file: %s"), error->message);
    g_error_free(error);
    return NULL; /* This will cause warnings in the calling function, but
                    there is need to abort the program because of this. */
  }

  panel = gtk_builder_get_object(ui_builder,
                                 "we_panel_generic_graphical_editor");
  /* So it's not destroyed when the builder is destroyed */
  g_object_ref(panel);

  world->viewTimer = GTK_TREE_VIEW(gtk_builder_get_object(ui_builder,
                                                        "viewObjects"));
  g_object_set_data(G_OBJECT(world->viewTimer), "forworld", world);
  g_signal_connect(G_OBJECT(world->viewTimer), "row-activated",
                   G_CALLBACK(we_timer_view_row_dblclick_cb), world);
  g_signal_connect(G_OBJECT(world->viewTimer), "key-press-event",
                   G_CALLBACK(we_timer_view_keypress_cb), world);

  lblType = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblGEType"));
  gtk_label_set_markup(lblType, "<span weight=\"bold\">Timers</span>");

  btnAdd = gtk_builder_get_object(ui_builder, "btnGEAdd");
  g_signal_connect(btnAdd, "clicked",
                   G_CALLBACK(we_timer_add_cb), world->viewTimer);
  btnEdit = gtk_builder_get_object(ui_builder, "btnGEEdit");
  g_signal_connect(btnEdit, "clicked",
                   G_CALLBACK(we_timer_edit_cb), world->viewTimer);
  btnDelete = gtk_builder_get_object(ui_builder, "btnGEDelete");
  g_signal_connect(btnDelete, "clicked",
                   G_CALLBACK(we_timer_delete_cb), world->viewTimer);

  btnUp = gtk_builder_get_object(ui_builder, "btnGEUp");
  g_object_set_data(btnUp, "isup", GINT_TO_POINTER(TRUE));
  g_signal_connect(btnUp, "clicked",
                   G_CALLBACK(we_timer_move_cb), world->viewTimer);
  btnDown = gtk_builder_get_object(ui_builder, "btnGEDown");
  g_object_set_data(btnDown, "isup", GINT_TO_POINTER(FALSE));
  g_signal_connect(btnDown, "clicked",
                   G_CALLBACK(we_timer_move_cb), world->viewTimer);

  btnExport = gtk_builder_get_object(ui_builder, "btnGEExport");
  g_object_set_data(btnExport, "preselect", "Timer");

  world->timer_model = create_we_timers_model(world);
  world->timer_model_filter = GTK_TREE_MODEL_FILTER(gtk_tree_model_filter_new(world->timer_model, NULL));
  g_object_unref(G_OBJECT(world->timer_model));
  gtk_tree_model_filter_set_visible_func(GTK_TREE_MODEL_FILTER(world->timer_model_filter),
                                         we_guied_plugin_filter_func,
                                         world,
                                         NULL);

  we_timer_configure_view(world->viewTimer,
                          GTK_TREE_MODEL(world->timer_model_filter));
  selection = gtk_tree_view_get_selection(world->viewTimer);
  gtk_tree_selection_set_mode(selection, GTK_SELECTION_MULTIPLE);
  g_object_set_data(G_OBJECT(selection), "btnEdit",   btnEdit);
  g_object_set_data(G_OBJECT(selection), "btnDelete", btnDelete);
  g_object_set_data(G_OBJECT(selection), "btnUp",     btnUp);
  g_object_set_data(G_OBJECT(selection), "btnDown",   btnDown);
  g_object_set_data(G_OBJECT(selection), "btnExport", btnExport);
  g_signal_connect(G_OBJECT(selection), "changed",
                   G_CALLBACK(we_guied_selection_changed_cb), NULL);

  gtk_builder_connect_signals(ui_builder, world);
  g_object_unref(ui_builder);
  return panel;
}


static
GtkTreeModel *
create_we_timers_model(World *world)
{
  GtkListStore *store;
  GtkTreeIter   iter;
  GSList       *timerptr;
  Timer        *timer;

  store = gtk_list_store_new(WE_GUIED_N_COLS,
                             G_TYPE_POINTER);

  timerptr = world->timers;
  while (timerptr) {
    timer = (Timer *) timerptr->data;
    gtk_list_store_append(store, &iter);
    gtk_list_store_set(store, &iter,
                       WE_GUIED_POINTER, timer,
                       -1);

    timerptr = timerptr->next;
  }

  return GTK_TREE_MODEL(store);
}


static
void
we_timer_configure_view(GtkTreeView *view, GtkTreeModel *model)
{
  GtkCellRenderer   *renderer;
  GtkTreeViewColumn *column;

  gtk_tree_view_set_model(view, model);
  g_object_unref(model);

  /* Column 1 - Enabled? */
  renderer = gtk_cell_renderer_toggle_new();
  g_object_set(G_OBJECT(renderer), "activatable", TRUE, NULL);
  g_object_set_data(G_OBJECT(renderer), "column",
                    GINT_TO_POINTER(TIMER_ENABLED));
  g_signal_connect(G_OBJECT(renderer), "toggled",
                   G_CALLBACK(we_guied_bool_col_toggled_cb), view);
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Enabled"));
  gtk_tree_view_column_pack_start(column, renderer, FALSE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_bool_func,
                                          GINT_TO_POINTER(TIMER_ENABLED),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 2 - Name */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Name"));
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_text_func,
                                          GINT_TO_POINTER(TIMER_NAME),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 3 - Interval */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Interval"));
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_text_func,
                                          GINT_TO_POINTER(TIMER_INTERVAL),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 4 - Count */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Count"));
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_text_func,
                                          GINT_TO_POINTER(TIMER_COUNT),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 5 - Action */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Action"));
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_text_func,
                                          GINT_TO_POINTER(TIMER_ACTION),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 6 - Temporary? */
  renderer = gtk_cell_renderer_toggle_new();
  g_object_set(G_OBJECT(renderer), "activatable", TRUE, NULL);
  g_object_set_data(G_OBJECT(renderer), "column",
                    GINT_TO_POINTER(TIMER_TEMPORARY));
  g_signal_connect(G_OBJECT(renderer), "toggled",
                   G_CALLBACK(we_guied_bool_col_toggled_cb), view);
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Temp"));
  gtk_tree_view_column_pack_start(column, renderer, FALSE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_bool_func,
                                          GINT_TO_POINTER(TIMER_TEMPORARY),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 7 - Plugin */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Plugin"));
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_text_func,
                                          GINT_TO_POINTER(OBJECT_PLUGIN),
                                          NULL);
  gtk_tree_view_append_column(view, column);


  gtk_tree_view_columns_autosize(view);
}


static
void
we_timer_add_cb(GtkButton *button, gpointer data)
{
  World       *world;
  GtkTreeView *view;
  Timer     *timer;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");

  timer = g_new0(Timer, 1);
  timer->enabled   = 1;
  timer->count     = -1;
  timer->for_world = world;

  if (edit_timer(world, timer, FALSE)) {
    world->timers = g_slist_insert(world->timers,
                                   timer,
                                   world->timer_pos);
    we_timer_insert_timer(world, timer, world->timer_pos++);
    if (world->connected && timer->enabled) {
      timer->function_id = g_timeout_add(timer->interval * 1000,
                                         run_timer,
                                         timer);
    }
  } else {
    g_free(timer);
  }
}


static
void
we_timer_edit_cb(GtkButton *button, gpointer data)
{
  /* Note that the Edit button is only active when exactly one row
     is selected. */
  World            *world;
  GtkTreeView      *view;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  GList            *selected;
  GtkTreeIter       iter;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");
  selection = gtk_tree_view_get_selection(view);
  selected = gtk_tree_selection_get_selected_rows(selection, &model);
  if (selected) {
    Timer *timer;

    gtk_tree_model_get_iter(model, &iter, selected->data);
    gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &timer, -1);

    if (edit_timer(world, timer, TRUE)) {
      we_timer_update_timer(world, timer);
    }

    g_list_foreach(selected, (GFunc) gtk_tree_path_free, NULL);
    g_list_free(selected);
  }
}


static
void
we_timer_delete_cb(GtkButton *button, gpointer data)
{
  World            *world;
  GtkTreeView      *view;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  GList            *selected;
  GtkTreeIter       iter;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");
  selection = gtk_tree_view_get_selection(view);
  selected = gtk_tree_selection_get_selected_rows(selection, &model);
  if (selected) {
    Timer     *timer;
    GSList    *timeritem;
    GtkWidget *msgdlg;
    gint       n;

    n = gtk_tree_selection_count_selected_rows(selection);
    msgdlg = we_guied_confirm_delete_dialog_new(GTK_WINDOW(world->dlgEditWorld),
                                                ngettext("timer", "timers", n),
                                                n);

    if (!world->confirm_delete
        || gtk_dialog_run(GTK_DIALOG(msgdlg)) == GTK_RESPONSE_YES) {
      GList *listiter = g_list_last(selected);

      while (listiter) {
        gtk_tree_model_get_iter(model, &iter, listiter->data);
        gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &timer, -1);

        timeritem = g_slist_find(world->timers, timer);
        remove_timer(world, timeritem);

        listiter = listiter->prev;
      }
    }
    gtk_widget_destroy(msgdlg);

    g_list_foreach(selected, (GFunc) gtk_tree_path_free, NULL);
    g_list_free(selected);

  }
}


static
void
we_timer_view_row_dblclick_cb(GtkTreeView        *view,
                              GtkTreePath        *path,
                              GtkTreeViewColumn  *col,
                              gpointer            data)
{
  World        *world;
  GtkTreeModel *model;
  GtkTreeIter   iter;

  world = (World *) data;
  model = gtk_tree_view_get_model(view);

  if (gtk_tree_model_get_iter(model, &iter, path)) {
    Timer *timer;

    gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &timer, -1);

    if (edit_timer(world, timer, TRUE)) {
      we_timer_update_timer(world, timer);
    }
  }
}


static
gboolean
edit_timer(World *world, Timer *timer, gboolean editing)
{
  GtkBuilder      *ui_builder;
  gchar           *objects[] = { "dlgEditTimer",
                                 "adjSpnTimerInterval", "adjSpnTimerCount",
                                 NULL };
  GError          *error = NULL;
  GtkWidget       *dlgEditTimer;
  GtkEntry        *txtName;
  GtkSpinButton   *spnInterval;
  GtkSpinButton   *spnCount;
  GtkEntry        *txtAction;
  GtkToggleButton *chkTemporary;
  GtkToggleButton *chkEnabled;
  const gchar     *newname;
  const gchar     *newaction;
  gboolean         newenabled;
  gdouble          newinterval;

  ui_builder = gtk_builder_new();
  if (!gtk_builder_add_objects_from_resource(ui_builder,
                                             "/ekalin/kildclient/kildclient.ui",
                                             objects,
                                             &error)) {
    g_warning(_("Error loading UI from XML file: %s"), error->message);
    g_error_free(error);
    return FALSE;
  }

  dlgEditTimer = GTK_WIDGET(gtk_builder_get_object(ui_builder, "dlgEditTimer"));
  gtk_window_set_transient_for(GTK_WINDOW(dlgEditTimer),
                               GTK_WINDOW(world->dlgEditWorld));
  gtk_dialog_set_default_response(GTK_DIALOG(dlgEditTimer), GTK_RESPONSE_OK);

  txtName      = GTK_ENTRY(gtk_builder_get_object(ui_builder,
                                                  "txtTimerName"));
  txtAction    = GTK_ENTRY(gtk_builder_get_object(ui_builder,
                                                  "txtTimerAction"));
  spnInterval  = GTK_SPIN_BUTTON(gtk_builder_get_object(ui_builder,
                                                        "spnTimerInterval"));
  spnCount     = GTK_SPIN_BUTTON(gtk_builder_get_object(ui_builder,
                                                        "spnTimerCount"));
  chkTemporary = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder,
                                                          "chkTimerTemporary"));
  chkEnabled   = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder,
                                                          "chkTimerEnabled"));

  /* Only display note when editing a plugin's timer */
  if (!timer->owner_plugin) {
    GObject *label;

    label = gtk_builder_get_object(ui_builder, "lblNoteTimer");
    gtk_widget_destroy(GTK_WIDGET(label));
    label = gtk_builder_get_object(ui_builder, "lblNoteTextTimer");
    gtk_widget_destroy(GTK_WIDGET(label));
  }

  /* We don't need it anymore */
  g_object_unref(ui_builder);

  /* Fill-in values */
  if (timer->name) {
    gtk_entry_set_text(txtName, timer->name);
  }
  if (timer->action) {
    gtk_entry_set_text(txtAction, timer->action);
  }
  gtk_spin_button_set_value(spnInterval, timer->interval);
  gtk_spin_button_set_value(spnCount,    timer->count);
  gtk_toggle_button_set_active(chkTemporary, timer->temporary);
  gtk_toggle_button_set_active(chkEnabled,   timer->enabled);

  /* Run the dialog until the input is valid or cancelled */
  gtk_widget_show_all(dlgEditTimer);
  while (1) {
    if (gtk_dialog_run(GTK_DIALOG(dlgEditTimer)) == GTK_RESPONSE_OK) {
      /* Validate */
      newenabled = gtk_toggle_button_get_active(chkEnabled);

      newinterval = gtk_spin_button_get_value(spnInterval);
      if (newinterval <= 0) {
        kc_error_dialog(GTK_WINDOW(dlgEditTimer),
                        _("The interval must be positive."));
        continue;
      }

      newaction = gtk_entry_get_text(txtAction);
      if (strcmp(newaction, "") == 0) {
        kc_error_dialog(GTK_WINDOW(dlgEditTimer),
                        _("You must specify the action."));
        continue;
      }

      /* Update values */
      newname = gtk_entry_get_text(txtName);
      if (!timer->name || strcmp(newname, timer->name) != 0) {
        g_free(timer->name);
        if (strcmp(newname, "") != 0) {
          timer->name = g_strdup(newname);
        } else {
          timer->name = NULL;
        }
      }

      if (!timer->action || strcmp(newaction, timer->action) != 0) {
        g_free(timer->action);
        timer->action = g_strdup(newaction);
      }

      timer->count     = gtk_spin_button_get_value_as_int(spnCount);
      timer->temporary = gtk_toggle_button_get_active(chkTemporary);

      if (world->connected && editing) {
        /* Recreate timeout if interval is changed */
        if (timer->interval != newinterval
            && timer->enabled && newenabled) {
          g_source_remove(timer->function_id);
          timer->function_id = g_timeout_add(newinterval * 1000,
                                             run_timer,
                                             timer);
        }
        /* Turn on or off if necessary. Never called if the above
           condition was executed, because the above happens when
           enabled was not changed, and the below happens when it
           changed. */
        if (timer->enabled && !newenabled) {
          g_source_remove(timer->function_id);
        } else if (!timer->enabled && newenabled) {
          timer->function_id = g_timeout_add(timer->interval * 1000,
                                             run_timer,
                                             timer);
        }
      }

      timer->interval = newinterval;
      timer->enabled  = newenabled;

      /* We've finished successfully */
      gtk_widget_destroy(dlgEditTimer);
      return TRUE;
    } else {
      /* Cancel pressed */
      gtk_widget_destroy(dlgEditTimer);
      return FALSE;
    }
  }
}


static
gboolean
we_timer_view_keypress_cb(GtkWidget   *widget,
                          GdkEventKey *evt,
                          gpointer     data)
{
  if (evt->keyval == GDK_KEY_Delete || evt->keyval == GDK_KEY_KP_Delete) {
    we_timer_delete_cb(NULL, widget);
    return TRUE;
  }

  return FALSE;
}


static
void
we_timer_move_cb(GtkButton *button, gpointer data)
{
  GtkTreeView      *view;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  GList            *selected;
  GtkTreeIter       iter;
  World            *world;
  gboolean          is_up;
  gint              new_pos;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");
  selection = gtk_tree_view_get_selection(view);
  is_up = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(button), "isup"));
  selected = gtk_tree_selection_get_selected_rows(selection, &model);
  if (selected) {
    Timer *timer;
    gint     pos;

    gtk_tree_model_get_iter(model, &iter, selected->data);
    gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &timer, -1);
    pos = g_slist_index(world->timers, timer);
    new_pos = pos;
    if (is_up) {
      if (pos != 0) {
        new_pos = pos - 1;
        move_timer(world, pos, new_pos);
      }
    } else {
      if (pos != world->timer_pos - 1) {
        new_pos = pos + 1;
        move_timer(world, pos, new_pos);
      }
    }
    /* Reselect the moved item */
    gtk_tree_model_iter_nth_child(model, &iter, NULL, new_pos);
    gtk_tree_selection_select_iter(selection, &iter);

    g_list_foreach(selected, (GFunc) gtk_tree_path_free, NULL);
    g_list_free(selected);
  }
}


void
we_timer_update_timer(World *world, Timer *timer_arg)
{
  /* Called by the Perl functions when an timer is changed, so that
     the display is updated. */
  GtkTreeIter  iter;
  GtkTreePath *path;
  Timer     *timer;
  gboolean     success;

  if (!world->timer_model) {
    return;
  }

  success = gtk_tree_model_get_iter_first(world->timer_model, &iter);
  while (success) {
    gtk_tree_model_get(world->timer_model, &iter,
                       WE_GUIED_POINTER, &timer, -1);
    if (timer == timer_arg) {
      path = gtk_tree_model_get_path(world->timer_model, &iter);
      gtk_tree_model_row_changed(world->timer_model, path, &iter);
      gtk_tree_path_free(path);
      return;
    }
    success = gtk_tree_model_iter_next(world->timer_model, &iter);
  }
}


void
we_timer_insert_timer(World *world, Timer *timer, gint pos)
{
  /* Called by the Perl functions when an timer is inserted, so that
     the display is updated. */
  GtkTreeIter iter;

  if (!world->timer_model) {
    return;
  }

  gtk_list_store_insert(GTK_LIST_STORE(world->timer_model), &iter, pos);
  gtk_list_store_set(GTK_LIST_STORE(world->timer_model), &iter,
                     WE_GUIED_POINTER, timer,
                     -1);
}


void
we_timer_delete_timer(World *world, Timer *timer_arg)
{
  /* Called by the Perl functions when an timer is deleted, so that
     the display is updated. */
  GtkTreeIter  iter;
  Timer       *timer;
  gboolean     success;

  if (!world->timer_model) {
    return;
  }

  success = gtk_tree_model_get_iter_first(world->timer_model, &iter);
  while (success) {
    gtk_tree_model_get(world->timer_model, &iter,
                       WE_GUIED_POINTER, &timer, -1);
    if (timer == timer_arg) {
      gtk_list_store_remove(GTK_LIST_STORE(world->timer_model), &iter);
      return;
    }
    success = gtk_tree_model_iter_next(world->timer_model, &iter);
  }
}
