/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifndef __KCIRCULARARRAY_H
#define __KCIRCULARARRAY_H

#include <stddef.h>


/**
  A queue in which you can push elements in one end and pop them off the
  other end. Elements can be of any size, but must be all of the same
  type (and size). It grows as necessary when new elements are added.
  Internally, it is represented as a circular array for efficiency.
*/
typedef struct _KCircularQueue
{
  char         *data;
  size_t        el_size;
  unsigned int  allocated_size;
  unsigned int  size;
  unsigned int  first;
  unsigned int  first_free;
  unsigned int  free_until_end;
} KCircularQueue;



/**
  Creates a new KCircularQueue.

  @param el_size  The size of the elements of the queue.
  @param how_many Number of elements to allocate.

  @return The newly created KCircularQueue.
*/
KCircularQueue *k_circular_queue_new        (size_t          el_size,
                                             unsigned int    how_many);
/**
  Frees memory used by a KCircularQueue.

  @param q The KCircularQueue. The pointer will be invalid after the
           call to this function.
*/
void            k_circular_queue_free       (KCircularQueue *q);
/**
  Pushes several copies of an element in the end of a KCircularQueue.

  @param q   The KCircularQueue to push elements into.
  @param val A pointer to the element to push.
  @param n   The number of copies of the element to push.
*/
void            k_circular_queue_push_copies(KCircularQueue *q,
                                             const void     *val,
                                             unsigned int    n);
/**
  Removes elements from the front of the KCircularQueue..

  @param q   The KCircularQueue to remove elements from.
  @param n   The number of elements to remove.
*/
void            k_circular_queue_pop        (KCircularQueue *q,
                                             unsigned int    n);

/**
  Returns the number of elements in a KCircularQueue.

  @param q   The KCircularQueue.

  @return The number of elements..
*/
#define k_circular_queue_size(q)      (q->size)
/**
  Pushes an element in the end of a KCircularQueue.

  @param q   The KCircularQueue to push elements into.
  @param val The element to push.
*/
#define k_circular_queue_push(q, v)   k_circular_queue_push_copies(q, &(v), 1)
/**
  Retrieves a given element from a KCircularQueue.

  @param q The KCircularQueue.
  @param t The type of the element (int, double, etc.)
  @param i The index of the element to retrieve.

  @return The element, cast to the given type.
*/
#define k_circular_queue_nth(q, t, i) *((t *) _k_circular_queue_nth(q, i))



/*************************************************************
 * Private functions                                         *
 *************************************************************/
void        _k_circular_queue_realloc(KCircularQueue *q,
                                      unsigned int    new_size);
const void *_k_circular_queue_nth    (KCircularQueue *q,
                                      unsigned int    i);


#endif /* __KCIRCULARARRAY_H */
