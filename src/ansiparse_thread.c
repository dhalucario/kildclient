/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif
#include <stddef.h>
#include <regex.h>

#include "kildclient.h"
#include "ansi.h"


/*************************
 * File global variables *
 *************************/
/* Values used to get the 6x6x6 color cube of xterm 256 color mode */
static guint8 xterm256color[] = { 0x00, 0x5f, 0x87, 0xaf, 0xd7, 0xff };
/* Values used for greyscales in xterm 256 color mode */
static guint8 xterm256gray[] = { 0x08, 0x12, 0x1c, 0x26,
                                 0x30, 0x3a, 0x44, 0x4e,
                                 0x58, 0x62, 0x6c, 0x76,
                                 0x80, 0x8a, 0x94, 0x9e,
                                 0xa8, 0xb2, 0xbc, 0xc6,
                                 0xd0, 0xda, 0xe4, 0xee };
/* UTF-8 codes for the characters used in line-drawing mode */
static gchar *linedrawingchars[] = { "\342\227\206", /* ` */
                                     "\342\226\222", /* a */
                                     "\342\220\211", /* b */
                                     "\342\220\214", /* c */
                                     "\342\220\215", /* d */
                                     "\342\220\212", /* e */
                                     "\302\260",     /* f */
                                     "\302\261",     /* g */
                                     "\342\220\244", /* h */
                                     "\342\220\213", /* i */
                                     "\342\224\230", /* j */
                                     "\342\224\220", /* k */
                                     "\342\224\214", /* l */
                                     "\342\224\224", /* m */
                                     "\342\224\274", /* n */
                                     "\342\216\272", /* o */
                                     "\342\216\273", /* p */
                                     "\342\224\200", /* q */
                                     "\342\216\274", /* r */
                                     "\342\216\275", /* s */
                                     "\342\224\234", /* t */
                                     "\342\224\244", /* u */
                                     "\342\224\264", /* v */
                                     "\342\224\254", /* w */
                                     "\342\224\202", /* x */
                                     "\342\211\244", /* y */
                                     "\342\211\245", /* z */
                                     "\317\200",     /* { */
                                     "\342\211\240", /* | */
                                     "\302\243",     /* } */
                                     "\302\267",     /* ~ */ };
/* ASCII characters replacing those of line-drawing mode when they are
   not available. */
static gchar *replacingchars[] = { "?", /* ` */
                                   "#", /* a */
                                   "?", /* b */
                                   "?", /* c */
                                   "?", /* d */
                                   "?", /* e */
                                   "o", /* f */
                                   "?", /* g */
                                   "?", /* h */
                                   "?", /* i */
                                   "+", /* j */
                                   "+", /* k */
                                   "+", /* l */
                                   "+", /* m */
                                   "+", /* n */
                                   "-", /* o */
                                   "-", /* p */
                                   "-", /* q */
                                   "_", /* r */
                                   "_", /* s */
                                   "|", /* t */
                                   "|", /* u */
                                   "-", /* v */
                                   "-", /* w */
                                   "|", /* x */
                                   "<", /* y */
                                   ">", /* z */
                                   "p", /* { */
                                   "?", /* | */
                                   "L", /* } */
                                   ".", /* ~ */ };


/***********************
 * Function prototypes *
 ***********************/
static void   process_ansiparse_operation(AnsiParseOperation *operation,
                                          World *world);
static void   queue_operation_from_gui(WorldGUI            *gui,
                                       MainThreadOperation *operation);
static void   clear_pending_line(World *world);
static void   parse_and_append_ansi_string(WorldGUI *gui,
                                           const gchar *string, gsize len);
static void   apply_highlights_and_move_mark(World *world, GSList *highlights);
static void   parse_and_append_echoed_string(WorldGUI    *gui,
                                             const gchar *str,
                                             gsize        len);
static void   apply_highlights(World *world, GSList *highlights);
static void   finalize_line(World *world);
static void   insert_text(WorldGUI                    *gui,
                          const guchar                *text,
                          gsize                        len,
                          MainThreadOperationType      where,
                          MainThreadOperationFunction  operation_func);
static void   parse_ansi_sequence(WorldGUI *gui);
static void   insert_with_possible_urls(WorldGUI                    *gui,
                                        const guchar                *text,
                                        gsize                        len,
                                        MainThreadOperationType      where,
                                        MainThreadOperationFunction  operation_func);
static void   reset_ansi_state(TAState *state);
static gchar *convert_vt100linedraw_string(WorldGUI     *gui,
                                           const guchar *text,
                                           gsize         len);
static GtkTextTag *get_tag_for_xterm256_color(WorldGUI *gui,
                                              gint      x256idx,
                                              gint      code_used);
static GtkTextTag *get_tag_for_color(WorldGUI *gui,
                                     guint8 r, guint8 g, guint8 b,
                                     gboolean  is_back);



gpointer
ansiparse_thread(gpointer data)
{
  World *world = (World *) data;

  g_async_queue_ref(world->ansiparse_operations);
  g_async_queue_ref(world->mainthread_operations);

  while (1) {
    AnsiParseOperation *operation
      = (AnsiParseOperation *) g_async_queue_pop(world->ansiparse_operations);

    if (world->to_be_destructed) {
      g_async_queue_unref(world->ansiparse_operations);
      g_async_queue_unref(world->mainthread_operations);

      free_ansiparse_operation(operation);
      return NULL;
    }

    process_ansiparse_operation(operation, world);
    free_ansiparse_operation(operation);
  }
}


void
free_ansiparse_operation(gpointer data)
{
  AnsiParseOperation *operation = (AnsiParseOperation *) data;

  switch (operation->action) {
  case APPEND_LINE:
  case APPEND_ECHOED_LINE:
    g_free(operation->line.line);
    break;

  case APPLY_HIGHLIGHTS_AND_MOVE_MARK:
    g_slist_free(operation->highlights);
    break;

  default: /* Nothing special to free */
    break;
  }

  g_free(operation);
}


static
void
process_ansiparse_operation(AnsiParseOperation *operation, World *world)
{
  switch (operation->action) {
  case CLEAR_PENDING_LINE_ANSI:
    clear_pending_line(world);
    break;

  case APPEND_LINE:
    parse_and_append_ansi_string(world->gui,
                                 operation->line.line,
                                 operation->line.line_len);
    break;

  case APPEND_ECHOED_LINE:
    parse_and_append_echoed_string(world->gui,
                                   operation->line.line,
                                   operation->line.line_len);
    break;

  case APPLY_HIGHLIGHTS_AND_MOVE_MARK:
    apply_highlights_and_move_mark(world, operation->highlights);
    break;

  case FINALIZE_LINE:
    finalize_line(world);
    break;

  case DUMMY_ANSIPARSE:
    break;
  }
}


static
void
queue_operation_from_gui(WorldGUI            *gui,
                         MainThreadOperation *operation)
{
  queue_mainthread_operation(gui->world, operation);
}


static
void
clear_pending_line(World *world)
{
  MainThreadOperation *operation = g_new(MainThreadOperation, 1);
  operation->action = CLEAR_PENDING_LINE;
  queue_mainthread_operation(world, operation);

  /* Since an ANSI sequence cannot be incomplete at the end of
     one line and continue at another (because \n is not valid
     inside ANSI strings), ANSI parsing at a line always starts
     in DATA mode. */
  world->gui->ansi_parse.ansifsm_state  = ANSIFSM_DATA;
  world->gui->ansi_parse.ansiseq_pos = 0;
  /* Furthermore, colors are restored to what they were in the
     end of the last complete line. */
  world->gui->ta_state = world->gui->saved_ta_state;
}


static
void
parse_and_append_ansi_string(WorldGUI *gui,
                             const gchar *string, gsize len)
{
  insert_ansi_string(gui, string, len, INSERT_AT_END, queue_operation_from_gui);
}


static
void
parse_and_append_echoed_string(WorldGUI    *gui,
                               const gchar *str,
                               gsize        len)
{
  /*
   * Append str (a null-terminated string) possibly with ANSI color
   * codes to the Buffer, which does not come from the MUD, but was
   * echoed. The state of the ANSI parser is saved in case an
   * incomplete ANSI sequence was pending. The line is printed above
   * any incomplete line (prompt).
   */
  AnsiParseState saved_ansi_parse;
  TAState        saved_state;
  guint          printed;
  guint          to_print;

  saved_ansi_parse = gui->ansi_parse;
  saved_state      = gui->ta_state;

  gui->ansi_parse.ansifsm_state  = ANSIFSM_DATA;
  gui->ansi_parse.ansiseq_pos    = 0;

  /* Due to static buffers, only MAX_BUFFER chars can be printed at a time. */
  printed = 0;
  while (printed < len) {
    to_print = (len - printed) > MAX_BUFFER ? MAX_BUFFER : (len - printed);
    insert_ansi_string(gui, str + printed, to_print,
                       INSERT_AT_MARK, queue_operation_from_gui);
    printed += to_print;
  }

  gui->ansi_parse = saved_ansi_parse;
  gui->ta_state   = saved_state;
}


static
void
apply_highlights_and_move_mark(World *world, GSList *highlights)
{
  apply_highlights(world, highlights);

  /* Move mark */
  MainThreadOperation *operation = g_new(MainThreadOperation, 1);
  operation->action = MOVE_LINESTART_MARK;
  queue_mainthread_operation(world, operation);

  /* Save color at end of line */
  world->gui->saved_ta_state = world->gui->ta_state;
}


static
void
apply_highlights(World *world, GSList *highlights)
{
  GSList *cur_highlight;

  for (cur_highlight = highlights;
       cur_highlight;
       cur_highlight = cur_highlight->next) {
    MainThreadOperation *operation = g_new(MainThreadOperation, 1);
    operation->action = APPLY_HIGHLIGHT;
    operation->highlight = cur_highlight->data;
    queue_mainthread_operation(world, operation);
  }
}


static
void
finalize_line(World *world)
{
  g_timer_start(world->last_data_written);
}


void
insert_ansi_string(WorldGUI                    *gui,
                   const gchar                 *str,
                   gsize                        len,
                   MainThreadOperationType      where,
                   MainThreadOperationFunction  operation_func)
{
  /*
   * Interprets str, possibly with ANSI color codes, and queues
   * operations that will insert it in the buffer, using where as the
   * operation (INSERT_AT_END or INSERT_AT_MARK).
   */
  guchar               text_buffer[MAX_BUFFER];
  gint                 text_buffer_pos;
  guint                original_pos;
  MainThreadOperation *operation;

  text_buffer_pos = 0;
  original_pos    = 0;
  while (str[original_pos] && original_pos < len) {
    switch (gui->ansi_parse.ansifsm_state) {
    case ANSIFSM_DATA:
      if (str[original_pos] == ANSI_ESC) {
        gui->ansi_parse.ansifsm_state = ANSIFSM_ESC;
      } else {
        text_buffer[text_buffer_pos++] = str[original_pos];
      }
      break;

    case ANSIFSM_ESC:
      if (str[original_pos] == '[') {
        gui->ansi_parse.ansifsm_state = ANSIFSM_ANSISEQ;
        if (text_buffer_pos) {
          insert_text(gui, text_buffer, text_buffer_pos, where, operation_func);
          text_buffer_pos = 0;
        }
      } else if (str[original_pos] == '(') {
        gui->ansi_parse.ansifsm_state = ANSIFSM_ESCLEFTPAR;
      } else {
        /* Put the ESC back */
        text_buffer[text_buffer_pos++] = ANSI_ESC;
        text_buffer[text_buffer_pos++] = str[original_pos];
        gui->ansi_parse.ansifsm_state = ANSIFSM_DATA;
      }
      break;

    case ANSIFSM_ANSISEQ:
      /* Since there is no reason for a valid ANSI sequence that is longer
         than MAX_BUFFER, we simply cut it if it is too long. */
      if ((isdigit(str[original_pos]) || str[original_pos] == ';')
          && gui->ansi_parse.ansiseq_pos < MAX_BUFFER) {
        gui->ansi_parse.ansiseq_buffer[gui->ansi_parse.ansiseq_pos++] = str[original_pos];
      } else {
        gui->ansi_parse.ansifsm_state = ANSIFSM_DATA;
        if (str[original_pos] == 'm') {
          parse_ansi_sequence(gui);
        } else {
          /* Not recognized, ignored and cleared. */
          /* FIXME: Copy it back, being careful with buffer size. */
          gui->ansi_parse.ansiseq_pos = 0;
        }
      }
      break;

    case ANSIFSM_ESCLEFTPAR:
      if (str[original_pos] == '0' || str[original_pos] == 'B') {
        gui->ansi_parse.ansifsm_state = ANSIFSM_DATA;
        if (text_buffer_pos) {
          insert_text(gui, text_buffer, text_buffer_pos, where, operation_func);
          text_buffer_pos = 0;
        }
        gui->ansi_parse.linedraw = (str[original_pos] == '0');
      } else {
        /* Put unrecognized sequence back */
        text_buffer[text_buffer_pos++] = ANSI_ESC;
        text_buffer[text_buffer_pos++] = '(';
        text_buffer[text_buffer_pos++] = str[original_pos];
        gui->ansi_parse.ansifsm_state = ANSIFSM_DATA;
      }
      break;
    }
    ++original_pos;
  }

  /* Add any remaining text */
  if (text_buffer_pos) {
    insert_text(gui, text_buffer, text_buffer_pos, where, operation_func);
    text_buffer_pos = 0;
  }

  /* Notify that text has been added. */
  operation = g_new(MainThreadOperation, 1);
  operation->action = TEXT_ADDED;
  operation_func(gui, operation);
}


static
void
insert_text(WorldGUI                    *gui,
            const guchar                *text,
            gsize                        len,
            MainThreadOperationType      where,
            MainThreadOperationFunction  operation_func)
{
  if (gui->ansi_parse.linedraw) {
    MainThreadOperation *operation = g_new(MainThreadOperation, 1);
    operation->action = where;
    operation->text_insert.text
      = convert_vt100linedraw_string(gui, text, len);
    operation->text_insert.use_ta = TRUE;
    operation->text_insert.ta_state = gui->ta_state;
    operation->text_insert.extra_tag = NULL;

    operation_func(gui, operation);
  } else {
    insert_with_possible_urls(gui, text, len, where, operation_func);
  }
}


static
void
insert_with_possible_urls(WorldGUI                    *gui,
                          const guchar                *text,
                          gsize                        len,
                          MainThreadOperationType      where,
                          MainThreadOperationFunction  operation_func)
{
  static regex_t regex;
  static gsize   re_compiled = 0;
  guchar         ctext[MAX_BUFFER + 1];
  regmatch_t     match;
  guint          search_start;

  if (g_once_init_enter(&re_compiled)) {
    regcomp(&regex,
            "(((https?|ftp)://)|www\\.)[-A-Za-z0-9\\./?=&+%:#_~@,]*[-A-Za-z0-9\\/?=&+%:#_~]",
            REG_EXTENDED | REG_ICASE);
    g_once_init_leave(&re_compiled, 1);
  }

  /* We need a NULL-terminated string */
  memcpy(ctext, text, len);
  ctext[len] = '\0';

  search_start = 0;
  while (search_start < len) {
    if (regexec(&regex, (char *) (ctext + search_start), 1, &match, 0) == 0) {
      MainThreadOperation *operation = g_new(MainThreadOperation, 1);

      /* Text before the URL */
      operation->action = where;
      operation->text_insert.text
        = g_strndup((gchar *) (text + search_start), match.rm_so);
      operation->text_insert.use_ta = TRUE;
      operation->text_insert.ta_state = gui->ta_state;
      operation->text_insert.extra_tag = NULL;
      operation_func(gui, operation);

      /* The URL */
      operation = g_new(MainThreadOperation, 1);
      operation->action = where;
      operation->text_insert.text
        = g_strndup((gchar *) (text + search_start + match.rm_so),
                    match.rm_eo - match.rm_so);
      operation->text_insert.use_ta = TRUE;
      operation->text_insert.ta_state = gui->ta_state;
      operation->text_insert.extra_tag = gui->txttag_url;
      operation_func(gui, operation);

      /* Continue looking */
      search_start += match.rm_eo;
    } else {
      MainThreadOperation *operation = g_new(MainThreadOperation, 1);

      /* Text after the URL */
      operation->action = where;
      operation->text_insert.text
        = g_strndup((gchar *) (text + search_start), len - search_start);
      operation->text_insert.use_ta = TRUE;
      operation->text_insert.ta_state = gui->ta_state;
      operation->text_insert.extra_tag = NULL;

      operation_func(gui, operation);

      break;
    }
  }
}


static
void
parse_ansi_sequence(WorldGUI *gui)
{
  gchar **sequences;
  gchar **curr_seq;
  int    val;
  /* xterm256 state: used to parse xterm's 256 color cube.
     xterm sequences: ESC[38;5;Xm and ESC[48;5;Xm, fore and back resp.
     When this variable is 0, no xterm sequence parsing is being done.
     When 38 or 48 is seen, the variable is set to -38 or -48, respectively.
     When it is negative, and -5 is seen, its sign is changed.
     When it is positive, the color value is read and parsed.
  */
  gint   xterm256_state = 0;

  /* ESC[m, this means reset to default. */
  if (gui->ansi_parse.ansiseq_pos == 0) {
    reset_ansi_state(&gui->ta_state);
    return;
  }

  /* Add NULL terminator. */
  gui->ansi_parse.ansiseq_buffer[gui->ansi_parse.ansiseq_pos] = 0;

  sequences = g_strsplit_set((gchar *) gui->ansi_parse.ansiseq_buffer, ";", -1);
  curr_seq  = sequences;
  while (*curr_seq) {
    val = atoi(*curr_seq);

    if (xterm256_state < 0) {
      if (val == XTERM256_SECOND_CODE) {
        xterm256_state = -xterm256_state;
      } else {
        /* Ignore the strange sequence. */
        xterm256_state = 0;
      }
      ++curr_seq;
      continue;
    } else if (xterm256_state > 0) {
      if (xterm256_state == XTERM256_SET_FORE) {
        gui->ta_state.foreground_color_tag
          = get_tag_for_xterm256_color(gui, val, xterm256_state);
      } else if (xterm256_state == XTERM256_SET_BACK) {
        gui->ta_state.background_color_tag
          = get_tag_for_xterm256_color(gui, val, xterm256_state);
      }
      xterm256_state = 0;
      ++curr_seq;
      continue;
    }

    if (val == 0) {
      reset_ansi_state(&gui->ta_state);
    } else if (val >= ANSI_FIRST_FG_COLOR && val <= ANSI_LAST_FG_COLOR) {
      gui->ta_state.fg_color = val - ANSI_FIRST_FG_COLOR;
      gui->ta_state.foreground_color_tag = NULL;
    } else if (val == ANSI_FG_DEFAULT) {
      gui->ta_state.fg_color = ANSI_DEFAULT_COLOR_IDX;
      gui->ta_state.foreground_color_tag = NULL;
    } else if (val == ANSI_SET_FG_BOLD) {
      gui->ta_state.fg_base_idx = ANSI_BOLD_BASE_IDX;
    } else if (val == ANSI_BOLD_OFF) {
      gui->ta_state.fg_base_idx = ANSI_NORMAL_BASE_IDX;
    } else if (val >= ANSI_FIRST_BG_COLOR && val <= ANSI_LAST_BG_COLOR) {
      gui->ta_state.bg_color = val - ANSI_FIRST_BG_COLOR;
      gui->ta_state.background_color_tag = NULL;
    } else if (val == ANSI_BG_DEFAULT) {
      gui->ta_state.bg_color = ANSI_DEFAULT_COLOR_IDX;
      gui->ta_state.background_color_tag = NULL;
    } else if (val == ANSI_SET_BG_LIGHT) {
      gui->ta_state.bg_base_idx = ANSI_BOLD_BASE_IDX;
    } else if (val == ANSI_BOLD_BG_OFF) {
      gui->ta_state.bg_base_idx = ANSI_NORMAL_BASE_IDX;
    } else if (val == ANSI_SET_UNDERLINE) {
      gui->ta_state.underline = PANGO_UNDERLINE_SINGLE;
    } else if (val == ANSI_SET_DBLUNDERLINE) {
      gui->ta_state.underline = PANGO_UNDERLINE_DOUBLE;
    } else if (val == ANSI_UNDERLINE_OFF) {
      gui->ta_state.underline = PANGO_UNDERLINE_NONE;
    } else if (val == ANSI_SET_STRIKE) {
      gui->ta_state.strike = TRUE;
    } else if (val == ANSI_STRIKE_OFF) {
      gui->ta_state.strike = FALSE;
    } else if (val == ANSI_SET_ITALICS) {
      gui->ta_state.italics = TRUE;
    } else if (val == ANSI_ITALICS_OFF) {
      gui->ta_state.italics = FALSE;
    } else if (val == ANSI_SET_REVERSE) {
      gui->ta_state.reverse = TRUE;
    } else if (val == ANSI_REVERSE_OFF) {
      gui->ta_state.reverse = FALSE;
    } else if (val == ANSI_SET_HIDDEN) {
      gui->ta_state.hidden = TRUE;
    } else if (val == ANSI_HIDDEN_OFF) {
      gui->ta_state.hidden = FALSE;
    } else if (val == XTERM256_SET_FORE || val == XTERM256_SET_BACK) {
      xterm256_state = -val;
    } /* else ignored */
    ++curr_seq;
  }
  g_strfreev(sequences);

  gui->ansi_parse.ansiseq_pos = 0;
}


static
void
reset_ansi_state(TAState *state)
{
  /* Resets state to defaults, when ESC[0m or ESC[m is seen. */
  state->fg_color    = state->bg_color    = ANSI_DEFAULT_COLOR_IDX;
  state->fg_base_idx = state->bg_base_idx = ANSI_NORMAL_BASE_IDX;
  state->foreground_color_tag = state->background_color_tag = NULL;
  state->underline = PANGO_UNDERLINE_NONE;
  state->strike    = FALSE;
  state->italics   = FALSE;
  state->reverse   = FALSE;
  state->hidden    = FALSE;
}


static
gchar *
convert_vt100linedraw_string(WorldGUI     *gui,
                             const guchar *text,
                             gsize         len)
{
  GString *result;
  guint    i;

  // The longest sequences are three bytes;
  result = g_string_sized_new(len * 3 + 1);

  worldgui_determine_supported_chars(gui);

  for (i = 0; i < len; ++i) {
    if (text[i] < '_' || text[i] > '~') {
      g_string_append_c(result, text[i]);
    } else {
      switch (text[i]) {
      case '_': /* A blank */
        g_string_append_c(result, ' ');
        break;

      /* Geometric Shapes */
      case '`':
        g_string_append(result, gui->ta.sup_geom_shapes
                                  ? linedrawingchars[text[i] - '_' - 1]
                                  : replacingchars[text[i] - '_' - 1]);
        break;

      /* Block elements */
      case 'a':
        g_string_append(result, gui->ta.sup_block
                                  ? linedrawingchars[text[i] - '_' - 1]
                                  : replacingchars[text[i] - '_' - 1]);
        break;

      /* Control Pictures */
      case 'b': case 'c': case 'd': case 'e': case 'h': case 'i':
        g_string_append(result, gui->ta.sup_control
                                  ? linedrawingchars[text[i] - '_' - 1]
                                  : replacingchars[text[i] - '_' - 1]);
        break;

      /* Latin-1 Supplement */
      case 'f': case 'g': case '}': case '~':
        g_string_append(result, gui->ta.sup_l1_supplement
                                  ? linedrawingchars[text[i] - '_' - 1]
                                  : replacingchars[text[i] - '_' - 1]);
        break;

      /* Box drawing */
      case 'j': case 'k': case 'l': case 'm': case 'n': case 'q':
      case 't': case 'u': case 'v': case 'w': case 'x':
        g_string_append(result, gui->ta.sup_box_drawing
                                  ? linedrawingchars[text[i] - '_' - 1]
                                  : replacingchars[text[i] - '_' - 1]);
        break;

      /* Miscellaneous Technical */
      case 'o': case 'p': case 'r': case 's':
        g_string_append(result, gui->ta.sup_misc_tech
                                  ? linedrawingchars[text[i] - '_' - 1]
                                  : replacingchars[text[i] - '_' - 1]);
        break;

      /* Mathematical Operators */
      case 'y': case 'z': case '|':
        g_string_append(result, gui->ta.sup_math
                                  ? linedrawingchars[text[i] - '_' - 1]
                                  : replacingchars[text[i] - '_' - 1]);
        break;

      /* Greek and Coptic */
      case '{':
        g_string_append(result, gui->ta.sup_greek
                                  ? linedrawingchars[text[i] - '_' - 1]
                                  : replacingchars[text[i] - '_' - 1]);
        break;
      }
    }
  }

  return g_string_free(result, FALSE);
}


static
GtkTextTag *
get_tag_for_xterm256_color(WorldGUI *gui,
                           gint      x256idx,
                           gint      code_used)
{
  guint8 r, g, b;

  if (x256idx <= 7) {           /* Normal ANSI colors */
    if (code_used == XTERM256_SET_FORE) {
      return gui->ta.ansi_fore_tags[x256idx];
    } else if (code_used == XTERM256_SET_BACK) {
      return gui->ta.ansi_back_tags[x256idx];
    }
  }

  if (x256idx <= 15) {   /* Highlighted ANSI colors */
    if (code_used == XTERM256_SET_FORE) {
      return gui->ta.ansi_fore_tags[x256idx - ANSI_N_COLORS + ANSI_BOLD_BASE_IDX];
    } else if (code_used == XTERM256_SET_BACK) {
      return gui->ta.ansi_back_tags[x256idx - ANSI_N_COLORS + ANSI_BOLD_BASE_IDX];
    }
  }

  if (x256idx <= 231) {  /* 6x6x6 rgb color cube */
    guint8 ri, gi, bi;

    x256idx -= 16;
    ri = x256idx / 36;
    x256idx %= 36;
    gi = x256idx / 6;
    x256idx %= 6;
    bi = x256idx;

    r = xterm256color[ri];
    g = xterm256color[gi];
    b = xterm256color[bi];
  } else {
    x256idx -= 232;

    r = xterm256gray[x256idx];
    g = b = r;
  }

  return get_tag_for_color(gui, r, g, b, code_used == XTERM256_SET_BACK);
}


static
GtkTextTag *
get_tag_for_color(WorldGUI *gui,
                  guint8 r, guint8 g, guint8 b,
                  gboolean  is_back)
{
  gint        rgb;
  GtkTextTag *tag;
  GHashTable *hash;
  GdkRGBA     color;

  hash = is_back ? gui->ta.rgb_back_tags : gui->ta.rgb_fore_tags;
  rgb = (r << 16) | (g << 8) | b;

  tag = g_hash_table_lookup(hash, GINT_TO_POINTER(rgb));
  if (tag) {
    return tag;
  }

  /* Create if it does not exist */
  color.alpha = 1.0;
  color.red   = r / 255.0;
  color.green = g / 255.0;
  color.blue  = b / 255.0;
  if (!is_back) {
    tag = gtk_text_buffer_create_tag(gui->txtBuffer,
                                     NULL,
                                     "foreground-rgba", &color,
                                     NULL);
  } else {
    tag = gtk_text_buffer_create_tag(gui->txtBuffer,
                                     NULL,
                                     "background-rgba", &color,
                                     NULL);
  }

  gtk_text_tag_set_priority(tag, 0);
  g_hash_table_insert(hash, GINT_TO_POINTER(rgb), tag);
  return tag;
}
