/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <signal.h>
#include "libintl-wrapper.h"
#include <locale.h>
#include <gtk/gtk.h>
#include <gnutls/gnutls.h>

#ifdef __MINGW32__
#  include <winsock2.h>
#  include <windows.h>
#  include <shlobj.h>
#endif

#include "kildclient.h"
#include "perlscript.h"



/********************
 * Global variables *
 ********************/
FILE *dumpFile  = NULL;
FILE *rdumpFile = NULL;


/*************************
 * File global variables *
 *************************/
gnutls_certificate_credentials_t tls_certif_cred;


/************************
 * Command-line options *
 ************************/
static gboolean wants_version = FALSE;
static gchar   *dumpfile = NULL;
static gchar   *rdumpfile = NULL;
static gchar   *kc_user_dir = NULL;
static GOptionEntry options[] = {
  { "version", 0, 0, G_OPTION_ARG_NONE, &wants_version,
    N_("Outputs version information and exits"), NULL },
  { "config", 'c', 0, G_OPTION_ARG_FILENAME, &kc_user_dir,
    N_("Directory to store settings and saved worlds"), N_("DIR") },
  { "dump", 0, 0, G_OPTION_ARG_FILENAME, &dumpfile,
    N_("Dumps all data received from (after decompression, if "
       "appropriate) to the file"), N_("FILE") },
  { "rawdump", 0, 0, G_OPTION_ARG_FILENAME, &rdumpfile,
    N_("Dumps all data received (possibly compressed) to the file"),
    N_("FILE") },
  { NULL },
};


int
main(int argc, char *argv[], char *env[])
{
  int x = -1, y, width, height;
  WorldGUI       *gui;
  GOptionContext *opt_context;
  GError         *error = NULL;
  int             argpos = 1;
  gboolean        opened_one = FALSE;
#ifdef __MINGW32__
  WORD    wWSAVersion;
  WSADATA wsaData;
#endif


  /* Internationalization initialization */
#ifdef __MINGW32__
  bindtextdomain(GETTEXT_PACKAGE, get_kildclient_installed_file("locale"));
#else
  bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
#endif
  bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
  textdomain(GETTEXT_PACKAGE);

  /* Parse command-line options */
  opt_context = g_option_context_new(N_("[WORLD...]"));
  g_option_context_add_main_entries(opt_context, options, GETTEXT_PACKAGE);
  g_option_context_add_group(opt_context, gtk_get_option_group(TRUE));
  if (!g_option_context_parse(opt_context, &argc, &argv, &error)) {
    printf("%s\n", error->message);
    g_error_free(error);
    exit(1);
  }
  g_option_context_free(opt_context);

  if (wants_version) {
    printf(PACKAGE_NAME " " PACKAGE_VERSION "\n");
    printf(_("Written by Eduardo M Kalinowski (eduardo@kalinowski.com.br)\n"));
    exit(0);
  }
  if (dumpfile) {
    if (!(dumpFile = fopen(dumpfile, "w"))) {
      fprintf(stderr, _("Could not open dump file %s: %s\n"),
              dumpfile, g_strerror(errno));
    }
    g_free(dumpfile);
  }
  if (rdumpfile) {
    if (!(rdumpFile = fopen(rdumpfile, "w"))) {
      fprintf(stderr, _("Could not open raw dump file %s: %s\n"),
              rdumpfile, g_strerror(errno));
    }
    g_free(rdumpfile);
  }

#ifdef __MINGW32__
  /* Initialise WinSock library (mingw32) */
  wWSAVersion = MAKEWORD(2, 2);
  if (WSAStartup(wWSAVersion, &wsaData) != 0) {
    /* FIXME: Inform why */
    printf("Could not initialize windows sockets.\n");
    exit(1);
  }
#endif

  gtk_init(&argc, &argv);

  read_global_preferences(&x, &y, &width, &height);
  create_default_world();

  gui = world_gui_new(TRUE);
  wndMain = create_main_window(gui);
  set_main_window_size(x, y, width, height, gui);
  add_global_css();

  init_perl_system();

  gnutls_global_init();
  gnutls_certificate_allocate_credentials(&tls_certif_cred);
  /* We don't really verify certificates */
  /*gnutls_certificate_set_x509_trust_file(tls_certif_cred, CAFILE, GNUTLS_X509_FMT_PEM);*/

  gtk_widget_show_all(wndMain);

#ifndef __MINGW32__
  /* We don't want SIGPIPEs killing us */
  signal(SIGPIPE, SIG_IGN);
#endif

  /* Open worlds given in command line */
  while (argpos < argc) {
    if (!opened_one) {
      /* First one uses the first GUI */
      opened_one |= open_new_world(gui, argv[argpos], FALSE, NULL);
    } else {
      opened_one |= open_new_world(NULL, argv[argpos], FALSE, NULL);
    }
    ++argpos;
  }
  if (!opened_one) {
    if (!open_new_world(gui, NULL, FALSE, NULL))
      return 0;
  }

  g_timeout_add(500, update_times_cb, NULL);

  gtk_main();

  /* Save and free all words that happen to be open yet. */
  while (open_worlds) {
    World *world = (World *) open_worlds->data;

    if (world->connected || world->connecting) {
      /* This leaks the return value, but we are exiting the program. */
      close_connection(world, TRUE);
    }
    save_world_to_file(world);
    save_command_history(world);
    save_permanent_variables(world);
    free_world(world);

    open_worlds = open_worlds->next;
  }

  save_global_preferences();
  save_world_to_file(default_world);

  PERL_SYS_TERM();

  gnutls_certificate_free_credentials(tls_certif_cred);
  gnutls_global_deinit();

  if (dumpFile) {
    fclose(dumpFile);
  }
  if (rdumpFile) {
    fclose(rdumpFile);
  }

  return 0;
}


gboolean
confirm_quit(void)
{
  GList     *worldptr;
  int        n_open;
  gboolean   retval = FALSE;
  GtkWidget *msgdlg;

  n_open = 0;
  worldptr = open_worlds;
  while (worldptr) {
    World *world = (World *) worldptr->data;
    if ((world->connected || world->connecting)
	&& !world->hooks.OnCloseConnected) {
      ++n_open;
    }
    worldptr = worldptr->next;
  }

  if (n_open == 0) {
    return TRUE;
  }

  msgdlg = gtk_message_dialog_new(GTK_WINDOW(wndMain),
                                  GTK_DIALOG_MODAL,
                                  GTK_MESSAGE_QUESTION,
                                  GTK_BUTTONS_NONE,
                                  ngettext("There is one open world. Do you really want to quit?",
                                           "There are %d open worlds. Do you really want to quit?",
                                           n_open),
                                  n_open);
  gtk_window_set_title(GTK_WINDOW(msgdlg), _("Really quit KildClient?"));
  gtk_dialog_add_buttons(GTK_DIALOG(msgdlg),
                         _("_Don't quit"), GTK_RESPONSE_NO,
                         _("_Quit"), GTK_RESPONSE_YES,
                         NULL);
  if (gtk_dialog_run(GTK_DIALOG(msgdlg)) == GTK_RESPONSE_YES) {
    retval = TRUE;
  }
  gtk_widget_destroy(msgdlg);
  return retval;
}


/*
 * Functions to get the user path (e.g. ~/.kildclient) and the
 * directory where the data files are installed.
 */
#ifndef __MINGW32__
#include <unistd.h>

const char *
get_kildclient_directory_path(void)
{
  char *homedir;

  if (!kc_user_dir) {
    homedir = getenv("HOME");
    kc_user_dir = g_strdup_printf("%s/.kildclient", homedir);
  }

  return kc_user_dir;
}

const char *
get_kildclient_installed_file(const char *file)
{
  /* Gets the path to a file in the data directory (typically
   * /usr/share/kildclient) */
  static char path[PATH_MAX];

  strcpy(path, PKGDATADIR "/");
  strcat(path, file);

  return path;
}
#else /* __MINGW32__ */
const char *
get_kildclient_directory_path(void)
{
  char appdatadir[MAX_PATH];

  if (!kc_user_dir) {
    if (SHGetFolderPath(NULL,
                        CSIDL_APPDATA,
                        NULL,
                        0,
                        appdatadir) >= 0) {
      kc_user_dir = g_strdup_printf("%s\\kildclient", appdatadir);
    } else {      /* Shouldn't fail, but... */
      kc_user_dir = g_strdup("c:\\kildclient");
    }
  }

  return kc_user_dir;
}


const char *
get_kildclient_installed_file(const char *file)
{
  /* Under windows, paths start at the same directory that
     kildclient.exe is in. file is the name of a file that should be
     in the same directory as kildclient.exe. This function finds the
     installation directory and returns the full path to that file. */
  static char path[PATH_MAX];
  char       *pos;

  GetModuleFileName(NULL, path, PATH_MAX - 1);
  pos = strrchr(path, '\\');

  strcpy(pos + 1, file);

  return path;
}
#endif
