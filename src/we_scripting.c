/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include "libintl-wrapper.h"
#include <gtk/gtk.h>

#include "kildclient.h"
#include "perlscript.h"


/***********************
 * Function prototypes *
 ***********************/


/*************************
 * File global variables *
 *************************/


GObject *
get_we_scripting_tab(GtkBuilder *ui_builder)
{
  GObject *panel;

  panel = gtk_builder_get_object(ui_builder, "we_panel_scripting");

  return panel;
}


void
fill_we_scripting_tab(World *world, GtkBuilder *ui_builder)
{
  GtkFileChooser *txtScriptFile;

  if (!world) {
    return;
  }

  txtScriptFile = GTK_FILE_CHOOSER(gtk_builder_get_object(ui_builder,
                                                          "txtScriptFile"));
  if (world->scriptfile) {
    gtk_file_chooser_set_filename(txtScriptFile, world->scriptfile);
  }
}



void
update_we_scripting_parameters(World *world, GtkBuilder *ui_builder)
{
  GtkFileChooser *txtScriptFile;
  gchar          *newval;

  txtScriptFile = GTK_FILE_CHOOSER(gtk_builder_get_object(ui_builder,
                                                          "txtScriptFile"));
  newval = gtk_file_chooser_get_filename(txtScriptFile);
  if (!newval) {
    g_free(world->scriptfile);
    world->scriptfile = NULL;
  } else if (!world->scriptfile || strcmp(world->scriptfile, newval) != 0) {
    g_free(world->scriptfile);
    world->scriptfile = g_strdup(newval);
  }
  g_free(newval);
}
