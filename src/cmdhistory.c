/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include "libintl-wrapper.h"
#include <locale.h>
#include <gtk/gtk.h>
#include <gmodule.h>

#include "kildclient.h"
#include "perlscript.h"



/**************
 * Data types *
 **************/
struct CmdHistorySendData_s
{
  World         *world;
  gint           lines_at_a_time;
  GSList        *list;
  GSList        *list_iter;
};
typedef struct CmdHistorySendData_s CmdHistorySendData;


/***********************
 * Function prototypes *
 ***********************/
static GtkWidget *create_cmd_history_dialog(GtkWindow *parent, World *world);
static gboolean   cmdhistory_send_timer_cb(gpointer data);
static void       save_tentative_command(World *world);
/* XML UI signals */
G_MODULE_EXPORT void menu_command_history_cb(GtkMenuItem *widget,
                                             gpointer     data);
G_MODULE_EXPORT void cmdhistory_recall_cb(GtkWidget *widget, gpointer data);
G_MODULE_EXPORT void cmdhistory_start_send_cb(GtkWidget *widget, gpointer data);
G_MODULE_EXPORT void cmdhistory_find_cb(GtkWidget *widget, gpointer data);


void
prev_or_next_command(World *world, gboolean next)
{
  gchar        *command;
  SimoComboBox *cmb = world->gui->cmbEntry;

  save_tentative_command(world);

  /* Update position */
  if (world->cmd_list_size) {
    if (!next) {
      if (!gtk_tree_model_iter_next(world->cmd_list,
                                    &world->current_cmd)) {
        gtk_tree_model_get_iter_first(world->cmd_list,
                                      &world->current_cmd);
      }
    } else {
      GtkTreePath *path;

      path = gtk_tree_model_get_path(world->cmd_list,
                                     &world->current_cmd);
      if (gtk_tree_path_prev(path)) {
        gtk_tree_model_get_iter(world->cmd_list,
                                &world->current_cmd,
                                path);
      } else {
        gtk_tree_model_iter_nth_child(world->cmd_list, &world->current_cmd,
                                      NULL, world->cmd_list_size);
      }
      gtk_tree_path_free(path);
    }

    /* Fetch new command */
    command = cmdhistory_get_command(world->cmd_list, &world->current_cmd);
    simo_combo_box_set_text(cmb, command);
    g_free(command);
  }
  simo_combo_box_select_region(cmb, 0, -1);
  world->cmd_position_changed = FALSE;
}


void
find_prev_or_next_command(World *world, gboolean next)
{
  SimoComboBox *cmb = world->gui->cmbEntry;
  gchar        *command;
  GtkTreeIter   start_iter;
  gboolean      found = FALSE;

  if (!world->cmd_list_size) {
    return;
  }

  if (!world->saved_command_find_search) {
    world->saved_command_find_search
      = g_strdup(simo_combo_box_get_text(cmb));
  }

  save_tentative_command(world);
  start_iter = world->current_cmd;

  while (1) {
    if (!next) {
      if (!gtk_tree_model_iter_next(world->cmd_list,
                                    &world->current_cmd)) {
        world->current_cmd = start_iter;
        break;
      }
    } else {
      GtkTreePath *path;

      path = gtk_tree_model_get_path(world->cmd_list,
                                     &world->current_cmd);
      if (gtk_tree_path_prev(path)) {
        gtk_tree_model_get_iter(world->cmd_list,
                                &world->current_cmd,
                                path);
      } else {
        gtk_tree_path_free(path);
        break;
      }
      gtk_tree_path_free(path);
    }

    command = cmdhistory_get_command(world->cmd_list, &world->current_cmd);
    if (g_str_has_prefix(command, world->saved_command_find_search)) {
      found = TRUE;
      break;
    }
    g_free(command);
  }

  if (found) {
    simo_combo_box_set_text(cmb, command);
    g_free(command);
    simo_combo_box_select_region(cmb, 0, -1);
    world->cmd_position_changed = FALSE;
  } else {
    world->current_cmd = start_iter;
  }
}


void
menu_command_history_cb(GtkMenuItem *widget, gpointer data)
{
  if (!currentWorld) {
    return;
  }

  if (!currentWorld->dlgCmdHistory) {
    currentWorld->dlgCmdHistory
      = create_cmd_history_dialog(GTK_WINDOW(wndMain), currentWorld);
  }

  gtk_widget_show_all(currentWorld->dlgCmdHistory);
  gtk_window_present(GTK_WINDOW(currentWorld->dlgCmdHistory));
}


static
GtkWidget *
create_cmd_history_dialog(GtkWindow *parent, World *world)
{
  gchar            *objects[] = { "dlgCmdHistory",
                                  "adjSpnCHDelay", "adjSpnNCommands",
                                  NULL };
  GError           *error = NULL;
  GObject          *dlg;
  GtkTreeView      *viewCommands;
  GtkTreeSelection *selection;
  GtkCellRenderer  *renderer;
  GObject          *btnSendClose;
  GObject          *btnRecallClose;
  GObject          *btnFind;
  GtkSpinButton    *spnDelay;
  GtkSpinButton    *spnNCommands;

  /* Create the dialog */
  if (!world->ui_builder) {
    world->ui_builder = gtk_builder_new();
  }
  if (!gtk_builder_add_objects_from_resource(world->ui_builder,
                                             "/ekalin/kildclient/dlgCmdHistory.ui",
                                             objects,
                                             &error)) {
    g_warning(_("Error loading UI from XML file: %s"), error->message);
    g_error_free(error);
    return NULL; /* This will cause warnings in the calling function, but
                    there is need to abort the program because of this. */
  }
  dlg = gtk_builder_get_object(world->ui_builder, "dlgCmdHistory");
  gtk_window_set_transient_for(GTK_WINDOW(dlg), GTK_WINDOW(wndMain));

  /* Configure tree view */
  viewCommands = GTK_TREE_VIEW(gtk_builder_get_object(world->ui_builder,
                                                      "viewCommands"));
  gtk_tree_view_set_model(viewCommands, world->cmd_list);
  renderer = gtk_cell_renderer_text_new();
  gtk_tree_view_insert_column_with_data_func(viewCommands,
                                             0,
                                             NULL,
                                             renderer,
                                             (GtkTreeCellDataFunc) completion_cell_data_function,
                                             NULL,
                                             NULL);
  selection = gtk_tree_view_get_selection(viewCommands);
  gtk_tree_selection_set_mode(selection, GTK_SELECTION_MULTIPLE);

  /* This information is used in the callbacks */
  btnSendClose = gtk_builder_get_object(world->ui_builder, "btnSendClose");
  g_object_set_data(btnSendClose, "close", GINT_TO_POINTER(1));
  btnRecallClose = gtk_builder_get_object(world->ui_builder, "btnRecallClose");
  g_object_set_data(btnRecallClose, "close", GINT_TO_POINTER(1));
  btnFind = gtk_builder_get_object(world->ui_builder, "btnFind");
  g_object_set_data(btnFind, "startsearch", GINT_TO_POINTER(1));

  /* Default values */
  spnDelay = GTK_SPIN_BUTTON(gtk_builder_get_object(world->ui_builder,
                                                    "spnCHDelay"));
  gtk_spin_button_set_value(spnDelay, globalPrefs.multi_cmd_delay);
  spnNCommands = GTK_SPIN_BUTTON(gtk_builder_get_object(world->ui_builder,
                                                        "spnNCommands"));
  gtk_spin_button_set_value(spnNCommands, globalPrefs.multi_cmd_group_size);

  /* Signals */
  gtk_builder_connect_signals(world->ui_builder, world);

  return GTK_WIDGET(dlg);
}


void
cmdhistory_recall_cb(GtkWidget *widget, gpointer data)
{
  World            *world = (World *) data;
  GtkTreeView      *viewCommands;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  GList            *selected_rows;
  gint              n_selected;
  gchar            *command;

  viewCommands = GTK_TREE_VIEW(gtk_builder_get_object(world->ui_builder,
                                                      "viewCommands"));
  selection = gtk_tree_view_get_selection(viewCommands);

  n_selected = gtk_tree_selection_count_selected_rows(selection);
  if (n_selected == 0) {
    kc_error_dialog(GTK_WINDOW(world->dlgCmdHistory),
                    _("No command selected."));
    return;
  } else if (n_selected > 1) {
    kc_error_dialog(GTK_WINDOW(world->dlgCmdHistory),
                    _("More than one command selected."));
    return;
  }

  if (GPOINTER_TO_INT(g_object_get_data(G_OBJECT(widget), "close"))) {
    gtk_widget_hide(world->dlgCmdHistory);
  }

  save_tentative_command(world);
  selected_rows = gtk_tree_selection_get_selected_rows(selection, &model);
  gtk_tree_model_get_iter(model, &world->current_cmd,
                          (GtkTreePath *) selected_rows->data);
  command = cmdhistory_get_command(world->cmd_list, &world->current_cmd);
  simo_combo_box_set_text(world->gui->cmbEntry, command);
  g_free(command);

  g_list_foreach(selected_rows, (GFunc) gtk_tree_path_free, NULL);
  g_list_free(selected_rows);
}


void
cmdhistory_start_send_cb(GtkWidget *widget, gpointer data)
{
  World            *world = (World *) data;
  GtkTreeView      *viewCommands;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  GtkSpinButton    *spnDelay;
  GtkSpinButton    *spnNCommands;
  GList            *selected_rows;
  GList            *list_iter;
  gdouble           delay;

  if (GPOINTER_TO_INT(g_object_get_data(G_OBJECT(widget), "close"))) {
    gtk_widget_hide(world->dlgCmdHistory);
  }

  viewCommands = GTK_TREE_VIEW(gtk_builder_get_object(world->ui_builder,
                                                      "viewCommands"));
  selection = gtk_tree_view_get_selection(viewCommands);

  selected_rows = gtk_tree_selection_get_selected_rows(selection, &model);
  list_iter = selected_rows;
  if (!list_iter) {
    return;
  }

  spnDelay = GTK_SPIN_BUTTON(gtk_builder_get_object(world->ui_builder,
                                                    "spnCHDelay"));
  delay = gtk_spin_button_get_value(spnDelay);
  spnNCommands = GTK_SPIN_BUTTON(gtk_builder_get_object(world->ui_builder,
                                                        "spnNCommands"));

  if (delay) {
    gchar              *command;
    GtkTreeIter         iter;
    GSList             *cmd_list = NULL;
    CmdHistorySendData *data;

    /* Create list of commands */
    while (list_iter) {
      gtk_tree_model_get_iter(model, &iter, (GtkTreePath *) list_iter->data);
      command = cmdhistory_get_command(model, &iter);
      cmd_list = g_slist_append(cmd_list, command);
      list_iter = list_iter->next;
    }

    data            = g_new(CmdHistorySendData, 1);
    data->world     = world;
    data->list      = cmd_list;
    data->list_iter = cmd_list;
    data->lines_at_a_time
      = gtk_spin_button_get_value_as_int(spnNCommands);

    /* Send first command immediately */
    if (cmdhistory_send_timer_cb(data)) {
      /* Start timeout function to send the rest (if necessary) */
      if (cmd_list) {
        g_timeout_add_full(G_PRIORITY_HIGH,
                           delay * 1000,
                           cmdhistory_send_timer_cb,
                           data,
                           NULL);
      }
    }
  } else {
    /* Send everything as fast as possible */
    while (list_iter) {
      GtkTreeIter  iter;
      gchar       *command;

      gtk_tree_model_get_iter(model, &iter, (GtkTreePath *) list_iter->data);
      command = cmdhistory_get_command(model, &iter);

      parse_commands(world, command);
      g_free(command);
      list_iter = list_iter->next;
    }
  }

  g_list_foreach(selected_rows, (GFunc) gtk_tree_path_free, NULL);
  g_list_free(selected_rows);
}


static
gboolean
cmdhistory_send_timer_cb(gpointer data)
{
  CmdHistorySendData *cmd_data = (CmdHistorySendData *) data;
  gint n;

  n = cmd_data->lines_at_a_time;
  while (n-- && cmd_data->list_iter) {
    gchar *command = (gchar *) cmd_data->list_iter->data;
    parse_commands(cmd_data->world, command);
    cmd_data->list_iter = cmd_data->list_iter->next;
  }

  /* No more commands to send */
  if (!cmd_data->list_iter) {
    g_slist_foreach(cmd_data->list, (GFunc) g_free, NULL);
    g_slist_free(cmd_data->list);
    g_free(cmd_data);

    return FALSE;
  }

  return TRUE;
}


void
cmdhistory_find_cb(GtkWidget *widget, gpointer data)
{
  World            *world = (World *) data;
  GtkTreeView      *viewCommands;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  gboolean          found;
  gboolean          error;
  GtkTreePath      *search_path = NULL;

  viewCommands = GTK_TREE_VIEW(gtk_builder_get_object(world->ui_builder,
                                                      "viewCommands"));
  model = gtk_tree_view_get_model(viewCommands);
  selection = gtk_tree_view_get_selection(viewCommands);

  /* Start search if asked */
  if (GPOINTER_TO_INT(g_object_get_data(G_OBJECT(widget), "startsearch"))) {
    GtkEntry        *txtSearchText;
    GtkToggleButton *radUpwards;

    if (!world->dlgCmdHistoryFind) {
      gchar  *objects[] = { "dlgFind", NULL };
      GError *gerror    = NULL;

      if (!gtk_builder_add_objects_from_resource(world->ui_builder,
                                                 "/ekalin/kildclient/dlgCmdHistory.ui",
                                                 objects,
                                                 &gerror)) {
        g_warning(_("Error loading UI from XML file: %s"), gerror->message);
        g_error_free(gerror);
        return;
      }

      world->dlgCmdHistoryFind
        = GTK_WIDGET(gtk_builder_get_object(world->ui_builder, "dlgFind"));
      gtk_window_set_transient_for(GTK_WINDOW(world->dlgCmdHistoryFind),
                                   GTK_WINDOW(world->dlgCmdHistory));
    }

    txtSearchText = GTK_ENTRY(gtk_builder_get_object(world->ui_builder,
                                                   "txtSearchText"));
    radUpwards    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(world->ui_builder,
                                                             "radUpwards"));

    gtk_dialog_set_default_response(GTK_DIALOG(world->dlgCmdHistoryFind),
                                    GTK_RESPONSE_OK);

    gtk_widget_show_all(world->dlgCmdHistoryFind);
    while (1) {
      if (gtk_dialog_run(GTK_DIALOG(world->dlgCmdHistoryFind)) == GTK_RESPONSE_OK) {
        const gchar *search_text;

        search_text = gtk_entry_get_text(txtSearchText);
        if (strcmp(search_text, "") == 0) {
          kc_error_dialog(GTK_WINDOW(world->dlgCmdHistory),
                          _("Enter some text to search for."));
          continue;
        }

        world->cmdfind_upwards = gtk_toggle_button_get_active(radUpwards);
        g_free(world->cmdfind_string);
        world->cmdfind_string = g_strdup(search_text);
        break;
      } else {
        /* Clear dialog */
        gtk_entry_set_text(txtSearchText, "");
        gtk_widget_hide(world->dlgCmdHistoryFind);
        return;
      }
    }
    /* Clear dialog */
    gtk_entry_set_text(txtSearchText, "");
    gtk_widget_hide(world->dlgCmdHistoryFind);

    if (world->cmdfind_row) {
      gtk_tree_row_reference_free(world->cmdfind_row);
    }
  } else {   /* Verify if there was a search */
    if (!world->cmdfind_string) {
      kc_error_dialog(GTK_WINDOW(world->dlgCmdHistory),
                      _("Use the Find button to find the first result."));
      return;
    }

    search_path = gtk_tree_row_reference_get_path(world->cmdfind_row);
  }

  if (!search_path) {
    if (world->cmdfind_upwards) {
      search_path = gtk_tree_path_new_from_indices(world->cmd_list_size, -1);
    } else {
      search_path = gtk_tree_path_new_first();
    }
    world->cmdfind_row = gtk_tree_row_reference_new(model, search_path);
  }

  error = FALSE;
  found = FALSE;
  while (!found) {
    GtkTreeIter  iter;
    gchar       *command;

    gtk_tree_model_get_iter(model, &iter, search_path);
    command = cmdhistory_get_command(model, &iter);
    if (strstr(command, world->cmdfind_string)) { /* Found */
      gtk_tree_selection_unselect_all(selection);
      gtk_tree_selection_select_iter(selection, &iter);
      gtk_tree_view_scroll_to_cell(viewCommands,
                                   search_path, NULL,
                                   FALSE, 0, 0);
      found = TRUE;
    }
    g_free(command);

    /* Move to next/previous row */
    if (world->cmdfind_upwards) {
      if (!gtk_tree_path_prev(search_path)) {
        error = TRUE;
      }
    } else {
      if (!gtk_tree_model_iter_next(model, &iter)) {
        error = TRUE;
      } else {
        gtk_tree_path_next(search_path);
      }
    }

    if (error && !found) {
      kc_error_dialog(GTK_WINDOW(world->dlgCmdHistory),
                      _("Command not found."));
      break;
    }
  }

  gtk_tree_row_reference_free(world->cmdfind_row);
  world->cmdfind_row = gtk_tree_row_reference_new(model, search_path);
}


gchar *
cmdhistory_get_command(GtkTreeModel *model, GtkTreeIter *iter)
{
  gchar *command;

  gtk_tree_model_get(model, iter,
                     CMDLIST_COL_TENTATIVE, &command,
                     -1);
  if (!command) {
    gtk_tree_model_get(model, iter,
                       CMDLIST_COL_COMMAND, &command,
                       -1);
  }

  return command;
}


void
add_recent_command(World *world, const gchar *command)
{
  GtkTreeIter  previous;
  GtkTreeIter  most_recent;
  gboolean     go_on;
  gboolean     is_repeated = FALSE;
  gchar       *previous_command = NULL;

  if (!world->repeat_commands || world->cmd_has_been_edited
      || world->cmd_just_selected_from_combo) {
    gtk_tree_model_get_iter_first(world->cmd_list, &most_recent);
    gtk_list_store_set(GTK_LIST_STORE(world->cmd_list), &world->current_cmd,
                       CMDLIST_COL_TENTATIVE, NULL,
                       -1);

    /* Try to locate the same command in the history */
    go_on = gtk_tree_model_iter_nth_child(world->cmd_list, &previous,
                                          NULL, 1);
    while (go_on) {
      previous_command = cmdhistory_get_command(world->cmd_list, &previous);
      if (previous_command && strcmp(command, previous_command) == 0) {
        is_repeated = TRUE;
        g_free(previous_command);
        break;
      }

      go_on = gtk_tree_model_iter_next(world->cmd_list, &previous);
    }

    if (is_repeated) {
      /* A duplicate command */
      /* It is not added again, but the old command moved into position */
      gtk_list_store_move_after(GTK_LIST_STORE(world->cmd_list),
                                &previous,
                                &most_recent);
      /* most_recent might have become invalid */
      gtk_tree_model_get_iter_first(world->cmd_list, &most_recent);

      if (world->repeat_commands) {
        /* In this case, a duplicate entry has already been added, so let's
           remove it. */
        gtk_list_store_remove(GTK_LIST_STORE(world->cmd_list), &most_recent);
        --world->cmd_list_size;
        world->cmd_has_been_edited = FALSE;
        world->gui->execute_changed_signal = TRUE;
        world->cmd_just_selected_from_combo = FALSE;
      } else {
        gtk_list_store_set(GTK_LIST_STORE(world->cmd_list), &most_recent,
                           CMDLIST_COL_TENTATIVE, "",
                           -1);
      }
    } else {
      /* A new command */
      gtk_list_store_set(GTK_LIST_STORE(world->cmd_list), &most_recent,
                         CMDLIST_COL_COMMAND, command,
                         CMDLIST_COL_TENTATIVE, NULL,
                         -1);
      if (!world->repeat_commands) {
        insert_recent_command(world, "");
      } else {
        world->cmd_has_been_edited = FALSE;
        world->cmd_just_selected_from_combo = FALSE;
        world->gui->execute_changed_signal = TRUE;
      }
    }
  }

  /* Set the pointer to point to the new command */
  gtk_tree_model_get_iter_first(world->cmd_list, &world->current_cmd);
}


void
insert_recent_command(World *world, const gchar *command)
{
  GtkTreeIter new;

  gtk_list_store_prepend(GTK_LIST_STORE(world->cmd_list), &new);
  gtk_list_store_set(GTK_LIST_STORE(world->cmd_list), &new,
                     CMDLIST_COL_COMMAND, NULL,
                     CMDLIST_COL_TENTATIVE, command,
                     -1);
  ++world->cmd_list_size;

  /* See if the maximum list size has been reached */
  if (world->cmd_list_size >= world->commands_to_save) {
    GtkTreeIter last_iter;

    if (gtk_tree_model_iter_nth_child(world->cmd_list, &last_iter,
                                      NULL, world->commands_to_save + 1)) {
      while (gtk_list_store_remove(GTK_LIST_STORE(world->cmd_list),
                                   &last_iter)) {
        --world->cmd_list_size;
      }
      /* This is for the last removed one --- when the command returns
         false, and the body of the loop is not called. */
      --world->cmd_list_size;
    }
  }
}


static
void
save_tentative_command(World *world)
{
  if (world->repeat_commands && !world->cmd_has_been_edited) {
    insert_recent_command(world, "");
    world->gui->execute_changed_signal = FALSE;
    world->cmd_has_been_edited = TRUE;
    world->cmd_just_selected_from_combo = FALSE;
  } else {
    /* Save possibly modified text as tentative command */
    gtk_list_store_set(GTK_LIST_STORE(world->cmd_list), &world->current_cmd,
                       CMDLIST_COL_TENTATIVE,
                       simo_combo_box_get_text(world->gui->cmbEntry),
                       -1);
  }
}
