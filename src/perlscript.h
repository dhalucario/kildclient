/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */


#ifndef __PERLSCRIPT_H
#define __PERLSCRIPT_H

#include <EXTERN.h>
#include <perl.h>
/* Conflicts with C library function */
#undef gethostbyname
#undef ctime
/* Conflits with gettext */
#undef _
#define _(string)  gettext(string)
#define N_(string) (string)

#ifdef __MINGW32__
/* Why the hell does Perl (under Mingw32) does all that? */
#undef FILE
#undef clearerr
#undef fclose
#undef fdopen
#undef feof
#undef ferror
#undef fflush
#undef fgetc
#undef fgetpos
#undef fgets
#undef fileno
#undef flockfile
#undef fopen
#undef fprintf
#undef fputc
#undef fputs
#undef fread
#undef freopen
#undef fscanf
#undef fseek
#undef fsetpos
#undef ftell
#undef ftrylockfile
#undef funlockfile
#undef fwrite
#undef getc
#undef getc_unlocked
#undef getw
#undef pclose
#undef popen
#undef putc
#undef putc_unlocked
#undef putw
#undef rewind
#undef setbuf
#undef setvbuf
#undef tmpfile
#undef ungetc
#undef vfprintf
#undef printf
#undef mkdir
#undef stat
#undef unlink

#undef  stdin
#define stdin (&_iob[0])
#undef  stdout
#define stdout (&_iob[1])
#undef  stderr
#define stderr (&_iob[2])

#undef htons
#undef gethostbyname
#undef socket
#undef connect
#undef write
#undef read
#undef send
#undef recv
#undef close
#undef getsockopt
#undef setsockopt

#undef gettimeofday
#undef malloc
#undef free
#undef calloc
#undef realloc

#undef exit

#undef __attribute__
#endif /* __MINGW32 */


/***********************
 * Forward declaration *
 ***********************/
struct world_s;


/**********
 * Macros *
 **********/
#define GRAB_PERL(w)    g_rec_mutex_lock(&w->perl_mutex);  \
                        PERL_SET_CONTEXT(w->perl_interpreter)
#define RELEASE_PERL(w) g_rec_mutex_unlock(&w->perl_mutex)


/***********************
 * Function prototypes *
 ***********************/
void  init_perl_system(void);
void  init_perl_script(struct world_s *world);
void  destruct_perl_script(PerlInterpreter *perl_interpreter);
void  perl_script_import_file(const char *file);
int   perl_match(const char      *line,
                 SV              *pattern_re,
                 int             *match_start,
                 int             *match_end,
                 int              substring,
                 AV             **matches);
char *perl_substitute(const char *line,
                      SV         *pattern_re,
                      const char *substitution,
                      int         perl_eval,
                      int         global);
SV   *precompute_re(const char      *re_string,
                    int              ignore_case);
void  perl_set_arg_array(AV *array);
void  parse_server_data(char            *protocol,
                        char            *data);
void  menu_perl_run_cb(GtkWidget *widget, gpointer data);
void  run_perl_command(char *cmd);


/***********************************
 * KCWin XS subroutines prototypes *
 ***********************************/
void XS_KCWin__new(pTHX_ CV *perlcv);
void XS_KCWin_feed(pTHX_ CV *perlcv);
void XS_KCWin__destroy(pTHX_ CV *perlcv);


#endif
