/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <sys/time.h>
#include "libintl-wrapper.h"
#include <locale.h>
#include <gtk/gtk.h>
#include <zlib.h>

#ifndef __MINGW32__
#  include <fcntl.h>
#  include <sys/types.h>
#  include <sys/socket.h>
#  include <netdb.h>
#  include <arpa/inet.h>
#else /* ! defined __MINGW32__ */
#  include <winsock2.h>
#  include <ws2tcpip.h>
#endif

#include "kildclient.h"
#include "perlscript.h"
#include "net.h"


/*************************
 * File global variables *
 *************************/
#define MAX_US 250000


/**************
 * Data types *
 **************/


/***********************
 * Function prototypes *
 ***********************/
static gboolean connect_ready_cb(GIOChannel   *iochan,
                                 GIOCondition  cond,
                                 gpointer      data);
static gboolean connected_to_dispatcher(World *world);
static gboolean tls_handshake_cb(GIOChannel   *iochan,
                                 GIOCondition  cond,
                                 gpointer      data);
static gboolean tls_handshake_timeout_cb(gpointer data);
static void     disconnected_decide_action(World *world, gint action);
static gboolean data_ready_cb(GIOChannel   *iochan,
                              GIOCondition  cond,
                              gpointer      data);
static gboolean flush_buffer(World *world, int action);
static void     convert_recv_line(World *world,
                                  int start, int end,
                                  gboolean incomplete);
static ssize_t  write_escaped(World *world, const void *buf, size_t count);


#ifndef __MINGW32__
#  define socket_valid(a) ((a) != -1)
#else
#  define socket_valid(a) ((a) != INVALID_SOCKET)
#endif


void
connect_to(World *world)
{
  gchar *msg;
  struct resolve_data_s *resolve_data;
  GThread *resolver_thread;

  if (world->use_tls) {
    gnutls_init(&world->tls_session, GNUTLS_CLIENT);
    gnutls_priority_set_direct(world->tls_session, "NORMAL", NULL);
    gnutls_credentials_set(world->tls_session,
                           GNUTLS_CRD_CERTIFICATE, tls_certif_cred);
  }

  /* Set up gui */
  if (!world->connected_at_least_once) {
    configure_gui(world->gui, world);
    prepare_display_name(world);
  }
  gtk_label_set_text(world->gui->lblNotebook, world->display_name);

  if (g_list_index(open_worlds, world) == -1) {
    open_worlds = g_list_append(open_worlds, world);
  }

  world->rawbytes = world->bytes = 0;

  /* Set it as the last opened one */
  g_free(globalPrefs.last_open_world);
  globalPrefs.last_open_world = g_strdup(world->name);

  currentWorld = world;
  world_for_perl = world;
  if (!world->connected_at_least_once) {
    init_perl_script(world);

    init_threads(world);

    GRAB_PERL(world);
    load_permanent_variables(world);
    load_startup_plugins(world);
    if (world->scriptfile) {
      perl_script_import_file(world->scriptfile);
    }
    RELEASE_PERL(world);

    load_command_history(world);
  } else {
    if (world->has_unread_text) {
      world->has_unread_text = 0;
      --worlds_with_new_text;
      adjust_window_title();
    }
  }

  if (!world->connected_at_least_once) {
    trigger_precompute_res(world);
    alias_precompute_res(world);
  }

  if (!world->connected_at_least_once) {
    start_timers(world);
  }

  world->connected_at_least_once = TRUE;

  proxy_info_free(&world->proxy_used);
  if (world->proxy.type == PROXY_USE_GLOBAL) {
    proxy_info_copy(&globalPrefs.proxy, &world->proxy_used);
  } else if (world->proxy.type == PROXY_NONE) {
    world->proxy_used.type = PROXY_NONE;
  } else {
    proxy_info_copy(&world->proxy, &world->proxy_used);
  }

  if (world->proxy_used.type == PROXY_SOCKS5) {
    msg = g_strdup_printf(_("Resolving proxy host %s..."),
                          world->proxy_used.server);
  } else {
    msg = g_strdup_printf(_("Resolving host %s..."), world->host);
  }
  gtk_label_set_text(world->gui->lblStatus, msg);
  ansitextview_append_string_with_fgcolor(world->gui, msg,
                                          globalPrefs.idxInfoMsgColor);
  ansitextview_append_string(world->gui, "\n");
  g_free(msg);

  world->connecting = TRUE;

  /* Spawn a thread to resolve the host name */
  resolve_data = g_new(struct resolve_data_s, 1);
  resolve_data->world    = world;
  if (world->proxy_used.type == PROXY_SOCKS4) {
    resolve_data->host      = g_strdup(world->host);
    resolve_data->port      = g_strdup(world->port);
    resolve_data->callback  = proxy_socks4_resolved_real_host_cb;
    resolve_data->ai_family = AF_INET;
  } else if (world->proxy_used.type == PROXY_SOCKS5) {
    resolve_data->host     = g_strdup(world->proxy_used.server);
    resolve_data->port     = g_strdup(world->proxy_used.port);
    resolve_data->callback = continue_connecting_cb;
    resolve_data->ai_family = AF_UNSPEC;
  } else if (world->proxy_used.type == PROXY_HTTP) {
    resolve_data->host     = g_strdup(world->proxy_used.server);
    resolve_data->port     = g_strdup(world->proxy_used.port);
    resolve_data->callback = continue_connecting_cb;
    resolve_data->ai_family = AF_UNSPEC;
  } else {
    resolve_data->host     = g_strdup(world->host);
    resolve_data->port     = g_strdup(world->port);
    resolve_data->callback = continue_connecting_cb;
    resolve_data->ai_family = AF_UNSPEC;
  }

  resolver_thread = g_thread_new("resolver",
                                 resolve_name_thread, resolve_data);
  g_thread_unref(resolver_thread);
}


gpointer
resolve_name_thread(gpointer data)
{
  struct resolve_data_s *resolve_data = (struct resolve_data_s *) data;
  struct addrinfo  hints;

  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family   = resolve_data->ai_family;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_protocol = 0;
  hints.ai_flags    = 0;

  resolve_data->ret = getaddrinfo(resolve_data->host, resolve_data->port,
                                  &hints,
                                  &resolve_data->addrinfo);
  g_idle_add(resolve_data->callback, resolve_data);

  return NULL;
}


gboolean
continue_connecting_cb(gpointer data)
{
  struct resolve_data_s *resolve_data = (struct resolve_data_s *) data;
  World *world = resolve_data->world;
  gchar *msg;
  int    ret;
#ifndef __MINGW32__
  int    flags;
#else /* defined __MINGW32__ */
  u_long flags;
#endif

  g_free(resolve_data->port);

  if (g_list_index(open_worlds, world) == -1) {
    /* It could happen that the world has been closed */
    g_free(resolve_data->host);
    g_free(resolve_data);

    return FALSE;
  }

  if (resolve_data->ret != 0) {
    gchar *msg;
    gsize  len;

    msg = g_locale_to_utf8(gai_strerror(resolve_data->ret), -1,
                           NULL, &len, NULL);
    world->errmsg = g_strdup_printf(_("Could not resolve host %s: %s"),
                                    resolve_data->host, msg);
    g_free(msg);
    disconnect_world(world, FALSE);

    g_free(resolve_data->host);
    g_free(resolve_data);

    /* Only run once */
    return FALSE;
  }

  world->addrinfo = resolve_data->addrinfo;
  g_free(resolve_data->host);
  g_free(resolve_data);

  /* Create IP address representation */
  if (!world->host_ip) {
    world->host_ip = g_malloc(INET6_ADDRSTRLEN);
    /* Just in case it fails... */
    strcpy(world->host_ip, "?");
    getnameinfo(world->addrinfo->ai_addr, world->addrinfo->ai_addrlen,
                world->host_ip, INET6_ADDRSTRLEN,
                NULL, 0,
                NI_NUMERICHOST);
  }

  if (world->proxy_used.type == PROXY_NONE) {
    msg = g_strdup_printf(_("Attempting to connect to host %s (%s) port %s..."),
                          world->host, world->host_ip, world->port);
  } else {
    msg = g_strdup_printf(_("Attempting to connect to proxy host %s (%s) port %s..."),
                          world->proxy_used.server,
                          world->host_ip,
                          world->proxy_used.port);
  }
  ansitextview_append_string_with_fgcolor(world->gui, msg,
                                          globalPrefs.idxInfoMsgColor);
  ansitextview_append_string(world->gui, "\n");
  gtk_label_set_text(world->gui->lblStatus, msg);
  g_free(msg);

  world->sock = socket(world->addrinfo->ai_family,
                       world->addrinfo->ai_socktype,
                       world->addrinfo->ai_protocol);
  if (!socket_valid(world->sock)) {
#ifndef __MINGW32__
    world->errmsg = g_strdup_printf(_("Could not create socket: %s"),
                                    g_strerror(errno));
#else /* defined __MINGW32__ */
    world->errmsg = g_strdup_printf(_("Could not create socket: error #%d"),
                                    WSAGetLastError());
#endif
    disconnect_world(world, FALSE);
    return FALSE;
  }
#ifndef __MINGW32__
  flags = fcntl(world->sock, F_GETFL);
  flags |= O_NONBLOCK;
  fcntl(world->sock, F_SETFL, flags);
#else /* defined __MINGW32__ */
  flags = 1;
  ioctlsocket(world->sock, FIONBIO, &flags);
#endif

  ret = connect(world->sock,
                world->addrinfo->ai_addr, world->addrinfo->ai_addrlen);
  if (ret < 0) {
#ifndef __MINGW32__
    if (errno != EINPROGRESS && errno != EALREADY) {
      world->errmsg = g_strdup_printf(_("Could not connect to host %s port %s: %s"),
                                      world->host, world->port,
                                      g_strerror(errno));
      return FALSE;
    }
#else /* defined __MINGW32__ */
    int error;

    error = WSAGetLastError();
    if (error != WSAEWOULDBLOCK) {
      world->errmsg = g_strdup_printf(_("Could not connect to host %s port %s: error #%d"),
                                      world->host, world->port,
                                      error);
      return FALSE;
    }
#endif
  }

  /* Enable TCP Keep Alive if selected */
  if (world->keep_alive) {
    int optval = 1;
    setsockopt(world->sock, SOL_SOCKET, SO_KEEPALIVE, &optval, sizeof(optval));
  }

  world->noecho = FALSE;
#ifndef __MINGW32__
  world->iochan = g_io_channel_unix_new(world->sock);
#else  /* defined __MINGW32__ */
  world->iochan = g_io_channel_win32_new_socket(world->sock);
#endif
  world->io_watch_id = g_io_add_watch(world->iochan,
                                      G_IO_OUT | G_IO_ERR,
                                      connect_ready_cb,
                                      world);
  /* We'll do character code handling later */
  g_io_channel_set_encoding(world->iochan, NULL, NULL);

  if (!world->connecting) {
    connected_to_dispatcher(world);
  }

  /* This is only called once. */
  return FALSE;
}


#ifndef __MINGW32__
static
gboolean
connect_ready_cb(GIOChannel   *iochan,
                 GIOCondition  cond,
                 gpointer      data)
{
  World *world;
  int    ret;

  world = (World *) data;

  ret = connect(world->sock,
                world->addrinfo->ai_addr, world->addrinfo->ai_addrlen);
  if (ret < 0 && errno != EISCONN) {
    if (errno == EALREADY || errno == EINPROGRESS) {
      return TRUE;  /* Still connecting. */
    }

    if (world->proxy_used.type == PROXY_NONE) {
      world->errmsg = g_strdup_printf(_("Unable to connect to host %s: %s."),
                                      world->host, g_strerror(errno));
    } else {
      world->errmsg = g_strdup_printf(_("Unable to connect to proxy host %s: %s."),
                                      world->proxy_used.server,
                                      g_strerror(errno));
    }

    disconnect_world(world, FALSE);
    return FALSE;
  } else {
    connected_to_dispatcher(world);
    return FALSE;
  }
}
#else /* defined __MINGW32__ */
gboolean
connect_ready_cb(GIOChannel   *iochan,
                 GIOCondition  cond,
                 gpointer      data)
{
  World *world;
  int    ret;
  int    error;
  int    sockerror = 0;
  int    optlen = sizeof(sockerror);

  world = (World *) data;

  /* Apparently, under Windows it always fails */
  ret = connect(world->sock,
                world->addrinfo->ai_addr, world->addrinfo->ai_addrlen);
  error = WSAGetLastError();
  if (error == WSAEISCONN) { /* Connection succeeded */
    connected_to_dispatcher(world);
    return FALSE;
  }

  ret = getsockopt(world->sock, SOL_SOCKET, SO_ERROR,
                   (char *) &sockerror, &optlen);
  if (ret != 0) {
    sockerror = WSAGetLastError();
  }
  if (sockerror) {
    if (world->proxy_used.type == PROXY_NONE) {
      world->errmsg = g_strdup_printf(_("Unable to connect to host %s: error #%d."),
                                      world->host, sockerror);
    } else {
      world->errmsg = g_strdup_printf(_("Unable to connect to proxy host %s: error #%d."),
                                      world->proxy_used.server, sockerror);
    }
    disconnect_world(world, FALSE);
    return FALSE;
  }

  if (error == WSAEALREADY || error == WSAEINVAL || error == WSAEWOULDBLOCK) {
    return TRUE;  /* Still connecting. */
  }

  /* This should never be reached. But under Windows, it is better
     to be safe. */
  g_warning("Unreachable code in connect_read_cb.");
  if (world->proxy_used.type == PROXY_NONE) {
    world->errmsg = g_strdup_printf(_("Unable to connect to host %s: error #%d."),
                                    world->host, sockerror);
  } else {
    world->errmsg = g_strdup_printf(_("Unable to connect to proxy host %s: error #%d."),
                                    world->proxy_used.server, sockerror);
  }
  disconnect_world(world, FALSE);
  return FALSE;
}
#endif


static
gboolean
connected_to_dispatcher(World *world)
{
  /* When this function is called, a connection to the proxy host or
     the real host (if no proxy is being used) has been established.
     This function then calls the appropriate action if a proxy is
     being used. */
  if (world->proxy_used.type == PROXY_SOCKS4) {
    return proxy_socks4_connected_to(world);
  } else if (world->proxy_used.type == PROXY_SOCKS5) {
    return proxy_socks5_connected_to(world);
  } else if (world->proxy_used.type == PROXY_HTTP) {
    return proxy_http_connected_to(world);
  } else {
    return perform_tls_handshake(world);
  }
}


gboolean
perform_tls_handshake(World *world)
{
  gchar *status_text;

  if (world->use_tls) {
    status_text = g_strdup_printf(_("Performing SSL handshake..."));
    ansitextview_append_string_with_fgcolor(world->gui, status_text,
                                            globalPrefs.idxInfoMsgColor);
    ansitextview_append_string(world->gui, "\n");
    gtk_label_set_text(world->gui->lblStatus, status_text);
    g_free(status_text);

    gnutls_transport_set_int(world->tls_session, world->sock);
    /* Register callback to perform the TLS handshake
     * The first action is we sending data (ClientHello) */
    world->tls_handshake_dir = 1;
    world->io_watch_id = g_io_add_watch(world->iochan,
                                        G_IO_OUT | G_IO_HUP | G_IO_ERR,
                                        tls_handshake_cb,
                                        world);
    g_timeout_add_seconds(60, tls_handshake_timeout_cb, world);
    return TRUE;
  } else {
    return connected_to(world);
  }
}


static
gboolean
tls_handshake_cb(GIOChannel   *iochan,
                 GIOCondition  cond,
                 gpointer      data)
{
  World *world = (World *) data;
  int ret;

  ret = gnutls_handshake(world->tls_session);

  if (ret == GNUTLS_E_SUCCESS) {
    /* Sucess */
    connected_to(world);
    /* Do not call the function again */
    return FALSE;
  }

  if (gnutls_error_is_fatal(ret) != 0) {
    /* Fatal error */
    world->errmsg = g_strdup_printf(_("SSL handshake failed: %s"),
                                    gnutls_strerror(ret));
    disconnect_world(world, FALSE);
    /* Do not call the function again */
    return FALSE;
  }

  /* Continue when more data is available, watching the direction. */
  int dir = gnutls_record_get_direction(world->tls_session);
  if (dir != world->tls_handshake_dir) {
    GIOCondition cond = G_IO_HUP | G_IO_ERR;
    if (dir == 0) {
      cond |= G_IO_IN;
    } else {
      cond |= G_IO_OUT;
    }

    world->io_watch_id = g_io_add_watch(world->iochan,
                                        cond,
                                        tls_handshake_cb,
                                        world);
    world->tls_handshake_dir = dir;
    /* A new callback has been set up for the new direction, remove this one */
    return FALSE;
  } else {
    /* Continue */
    return TRUE;
  }
}


static
gboolean
tls_handshake_timeout_cb(gpointer data)
{
  /* Called 60 seconds after a TLS handshake has started. If the connection
   * has not completed, aborts it. */
  World *world = (World *) data;

  // The world might have been closed
  if (g_list_index(open_worlds, world) != -1 && world->connecting) {
    world->errmsg = g_strdup_printf(_("SSL handshake took too long."));
    disconnect_world(world, FALSE);
  }

  /* Never called again */
  return FALSE;
}


gboolean
connected_to(World *world)
{
  /* A connection has been established to the real mud host, via proxy
     or indirectly. */
  gchar *status_text;

  world->connected         = TRUE;
  world->connecting        = FALSE;
  world->connected_time    = time(NULL);
  world->last_command_time = time(NULL);

  if (world->proxy_used.type == PROXY_NONE) {
    status_text = g_strdup_printf(_("Connected to host %s (%s) port %s."),
                                  world->host, world->host_ip, world->port);
  } else {
    status_text = g_strdup_printf(_("Connected to host %s port %s, via proxy %s (%s) port %s."),
                                  world->host, world->port,
                                  world->proxy_used.server, world->host_ip,
                                  world->proxy_used.port);
  }
  if (world->use_tls) {
    gchar *tmp = status_text;
    status_text = g_strconcat(tmp, _(" SSL enabled."), NULL);
    g_free(tmp);
  }
  ansitextview_append_string_with_fgcolor(world->gui, status_text,
                                          globalPrefs.idxInfoMsgColor);
  ansitextview_append_string(world->gui, "\n");
  gtk_label_set_text(world->gui->lblStatus, status_text);
  g_free(status_text);
  adjust_window_title();

  perform_auto_logon(world);
  execute_hook(world, "OnConnect", NULL);
  if (world->log_autostart) {
    stop_log(world);
    start_log_and_display_result(world);
  }

  /* We now read the socket */
  g_source_remove(world->io_watch_id);
  world->io_watch_id = g_io_add_watch_full(world->iochan,
                                           G_PRIORITY_HIGH,
                                           G_IO_IN | G_IO_HUP | G_IO_ERR,
                                           data_ready_cb,
                                           world,
                                           NULL);

  /* No longer needed */
  freeaddrinfo(world->addrinfo);
  world->addrinfo = NULL;

  return TRUE;
}


gboolean
disconnect_world_idle(gpointer data)
{
  disconnect_world(data, TRUE);

  return FALSE;
}


void
disconnect_world(gpointer data, gboolean run_hook)
{
  /* This function is called as an idle function, so it must return FALSE. */
  World *world;
  char  *msg;
  gint   action;

  world = (World *) data;

  if (!world->connected && !world->connecting) {
    return;
  }

  /* In case we are still connecting and it has not been free'd */
  if (world->addrinfo) {
    freeaddrinfo(world->addrinfo);
    world->addrinfo = NULL;
  }

  msg = close_connection(world, run_hook);

  execute_hook(world, "OnDisconnect", NULL);

  currentWorld = NULL;

  /* Here we decide what to do */
  action = disconnected_msgbox(msg, TRUE);
  g_free(msg);

  disconnected_decide_action(world, action);
}


gboolean
reconnect_world(gpointer data)
{
  World *world = (World *) data;

  if (world->connected || world->connecting)
    g_free(close_connection(world, TRUE));

  /* This function is called as an idle function, so it must return FALSE. */
  disconnected_decide_action((World *) data, RESP_RECONNECT);
  return FALSE;
}


gboolean
connect_another(gpointer data)
{
  /* This function is called as an idle function, so it must return FALSE. */
  disconnected_decide_action((World *) data, RESP_OTHER);
  return FALSE;
}


static
void
disconnected_decide_action(World *world, gint action)
{
  while (action != GTK_RESPONSE_CLOSE) {
    if (action == RESP_OTHER) {
      gboolean offline_selected = FALSE;

      save_world_to_file(world);
      save_command_history(world);
      save_permanent_variables(world);

      open_new_world(world->gui, NULL, TRUE, &offline_selected);
      if (offline_selected) {
        mark_as_offline(world);
        return;
      }

      break;
    }
    if (action == RESP_OFFLINE) {
      mark_as_offline(world);
      return;
    }
    /* Else, which means reconnect */
    connect_to(world);
    return;
  }

  /* If we have gotten here, it means the current world must be closed,
     because either we selected close or connected to another one. */
  remove_world(world, action == GTK_RESPONSE_CLOSE);
}


gchar *
close_connection(World *world, gboolean run_hook)
{
  /* Note: this function should only be called if the world is
     connected or connecting. */
  gchar *msg;

  if (run_hook) {
    execute_hook(world, "OnCloseConnected", NULL);
  }

  flush_buffer(world, 2);
  if (world->errmsg) {
    msg = world->errmsg;
    world->errmsg = NULL;
  } else {
    msg = g_strdup_printf(_("Disconnected from host %s."), world->host);
  }
  ansitextview_append_string_with_fgcolor(world->gui, msg,
                                          globalPrefs.idxInfoMsgColor);
  ansitextview_append_string(world->gui, "\n");

  if (world->use_tls) {
    gnutls_bye(world->tls_session, GNUTLS_SHUT_RDWR);
  }
#ifndef __MINGW32__
  close(world->sock);
#else /* defined __MINGW32__ */
  closesocket(world->sock);
#endif
  world->connected = FALSE;
  world->connecting = FALSE;
  if (world->use_tls) {
    gnutls_deinit(world->tls_session);
  }

  if (world->iochan) {
    g_io_channel_unref(world->iochan);
    g_source_remove(world->io_watch_id);
  }

  if (world->timeout_id) {
    g_source_remove(world->timeout_id);
    world->timeout_id = 0;
  }

  if (world->zstream) {
    end_mccp(world);
  }

  return msg;
}


static
gboolean
data_ready_cb(GIOChannel   *iochan,
              GIOCondition  cond,
              gpointer      data)
{
  World          *world;
  ssize_t         nread;
  unsigned char  *nlptr;
  int             start;
  int             nlpos;

  world = (World *) data;

  /* What if we have read a very long line and the buffer is full?
     It does happen in some muds. */
  if (world->pbuf_size == MAX_BUFFER) {
    flush_buffer(world, 1);
  }

  nread = kc_recv(world,
                  world->inbuffer,
                  MAX_BUFFER - world->pbuf_size);
  if (nread <= 0) {      /* Connection closed */
    if (nread != 0) {    /* Error */
      if (world->use_tls) {
        if (gnutls_error_is_fatal(nread) == 0) {
          return TRUE;
        }

        world->errmsg = g_strdup_printf(_("Disconnected from host %s: %s."),
                                        world->host, gnutls_strerror(nread));
      } else {
#ifndef __MINGW32__
        if (errno == EAGAIN || errno == EWOULDBLOCK) {
          return TRUE;
        }

        world->errmsg = g_strdup_printf(_("Disconnected from host %s: %s."),
                                      world->host, g_strerror(errno));
#else /* defined __MINGW32__ */
        int errcode = WSAGetLastError();

        if (errcode == WSAEINTR
            || errcode == WSAEINPROGRESS
            || errcode == WSAEWOULDBLOCK) {
          return TRUE;
        }

        world->errmsg = g_strdup_printf(_("Disconnected from host %s: error #%d."),
                                        world->host, errcode);
#endif
      }
    }
    disconnect_world(world, FALSE);
    return FALSE;
  }

  world->rawbytes += nread;
  if (rdumpFile) {
    fwrite(world->inbuffer, nread, 1, rdumpFile);
  }

  /* Prepare data to be uncompressed */
  if (world->zstream) {
    world->zstream->next_in  = world->inbuffer;
    world->zstream->avail_in = nread;
  }

  do {
    if (world->zstream) {
      if (world->zstream->avail_in) {
        gint status;

        /* What if we have read a very long line and buffer is full?
           It does happen in some muds. */
        if (world->pbuf_size == MAX_BUFFER) {
          flush_buffer(world, 1);
        }

        world->zstream->next_out  = world->dbuffer;
        world->zstream->avail_out = MAX_BUFFER - world->pbuf_size;
        status = inflate(world->zstream, Z_SYNC_FLUSH);
        if (status == Z_OK || status == Z_STREAM_END) {
          nread = MAX_BUFFER - world->pbuf_size - world->zstream->avail_out;
          if (status == Z_STREAM_END) {
            end_mccp(world);
          }
          /* If there was an error, we're in trouble.
             Let's simply ignore it. */
        }
      } else {
        break;
      }
    } else {
      memcpy(world->dbuffer, world->inbuffer, nread);
    }

    world->bytes += nread;
    if (dumpFile) {
      fwrite(world->dbuffer, nread, 1, dumpFile);
    }

    process_telnet_iac(world, 0, nread);

    start = 0;
    nlpos = -1;
    nlptr = (unsigned char *) memchr(world->pbuffer, '\n', world->pbuf_size);
    while (nlptr) {
      nlpos = nlptr - world->pbuffer;

      convert_recv_line(world, start, nlpos + 1, FALSE);
      /* We set printed_until to zero to indicate that a full line
         has been printed and there is no more pending text. */
      world->printed_until = 0;
      start = nlpos + 1;
      nlptr = (unsigned char *) memchr(world->pbuffer + start,
                                       '\n',
                                       world->pbuf_size - start);
    }

    memmove(world->pbuffer,
            world->pbuffer + nlpos + 1,
            world->pbuf_size - nlpos - 1);
    world->pbuf_size = world->pbuf_size - nlpos - 1;
  } while (world->zstream);
  /* When data is not compressed, the end condition will be always
     false and the loop will execute only once. When data is
     compressed, the end condition will be always true, and the loop
     will run forever, until stopped by the break condition above. */

  /* Set up a timeout to print incomplete lines if no more data arrives. */
  if (world->pbuf_size && !world->timeout_id) {
    world->timeout_id = g_timeout_add_full(G_PRIORITY_LOW,
                                           150,
                                           flush_buffer_timeout_cb,
                                           world,
                                           NULL);
  }

  return TRUE;
}


gboolean
flush_buffer_timeout_cb(gpointer data)
{
  gboolean return_value;

  return_value = flush_buffer((World *) data, 0);
  if (!return_value) {
    ((World *) data)->timeout_id = 0;
  }
  return return_value;
}


static
gboolean
flush_buffer(World *world, int action)
{
  /*
   * If action != 0, flush it anyway.
   * If action = 0, only flush if last data written was more than
   *    200ms ago.
   * If action = 2, stop this callback aditionally.
   */
  gulong us_diff;

  /* Called when a command is sent, the line must be marked as new,
     even if there is nothing new. */
  if (action && world->pbuf_size) {
    convert_recv_line(world, 0, world->pbuf_size,
                      FALSE);
    world->printed_until = 0;
    world->pbuf_size = 0;
  }

  /* If action is != 0, this will always be true */
  if (world->pbuf_size == world->printed_until) {
    return (action != 2);
  }

  /* This part below only executes if action == 0 */
  g_timer_elapsed(world->last_data_written, &us_diff);
  if (us_diff < MAX_US) {
    return TRUE;
  }

  /* action = 1 is when flushing because a command is sent. We should
     consider lines as finished in this case. */
  convert_recv_line(world, world->printed_until, world->pbuf_size,
                    TRUE);
  world->printed_until = world->pbuf_size;

  return FALSE;
}


static
void
convert_recv_line(World *world, int start, int end, gboolean incomplete)
{
  gchar  *converted;
  gsize   output_len;
  GError *error = NULL;

  /* Clear pending line if it gets completed */
  if (!incomplete && world->printed_until) {
    AnsiParseOperation *operation = g_new(AnsiParseOperation, 1);
    operation->action = CLEAR_PENDING_LINE_ANSI;
    queue_ansiparse_operation(world, operation);
  }

  converted = g_convert((gchar *) (world->pbuffer + start),
                        end - start,
                        VTE_ENC, world->charset,
                        NULL, &output_len,
                        &error);
  if (converted) {
    ProcessLineOperation *operation = g_new(ProcessLineOperation, 1);
    operation->line = converted;
    operation->len = output_len;
    operation->action = incomplete ? PROCESS_INCOMPLETE_LINE
                                   : PROCESS_COMPLETE_LINE;
    queue_process_line_operation(world, operation);
  } else {
    fprintf(stderr, "Error in charset conversion: %s\n", error->message);
    g_error_free(error);
  }
}


void
start_mccp(World *world, int pos, int end)
{
  if (world->mccp_ver > 0 && !world->zstream) {
    world->zstream = g_new0(z_stream, 1);
    if (inflateInit(world->zstream) == Z_OK) {
      gchar *status_text;

      memcpy(world->inbuffer, world->dbuffer + pos + 1, end - pos - 1);
      world->zstream->next_in   = world->inbuffer;
      world->zstream->avail_in  = end - pos - 1;
      world->zstream->next_out  = world->dbuffer;
      world->zstream->avail_out = MAX_BUFFER;

      if (world->proxy_used.type == PROXY_NONE) {
        status_text = g_strdup_printf(_("Connected to host %s (%s) port %s, MCCP version %d enabled."),
                                      world->host, world->host_ip, world->port,
                                      world->mccp_ver);
      } else {
        status_text = g_strdup_printf(_("Connected to host %s port %s, via proxy %s (%s) port %s, MCCP version %d enabled."),
                                      world->host, world->port,
                                      world->proxy_used.server, world->host_ip,
                                      world->proxy_used.port,
                                      world->mccp_ver);
      }
      if (world->use_tls) {
        gchar *tmp = status_text;
        status_text = g_strconcat(tmp, _(" SSL enabled."), NULL);
        g_free(tmp);
      }
      gtk_label_set_text(world->gui->lblStatus, status_text);
      g_free(status_text);
    } else {
      /* We're in trouble, but this shouldn't happen */
      g_free(world->zstream);
      world->zstream = NULL;
    }
  }
}


void
end_mccp(World *world)
{
  gchar *status_text;

  inflateEnd(world->zstream);
  g_free(world->zstream);
  world->zstream = NULL;

  if (world->proxy_used.type == PROXY_NONE) {
    status_text = g_strdup_printf(_("Connected to host %s (%s) port %s."),
                                  world->host, world->host_ip, world->port);
  } else {
    status_text = g_strdup_printf(_("Connected to host %s port %s, via proxy %s (%s) port %s."),
                                  world->host, world->port,
                                  world->proxy_used.server, world->host_ip,
                                  world->proxy_used.port);
  }
  if (world->use_tls) {
    gchar *tmp = status_text;
    status_text = g_strconcat(tmp, _(" SSL enabled."), NULL);
    g_free(tmp);
  }
  gtk_label_set_text(world->gui->lblStatus, status_text);
  g_free(status_text);
}


void
send_to_world(World *world, const char *cmd, gint len)
{
  /* cmd should be in UTF-8 */
  gchar    *locale_cmd;
  gsize     locale_len;
  GError   *error = NULL;
  gboolean  has_incomplete_line;

  has_incomplete_line = world->printed_until;
  flush_buffer(world, 1);

  locale_cmd = g_convert(cmd, len,
                         world->charset, VTE_ENC,
                         NULL, &locale_len,
                         &error);
  if (error) {
    fprintf(stderr, "%s\n", error->message);
    g_error_free(error);
    return;
  }

  /* Flooding prevention */
  if (!world->last_command) {
    world->last_command = locale_cmd;
  } else {
    if (world->flood_prevention) {
      if (strcmp(world->last_command, locale_cmd) == 0) {
        if (++world->repeat_count == world->max_equal_commands) {
          send_to_world_no_check(world,
                                 world->flood_prevention_command,
                                 strlen(world->flood_prevention_command),
                                 NULL, 0,
                                 TRUE,
                                 has_incomplete_line);
          world->repeat_count = 1;
        }
      } else {
        world->repeat_count = 1;
      }
    }

    g_free(world->last_command);
    world->last_command = locale_cmd;
  }

  send_to_world_no_check(world,
                         cmd,
                         len,
                         locale_cmd,
                         locale_len,
                         TRUE,
                         has_incomplete_line);
}


void
send_to_world_no_check(World      *world,
                       const char *utf8_command,
                       int         utf8_len,
                       char       *locale_command,
                       gsize       locale_len,
                       gboolean    add_newline,
                       gboolean    has_incomplete_line)
{
  GError   *error = NULL;
  gboolean  needs_freeing = FALSE;

  if (!world->connected) {
    ansitextview_append_string_nl(world->gui, _("Not connected."));
    return;
  }

  if (!locale_command) {
    locale_command = g_convert(utf8_command, utf8_len,
                               world->charset, VTE_ENC,
                               NULL, &locale_len,
                               &error);
    if (error) {
      fprintf(stderr, "%s\n", error->message);
      g_error_free(error);
      return;
    }
    needs_freeing = TRUE;
  }

  write_escaped(world, locale_command, locale_len);
  if (add_newline) {
    kc_send(world, "\r\n", 2);
  }

  if (!world->itime_reset_activate) {
    world->last_command_time = time(NULL);
  }

  if (!world->noecho && world->cmd_echo) {
    ansitextview_append_string_with_fgcolor(world->gui, utf8_command,
                                            globalPrefs.idxCmdEchoColor);
    if (add_newline) {
      ansitextview_append_string(world->gui, "\n");
    }
  } else {
    /* When commands are not echoed, add a newline if there is an
       incomplete line --- most likely a prompt --- so that the
       output of the command appears in the next line. */
    if (has_incomplete_line) {
      ansitextview_append_string(world->gui, "\n");
    }
  }

  execute_hook_immediately(world, "OnSentCommand", utf8_command);

  if (needs_freeing) {
    g_free(locale_command);
  }
}


void
perform_auto_logon(World *world)
{
  gchar       *character = NULL;
  gchar       *password;
  gchar       *command = NULL;
  GtkTreePath *path;
  GtkTreeIter  iter;

  if (world->character_used == NULL || world->connection_style == NONE) {
    return;
  }

  path = gtk_tree_row_reference_get_path(world->character_used);
  if (!path) {
    gtk_tree_row_reference_free(world->character_used);
    world->character_used = NULL;
    return;
  }

  gtk_tree_model_get_iter(world->logon_characters,
                          &iter, path);
  gtk_tree_model_get(world->logon_characters, &iter,
                     LOGON_CHAR, &character,
                     LOGON_PASS, &password,
                     -1);

  switch (world->connection_style) {
  case DIKU:
    command = g_strdup_printf("%s\r\n%s\r\n", character, password);
    break;

  case LPMUD:
    command = g_strdup_printf("connect %s %s\r\n", character, password);
    break;

  case NONE:
    /* Should never reach here. */
    break;
  }

  write_escaped(world, command, strlen(command));
  g_free(command);
  g_free(character);
  g_free(password);
  gtk_tree_path_free(path);
}


static
ssize_t
write_escaped(World *world, const void *buf, size_t count)
{
  /*
   * Similar to write(), but escapes any IAC character sending
   * IAC IAC instead, as the specification tells us to do.
   */
  ssize_t  written = 0;
  size_t   size;
  void    *iac_pos;
  guchar   iac = TELNET_IAC;

  iac_pos = memchr(buf, TELNET_IAC, count);
  while (iac_pos) {
    size = iac_pos - buf;
    written += kc_send(world, buf, size + 1);
    kc_send(world, &iac, 1);
    count = count - size - 1;
    if (count == 0) {   /* Force exiting of loop */
      iac_pos = NULL;
      break;
    } else {
      buf = iac_pos + 1;
      iac_pos = memchr(buf, TELNET_IAC, count);
    }
  }
  if (count != 0) {
    written += kc_send(world, buf, count);
  }

  return written;
}


ssize_t
kc_recv(World *world, void *buf, size_t s)
{
  if (world->use_tls) {
    return gnutls_record_recv(world->tls_session, buf, s);
  } else {
    return recv(world->sock, buf, s, 0);
  }
}


ssize_t
kc_send(World *world, const void *buf, size_t s)
{
  if (world->use_tls) {
    return gnutls_record_send(world->tls_session, buf, s);
  } else {
    return send(world->sock, buf, s, 0);
  }
}
