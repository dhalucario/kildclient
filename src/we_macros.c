/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include "libintl-wrapper.h"
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

#include "kildclient.h"
#include "perlscript.h"


/***********************
 * Function prototypes *
 ***********************/
static GtkTreeModel *create_we_macros_model(World *world);
static void          we_macro_configure_view(GtkTreeView *view,
                                             GtkTreeModel *model);
static void          we_macro_add_cb(GtkButton *button, gpointer data);
static void          we_macro_edit_cb(GtkButton *button, gpointer data);
static void          we_macro_delete_cb(GtkButton *button, gpointer data);
static void          we_macro_view_row_dblclick_cb(GtkTreeView        *view,
                                                   GtkTreePath        *path,
                                                   GtkTreeViewColumn  *col,
                                                   gpointer            data);
static gboolean      edit_macro(World *world, Macro *macro);
static gboolean      we_macro_view_keypress_cb(GtkWidget   *widget,
                                               GdkEventKey *evt,
                                               gpointer     data);
static void          we_macro_move_cb(GtkButton *button, gpointer data);
/* XML UI signals */
G_MODULE_EXPORT gboolean we_macro_txtname_keypress_cb(GtkWidget   *widget,
                                                      GdkEventKey *evt,
                                                      gpointer     data);


GObject *
get_we_macros_tab(World *world)
{
  GtkBuilder       *ui_builder;
  gchar            *objects[] = { "we_panel_generic_graphical_editor", NULL };
  GError           *error = NULL;
  GObject          *panel;
  GtkLabel         *lblType;
  GObject          *btnAdd;
  GObject          *btnEdit;
  GObject          *btnDelete;
  GObject          *btnUp;
  GObject          *btnDown;
  GObject          *btnExport;
  GtkTreeSelection *selection;

  /* Create the dialog */
  ui_builder = gtk_builder_new();
  if (!gtk_builder_add_objects_from_resource(ui_builder,
                                             "/ekalin/kildclient/kildclient.ui",
                                             objects,
                                             &error)) {
    g_warning(_("Error loading UI from XML file: %s"), error->message);
    g_error_free(error);
    return NULL; /* This will cause warnings in the calling function, but
                    there is need to abort the program because of this. */
  }

  panel = gtk_builder_get_object(ui_builder,
                                 "we_panel_generic_graphical_editor");
  /* So it's not destroyed when the builder is destroyed */
  g_object_ref(panel);

  world->viewMacro = GTK_TREE_VIEW(gtk_builder_get_object(ui_builder,
                                                        "viewObjects"));
  g_object_set_data(G_OBJECT(world->viewMacro), "forworld", world);
  g_signal_connect(G_OBJECT(world->viewMacro), "row-activated",
                   G_CALLBACK(we_macro_view_row_dblclick_cb), world);
  g_signal_connect(G_OBJECT(world->viewMacro), "key-press-event",
                   G_CALLBACK(we_macro_view_keypress_cb), world);

  lblType = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblGEType"));
  gtk_label_set_markup(lblType, "<span weight=\"bold\">Macros</span>");

  btnAdd = gtk_builder_get_object(ui_builder, "btnGEAdd");
  g_signal_connect(btnAdd, "clicked",
                   G_CALLBACK(we_macro_add_cb), world->viewMacro);
  btnEdit = gtk_builder_get_object(ui_builder, "btnGEEdit");
  g_signal_connect(btnEdit, "clicked",
                   G_CALLBACK(we_macro_edit_cb), world->viewMacro);
  btnDelete = gtk_builder_get_object(ui_builder, "btnGEDelete");
  g_signal_connect(btnDelete, "clicked",
                   G_CALLBACK(we_macro_delete_cb), world->viewMacro);

  btnUp = gtk_builder_get_object(ui_builder, "btnGEUp");
  g_object_set_data(btnUp, "isup", GINT_TO_POINTER(TRUE));
  g_signal_connect(btnUp, "clicked",
                   G_CALLBACK(we_macro_move_cb), world->viewMacro);
  btnDown = gtk_builder_get_object(ui_builder, "btnGEDown");
  g_object_set_data(btnDown, "isup", GINT_TO_POINTER(FALSE));
  g_signal_connect(btnDown, "clicked",
                   G_CALLBACK(we_macro_move_cb), world->viewMacro);

  btnExport = gtk_builder_get_object(ui_builder, "btnGEExport");
  g_object_set_data(btnExport, "preselect", "Macro");

  world->macro_model = create_we_macros_model(world);
  world->macro_model_filter = GTK_TREE_MODEL_FILTER(gtk_tree_model_filter_new(world->macro_model, NULL));
  g_object_unref(G_OBJECT(world->macro_model));
  gtk_tree_model_filter_set_visible_func(GTK_TREE_MODEL_FILTER(world->macro_model_filter),
                                         we_guied_plugin_filter_func,
                                         world,
                                         NULL);

  we_macro_configure_view(world->viewMacro,
                          GTK_TREE_MODEL(world->macro_model_filter));
  selection = gtk_tree_view_get_selection(world->viewMacro);
  gtk_tree_selection_set_mode(selection, GTK_SELECTION_MULTIPLE);
  g_object_set_data(G_OBJECT(selection), "btnEdit",   btnEdit);
  g_object_set_data(G_OBJECT(selection), "btnDelete", btnDelete);
  g_object_set_data(G_OBJECT(selection), "btnUp",     btnUp);
  g_object_set_data(G_OBJECT(selection), "btnDown",   btnDown);
  g_object_set_data(G_OBJECT(selection), "btnExport", btnExport);
  g_signal_connect(G_OBJECT(selection), "changed",
                   G_CALLBACK(we_guied_selection_changed_cb), NULL);

  gtk_builder_connect_signals(ui_builder, world);
  g_object_unref(ui_builder);
  return panel;
}


static
GtkTreeModel *
create_we_macros_model(World *world)
{
  GtkListStore *store;
  GtkTreeIter   iter;
  GSList       *macroptr;
  Macro        *macro;

  store = gtk_list_store_new(WE_GUIED_N_COLS,
                             G_TYPE_POINTER);

  macroptr = world->macros;
  while (macroptr) {
    macro = (Macro *) macroptr->data;
    gtk_list_store_append(store, &iter);
    gtk_list_store_set(store, &iter,
                       WE_GUIED_POINTER, macro,
                       -1);

    macroptr = macroptr->next;
  }

  return GTK_TREE_MODEL(store);
}


static
void
we_macro_configure_view(GtkTreeView *view, GtkTreeModel *model)
{
  GtkCellRenderer   *renderer;
  GtkTreeViewColumn *column;

  gtk_tree_view_set_model(view, model);
  g_object_unref(model);

  /* Column 1 - Enabled? */
  renderer = gtk_cell_renderer_toggle_new();
  g_object_set(G_OBJECT(renderer), "activatable", TRUE, NULL);
  g_object_set_data(G_OBJECT(renderer), "column",
                    GINT_TO_POINTER(MACRO_ENABLED));
  g_signal_connect(G_OBJECT(renderer), "toggled",
                   G_CALLBACK(we_guied_bool_col_toggled_cb), view);
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Enabled"));
  gtk_tree_view_column_pack_start(column, renderer, FALSE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_bool_func,
                                          GINT_TO_POINTER(MACRO_ENABLED),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 2 - Name */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Name"));
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_text_func,
                                          GINT_TO_POINTER(MACRO_NAME),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 3 - Key */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Key"));
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_text_func,
                                          GINT_TO_POINTER(MACRO_KEY),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 4 - Action */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Action"));
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_text_func,
                                          GINT_TO_POINTER(MACRO_ACTION),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 5 - Plugin */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Plugin"));
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_text_func,
                                          GINT_TO_POINTER(OBJECT_PLUGIN),
                                          NULL);
  gtk_tree_view_append_column(view, column);


  gtk_tree_view_columns_autosize(view);
}


static
void
we_macro_add_cb(GtkButton *button, gpointer data)
{
  World       *world;
  GtkTreeView *view;
  Macro       *macro;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");

  macro = g_new0(Macro, 1);
  macro->enabled = 1;

  if (edit_macro(world, macro)) {
    world->macros = g_slist_insert(world->macros,
                                   macro,
                                   world->macro_pos);
    we_macro_insert_macro(world, macro, world->macro_pos++);
  } else {
    g_free(macro);
  }
}


static
void
we_macro_edit_cb(GtkButton *button, gpointer data)
{
  /* Note that the Edit button is only active when exactly one row
     is selected. */
  World            *world;
  GtkTreeView      *view;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  GList            *selected;
  GtkTreeIter       iter;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");
  selection = gtk_tree_view_get_selection(view);
  selected = gtk_tree_selection_get_selected_rows(selection, &model);
  if (selected) {
    Macro *macro;

    gtk_tree_model_get_iter(model, &iter, selected->data);
    gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &macro, -1);

    if (edit_macro(world, macro)) {
      we_macro_update_macro(world, macro);
    }

    g_list_foreach(selected, (GFunc) gtk_tree_path_free, NULL);
    g_list_free(selected);
  }
}


static
void
we_macro_delete_cb(GtkButton *button, gpointer data)
{
  World            *world;
  GtkTreeView      *view;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  GList            *selected;
  GtkTreeIter       iter;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");
  selection = gtk_tree_view_get_selection(view);
  selected = gtk_tree_selection_get_selected_rows(selection, &model);
  if (selected) {
    Macro     *macro;
    GSList    *macroitem;
    GtkWidget *msgdlg;
    gint       n;

    n = gtk_tree_selection_count_selected_rows(selection);
    msgdlg = we_guied_confirm_delete_dialog_new(GTK_WINDOW(world->dlgEditWorld),
                                                ngettext("macro", "macros", n),
                                                n);

    if (!world->confirm_delete
        || gtk_dialog_run(GTK_DIALOG(msgdlg)) == GTK_RESPONSE_YES) {
      GList *listiter = g_list_last(selected);

      while (listiter) {
        gtk_tree_model_get_iter(model, &iter, listiter->data);
        gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &macro, -1);

        macroitem = g_slist_find(world->macros, macro);
        remove_macro(world, macroitem);

        listiter = listiter->prev;
      }
    }
    gtk_widget_destroy(msgdlg);

    g_list_foreach(selected, (GFunc) gtk_tree_path_free, NULL);
    g_list_free(selected);
  }
}


static
void
we_macro_view_row_dblclick_cb(GtkTreeView        *view,
                              GtkTreePath        *path,
                              GtkTreeViewColumn  *col,
                              gpointer            data)
{
  World        *world;
  GtkTreeModel *model;
  GtkTreeIter   iter;

  world = (World *) data;
  model = gtk_tree_view_get_model(view);

  if (gtk_tree_model_get_iter(model, &iter, path)) {
    Macro *macro;

    gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &macro, -1);

    if (edit_macro(world, macro)) {
      we_macro_update_macro(world, macro);
    }
  }
}


static
gboolean
edit_macro(World *world, Macro *macro)
{
  GtkBuilder      *ui_builder;
  gchar           *objects[] = { "dlgEditMacro", NULL };
  GError          *error = NULL;
  GtkWidget       *dlgEditMacro;
  GtkEntry        *txtName;
  GtkEntry        *txtKey;
  GtkEntry        *txtAction;
  GtkToggleButton *chkEnabled;
  const gchar     *newname;
  const gchar     *newkey;
  guint            newkeyval;
  GdkModifierType  newmodifiers;
  const gchar     *newaction;

  ui_builder = gtk_builder_new();
  if (!gtk_builder_add_objects_from_resource(ui_builder,
                                             "/ekalin/kildclient/kildclient.ui",
                                             objects,
                                             &error)) {
    g_warning(_("Error loading UI from XML file: %s"), error->message);
    g_error_free(error);
    return FALSE;
  }
  gtk_builder_connect_signals(ui_builder, NULL);

  dlgEditMacro = GTK_WIDGET(gtk_builder_get_object(ui_builder, "dlgEditMacro"));
  gtk_dialog_set_default_response(GTK_DIALOG(dlgEditMacro),
                                  GTK_RESPONSE_OK);
  gtk_window_set_transient_for(GTK_WINDOW(dlgEditMacro),
                               GTK_WINDOW(world->dlgEditWorld));

  txtName      = GTK_ENTRY(gtk_builder_get_object(ui_builder,
                                                  "txtMacroName"));
  txtKey       = GTK_ENTRY(gtk_builder_get_object(ui_builder,
                                                  "txtMacroKey"));
  txtAction    = GTK_ENTRY(gtk_builder_get_object(ui_builder,
                                                  "txtMacroAction"));
  chkEnabled   = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder,
                                                          "chkMacroEnabled"));

  /* Only display note when editing a plugin's macro */
  if (!macro->owner_plugin) {
    GObject *label;

    label = gtk_builder_get_object(ui_builder, "lblNoteMacro");
    gtk_widget_destroy(GTK_WIDGET(label));
    label = gtk_builder_get_object(ui_builder, "lblNoteTextMacro");
    gtk_widget_destroy(GTK_WIDGET(label));
  }

  /* We don't need it anymore */
  g_object_unref(ui_builder);

  /* Fill-in values */
  if (macro->name) {
    gtk_entry_set_text(txtName, macro->name);
  }
  if (macro->keyval) {
    gchar *keycodestr = gtk_accelerator_name(macro->keyval, macro->modifiers);
    gtk_entry_set_text(txtKey, keycodestr);
    g_free(keycodestr);
  }
  if (macro->action) {
    gtk_entry_set_text(txtAction, macro->action);
  }
  gtk_toggle_button_set_active(chkEnabled,  macro->enabled);

  /* Run the dialog until the input is valid or cancelled */
  gtk_widget_show_all(dlgEditMacro);
  while (1) {
    if (gtk_dialog_run(GTK_DIALOG(dlgEditMacro)) == GTK_RESPONSE_OK) {
      /* Validate */
      newkey = gtk_entry_get_text(txtKey);
      if (strcmp(newkey, "") == 0) {
        kc_error_dialog(GTK_WINDOW(dlgEditMacro),
                        _("You must specify the key."));
        continue;
      }
      gtk_accelerator_parse(newkey, &newkeyval, &newmodifiers);
      /* Shouldn't really happen if key codes are gotten from key press evt. */
      if (!newkeyval) {
        kc_error_dialog(GTK_WINDOW(dlgEditMacro),
                        _("Invalid key code."));
        continue;
      }

      newaction = gtk_entry_get_text(txtAction);
      if (strcmp(newaction, "") == 0) {
        kc_error_dialog(GTK_WINDOW(dlgEditMacro),
                        _("You must specify the action."));
        continue;
      }

      /* Update values */
      newname = gtk_entry_get_text(txtName);
      if (!macro->name || strcmp(newname, macro->name) != 0) {
        g_free(macro->name);
        if (strcmp(newname, "") != 0) {
          macro->name = g_strdup(newname);
        } else {
          macro->name = NULL;
        }
      }

      if (!macro->action || strcmp(newaction, macro->action) != 0) {
        g_free(macro->action);
        macro->action = g_strdup(newaction);
      }

      macro->keyval    = newkeyval;
      macro->modifiers = newmodifiers;
      macro->enabled   = gtk_toggle_button_get_active(chkEnabled);

      /* We've finished successfully */
      gtk_widget_destroy(dlgEditMacro);
      return TRUE;
    } else {
      /* Cancel pressed */
      gtk_widget_destroy(dlgEditMacro);
      return FALSE;
    }
  }
}


static
gboolean
we_macro_view_keypress_cb(GtkWidget   *widget,
                          GdkEventKey *evt,
                          gpointer     data)
{
  if (evt->keyval == GDK_KEY_Delete || evt->keyval == GDK_KEY_KP_Delete) {
    we_macro_delete_cb(NULL, widget);
    return TRUE;
  }

  return FALSE;
}


static
void
we_macro_move_cb(GtkButton *button, gpointer data)
{
  GtkTreeView      *view;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  GList            *selected;
  GtkTreeIter       iter;
  World            *world;
  gboolean          is_up;
  gint              new_pos;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");
  selection = gtk_tree_view_get_selection(view);
  is_up = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(button), "isup"));
  selected = gtk_tree_selection_get_selected_rows(selection, &model);
  if (selected) {
    Macro *macro;
    gint   pos;

    gtk_tree_model_get_iter(model, &iter, selected->data);
    gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &macro, -1);
    pos = g_slist_index(world->macros, macro);
    new_pos = pos;
    if (is_up) {
      if (pos != 0) {
        new_pos = pos - 1;
        move_macro(world, pos, new_pos);
      }
    } else {
      if (pos != world->macro_pos - 1) {
        new_pos = pos + 1;
        move_macro(world, pos, new_pos);
      }
    }
    /* Reselect the moved item */
    gtk_tree_model_iter_nth_child(model, &iter, NULL, new_pos);
    gtk_tree_selection_select_iter(selection, &iter);

    g_list_foreach(selected, (GFunc) gtk_tree_path_free, NULL);
    g_list_free(selected);
  }
}


void
we_macro_update_macro(World *world, Macro *macro_arg)
{
  /* Called by the Perl functions when an macro is changed, so that
     the display is updated. */
  GtkTreeIter  iter;
  GtkTreePath *path;
  Macro     *macro;
  gboolean     success;

  if (!world->macro_model) {
    return;
  }

  success = gtk_tree_model_get_iter_first(world->macro_model, &iter);
  while (success) {
    gtk_tree_model_get(world->macro_model, &iter,
                       WE_GUIED_POINTER, &macro, -1);
    if (macro == macro_arg) {
      path = gtk_tree_model_get_path(world->macro_model, &iter);
      gtk_tree_model_row_changed(world->macro_model, path, &iter);
      gtk_tree_path_free(path);
      return;
    }
    success = gtk_tree_model_iter_next(world->macro_model, &iter);
  }
}


void
we_macro_insert_macro(World *world, Macro *macro, gint pos)
{
  /* Called by the Perl functions when an macro is inserted, so that
     the display is updated. */
  GtkTreeIter iter;

  if (!world->macro_model) {
    return;
  }

  gtk_list_store_insert(GTK_LIST_STORE(world->macro_model), &iter, pos);
  gtk_list_store_set(GTK_LIST_STORE(world->macro_model), &iter,
                     WE_GUIED_POINTER, macro,
                     -1);
}


void
we_macro_delete_macro(World *world, Macro *macro_arg)
{
  /* Called by the Perl functions when an macro is deleted, so that
     the display is updated. */
  GtkTreeIter  iter;
  Macro       *macro;
  gboolean     success;

  if (!world->macro_model) {
    return;
  }

  success = gtk_tree_model_get_iter_first(world->macro_model, &iter);
  while (success) {
    gtk_tree_model_get(world->macro_model, &iter,
                       WE_GUIED_POINTER, &macro, -1);
    if (macro == macro_arg) {
      gtk_list_store_remove(GTK_LIST_STORE(world->macro_model), &iter);
      return;
    }
    success = gtk_tree_model_iter_next(world->macro_model, &iter);
  }
}


gboolean
we_macro_txtname_keypress_cb(GtkWidget   *widget,
                             GdkEventKey *evt,
                             gpointer     data)
{
  gchar *keycode;

  if (gtk_accelerator_valid(evt->keyval, evt->state)) {
    keycode = gtk_accelerator_name(evt->keyval, evt->state);
    gtk_entry_set_text(GTK_ENTRY(widget), keycode);
    g_free(keycode);
  }

  return TRUE;
}
