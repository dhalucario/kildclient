/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */


#ifndef __KILDCLIENT_H
#define __KILDCLIENT_H

#include "kcircularqueue.h"
#include "simocombobox.h"

#include <gmodule.h>
#include <EXTERN.h>
#include <perl.h>
#include <XSUB.h>

#include <zlib.h>

#include <gnutls/gnutls.h>


/**********************
 * Limits and defines *
 **********************/
#define MAX_BUFFER   4096
#define MAX_TIME_STR 150
#define VTE_ENC      "UTF-8"
#define DEFAULT_TERMINAL_FONT "Monospace 10"
#define DEFAULT_LOGTIME_FORMAT "%c> "
#define CLIENT_NAME "KildClient"
#define CLIENT_NAME_LEN 10


/**************
 * Data types *
 **************/
enum ConnStyle_s {
  NONE,
  DIKU,
  LPMUD,
};
typedef enum ConnStyle_s ConnStyle;

enum TimeType_s {
  HMS,
  SEC,
  NO,
};
typedef enum TimeType_s TimeType;

/* States for the Telnet IAC processing State Machine */
enum TelnetState_e {
  TELNETFSM_DATA,
  TELNETFSM_DATA_NL,
  TELNETFSM_DATA_CR,
  TELNETFSM_IAC,
  TELNETFSM_DO,
  TELNETFSM_DONT,
  TELNETFSM_WILL,
  TELNETFSM_WONT,
  TELNETFSM_SB,
  TELNETFSM_SB_IAC,
  TELNETFSM_SB_IAC_TERMTYPE,
  TELNETFSM_SB_IAC_COMPRESS,
  TELNETFSM_SB_IAC_COMPRESS2,
  TELNETFSM_SB_IAC_COMPRESS_WILL, /* Also serves for SB_IAC_COMPRESS2_IAC */
  TELNETFSM_SERVER_DATA,
  TELNETFSM_SERVER_DATA_IAC,
};
typedef enum TelnetState_e TelnetState;


enum proxy_type_e {
  PROXY_USE_GLOBAL = -1,
  PROXY_NONE,
  PROXY_SOCKS4,
  PROXY_SOCKS5,
  PROXY_HTTP,
};
typedef enum proxy_type_e proxy_type;

enum proxy_socks5_state_e {
  SOCKS5_STATE_INITIAL,
  SOCKS5_STATE_AUTH,
  SOCKS5_STATE_CONNECTION,
};
typedef enum proxy_socks5_state_e proxy_socks5_state;

struct proxy_settings_s
{
  proxy_type          type;
  char               *server;
  char               *port;
  char               *user;
  char               *password;
  proxy_socks5_state  state;
};
typedef struct proxy_settings_s proxy_settings;


enum NameDisplayStyle_e {
  NAME_DISPLAY_WORLD,
  NAME_DISPLAY_WORLD_CHAR,
  NAME_DISPLAY_CHAR_WORLD,
  NAME_DISPLAY_CUSTOM,
};
typedef enum NameDisplayStyle_e NameDisplayStyle;


struct plugin_s
{
  char     *name;
  char     *version;
  char     *description;
  char     *author;
  gboolean  enabled;
};
typedef struct plugin_s Plugin;


struct HookList_s
{
  GSList             *OnConnect;
  gint                OnConnect_pos;
  GtkTreeModel       *OnConnect_model;
  GtkTreeModelFilter *OnConnect_model_filter;
  GSList             *OnDisconnect;
  gint                OnDisconnect_pos;
  GtkTreeModel       *OnDisconnect_model;
  GtkTreeModelFilter *OnDisconnect_model_filter;
  GSList             *OnReceivedText;
  gint                OnReceivedText_pos;
  GtkTreeModel       *OnReceivedText_model;
  GtkTreeModelFilter *OnReceivedText_model_filter;
  GSList             *OnSentCommand;
  gint                OnSentCommand_pos;
  GtkTreeModel       *OnSentCommand_model;
  GtkTreeModelFilter *OnSentCommand_model_filter;
  GSList             *OnGetFocus;
  gint                OnGetFocus_pos;
  GtkTreeModel       *OnGetFocus_model;
  GtkTreeModelFilter *OnGetFocus_model_filter;
  GSList             *OnLoseFocus;
  gint                OnLoseFocus_pos;
  GtkTreeModel       *OnLoseFocus_model;
  GtkTreeModelFilter *OnLoseFocus_model_filter;
  GSList             *OnCloseConnected;
  gint                OnCloseConnected_pos;
  GtkTreeModel       *OnCloseConnected_model;
  GtkTreeModelFilter *OnCloseConnected_model_filter;
  GSList             *OnServerData;
  gint                OnServerData_pos;
  GtkTreeModel       *OnServerData_model;
  GtkTreeModelFilter *OnServerData_model_filter;
};
typedef struct HookList_s HookList;


struct Hook_s
{
  char     *name;
  Plugin   *owner_plugin;
  gboolean  enabled;
  gchar    *action;
};
typedef struct Hook_s Hook;


/* For ANSI parsing - FSM */
enum ANSIFSMState_e {
  ANSIFSM_DATA,
  ANSIFSM_ESC,
  ANSIFSM_ANSISEQ,
  ANSIFSM_ESCLEFTPAR,
};
typedef enum ANSIFSMState_e ANSIFSMState;


struct TAState_s
{
  guint           fg_color;
  guint           fg_base_idx;
  guint           bg_color;
  guint           bg_base_idx;
  PangoUnderline  underline;
  gboolean        strike;
  gboolean        italics;
  gboolean        reverse;
  gboolean        hidden;
  /* Pointers to the tags used for foreground and background colors.
     It they are set, the tag is used, otherwise the color is determined
     from the ANSI state. */
  GtkTextTag     *foreground_color_tag;
  GtkTextTag     *background_color_tag;
};
typedef struct TAState_s TAState;


enum MCCPBehavior_s {
  MCCP_AFTER_CONNECT,
  MCCP_ALWAYS,
  MCCP_DISABLE,
};
typedef enum MCCPBehavior_s MCCPBehavior;


struct TextAppearance_s
{
  /* The tags for ansi colors are stored here.
     Each array contains 20 elements:
       0-7  Basic colors, from black to white
       8    Default color
       9-16 Bold basic colors
       17   Default bold color
       18   Default color for reverse video
       19   Bold default color for reverse video

     The last two entries are used for reverse video. For the foreground
     array, they contain tags that set the foreground to the default
     background and bold background colors, respectively. For the background
     array, they contain tags that set the background to the default
     foreground and bold foreground colors, respectively. */
  GtkTextTag **ansi_fore_tags;
  GtkTextTag **ansi_back_tags;

  /* Tags for other foreground/background colors, specified as RGB values.
     The hash is indexed by ints (converted to pointers with Glib macros).
     Each color component is one byte (256 values, thus), and the index
     it is formed with ((R << 16) | (G << 8) || B).
     Colors are allocated as needed. */
  GHashTable *rgb_fore_tags;
  GHashTable *rgb_back_tags;

  /* Tags for text attributes */
  GtkTextTag *underline_tag;
  GtkTextTag *dblunderline_tag;
  GtkTextTag *strike_tag;
  GtkTextTag *italics_tag;

  /* Character groups supported. -1: unknown. 0: not supported. 1: supported
     If a group is not supported, replacement ASCII characters (not
     necessarily good) are used when in vt100 line drawing mode. */
  gint sup_geom_shapes;
  gint sup_block;
  gint sup_control;
  gint sup_l1_supplement;
  gint sup_box_drawing;
  gint sup_misc_tech;
  gint sup_math;
  gint sup_greek;
};
typedef struct TextAppearance_s TextAppearance;


struct AnsiParseState_s
{
  /* For ANSI parsing */
  ANSIFSMState ansifsm_state;
  gboolean     linedraw;
  guchar       ansiseq_buffer[MAX_BUFFER]; /* Holds incomplete ansi seqs. */
  gint         ansiseq_pos;
};
typedef struct AnsiParseState_s AnsiParseState;


struct WorldGUI_s
{
  struct world_s    *world;

  GtkCssProvider    *css_provider;

  GtkWidget         *vbox;
  GtkPaned          *split_pane;
  gint               last_split_position;
  GtkScrolledWindow *scrolled_win;
  GtkTextView       *txtView;
  GtkScrolledWindow *scrwinScroll;
  GtkTextView       *txtViewScroll;
  GtkTextBuffer     *txtBuffer;
  gdouble            last_scroll_info; /* For scrolled_win_size_allocate_cb() */

  GtkBox            *hboxTab;
  GtkLabel          *lblNotebook;

  GtkWidget         *search_box;
  GtkWidget         *txtSearchTerm;
  GtkWidget         *btnFindNext;
  GtkLabel          *lblSearchInfo;
  GtkTextMark       *txtmark_search_start;
  GtkTextMark       *txtmark_next_search_start;

  GtkWidget         *commandArea;
  GtkWidget         *btnClear;
  union {
    SimoComboBox    *cmbEntry;        /* For MUD windows */
    GtkEntry        *txtEntry;        /* For KCWin's */
  };

  GtkBox            *statusbar_box;
  GtkLabel          *lblStatus;
  GtkLabel          *lblLines;
  GtkLabel          *lblTime;

  GtkTextMark       *txtmark_end;
  GtkTextMark       *txtmark_linestart;
  GtkTextTag        *txttag_url;

  gboolean           execute_changed_signal;

  guint              prune_timeout_id;

  /* Line times for tooltips */
  KCircularQueue    *line_times;

  /* Determines the appearance of the text */
  TextAppearance     ta;
  AnsiParseState     ansi_parse;

  /* The current color and attributes is determined from these variables */
  TAState ta_state;
  /* A place to store the state at the end of the last complete line,
     so that it can be restored when an incomplete line is erased. */
  TAState saved_ta_state;
};
typedef struct WorldGUI_s WorldGUI;


struct world_s
{
  /* Basic information */
  gchar           *name;              /* World name */
  gchar           *display_name;      /* Name displayed in tab & window title */
  gchar           *new_text_name;     /* Red version of the above */
  gchar           *file;
  gchar           *charset;
  gint             charset_index;
  gchar           *host;
  gchar           *host_ip;
  gchar           *port;
  proxy_settings   proxy;
  proxy_settings   proxy_used;
  gboolean         keep_alive;

  /* Auto-login data */
  ConnStyle            connection_style;
  GtkTreeModel        *logon_characters;
  GtkTreeRowReference *character_used;

  /* Display settings */
  char             *terminalfont;
  char             *entryfont;
  char             *statusfont;
  gboolean          usebold;
  gboolean          wrap;
  gint              wrap_indent;
  gboolean          use_wrap_column;
  gint              wrap_column;
  GdkRGBA          *deffore;
  GdkRGBA          *defbold;
  GdkRGBA          *defback;
  GdkRGBA          *defboldback;
  GdkRGBA          *ansicolors;
  NameDisplayStyle  name_display_style;
  char             *name_display_pattern;
  gboolean          use_tooltips;

  /* Scrolling settings */
  gboolean         scrollOutput;
  glong            buffer_lines;

  /* Scripting support */
  gchar           *scriptfile;
  GSList          *triggers;
  gint             trigger_pos;
  gboolean         disable_triggers;
  SV              *trigger_action_re; /* Pattern to replace $n in action */
  GSList          *aliases;
  gint             alias_pos;
  gboolean         disable_aliases;
  GSList          *macros;
  gint             macro_pos;
  gboolean         disable_macros;
  GSList          *timers;
  gint             timer_pos;
  gboolean         disable_timers;
  GSList          *permanent_variables;
  GSList          *plugins;
  GSList          *plugins_being_defined;
  GSList          *startup_plugin_files;
  GtkTreeModel    *startup_plugins;     /* Plugins loaded at startup */

  HookList         hooks;

  /* Input settings */
  gboolean         repeat_commands;
  gboolean         cmd_echo;
  gboolean         never_hide_input;
  gboolean         store_commands;
  gint             commands_to_save;
  gint             input_n_lines;
  gint             input_n_lines_saved;
  gboolean         spell;
  gchar           *spell_language;
  gboolean         autocompletion;
  gint             autocompl_minprefix;
  gchar            command_separator[10];
  gchar            perl_character;
  gboolean         updating_controls;     /* To prevent infinite loop */
  gboolean         ignore_up_down_keys;

  /* Command history support */
  GtkTreeModel    *cmd_list;
  GtkTreeIter      current_cmd;
  gint             cmd_list_size;
  gboolean         cmd_has_been_edited;
  gboolean         cmd_position_changed;
  gboolean         combo_is_poped;
  gboolean         cmd_just_selected_from_combo;
  gchar           *saved_command_find_search;

  /* Flood prevention */
  gboolean         flood_prevention;
  gint             max_equal_commands;
  gchar           *flood_prevention_command;
  gchar           *last_command;
  int              repeat_count;

  /* Protocols configuration */
  MCCPBehavior    mccp_behavior;

  /* Logging */
  gchar           *log_file_name;
  gchar           *log_actual_file;
  FILE            *log_file;
  gchar           *log_timeformat;
  gboolean         log_add_time;
  gboolean         log_autostart;

  /* Basic connection information */
  gboolean         connected;
  gboolean         connecting;
  gboolean         connected_at_least_once;
  gchar           *errmsg;        /* Error message to display */
  GIOChannel      *iochan;
  int              sock;
  guint            io_watch_id;
  guint            timeout_id;
  glong            deleted_lines;
  glong            rawbytes;      /* Bytes received from network */
  glong            bytes;         /* Bytes seen after decompression */
  struct addrinfo *addrinfo;      /* addrinfo for real connection */
  guint32          real_ip_for_socks4; /* IP of actual host, for SOCKS4 */

  /* Buffers and state */
  TelnetState      telnet_state;             /* State of Telnet IAC FSM */
  gint             telnet_state_ncrs;        /* Last # of \r seen in a row */
  guchar           inbuffer[MAX_BUFFER];     /* Input buffer from network */
  guchar           dbuffer[MAX_BUFFER];      /* Buffer for decompressed data */
  guchar          *pbuffer;                  /* Buffer to process */
  guint            pbuf_alloc_size;          /* Current size of pbuffer */
  guint            pbuf_size;                /* Buffer size */
  /* For MCCP handling */
  z_stream        *zstream;                  /* Used for decompression */
  gint             mccp_ver;                 /* Version of MCCP supported */
  /* Server data */
  guchar           server_data_protocol;     /* Protocol */
  guchar          *server_data_buffer;       /* Buffer */
  guint            server_data_alloc_size;   /* Allocated size */
  guint            server_data_size;         /* Current size */
  gboolean         gmcp_enabled;             /* Is GMCP enabled? */
  gboolean         msdp_enabled;             /* Is MSDP enabled? */

  /* Time counts */
  guint            printed_until;
  GTimer          *last_data_written;

  /* For connection and idle time display */
  TimeType         ctime_type;
  TimeType         itime_type;
  gboolean         itime_reset_activate;     /* When to reset idle timer */
  time_t           connected_time;           /* Time of connection */
  time_t           last_command_time;        /* Time of last sent command */

  /* For the getkeycode() function */
  gboolean         print_next_keycode;
  gchar           *keycode_to_print;

  /* Miscellaneous data */
  gboolean         noecho;
  gboolean         use_naws;
  GdkRectangle     last_naws_size;
  gboolean         has_focus;
  gboolean         is_current_tab;
  gboolean         has_unread_text;
  gint             selection_start;
  gint             selection_end;

  /* GUI structure */
  WorldGUI        *gui;

  /* UI Builder for the dialogs */
  GtkBuilder      *ui_builder;

  /* World Editor dialog and related objects */
  GtkWidget          *dlgEditWorld;
  GtkTreeModel       *plugin_model;
  GtkTreeView        *viewTrigger;
  GtkTreeModel       *trigger_model;
  GtkTreeModelFilter *trigger_model_filter;
  GtkTreeView        *viewAlias;
  GtkTreeModel       *alias_model;
  GtkTreeModelFilter *alias_model_filter;
  GtkTreeView        *viewMacro;
  GtkTreeModel       *macro_model;
  GtkTreeModelFilter *macro_model_filter;
  GtkTreeView        *viewTimer;
  GtkTreeModel       *timer_model;
  GtkTreeModelFilter *timer_model_filter;
  GtkTreeView        *viewVar;
  GtkTreeModel       *permvars_model;
  GSList             *selected_OnConnect_hooks;
  GSList             *selected_OnDisconnect_hooks;
  GSList             *selected_OnReceivedText_hooks;
  GSList             *selected_OnSentCommand_hooks;
  GSList             *selected_OnGetFocus_hooks;
  GSList             *selected_OnLoseFocus_hooks;
  GSList             *selected_OnCloseConnected_hooks;
  GSList             *selected_OnServerData_hooks;
  gboolean            confirm_delete;
  gboolean            show_plugin_items;

  /* Multi-line send dialog */
  GtkWidget *dlgMLSend;

  /* Command history dialog */
  GtkWidget           *dlgCmdHistory;
  gboolean             cmdfind_upwards;
  gchar               *cmdfind_string;
  GtkTreeRowReference *cmdfind_row;
  GtkWidget           *dlgCmdHistoryFind;

  /* Test Triggers dialog */
  GtkWidget *dlgTestTriggers;

  /* Perl interpreter */
  PerlInterpreter *perl_interpreter;

  /* TLS/SSL support */
  gboolean         use_tls;
  gnutls_session_t tls_session;
  int              tls_handshake_dir;

  /* Inter-thread communication */
  GRecMutex    perl_mutex;
  GThread     *process_line_thread;
  GAsyncQueue *process_line_operations;

  GThread     *ansiparse_thread;
  GAsyncQueue *ansiparse_operations;

  GMutex       write_log_line_mutex;

  GAsyncQueue *mainthread_operations;
  guint        mainthread_operations_idle_id;

  GMutex       wait_command_mutex;
  GCond        wait_command_cond;
  gboolean     command_executed;

  gboolean     to_be_destructed;
};
typedef struct world_s World;


enum {
  RESP_RECONNECT = 10,
  RESP_OTHER,
  RESP_OFFLINE,
};


/* For recent command list */
enum {
  CMDLIST_COL_COMMAND,
  CMDLIST_COL_TENTATIVE,
  N_CMDLIST_COLUMNS,
};


/* Columns of model for auto-logon characters */
enum {
  LOGON_CHAR,
  LOGON_PASS,
  N_LOGON_COLUMNS,
};


/* Columns of model for plugins loaded on startup */
enum {
  SPLUGIN_FILE,
  SPLUGIN_NAME,
  SPLUGIN_AUTHOR,
  SPLUGIN_DESCRIPTION,
  N_SPLUGIN_COLUMNS,
};


struct prefs_s
{
  int              idxInfoMsgColor;
  int              idxCmdEchoColor;
  gdouble          multi_cmd_delay;
  gint             multi_cmd_group_size;
  GtkPositionType  tab_position;
  gboolean         hide_single_tab;
  gboolean         urgency_hint;
  char            *audio_player_command;
  char            *last_open_world;
  gboolean         no_plugin_help_msg;
  proxy_settings   proxy;
};
typedef struct prefs_s Prefs;


struct general_object_s
{
  char     *name;
  Plugin   *owner_plugin;
  gboolean  enabled;
};
typedef struct general_object_s GeneralObject;


struct trigger_s
{
  char     *name;
  Plugin   *owner_plugin;
  gboolean  enabled;
  char     *pattern;
  SV       *pattern_re;
  char     *action;
  gboolean  ignore_case;
  gboolean  gag_output;
  gboolean  gag_log;
  gboolean  keepexecuting;
  gboolean  rewriter;

  gboolean  highlight;
  /* Style to apply / -1 means no change*/
  gint      high_target;
  gint      high_fg_color;
  gint      high_bg_color;
  gint      high_italic;
  gint      high_strike;
  gint      high_underline;
};
typedef struct trigger_s Trigger;

struct alias_s
{
  char     *name;
  Plugin   *owner_plugin;
  gboolean  enabled;
  char     *pattern;
  SV       *pattern_re;
  char     *substitution;
  gboolean  ignore_case;
  gboolean  perl_eval;
};
typedef struct alias_s Alias;

struct macro_s
{
  char            *name;
  Plugin          *owner_plugin;
  gboolean         enabled;
  guint            keyval;
  GdkModifierType  modifiers;
  char            *action;
};
typedef struct macro_s Macro;

struct timer_s
{
  char     *name;
  Plugin   *owner_plugin;
  gboolean  enabled;
  World    *for_world;
  gdouble   interval;
  gint      count;
  char     *action;
  gboolean  temporary;
  guint     function_id;
};
typedef struct timer_s Timer;


/* For multi-line send */
enum MLSendState_e {
  ML_TRY_INITIALBUFFER,
  ML_INITIALBUFFER,
  ML_TRY_INITIALLINES,
  ML_INITIALLINES,
  ML_TRY_FILE,
  ML_FILECONTENTS,
  ML_TRY_FINALBUFFER,
  ML_FINALBUFFER,
  ML_TRY_FINALLINES,
  ML_FINALLINES,
  ML_FINISH,
};
typedef enum MLSendState_e MLSendState;

struct MLSendData_s
{
  World         *world;
  MLSendState    state;
  GtkTextBuffer *textInitial;
  GtkTextBuffer *textFinal;
  GSList        *linesInitial;
  GSList        *linesFinal;
  gchar         *file;
  FILE          *fp;
  gchar         *textStart;
  gchar         *textEnd;
  int            nlines;
  int            linesatime;
  int            currline;
  GSList        *list_iterator;
  gboolean       dont_close;
};
typedef struct MLSendData_s MLSendData;


struct HighlightApplication_s
{
  Trigger *trigger;
  gint     match_start;
  gint     match_end;
};
typedef struct HighlightApplication_s HighlightApplication;


struct HookExecution_s
{
  World *world;
  gchar *data;
};
typedef struct HookExecution_s HookExecution;


/* Line processing operations */
enum ProcessLineOperationType_s {
  PROCESS_COMPLETE_LINE,
  PROCESS_INCOMPLETE_LINE,
  DUMMY_PROCESS_LINE,
};
typedef enum ProcessLineOperationType_s ProcessLineOperationType;

struct ProcessLineOperation_s
{
  ProcessLineOperationType  action;
  gchar                    *line;
  gsize                     len;
};
typedef struct ProcessLineOperation_s ProcessLineOperation;


/* Ansi parsing operation */
enum AnsiParseOperationType_s {
  CLEAR_PENDING_LINE_ANSI,
  APPEND_LINE,
  APPEND_ECHOED_LINE,
  APPLY_HIGHLIGHTS_AND_MOVE_MARK,
  FINALIZE_LINE,
  DUMMY_ANSIPARSE,
};
typedef enum AnsiParseOperationType_s AnsiParseOperationType;

struct AnsiParseOperation_s
{
  AnsiParseOperationType action;

  union
  {
    struct
    {
      gchar *line;
      gsize  line_len;
    } line;

    GSList *highlights;
  };
};
typedef struct AnsiParseOperation_s AnsiParseOperation;


/* Main thread operations */
enum MainThreadOperationType_s {
  INSERT_AT_END,
  INSERT_AT_MARK,
  TEXT_ADDED,
  CLEAR_PENDING_LINE,
  MOVE_LINESTART_MARK,
  APPLY_HIGHLIGHT,
  PARSE_COMMANDS,
  EXECUTE_ON_RECEIVED_TEXT,
};
typedef enum MainThreadOperationType_s MainThreadOperationType;

struct MainThreadOperation_s
{
  MainThreadOperationType  action;

  union
  {
    /* For INSERT_AT_END and INSERT_AT_MARK */
    struct
    {
      gchar      *text;
      gboolean    use_ta;
      TAState     ta_state;
      GtkTextTag *extra_tag;
    } text_insert;

    /* For APPLY_HIGHLIGHT */
    HighlightApplication *highlight;

    /* For PARSE_COMMANDS */
    struct
    {
      gchar    *commands;
      gchar    *colorline;
      AV       *matches;
      gboolean  notify;
    } commands;

    /* For EXECUTE_ON_RECEIVED_TEXT */
    HookExecution *hook_execution;
  };
};
typedef struct MainThreadOperation_s MainThreadOperation;

/* Function that enqueues or executes a text view operation. */
typedef void  (*MainThreadOperationFunction)(WorldGUI            *gui,
                                             MainThreadOperation *operation);


/**************************************************
 * Model Columns in GUI Editors for Triggers, etc *
 **************************************************/
enum {
  WE_GUIED_POINTER,
  WE_GUIED_N_COLS,
};

/**************************************************************
 * Values used by the cell data functions for the GUI Editors *
 **************************************************************/
enum {
  PLUGIN_ENABLED,
  PLUGIN_NAME,
  PLUGIN_VERSION,
  PLUGIN_AUTHOR,
  PLUGIN_DESCRIPTION,
  TRIGGER_ENABLED,
  TRIGGER_NAME,
  TRIGGER_PATTERN,
  TRIGGER_ACTION,
  TRIGGER_GAG,
  TRIGGER_GAGLOG,
  TRIGGER_KEEPEXEC,
  TRIGGER_REWRITER,
  TRIGGER_ICASE,
  TRIGGER_HIGHLIGHT,
  ALIAS_ENABLED,
  ALIAS_PERL_EVAL,
  ALIAS_NAME,
  ALIAS_PATTERN,
  ALIAS_SUBSTITUTION,
  ALIAS_ICASE,
  MACRO_ENABLED,
  MACRO_NAME,
  MACRO_KEY,
  MACRO_ACTION,
  TIMER_ENABLED,
  TIMER_NAME,
  TIMER_INTERVAL,
  TIMER_COUNT,
  TIMER_ACTION,
  TIMER_TEMPORARY,
  HOOK_ENABLED,
  HOOK_NAME,
  HOOK_ACTION,
  PERMVAR_NAME,
  OBJECT_PLUGIN,
};


/********************
 * Global variables *
 ********************/
extern GtkWidget  *wndMain;              /* The main window widget */
extern GtkBuilder *main_builder;        /* Created main window & others */
extern GtkWidget  *ntbkWorlds;           /* Notebook with the worlds */
extern GList      *open_worlds;          /* List of open worlds */
extern World      *currentWorld;         /* The current world */
extern World      *world_for_perl;       /* The world for Perl to use */
extern World      *default_world;        /* World with default settings */
extern gboolean    window_has_focus;     /* Whether the main window has focus */
extern gint        worlds_with_new_text; /* # of worlds with unread text */
extern gboolean    debug_matches;        /* Show when a trigger/alias matches */
extern Prefs       globalPrefs;          /* Holds the global preferences */
extern FILE       *dumpFile;             /* File to dump decompr. data*/
extern FILE       *rdumpFile;            /* File to dump raw received data */
extern GdkRGBA     deffore;              /* Default foreground color */
extern GdkRGBA     defbold;              /* Default bold foreground color */
extern GdkRGBA     defback;              /* Default background color */
extern GdkRGBA     defboldback;          /* Default bold background color */
extern GdkRGBA     defansi[];            /* Default ansi colors */
/* gnutls certificate credentials */
extern gnutls_certificate_credentials_t tls_certif_cred;


/*********************************
 * Function prototypes - Main UI *
 *********************************/
GtkWidget* create_main_window(WorldGUI *gui);
void       set_main_window_size(int x, int y, int width, int height,
                                WorldGUI *gui);
void       add_global_css();
World*     get_world_to_connect();
void       remove_world(World *world, gboolean remove_gui);
gboolean   remove_world_timer(World *world);
void       set_focused_world(gint page);
gint       get_focused_world(void);
void       set_notebook_tab_position(GtkPositionType pos);
void       adjust_window_title(void);
gboolean   open_new_world(WorldGUI    *gui,
                          const gchar *cmdline,
                          gboolean     can_offline,
                          gboolean    *offline_selected);
gboolean   update_times_cb(gpointer data);
gboolean   confirm_quit(void);
void       worldgui_destroy_tooltip_window(WorldGUI *gui);
G_MODULE_EXPORT void     menu_findnext_activate_cb(GtkMenuItem *widget,
                                                   gpointer     data);


/*********************
 * Handling of paths *
 *********************/
const char *get_kildclient_directory_path(void);
const char *get_kildclient_installed_file(const char *file);


/**********************************
 * Function prototypes - World UI *
 **********************************/
WorldGUI *world_gui_new(gboolean is_for_mud);
void      world_gui_size_textview(WorldGUI *gui);
void      worldgui_determine_supported_chars(WorldGUI *gui);
void      free_world_gui(WorldGUI *gui);
void      configure_gui(WorldGUI *gui, World *world);
void      configure_css(WorldGUI *gui, World *world);
void      worldgui_configure_spell(WorldGUI *gui);
void      clear_button_cb(GtkButton *button, gpointer data);
void      send_naws_size(World *world);
void      completion_cell_data_function(GtkCellLayout   *cell_layout,
                                        GtkCellRenderer *renderer,
                                        GtkTreeModel    *model,
                                        GtkTreeIter     *iter,
                                        gpointer         data);
void      find_next_cb(GtkButton *button, gpointer data);
G_MODULE_EXPORT void menu_url_open(GtkMenuItem *menu, gchar *url);


/************************************************
 * Function prototypes - Line processing thread *
 ************************************************/
gpointer process_line_thread(gpointer data);
void     queue_process_line_operation(World                *world,
                                      ProcessLineOperation *operation);
void     free_process_line_operation(gpointer data);
void     match_triggers(World     *world,
                        gchar    **line,
                        gchar    **color_line,
                        gsize     *output_len,
                        gboolean  *gag_output,
                        gboolean  *gag_log,
                        gint      *n_matches,
                        GSList   **trigger_response);


/**************************************
 * Function prototypes - ANSI parsing *
 **************************************/
gpointer ansiparse_thread(gpointer data);
void     queue_ansiparse_operation(World *gui, AnsiParseOperation *operation);
void     free_ansiparse_operation(gpointer data);
void     insert_ansi_string(WorldGUI                    *gui,
                            const gchar                 *str,
                            gsize                        len,
                            MainThreadOperationType      where,
                            MainThreadOperationFunction  operation_func);


/*********************************************************************
 * Function prototypes - Operations that must run in the main thread *
 *********************************************************************/
void      queue_mainthread_operation(World               *world,
                                     MainThreadOperation *operation);
void      free_mainthread_operation(gpointer data);
gboolean  mainthread_operations_idle_cb(gpointer data);
void      process_mainthread_operation(WorldGUI            *world,
                                       MainThreadOperation *operation);


/***************************************************
 * Function prototypes - ANSI support for TextView *
 ***************************************************/
void      ansitextview_append_string(WorldGUI    *gui,
                                     const gchar *str);
void      ansitextview_append_string_nl(WorldGUI    *gui,
                                        const gchar *str);
void      ansitextview_append_stringf(WorldGUI    *gui,
                                      const gchar *format,
                                      ...);
void      ansitextview_append_stringf_nl(WorldGUI    *gui,
                                         const gchar *format,
                                         ...);
void      ansitextview_append_string_with_fgcolor(WorldGUI    *gui,
                                                  const gchar *str,
                                                  gint         fg_color);
void      ansitextview_append_ansi_string_immediately(WorldGUI *gui,
                                                      gchar *str,
                                                      gsize len);
void      ansitextview_get_char_size(WorldGUI *gui,
                                     gint     *char_height,
                                     gint     *char_width);
void      ansitextview_get_size(WorldGUI *gui, guint *lines, guint *cols);
void      ansitextview_update_color_tags(WorldGUI *gui, World *world);
gboolean  ansitextview_prune_extra_lines(gpointer data);
gchar    *strip_ansi(const gchar *original, gsize len);
/* Main thread operations */
void      ansitextview_op_insert_at_end(WorldGUI            *gui,
                                        MainThreadOperation *operation);
void      ansitextview_op_insert_at_mark(WorldGUI            *gui,
                                         MainThreadOperation *operation);
void      ansitextview_op_text_added(WorldGUI            *gui,
                                     MainThreadOperation *operation);
void      ansitextview_op_clear_pending_line(WorldGUI            *gui,
                                             MainThreadOperation *operation);
void      ansitextview_op_move_linestart_mark(WorldGUI            *gui,
                                              MainThreadOperation *operation);
void      ansitextview_op_apply_highlight(WorldGUI            *gui,
                                          MainThreadOperation *operation);


/********************************
 * Function prototypes - Worlds *
 ********************************/
void      set_default_colors(World *world);
void      create_default_world(void);
World    *create_new_world(gboolean load_defaults);
void      init_threads(World *world);
void      free_world(World *world);
void      remove_plugin_objects(World *world, Plugin *plugin);
gboolean  load_world_from_file(const char *file,
                               World *world,
                               gboolean forWorldSelector,
                               GError **error);
void      create_world_from_parameters(World *world,
                                       gchar *host,
                                       gchar *port,
                                       gchar *user,
                                       gchar *password);
void      load_command_history(World *world);
World    *get_world_from_cmdline(const gchar *cmdline);
void      save_world_to_file(World *world);
void      save_command_history(World *world);
gboolean  edit_world(World     **world,
                     GtkWindow  *parent,
                     gboolean   *newworld,
                     gboolean    modal);
void      kc_g_string_append_escaped(GString *str, const char *template, ...);
gint      disconnected_msgbox(gchar *msg, gboolean can_offline);
void      mark_as_offline(World *world);
void      prepare_display_name(World *world);
gchar    *replace_kc_escapes(const gchar *pattern,
                             const gchar *name,
                             const gchar *character);


/*********************************************
 * Function prototypes - World Editor Dialog *
 *********************************************/
GObject   *get_we_general_tab(World *world, GtkBuilder *ui_builder);
void       fill_we_general_tab(World *world, GtkBuilder *ui_builder);
void       update_we_general_parameters(World *world, GtkBuilder *ui_builder);
GObject   *get_we_mainwindow_tab(GtkBuilder *ui_builder);
void       fill_we_mainwindow_tab(World *world, GtkBuilder *ui_builder);
void       update_we_mainwindow_parameters(World      *world,
                                           GtkBuilder *ui_builder);
GObject   *get_we_colors_tab(World *world, GtkBuilder *ui_builder);
GObject   *get_we_statusbar_tab(GtkBuilder *ui_builder);
void       fill_we_statusbar_tab(World *world, GtkBuilder *ui_builder);
GObject   *get_we_input_tab(GtkBuilder *ui_builder);
void       fill_we_input_tab(World *world, GtkBuilder *ui_builder);
void       update_we_input_parameters(World *world, GtkBuilder *ui_builder);
GObject   *get_we_logging_tab(GtkBuilder *ui_builder, World *world);
void       fill_we_logging_tab(World *world, GtkBuilder *ui_builder);
void       update_we_logging_parameters(World *world, GtkBuilder *ui_builder);
void       set_input_line_controls(World         *world,
                                   GObject       *radSingleLine,
                                   GObject       *radMultiLine,
                                   GtkSpinButton *spnNLines);
GObject   *get_we_scripting_tab(GtkBuilder *ui_builder);
void       fill_we_scripting_tab(World *world, GtkBuilder *ui_builder);
void       update_we_scripting_parameters(World *world, GtkBuilder *ui_builder);
GObject   *get_we_plugins_tab(GtkBuilder *ui_builder, World *world);
GObject   *get_we_triggers_tab(World *world);
GObject   *get_we_aliases_tab(World *world);
GObject   *get_we_macros_tab(World *world);
GObject   *get_we_timers_tab(World *world);
GObject   *get_we_hooks_tab(World *world);
GObject   *get_we_vars_tab(World *world);
GObject   *get_we_protocols_tab(GtkBuilder *ui_builder);
void       fill_we_protocols_tab(World *world, GtkBuilder *ui_builder);
void       update_we_protocols_parameters(World *world, GtkBuilder *ui_builder);
GObject   *get_we_advanced_tab(World *world, GtkBuilder *ui_builder);
void       fill_we_advanced_tab(World *world, GtkBuilder *ui_builder);
void       update_we_advanced_parameters(World *world, gboolean *newworld,
                                         GtkBuilder *ui_builder);

G_MODULE_EXPORT void we_guied_export_cb(GtkButton *button, gpointer data);

void       we_guied_bool_func(GtkTreeViewColumn *column,
                              GtkCellRenderer   *renderer,
                              GtkTreeModel      *model,
                              GtkTreeIter       *iter,
                              gpointer           data);
void       we_guied_text_func(GtkTreeViewColumn *column,
                              GtkCellRenderer   *renderer,
                              GtkTreeModel      *model,
                              GtkTreeIter       *iter,
                              gpointer           data);
gboolean   we_guied_plugin_filter_func(GtkTreeModel *model,
                                       GtkTreeIter  *iter,
                                       gpointer      data);
void       we_guied_bool_col_toggled_cb(GtkCellRendererToggle *renderer,
                                        gchar                 *pathstr,
                                        gpointer               data);
void       we_guied_selection_changed_cb(GtkTreeSelection *selection,
                                         gpointer          data);

GtkWidget *we_guied_confirm_delete_dialog_new(GtkWindow   *parent,
                                              const gchar *objname,
                                              int          n);
void       we_lplugin_insert_plugin(World *world, Plugin *plugin);
void       we_lplugin_update_plugin(World *world, Plugin *plugin);

void       we_trigger_update_trigger(World *world, Trigger *trigger);
void       we_trigger_insert_trigger(World *world, Trigger *trigger, gint pos);
void       we_trigger_delete_trigger(World *world, Trigger *trigger);

void       we_alias_update_alias(World *world, Alias *alias);
void       we_alias_insert_alias(World *world, Alias *alias, gint pos);
void       we_alias_delete_alias(World *world, Alias *alias);

void       we_macro_update_macro(World *world, Macro *macro);
void       we_macro_insert_macro(World *world, Macro *macro, gint pos);
void       we_macro_delete_macro(World *world, Macro *macro);

void       we_timer_update_timer(World *world, Timer *timer);
void       we_timer_insert_timer(World *world, Timer *timer, gint pos);
void       we_timer_delete_timer(World *world, Timer *timer);

void       we_hook_update_hook(World *world, const gchar *event,
                               Hook *hook);
void       we_hook_insert_hook(World *world, const gchar *event,
                               Hook *hook, gint pos);
void       we_hook_delete_hook(World *world, const gchar *event,
                               Hook *hook);

void       we_var_update_var(World *world,
                             const gchar *old_var, const gchar *new_var);
void       we_var_insert_var(World *world, const gchar *var, gint pos);
void       we_var_delete_var(World *world, const gchar *var);


/*****************************************
 * Function prototypes - Command History *
 *****************************************/
void   prev_or_next_command(World *world, gboolean next);
void   find_prev_or_next_command(World *world, gboolean next);
gchar *cmdhistory_get_command(GtkTreeModel *model, GtkTreeIter *iter);
void   add_recent_command(World *world, const gchar *command);
void   insert_recent_command(World *world, const gchar *command);


/*****************************************
 * Function prototypes - Multi-line send *
 *****************************************/
void do_multi_line_send(MLSendData *mlcontext, gdouble delay);


/*********************************
 * Function prototypes - Network *
 *********************************/
void      connect_to(World *world);
gboolean  disconnect_world_idle(gpointer data);
void      disconnect_world(gpointer data, gboolean run_hook);
gboolean  reconnect_world(gpointer data);
gboolean  connect_another(gpointer data);
gchar    *close_connection(World *world, gboolean run_hook);
gboolean  flush_buffer_timeout_cb(gpointer data);
void      perform_auto_logon(World *world);


/*************************************
 * Function prototypes - Preferences *
 *************************************/
void        read_global_preferences(int *x, int *y, int *width, int *height);
void        save_global_preferences(void);
const char *create_kildclient_directory(void);
G_MODULE_EXPORT void show_preferences_dialog_cb(GtkMenuItem *widget,
                                                gpointer     data);


/********************************
 * Function Prototypes - Parser *
 ********************************/
void parse_commands(World *world, const char *cmdline);
void parse_commands_full(World       *world,
                         const char  *cmdline,
                         AV          *matches,
                         const gchar *colorline,
                         gboolean     notify);
void mainthread_op_parse_commands(World *world, MainThreadOperation *operation);
void parse_command_line_immediately(World *world, const char *cmdline);
void send_to_world(World *world, const char *cmd, gint len);
void send_to_world_no_check(World      *world,
                            const char *utf8_command,
                            int         utf8_len,
                            char       *locale_command,
                            gsize       locale_len,
                            gboolean    add_newline,
                            gboolean    has_incomplete_line);


/**********************************
 * Function Prototypes - Triggers *
 **********************************/
Trigger  *new_trigger(void);
void      remove_trigger(World *world, GSList *triggeritem);
gboolean  move_trigger(World *world, gint old_pos, gint new_pos);
void      free_trigger(Trigger *trigger, gpointer data);
void      list_triggers(World *world, Plugin *plugin);
void      trigger_precompute_res(World *world);
void      save_trigger(GString *str, Trigger *trigger);
void      open_test_triggers_dialog(World *world);


/*********************************
 * Function Prototypes - Aliases *
 *********************************/
char    *substitute_aliases(World *world, const char *cmd);
void     remove_alias(World *world, GSList *aliasitem);
gboolean move_alias(World *world, gint old_pos, gint new_pos);
void     free_alias(Alias *alias, gpointer data);
void     list_aliases(World *world, Plugin *plugin);
void     alias_precompute_res(World *world);
void     save_alias(GString *str, Alias *alias);


/********************************
 * Function Prototypes - Macros *
 ********************************/
gboolean check_macros(World *world, guint keyval, GdkModifierType modifiers);
void     remove_macro(World *world, GSList *macroitem);
gboolean move_macro(World *world, gint old_pos, gint new_pos);
void     free_macro(Macro *macro, gpointer data);
void     list_macros(World *world, Plugin *plugin);
void     save_macro(GString *str, Macro *macro);


/********************************
 * Function Prototypes - Timers *
 ********************************/
gboolean run_timer(gpointer data);
void     start_timers(World *world);
void     remove_timer(World *world, GSList *timeritem);
gboolean move_timer(World *world, gint old_pos, gint new_pos);
void     free_timer(Timer *timer, gpointer data);
void     list_timers(World *world, Plugin *plugin);
void     save_timer(GString *str, Timer *timer);


/*********************************************
 * Function Prototypes - Permanent Variables *
 *********************************************/
void     load_permanent_variables(World *world);
void     save_permanent_variables(World *world);
gboolean move_var(World *world, gint old_pos, gint new_pos);
void     free_permanent_variable(gchar *variable, gpointer data);


/*******************************
 * Function Prototypes - Hooks *
 *******************************/
gboolean      connect_hook(World       *world,
                           const gchar *event,
                           int          pos,
                           gchar       *action,
                           gchar       *name,
                           gboolean     enabled);
void          execute_hook(World       *world,
                           const gchar *hook,
                           const gchar *hookdata);
void          execute_hook_immediately(World       *world,
                                       const gchar *hook,
                                       const gchar *hookdata);
void          list_hooks(World *world, const gchar *event, Plugin *plugin);
void          list_hook(World *world, const gchar *event, int pos);
gboolean      delete_hook(World       *world,
                          const gchar *event,
                          int          pos,
                          GSList      *hook);
gboolean      move_hook(World *world, const gchar *event,
                        gint old_pos, gint new_pos);
void          free_hook(Hook *hook, gpointer data);
void          save_hooks(GString *str, World *world);
void          save_hook(GString *str, Hook *hook);
GSList      **get_hook_list_for_writing(World        *world,
                                        const gchar  *event,
                                        gint        **poscounter);
gboolean      get_hook_list_for_reading(World        *world,
                                        const gchar  *event,
                                        GSList      **list,
                                        gint         *pos);
GSList       *get_hook_list_or_warn_if_nonexistent(World       *world,
                                                   const gchar *event);
GtkTreeModel *get_hook_model(World *world, const gchar *event);
gboolean      are_there_enabled_server_data_hooks(World *world);
void          mainthread_op_execute_on_received_text_hook(HookExecution *hook_execution);


/*********************************
 * Function Prototypes - Logging *
 *********************************/
void     write_log_line(World *world, const gchar *line, time_t *line_time);
void     start_log_and_display_result(World *world);
gboolean start_log(World *world, gint lines, gchar **errormsg);
void     stop_log(World *world);


/*********************************
 * Function prototypes - Plugins *
 *********************************/
Plugin  *get_plugin(World *world, const char *name);
void     free_plugin(Plugin *plugin, World *world);
gboolean load_plugin(World *world, gchar *filename, GError **error);
void     load_startup_plugins(World *world);
gboolean add_startup_plugin(World *world, gchar *filename, GError **error);
void     make_startup_plugin_list_for_editing(World *world);


/*******************************
 * Function prototypes - Sound *
 *******************************/
void play_sound(const gchar *file);


/*********************************************************
 * Function prototypes - Miscellaneous utility functions *
 *********************************************************/
void   add_css_provider_and_class(GtkWidget      *widget,
                                  GtkCssProvider *provider,
                                  const gchar    *class);
gchar *pango_font_to_css(const gchar *pango_font);
void   kc_error_dialog(GtkWindow *parent, const gchar *format, ...);
void   kc_error_dialog_with_title(GtkWindow   *parent,
                                  const gchar *title,
                                  const gchar *format, ...);


#endif
