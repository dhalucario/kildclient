/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include "libintl-wrapper.h"
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

#include "kildclient.h"
#include "perlscript.h"


/***********************
 * Function prototypes *
 ***********************/
static GtkTreeModel *create_we_aliases_model(World *world);
static void          we_alias_configure_view(GtkTreeView *view,
                                             GtkTreeModel *model);
static void          we_alias_add_cb(GtkButton *button, gpointer data);
static void          we_alias_edit_cb(GtkButton *button, gpointer data);
static void          we_alias_delete_cb(GtkButton *button, gpointer data);
static void          we_alias_view_row_dblclick_cb(GtkTreeView        *view,
                                                   GtkTreePath        *path,
                                                   GtkTreeViewColumn  *col,
                                                   gpointer            data);
static gboolean      edit_alias(World *world, Alias *alias);
static gboolean      we_alias_view_keypress_cb(GtkWidget   *widget,
                                               GdkEventKey *evt,
                                               gpointer     data);
static void          we_alias_move_cb(GtkButton *button, gpointer data);



GObject *
get_we_aliases_tab(World *world)
{
  GtkBuilder       *ui_builder;
  gchar            *objects[] = { "we_panel_generic_graphical_editor", NULL };
  GError           *error = NULL;
  GObject          *panel;
  GtkLabel         *lblType;
  GObject          *btnAdd;
  GObject          *btnEdit;
  GObject          *btnDelete;
  GObject          *btnUp;
  GObject          *btnDown;
  GObject          *btnExport;
  GtkTreeSelection *selection;

  /* Create the dialog */
  ui_builder = gtk_builder_new();
  if (!gtk_builder_add_objects_from_resource(ui_builder,
                                             "/ekalin/kildclient/kildclient.ui",
                                             objects,
                                             &error)) {
    g_warning(_("Error loading UI from XML file: %s"), error->message);
    g_error_free(error);
    return NULL; /* This will cause warnings in the calling function, but
                    there is need to abort the program because of this. */
  }

  panel = gtk_builder_get_object(ui_builder,
                                 "we_panel_generic_graphical_editor");
  /* So it's not destroyed when the builder is destroyed */
  g_object_ref(panel);

  world->viewAlias = GTK_TREE_VIEW(gtk_builder_get_object(ui_builder,
                                                          "viewObjects"));
  g_object_set_data(G_OBJECT(world->viewAlias), "forworld", world);
  g_signal_connect(G_OBJECT(world->viewAlias), "row-activated",
                   G_CALLBACK(we_alias_view_row_dblclick_cb), world);
  g_signal_connect(G_OBJECT(world->viewAlias), "key-press-event",
                   G_CALLBACK(we_alias_view_keypress_cb), world);

  lblType = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblGEType"));
  gtk_label_set_markup(lblType, "<span weight=\"bold\">Aliases</span>");

  btnAdd = gtk_builder_get_object(ui_builder, "btnGEAdd");
  g_signal_connect(btnAdd, "clicked",
                   G_CALLBACK(we_alias_add_cb), world->viewAlias);
  btnEdit = gtk_builder_get_object(ui_builder, "btnGEEdit");
  g_signal_connect(btnEdit, "clicked",
                   G_CALLBACK(we_alias_edit_cb), world->viewAlias);
  btnDelete = gtk_builder_get_object(ui_builder, "btnGEDelete");
  g_signal_connect(btnDelete, "clicked",
                   G_CALLBACK(we_alias_delete_cb), world->viewAlias);

  btnUp = gtk_builder_get_object(ui_builder, "btnGEUp");
  g_object_set_data(btnUp, "isup", GINT_TO_POINTER(TRUE));
  g_signal_connect(btnUp, "clicked",
                   G_CALLBACK(we_alias_move_cb), world->viewAlias);
  btnDown = gtk_builder_get_object(ui_builder, "btnGEDown");
  g_object_set_data(btnDown, "isup", GINT_TO_POINTER(FALSE));
  g_signal_connect(btnDown, "clicked",
                   G_CALLBACK(we_alias_move_cb), world->viewAlias);

  btnExport = gtk_builder_get_object(ui_builder, "btnGEExport");
  g_object_set_data(btnExport, "preselect", "Alias");

  world->alias_model = create_we_aliases_model(world);
  world->alias_model_filter = GTK_TREE_MODEL_FILTER(gtk_tree_model_filter_new(world->alias_model, NULL));
  g_object_unref(G_OBJECT(world->alias_model));
  gtk_tree_model_filter_set_visible_func(GTK_TREE_MODEL_FILTER(world->alias_model_filter),
                                         we_guied_plugin_filter_func,
                                         world,
                                         NULL);

  we_alias_configure_view(world->viewAlias,
                          GTK_TREE_MODEL(world->alias_model_filter));
  selection = gtk_tree_view_get_selection(world->viewAlias);
  gtk_tree_selection_set_mode(selection, GTK_SELECTION_MULTIPLE);
  g_object_set_data(G_OBJECT(selection), "btnEdit",   btnEdit);
  g_object_set_data(G_OBJECT(selection), "btnDelete", btnDelete);
  g_object_set_data(G_OBJECT(selection), "btnUp",     btnUp);
  g_object_set_data(G_OBJECT(selection), "btnDown",   btnDown);
  g_object_set_data(G_OBJECT(selection), "btnExport", btnExport);
  g_signal_connect(G_OBJECT(selection), "changed",
                   G_CALLBACK(we_guied_selection_changed_cb), NULL);

  gtk_builder_connect_signals(ui_builder, world);
  g_object_unref(ui_builder);
  return panel;
}


static
GtkTreeModel *
create_we_aliases_model(World *world)
{
  GtkListStore *store;
  GtkTreeIter   iter;
  GSList       *aliasptr;
  Alias        *alias;

  store = gtk_list_store_new(WE_GUIED_N_COLS,
                             G_TYPE_POINTER);

  aliasptr = world->aliases;
  while (aliasptr) {
    alias = (Alias *) aliasptr->data;
    gtk_list_store_append(store, &iter);
    gtk_list_store_set(store, &iter,
                       WE_GUIED_POINTER, alias,
                       -1);

    aliasptr = aliasptr->next;
  }

  return GTK_TREE_MODEL(store);
}


static
void
we_alias_configure_view(GtkTreeView *view, GtkTreeModel *model)
{
  GtkCellRenderer   *renderer;
  GtkTreeViewColumn *column;

  gtk_tree_view_set_model(view, model);
  g_object_unref(model);

  /* Column 1 - Enabled? */
  renderer = gtk_cell_renderer_toggle_new();
  g_object_set(G_OBJECT(renderer), "activatable", TRUE, NULL);
  g_object_set_data(G_OBJECT(renderer), "column",
                    GINT_TO_POINTER(ALIAS_ENABLED));
  g_signal_connect(G_OBJECT(renderer), "toggled",
                   G_CALLBACK(we_guied_bool_col_toggled_cb), view);
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Enabled"));
  gtk_tree_view_column_pack_start(column, renderer, FALSE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_bool_func,
                                          GINT_TO_POINTER(ALIAS_ENABLED),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 2 - Name */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Name"));
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_text_func,
                                          GINT_TO_POINTER(ALIAS_NAME),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 3 - Pattern */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Pattern"));
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_text_func,
                                          GINT_TO_POINTER(ALIAS_PATTERN),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 4 - Ignore Case? */
  renderer = gtk_cell_renderer_toggle_new();
  g_object_set(G_OBJECT(renderer), "activatable", TRUE, NULL);
  g_object_set_data(G_OBJECT(renderer), "column",
                    GINT_TO_POINTER(ALIAS_ICASE));
  g_signal_connect(G_OBJECT(renderer), "toggled",
                   G_CALLBACK(we_guied_bool_col_toggled_cb), view);
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Ig. case"));
  gtk_tree_view_column_pack_start(column, renderer, FALSE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_bool_func,
                                          GINT_TO_POINTER(ALIAS_ICASE),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 5 - Substitution */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Substitution"));
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_text_func,
                                          GINT_TO_POINTER(ALIAS_SUBSTITUTION),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 6 - Perl Eval? */
  renderer = gtk_cell_renderer_toggle_new();
  g_object_set(G_OBJECT(renderer), "activatable", TRUE, NULL);
  g_object_set_data(G_OBJECT(renderer), "column",
                    GINT_TO_POINTER(ALIAS_PERL_EVAL));
  g_signal_connect(G_OBJECT(renderer), "toggled",
                   G_CALLBACK(we_guied_bool_col_toggled_cb), view);
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Eval as Perl"));
  gtk_tree_view_column_pack_start(column, renderer, FALSE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_bool_func,
                                          GINT_TO_POINTER(ALIAS_PERL_EVAL),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 7 - Plugin */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Plugin"));
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_text_func,
                                          GINT_TO_POINTER(OBJECT_PLUGIN),
                                          NULL);
  gtk_tree_view_append_column(view, column);


  gtk_tree_view_columns_autosize(view);
}


static
void
we_alias_add_cb(GtkButton *button, gpointer data)
{
  World       *world;
  GtkTreeView *view;
  Alias       *alias;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");

  alias = g_new0(Alias, 1);
  alias->enabled = 1;

  if (edit_alias(world, alias)) {
    world->aliases = g_slist_insert(world->aliases,
                                    alias,
                                    world->alias_pos);
    we_alias_insert_alias(world, alias, world->alias_pos++);
  } else {
    g_free(alias);
  }
}


static
void
we_alias_edit_cb(GtkButton *button, gpointer data)
{
  /* Note that the Edit button is only active when exactly one row
     is selected. */
  World            *world;
  GtkTreeView      *view;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  GList            *selected;
  GtkTreeIter       iter;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");
  selection = gtk_tree_view_get_selection(view);
  selected = gtk_tree_selection_get_selected_rows(selection, &model);
  if (selected) {
    Alias *alias;

    gtk_tree_model_get_iter(model, &iter, selected->data);
    gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &alias, -1);

    if (edit_alias(world, alias)) {
      we_alias_update_alias(world, alias);
    }

    g_list_foreach(selected, (GFunc) gtk_tree_path_free, NULL);
    g_list_free(selected);
  }
}


static
void
we_alias_delete_cb(GtkButton *button, gpointer data)
{
  World            *world;
  GtkTreeView      *view;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  GList            *selected;
  GtkTreeIter       iter;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");
  selection = gtk_tree_view_get_selection(view);
  selected = gtk_tree_selection_get_selected_rows(selection, &model);
  if (selected) {
    Alias     *alias;
    GSList    *aliasitem;
    GtkWidget *msgdlg;
    gint       n;

    n = gtk_tree_selection_count_selected_rows(selection);
    msgdlg = we_guied_confirm_delete_dialog_new(GTK_WINDOW(world->dlgEditWorld),
                                                ngettext("alias", "aliases", n),
                                                n);

    if (!world->confirm_delete
        || gtk_dialog_run(GTK_DIALOG(msgdlg)) == GTK_RESPONSE_YES) {
      GList *listiter = g_list_last(selected);

      while (listiter) {
        gtk_tree_model_get_iter(model, &iter, listiter->data);
        gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &alias, -1);

        aliasitem = g_slist_find(world->aliases, alias);
        remove_alias(world, aliasitem);

        listiter = listiter->prev;
      }
    }
    gtk_widget_destroy(msgdlg);

    g_list_foreach(selected, (GFunc) gtk_tree_path_free, NULL);
    g_list_free(selected);
  }
}


static
void
we_alias_view_row_dblclick_cb(GtkTreeView        *view,
                              GtkTreePath        *path,
                              GtkTreeViewColumn  *col,
                              gpointer            data)
{
  World        *world;
  GtkTreeModel *model;
  GtkTreeIter   iter;

  world = (World *) data;
  model = gtk_tree_view_get_model(view);

  if (gtk_tree_model_get_iter(model, &iter, path)) {
    Alias *alias;

    gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &alias, -1);

    if (edit_alias(world, alias)) {
      we_alias_update_alias(world, alias);
    }
  }
}


static
gboolean
edit_alias(World *world, Alias *alias)
{
  GtkBuilder      *ui_builder;
  gchar           *objects[] = { "dlgEditAlias", NULL };
  GError          *error = NULL;
  GtkWidget       *dlgEditAlias;
  GtkEntry        *txtName;
  GtkEntry        *txtPattern;
  GtkEntry        *txtSubstitution;
  GtkToggleButton *chkICase;
  GtkToggleButton *chkPerlEval;
  GtkToggleButton *chkEnabled;
  const gchar     *newname;
  const gchar     *newpattern;
  const gchar     *newsubstitution;
  gboolean         newicase;
  gboolean         need_recompute = FALSE;

  ui_builder = gtk_builder_new();
  if (!gtk_builder_add_objects_from_resource(ui_builder,
                                             "/ekalin/kildclient/kildclient.ui",
                                             objects,
                                             &error)) {
    g_warning(_("Error loading UI from XML file: %s"), error->message);
    g_error_free(error);
    return FALSE;
  }

  dlgEditAlias
    = GTK_WIDGET(gtk_builder_get_object(ui_builder, "dlgEditAlias"));
  gtk_window_set_transient_for(GTK_WINDOW(dlgEditAlias),
                               GTK_WINDOW(world->dlgEditWorld));
  gtk_dialog_set_default_response(GTK_DIALOG(dlgEditAlias), GTK_RESPONSE_OK);

  txtName
    = GTK_ENTRY(gtk_builder_get_object(ui_builder, "txtAliasName"));
  txtPattern
    = GTK_ENTRY(gtk_builder_get_object(ui_builder, "txtAliasPattern"));
  txtSubstitution
    = GTK_ENTRY(gtk_builder_get_object(ui_builder, "txtAliasSubstitution"));
  chkICase
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "chkAliasICase"));
  chkPerlEval
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "chkAliasPerlEval"));
  chkEnabled
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "chkAliasEnabled"));

  /* Only display note when editing a plugin's alias */
  if (!alias->owner_plugin) {
    GObject *label;

    label = gtk_builder_get_object(ui_builder, "lblNoteAlias");
    gtk_widget_destroy(GTK_WIDGET(label));
    label = gtk_builder_get_object(ui_builder, "lblNoteTextAlias");
    gtk_widget_destroy(GTK_WIDGET(label));
  }

  /* We don't need it anymore */
  g_object_unref(ui_builder);

  /* Fill-in values */
  if (alias->name) {
    gtk_entry_set_text(txtName, alias->name);
  }
  if (alias->pattern) {
    gtk_entry_set_text(txtPattern, alias->pattern);
  }
  if (alias->substitution) {
    gtk_entry_set_text(txtSubstitution, alias->substitution);
  }
  gtk_toggle_button_set_active(chkICase,    alias->ignore_case);
  gtk_toggle_button_set_active(chkPerlEval, alias->perl_eval);
  gtk_toggle_button_set_active(chkEnabled,  alias->enabled);

  /* Run the dialog until the input is valid or cancelled */
  gtk_widget_show_all(dlgEditAlias);
  while (1) {
    if (gtk_dialog_run(GTK_DIALOG(dlgEditAlias)) == GTK_RESPONSE_OK) {
      /* Validate */
      newicase = gtk_toggle_button_get_active(chkICase);

      newpattern = gtk_entry_get_text(txtPattern);
      if (strcmp(newpattern, "") == 0) {
        kc_error_dialog(GTK_WINDOW(dlgEditAlias),
                        _("You must specify the pattern."));
        continue;
      }

      newsubstitution = gtk_entry_get_text(txtSubstitution);
      if (strcmp(newsubstitution, "") == 0) {
        kc_error_dialog(GTK_WINDOW(dlgEditAlias),
                        _("You must specify the substitution."));
        continue;
      }

      /* Update values */
      newname = gtk_entry_get_text(txtName);
      if (!alias->name || strcmp(newname, alias->name) != 0) {
        g_free(alias->name);
        if (strcmp(newname, "") != 0) {
          alias->name = g_strdup(newname);
        } else {
          alias->name = NULL;
        }
      }

      if (!alias->pattern || strcmp(newpattern, alias->pattern) != 0) {
        g_free(alias->pattern);
        alias->pattern = g_strdup(newpattern);
        need_recompute = TRUE;
      }

      if (!alias->substitution
          || strcmp(newsubstitution, alias->substitution) != 0) {
        g_free(alias->substitution);
        alias->substitution = g_strdup(newsubstitution);
      }

      if (alias->ignore_case != newicase) {
        need_recompute = TRUE;
      }
      alias->ignore_case = newicase;
      alias->perl_eval   = gtk_toggle_button_get_active(chkPerlEval);
      alias->enabled     = gtk_toggle_button_get_active(chkEnabled);

      if (need_recompute && world->perl_interpreter) {
        GRAB_PERL(world);
        if (alias->pattern_re) {
          SvREFCNT_dec(alias->pattern_re);
        }
        alias->pattern_re = precompute_re(alias->pattern, FALSE);
        RELEASE_PERL(world);
      }

      /* We've finished successfully */
      gtk_widget_destroy(dlgEditAlias);
      return TRUE;
    } else {
      /* Cancel pressed */
      gtk_widget_destroy(dlgEditAlias);
      return FALSE;
    }
  }
}


static
gboolean
we_alias_view_keypress_cb(GtkWidget   *widget,
                          GdkEventKey *evt,
                          gpointer     data)
{
  if (evt->keyval == GDK_KEY_Delete || evt->keyval == GDK_KEY_KP_Delete) {
    we_alias_delete_cb(NULL, widget);
    return TRUE;
  }

  return FALSE;
}


static
void
we_alias_move_cb(GtkButton *button, gpointer data)
{
  GtkTreeView      *view;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  GList            *selected;
  GtkTreeIter       iter;
  World            *world;
  gboolean          is_up;
  gint              new_pos;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");
  selection = gtk_tree_view_get_selection(view);
  is_up = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(button), "isup"));
  selected = gtk_tree_selection_get_selected_rows(selection, &model);
  if (selected) {
    Alias *alias;
    gint   pos;

    gtk_tree_model_get_iter(model, &iter, selected->data);
    gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &alias, -1);
    pos = g_slist_index(world->aliases, alias);
    new_pos = pos;
    if (is_up) {
      if (pos != 0) {
        new_pos = pos - 1;
        move_alias(world, pos, new_pos);
      }
    } else {
      if (pos != world->alias_pos - 1) {
        new_pos = pos + 1;
        move_alias(world, pos, new_pos);
      }
    }
    /* Reselect the moved item */
    gtk_tree_model_iter_nth_child(model, &iter, NULL, new_pos);
    gtk_tree_selection_select_iter(selection, &iter);

    g_list_foreach(selected, (GFunc) gtk_tree_path_free, NULL);
    g_list_free(selected);
  }
}


void
we_alias_update_alias(World *world, Alias *alias_arg)
{
  /* Called by the Perl functions when an alias is changed, so that
     the display is updated. */
  GtkTreeIter  iter;
  GtkTreePath *path;
  Alias       *alias;
  gboolean     success;

  if (!world->alias_model) {
    return;
  }

  success = gtk_tree_model_get_iter_first(world->alias_model, &iter);
  while (success) {
    gtk_tree_model_get(world->alias_model, &iter,
                       WE_GUIED_POINTER, &alias, -1);
    if (alias == alias_arg) {
      path = gtk_tree_model_get_path(world->alias_model, &iter);
      gtk_tree_model_row_changed(world->alias_model, path, &iter);
      gtk_tree_path_free(path);
      return;
    }
    success = gtk_tree_model_iter_next(world->alias_model, &iter);
  }
}


void
we_alias_insert_alias(World *world, Alias *alias, gint pos)
{
  /* Called by the Perl functions when an alias is inserted, so that
     the display is updated. */
  GtkTreeIter iter;

  if (!world->alias_model) {
    return;
  }

  gtk_list_store_insert(GTK_LIST_STORE(world->alias_model), &iter, pos);
  gtk_list_store_set(GTK_LIST_STORE(world->alias_model), &iter,
                     WE_GUIED_POINTER, alias,
                     -1);
}


void
we_alias_delete_alias(World *world, Alias *alias_arg)
{
  /* Called by the Perl functions when an alias is deleted, so that
     the display is updated. */
  GtkTreeIter  iter;
  Alias       *alias;
  gboolean     success;

  if (!world->alias_model) {
    return;
  }

  success = gtk_tree_model_get_iter_first(world->alias_model, &iter);
  while (success) {
    gtk_tree_model_get(world->alias_model, &iter,
                       WE_GUIED_POINTER, &alias, -1);
    if (alias == alias_arg) {
      gtk_list_store_remove(GTK_LIST_STORE(world->alias_model), &iter);
      return;
    }
    success = gtk_tree_model_iter_next(world->alias_model, &iter);
  }
}
