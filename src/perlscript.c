/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include "libintl-wrapper.h"
#include <time.h>
#include <locale.h>
#include <gtk/gtk.h>

/* Perl includes */
#include <EXTERN.h>
#include <perl.h>
#include <XSUB.h>

#include "kildclient.h"
#include "ansi.h"
#include "perlscript.h"
#include "net.h"


/*****************
 * Useful macros *
 *****************/
#define FETCHWORLDVARS     World  *world
#define FETCHWORLDVARSFULL HV     *self; \
                           World  *world
#define FETCHWORLD         if (!(world = fetch_world(items, ST(0), NULL))) { \
                             XSRETURN_EMPTY;                                 \
                           }
#define FETCHWORLDFULL     if (!(world = fetch_world(items, ST(0), &self))) { \
                             XSRETURN_EMPTY;                                  \
                           }



/***********************
 * Function prototypes *
 ***********************/
static GSList *AVtoGSList(AV *array);
static SV     *create_world_var(World *world);
static AV     *perl_clone_array(AV *orig);
static void    perl_copy_array(AV *source, AV *dest);
static SV     *create_window_var(void);
static World  *fetch_world(int items, SV *self_ref, HV **self_to_store);
static void    xs_init();
static int     world_name_comparer(const World *world, const char *name);
static GSList *get_object_numbers(GSList     *list,
                                  const char *name,
                                  int         searchtype);
static void    fill_highlight(Trigger *trigger, HV *style);
/* For dynamic loading of modules */
EXTERN_C void  boot_DynaLoader (pTHX_ CV* cv);


/******************
 * Initialization *
 ******************/
World *world_for_perl = NULL;


/*************************
 * File global variables *
 *************************/
static int   my_argc_static = 3;
static char *my_argv_static[] = { "kildperl",
                                  "-MData::Dumper",
                                  "-w" };


void
init_perl_system(void)
{
  PERL_SYS_INIT(&my_argc_static, (char ***) &my_argv_static);
}


void
init_perl_script(World *world)
{
  int    my_argc;
  char **my_argv;
  char  *perl_cmds;
  char  *system_plugin_dir;
  char  *user_plugin_dir;

  SV *world_ref;
  SV *world_var;
  SV *window_ref;
  SV *window_var;

  my_argv = g_malloc(sizeof(char *) * (my_argc_static + 2));
  for (my_argc = 0; my_argc < my_argc_static; ++my_argc) {
    my_argv[my_argc] = my_argv_static[my_argc];
  }
  my_argv[my_argc++] = g_strdup_printf("-I%s", get_kildclient_installed_file(""));
#ifdef __MINGW32__
  my_argv[my_argc++] = g_strdup_printf("-I%s", get_kildclient_installed_file("perl"));
#endif
  my_argv[my_argc] = g_strdup(get_kildclient_installed_file("kildclient.pl"));

#ifndef __MINGW32__
  user_plugin_dir = g_strdup_printf("%s/plugins",
                                    get_kildclient_directory_path());
#else
  user_plugin_dir = g_strdup_printf("%s\\plugins",
                                    get_kildclient_directory_path());
#endif
  system_plugin_dir = g_strdup(get_kildclient_installed_file("plugins"));

  perl_cmds = g_strdup_printf("$SIG{__WARN__} = sub {"
                              "$world->echo(\"Perl Warning: $_[0]\");"
                              "};"
                              "__sethelpfile('%s');"
                              "push(@PLUGINDIRS, '%s', '%s')",
                              get_kildclient_installed_file("kildclient.hlp"),
                              system_plugin_dir,
                              user_plugin_dir);
  g_free(user_plugin_dir);
  g_free(system_plugin_dir);

  world->perl_interpreter = perl_alloc();
  PERL_SET_CONTEXT(world->perl_interpreter);
  perl_construct(world->perl_interpreter);

  perl_parse(world->perl_interpreter, xs_init,
             my_argc, my_argv, NULL);
  /* PL_exit_flags |= PERL_EXIT_DESTRUCT_END; */
  eval_pv(perl_cmds, TRUE);

  /* Create $world variable */
  world_ref = create_world_var(world);
  world_var = get_sv("world", TRUE);
  sv_setsv(world_var, world_ref);

  /* Create $window variable */
  window_ref = create_window_var();
  window_var = get_sv("window", TRUE);
  sv_setsv(window_var, window_ref);

  g_free(my_argv[my_argc]);
  g_free(my_argv[my_argc - 1]);
#ifdef __MINGW32__
  g_free(my_argv[my_argc - 2]);
#endif
  g_free(my_argv);
  g_free(perl_cmds);
}


static
SV *
create_world_var(World *world)
{
  /* Currently the hash holds nothing. It's just used to indicate that
     the function refers to the window. */
  HV *world_hash;
  IV  worldptriv;
  SV *worldptrsv;
  SV *silentsv;
  SV *world_ref;
  HV *world_pkg_stash;

  world_hash = newHV();

  worldptriv = PTR2IV(world);
  worldptrsv = newSViv(worldptriv);
  (void) hv_store(world_hash, "WORLDPTR", 8, worldptrsv, 0);

  silentsv = newSViv(0);
  (void) hv_store(world_hash, "SILENT", 6, silentsv, 0);

  world_ref = newRV_noinc((SV *) world_hash);
  world_pkg_stash = gv_stashpv("World", TRUE);
  world_ref = sv_bless(world_ref, world_pkg_stash);

  return world_ref;
}


static
SV *
create_window_var(void)
{
  HV *window_hash;
  SV *window_ref;
  HV *window_pkg_stash;

  window_hash = newHV();

  window_ref = newRV_noinc((SV *) window_hash);
  window_pkg_stash = gv_stashpv("Window", TRUE);
  window_ref = sv_bless(window_ref, window_pkg_stash);

  return window_ref;
}


void
destruct_perl_script(PerlInterpreter *perl_interpreter)
{
  perl_destruct(perl_interpreter);
  perl_free(perl_interpreter);
}


void
perl_script_import_file(const char *file)
{
  char *cmd = g_strdup_printf("do '%s';", file);
  eval_pv(cmd, TRUE);
  g_free(cmd);
}


int
perl_match(const char      *line,
           SV              *pattern_re,
           int             *match_start,
           int             *match_end,
           int              substring,
           AV             **matches)
{
  SV  *string;
  SV  *re;
  SV  *command;
  int  retval;
  int  start;
  int  end;
  dSP;

  /* Set the string in a variable */
  string = get_sv("triggerline", TRUE);
  sv_setpv(string, line);
  SvUTF8_on(string);
  /* Set the regexp in a variable */
  re = get_sv("_re", TRUE);
  sv_setsv(re, pattern_re);
  /* Create the command to run */
  command = newSVpvf("$triggerline =~ tr/\\n\\r//d;"
                     "my $target = %d;"
                     "@__M = ($triggerline =~ m\x05$_re\x05);"
                     "my $match = scalar @__M;"
                     "if ($match) {"
                     "  unshift(@__M, $triggerline);"
                     "}"
                     "(defined($-[$target]) ? $-[$target] : 0,"
                     " defined($+[$target]) ? $+[$target] : 0,"
                     " $match);",
                     (substring != -1 ? substring : 0));
  SvUTF8_on(command);

  /* Run the command */
  ENTER;
  SAVETMPS;
  PUSHMARK(SP);
  eval_sv(command, G_ARRAY);
  SPAGAIN;
  retval = POPi;
  end    = POPi;
  start  = POPi;

  if (matches) {
    if (retval) {
      AV *M = get_av("__M", 0);
      *matches = perl_clone_array(M);
    } else {
      *matches = NULL;
    }
  }

  PUTBACK;
  FREETMPS;
  LEAVE;

  SvREFCNT_dec(command);

  if (match_start) {
    *match_start = start;
  }
  if (match_end) {
    *match_end = end;
  }

  return retval;
}


char *
perl_substitute(const char      *line,
                SV              *pattern_re,
                const char      *substitution,
                int              perl_eval,
                int              global)
{
  SV  *string;
  SV  *re;
  SV  *command;
  int  retval;
  SV  *result;
  dSP;

  /* Set the string in a variable */
  string = get_sv("aliascmd", TRUE);
  sv_setpv(string, line);
  SvUTF8_on(string);
  /* Set the regexp in a variable */
  re = get_sv("_re", TRUE);
  sv_setsv(re, pattern_re);
  /* Create the command to run */
  command = newSVpvf("$__aliasmatched = ($aliascmd =~ s\x05$_re\x05%s\x05%s%s);",
                     substitution,
                     global    ? "g" : "",
                     perl_eval ? "e" : "");
  SvUTF8_on(command);

  /* Run the command */
  ENTER;
  SAVETMPS;
  PUSHMARK(SP);
  eval_sv(command, G_SCALAR);
  SPAGAIN;
  retval = POPi;
  PUTBACK;
  FREETMPS;
  LEAVE;

  SvREFCNT_dec(command);

  if (retval) {
    result = get_sv("aliascmd", FALSE);
    return g_strdup(SvPV_nolen(result));
  } else {
    return NULL;
  }
}


SV *
precompute_re(const char      *re_string,
              int              ignore_case)
{
  SV              *re_int;
  SV              *re_new;
  SV              *command;
  dSP;

  command = newSVpvf("$_re = qr\x05%s\x05%s;",
                     re_string,
                     ignore_case ? "i" : "");
  SvUTF8_on(command);
  ENTER;
  SAVETMPS;
  PUSHMARK(SP);
  eval_sv(command, G_SCALAR);
  SPAGAIN;
  re_int = POPs;
  re_new = newSVsv(re_int);
  PUTBACK;
  FREETMPS;
  LEAVE;

  SvREFCNT_dec(command);

  return re_new;
}


void
perl_set_arg_array(AV *matches)
{
  AV *args = get_av("_", 0);
  av_clear(args);
  perl_copy_array(matches, args);
}


static
AV *
perl_clone_array(AV *orig)
{
  AV *clone;
  int high_elem;

  high_elem = av_len(orig);

  clone = newAV();
  av_extend(clone, high_elem);

  perl_copy_array(orig, clone);
  return clone;
}


static
void
perl_copy_array(AV *source, AV *dest)
{
  int i;
  int highest_elem;

  highest_elem = av_len(source);
  for (i = 0; i <= highest_elem; ++i) {
    SV **val = av_fetch(source, i, 0);
    if (val) {
      SvREFCNT_inc(*val);
      if (!av_store(dest, i, *val)) {
        SvREFCNT_dec(*val);
      }
    }
  }
}


void
parse_server_data(char *protocol,
                  char *data)
{
  SV  *serverdataSV;
  SV  *command;
  dSP;

  /* Set the string in a variable */
  serverdataSV = get_sv("_serverdata", TRUE);
  sv_setpv(serverdataSV, data);
  SvUTF8_on(serverdataSV);
  command = newSVpvf("_parseserverdata(%s)", protocol);

  /* Run the command */
  ENTER;
  SAVETMPS;
  PUSHMARK(SP);
  eval_sv(command, G_VOID);
  SPAGAIN;
  PUTBACK;
  FREETMPS;
  LEAVE;

  SvREFCNT_dec(command);
}


static
GSList *
AVtoGSList(AV *array)
{
  GSList *list = NULL;
  int     i;
  int     max;
  SV    **element;

  /* max is the highest index number of the array */
  max = av_len(array);
  for (i = 0; i <= max; ++i) {
    element = av_fetch(array, i, FALSE);
    if (element) {
      list = g_slist_append(list, g_strdup(SvPV_nolen(*element)));
    }
  }

  return list;
}


void
menu_perl_run_cb(GtkWidget *widget, gpointer data)
{
  char *cmd = (char *) data;
  int   page_num;

  page_num = get_focused_world();
  currentWorld = (World *) g_list_nth_data(open_worlds, page_num);
  world_for_perl = currentWorld;
  GRAB_PERL(world_for_perl);

  eval_pv(cmd, TRUE);

  RELEASE_PERL(world_for_perl);
}


static
World *fetch_world(int items, SV *self_ref, HV **self_to_store)
{
  SV     *self_def;
  HV     *self;
  SV    **worldptrsv;
  World  *world;

  if (!items || !SvROK(self_ref) || !sv_isa(self_ref, "World")) {
    warn(_("Invalid function call: syntax is $world->FUNCTION"));
    return NULL;
  }

  self_def = SvRV(self_ref);
  if (SvTYPE(self_def) != SVt_PVHV) {
    warn(_("Invalid function call: syntax is $world->FUNCTION"));
    return NULL;
  }

  self       = (HV*) self_def;
  worldptrsv = hv_fetch(self, "WORLDPTR", 8, FALSE);
  if (!worldptrsv) {
    warn(_("Invalid function call: syntax is $world->FUNCTION"));
    return NULL;
  }

  world = INT2PTR(World *, SvIV(*worldptrsv));

  if (self_to_store) {
    *self_to_store = self;
  }
  return world;
}


static
void
XS_gettext(pTHX_ CV *perlcv)
{
  dXSARGS;

  if (items == 0) {
    XSRETURN_EMPTY;
  }

  XST_mPV(0, _(SvPV_nolen(ST(0))));
  XSRETURN(1);
}


static
void
XS_ngettext(pTHX_ CV *perlcv)
{
  dXSARGS;

  if (items < 3) {
    XSRETURN_EMPTY;
  }

  XST_mPV(0, ngettext(SvPV_nolen(ST(0)), SvPV_nolen(ST(1)), SvIV(ST(2))));
  XSRETURN(1);
}


static
void
XS_getclientname(pTHX_ CV *perlcv)
{
  dMARK;
  dAX;

  XST_mPV(0, CLIENT_NAME);
  XSRETURN(1);
}


static
void
XS_getversion(pTHX_ CV *perlcv)
{
  dMARK;
  dAX;

  XST_mPV(0, PACKAGE_VERSION);
  XSRETURN(1);
}


static
void
XS_quit(pTHX_ CV *perlcv)
{
  dMARK;
  dAX;

  if (confirm_quit()) {
    gtk_main_quit();
  }

  XSRETURN_EMPTY;
}


static
void
XS_dc(pTHX_ CV *perlcv)
{
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  /* We don't call the function directly because disconnect_world
     can destroy the Perl interpreter if another world is loaded, and
     this would cause a segfault in the function that evaluates
     Perl commands. */
  g_idle_add(disconnect_world_idle, world);

  XSRETURN_EMPTY;
}


static
void
XS_reconnect(pTHX_ CV *perlcv)
{
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  if (world->connected) {
    g_free(close_connection(world, TRUE));
  }

  /* We don't call the function directly because disconnect_world
     can destroy the Perl interpreter if another world is loaded, and
     this would cause a segfault in the function that evaluates
     Perl commands. */
  g_idle_add(reconnect_world, world);

  XSRETURN_EMPTY;
}


static
void
XS_connectother(pTHX_ CV *perlcv)
{
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  if (world->connected) {
    g_free(close_connection(world, TRUE));
  }

  /* We don't call the function directly because disconnect_world
     can destroy the Perl interpreter if another world is loaded, and
     this would cause a segfault in the function that evaluates
     Perl commands. */
  g_idle_add(connect_another, world);

  XSRETURN_EMPTY;
}


static
void
XS_window_minimize(pTHX_ CV *perlcv)
{
  dMARK;
  dAX;

  gtk_window_iconify(GTK_WINDOW(wndMain));

  XSRETURN_EMPTY;
}


static
void
XS_window_getsize(pTHX_ CV *perlcv)
{
  guint cols  = 0;
  guint lines = 0;
  dMARK;
  dAX;

  ansitextview_get_size(world_for_perl->gui, &lines, &cols);

  XST_mIV(1, cols);
  XST_mIV(0, lines);
  XSRETURN(2);
}


static
void
XS_window_settitle(pTHX_ CV *perlcv)
{
  dXSARGS;

  if (items == 1) {
    warn(_("%s: Too few arguments"), "settitle");
    XSRETURN_EMPTY;
  }

  gtk_window_set_title(GTK_WINDOW(wndMain), SvPV_nolen(ST(1)));

  XSRETURN_EMPTY;
}


static
void
XS_window_seturgencyhint(pTHX_ CV *perlcv)
{
  dXSARGS;

  if (items == 1) {
    warn(_("%s: Too few arguments"), "seturgencyhint");
    XSRETURN_EMPTY;
  }

  if (globalPrefs.urgency_hint) {
    gtk_window_set_urgency_hint(GTK_WINDOW(wndMain), SvIV(ST(1)));
  }

  XSRETURN_EMPTY;
}


static
void
XS_next(pTHX_ CV *perlcv)
{
  int num;
  int howmany;
  int n_worlds;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  if (items < 2) {
    howmany = 1;
  } else {
    if (!SvIOK(ST(1))) {
      warn(_("%s: Argument must be numeric"), "next");
      XSRETURN_EMPTY;
    } else {
      howmany = SvIV(ST(1));
    }
  }

  num = g_list_index(open_worlds, world);
  num += howmany;

  n_worlds = g_list_length(open_worlds);
  if (num >= 0) {
    num %= n_worlds;
  } else {
    while (num < 0) {
      num += n_worlds;
    }
  }

  set_focused_world(num);

  XSRETURN_EMPTY;
}


static
void
XS_gotow(pTHX_ CV *perlcv)
{
  int    num;
  GList *worldpos;
  dXSARGS;

  if (items < 1) {
    warn(_("%s: Too few arguments"), "gotow");
    XSRETURN_EMPTY;
  } else {
    if (SvIOK(ST(0))) {
      num = SvIV(ST(0));
    } else {
      worldpos = g_list_find_custom(open_worlds,
                                    SvPV_nolen(ST(0)),
                                    (GCompareFunc) world_name_comparer);
      num = g_list_position(open_worlds, worldpos);
    }
  }

  if (num < 0 || num >= g_list_length(open_worlds)) {
    warn(_("%s: No such world"), "gotow");
    XSRETURN_EMPTY;
  }

  set_focused_world(num);

  XSRETURN_EMPTY;
}


static
void
XS_getworld(pTHX_ CV *perlcv)
{
  GList *worldpos;
  SV    *world_ref;
  dXSARGS;

  if (items < 1) {
    warn(_("%s: Too few arguments"), "getworld");
    XSRETURN_UNDEF;
  } else {
    if (SvIOK(ST(0))) {
      worldpos = g_list_nth(open_worlds, SvIV(ST(0)));
    } else {
      worldpos = g_list_find_custom(open_worlds,
                                    SvPV_nolen(ST(0)),
                                    (GCompareFunc) world_name_comparer);
    }
  }

  if (!worldpos) {
    warn(_("%s: No such world"), "getworld");
    XSRETURN_UNDEF;
  }

  world_ref = create_world_var((World *) worldpos->data);
  ST(0) = world_ref;
  XSRETURN(1);
}


static
int
world_name_comparer(const World *world, const char *name)
{
  return strcmp(world->name, name);
}


static
void
XS_save(pTHX_ CV *perlcv)
{
  gboolean dummy;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  if (!world->file) {
    if (!edit_world(&world, GTK_WINDOW(wndMain), &dummy, TRUE))
      XSRETURN_EMPTY;
  }
  save_world_to_file(world);
  save_permanent_variables(world);

  ansitextview_append_string_nl(world->gui, _("World saved."));

  XSRETURN_EMPTY;
}


static
void
XS_close(pTHX_ CV *perlcv)
{
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  /* We don't call the function directly because remove_world
     can destroy the Perl interpreter if another world is loaded, and
     this would cause a segfault in the function that evaluates
     Perl commands. */
  g_idle_add((GSourceFunc) remove_world_timer, world);

  XSRETURN_EMPTY;
}


static
void
XS_getname(pTHX_ CV *perlcv)
{
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  /* And now return the current value */
  XST_mPV(0, world->name);
  SvUTF8_on(ST(0));
  XSRETURN(1);
}


static
void
XS_getcharacter(pTHX_ CV *perlcv)
{
  GtkTreePath *path;
  GtkTreeIter  iter;
  gchar       *character = NULL;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  if (world->character_used == NULL || world->connection_style == NONE) {
    XSRETURN_UNDEF;
  }

  path = gtk_tree_row_reference_get_path(world->character_used);
  if (!path) {
    gtk_tree_row_reference_free(world->character_used);
    world->character_used = NULL;
    XSRETURN_UNDEF;
  }

  gtk_tree_model_get_iter(world->logon_characters,
                          &iter, path);
  gtk_tree_model_get(world->logon_characters, &iter,
                     LOGON_CHAR, &character,
                     -1);
  XST_mPV(0, character);
  SvUTF8_on(ST(0));
  g_free(character);
  gtk_tree_path_free(path);

  XSRETURN(1);
}


static
void
XS_getmainfont(pTHX_ CV *perlcv)
{
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  /* And now return the current value */
  XST_mPV(0, world->terminalfont);
  SvUTF8_on(ST(0));
  XSRETURN(1);
}


static
void
XS_getentryfont(pTHX_ CV *perlcv)
{
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  /* And now return the current value */
  XST_mPV(0, world->entryfont);
  SvUTF8_on(ST(0));
  XSRETURN(1);
}


static
void
XS_commandecho(pTHX_ CV *perlcv)
{
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  /* If there is an argument, set the option */
  if (items == 2) {
    world->cmd_echo = SvIV(ST(1));
  }

  /* And now return the current value */
  XST_mIV(0, world->cmd_echo);
  XSRETURN(1);
}


static
void
XS_cmdseparator(pTHX_ CV *perlcv)
{
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  /* If there is an argument, set the option */
  if (items == 2) {
    g_strlcpy(world->command_separator, SvPV_nolen(ST(1)),
              sizeof(world->command_separator));
  }

  /* And now return the current value */
  XST_mPV(0, world->command_separator);
  SvUTF8_on(ST(0));
  XSRETURN(1);
}


static
void
XS_getconntime(pTHX_ CV *perlcv)
{
  time_t now;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  now = time(NULL);

  XST_mNV(0, difftime(now, world->connected_time));
  XSRETURN(1);
}


static
void
XS_getidletime(pTHX_ CV *perlcv)
{
  time_t now;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  now = time(NULL);

  XST_mNV(0, difftime(now, world->last_command_time));
  XSRETURN(1);
}


static
void
XS_echo(pTHX_ CV *perlcv)
{
  int     i;
  char   *word;
  STRLEN  len;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  for (i = 1; i < items; ++i) {
    word = SvPV(ST(i), len);
    if (world && world->gui->txtView) {
      AnsiParseOperation *operation = g_new(AnsiParseOperation, 1);
      operation->action = APPEND_ECHOED_LINE;
      operation->line.line = g_strdup(word);
      operation->line.line_len = len;
      queue_ansiparse_operation(world, operation);
    } else {
      puts(word);
    }
  }

  XSRETURN_EMPTY;
}


static
void
XS_send(pTHX_ CV *perlcv)
{
  int     i;
  char   *word;
  STRLEN  len;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  for (i = 1; i < items; ++i) {
    word = SvPV(ST(i), len);
    send_to_world(world, word, len);
  }

  XSRETURN_EMPTY;
}


static
void
XS__mlsend(pTHX_ CV *perlcv)
{
  HV         *attributes;
  SV        **value;
  AV         *AVinitialtext = NULL;
  AV         *AVfinaltext   = NULL;
  char       *file          = NULL;
  gdouble     delay;
  MLSendData *mlcontext;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  /* Check argument */
  if (items < 2) {
    warn(_("%s: This is an internal function, do not call directly."), "_mlsend");
    XSRETURN_UNDEF;
  }
  if (!SvROK(ST(1)) || SvTYPE(SvRV(ST(1))) != SVt_PVHV) {
    warn(_("%s: Wrong argument type"), "_mlsend");
    XSRETURN_EMPTY;
  }

  /* Fetch attributes */
  attributes = (HV *) SvRV(ST(1));

  /* See if everthing is alright before starting to allocate memory */
  value = hv_fetch(attributes, "initialtext", 11, FALSE);
  if (value) {
    if (!SvROK(*value) || SvTYPE(SvRV(*value)) != SVt_PVAV) {
      warn(_("%s: This is an internal function, do not call directly."), "_mlsend");
      XSRETURN_EMPTY;
    } else {
      AVinitialtext = (AV *) SvRV(*value);
    }
  }

  value = hv_fetch(attributes, "finaltext", 9, FALSE);
  if (value) {
    if (!SvROK(*value) || SvTYPE(SvRV(*value)) != SVt_PVAV) {
      warn(_("%s: This is an internal function, do not call directly."), "_mlsend");
      XSRETURN_EMPTY;
    } else {
      AVfinaltext = (AV *) SvRV(*value);
    }
  }

  value = hv_fetch(attributes, "file", 4, FALSE);
  if (value) {
    FILE *fp;

    file = SvPV_nolen(*value);
    /* See if file can be read */
    if ((fp = fopen(file, "r")) == NULL) {
      XSRETURN_NO;
    }
    fclose(fp);
  }

  /* No problems - let's prepare the structure for ML Send. */
  mlcontext = calloc(1, sizeof(MLSendData));
  mlcontext->state = ML_TRY_INITIALLINES;
  mlcontext->world = world;

  if (AVinitialtext) {
    mlcontext->linesInitial = AVtoGSList(AVinitialtext);
  }
  if (AVfinaltext) {
    mlcontext->linesFinal = AVtoGSList(AVfinaltext);
  }
  if (file) {
    mlcontext->file = g_strdup(file);
  }

  /* Fetch other attributes */
  value = hv_fetch(attributes, "linestart", 9, FALSE);
  if (value) {
    mlcontext->textStart = g_strdup(SvPV_nolen(*value));
  } else {
    mlcontext->textStart = g_strdup("");
  }

  value = hv_fetch(attributes, "lineend", 7, FALSE);
  if (value) {
    mlcontext->textEnd = g_strdup(SvPV_nolen(*value));
  } else {
    mlcontext->textEnd = g_strdup("");
  }

  value = hv_fetch(attributes, "delay", 5, FALSE);
  if (value) {
    delay = SvNV(*value);
  } else {
    delay = globalPrefs.multi_cmd_delay;
  }

  value = hv_fetch(attributes, "linesatime", 10, FALSE);
  if (value) {
    mlcontext->linesatime = SvIV(*value);
  } else {
    mlcontext->linesatime = globalPrefs.multi_cmd_group_size;
  }

  do_multi_line_send(mlcontext, delay);

  XSRETURN_YES;
}


static
void
XS_interpret(pTHX_ CV *perlcv)
{
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  if (items == 1) {
    warn(_("%s: Too few arguments"), "interpret");
    XSRETURN_EMPTY;
  }

  parse_commands(world, SvPV_nolen(ST(1)));

  XSRETURN_EMPTY;
}


static
void
XS_getinput(pTHX_ CV *perlcv)
{
  const gchar *text;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  text = simo_combo_box_get_text(world->gui->cmbEntry);

  XST_mPV(0, text);
  SvUTF8_on(ST(0));
  XSRETURN(1);
}


static
void
XS_setinput(pTHX_ CV *perlcv)
{
  char *input = "";
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  if (items > 1) {
    input = SvPV_nolen(ST(1));
  }

  simo_combo_box_set_text(world->gui->cmbEntry, input);

  XSRETURN_EMPTY;
}


static
void
XS_getline(pTHX_ CV *perlcv)
{
  int          line;
  int          last_line;
  GtkTextIter  start_iter;
  GtkTextIter  end_iter;
  gchar       *text;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  if (items == 1) {
    warn(_("%s: Too few arguments"), "getline");
    XSRETURN_EMPTY;
  }

  line = SvIV(ST(1));
  last_line = gtk_text_buffer_get_line_count(world->gui->txtBuffer);
  if (line < 0) {
    /* Negative: nth line from the bottom */
    line = last_line - (-line) - 1;
  } else {
    /* Positive: line number */
    line = line - world->deleted_lines - 1;

    if (line > last_line) {
      XSRETURN_EMPTY;
    }
  }

  if (line < 0) {
    XSRETURN_EMPTY;
  }

  gtk_text_buffer_get_iter_at_line(world->gui->txtBuffer,
                                   &start_iter, line);
  end_iter = start_iter;
  gtk_text_iter_forward_to_line_end(&end_iter);
  text = gtk_text_buffer_get_text(world->gui->txtBuffer,
                                  &start_iter, &end_iter,
                                  FALSE);

  XST_mPV(0, text);
  SvUTF8_on(ST(0));
  g_free(text);
  if (GIMME_V == G_ARRAY) {
    XST_mIV(1, k_circular_queue_nth(world->gui->line_times,
                                    time_t,
                                    line));
    XSRETURN(2);
  } else {
    XSRETURN(1);
  }
}


static
void
XS__sendserverdata(pTHX_ CV *perlcv)
{
  char   *protocol;
  char   *data;
  STRLEN  len;
  unsigned char telnet_code[3];
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  if (items < 3) {
    warn(_("%s: This is an internal function, do not call directly."),
         "_sendserverdata");
    XSRETURN_EMPTY;
  }

  protocol = SvPV_nolen(ST(1));
  data = SvPV(ST(2), len);

  if (strcmp(protocol, "GMCP") == 0) {
    if (!world->gmcp_enabled) {
      warn(_("Protocol %s not enabled."), protocol);
      XSRETURN_EMPTY;
    }
    telnet_code[2] = TELNET_OPT_GMCP;
  } else if (strcmp(protocol, "MSDP") == 0) {
    if (!world->msdp_enabled) {
      warn(_("Protocol %s not enabled."), protocol);
      XSRETURN_EMPTY;
    }
    telnet_code[2] = TELNET_OPT_MSDP;
  } else {
    warn(_("Unknown protocol"));
    XSRETURN_EMPTY;
  }

  telnet_code[0] = TELNET_IAC;
  telnet_code[1] = TELNET_SB;
  kc_send(world, telnet_code, 3);
  kc_send(world, data, len);
  /* telnet_code[0] = TELNET_IAC; - Already set */
  telnet_code[1] = TELNET_SE;
  kc_send(world, telnet_code, 2);

  XSRETURN_EMPTY;
}


static
void
XS_play(pTHX_ CV *perlcv)
{
  dXSARGS;

  if (items == 0) {
    warn(_("%s: Too few arguments"), "play");
    XSRETURN_EMPTY;
  }

  play_sound(SvPV_nolen(ST(0)));

  XSRETURN_EMPTY;
}


static
void
XS_stripansi(pTHX_ CV *perlcv)
{
  char   *original;
  STRLEN  len;
  char   *stripped;
  dXSARGS;

  if (items == 0) {
    warn(_("%s: Too few arguments"), "stripansi");
    XSRETURN_EMPTY;
  }

  original = SvPV(ST(0), len);
  stripped = strip_ansi(original, len);

  XST_mPV(0, stripped);
  SvUTF8_on(ST(0));
  g_free(stripped);
  XSRETURN(1);
}


static
void
XS_setstatus(pTHX_ CV *perlcv)
{
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  if (items == 1) {
    warn(_("%s: Too few arguments"), "setstatus");
    XSRETURN_EMPTY;
  }

  gtk_label_set_text(world->gui->lblStatus, SvPV_nolen(ST(1)));

  XSRETURN_EMPTY;
}


static
void
XS_trigger(pTHX_ CV *perlcv)
{
  int       i              = -1;
  int       argpos         = 1;
  int       have_pattern   = 0;
  int       have_action    = 0;
  int       need_recompute = 0;
  HV       *attributes;
  Trigger  *trigger;
  SV      **is_silent;
  FETCHWORLDVARSFULL;
  dXSARGS;

  FETCHWORLDFULL;

  if (items < 2) {
    warn(_("%s: Too few arguments"),  "trigger");
    XSRETURN_EMPTY;
  }

  /* See if first argument is an integer */
  if (SvIOK(ST(1))) {
    i = SvIV(ST(1));
    argpos = 2;
    if (items < 3) {
      warn(_("%s: Too few arguments"),  "trigger");
      XSRETURN_EMPTY;
    }
  }

  if (i == -1) {
    trigger = new_trigger();
    trigger->enabled = 1;       /* Starts enabled by default */
    if (world->plugins_being_defined) {
      trigger->owner_plugin = world->plugins_being_defined->data;
    }
  } else {
    trigger = (Trigger *) g_slist_nth_data(world->triggers, i);
    if (!trigger) {
      ansitextview_append_string_nl(world->gui, _("No such trigger"));
      XSRETURN_EMPTY;
    }
  }

  while (argpos < items) {
    switch (SvTYPE(ST(argpos))) {
    case SVt_PV:
      if (!have_pattern) {
        g_free(trigger->pattern);
        trigger->pattern = g_strdup(SvPV_nolen(ST(argpos)));
        need_recompute = TRUE;
        have_pattern = 1;
      } else if (!have_action) {
        g_free(trigger->action);
        trigger->action = g_strdup(SvPV_nolen(ST(argpos)));
        have_action = 1;
      } else {
        warn(_("%s: Too many string arguments"), "trigger");
        if (i == -1) {
          g_free(trigger);
        }
        XSRETURN_EMPTY;
      }
      break;

    case SVt_RV:
      if (SvTYPE(SvRV(ST(argpos))) == SVt_PVHV) {
        SV **value;

        attributes = (HV *) SvRV(ST(argpos));

        value = hv_fetch(attributes, "name", 4, FALSE);
        if (value) {
          g_free(trigger->name);
          trigger->name = g_strdup(SvPV_nolen(*value));
        }

        value = hv_fetch(attributes, "pattern", 7, FALSE);
        if (value) {
          if (have_pattern) {
            warn(_("%s: Pattern given twice, using the named attribute one"),
                 "trigger");
          }
          g_free(trigger->pattern);
          trigger->pattern = g_strdup(SvPV_nolen(*value));
          need_recompute = TRUE;
          have_pattern = 1;
        }

        value = hv_fetch(attributes, "action", 6, FALSE);
        if (value) {
          if (have_action) {
            warn(_("%s: Action given twice, using the named attribute one"),
                 "trigger");
          }
          g_free(trigger->action);
          trigger->action = g_strdup(SvPV_nolen(*value));
          have_action = 1;
        }

        value = hv_fetch(attributes, "ignorecase", 10, FALSE);
        if (value) {
          int newval = SvIV(*value);

          need_recompute |= (newval != trigger->ignore_case);
          trigger->ignore_case = newval;
        }

        value = hv_fetch(attributes, "gag", 3, FALSE);
        if (value) {
          trigger->gag_output = SvIV(*value);
        }

        value = hv_fetch(attributes, "gaglog", 6, FALSE);
        if (value) {
          trigger->gag_log = SvIV(*value);
        }

        value = hv_fetch(attributes, "enabled", 7, FALSE);
        if (value) {
          trigger->enabled = SvIV(*value);
        }

        value = hv_fetch(attributes, "keepexecuting", 13, FALSE);
        if (value) {
          trigger->keepexecuting = SvIV(*value);
        }

        value = hv_fetch(attributes, "rewriter", 8, FALSE);
        if (value) {
          trigger->rewriter = SvIV(*value);
        }

        value = hv_fetch(attributes, "style", 5, FALSE);
        if (value) {
          fill_highlight(trigger, (HV *) SvRV(*value));
        }
      } else {
        warn(_("%s: Wrong type of reference"),  "trigger");
        if (i == -1) {
          g_free(trigger);
        }
        XSRETURN_EMPTY;
      }
      break;

    default:
      warn(_("%s: Wrong argument type"),  "trigger");
      if (i == -1) {
        g_free(trigger);
      }
      XSRETURN_EMPTY;
    }
    ++argpos;
  }

  if (i == -1) {
    if (!have_pattern) {
      warn(_("%s: Pattern not specified, not creating"),
           "trigger");
      if (i == -1) {
        g_free(trigger);
      }
      XSRETURN_EMPTY;
    }

    if (!have_action && !trigger->gag_output && !trigger->gag_log) {
      warn(_("%s: Action not specified, and isn't gag, not creating"),
           "trigger");
      if (i == -1) {
        g_free(trigger);
      }
      XSRETURN_EMPTY;
    }
  }

  if (need_recompute) {
    if (trigger->pattern_re) {
      SvREFCNT_dec(trigger->pattern_re);
    }
    trigger->pattern_re = precompute_re(trigger->pattern,
                                        trigger->ignore_case);
  }

  is_silent = hv_fetch(self, "SILENT", 6, FALSE);
  if (i == -1) {
    if (trigger->owner_plugin) {
      world->triggers = g_slist_append(world->triggers, trigger);
      we_trigger_insert_trigger(world, trigger,
                                g_slist_length(world->triggers) - 1);
    } else {
      world->triggers = g_slist_insert(world->triggers,
                                       trigger,
                                       world->trigger_pos);
      we_trigger_insert_trigger(world, trigger, world->trigger_pos++);
      if (!is_silent || !SvIV(*is_silent)) {
        ansitextview_append_string_nl(world->gui, _("Trigger added."));
      }
    }
  } else {
    if (!is_silent || !SvIV(*is_silent)) {
      ansitextview_append_string_nl(world->gui, _("Trigger modified."));
    }
    we_trigger_update_trigger(world, trigger);
  }

  XSRETURN_EMPTY;
}


static
void
fill_highlight(Trigger *trigger, HV *style)
{
  SV **value;
  int  ival;

  value = hv_fetch(style, "enabled", 7, FALSE);
  if (value) {
    trigger->highlight = SvIV(*value);
  }

  value = hv_fetch(style, "target", 6, FALSE);
  if (value) {
    ival = SvIV(*value);
    if (ival < -1) {
      warn(_("%s: Invalid target"), "trigger");
    } else {
      trigger->high_target = ival;
    }
  }

  value = hv_fetch(style, "fg", 2, FALSE);
  if (value) {
    ival = SvIV(*value);
    if (ival < -1 || ival > ANSI_N_COLORS_HIGHLIGHT) {
      warn(_("%s: Invalid fg color"), "trigger");
    } else {
      trigger->high_fg_color = ival;
    }
  }

  value = hv_fetch(style, "bg", 2, FALSE);
  if (value) {
    ival = SvIV(*value);
    if (ival < -1 || ival > ANSI_N_COLORS_HIGHLIGHT) {
      warn(_("%s: Invalid bg color"), "trigger");
    } else {
      trigger->high_bg_color = ival;
    }
  }

  value = hv_fetch(style, "underline", 9, FALSE);
  if (value) {
    ival = SvIV(*value);
    if (ival < -1 || ival > 2) {
      warn(_("%s: Invalid underline style"), "trigger");
    } else {
      trigger->high_underline = ival;
    }
  }

  value = hv_fetch(style, "italics", 7, FALSE);
  if (value) {
    ival = SvIV(*value);
    if (ival < 0) {
      trigger->high_italic = -1;
    } else {
      /* So that it becomes 0 or 1 */
      trigger->high_italic = !!ival;
    }
  }

  value = hv_fetch(style, "strike", 6, FALSE);
  if (value) {
    ival = SvIV(*value);
    if (ival < 0) {
      trigger->high_strike = -1;
    } else {
      /* So that it becomes 0 or 1 */
      trigger->high_strike = !!ival;
    }
  }
}


static
void
XS__listtrigger(pTHX_ CV *perlcv)
{
  /* Only accepts numbers. For internal use only. */
  int      i;
  Trigger *trigger;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  if (items == 1) {
    ansitextview_append_string(world->gui, "\n");
    list_triggers(world, NULL);
    XSRETURN_EMPTY;
  }

  i = SvIV(ST(1));
  trigger = (Trigger *) g_slist_nth_data(world->triggers, i);
  if (!trigger) {
    ansitextview_append_string_nl(world->gui, _("No such trigger"));
    XSRETURN_NO;
  }

  ansitextview_append_stringf(world->gui,
                              _("Trigger number %d\n"
                                "  Name:           %s\n"
                                "  Pattern:        %s\n"
                                "  Action:         %s\n"
                                "  Enabled:        %s\n"
                                "  Ignore case:    %s\n"
                                "  Gag output:     %s\n"
                                "  Gag in log:     %s\n"
                                "  Keep executing: %s\n"
                                "  Rewriter:       %s\n"
                                "  Change style:   %s\n"),
                              i,
                              trigger->name ? trigger->name : _("Not set"),
                              trigger->pattern,
                              trigger->action ? trigger->action : "",
                              trigger->ignore_case   ? _("yes") : _("no"),
                              trigger->gag_output    ? _("yes") : _("no"),
                              trigger->gag_log       ? _("yes") : _("no"),
                              trigger->enabled       ? _("yes") : _("no"),
                              trigger->keepexecuting ? _("yes") : _("no"),
                              trigger->rewriter      ? _("yes") : _("no"),
                              trigger->highlight     ? _("yes") : _("no"));
  if (trigger->highlight) {
    ansitextview_append_stringf(world->gui,
                                _("    Target:       %d\n"
                                  "    Foreground:   %d\n"
                                  "    Background:   %d\n"
                                  "    Italics:      %d\n"
                                  "    Striked-thru: %d\n"
                                  "    Underline:    %d\n"),
                                trigger->high_target,
                                trigger->high_fg_color,
                                trigger->high_bg_color,
                                trigger->high_italic,
                                trigger->high_strike,
                                trigger->high_underline);
  }

  XSRETURN_YES;
}


static
void
XS__deltrigger(pTHX_ CV *perlcv)
{
  /* Only accepts numbers. For internal use only. */
  int      i;
  GSList  *triggeritem;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  i = SvIV(ST(1));
  triggeritem = g_slist_nth(world->triggers, i);
  if (!triggeritem) {
    XSRETURN_NO;
  }

  remove_trigger(world, triggeritem);
  XSRETURN_YES;
}


static
void
XS__movetrigger(pTHX_ CV *perlcv)
{
  /* Only accepts numbers. For internal use only. */
  int old_pos;
  int new_pos;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  old_pos = SvIV(ST(1));
  new_pos = SvIV(ST(2));

  if (new_pos < 0 || new_pos >= world->trigger_pos) {
    new_pos = world->trigger_pos - 1;
  }

  if (move_trigger(world, old_pos, new_pos)) {
    XSRETURN_YES;
  } else {
    XSRETURN_NO;
  }
}


static
void
XS__getobjectnumber(pTHX_ CV *perlcv)
{
  /* Not to be called by user.
     Arguments: name to match
                object type (0: trigger, 1: alias, 2: macro, 3: timer, 4:hook)
                search type (0: by name, 1: by owner plugin)
                [optional] hook event name, if we are dealing with a hook
  */
  GSList *tofind = NULL;
  GSList *found;
  GSList *iter;
  int     i;
  int     searchtype;
  FETCHWORLDVARS;
  dXSARGS;

  searchtype = SvIV(ST(3));

  FETCHWORLD;

  switch (SvIV(ST(2))) {
  case 0:
    tofind = world->triggers;
    break;

  case 1:
    tofind = world->aliases;
    break;

  case 2:
    tofind = world->macros;
    break;

  case 3:
    tofind = world->timers;
    break;

  case 4:
    tofind = get_hook_list_or_warn_if_nonexistent(world, SvPV_nolen(ST(4)));
    if (!tofind) {
      XSRETURN(0);
    }
    break;
  }

  found = get_object_numbers(tofind, SvPV_nolen(ST(1)), searchtype);

  i = 0;
  iter = found;
  while (iter) {
    XST_mIV(i++, GPOINTER_TO_INT(iter->data));

    iter = iter->next;
  }

  g_slist_free(found);

  XSRETURN(i);
}

static
void
XS__objectenabled(pTHX_ CV *perlcv)
{
  /* Not to be called by user.
     Arguments: name or number of object to match
                object type (0: trigger, 1: alias, 2: macro, 3: timer, 4:hook)
                [optional] hook event name, if we are dealing with a hook
     Returns: TRUE/FALSE if object is enabled. If there are several with
              the same name, returns a list.
  */
  GSList        *tofind = NULL;
  GeneralObject *obj;
  int            i;
  int            j;
  int            num_to_find;
  char          *name_to_find = NULL;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  switch (SvIV(ST(2))) {
  case 0:
    tofind = world->triggers;
    break;

  case 1:
    tofind = world->aliases;
    break;

  case 2:
    tofind = world->macros;
    break;

  case 3:
    tofind = world->timers;
    break;

  case 4:
    tofind = get_hook_list_or_warn_if_nonexistent(world, SvPV_nolen(ST(3)));
    if (!tofind) {
      XSRETURN(0);
    }
    break;
  }

  if (SvIOK(ST(1))) {
    num_to_find = SvIV(ST(1));
    if (num_to_find < 0) {
      XSRETURN_EMPTY;
    }
  } else {
    num_to_find = -1;
    name_to_find = SvPV_nolen(ST(1));
  }

  i = 0;
  j = 0;
  while (tofind) {
    obj = (GeneralObject *) tofind->data;
    if (num_to_find != -1 && num_to_find == i) {
      XST_mIV(0, obj->enabled);
      XSRETURN(1);
    }

    if (num_to_find == -1
        && obj->name && strcmp(name_to_find, obj->name) == 0) {
      XST_mIV(j, obj->enabled);
      ++j;
    }

    ++i;
    tofind = tofind->next;
  }

  XSRETURN(j);
}


static
void
XS_alias(pTHX_ CV *perlcv)
{
  int     i                 = -1;
  int     argpos            = 1;
  int     have_pattern      = 0;
  int     have_substitution = 0;
  int     need_recompute    = 0;
  HV     *attributes;
  Alias  *alias;
  SV    **is_silent;
  FETCHWORLDVARSFULL;
  dXSARGS;

  FETCHWORLDFULL;

  if (items < 2) {
    warn(_("%s: Too few arguments"), "alias");
    XSRETURN_EMPTY;
  }

  /* See if first argument is an integer */
  if (SvIOK(ST(1))) {
    i = SvIV(ST(1));
    argpos = 2;
    if (items < 3) {
      warn(_("%s: Too few arguments"), "alias");
      XSRETURN_EMPTY;
    }
  }

  if (i == -1) {
    alias = g_new0(Alias, 1);
    alias->enabled = 1;       /* Starts enabled by default */
    if (world->plugins_being_defined) {
      alias->owner_plugin = world->plugins_being_defined->data;
    }
  } else {
    alias = (Alias *) g_slist_nth_data(world->aliases, i);
    if (!alias) {
      ansitextview_append_string_nl(world->gui, _("No such alias"));
      XSRETURN_EMPTY;
    }
  }

  while (argpos < items) {
    switch (SvTYPE(ST(argpos))) {
    case SVt_PV:
      if (!have_pattern) {
        g_free(alias->pattern);
        alias->pattern = g_strdup(SvPV_nolen(ST(argpos)));
        need_recompute = 1;
        have_pattern = 1;
      } else if (!have_substitution) {
        g_free(alias->substitution);
        alias->substitution = g_strdup(SvPV_nolen(ST(argpos)));
        have_substitution = 1;
      } else {
        warn(_("%s: Too many string arguments"), "alias");
        if (i == -1) {
          g_free(alias);
        }
        XSRETURN_EMPTY;
      }
      break;

    case SVt_RV:
      if (SvTYPE(SvRV(ST(argpos))) == SVt_PVHV) {
        SV** value;

        attributes = (HV *) SvRV(ST(argpos));

        value = hv_fetch(attributes, "name", 4, FALSE);
        if (value) {
          g_free(alias->name);
          alias->name = g_strdup(SvPV_nolen(*value));
        }

        value = hv_fetch(attributes, "pattern", 7, FALSE);
        if (value) {
          if (have_pattern) {
            warn(_("%s: Pattern given twice, using the named attribute one"),
                 "alias");
          }
          g_free(alias->pattern);
          alias->pattern = g_strdup(SvPV_nolen(*value));
          need_recompute = 1;
          have_pattern = 1;
        }

        value = hv_fetch(attributes, "substitution", 12, FALSE);
        if (value) {
          if (have_substitution) {
            warn(_("%s: Substitution given twice, using the named attribute one"),
                 "alias");
          }
          g_free(alias->substitution);
          alias->substitution = g_strdup(SvPV_nolen(*value));
          have_substitution = 1;
        }

        value = hv_fetch(attributes, "ignorecase", 10, FALSE);
        if (value) {
          int newval = SvIV(*value);

          need_recompute |= (newval != alias->ignore_case);
          alias->ignore_case = newval;
        }

        value = hv_fetch(attributes, "perleval", 8, FALSE);
        if (value) {
          alias->perl_eval = SvIV(*value);
        }

        value = hv_fetch(attributes, "enabled", 7, FALSE);
        if (value) {
          alias->enabled = SvIV(*value);
        }
      } else {
        warn(_("%s: Wrong type of reference"), "alias");
        if (i == -1) {
          g_free(alias);
        }
        XSRETURN_EMPTY;
      }
      break;

    default:
      warn(_("%s: Wrong argument type"),  "alias");
      if (i == -1) {
        g_free(alias);
      }
      XSRETURN_EMPTY;
    }
    ++argpos;
  }

  if (i == -1) {
    if (!have_pattern) {
      warn(_("alias: Not creating alias without pattern"));
      if (i == -1) {
        g_free(alias);
      }
      XSRETURN_EMPTY;
    }

    if (!have_substitution) {
      warn(_("alias: Not creating alias without substitution"));
      if (i == -1) {
        g_free(alias);
      }
      XSRETURN_EMPTY;
    }
  }

  if (need_recompute) {
    if (alias->pattern_re) {
      SvREFCNT_dec(alias->pattern_re);
    }
    alias->pattern_re = precompute_re(alias->pattern,
                                      alias->ignore_case);
  }

  is_silent = hv_fetch(self, "SILENT", 6, FALSE);
  if (i == -1) {
    if (alias->owner_plugin) {
      world->aliases = g_slist_append(world->aliases, alias);
      we_alias_insert_alias(world, alias,
                            g_slist_length(world->aliases) - 1);
    } else {
      world->aliases = g_slist_insert(world->aliases,
                                      alias,
                                      world->alias_pos);
      we_alias_insert_alias(world, alias, world->alias_pos++);
      if (!is_silent || !SvIV(*is_silent)) {
        ansitextview_append_string_nl(world->gui, _("Alias added."));
      }
    }
  } else {
    if (!is_silent || !SvIV(*is_silent)) {
      ansitextview_append_string_nl(world->gui, _("Alias modified."));
    }
    we_alias_update_alias(world, alias);
  }

  XSRETURN_EMPTY;
}


static
void
XS_expandalias(pTHX_ CV *perlcv)
{
  GSList *aliasptr;
  Alias  *alias;
  char   *line;
  char   *resultline;
  SV     *re;
  SV     *tmp;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  if (items == 1) {
    warn(_("%s: Too few arguments"), "expandalias");
    XSRETURN_EMPTY;
  }

  line = SvPV_nolen(ST(1));
  tmp = get_sv("_tmpexpalias", TRUE);
  SvUTF8_on(tmp);
  sv_setpv(tmp, line);

  aliasptr = world->aliases;
  while (aliasptr) {
    SV *subcmd;

    alias = (Alias *) aliasptr->data;

    if (!alias->enabled) {
      break;
    }

    /* Set the regexp in a variable */
    re = get_sv("_re", TRUE);
    sv_setsv(re, alias->pattern_re);

    subcmd = newSVpvf("$_tmpexpalias =~ s\x05$_re\x05%s\x05%s;",
                      alias->substitution,
                      alias->perl_eval ? "e" : "");
    SvUTF8_on(subcmd);
    eval_sv(subcmd, G_VOID);
    SvREFCNT_dec(subcmd);

    aliasptr = aliasptr->next;
  }

  resultline = SvPV_nolen(tmp);
  XST_mPV(0, resultline);
  SvUTF8_on(ST(0));
  XSRETURN(1);
}


static
void
XS__listalias(pTHX_ CV *perlcv)
{
  /* Only accepts numbers. For internal use only. */
  int     i;
  Alias  *alias;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  if (items == 1) {
    ansitextview_append_string(world->gui, "\n");
    list_aliases(world, NULL);
    XSRETURN_EMPTY;
  }

  i = SvIV(ST(1));
  alias = (Alias *) g_slist_nth_data(world->aliases, i);
  if (!alias) {
    ansitextview_append_string_nl(world->gui, _("No such alias"));
    XSRETURN_NO;
  }

  ansitextview_append_stringf(world->gui,
                              _("Alias number %d\n"
                                "  Name:         %s\n"
                                "  Pattern:      %s\n"
                                "  Substitution: %s\n"
                                "  Ignore case:  %s\n"
                                "  Eval as Perl: %s\n"
                                "  Enabled:      %s\n"),
                              i,
                              alias->name ? alias->name : _("Not set"),
                              alias->pattern,
                              alias->substitution,
                              alias->ignore_case ? _("yes") : _("no"),
                              alias->perl_eval   ? _("yes") : _("no"),
                              alias->enabled     ? _("yes") : _("no"));

  XSRETURN_YES;
}


static
void
XS__delalias(pTHX_ CV *perlcv)
{
  /* Only accepts numbers. For internal use only. */
  int      i;
  GSList  *aliasitem;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  i = SvIV(ST(1));
  aliasitem = g_slist_nth(world->aliases, i);
  if (!aliasitem) {
    XSRETURN_NO;
  }

  remove_alias(world, aliasitem);

  XSRETURN_YES;
}


static
void
XS__movealias(pTHX_ CV *perlcv)
{
  /* Only accepts numbers. For internal use only. */
  int old_pos;
  int new_pos;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  old_pos = SvIV(ST(1));
  new_pos = SvIV(ST(2));

  if (new_pos < 0 || new_pos >= world->alias_pos) {
    new_pos = world->alias_pos - 1;
  }

  if (move_alias(world, old_pos, new_pos)) {
    XSRETURN_YES;
  } else {
    XSRETURN_NO;
  }
}


static
void
XS_getkeycode(pTHX_ CV *perlcv)
{
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  ansitextview_append_string_nl(world->gui,
                                _("Press a key to get its keycode."));
  world->print_next_keycode = TRUE;
  gtk_main();
  /* After gtk_main_quit() is called in the event handler... */
  XST_mPV(0, world->keycode_to_print);
  SvUTF8_on(ST(0));
  g_free(world->keycode_to_print);

  XSRETURN(1);
}


static
void
XS_macro(pTHX_ CV *perlcv)
{
  int     i = -1;
  int     have_key    = 0;
  int     have_action = 0;
  int     argpos      = 1;
  HV     *attributes;
  Macro  *macro;
  SV    **is_silent;
  FETCHWORLDVARSFULL;
  dXSARGS;

  FETCHWORLDFULL;

  if (items < 1) {
    warn(_("%s: Too few arguments"), "macro");
    XSRETURN_EMPTY;
  }

  /* See if first argument is an integer */
  if (SvIOK(ST(1))) {
    i = SvIV(ST(1));
    argpos = 2;
    if (items < 3) {
      warn(_("%s: Too few arguments"), "macro");
      XSRETURN_EMPTY;
    }
  }

  if (i == -1) {
    macro = g_new0(Macro, 1);
    macro->enabled = 1;
    if (world->plugins_being_defined) {
      macro->owner_plugin = world->plugins_being_defined->data;
    }
  } else {
    macro = (Macro *) g_slist_nth_data(world->macros, i);
    if (!macro) {
      ansitextview_append_string_nl(world->gui, _("No such macro"));
      XSRETURN_EMPTY;
    }
  }

  while (argpos < items) {
    switch (SvTYPE(ST(argpos))) {
    case SVt_PV:
      if (!have_key) {
        guint           keyval;
        GdkModifierType modifiers;

        gtk_accelerator_parse(SvPV_nolen(ST(argpos)),
                              &keyval, &modifiers);
        if (keyval) {
          macro->keyval    = keyval;
          macro->modifiers = modifiers;
          have_key = 1;
        } else {
          warn(_("%s: Invalid key code"), "macro");
          if (i == -1) {
            g_free(macro);
          }
          XSRETURN_EMPTY;
        }
      } else if (!have_action) {
        g_free(macro->action);
        macro->action = g_strdup(SvPV_nolen(ST(argpos)));
        have_action = 1;
      } else {
        warn(_("%s: Too many string arguments"), "macro");
        if (i == -1) {
          g_free(macro);
        }
        XSRETURN_EMPTY;
      }
      break;

    case SVt_RV:
      if (SvTYPE(SvRV(ST(argpos))) == SVt_PVHV) {
        SV** value;

        attributes = (HV *) SvRV(ST(argpos));

        value = hv_fetch(attributes, "name", 4, FALSE);
        if (value) {
          g_free(macro->name);
          macro->name = g_strdup(SvPV_nolen(*value));
        }

        value = hv_fetch(attributes, "key", 3, FALSE);
        if (value) {
          guint           keyval;
          GdkModifierType modifiers;

          if (have_key) {
            warn(_("%s: Keycode given twice, using the named attribute one"),
                 "macro");
          }

          gtk_accelerator_parse(SvPV_nolen(*value),
                                &keyval, &modifiers);
          if (keyval) {
            macro->keyval    = keyval;
            macro->modifiers = modifiers;
            have_key = 1;
          } else {
            warn(_("%s: Invalid key code"), "macro");
            if (i == -1) {
              g_free(macro);
            }
            XSRETURN_EMPTY;
          }
          have_key = 1;
        }

        value = hv_fetch(attributes, "action", 6, FALSE);
        if (value) {
          if (have_action) {
            warn(_("%s: Action given twice, using the named attribute one"),
                 "macro");
          }
          g_free(macro->action);
          macro->action = g_strdup(SvPV_nolen(*value));
          have_action = 1;
        }

        value = hv_fetch(attributes, "enabled", 7, FALSE);
        if (value) {
          macro->enabled = SvIV(*value);
        }
      } else {
        warn(_("%s: Wrong type of reference"), "macro");
        if (i == -1) {
          g_free(macro);
        }
        XSRETURN_EMPTY;
      }
      break;

    default:
      warn(_("%s: Wrong argument type"),  "macro");
      if (i == -1) {
        g_free(macro);
      }
      XSRETURN_EMPTY;
    }
    ++argpos;
  }

  /* Verification of required arguments */
  if (i == -1 && !have_key) {
    warn(_("%s: Keycode not specified, not creating"), "macro");
    if (i == -1) {
      g_free(macro);
    }
    XSRETURN_EMPTY;
  }
  if (i == -1 && !have_action) {
    warn(_("%s: Action not specified, not creating"), "macro");
    if (i == -1) {
      g_free(macro);
    }
    XSRETURN_EMPTY;
  }

  is_silent = hv_fetch(self, "SILENT", 6, FALSE);
  if (i == -1) {
    if (macro->owner_plugin) {
      world->macros = g_slist_append(world->macros, macro);
      we_macro_insert_macro(world, macro,
                            g_slist_length(world->macros) - 1);
    } else {
      world->macros = g_slist_insert(world->macros,
                                     macro,
                                     world->macro_pos);
      we_macro_insert_macro(world, macro, world->macro_pos++);
      if (!is_silent || !SvIV(*is_silent)) {
        ansitextview_append_string_nl(world->gui, _("Macro added."));
      }
    }
  } else {
    if (!is_silent || !SvIV(*is_silent)) {
      ansitextview_append_string_nl(world->gui, _("Macro modified."));
    }
    we_macro_update_macro(world, macro);
  }

  XSRETURN_EMPTY;
}


static
void
XS__listmacro(pTHX_ CV *perlcv)
{
  /* Only accepts numbers. For internal use only. */
  int     i;
  Macro  *macro;
  char   *keycodestr;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  if (items == 1) {
    ansitextview_append_string(world->gui, "\n");
    list_macros(world, NULL);
    XSRETURN_EMPTY;
  }

  i = SvIV(ST(1));
  macro = (Macro *) g_slist_nth_data(world->macros, i);
  if (!macro) {
    ansitextview_append_string_nl(world->gui, _("No such macro"));
    XSRETURN_NO;
  }

  keycodestr = gtk_accelerator_name(macro->keyval, macro->modifiers);
  ansitextview_append_stringf(world->gui,
                              _("Macro number %d\n"
                                "  Name:     %s\n"
                                "  Key code: %s\n"
                                "  Action:   %s\n"
                                "  Enabled:  %s\n"),
                              i,
                              macro->name ? macro->name : _("Not set"),
                              keycodestr,
                              macro->action,
                              macro->enabled ? _("yes") : _("no"));
  g_free(keycodestr);

  XSRETURN_YES;
}


static
void
XS__delmacro(pTHX_ CV *perlcv)
{
  /* Only accepts numbers. For internal use only. */
  int      i;
  GSList  *macroitem;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  i = SvIV(ST(1));
  macroitem = g_slist_nth(world->macros, i);
  if (!macroitem) {
    XSRETURN_NO;
  }

  remove_macro(world, macroitem);

  XSRETURN_YES;
}


static
void
XS__movemacro(pTHX_ CV *perlcv)
{
  /* Only accepts numbers. For internal use only. */
  int old_pos;
  int new_pos;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  old_pos = SvIV(ST(1));
  new_pos = SvIV(ST(2));

  if (new_pos < 0 || new_pos >= world->macro_pos) {
    new_pos = world->macro_pos - 1;
  }

  if (move_macro(world, old_pos, new_pos)) {
    XSRETURN_YES;
  } else {
    XSRETURN_NO;
  }
}


static
void
XS_timer(pTHX_ CV *perlcv)
{
  int      i = -1;
  int      argpos = 1;
  gdouble  new_interval = 0;
  int      new_enabled = -999;
  HV      *attributes;
  SV     **value;
  Timer   *timer;
  SV    **is_silent;
  FETCHWORLDVARSFULL;
  dXSARGS;

  FETCHWORLDFULL;

  if (items < 2) {
    warn(_("%s: Too few arguments"), "timer");
    XSRETURN_EMPTY;
  }

  /* See if first argument is an integer */
  if (SvIOK(ST(1))) {
    i = SvIV(ST(1));
    argpos = 2;
    if (items < 3) {
      warn(_("%s: Too few arguments"), "timer");
      XSRETURN_EMPTY;
    }
  }

  if (i == -1) {
    timer = g_new0(Timer, 1);
    timer->for_world = world;
    timer->count = -1;
    if (world->plugins_being_defined) {
      timer->owner_plugin = world->plugins_being_defined->data;
    }
  } else {
    timer = (Timer *) g_slist_nth_data(world->timers, i);
    if (!timer) {
      ansitextview_append_string_nl(world->gui, _("No such timer"));
      XSRETURN_EMPTY;
    }
  }

  if (!SvROK(ST(argpos)) || SvTYPE(SvRV(ST(argpos))) != SVt_PVHV) {
    warn(_("%s: Wrong argument type"), "timer");
    if (i == -1) {
      g_free(timer);
    }
    XSRETURN_EMPTY;
  }

  /* Fetch attributes */
  attributes = (HV *) SvRV(ST(argpos));

  value = hv_fetch(attributes, "name", 4, FALSE);
  if (value) {
    g_free(timer->name);
    timer->name = g_strdup(SvPV_nolen(*value));
  }

  value = hv_fetch(attributes, "interval", 8, FALSE);
  if (value) {
    if (!SvNIOK(*value)) {
      warn(_("%s: Interval must be numeric"), "timer");
      if (i == -1) {
        g_free(timer);
      }
      XSRETURN_EMPTY;
    }

    new_interval = SvNV(*value);
  }

  value = hv_fetch(attributes, "count", 5, FALSE);
  if (value) {
    if (!SvIOK(*value)) {
      warn(_("%s: Count must be numeric"), "timer");
      if (i == -1) {
        g_free(timer);
      }
      XSRETURN_EMPTY;
    }

    timer->count = SvIV(*value);
    if (timer->count < 0) {
      timer->count = -1;
    }
  }

  value = hv_fetch(attributes, "action", 6, FALSE);
  if (value) {
    g_free(timer->action);
    timer->action = g_strdup(SvPV_nolen(*value));
  }

  value = hv_fetch(attributes, "temporary", 9, FALSE);
  if (value) {
    timer->temporary = SvIV(*value);
  }

  value = hv_fetch(attributes, "enabled", 7, FALSE);
  if (value) {
    new_enabled = SvIV(*value);
  }

  /* Verification of required arguments */
  if (new_interval == 0 && i == -1) {
    warn(_("%s: Interval not specified, not creating"), "timer");
    if (i == -1) {
      g_free(timer);
    }
    XSRETURN_EMPTY;
  }
  if (new_interval < 0) {
    warn(_("%s: Cannot create timer with negative interval"), "timer");
    if (i == -1) {
      g_free(timer);
    }
    XSRETURN_EMPTY;
  }

  /* Recreate timer function if interval is changed */
  if (i != -1 && new_enabled == -999 &&
      new_interval && new_interval != timer->interval) {
    g_source_remove(timer->function_id);
    timer->function_id = g_timeout_add(new_interval * 1000,
                                       run_timer,
                                       timer);
  }

  if (new_interval) {
    timer->interval = new_interval;
  }

  if (!timer->action) {
    warn(_("%s: Action not specified, not creating"), "timer");
    if (i == -1) {
      g_free(timer);
    }
    XSRETURN_EMPTY;
  }

  /* See if we are enabling or disabling */
  if (i == -1 && new_enabled == -999) {         /* New timers enabled */
    new_enabled = 1;                            /* by default         */
  }

  if (new_enabled != -999) {
    if (timer->enabled && !new_enabled) {
      g_source_remove(timer->function_id);
    } else if (!timer->enabled && new_enabled) {
      timer->function_id = g_timeout_add(timer->interval * 1000,
                                         run_timer,
                                         timer);
    }
  }
  timer->enabled = new_enabled;

  /* A little bonus :-) */
  if (timer->enabled && timer->count == 0) {
    timer->count = 1;
  }

  is_silent = hv_fetch(self, "SILENT", 6, FALSE);
  if (i == -1) {
    if (timer->owner_plugin) {
      world->timers = g_slist_append(world->timers, timer);
      we_timer_insert_timer(world, timer,
                            g_slist_length(world->timers) - 1);
    } else {
      world->timers = g_slist_insert(world->timers,
                                     timer,
                                     world->timer_pos);
      we_timer_insert_timer(world, timer, world->timer_pos++);
      if (!is_silent || !SvIV(*is_silent)) {
        ansitextview_append_string_nl(world->gui, _("Timer added."));
      }
    }
  } else {
    if (!is_silent || !SvIV(*is_silent)) {
      ansitextview_append_string_nl(world->gui, _("Timer modified."));
    }
    we_timer_update_timer(world, timer);
  }

  XSRETURN_EMPTY;
}


static
void
XS__listtimer(pTHX_ CV *perlcv)
{
  /* Only accepts numbers. For internal use only. */
  int     i;
  Timer  *timer;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  if (items == 1) {
    ansitextview_append_string(world->gui, "\n");
    list_timers(world, NULL);
    XSRETURN_EMPTY;
  }

  i = SvIV(ST(1));
  timer = (Timer *) g_slist_nth_data(world->timers, i);
  if (!timer) {
    ansitextview_append_string_nl(world->gui, _("No such timer"));
    XSRETURN_NO;
  }

  ansitextview_append_stringf(world->gui,
                              _("Timer number %d\n"
                                "  Name:         %s\n"
                                "  Interval:     %.3f\n"
                                "  Repeat count: %d\n"
                                "  Action:       %s\n"
                                "  Enabled:      %s\n"
                                "  Temporary:    %s\n"),
                              i,
                              timer->name ? timer->name : _("Not set"),
                              timer->interval,
                              timer->count,
                              timer->action,
                              timer->enabled ? _("yes") : _("no"),
                              timer->temporary ? _("yes") : _("no"));

  XSRETURN_YES;
}


static
void
XS__deltimer(pTHX_ CV *perlcv)
{
  /* Only accepts numbers. For internal use only. */
  int      i;
  GSList  *timeritem;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  i = SvIV(ST(1));
  timeritem = g_slist_nth(world->timers, i);
  if (!timeritem) {
    XSRETURN_NO;
  }

  remove_timer(world, timeritem);

  XSRETURN_YES;
}


static
void
XS__movetimer(pTHX_ CV *perlcv)
{
  /* Only accepts numbers. For internal use only. */
  int old_pos;
  int new_pos;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  old_pos = SvIV(ST(1));
  new_pos = SvIV(ST(2));

  if (new_pos < 0 || new_pos >= world->timer_pos) {
    new_pos = world->timer_pos - 1;
  }

  if (move_timer(world, old_pos, new_pos)) {
    XSRETURN_YES;
  } else {
    XSRETURN_NO;
  }
}


static
void
XS_makepermanent(pTHX_ CV *perlcv)
{
  char  *var;
  int    i;
  SV   **is_silent;
  FETCHWORLDVARSFULL;
  dXSARGS;

  FETCHWORLDFULL;

  if (items < 2) {
    warn(_("%s: Too few arguments"), "makepermanent");
    XSRETURN_EMPTY;
  }

  is_silent = hv_fetch(self, "SILENT", 6, FALSE);
  for (i = 1; i < items; ++i) {
    var = SvPV_nolen(ST(i));
    if (var[0] != '$' && var[0] != '@' && var[0] != '%') {
      warn(_("%s: Only scalar ($), array (@) and hash (%%) variables can be made permanent"),
           "makepermanent");
      continue;
    }

    if (g_slist_find_custom(world->permanent_variables,
                            var,
                            (GCompareFunc) strcmp)) {
      if (!world->plugins_being_defined) {
        warn(_("%s: %s is already permanent"),
             "makepermanent", var);
      }
      continue;
    }

    var = g_strdup(var);
    world->permanent_variables =
      g_slist_append(world->permanent_variables, var);
    we_var_insert_var(world, var, -1);

    if (!world->plugins_being_defined
        && (!is_silent || !SvIV(*is_silent))) {
      ansitextview_append_stringf(world->gui,
                                  _("%s: Variable '%s' made permanent.\n"),
                                  "makepermanent", var);
    }
  }

  XSRETURN_EMPTY;
}


static
void
XS_maketemporary(pTHX_ CV *perlcv)
{
  char    *var;
  int      i;
  GSList  *list;
  SV     **is_silent;
  FETCHWORLDVARSFULL;
  dXSARGS;

  FETCHWORLDFULL;

  if (items < 2) {
    warn(_("%s: Too few arguments"), "maketemporary");
    XSRETURN_EMPTY;
  }

  is_silent = hv_fetch(self, "SILENT", 6, FALSE);
  for (i = 1; i < items; ++i) {
    var = SvPV_nolen(ST(i));

    list = g_slist_find_custom(world->permanent_variables,
                               var,
                               (GCompareFunc) strcmp);
    if (list) {
      we_var_delete_var(world, (const gchar *) list->data);
      g_free(list->data);
      world->permanent_variables =
        g_slist_delete_link(world->permanent_variables, list);

      if (!is_silent || !SvIV(*is_silent)) {
        ansitextview_append_stringf(world->gui,
                                    _("%s: Variable '%s' made temporary.\n"),
                                    "maketemporary", var);
      }
    } else {
      warn(_("%s: %s is not permanent"),
           "maketemporary", var);
      continue;
    }
  }

  XSRETURN_EMPTY;
}


static
void
XS_ispermanent(pTHX_ CV *perlcv)
{
  char    *var;
  GSList  *list;
  FETCHWORLDVARSFULL;
  dXSARGS;

  FETCHWORLDFULL;

  if (items < 2) {
    warn(_("%s: Too few arguments"), "ispermanent");
    XSRETURN_EMPTY;
  }

  var = SvPV_nolen(ST(1));

  list = g_slist_find_custom(world->permanent_variables,
                             var,
                             (GCompareFunc) strcmp);
  if (list) {
    XST_mIV(0, TRUE);
  } else {
    XST_mIV(0, FALSE);
  }

  XSRETURN(1);
}


static
void
XS_listpermanent(pTHX_ CV *perlcv)
{
  GSList *var;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  ansitextview_append_string(world->gui, "\n");
  ansitextview_append_string_nl(world->gui,
                                _("Permanent variables"));
  ansitextview_append_string_nl(world->gui, "-------------------");

  var = world->permanent_variables;
  while (var) {
    ansitextview_append_string(world->gui, "  ");
    ansitextview_append_string_nl(world->gui, (char *) var->data);

    var = var->next;
  }

  XSRETURN_EMPTY;
}


static
void
XS_hook(pTHX_ CV *perlcv)
{
  int   i           = -1;
  char *hook        = NULL;
  int   argpos      = 2;
  int   have_action = 0;
  char *action      = NULL;
  char *name        = NULL;
  int   enabled     = 1;
  HV   *attributes;
  SV  **is_silent;
  FETCHWORLDVARSFULL;
  dXSARGS;

  FETCHWORLDFULL;

  if (items < 3) {
    warn(_("%s: Too few arguments"), "hook");
    XSRETURN_EMPTY;
  }

  hook = SvPV_nolen(ST(1));

  /* See if first argument is an integer */
  if (SvIOK(ST(2))) {
    i = SvIV(ST(2));
    argpos = 3;
    if (items < 4) {
      warn(_("%s: Too few arguments"), "hook");
      XSRETURN_EMPTY;
    }
  }

  while (argpos < items) {
    switch (SvTYPE(ST(argpos))) {
    case SVt_PV:
      if (!have_action) {
        action = g_strdup(SvPV_nolen(ST(argpos)));
        have_action = 1;
      } else {
        warn(_("%s: Too many string arguments"), "hook");
        g_free(action);
        XSRETURN_EMPTY;
      }
      break;

    case SVt_RV:
      if (SvTYPE(SvRV(ST(argpos))) == SVt_PVHV) {
        SV** value;

        attributes = (HV *) SvRV(ST(argpos));

        value = hv_fetch(attributes, "name", 4, FALSE);
        if (value) {
          name = g_strdup(SvPV_nolen(*value));
        }

        value = hv_fetch(attributes, "action", 6, FALSE);
        if (value) {
          if (have_action) {
            warn(_("%s: Action given twice, using the named attribute one"),
                 "hook");
            g_free(action);
          }
          action = g_strdup(SvPV_nolen(*value));
          have_action = 1;
        }

        value = hv_fetch(attributes, "enabled", 7, FALSE);
        if (value) {
          enabled = SvIV(*value);
        }
      } else {
        warn(_("%s: Wrong type of reference"), "hook");
        g_free(action);
        g_free(name);
        XSRETURN_EMPTY;
      }
      break;

    default:
      warn(_("%s: Wrong argument type"),  "hook");
      g_free(action);
      g_free(name);
      XSRETURN_EMPTY;
    }
    ++argpos;
  }

  is_silent = hv_fetch(self, "SILENT", 6, FALSE);
  if (connect_hook(world, hook, i,
                   action, name, enabled)) {
    if (i == -1) {
      if (!world->plugins_being_defined
          && (!is_silent || !SvIV(*is_silent))) {
        ansitextview_append_string_nl(world->gui, _("Hook defined."));
      }
    } else {
      if (!is_silent || !SvIV(*is_silent)) {
        ansitextview_append_string_nl(world->gui, _("Hook modified."));
      }
    }
  }

  XSRETURN_EMPTY;
}


static
void
XS__listhook(pTHX_ CV *perlcv)
{
  /* Only accepts numbers. For internal use only. */
  int i;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  if (items < 2) {
    warn(_("%s: Too few arguments"), "listhook");
    XSRETURN_EMPTY;
  }

  if (items == 2) {
    ansitextview_append_string(world->gui, "\n");
    list_hooks(world, SvPV_nolen(ST(1)), NULL);
    XSRETURN_EMPTY;
  }

  i = SvIV(ST(2));
  list_hook(world, SvPV_nolen(ST(1)), i);

  XSRETURN_EMPTY;
}


static
void
XS__delhook(pTHX_ CV *perlcv)
{
  /* Only accepts numbers. For internal use only. */
  int   i;
  char *hookname;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  hookname = SvPV_nolen(ST(1));
  i = SvIV(ST(2));
  if (delete_hook(world, hookname, i, NULL)) {
    XSRETURN_YES;
  } else {
    XSRETURN_NO;
  }
}


static
void
XS__movehook(pTHX_ CV *perlcv)
{
  /* Only accepts numbers. For internal use only. */
  char *hookname;
  int   old_pos;
  int   new_pos;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  hookname = SvPV_nolen(ST(1));
  old_pos  = SvIV(ST(2));
  new_pos  = SvIV(ST(3));

  if (move_hook(world, hookname, old_pos, new_pos)) {
    XSRETURN_YES;
  } else {
    XSRETURN_NO;
  }
}

static
void
XS_logfile(pTHX_ CV *perlcv)
{
  char *tmp;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  /* This will be overwritten if there are two arguments. */
  world->log_add_time = FALSE;

  /* If logging is started, it must be finished to start again
     in the new file and/or with the new format. */
  if (world->log_file) {
    stop_log(world);
  }

  switch (items) {
  case 1:
    ansitextview_append_string_nl(world->gui, _("Logging stopped."));
    break;

  case 3:
    world->log_add_time = TRUE;
    tmp = SvPV_nolen(ST(2));
    g_free(world->log_timeformat);
    world->log_timeformat = g_strdup(tmp);
    /* FALL THROUGH */

  case 2:
    tmp = SvPV_nolen(ST(1));
    g_free(world->log_file_name);
    world->log_file_name = g_strdup(tmp);

    start_log_and_display_result(world);
    break;
  }

  fill_we_logging_tab(world, NULL);

  XSRETURN_EMPTY;
}


static
void
XS_getlogfile(pTHX_ CV *perlcv)
{
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  if (world->log_file) {
    XST_mPV(0, world->log_actual_file);
    SvUTF8_on(ST(0));
    XSRETURN(1);
  } else {
    XSRETURN_UNDEF;
  }
}


static
void
XS__writetolog(pTHX_ CV *perlcv)
{
  /* This function simply writes the line, with color codes and
     ANSI codes, if present. It is not meant to be called by the user. */
  char *line;
  int   i;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  for (i = 1; i < items; ++i) {
    line = SvPV_nolen(ST(i));
    write_log_line(world, line, NULL);
  }

  XSRETURN_EMPTY;
}


static
GSList *
get_object_numbers(GSList *list, const char *name, int searchtype)
{
 /* To avoid the need of four virtually equal functions, for triggers,
    aliases, macros and timers, this function is a hack that works for
    all of them. It exploits the fact that the four structures have
    the name as the first member and owner plugin as second, and uses
    some casting to retrieve it.
  */
  GSList        *found = NULL;
  GeneralObject *obj;
  char          *tried_name = NULL;
  int            i = 0;

  while (list) {
    obj = (GeneralObject *) list->data;
    if (searchtype == 0) {
      tried_name = obj->name;
    } else if (searchtype == 1) {
      if (obj->owner_plugin) {
        tried_name = obj->owner_plugin->name;
      } else {
        tried_name = NULL;
      }
    }

    if (tried_name && strcmp(name, tried_name) == 0) {
      found = g_slist_append(found, GINT_TO_POINTER(i));
    }

    ++i;
    list = list->next;
  }

  return found;
}


static
void
XS_listplugin(pTHX_ CV *perlcv)
{
  guint    i;
  guint    total_width;
  guint    field1_width;
  guint    field2_width;
  GSList  *pluginptr;
  Plugin  *plugin;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  if (items == 1) {
    /* The rows argument is not used. */
    ansitextview_get_size(world->gui, &field1_width, &total_width);

    /* If the screen is really narrow, we can do nothing. */
    if (total_width < 35) {
      total_width = 35;
    }
    field1_width = (total_width - 17)*.3;
    field2_width = (total_width - 17)*.7;

    ansitextview_append_stringf(world->gui,
                                _("Num Ena %-*.*s Version %-*.*s\n"),
                                field1_width, field1_width, _("Name"),
                                field2_width, field2_width, _("Description"));
    ansitextview_append_string(world->gui, "--- --- ");
    for (i = 0; i < field1_width; ++i) {
      ansitextview_append_string(world->gui, "-");
    }
    ansitextview_append_string(world->gui, " ------- ");
    for (i = 0; i < field2_width; ++i) {
      ansitextview_append_string(world->gui, "-");
    }
    ansitextview_append_string(world->gui, "\n");

    i = 0;
    pluginptr = world->plugins;
    while (pluginptr) {
      plugin = (Plugin *) pluginptr->data;

      ansitextview_append_stringf(world->gui,
                                  "%3d %-3.3s %-*.*s %-7.7s %-*.*s\n",
                                  i,
                                  plugin->enabled ? _("y") : _("n"),
                                  field1_width, field1_width,
                                  plugin->name,
                                  plugin->version,
                                  field2_width, field2_width,
                                  plugin->description);
      ++i;
      pluginptr = pluginptr->next;
    }

    XSRETURN_EMPTY;
  }

  if (SvIOK(ST(1))) {
    i = SvIV(ST(1));
    plugin = (Plugin *) g_slist_nth_data(world->plugins, i);
  } else {
    plugin = get_plugin(world, SvPV_nolen(ST(1)));
    i = g_slist_index(world->plugins, plugin);
  }

  if (!plugin) {
    ansitextview_append_string_nl(world->gui, _("No such plugin"));
    XSRETURN_NO;
  }

  ansitextview_append_stringf(world->gui,
                              _("Plugin number %d\n"
                                "  Name:        %s\n"
                                "  Version:     %s\n"
                                "  Description: %s\n"
                                "  Author:      %s\n"
                                "  Enabled:     %s\n"),
                              i,
                              plugin->name,
                              plugin->version,
                              plugin->description,
                              plugin->author,
                              plugin->enabled ? _("yes") : _("no"));

  ansitextview_append_string_nl(world->gui, "\nPlugin triggers:");
  list_triggers(world, plugin);

  ansitextview_append_string_nl(world->gui, "\nPlugin aliases:");
  list_aliases(world, plugin);

  ansitextview_append_string_nl(world->gui, "\nPlugin macros:");
  list_macros(world, plugin);

  ansitextview_append_string_nl(world->gui, "\nPlugin timers:");
  list_timers(world, plugin);

  ansitextview_append_string_nl(world->gui, "\nPlugin hooks:");
  list_hooks(world, "OnConnect",        plugin);
  ansitextview_append_string(world->gui, "\n");
  list_hooks(world, "OnDisconnect",     plugin);
  ansitextview_append_string(world->gui, "\n");
  list_hooks(world, "OnReceivedText",   plugin);
  ansitextview_append_string(world->gui, "\n");
  list_hooks(world, "OnSentCommand",    plugin);
  ansitextview_append_string(world->gui, "\n");
  list_hooks(world, "OnGetFocus",       plugin);
  ansitextview_append_string(world->gui, "\n");
  list_hooks(world, "OnLoseFocus",      plugin);
  ansitextview_append_string(world->gui, "\n");
  list_hooks(world, "OnCloseConnected", plugin);
  ansitextview_append_string(world->gui, "\n");
  list_hooks(world, "OnServerData", plugin);

  XSRETURN_YES;
}


static
void
XS_getpluginversion(pTHX_ CV *perlcv)
{
  /* Argument: Plugin name.
     Returns: Plugin version, or empty string.
  */
  Plugin *plugin;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  if (items != 2) {
    warn(_("%s: Too few arguments"), "getpluginversion");
  }

  plugin = get_plugin(world, SvPV_nolen(ST(1)));
  if (!plugin) {
    XST_mPV(0, "");
  } else {
    XST_mPV(0, plugin->version);
  }
  SvUTF8_on(ST(0));

  XSRETURN(1);
}


static
void
XS__startplugin(pTHX_ CV *perlcv)
{
  /* Not meant to be called by user.
     Arguments: name, version, description, author. */
  Plugin *plugin;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  plugin = g_new(Plugin, 1);

  plugin->name        = g_strdup(SvPV_nolen(ST(1)));
  plugin->version     = g_strdup(SvPV_nolen(ST(2)));
  plugin->description = g_strdup(SvPV_nolen(ST(3)));
  plugin->author      = g_strdup(SvPV_nolen(ST(4)));
  plugin->enabled     = TRUE;

  world->plugins_being_defined = g_slist_prepend(world->plugins_being_defined,
                                                 plugin);

  XSRETURN_EMPTY;
}


static
void
XS__stopplugin(pTHX_ CV *perlcv)
{
  /* Not meant to be called by user.
     Argument: 0 to cancel, the plugin was not loaded
               1 to add it to list, the plugin was loaded
  */
  Plugin *plugin;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  plugin = (Plugin *) world->plugins_being_defined->data;
  world->plugins_being_defined = g_slist_remove(world->plugins_being_defined,
                                                plugin);

  if (SvTRUE(ST(1))) {
    world->plugins = g_slist_append(world->plugins,
                                    plugin);
    we_lplugin_insert_plugin(world, plugin);
  } else {
    remove_plugin_objects(world, plugin);

    g_free(plugin);
  }

  XSRETURN_EMPTY;
}


static
void
XS__markpluginenadis(pTHX_ CV *perlcv)
{
  /* Not meant to be called by user.
     Arguments: plugin name, new value. */
  Plugin *plugin;
  FETCHWORLDVARS;
  dXSARGS;

  FETCHWORLD;

  plugin = get_plugin(world, SvPV_nolen(ST(1)));
  plugin->enabled = SvIV(ST(2));

  we_lplugin_update_plugin(world, plugin);

  XSRETURN_EMPTY;
}


static
void
xs_init()
{
  /* This is for dynamic loading of modules */
  char *file = __FILE__;
  dXSUB_SYS;

  /* DynaLoader is a special case */
  newXS("DynaLoader::boot_DynaLoader", boot_DynaLoader, file);


  /* Our subroutines */
  newXS("gettext",  XS_gettext,  "");
  newXS("ngettext", XS_ngettext, "");

  newXS("getclientname", XS_getclientname, "");
  newXS("getversion",    XS_getversion,    "");
  newXS("quit",          XS_quit,          "");

  newXS("gotow",    XS_gotow,    "");
  newXS("getworld", XS_getworld, "");

  newXS("play", XS_play, "");

  newXS("stripansi", XS_stripansi, "");


  newXS("Window::minimize",       XS_window_minimize,       "Window");
  newXS("Window::getsize",        XS_window_getsize,        "Window");
  newXS("Window::settitle",       XS_window_settitle,       "Window");
  newXS("Window::seturgencyhint", XS_window_seturgencyhint, "Window");


  newXS("World::dc",           XS_dc,           "World");
  newXS("World::reconnect",    XS_reconnect,    "World");
  newXS("World::connectother", XS_connectother, "World");
  newXS("World::save",         XS_save,         "World");
  newXS("World::close",        XS_close,        "World");
  newXS("World::next",         XS_next,         "World");

  newXS("World::getname",      XS_getname,      "World");
  newXS("World::getcharacter", XS_getcharacter, "World");
  newXS("World::getmainfont",  XS_getmainfont,  "World");
  newXS("World::getentryfont", XS_getentryfont, "World");
  newXS("World::commandecho",  XS_commandecho,  "World");
  newXS("World::cmdseparator", XS_cmdseparator, "World");
  newXS("World::getconntime",  XS_getconntime,  "World");
  newXS("World::getidletime",  XS_getidletime,  "World");

  newXS("World::echo",            XS_echo,            "World");
  newXS("World::send",            XS_send,            "World");
  newXS("World::_mlsend",         XS__mlsend,         "World");
  newXS("World::interpret",       XS_interpret,       "World");
  newXS("World::getinput",        XS_getinput,        "World");
  newXS("World::setinput",        XS_setinput,        "World");
  newXS("World::getline",         XS_getline,         "World");
  newXS("World::_sendserverdata", XS__sendserverdata, "World");

  newXS("World::setstatus", XS_setstatus, "World");

  newXS("World::trigger",          XS_trigger,          "World");
  newXS("World::_listtrigger",     XS__listtrigger,     "World");
  newXS("World::_deltrigger",      XS__deltrigger,      "World");
  newXS("World::_movetrigger",     XS__movetrigger,     "World");
  newXS("World::_getobjectnumber", XS__getobjectnumber, "World");
  newXS("World::_objectenabled",   XS__objectenabled,   "World");

  newXS("World::alias",       XS_alias,       "World");
  newXS("World::expandalias", XS_expandalias, "World");
  newXS("World::_listalias",  XS__listalias,  "World");
  newXS("World::_delalias",   XS__delalias,   "World");
  newXS("World::_movealias",  XS__movealias,  "World");

  newXS("World::getkeycode", XS_getkeycode, "World");
  newXS("World::macro",      XS_macro,      "World");
  newXS("World::_listmacro", XS__listmacro, "World");
  newXS("World::_delmacro",  XS__delmacro,  "World");
  newXS("World::_movemacro", XS__movemacro, "World");

  newXS("World::timer",      XS_timer,      "World");
  newXS("World::_listtimer", XS__listtimer, "World");
  newXS("World::_deltimer",  XS__deltimer,  "World");
  newXS("World::_movetimer", XS__movetimer, "World");

  newXS("World::makepermanent", XS_makepermanent, "World");
  newXS("World::maketemporary", XS_maketemporary, "World");
  newXS("World::ispermanent",   XS_ispermanent,   "World");
  newXS("World::listpermanent", XS_listpermanent, "World");

  newXS("World::hook",      XS_hook,      "World");
  newXS("World::_listhook", XS__listhook, "World");
  newXS("World::_delhook",  XS__delhook,  "World");
  newXS("World::_movehook", XS__movehook, "World");

  newXS("World::logfile",     XS_logfile,     "World");
  newXS("World::getlogfile",  XS_getlogfile,  "World");
  newXS("World::_writetolog", XS__writetolog, "World");

  newXS("World::listplugin",        XS_listplugin,        "World");
  newXS("World::getpluginversion",  XS_getpluginversion,  "World");
  newXS("World::_startplugin",      XS__startplugin,      "World");
  newXS("World::_stopplugin",       XS__stopplugin,       "World");
  newXS("World::_markpluginenadis", XS__markpluginenadis, "World");

  /* KCWin subroutines */
  newXS("KCWin::_new",     XS_KCWin__new,     "KCWin");
  newXS("KCWin::feed",     XS_KCWin_feed,     "KCWin");
  newXS("KCWin::_destroy", XS_KCWin__destroy, "KCWin");
}
