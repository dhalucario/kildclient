/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include <ctype.h>
#include <sys/stat.h>
#include "libintl-wrapper.h"
#include <gtk/gtk.h>

#include "kildclient.h"
#include "perlscript.h"


/********************
 * Type definitions *
 ********************/
enum {
  COLUMN_NAME,
  COLUMN_WIDGET,
  N_COLUMNS,
};


/***********************
 * Function prototypes *
 ***********************/
static void          create_edit_world_dialog(World *world,
                                              GtkWindow  *parent);
static GtkTreeStore *create_model(GtkStack   *stack,
                                  World      *world,
                                  GtkBuilder *ui_builder);
static void          list_selection_changed_cb(GtkTreeSelection *selection,
                                               gpointer data);
static GtkWidget    *new_descriptive_panel(const char *title,
                                           const char *message);
/* XML UI signals */
G_MODULE_EXPORT void world_editor_response_cb(GtkWidget *dialog,
                                              gint       response,
                                              gpointer   data);


/*************************
 * File global variables *
 *************************/


gboolean
edit_world(World     **world,
           GtkWindow  *parent,
           gboolean   *newworld,
           gboolean    modal)
{
  int response;

  *newworld = FALSE;

  if (!*world) {
    *world = create_new_world(TRUE);
  }

  if (!((*world)->dlgEditWorld)) {
    create_edit_world_dialog(*world, parent);
    gtk_widget_show_all((*world)->dlgEditWorld);
  }
  g_object_set_data(G_OBJECT((*world)->dlgEditWorld),
                    "newworldptr", newworld);

  gtk_window_present(GTK_WINDOW((*world)->dlgEditWorld));

  if (modal) {
    response = gtk_dialog_run(GTK_DIALOG((*world)->dlgEditWorld));

    gtk_widget_destroy((*world)->dlgEditWorld);
    (*world)->dlgEditWorld = NULL;
    g_object_unref((*world)->ui_builder);
    (*world)->ui_builder = NULL;

    return (response == GTK_RESPONSE_OK);
  }

  return TRUE;
}


void
world_editor_response_cb(GtkWidget *dialog, gint response, gpointer data)
{
  World      *world = (World *) data;
  GtkBuilder *ui_builder;
  gboolean   *newworld;

  ui_builder = world->ui_builder;

  if (response == GTK_RESPONSE_OK) {
    if (world != default_world) {
      update_we_general_parameters(world, ui_builder);
    }
    update_we_mainwindow_parameters(world, ui_builder);
    update_we_input_parameters(world, ui_builder);
    update_we_logging_parameters(world, ui_builder);
    if (world != default_world) {
      update_we_scripting_parameters(world, ui_builder);
    }
    update_we_protocols_parameters(world, ui_builder);
    newworld = g_object_get_data(G_OBJECT(dialog), "newworldptr");
    update_we_advanced_parameters(world, newworld, ui_builder);

    /* In case the name or display name was changed, we must update the tab */
    if (world->gui) {
      prepare_display_name(world);
      if (world->has_unread_text) {
        gtk_label_set_text(world->gui->lblNotebook, world->new_text_name);
      } else {
        gtk_label_set_text(world->gui->lblNotebook, world->display_name);
      }
      /* The window title gets update automatically because when the world
         editor is closed, the main window gets focus. */
    }
  } else {
    /* Revert parameters that might have been changed */
    if (world != default_world) {
      fill_we_general_tab(world, ui_builder);
    }
    fill_we_input_tab(world, ui_builder);
    fill_we_logging_tab(world, ui_builder);
    if (world != default_world) {
      fill_we_scripting_tab(world, ui_builder);
    }
    fill_we_protocols_tab(world, ui_builder);
    fill_we_advanced_tab(world, ui_builder);
  }

  gtk_widget_hide(dialog);
}


static
void
create_edit_world_dialog(World *world, GtkWindow *parent)
{
  GError      *error = NULL;
  GtkTreeView *treeView;
  GtkStack    *stack;

  GtkTreeStore      *model;
  GtkCellRenderer   *renderer;
  GtkTreeViewColumn *column;
  GtkTreeSelection  *selection;
  GtkTreeIter        iter;

  /* Create the dialog */
  if (!world->ui_builder) {
    world->ui_builder = gtk_builder_new();
  }
  if (!gtk_builder_add_from_resource(world->ui_builder,
                                     "/ekalin/kildclient/dlgEditWorld.ui",
                                     &error)) {
    g_warning(_("Error loading UI from XML file: %s"), error->message);
    g_error_free(error);
  }

  world->dlgEditWorld = GTK_WIDGET(gtk_builder_get_object(world->ui_builder,
                                                          "dlgEditWorld"));
  gtk_window_set_transient_for(GTK_WINDOW(world->dlgEditWorld),
                               GTK_WINDOW(wndMain));
  gtk_dialog_set_default_response(GTK_DIALOG(world->dlgEditWorld),
                                  GTK_RESPONSE_OK);

  stack = GTK_STACK(gtk_builder_get_object(world->ui_builder, "stackWEPanel"));

  model = create_model(stack, world, world->ui_builder);
  g_object_set_data(G_OBJECT(model), "stackptr", stack);
  treeView = GTK_TREE_VIEW(gtk_builder_get_object(world->ui_builder,
                                                  "treeViewWE"));
  gtk_tree_view_set_model(treeView, GTK_TREE_MODEL(model));
  g_object_unref(G_OBJECT(model));
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new_with_attributes(NULL,
                                                    renderer,
                                                    "text",
                                                    COLUMN_NAME,
                                                    NULL);
  gtk_tree_view_append_column(treeView, column);
  gtk_tree_view_set_headers_visible(treeView, FALSE);

  selection = gtk_tree_view_get_selection(treeView);
  g_signal_connect(G_OBJECT(selection), "changed",
                   G_CALLBACK(list_selection_changed_cb), treeView);

  gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
  gtk_tree_selection_select_iter(selection, &iter);

  gtk_builder_connect_signals(world->ui_builder, world);
}


static
GtkTreeStore *
create_model(GtkStack   *stack,
             World      *world,
             GtkBuilder *ui_builder)
{
  GtkTreeStore *model;
  GObject      *panel;
  GtkWidget    *display_panel;
  GtkTreeIter   iter;
  GtkTreeIter   iterDisplay;
  GtkTreeIter   iterAutomation;

  model = gtk_tree_store_new(N_COLUMNS,
                             G_TYPE_STRING,
                             G_TYPE_POINTER);

  panel = get_we_general_tab(world, ui_builder);
  if (world != default_world) {
    gtk_tree_store_append(model, &iter, NULL);
    fill_we_general_tab(world, ui_builder);
    gtk_tree_store_set(model, &iter,
                       COLUMN_NAME, _("General"),
                       COLUMN_WIDGET, panel,
                       -1);
  } else {
    gtk_widget_destroy(GTK_WIDGET(panel));
  }

  gtk_tree_store_append(model, &iterDisplay, NULL);
  display_panel = new_descriptive_panel(_("Display"),
                                        _("Here you can set options to configure the appearance of KildClient."));
  gtk_stack_add_named(stack, display_panel, "we_panel_display");
  gtk_tree_store_set(model, &iterDisplay,
                     COLUMN_NAME, _("Display"),
                     COLUMN_WIDGET, display_panel,
                     -1);

  gtk_tree_store_append(model, &iter, &iterDisplay);
  panel = get_we_mainwindow_tab(ui_builder);
  fill_we_mainwindow_tab(world, ui_builder);
  gtk_tree_store_set(model, &iter,
                     COLUMN_NAME, _("Main window"),
                     COLUMN_WIDGET, panel,
                     -1);

  gtk_tree_store_append(model, &iter, &iterDisplay);
  panel = get_we_colors_tab(world, ui_builder);
  /* Colors is filled in automatically */
  gtk_tree_store_set(model, &iter,
                     COLUMN_NAME, _("Colors"),
                     COLUMN_WIDGET, panel,
                     -1);

  gtk_tree_store_append(model, &iter, &iterDisplay);
  panel = get_we_statusbar_tab(ui_builder);
  fill_we_statusbar_tab(world, ui_builder);
  gtk_tree_store_set(model, &iter,
                     COLUMN_NAME, _("Status bar"),
                     COLUMN_WIDGET, panel,
                     -1);

  gtk_tree_store_append(model, &iter, NULL);
  panel = get_we_input_tab(ui_builder);
  fill_we_input_tab(world, ui_builder);
  gtk_tree_store_set(model, &iter,
                     COLUMN_NAME, _("Input"),
                     COLUMN_WIDGET, panel,
                     -1);

  gtk_tree_store_append(model, &iter, NULL);
  panel = get_we_logging_tab(ui_builder, world);
  fill_we_logging_tab(world, ui_builder);
  gtk_tree_store_set(model, &iter,
                     COLUMN_NAME, _("Logging"),
                     COLUMN_WIDGET, panel,
                     -1);

  if (world != default_world) {
    gtk_tree_store_append(model, &iterAutomation, NULL);
    display_panel = new_descriptive_panel(_("Automation"),
                                          _("Here you can set options to configure the automation features of KildClient, such as triggers, aliases, plugins, etc."));
    gtk_stack_add_named(stack, display_panel, "we_panel_automation");
    gtk_tree_store_set(model, &iterAutomation,
                       COLUMN_NAME, _("Automation"),
                       COLUMN_WIDGET, display_panel,
                       -1);

    gtk_tree_store_append(model, &iter, &iterAutomation);
    panel = get_we_scripting_tab(ui_builder);
    fill_we_scripting_tab(world, ui_builder);
    gtk_tree_store_set(model, &iter,
                       COLUMN_NAME, _("Scripting"),
                       COLUMN_WIDGET, panel,
                       -1);

    gtk_tree_store_append(model, &iter, &iterAutomation);
    panel = get_we_plugins_tab(ui_builder, world);
    fill_we_scripting_tab(world, ui_builder);
    gtk_tree_store_set(model, &iter,
                       COLUMN_NAME, _("Plugins"),
                       COLUMN_WIDGET, panel,
                       -1);

    gtk_tree_store_append(model, &iter, &iterAutomation);
    panel = get_we_triggers_tab(world);
    gtk_stack_add_named(stack, GTK_WIDGET(panel), "we_panel_triggers");
    /* Let the stack own the panel, removing the ref added when it
       was created. */
    g_object_unref(panel);
    gtk_tree_store_set(model, &iter,
                       COLUMN_NAME, _("Triggers"),
                       COLUMN_WIDGET, panel,
                       -1);

    gtk_tree_store_append(model, &iter, &iterAutomation);
    panel = get_we_aliases_tab(world);
    gtk_stack_add_named(stack, GTK_WIDGET(panel), "we_panel_aliases");
    /* Let the stack own the panel, removing the ref added when it
       was created. */
    g_object_unref(panel);
    gtk_tree_store_set(model, &iter,
                       COLUMN_NAME, _("Aliases"),
                       COLUMN_WIDGET, panel,
                       -1);

    gtk_tree_store_append(model, &iter, &iterAutomation);
    panel = get_we_macros_tab(world);
    gtk_stack_add_named(stack, GTK_WIDGET(panel), "we_panel_macros");
    /* Let the stack own the panel, removing the ref added when it
       was created. */
    g_object_unref(panel);
    gtk_tree_store_set(model, &iter,
                       COLUMN_NAME, _("Macros"),
                       COLUMN_WIDGET, panel,
                       -1);

    gtk_tree_store_append(model, &iter, &iterAutomation);
    panel = get_we_timers_tab(world);
    gtk_stack_add_named(stack, GTK_WIDGET(panel), "we_panel_timers");
    /* Let the stack own the panel, removing the ref added when it
       was created. */
    g_object_unref(panel);
    gtk_tree_store_set(model, &iter,
                       COLUMN_NAME, _("Timers"),
                       COLUMN_WIDGET, panel,
                       -1);

    gtk_tree_store_append(model, &iter, &iterAutomation);
    panel = get_we_hooks_tab(world);
    gtk_stack_add_named(stack, GTK_WIDGET(panel), "we_panel_hooks");
    /* Let the stack own the panel, removing the ref added when it
       was created. */
    g_object_unref(panel);
    gtk_tree_store_set(model, &iter,
                       COLUMN_NAME, _("Hooks"),
                       COLUMN_WIDGET, panel,
                       -1);

    gtk_tree_store_append(model, &iter, &iterAutomation);
    panel = get_we_vars_tab(world);
    gtk_stack_add_named(stack, GTK_WIDGET(panel), "we_panel_vars");
    /* Let the stack own the panel, removing the ref added when it
       was created. */
    g_object_unref(panel);
    gtk_tree_store_set(model, &iter,
                       COLUMN_NAME, _("Variables"),
                       COLUMN_WIDGET, panel,
                       -1);
  }

  gtk_tree_store_append(model, &iter, NULL);
  panel = get_we_protocols_tab(ui_builder);
  fill_we_protocols_tab(world, ui_builder);
  gtk_tree_store_set(model, &iter,
                     COLUMN_NAME, _("Protocols"),
                     COLUMN_WIDGET, panel,
                     -1);

  gtk_tree_store_append(model, &iter, NULL);
  panel = get_we_advanced_tab(world, ui_builder);
  fill_we_advanced_tab(world, ui_builder);
  gtk_tree_store_set(model, &iter,
                     COLUMN_NAME, _("Advanced"),
                     COLUMN_WIDGET, panel,
                     -1);

  return model;
}


static
void
list_selection_changed_cb(GtkTreeSelection *selection,
                          gpointer data)
{
  GtkTreeModel *model;
  GtkTreeIter   iter;
  GtkStack     *stack;

  if (gtk_tree_selection_get_selected(selection, &model, &iter)) {
    GtkTreePath *path;
    GtkWidget   *panel;

    stack = g_object_get_data(G_OBJECT(model), "stackptr");

    path = gtk_tree_model_get_path(model, &iter);
    gtk_tree_view_expand_row(GTK_TREE_VIEW(data), path, FALSE);
    gtk_tree_path_free(path);

    gtk_tree_model_get(model, &iter,
                       COLUMN_WIDGET, &panel,
                       -1);
    gtk_stack_set_visible_child(stack, panel);
  }
}


static
GtkWidget *
new_descriptive_panel(const char *title, const char *message)
{
  GtkWidget *vbox;
  GtkWidget *label;
  gchar     *temp;

  vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 6);
  gtk_container_set_border_width(GTK_CONTAINER(vbox), 12);

  temp = g_strdup_printf("<b>%s</b>", title);
  label = gtk_label_new(NULL);
  gtk_label_set_markup(GTK_LABEL(label), temp);
  g_free(temp);
  gtk_widget_set_halign(label, GTK_ALIGN_START);
  gtk_widget_set_valign(label, GTK_ALIGN_START);
  gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 0);

  label = gtk_label_new(message);
  gtk_widget_set_margin_start(label, 16);
  gtk_widget_set_halign(label, GTK_ALIGN_START);
  gtk_widget_set_valign(label, GTK_ALIGN_START);
  gtk_label_set_line_wrap(GTK_LABEL(label), TRUE);
  gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 0);

  return vbox;
}
