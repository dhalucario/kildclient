/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include "libintl-wrapper.h"
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

#include "kildclient.h"
#include "perlscript.h"


/***********************
 * Function prototypes *
 ***********************/
static GtkTreeModel *create_we_triggers_model(World *world);
static void          we_trigger_configure_view(GtkTreeView *view,
                                               GtkTreeModel *model);
static void          we_trigger_add_cb(GtkButton *button, gpointer data);
static void          we_trigger_edit_cb(GtkButton *button, gpointer data);
static void          we_trigger_delete_cb(GtkButton *button, gpointer data);
static void          we_trigger_view_row_dblclick_cb(GtkTreeView        *view,
                                                     GtkTreePath        *path,
                                                     GtkTreeViewColumn  *col,
                                                     gpointer            data);
static gboolean      edit_trigger(World *world, Trigger *trigger);
static gboolean      we_trigger_view_keypress_cb(GtkWidget   *widget,
                                                 GdkEventKey *evt,
                                                 gpointer     data);
static void          we_trigger_move_cb(GtkButton *button, gpointer data);
/* XML UI signals */
G_MODULE_EXPORT void we_trigger_chkhighlight_toggled_cb(gpointer   data,
                                                        GtkWidget *widget);
G_MODULE_EXPORT void we_trigger_btnconfigh_cb(GtkWidget *widget,
                                              gpointer   data);



GObject *
get_we_triggers_tab(World *world)
{
  GtkBuilder       *ui_builder;
  gchar            *objects[] = { "we_panel_generic_graphical_editor", NULL };
  GError           *error = NULL;
  GObject          *panel;
  GtkLabel         *lblType;
  GObject          *btnAdd;
  GObject          *btnEdit;
  GObject          *btnDelete;
  GObject          *btnUp;
  GObject          *btnDown;
  GObject          *btnExport;
  GtkBox           *vbox;
  GtkWidget        *btnTest;
  GtkTreeSelection *selection;

  /* Create the dialog */
  ui_builder = gtk_builder_new();
  if (!gtk_builder_add_objects_from_resource(ui_builder,
                                             "/ekalin/kildclient/kildclient.ui",
                                             objects,
                                             &error)) {
    g_warning(_("Error loading UI from XML file: %s"), error->message);
    g_error_free(error);
    return NULL; /* This will cause warnings in the calling function, but
                    there is need to abort the program because of this. */
  }

  panel = gtk_builder_get_object(ui_builder,
                                 "we_panel_generic_graphical_editor");
  /* So it's not destroyed when the builder is destroyed */
  g_object_ref(panel);

  world->viewTrigger = GTK_TREE_VIEW(gtk_builder_get_object(ui_builder,
                                                            "viewObjects"));
  g_object_set_data(G_OBJECT(world->viewTrigger), "forworld", world);
  g_signal_connect(G_OBJECT(world->viewTrigger), "row-activated",
                   G_CALLBACK(we_trigger_view_row_dblclick_cb), world);
  g_signal_connect(G_OBJECT(world->viewTrigger), "key-press-event",
                   G_CALLBACK(we_trigger_view_keypress_cb), world);

  lblType = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblGEType"));
  gtk_label_set_markup(lblType, "<span weight=\"bold\">Triggers</span>");

  btnAdd = gtk_builder_get_object(ui_builder, "btnGEAdd");
  g_signal_connect(btnAdd, "clicked",
                   G_CALLBACK(we_trigger_add_cb), world->viewTrigger);
  btnEdit = gtk_builder_get_object(ui_builder, "btnGEEdit");
  g_signal_connect(btnEdit, "clicked",
                   G_CALLBACK(we_trigger_edit_cb), world->viewTrigger);
  btnDelete = gtk_builder_get_object(ui_builder, "btnGEDelete");
  g_signal_connect(btnDelete, "clicked",
                   G_CALLBACK(we_trigger_delete_cb), world->viewTrigger);

  btnUp = gtk_builder_get_object(ui_builder, "btnGEUp");
  g_object_set_data(btnUp, "isup", GINT_TO_POINTER(TRUE));
  g_signal_connect(btnUp, "clicked",
                   G_CALLBACK(we_trigger_move_cb), world->viewTrigger);
  btnDown = gtk_builder_get_object(ui_builder, "btnGEDown");
  g_object_set_data(btnDown, "isup", GINT_TO_POINTER(FALSE));
  g_signal_connect(btnDown, "clicked",
                   G_CALLBACK(we_trigger_move_cb), world->viewTrigger);

  btnExport = gtk_builder_get_object(ui_builder, "btnGEExport");
  g_object_set_data(btnExport, "preselect", "Trigger");

  /* Add an extra button for "Test Triggers" */
  vbox = GTK_BOX(gtk_builder_get_object(ui_builder,
                                        "vbox_int_generic_graphical_editor"));
  btnTest = gtk_button_new_with_mnemonic(_("_Test triggers"));
  g_signal_connect_swapped(G_OBJECT(btnTest), "clicked",
                           G_CALLBACK(open_test_triggers_dialog), world);
  /* Only enable it if connected */
  if (!world->connected) {
    gtk_widget_set_sensitive(btnTest, FALSE);
  }
  gtk_box_pack_start(vbox, btnTest, FALSE, FALSE, 0);

  world->trigger_model = create_we_triggers_model(world);
  world->trigger_model_filter = GTK_TREE_MODEL_FILTER(gtk_tree_model_filter_new(world->trigger_model, NULL));
  g_object_unref(G_OBJECT(world->trigger_model));
  gtk_tree_model_filter_set_visible_func(GTK_TREE_MODEL_FILTER(world->trigger_model_filter),
                                         we_guied_plugin_filter_func,
                                         world,
                                         NULL);

  we_trigger_configure_view(world->viewTrigger,
                            GTK_TREE_MODEL(world->trigger_model_filter));
  selection = gtk_tree_view_get_selection(world->viewTrigger);
  gtk_tree_selection_set_mode(selection, GTK_SELECTION_MULTIPLE);
  g_object_set_data(G_OBJECT(selection), "btnEdit",   btnEdit);
  g_object_set_data(G_OBJECT(selection), "btnDelete", btnDelete);
  g_object_set_data(G_OBJECT(selection), "btnUp",     btnUp);
  g_object_set_data(G_OBJECT(selection), "btnDown",   btnDown);
  g_object_set_data(G_OBJECT(selection), "btnExport", btnExport);
  g_signal_connect(G_OBJECT(selection), "changed",
                   G_CALLBACK(we_guied_selection_changed_cb), NULL);

  gtk_builder_connect_signals(ui_builder, world);
  g_object_unref(ui_builder);
  return panel;
}


static
GtkTreeModel *
create_we_triggers_model(World *world)
{
  GtkListStore *store;
  GtkTreeIter   iter;
  GSList       *triggerptr;
  Trigger      *trigger;

  store = gtk_list_store_new(WE_GUIED_N_COLS,
                             G_TYPE_POINTER);

  triggerptr = world->triggers;
  while (triggerptr) {
    trigger = (Trigger *) triggerptr->data;
    gtk_list_store_append(store, &iter);
    gtk_list_store_set(store, &iter,
                       WE_GUIED_POINTER, trigger,
                       -1);

    triggerptr = triggerptr->next;
  }

  return GTK_TREE_MODEL(store);
}


static
void
we_trigger_configure_view(GtkTreeView *view, GtkTreeModel *model)
{
  GtkCellRenderer   *renderer;
  GtkTreeViewColumn *column;

  gtk_tree_view_set_model(view, model);
  g_object_unref(model);

  /* Column 1 - Enabled? */
  renderer = gtk_cell_renderer_toggle_new();
  g_object_set(G_OBJECT(renderer), "activatable", TRUE, NULL);
  g_object_set_data(G_OBJECT(renderer), "column",
                    GINT_TO_POINTER(TRIGGER_ENABLED));
  g_signal_connect(G_OBJECT(renderer), "toggled",
                   G_CALLBACK(we_guied_bool_col_toggled_cb), view);
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Enabled"));
  gtk_tree_view_column_pack_start(column, renderer, FALSE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_bool_func,
                                          GINT_TO_POINTER(TRIGGER_ENABLED),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 2 - Name */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Name"));
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_text_func,
                                          GINT_TO_POINTER(TRIGGER_NAME),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 3 - Pattern */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Pattern"));
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_text_func,
                                          GINT_TO_POINTER(TRIGGER_PATTERN),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 4 - Ignore Case? */
  renderer = gtk_cell_renderer_toggle_new();
  g_object_set(G_OBJECT(renderer), "activatable", TRUE, NULL);
  g_object_set_data(G_OBJECT(renderer), "column",
                    GINT_TO_POINTER(TRIGGER_ICASE));
  g_signal_connect(G_OBJECT(renderer), "toggled",
                   G_CALLBACK(we_guied_bool_col_toggled_cb), view);
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Ig. case"));
  gtk_tree_view_column_pack_start(column, renderer, FALSE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_bool_func,
                                          GINT_TO_POINTER(TRIGGER_ICASE),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 5 - Action */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Action"));
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_text_func,
                                          GINT_TO_POINTER(TRIGGER_ACTION),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 6 - Gag? */
  renderer = gtk_cell_renderer_toggle_new();
  g_object_set(G_OBJECT(renderer), "activatable", TRUE, NULL);
  g_object_set_data(G_OBJECT(renderer), "column",
                    GINT_TO_POINTER(TRIGGER_GAG));
  g_signal_connect(G_OBJECT(renderer), "toggled",
                   G_CALLBACK(we_guied_bool_col_toggled_cb), view);
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Gag"));
  gtk_tree_view_column_pack_start(column, renderer, FALSE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_bool_func,
                                          GINT_TO_POINTER(TRIGGER_GAG),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 7 - Gag Log? */
  renderer = gtk_cell_renderer_toggle_new();
  g_object_set(G_OBJECT(renderer), "activatable", TRUE, NULL);
  g_object_set_data(G_OBJECT(renderer), "column",
                    GINT_TO_POINTER(TRIGGER_GAGLOG));
  g_signal_connect(G_OBJECT(renderer), "toggled",
                   G_CALLBACK(we_guied_bool_col_toggled_cb), view);
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Gag log"));
  gtk_tree_view_column_pack_start(column, renderer, FALSE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_bool_func,
                                          GINT_TO_POINTER(TRIGGER_GAGLOG),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 8 - Keep Exec? */
  renderer = gtk_cell_renderer_toggle_new();
  g_object_set(G_OBJECT(renderer), "activatable", TRUE, NULL);
  g_object_set_data(G_OBJECT(renderer), "column",
                    GINT_TO_POINTER(TRIGGER_KEEPEXEC));
  g_signal_connect(G_OBJECT(renderer), "toggled",
                   G_CALLBACK(we_guied_bool_col_toggled_cb), view);
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Keep exec"));
  gtk_tree_view_column_pack_start(column, renderer, FALSE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_bool_func,
                                          GINT_TO_POINTER(TRIGGER_KEEPEXEC),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 9 - Rewriter? */
  renderer = gtk_cell_renderer_toggle_new();
  g_object_set(G_OBJECT(renderer), "activatable", TRUE, NULL);
  g_object_set_data(G_OBJECT(renderer), "column",
                    GINT_TO_POINTER(TRIGGER_REWRITER));
  g_signal_connect(G_OBJECT(renderer), "toggled",
                   G_CALLBACK(we_guied_bool_col_toggled_cb), view);
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Rewriter"));
  gtk_tree_view_column_pack_start(column, renderer, FALSE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_bool_func,
                                          GINT_TO_POINTER(TRIGGER_REWRITER),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 10 - Change Style? */
  renderer = gtk_cell_renderer_toggle_new();
  g_object_set(G_OBJECT(renderer), "activatable", TRUE, NULL);
  g_object_set_data(G_OBJECT(renderer), "column",
                    GINT_TO_POINTER(TRIGGER_HIGHLIGHT));
  g_signal_connect(G_OBJECT(renderer), "toggled",
                   G_CALLBACK(we_guied_bool_col_toggled_cb), view);
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Style"));
  gtk_tree_view_column_pack_start(column, renderer, FALSE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_bool_func,
                                          GINT_TO_POINTER(TRIGGER_HIGHLIGHT),
                                          NULL);
  gtk_tree_view_append_column(view, column);

  /* Column 11 - Plugin */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_column_set_title(column, _("Plugin"));
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func(column,
                                          renderer,
                                          we_guied_text_func,
                                          GINT_TO_POINTER(OBJECT_PLUGIN),
                                          NULL);
  gtk_tree_view_append_column(view, column);


  gtk_tree_view_columns_autosize(view);
}


static
void
we_trigger_add_cb(GtkButton *button, gpointer data)
{
  World       *world;
  GtkTreeView *view;
  Trigger     *trigger;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");

  trigger = new_trigger();
  trigger->enabled = 1;

  if (edit_trigger(world, trigger)) {
    world->triggers = g_slist_insert(world->triggers,
                                     trigger,
                                     world->trigger_pos);
    we_trigger_insert_trigger(world, trigger, world->trigger_pos++);
  } else {
    g_free(trigger);
  }
}


static
void
we_trigger_edit_cb(GtkButton *button, gpointer data)
{
  /* Note that the Edit button is only active when exactly one row
     is selected. */
  World            *world;
  GtkTreeView      *view;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  GList            *selected;
  GtkTreeIter       iter;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");
  selection = gtk_tree_view_get_selection(view);
  selected = gtk_tree_selection_get_selected_rows(selection, &model);
  if (selected) {
    Trigger *trigger;

    gtk_tree_model_get_iter(model, &iter, selected->data);
    gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &trigger, -1);

    if (edit_trigger(world, trigger)) {
      we_trigger_update_trigger(world, trigger);
    }

    g_list_foreach(selected, (GFunc) gtk_tree_path_free, NULL);
    g_list_free(selected);
  }
}


static
void
we_trigger_delete_cb(GtkButton *button, gpointer data)
{
  World            *world;
  GtkTreeView      *view;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  GList            *selected;
  GtkTreeIter       iter;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");
  selection = gtk_tree_view_get_selection(view);
  selected = gtk_tree_selection_get_selected_rows(selection, &model);
  if (selected) {
    Trigger   *trigger;
    GSList    *triggeritem;
    GtkWidget *msgdlg;
    gint       n;

    n = gtk_tree_selection_count_selected_rows(selection);
    msgdlg = we_guied_confirm_delete_dialog_new(GTK_WINDOW(world->dlgEditWorld),
                                                ngettext("trigger", "triggers", n),
                                                n);
    if (!world->confirm_delete
        || gtk_dialog_run(GTK_DIALOG(msgdlg)) == GTK_RESPONSE_YES) {
      GList *listiter = g_list_last(selected);

      while (listiter) {
        gtk_tree_model_get_iter(model, &iter, listiter->data);
        gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &trigger, -1);

        triggeritem = g_slist_find(world->triggers, trigger);
        remove_trigger(world, triggeritem);

        listiter = listiter->prev;
      }
    }
    gtk_widget_destroy(msgdlg);

    g_list_foreach(selected, (GFunc) gtk_tree_path_free, NULL);
    g_list_free(selected);
  }
}


static
void
we_trigger_view_row_dblclick_cb(GtkTreeView        *view,
                                GtkTreePath        *path,
                                GtkTreeViewColumn  *col,
                                gpointer            data)
{
  World        *world;
  GtkTreeModel *model;
  GtkTreeIter   iter;

  world = (World *) data;
  model = gtk_tree_view_get_model(view);

  if (gtk_tree_model_get_iter(model, &iter, path)) {
    Trigger *trigger;

    gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &trigger, -1);

    if (edit_trigger(world, trigger)) {
      we_trigger_update_trigger(world, trigger);
    }
  }
}


static
gboolean
edit_trigger(World *world, Trigger *trigger)
{
  GtkBuilder      *ui_builder;
  gchar           *objects[] = { "dlgEditTrigger", NULL };
  GError          *error = NULL;
  GtkWidget       *dlgEditTrigger;
  GtkEntry        *txtName;
  GtkEntry        *txtPattern;
  GtkEntry        *txtAction;
  GtkToggleButton *chkICase;
  GtkToggleButton *chkGag;
  GtkToggleButton *chkGagLog;
  GtkToggleButton *chkKeepExec;
  GtkToggleButton *chkRewriter;
  GtkToggleButton *chkHighlight;
  GObject         *btnConfigH;
  GtkToggleButton *chkEnabled;
  const gchar     *newname;
  const gchar     *newpattern;
  const gchar     *newaction;
  gboolean         newgag;
  gboolean         newgaglog;
  gboolean         newhighlight;
  gboolean         newicase;
  gboolean         need_recompute = FALSE;

  ui_builder = gtk_builder_new();
  if (!gtk_builder_add_objects_from_resource(ui_builder,
                                             "/ekalin/kildclient/kildclient.ui",
                                             objects,
                                             &error)) {
    g_warning(_("Error loading UI from XML file: %s"), error->message);
    g_error_free(error);
    return FALSE;
  }
  gtk_builder_connect_signals(ui_builder, NULL);

  dlgEditTrigger = GTK_WIDGET(gtk_builder_get_object(ui_builder,
                                                     "dlgEditTrigger"));
  gtk_window_set_transient_for(GTK_WINDOW(dlgEditTrigger),
                               GTK_WINDOW(world->dlgEditWorld));
  gtk_dialog_set_default_response(GTK_DIALOG(dlgEditTrigger),
                                  GTK_RESPONSE_OK);

  txtName      = GTK_ENTRY(gtk_builder_get_object(ui_builder,
                                                  "txtTriggerName"));
  txtPattern   = GTK_ENTRY(gtk_builder_get_object(ui_builder,
                                                  "txtTriggerPattern"));
  txtAction    = GTK_ENTRY(gtk_builder_get_object(ui_builder,
                                                  "txtTriggerAction"));
  chkICase     = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder,
                                                          "chkTriggerICase"));
  chkGag       = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder,
                                                          "chkTriggerGag"));
  chkGagLog    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder,
                                                          "chkTriggerGagLog"));
  chkKeepExec  = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder,
                                                          "chkTriggerKeepExec"));
  chkRewriter  = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder,
                                                          "chkTriggerRewriter"));
  chkHighlight = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder,
                                                          "chkTriggerHighlight"));
  btnConfigH   = gtk_builder_get_object(ui_builder, "btnTriggerConfigHighlight");
  chkEnabled   = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder,
                                                          "chkTriggerEnabled"));

  g_object_set_data(btnConfigH, "trigger", trigger);
  g_object_set_data(btnConfigH, "parent-window", dlgEditTrigger);

  /* Only display note when editing a plugin's trigger */
  if (!trigger->owner_plugin) {
    GObject *label;

    label = gtk_builder_get_object(ui_builder, "lblNotePlugin");
    gtk_widget_destroy(GTK_WIDGET(label));
    label = gtk_builder_get_object(ui_builder, "lblNoteTextPlugin");
    gtk_widget_destroy(GTK_WIDGET(label));
  }

  /* We don't need it anymore */
  g_object_unref(ui_builder);

  /* Fill-in values */
  if (trigger->name) {
    gtk_entry_set_text(txtName, trigger->name);
  }
  if (trigger->pattern) {
    gtk_entry_set_text(txtPattern, trigger->pattern);
  }
  if (trigger->action) {
    gtk_entry_set_text(txtAction, trigger->action);
  }
  gtk_toggle_button_set_active(chkICase,     trigger->ignore_case);
  gtk_toggle_button_set_active(chkGag,       trigger->gag_output);
  gtk_toggle_button_set_active(chkGagLog,    trigger->gag_log);
  gtk_toggle_button_set_active(chkKeepExec,  trigger->keepexecuting);
  gtk_toggle_button_set_active(chkRewriter,  trigger->rewriter);
  gtk_toggle_button_set_active(chkHighlight, trigger->highlight);
  gtk_toggle_button_set_active(chkEnabled,   trigger->enabled);

  /* Run the dialog until the input is valid or cancelled */
  gtk_widget_show_all(dlgEditTrigger);
  while (1) {
    if (gtk_dialog_run(GTK_DIALOG(dlgEditTrigger)) == GTK_RESPONSE_OK) {
      /* Validate */
      newicase     = gtk_toggle_button_get_active(chkICase);
      newgag       = gtk_toggle_button_get_active(chkGag);
      newgaglog    = gtk_toggle_button_get_active(chkGagLog);
      newhighlight = gtk_toggle_button_get_active(chkHighlight);

      newpattern = gtk_entry_get_text(txtPattern);
      if (strcmp(newpattern, "") == 0) {
        kc_error_dialog(GTK_WINDOW(dlgEditTrigger),
                        _("You must specify the pattern."));
        continue;
      }

      newaction = gtk_entry_get_text(txtAction);
      if (strcmp(newaction, "") == 0
          && !newgag && !newgaglog && !newhighlight) {
        kc_error_dialog(GTK_WINDOW(dlgEditTrigger),
                        _("You must specify the action, unless the trigger is a gag or changes the style."));
        continue;
      }

      /* Update values */
      newname = gtk_entry_get_text(txtName);
      if (!trigger->name || strcmp(newname, trigger->name) != 0) {
        g_free(trigger->name);
        if (strcmp(newname, "") != 0) {
          trigger->name = g_strdup(newname);
        } else {
          trigger->name = NULL;
        }
      }

      if (!trigger->pattern || strcmp(newpattern, trigger->pattern) != 0) {
        g_free(trigger->pattern);
        trigger->pattern = g_strdup(newpattern);
        need_recompute = TRUE;
      }

      if (!trigger->action || strcmp(newaction, trigger->action) != 0) {
        g_free(trigger->action);
        if (strcmp(newaction, "") != 0) {
          trigger->action = g_strdup(newaction);
        } else {
          trigger->action = NULL;
        }
      }

      if (trigger->ignore_case != newicase) {
        need_recompute = TRUE;
      }
      trigger->ignore_case   = newicase;
      trigger->gag_output    = newgag;
      trigger->gag_log       = newgaglog;
      trigger->keepexecuting = gtk_toggle_button_get_active(chkKeepExec);
      trigger->rewriter      = gtk_toggle_button_get_active(chkRewriter);
      trigger->highlight     = newhighlight;
      trigger->enabled       = gtk_toggle_button_get_active(chkEnabled);

      if (need_recompute && world->perl_interpreter) {
        GRAB_PERL(world);
        if (trigger->pattern_re) {
          SvREFCNT_dec(trigger->pattern_re);
        }
        trigger->pattern_re = precompute_re(trigger->pattern,
                                            trigger->ignore_case);
        RELEASE_PERL(world);
      }

      /* We've finished successfully */
      gtk_widget_destroy(dlgEditTrigger);
      return TRUE;
    } else {
      /* Cancel pressed */
      gtk_widget_destroy(dlgEditTrigger);
      return FALSE;
    }
  }
}


void
we_trigger_chkhighlight_toggled_cb(gpointer data, GtkWidget *widget)
{
  GtkWidget *btnConfigH = GTK_WIDGET(data);

  gtk_widget_set_sensitive(btnConfigH, gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget)));
}


void
we_trigger_btnconfigh_cb(GtkWidget *widget, gpointer data)
{
  Trigger         *trigger;
  GtkBuilder      *ui_builder;
  gchar           *objects[] = { "dlgEditTriggerHighlight",
                                 "modelColors", "modelUnderline",
                                 "modelYesNo", "adjSpnTarget", NULL };
  GError          *error = NULL;
  GtkWidget       *dlgEditHighlight;
  GtkWindow       *parent;
  GtkToggleButton *radTargetLine;
  GtkToggleButton *radTargetMatch;
  GtkToggleButton *radTargetSubstring;
  GtkSpinButton   *spnTarget;
  GtkComboBox     *cmbFgColor;
  GtkComboBox     *cmbBgColor;
  GtkComboBox     *cmbItalic;
  GtkComboBox     *cmbStrike;
  GtkComboBox     *cmbUnderline;

  trigger = g_object_get_data(G_OBJECT(widget), "trigger");
  parent  = g_object_get_data(G_OBJECT(widget), "parent-window");

  ui_builder = gtk_builder_new();
  if (!gtk_builder_add_objects_from_resource(ui_builder,
                                             "/ekalin/kildclient/kildclient.ui",
                                             objects,
                                             &error)) {
    g_warning(_("Error loading UI from XML file: %s"), error->message);
    g_error_free(error);
    return;
  }
  dlgEditHighlight
    = GTK_WIDGET(gtk_builder_get_object(ui_builder, "dlgEditTriggerHighlight"));
  gtk_window_set_transient_for(GTK_WINDOW(dlgEditHighlight), parent);

  cmbFgColor = GTK_COMBO_BOX(gtk_builder_get_object(ui_builder, "cmbFgColor"));
  cmbBgColor = GTK_COMBO_BOX(gtk_builder_get_object(ui_builder, "cmbBgColor"));

  radTargetLine
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "radTargetLine"));
  radTargetMatch
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "radTargetMatch"));
  radTargetSubstring
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "radTargetSubstring"));
  spnTarget
    = GTK_SPIN_BUTTON(gtk_builder_get_object(ui_builder, "spnTarget"));
  cmbItalic
    = GTK_COMBO_BOX(gtk_builder_get_object(ui_builder, "cmbHighlightItalic"));
  cmbStrike
    = GTK_COMBO_BOX(gtk_builder_get_object(ui_builder, "cmbHighlightStrike"));
  cmbUnderline
    = GTK_COMBO_BOX(gtk_builder_get_object(ui_builder, "cmbHighlightUnderline"));

  /* Fill in values. */
  if (trigger->high_target == -1) {
    gtk_toggle_button_set_active(radTargetLine, TRUE);
  } else if (trigger->high_target == 0) {
    gtk_toggle_button_set_active(radTargetMatch, TRUE);
  } else {
    gtk_toggle_button_set_active(radTargetSubstring, TRUE);
    gtk_spin_button_set_value(spnTarget, trigger->high_target);
  }
  /* +1 is because -1 corresponds to "No change" */
  gtk_combo_box_set_active(cmbFgColor,   trigger->high_fg_color  + 1);
  gtk_combo_box_set_active(cmbBgColor,   trigger->high_bg_color  + 1);
  gtk_combo_box_set_active(cmbItalic,    trigger->high_italic    + 1);
  gtk_combo_box_set_active(cmbStrike,    trigger->high_strike    + 1);
  gtk_combo_box_set_active(cmbUnderline, trigger->high_underline + 1);

  /* Run the dialog */
  gtk_widget_show_all(dlgEditHighlight);
  if (gtk_dialog_run(GTK_DIALOG(dlgEditHighlight)) == GTK_RESPONSE_OK) {
    /* Update values */
    if (gtk_toggle_button_get_active(radTargetLine)) {
      trigger->high_target = -1;
    } else if (gtk_toggle_button_get_active(radTargetMatch)) {
      trigger->high_target = 0;
    } else {
      trigger->high_target = gtk_spin_button_get_value_as_int(spnTarget);
    }

    trigger->high_fg_color  = gtk_combo_box_get_active(cmbFgColor)   - 1;
    trigger->high_bg_color  = gtk_combo_box_get_active(cmbBgColor)   - 1;
    trigger->high_italic    = gtk_combo_box_get_active(cmbItalic)    - 1;
    trigger->high_strike    = gtk_combo_box_get_active(cmbStrike)    - 1;
    trigger->high_underline = gtk_combo_box_get_active(cmbUnderline) - 1;
  }

  gtk_widget_destroy(dlgEditHighlight);
  g_object_unref(ui_builder);
}


static
gboolean
we_trigger_view_keypress_cb(GtkWidget   *widget,
                            GdkEventKey *evt,
                            gpointer     data)
{
  if (evt->keyval == GDK_KEY_Delete || evt->keyval == GDK_KEY_KP_Delete) {
    we_trigger_delete_cb(NULL, widget);
    return TRUE;
  }

  return FALSE;
}


static
void
we_trigger_move_cb(GtkButton *button, gpointer data)
{
  GtkTreeView      *view;
  GtkTreeModel     *model;
  GtkTreeSelection *selection;
  GList            *selected;
  GtkTreeIter       iter;
  World            *world;
  gboolean          is_up;
  gint              new_pos;

  view = (GtkTreeView *) data;
  world = (World *) g_object_get_data(G_OBJECT(view), "forworld");
  selection = gtk_tree_view_get_selection(view);
  is_up = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(button), "isup"));
  selected = gtk_tree_selection_get_selected_rows(selection, &model);
  if (selected) {
    Trigger *trigger;
    gint     pos;

    gtk_tree_model_get_iter(model, &iter, selected->data);
    gtk_tree_model_get(model, &iter, WE_GUIED_POINTER, &trigger, -1);
    pos = g_slist_index(world->triggers, trigger);
    new_pos = pos;
    if (is_up) {
      if (pos != 0) {
        new_pos = pos - 1;
        move_trigger(world, pos, new_pos);
      }
    } else {
      if (pos != world->trigger_pos - 1) {
        new_pos = pos + 1;
        move_trigger(world, pos, new_pos);
      }
    }
    /* Reselect the moved item */
    gtk_tree_model_iter_nth_child(model, &iter, NULL, new_pos);
    gtk_tree_selection_select_iter(selection, &iter);

    g_list_foreach(selected, (GFunc) gtk_tree_path_free, NULL);
    g_list_free(selected);
  }
}


void
we_trigger_update_trigger(World *world, Trigger *trigger_arg)
{
  /* Called by the Perl functions when an trigger is changed, so that
     the display is updated. */
  GtkTreeIter  iter;
  GtkTreePath *path;
  Trigger     *trigger;
  gboolean     success;

  if (!world->trigger_model) {
    return;
  }

  success = gtk_tree_model_get_iter_first(world->trigger_model, &iter);
  while (success) {
    gtk_tree_model_get(world->trigger_model, &iter,
                       WE_GUIED_POINTER, &trigger, -1);
    if (trigger == trigger_arg) {
      path = gtk_tree_model_get_path(world->trigger_model, &iter);
      gtk_tree_model_row_changed(world->trigger_model, path, &iter);
      gtk_tree_path_free(path);
      return;
    }
    success = gtk_tree_model_iter_next(world->trigger_model, &iter);
  }
}


void
we_trigger_insert_trigger(World *world, Trigger *trigger, gint pos)
{
  /* Called by the Perl functions when an trigger is inserted, so that
     the display is updated. */
  GtkTreeIter iter;

  if (!world->trigger_model) {
    return;
  }

  gtk_list_store_insert(GTK_LIST_STORE(world->trigger_model), &iter, pos);
  gtk_list_store_set(GTK_LIST_STORE(world->trigger_model), &iter,
                     WE_GUIED_POINTER, trigger,
                     -1);
}


void
we_trigger_delete_trigger(World *world, Trigger *trigger_arg)
{
  /* Called by the Perl functions when an trigger is deleted, so that
     the display is updated. */
  GtkTreeIter  iter;
  Trigger       *trigger;
  gboolean     success;

  if (!world->trigger_model) {
    return;
  }

  success = gtk_tree_model_get_iter_first(world->trigger_model, &iter);
  while (success) {
    gtk_tree_model_get(world->trigger_model, &iter,
                       WE_GUIED_POINTER, &trigger, -1);
    if (trigger == trigger_arg) {
      gtk_list_store_remove(GTK_LIST_STORE(world->trigger_model), &iter);
      return;
    }
    success = gtk_tree_model_iter_next(world->trigger_model, &iter);
  }
}
