/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include "libintl-wrapper.h"
#include <gtk/gtk.h>

#include "kildclient.h"
#include "perlscript.h"


/***********************
 * Function prototypes *
 ***********************/
/* XML UI signals */
G_MODULE_EXPORT void statusfont_set_cb(GtkFontButton *font_btn,
                                       gpointer data);
G_MODULE_EXPORT void time_type_changed_cb(GtkWidget *widget, gpointer data);
G_MODULE_EXPORT void reset_activate_toggled_cb(GtkWidget *widget,
                                               gpointer data);


/*************************
 * File global variables *
 *************************/



GObject *
get_we_statusbar_tab(GtkBuilder *ui_builder)
{
  GObject *panel;

  panel = gtk_builder_get_object(ui_builder, "we_panel_statusbar");

  return panel;
}


void
fill_we_statusbar_tab(World *world, GtkBuilder *ui_builder)
{
  GtkFontChooser  *statusfont_btn;
  GtkToggleButton *radCTime_no;
  GtkToggleButton *radCTime_hms;
  GtkToggleButton *radCTime_sec;
  GtkToggleButton *radITime_no;
  GtkToggleButton *radITime_hms;
  GtkToggleButton *radITime_sec;
  GtkToggleButton *chkResetActivate;

  statusfont_btn = GTK_FONT_CHOOSER(gtk_builder_get_object(ui_builder,
                                                           "statusfont_btn"));
  gtk_font_chooser_set_font(statusfont_btn, world->statusfont);

  radCTime_no  = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder,
                                                          "radCTime_no"));
  radCTime_hms = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder,
                                                          "radCTime_hms"));
  radCTime_sec = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder,
                                                          "radCTime_sec"));
  if (world->ctime_type == NO) {
    gtk_toggle_button_set_active(radCTime_no, TRUE);
  } else if (world->ctime_type == HMS) {
    gtk_toggle_button_set_active(radCTime_hms, TRUE);
  } else {
    gtk_toggle_button_set_active(radCTime_sec, TRUE);
  }

  radITime_no  = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder,
                                                          "radITime_no"));
  radITime_hms = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder,
                                                          "radITime_hms"));
  radITime_sec = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder,
                                                          "radITime_sec"));
  if (world->itime_type == NO) {
    gtk_toggle_button_set_active(radITime_no, TRUE);
  } else if (world->itime_type == HMS) {
    gtk_toggle_button_set_active(radITime_hms, TRUE);
  } else {
    gtk_toggle_button_set_active(radITime_sec, TRUE);
  }

  chkResetActivate
    = GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "chkResetActivate"));
  gtk_toggle_button_set_active(chkResetActivate, world->itime_reset_activate);
}


void
statusfont_set_cb(GtkFontButton *font_btn, gpointer data)
{
  World    *world = (World *) data;
  WorldGUI *gui   = world->gui;

  free(world->statusfont);
  world->statusfont = g_strdup(gtk_font_chooser_get_font(GTK_FONT_CHOOSER(font_btn)));

  if (gui) {
    configure_css(gui, world);
  }
}


void
time_type_changed_cb(GtkWidget *widget, gpointer data)
{
  World           *world = (World *) data;
  GtkToggleButton *radCTime_no;
  GtkToggleButton *radCTime_hms;
  GtkToggleButton *radITime_no;
  GtkToggleButton *radITime_hms;

  radCTime_no  = GTK_TOGGLE_BUTTON(gtk_builder_get_object(world->ui_builder,
                                                          "radCTime_no"));
  radCTime_hms = GTK_TOGGLE_BUTTON(gtk_builder_get_object(world->ui_builder,
                                                          "radCTime_hms"));
  radITime_no  = GTK_TOGGLE_BUTTON(gtk_builder_get_object(world->ui_builder,
                                                          "radITime_no"));
  radITime_hms = GTK_TOGGLE_BUTTON(gtk_builder_get_object(world->ui_builder,
                                                          "radITime_hms"));

  if (gtk_toggle_button_get_active(radCTime_no)) {
    world->ctime_type = NO;
  } else if (gtk_toggle_button_get_active(radCTime_hms)) {
    world->ctime_type = HMS;
  } else {
    world->ctime_type = SEC;
  }

  if (gtk_toggle_button_get_active(radITime_no)) {
    world->itime_type = NO;
  } else if (gtk_toggle_button_get_active(radITime_hms)) {
    world->itime_type = HMS;
  } else {
    world->itime_type = SEC;
  }
}


void
reset_activate_toggled_cb(GtkWidget *widget, gpointer data)
{
  World *world = (World *) data;

  world->itime_reset_activate =
    gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget));
}
