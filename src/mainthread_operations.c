/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <gtk/gtk.h>

#include "kildclient.h"



void
queue_mainthread_operation(World *world, MainThreadOperation *operation)
{
  g_async_queue_push(world->mainthread_operations, operation);
}


gboolean
mainthread_operations_idle_cb(gpointer data)
{
  World *world = (World *) data;

  gpointer operation = g_async_queue_try_pop(world->mainthread_operations);
  while (operation != NULL) {
    process_mainthread_operation(world->gui, (MainThreadOperation *) operation);
    operation = g_async_queue_try_pop(world->mainthread_operations);
  }

  return TRUE;
}


void
free_mainthread_operation(gpointer data)
{
  MainThreadOperation *operation = (MainThreadOperation *) data;

  switch (operation->action) {
  case INSERT_AT_END:
  case INSERT_AT_MARK:
    g_free(operation->text_insert.text);
    break;

  case APPLY_HIGHLIGHT:
    g_free(operation->highlight);
    break;

  case PARSE_COMMANDS:
    if (operation->commands.matches) {
      SvREFCNT_dec(operation->commands.matches);
    }
    g_free(operation->commands.commands);
    break;

  case EXECUTE_ON_RECEIVED_TEXT:
    g_free(operation->hook_execution->data);
    g_free(operation->hook_execution);
    break;

  default:
    /* No additional free'ing necessary. */
    break;
  }

  g_free(operation);
}


void
process_mainthread_operation(WorldGUI *gui, MainThreadOperation *operation)
{
  switch (operation->action) {
  case INSERT_AT_END:
    ansitextview_op_insert_at_end(gui, operation);
    break;

  case INSERT_AT_MARK:
    ansitextview_op_insert_at_mark(gui, operation);
    break;

  case TEXT_ADDED:
    ansitextview_op_text_added(gui, operation);
    break;

  case CLEAR_PENDING_LINE:
    ansitextview_op_clear_pending_line(gui, operation);
    break;

  case MOVE_LINESTART_MARK:
    ansitextview_op_move_linestart_mark(gui, operation);
    break;

  case APPLY_HIGHLIGHT:
    ansitextview_op_apply_highlight(gui, operation);
    break;

  case PARSE_COMMANDS:
    mainthread_op_parse_commands(gui->world, operation);
    break;

  case EXECUTE_ON_RECEIVED_TEXT:
    mainthread_op_execute_on_received_text_hook(operation->hook_execution);
    break;
  }

  free_mainthread_operation(operation);
}
