/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <unistd.h>
#include <gtk/gtk.h>

#include "kildclient.h"
#include "perlscript.h"
#include "net.h"


/*************************
 * File global variables *
 *************************/
#define MAX_US 250000


/***********************
 * Function prototypes *
 ***********************/
static void append_to_server_data_buffer(World *world, guchar chr);
static void process_server_data(World *world);


void
process_telnet_iac(World *world, int pos, int end)
{
  /* This function processes Telnet IAC sequences and converts the
     newlines to the canonical \n. */
  unsigned char iac_response[4];
  time_t        now;

  while (pos < end) {
    switch (world->telnet_state) {
    case TELNETFSM_DATA:
      switch (world->dbuffer[pos]) {
      case TELNET_IAC:
        world->telnet_state = TELNETFSM_IAC;
        break;

      case '\n':
        world->pbuffer[world->pbuf_size++] = world->dbuffer[pos];
        world->telnet_state = TELNETFSM_DATA_NL;
        break;

      case '\r':
        world->telnet_state_ncrs = 1;
        world->telnet_state = TELNETFSM_DATA_CR;
        break;

      case '\b': /* Backspace */
        if (world->pbuf_size > 0
            && world->pbuffer[world->pbuf_size - 1] != '\n') {
          --world->pbuf_size;
        }
        break;

      default:
        world->pbuffer[world->pbuf_size++] = world->dbuffer[pos];
      }
      break;

    case TELNETFSM_DATA_NL:
      if (world->dbuffer[pos] != '\r' && world->dbuffer[pos] != '\b') {
        --pos; /* Reprocess this character as data */
      }
      world->telnet_state = TELNETFSM_DATA;
      break;

    case TELNETFSM_DATA_CR:
      if (world->dbuffer[pos] == '\n') {
        world->pbuffer[world->pbuf_size++] = world->dbuffer[pos];
        world->telnet_state = TELNETFSM_DATA;
      } else if (world->dbuffer[pos] == '\r') {
        ++world->telnet_state_ncrs;
      } else {
        int i;

        /* Add the newlines. But check if there is space before. */
        if (world->pbuf_size + world->telnet_state_ncrs >=
            world->pbuf_alloc_size) {
          world->pbuf_alloc_size += world->telnet_state_ncrs;
          world->pbuffer = g_realloc(world->pbuffer,
                                     world->pbuf_alloc_size);
        }

        for (i = 0; i < world->telnet_state_ncrs; ++i) {
          world->pbuffer[world->pbuf_size++] = '\n';
        }
        world->pbuffer[world->pbuf_size++] = world->dbuffer[pos];
        world->telnet_state = TELNETFSM_DATA;
      }
      break;

    case TELNETFSM_IAC:
      switch (world->dbuffer[pos]) {
      case TELNET_IAC:
        world->pbuffer[world->pbuf_size++] = TELNET_IAC;
        world->telnet_state = TELNETFSM_DATA;
        break;

      case TELNET_DO:
        world->telnet_state = TELNETFSM_DO;
        break;

      case TELNET_DONT:
        world->telnet_state = TELNETFSM_DONT;
        break;

      case TELNET_WILL:
        world->telnet_state = TELNETFSM_WILL;
        break;

      case TELNET_WONT:
        world->telnet_state = TELNETFSM_WONT;
        break;

      case TELNET_SB:
        world->telnet_state = TELNETFSM_SB;
        break;

      default:
        world->telnet_state = TELNETFSM_DATA;
        break;
      }
      break;

    case TELNETFSM_DO:
      switch (world->dbuffer[pos]) {
      case TELNET_OPT_TERMINAL_TYPE:
        iac_response[0] = TELNET_IAC;
        iac_response[1] = TELNET_WILL;
        iac_response[2] = TELNET_OPT_TERMINAL_TYPE;
        kc_send(world, iac_response, 3);
        break;

      case TELNET_OPT_NAWS:
        iac_response[0] = TELNET_IAC;
        iac_response[1] = TELNET_WILL;
        iac_response[2] = TELNET_OPT_NAWS;
        kc_send(world, iac_response, 3);
        world->use_naws = TRUE;
        send_naws_size(world);
      }
      world->telnet_state = TELNETFSM_DATA;
      break;

    case TELNETFSM_DONT:
      switch (world->dbuffer[pos]) {
      case TELNET_OPT_NAWS:
        world->use_naws = FALSE;
        break;
      }
      world->telnet_state = TELNETFSM_DATA;
      break;

    case TELNETFSM_WILL:
      switch (world->dbuffer[pos]) {
      case TELNET_OPT_ECHO:
        world->noecho = TRUE;
        if (!world->never_hide_input) {
          simo_combo_box_set_visibility(world->gui->cmbEntry, FALSE);
        }
        iac_response[0] = TELNET_IAC;
        iac_response[1] = TELNET_DO;
        iac_response[2] = TELNET_OPT_ECHO;
        kc_send(world, iac_response, 3);
        break;

      case TELNET_OPT_COMPRESS2:
        now = time(NULL);
        if (world->mccp_behavior == MCCP_ALWAYS
            || (world->mccp_behavior == MCCP_AFTER_CONNECT
                && difftime(now, world->connected_time) <= 60)) {
          world->mccp_ver = 2;
          iac_response[0] = TELNET_IAC;
          iac_response[1] = TELNET_DO;
          iac_response[2] = TELNET_OPT_COMPRESS2;
          kc_send(world, iac_response, 3);
        }
        break;

      case TELNET_OPT_COMPRESS:
        /* Do nothing if version 2 is supported */
        now = time(NULL);
        if (world->mccp_ver <= 1
            && (world->mccp_behavior == MCCP_ALWAYS
                || (world->mccp_behavior == MCCP_AFTER_CONNECT
                    && difftime(now, world->connected_time) <= 60))) {
          world->mccp_ver = 1;
          iac_response[0] = TELNET_IAC;
          iac_response[1] = TELNET_DO;
          iac_response[2] = TELNET_OPT_COMPRESS;
          kc_send(world, iac_response, 3);
        }
        break;

      case TELNET_OPT_GMCP:
        world->gmcp_enabled = TRUE;
        iac_response[0] = TELNET_IAC;
        iac_response[1] = TELNET_DO;
        iac_response[2] = TELNET_OPT_GMCP;
        kc_send(world, iac_response, 3);
        break;

      case TELNET_OPT_MSDP:
        world->msdp_enabled = TRUE;
        iac_response[0] = TELNET_IAC;
        iac_response[1] = TELNET_DO;
        iac_response[2] = TELNET_OPT_MSDP;
        kc_send(world, iac_response, 3);
        break;
      }
      world->telnet_state = TELNETFSM_DATA;
      break;

    case TELNETFSM_WONT:
      switch (world->dbuffer[pos]) {
      case TELNET_OPT_ECHO:
        world->noecho = FALSE;
        simo_combo_box_set_visibility(world->gui->cmbEntry, TRUE);
        iac_response[0] = TELNET_IAC;
        iac_response[1] = TELNET_DONT;
        iac_response[2] = TELNET_OPT_ECHO;
        kc_send(world, iac_response, 3);
        break;

      case TELNET_OPT_GMCP:
        world->gmcp_enabled = FALSE;
        iac_response[0] = TELNET_IAC;
        iac_response[1] = TELNET_DONT;
        iac_response[2] = TELNET_OPT_GMCP;
        kc_send(world, iac_response, 3);
        break;

      case TELNET_OPT_MSDP:
        world->msdp_enabled = FALSE;
        iac_response[0] = TELNET_IAC;
        iac_response[1] = TELNET_DONT;
        iac_response[2] = TELNET_OPT_MSDP;
        kc_send(world, iac_response, 3);
        break;
      }
      world->telnet_state = TELNETFSM_DATA;
      break;

    case TELNETFSM_SB:
      if (world->dbuffer[pos] == TELNET_IAC) {
        world->telnet_state = TELNETFSM_SB_IAC;
      } else if (world->dbuffer[pos] == TELNET_OPT_TERMINAL_TYPE) {
        world->telnet_state = TELNETFSM_SB_IAC_TERMTYPE;
      } else if (world->dbuffer[pos] == TELNET_OPT_COMPRESS) {
        world->telnet_state = TELNETFSM_SB_IAC_COMPRESS;
      } else if (world->dbuffer[pos] == TELNET_OPT_COMPRESS2) {
        world->telnet_state = TELNETFSM_SB_IAC_COMPRESS2;
      } else if (world->dbuffer[pos] == TELNET_OPT_GMCP
                 || world->dbuffer[pos] == TELNET_OPT_MSDP) {
        world->server_data_protocol = world->dbuffer[pos];
        world->telnet_state = TELNETFSM_SERVER_DATA;
      }
      break;

    case TELNETFSM_SB_IAC:
      if (world->dbuffer[pos] == TELNET_SE) {
        world->telnet_state = TELNETFSM_DATA;
      } else {
        world->telnet_state = TELNETFSM_SB;
      }
      break;

    case TELNETFSM_SB_IAC_TERMTYPE:
      if (world->dbuffer[pos] == TELNET_TERMTYPE_SEND) {
        char *tmp;

        tmp = g_strdup_printf("%c%c%c%c%s%c%c",
                              TELNET_IAC, TELNET_SB,
                              TELNET_OPT_TERMINAL_TYPE, TELNET_TERMTYPE_IS,
                              CLIENT_NAME,
                              TELNET_IAC,
                              TELNET_SE);
        kc_send(world, tmp, CLIENT_NAME_LEN + 6);
        g_free(tmp);
      }
      world->telnet_state = TELNETFSM_SB;
      break;

    case TELNETFSM_SB_IAC_COMPRESS:
      if (world->dbuffer[pos] == TELNET_WILL) {
        world->telnet_state = TELNETFSM_SB_IAC_COMPRESS_WILL;
      } else {
        /* This shouldn't happen, and if it does, it will probably mess
           up everything. But let's hope for the best and wait for an SE */
        world->telnet_state = TELNETFSM_SB_IAC;
      }
      break;

    case TELNETFSM_SB_IAC_COMPRESS2:
      if (world->dbuffer[pos] == TELNET_IAC) {
        world->telnet_state = TELNETFSM_SB_IAC_COMPRESS_WILL;
      } else {
        /* This shouldn't happen, and if it does, it will probably mess
           up everything. But let's hope for the best and wait for an SE */
        world->telnet_state = TELNETFSM_SB_IAC;
      }
      break;

    case TELNETFSM_SB_IAC_COMPRESS_WILL:
      if (world->dbuffer[pos] == TELNET_SE) {
        start_mccp(world, pos, end);
        world->telnet_state = TELNETFSM_DATA;
        return;
      } else {
      /* This shouldn't happen, and if it does, it will probably mess
         up everything. But let's hope for the best and wait for an SE */
        world->telnet_state = TELNETFSM_SB_IAC;
      }
      break;

    case TELNETFSM_SERVER_DATA:
      if (world->dbuffer[pos] == TELNET_IAC) {
        world->telnet_state = TELNETFSM_SERVER_DATA_IAC;
      } else {
        append_to_server_data_buffer(world, world->dbuffer[pos]);
      }
      break;

    case TELNETFSM_SERVER_DATA_IAC:
      if (world->dbuffer[pos] == TELNET_IAC) {
        append_to_server_data_buffer(world, TELNET_IAC);
        world->telnet_state = TELNETFSM_SERVER_DATA;
      } else if (world->dbuffer[pos] == TELNET_SE) {
        process_server_data(world);
        world->telnet_state = TELNETFSM_DATA;
      }
      break;
    }
    ++pos;
  }
}


void
send_naws_size(World *world)
{
  /* 3 for start, 2 for end. Each value is 2 bytes, but may be 4
     if it happens to be 0xFFFF and must be IAC escaped. Thus 8 for both
     values. */
  guchar   buffer[3 + 8 + 2];
  int      bsize;
  guint16  size;
  guint    cols;
  guint    rows;

  ansitextview_get_size(world->gui, &rows, &cols);

  buffer[0] = TELNET_IAC;
  buffer[1] = TELNET_SB;
  buffer[2] = TELNET_OPT_NAWS;
  bsize = 3;

  size = htons((guint16) cols);
  memcpy(buffer + bsize, &size, sizeof(size));
  /* Escape IAC */
  if (buffer[bsize] == TELNET_IAC) {
    buffer[bsize+2] = buffer[bsize+1];
    buffer[bsize+1] = TELNET_IAC;
    ++bsize;
  }
  ++bsize;
  if (buffer[bsize] == TELNET_IAC) {
    buffer[bsize+2] = buffer[bsize+1];
    buffer[bsize+1] = TELNET_IAC;
    ++bsize;
  }
  ++bsize;

  size = htons((guint16) rows);
  memcpy(buffer + bsize, &size, sizeof(size));
  /* Escape IAC */
  if (buffer[bsize] == TELNET_IAC) {
    buffer[bsize+2] = buffer[bsize+1];
    buffer[bsize+1] = TELNET_IAC;
    ++bsize;
  }
  ++bsize;
  if (buffer[bsize] == TELNET_IAC) {
    buffer[bsize+2] = buffer[bsize+1];
    buffer[bsize+1] = TELNET_IAC;
    ++bsize;
  }
  ++bsize;

  buffer[bsize++] = TELNET_IAC;
  buffer[bsize++] = TELNET_SE;
  kc_send(world, buffer, bsize);
}


static
void
append_to_server_data_buffer(World *world, guchar chr)
{
  /* If there are no hooks, don't save the data.

     If there are hooks but they're disable, we save the data,
     because if probably is cheaper to save than to search the array
     to see if the hooks are enabled.
  */
  if (world->hooks.OnServerData == NULL) {
    return;
  }

  /* Resize buffer if necessary. */
  /* TODO: Put a maximum limit? */
  if (world->server_data_size >= world->server_data_alloc_size) {
    world->server_data_alloc_size += MAX_BUFFER;
    world->server_data_buffer = realloc(world->server_data_buffer,
                                        world->server_data_alloc_size);
  }

  world->server_data_buffer[world->server_data_size++] = chr;
}


static
void
process_server_data(World *world)
{
  /* If no hooks would be run, don't bother parsing the data */
  if (!are_there_enabled_server_data_hooks(world)) {
    return;
  }

  char *protocol = NULL;
  if (world->server_data_protocol == TELNET_OPT_GMCP) {
    protocol = "GMCP";
  } else if (world->server_data_protocol == TELNET_OPT_MSDP) {
    protocol = "MSDP";
  }

  /* Shouldn't happen, but it's better to be safe */
  if (!protocol) {
    return;
  }

  append_to_server_data_buffer(world, '\0');
  GRAB_PERL(world);
  parse_server_data(protocol,
                    (char *) world->server_data_buffer);
  RELEASE_PERL(world);

  execute_hook_immediately(world, "OnServerData", NULL);

  world->server_data_size = 0;
}
