/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include "libintl-wrapper.h"
#include <locale.h>
#include <gtk/gtk.h>

#include "kildclient.h"
#include "perlscript.h"

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif


/*************************
 * File global variables *
 *************************/


/***********************
 * Function prototypes *
 ***********************/



gboolean
check_macros(World *world, guint keyval, GdkModifierType modifiers)
{
  GSList *macroptr;
  Macro  *macro;

  if (world->disable_macros) {
    return FALSE;
  }

  macroptr = world->macros;
  while (macroptr) {
    macro = (Macro *) macroptr->data;
    if (macro->enabled &&
        macro->keyval == keyval && macro->modifiers == (modifiers & GDK_MODIFIER_MASK)) {
      parse_commands(world, macro->action);
      return TRUE;
    }
    macroptr = macroptr->next;
  }

  return FALSE;
}


void
remove_macro(World *world, GSList *macroitem)
{
  Macro *macro = (Macro *) macroitem->data;

  we_macro_delete_macro(world, macro);

  world->macros = g_slist_remove_link(world->macros, macroitem);
  if (!macro->owner_plugin) {
    --world->macro_pos;
  }
  free_macro(macro, NULL);
  g_slist_free(macroitem);
}


gboolean
move_macro(World *world, gint old_pos, gint new_pos)
{
  GSList   *macroitem;
  gpointer  macro;

  macroitem = g_slist_nth(world->macros, old_pos);
  if (!macroitem) {
    return FALSE;
  }
  macro = macroitem->data;

  world->macros = g_slist_delete_link(world->macros, macroitem);
  world->macros = g_slist_insert(world->macros, macro, new_pos);

  we_macro_delete_macro(world, macro);
  we_macro_insert_macro(world, macro, new_pos);

  return TRUE;
}


void
free_macro(Macro *macro, gpointer data)
{
  g_free(macro->name);
  g_free(macro->action);

  g_free(macro);
}


void
list_macros(World *world, Plugin *plugin)
{
  guint   i;
  guint   total_width;
  guint   field1_width;
  guint   field2_width;
  GSList *macroptr;
  Macro  *macro;
  char   *keycodestr;

  /* The rows argument is not used. */
  ansitextview_get_size(world->gui, &field1_width, &total_width);

  /* If the screen is really narrow, we can do nothing. */
  if (total_width < 15) {
    total_width = 15;
  }
  field1_width = (total_width - 9) * .3;
  field2_width = (total_width - 9) * .7;

  ansitextview_append_stringf(world->gui,
                                _("Num Ena %-*.*s %-*.*s\n"),
                                field1_width, field1_width, _("Key"),
                                field2_width, field2_width, _("Action"));
  ansitextview_append_string(world->gui, "--- --- ");
  for (i = 0; i < field1_width; ++i) {
    ansitextview_append_string(world->gui, "-");
  }
  ansitextview_append_string(world->gui, " ");
  for (i = 0; i < field2_width; ++i) {
    ansitextview_append_string(world->gui, "-");
  }
  ansitextview_append_string(world->gui, "\n");

  i = 0;
  macroptr = world->macros;
  while (macroptr) {
    macro = (Macro *) macroptr->data;

    if (macro->owner_plugin == plugin) {

      keycodestr = gtk_accelerator_name(macro->keyval, macro->modifiers);
      ansitextview_append_stringf(world->gui,
                                  "%3d %-3.3s %-*.*s %-*.*s\n",
                                  i,
                                  macro->enabled ? _("y") : _("n"),
                                  field1_width, field1_width, keycodestr,
                                  field2_width, field2_width, macro->action);
      g_free(keycodestr);
    }

    ++i;
    macroptr = macroptr->next;
  }
}


void
save_macro(GString *str, Macro *macro)
{
  char *keycodestr;

  keycodestr = gtk_accelerator_name(macro->keyval, macro->modifiers);
  g_string_append(str, "    <macro ");
  if (macro->name) {
    kc_g_string_append_escaped(str, "name=\"%s\" ", macro->name);
  }
  g_string_append_printf(str, "enabled=\"%d\">\n", macro->enabled);
  kc_g_string_append_escaped(str,
                             "      <key>%s</key>\n"
                             "      <action>%s</action>\n"
                             "    </macro>\n",
                             keycodestr,
                             macro->action);

  g_free(keycodestr);
}
