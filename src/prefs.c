/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "libintl-wrapper.h"
#include <glib.h>
#include <glib/gstdio.h>
#include <gtk/gtk.h>
#include <gmodule.h>

#include "kildclient.h"
#include "perlscript.h"


/******************
 * Initialization *
 ******************/
Prefs globalPrefs;


/*************************
 * File Global variables *
 *************************/
#define MAX_OLDSAVE_COLOR 8



/***********************
 * Function prototypes *
 ***********************/
static GString *get_preferences_string(void);
static void     read_global_preferences_from(FILE *fp,
                                             int *x, int *y,
                                             int *width, int *height);
static void     combo_changed_cb(GtkComboBox *combo, gpointer data);
static gboolean txt_cmd_focus_out_cb(GtkWidget     *widget,
                                     GdkEventFocus *event,
                                     gpointer       data);
/* XML UI signals */
G_MODULE_EXPORT void     cmd_group_size_changed_cb(GtkSpinButton *spin,
                                                   gpointer       data);
G_MODULE_EXPORT void     cmd_delay_changed_cb(GtkSpinButton *spin,
                                                   gpointer       data);
G_MODULE_EXPORT void     tab_position_changed_cb(GtkComboBox *combo,
                                                 gpointer data);
G_MODULE_EXPORT void     chk_hide_single_tab_toggled_cb(GtkToggleButton *button,
                                                        gpointer data);
G_MODULE_EXPORT void     chk_urgency_toggled_cb(GtkToggleButton *button,
                                                gpointer data);
G_MODULE_EXPORT void     cmbProxyType_changed_cb(GtkComboBox *combo, gpointer data);


void
show_preferences_dialog_cb(GtkMenuItem *widget, gpointer data)
{
  static GtkWidget *dialog = NULL;
  GtkNotebook      *notebook;
  GObject          *cmbInfo;
  GObject          *cmbCmdEcho;
  GtkComboBox      *cmbTabPos;
  GtkToggleButton  *chkHideSingleTab;
  GtkToggleButton  *chkUrgency;
  GtkSpinButton    *spnGroupSize;
  GtkSpinButton    *spnDelay;
  GtkComboBox      *cmbProxyType;
  GObject          *txtProxyServer;
  GObject          *txtProxyPort;
  GObject          *txtProxyUser;
  GObject          *txtProxyPassword;
#ifndef __MINGW32__
  GObject          *txtAudioPlayerCommand;
#else
  GtkWidget        *tabPrograms;
#endif

  if (!dialog) {
    GError *error = NULL;

    if (!gtk_builder_add_from_resource(main_builder,
                                       "/ekalin/kildclient/dlgPreferences.ui",
                                       &error)) {
      g_warning(_("Error loading UI from XML file: %s"), error->message);
      g_error_free(error);
      return;
    }

    dialog = GTK_WIDGET(gtk_builder_get_object(main_builder, "dlgPreferences"));
    gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(wndMain));
    notebook = GTK_NOTEBOOK(gtk_builder_get_object(main_builder, "ntbkPrefs"));

    /* Combos with colors */
    cmbInfo = gtk_builder_get_object(main_builder, "cmbInfoColor");
    gtk_combo_box_set_active(GTK_COMBO_BOX(cmbInfo),
                             globalPrefs.idxInfoMsgColor);
    g_signal_connect(cmbInfo, "changed",
                     G_CALLBACK(combo_changed_cb),
                     &globalPrefs.idxInfoMsgColor);

    cmbCmdEcho = gtk_builder_get_object(main_builder, "cmbCmdEchoColor");
    gtk_combo_box_set_active(GTK_COMBO_BOX(cmbCmdEcho),
                             globalPrefs.idxCmdEchoColor);
    g_signal_connect(cmbCmdEcho, "changed",
                     G_CALLBACK(combo_changed_cb),
                     &globalPrefs.idxCmdEchoColor);

    /* Load current tab position */
    cmbTabPos = GTK_COMBO_BOX(gtk_builder_get_object(main_builder, "cmbTabPos"));
    gtk_combo_box_set_active(cmbTabPos, globalPrefs.tab_position);

    chkHideSingleTab
      = GTK_TOGGLE_BUTTON(gtk_builder_get_object(main_builder,
                                                 "chkHideSingleTab"));
    gtk_toggle_button_set_active(chkHideSingleTab,
                                 globalPrefs.hide_single_tab);

    /* Load use of urgency hint */
    chkUrgency
      = GTK_TOGGLE_BUTTON(gtk_builder_get_object(main_builder, "chkUrgency"));
    gtk_toggle_button_set_active(chkUrgency, globalPrefs.urgency_hint);

    /* Load delays */
    spnGroupSize
      = GTK_SPIN_BUTTON(gtk_builder_get_object(main_builder, "spnGroupSize"));
    gtk_spin_button_set_value(spnGroupSize, globalPrefs.multi_cmd_group_size);
    spnDelay
      = GTK_SPIN_BUTTON(gtk_builder_get_object(main_builder, "spnPrefsDelay"));
    gtk_spin_button_set_value(spnDelay, globalPrefs.multi_cmd_delay);

    /* Load proxy settings */
    txtProxyServer   = gtk_builder_get_object(main_builder, "txtGProxyServer");
    gtk_entry_set_text(GTK_ENTRY(txtProxyServer), globalPrefs.proxy.server);
    g_signal_connect(txtProxyServer, "focus_out_event",
                     G_CALLBACK(txt_cmd_focus_out_cb),
                     &globalPrefs.proxy.server);
    txtProxyPort     = gtk_builder_get_object(main_builder, "txtGProxyPort");
    gtk_entry_set_text(GTK_ENTRY(txtProxyPort), globalPrefs.proxy.port);
    g_signal_connect(txtProxyPort, "focus_out_event",
                     G_CALLBACK(txt_cmd_focus_out_cb),
                     &globalPrefs.proxy.port);
    txtProxyUser     = gtk_builder_get_object(main_builder, "txtGProxyUser");
    gtk_entry_set_text(GTK_ENTRY(txtProxyUser), globalPrefs.proxy.user);
    g_signal_connect(txtProxyUser, "focus_out_event",
                     G_CALLBACK(txt_cmd_focus_out_cb),
                     &globalPrefs.proxy.user);
    txtProxyPassword = gtk_builder_get_object(main_builder, "txtGProxyPassword");
    gtk_entry_set_text(GTK_ENTRY(txtProxyPassword), globalPrefs.proxy.password);
    g_signal_connect(txtProxyPassword, "focus_out_event",
                     G_CALLBACK(txt_cmd_focus_out_cb),
                     &globalPrefs.proxy.password);
    /* Remove "Use Global Settings" - these are the global settings */
    cmbProxyType
      = GTK_COMBO_BOX(gtk_builder_get_object(main_builder, "cmbGProxyType"));
    gtk_combo_box_set_active(cmbProxyType, globalPrefs.proxy.type);

#ifndef __MINGW32__
    /* Load commands */
    txtAudioPlayerCommand
      = gtk_builder_get_object(main_builder, "txtAudioPlayerCommand");
    gtk_entry_set_text(GTK_ENTRY(txtAudioPlayerCommand),
                       globalPrefs.audio_player_command);
    g_signal_connect(txtAudioPlayerCommand, "focus_out_event",
                     G_CALLBACK(txt_cmd_focus_out_cb),
                     &globalPrefs.audio_player_command);
#else /* defined __MINGW32__ */
    tabPrograms
      = GTK_WIDGET(gtk_builder_get_object(main_builder, "prefs_panel_progs"));
    gtk_notebook_remove_page(notebook,
                             gtk_notebook_page_num(notebook, tabPrograms));
#endif

    gtk_builder_connect_signals(main_builder, NULL);

    /* For some reason the proxy page insists on being the default selected. */
    gtk_widget_show_all(dialog);
    gtk_notebook_set_current_page(notebook, 0);
  }

  gtk_window_present(GTK_WINDOW(dialog));
}


static
void
combo_changed_cb(GtkComboBox *combo, gpointer data)
{
  int *idx = (int *) data;

  *idx = gtk_combo_box_get_active(combo);
}


void cmd_group_size_changed_cb(GtkSpinButton *spin, gpointer data)
{
  globalPrefs.multi_cmd_group_size
    = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(spin));
}


void cmd_delay_changed_cb(GtkSpinButton *spin, gpointer data)
{
  globalPrefs.multi_cmd_delay
    = gtk_spin_button_get_value(GTK_SPIN_BUTTON(spin));
}


void
tab_position_changed_cb(GtkComboBox *combo, gpointer data)
{
  globalPrefs.tab_position = gtk_combo_box_get_active(combo);
  set_notebook_tab_position(globalPrefs.tab_position);
}


void
chk_hide_single_tab_toggled_cb(GtkToggleButton *button, gpointer data)
{
  globalPrefs.hide_single_tab = gtk_toggle_button_get_active(button);
  if (globalPrefs.hide_single_tab) {
    gtk_notebook_set_show_tabs(GTK_NOTEBOOK(ntbkWorlds),
                               gtk_notebook_get_n_pages(GTK_NOTEBOOK(ntbkWorlds)) != 1);
  } else {
    gtk_notebook_set_show_tabs(GTK_NOTEBOOK(ntbkWorlds), TRUE);
  }
}


void
chk_urgency_toggled_cb(GtkToggleButton *button, gpointer data)
{
  globalPrefs.urgency_hint = gtk_toggle_button_get_active(button);
}


static
gboolean
txt_cmd_focus_out_cb(GtkWidget     *widget,
                     GdkEventFocus *event,
                     gpointer       data)
{
  char       **command = (char **) data;
  const char  *new_command;

  new_command = gtk_entry_get_text(GTK_ENTRY(widget));
  if (strcmp(new_command, *command) != 0) {
    g_free(*command);
    *command = g_strdup(new_command);
  }

  return FALSE;
}


void
cmbProxyType_changed_cb(GtkComboBox *combo, gpointer data)
{
  globalPrefs.proxy.type = gtk_combo_box_get_active(combo);
}


void
read_global_preferences(int *x, int *y, int *width, int *height)
{
  const char *kilddir;
  char       *filepath;
  FILE       *fp;

  /* First, fill in defaults (for the case the file cannot be read,
     or some options are missing) */
  globalPrefs.idxInfoMsgColor = 12;
  globalPrefs.idxCmdEchoColor = 2;

  globalPrefs.multi_cmd_group_size = 1;
  globalPrefs.multi_cmd_delay = 0;

  globalPrefs.tab_position    = GTK_POS_TOP;
  globalPrefs.hide_single_tab = FALSE;
  globalPrefs.urgency_hint    = TRUE;

  globalPrefs.proxy.type = PROXY_NONE;

  /* And now try to load from file the actual values */
  kilddir = get_kildclient_directory_path();
  filepath = g_strdup_printf("%s/kildclient.cfg", kilddir);

  fp = fopen(filepath, "r");
  if (fp) {
    read_global_preferences_from(fp, x, y, width, height);
    fclose(fp);
  }

  /* Has the commands been set? */
  if (!globalPrefs.audio_player_command) {
    globalPrefs.audio_player_command = g_strdup("play \"%s\" &");
  }

  /* Have the proxy settings been set? */
  if (!globalPrefs.proxy.server) {
    globalPrefs.proxy.server   = g_strdup("");
  }
  if (!globalPrefs.proxy.port) {
    globalPrefs.proxy.port     = g_strdup("1080");
  }
  if (!globalPrefs.proxy.user) {
    globalPrefs.proxy.user     = g_strdup("");
  }
  if (!globalPrefs.proxy.password) {
    globalPrefs.proxy.password = g_strdup("");
  }

  g_free(filepath);
}


static
void
read_global_preferences_from(FILE *fp,
                             int *x, int *y, int *width, int *height)
{
  char  line[MAX_BUFFER];
  char  first_word[MAX_BUFFER];
  int   pos;

  while (fgets(line, MAX_BUFFER, fp)) {
    line[strlen(line) - 1] = '\0';  /* Remove newline at end */
    pos = strcspn(line, " ");
    memcpy(first_word, line, pos);
    first_word[pos] = '\0';

    if (strcmp(first_word, "newinfomsgcolor") == 0) {
      globalPrefs.idxInfoMsgColor = atoi(line + pos + 1);
    } else if (strcmp(first_word, "newcmdechocolor") == 0) {
      globalPrefs.idxCmdEchoColor = atoi(line + pos + 1);
    } else if (strcmp(first_word, "infomsgcolor") == 0) {
      globalPrefs.idxInfoMsgColor = atoi(line + pos + 1);
      if (globalPrefs.idxInfoMsgColor >= MAX_OLDSAVE_COLOR) {
        ++globalPrefs.idxInfoMsgColor;
      }
    } else if (strcmp(first_word, "cmdechocolor") == 0) {
      globalPrefs.idxCmdEchoColor = atoi(line + pos + 1);
      if (globalPrefs.idxCmdEchoColor >= MAX_OLDSAVE_COLOR) {
        ++globalPrefs.idxCmdEchoColor;
      }
    } else if (strcmp(first_word, "tabposition") == 0) {
      globalPrefs.tab_position = atoi(line + pos + 1);
    } else if (strcmp(first_word, "hidesingletab") == 0) {
      globalPrefs.hide_single_tab = atoi(line + pos + 1);
    } else if (strcmp(first_word, "urgencyhint") == 0) {
      globalPrefs.urgency_hint = atoi(line + pos + 1);
    } else if (strcmp(first_word, "audioplayercommand") == 0) {
      globalPrefs.audio_player_command = g_strdup(line + pos + 1);
    } else if (strcmp(first_word, "lastopenworld") == 0) {
      globalPrefs.last_open_world = g_strdup(line + pos + 1);

    } else if (strcmp(first_word, "multicmdgroupsize") == 0) {
      globalPrefs.multi_cmd_group_size = atoi(line + pos + 1);
    } else if (strcmp(first_word, "multicmddelay") == 0) {
      globalPrefs.multi_cmd_delay = strtod(line + pos + 1, NULL);

    } else if (strcmp(first_word, "window") == 0) {
      if (sscanf(line + pos + 1, "%d %d %d %d",
                 x, y, width, height) != 4) {
        fprintf(stderr, _("Invalid window size specification in config file.\n"));
        *x = -1;        /* It might have been changed */
      }

    } else if (strcmp(first_word, "nopluginhelpmsg") == 0) {
      globalPrefs.no_plugin_help_msg = atoi(line + pos + 1);

    } else if (strcmp(first_word, "proxytype") == 0) {
      globalPrefs.proxy.type = atoi(line + pos + 1);
    } else if (strcmp(first_word, "proxyserver") == 0) {
      globalPrefs.proxy.server = g_strdup(line + pos + 1);
    } else if (strcmp(first_word, "proxyport") == 0) {
      globalPrefs.proxy.port = g_strdup(line + pos + 1);
    } else if (strcmp(first_word, "proxyuser") == 0) {
      globalPrefs.proxy.user = g_strdup(line + pos + 1);
    } else if (strcmp(first_word, "proxypassword") == 0) {
      globalPrefs.proxy.password = g_strdup(line + pos + 1);

    } else {
      fprintf(stderr,
              _("Unknown directive '%s' in config file.\n"), first_word);
    }
  }
}


void
save_global_preferences(void)
{
  const char *dirpath;
  char       *filepath;
  GString    *file_contents;
  GError     *error = NULL;

  file_contents = get_preferences_string();

  dirpath = create_kildclient_directory();
  filepath = g_strdup_printf("%s/kildclient.cfg", dirpath);
  if (!g_file_set_contents(filepath,
                           file_contents->str, file_contents->len,
                           &error)) {
    kc_error_dialog(GTK_WINDOW(wndMain),
                    _("Could not open file '%s': %s"),
                    filepath, error->message);
    g_error_free(error);
  }

  g_free(filepath);
  g_string_free(file_contents, TRUE);
}


static
GString *
get_preferences_string(void)
{
  GString *str;
  int x, y, width, height;

  str = g_string_sized_new(512);

  g_string_append_printf(str,
                         "newinfomsgcolor %d\n", globalPrefs.idxInfoMsgColor);
  g_string_append_printf(str,
                         "newcmdechocolor %d\n", globalPrefs.idxCmdEchoColor);

  g_string_append_printf(str, "tabposition %d\n", globalPrefs.tab_position);
  g_string_append_printf(str, "hidesingletab %d\n",
                         globalPrefs.hide_single_tab);
  g_string_append_printf(str, "urgencyhint %d\n", globalPrefs.urgency_hint);

  g_string_append_printf(str,
                         "audioplayercommand %s\n",
                         globalPrefs.audio_player_command);

  g_string_append_printf(str,
                         "lastopenworld %s\n", globalPrefs.last_open_world);

  g_string_append_printf(str,
                         "multicmdgroupsize %d\n",
                         globalPrefs.multi_cmd_group_size);
  g_string_append_printf(str,
                         "multicmddelay %f\n", globalPrefs.multi_cmd_delay);

  g_string_append_printf(str,
                         "nopluginhelpmsg %d\n",
                         globalPrefs.no_plugin_help_msg);

  gtk_window_get_position(GTK_WINDOW(wndMain), &x, &y);
  gtk_window_get_size(GTK_WINDOW(wndMain), &width, &height);
  g_string_append_printf(str, "window %d %d %d %d\n", x, y, width, height);

  g_string_append_printf(str, "proxytype %d\n",     globalPrefs.proxy.type);
  g_string_append_printf(str, "proxyserver %s\n",   globalPrefs.proxy.server);
  g_string_append_printf(str, "proxyport %s\n",     globalPrefs.proxy.port);
  g_string_append_printf(str, "proxyuser %s\n",     globalPrefs.proxy.user);
  g_string_append_printf(str, "proxypassword %s\n", globalPrefs.proxy.password);

  return str;
}


const char *
create_kildclient_directory(void)
{
  const char *kilddir;
  struct stat stinfo;

  kilddir = get_kildclient_directory_path();

  if (stat(kilddir, &stinfo) == -1) {
    if (g_mkdir(kilddir, S_IRUSR | S_IWUSR | S_IXUSR) == -1) {
      kc_error_dialog(GTK_WINDOW(wndMain),
                      _("Could not create directory '%s': %s"),
                      kilddir, g_strerror(errno));
      return NULL;
    }
  }

  return kilddir;
}
