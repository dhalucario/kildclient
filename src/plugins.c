/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include "libintl-wrapper.h"
#include <locale.h>
#include <gtk/gtk.h>

#include "kildclient.h"
#include "perlscript.h"


/*************************
 * File global variables *
 *************************/


/***********************
 * Function prototypes *
 ***********************/
static int plugin_name_comparer(const Plugin *plugin, const char *name);



Plugin *
get_plugin(World *world, const char *name)
{
  GSList *pluginlst;

  pluginlst = g_slist_find_custom(world->plugins,
                                  name,
                                  (GCompareFunc) plugin_name_comparer);

  if (pluginlst) {
    return pluginlst->data;
  } else {
    return NULL;
  }
}


static
int
plugin_name_comparer(const Plugin *plugin, const char *name)
{
  return strcmp(plugin->name, name);
}


void
free_plugin(Plugin *plugin, World *world)
{
  gchar *unload_command;

  unload_command = g_strdup_printf("%s::UNLOAD", plugin->name);
  eval_pv(unload_command, FALSE);
  g_free(unload_command);

  g_free(plugin->name);
  g_free(plugin->version);
  g_free(plugin->description);

  g_free(plugin);
}


gboolean
load_plugin(World *world, gchar *filename, GError **error)
{
  SV          *command;
  SV          *ret;

  GRAB_PERL(world);
  command = newSVpvf("($_error, $_pname) = $::world->_loadplugin('%s');",
                     filename);
  eval_sv(command, G_ARRAY);
  SvREFCNT_dec(command);
  ret = get_sv("_error", FALSE);
  if (SvOK(ret)) {
    g_set_error(error,
                G_FILE_ERROR, G_FILE_ERROR_NOENT,
                _("Failed loading file %s: %s"),
                filename, SvPV_nolen(ret));
    RELEASE_PERL(world);
    return FALSE;
  }

  RELEASE_PERL(world);
  return TRUE;
}


/* Startup plugins support */
void
load_startup_plugins(World *world)
{
  GSList *pluginitem;

  pluginitem = world->startup_plugin_files;
  while (pluginitem) {
    GError *error = NULL;

    add_startup_plugin(world, pluginitem->data, &error);
    if (error) {
      fputs(error->message, stderr);
      fputc('\n', stderr);
      g_error_free(error);
    } else {
      gchar *cmd = g_strdup_printf("$::world->loadplugin('%s');",
                                   (gchar *) pluginitem->data);
      eval_pv(cmd, TRUE);
      g_free(cmd);
    }

    g_free(pluginitem->data);
    pluginitem = pluginitem->next;
  }
  g_slist_free(world->startup_plugin_files);
}


gboolean
add_startup_plugin(World *world, gchar *filename, GError **error)
{
  SV          *command;
  SV          *ret;
  gchar       *name;
  gchar       *description;
  gchar       *author;
  GtkTreeIter  iter;

  GRAB_PERL(world);
  command = newSVpvf("($_error, $_name, $_vers, $_desc, $_author) = $::world->_getplugininfo('%s');",
                     filename);
  eval_sv(command, G_ARRAY);
  SvREFCNT_dec(command);
  ret = get_sv("_error", FALSE);
  if (SvOK(ret)) {
    g_set_error(error,
                G_FILE_ERROR, G_FILE_ERROR_NOENT,
                _("Failed loading file %s: %s"),
                filename, SvPV_nolen(ret));
    RELEASE_PERL(world);
    return FALSE;
  }

  ret  = get_sv("_name", FALSE);
  name = SvPV_nolen(ret);

  ret         = get_sv("_desc", FALSE);
  description = SvPV_nolen(ret);

  ret    = get_sv("_author", FALSE);
  author = SvPV_nolen(ret);

  RELEASE_PERL(world);

  gtk_list_store_append(GTK_LIST_STORE(world->startup_plugins), &iter);
  gtk_list_store_set(GTK_LIST_STORE(world->startup_plugins), &iter,
                     SPLUGIN_FILE, filename,
                     SPLUGIN_NAME, name,
                     SPLUGIN_DESCRIPTION, description,
                     SPLUGIN_AUTHOR, author,
                     -1);
  return TRUE;
}


void
make_startup_plugin_list_for_editing(World *world)
{
  GSList      *pluginitem;
  GtkTreeIter  iter;

  pluginitem = world->startup_plugin_files;
  while (pluginitem) {
    gtk_list_store_append(GTK_LIST_STORE(world->startup_plugins), &iter);
    gtk_list_store_set(GTK_LIST_STORE(world->startup_plugins), &iter,
                       SPLUGIN_FILE, pluginitem->data,
                       -1);

    g_free(pluginitem->data);
    pluginitem = pluginitem->next;
  }
  g_slist_free(world->startup_plugin_files);
}
