/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include "libintl-wrapper.h"
#include <glib.h>
#include <gtk/gtk.h>
#include <gmodule.h>
#ifndef __MINGW32__
#  include <netinet/in.h>
#  include <netdb.h>
#else
#  include <winsock2.h>
#endif
#include <gnutls/gnutls.h>
#include <gnutls/x509.h>
#include <gnutls/openpgp.h>

#include "kildclient.h"
#include "perlscript.h"


/******************
 * Initialization *
 ******************/
World      *currentWorld         = NULL;
GtkBuilder *main_builder         = NULL;
GtkWidget  *wndMain              = NULL;
GtkWidget  *ntbkWorlds           = NULL;
GList      *open_worlds          = NULL;
gboolean    window_has_focus     = TRUE;
gint        worlds_with_new_text = 0;
gboolean    debug_matches        = 0;


/***********************
 * Function prototypes *
 ***********************/
static void     restore_main_window_size(int x, int y, int width, int height,
                                         WorldGUI *gui);
static void     set_initial_window_size(WorldGUI *gui);
static gboolean update_world_with_focus(gpointer data);
static gint     attach_world_gui(WorldGUI *gui);
static void     detach_world_gui(WorldGUI *gui);
static gchar   *format_time_string(World *world, int ctime, int itime);
static void     format_hms_time(GString *str, int seconds);
/* XMl UI callbacks */
G_MODULE_EXPORT gboolean exit_cb(GtkWidget *widget,
                                 GdkEvent *event,
                                 gpointer data);
G_MODULE_EXPORT gboolean window_focus_in_cb(GtkWidget     *widget,
                                            GdkEventFocus *evt,
                                            gpointer       data);
G_MODULE_EXPORT gboolean window_focus_out_cb(GtkWidget     *widget,
                                             GdkEventFocus *evt,
                                             gpointer       data);
G_MODULE_EXPORT void     notebook_page_changed_cb(GtkNotebook *notebook,
                                                  gpointer    *page,
                                                  guint        page_num,
                                                  gpointer     data);
G_MODULE_EXPORT void     notebook_page_reordered_cb(GtkNotebook *notebook,
                                                    GtkWidget   *page,
                                                    guint        page_num,
                                                    gpointer     data);
G_MODULE_EXPORT gboolean about_dlg_url_hook(GtkAboutDialog *about,
                                            gchar          *link,
                                            gpointer        data);
/* Menus */
G_MODULE_EXPORT void     menu_world_open_cb(GtkMenuItem *widget, gpointer data);
G_MODULE_EXPORT void     open_new_world_cb(GtkMenuItem *widget, gpointer data);
G_MODULE_EXPORT void     edit_this_world_cb(GtkMenuItem *widget, gpointer data);
G_MODULE_EXPORT void     menu_statistics_cb(GtkMenuItem *widget, gpointer data);

G_MODULE_EXPORT void     menu_edit_open_cb(GtkMenuItem *widget, gpointer data);
G_MODULE_EXPORT void     menu_cut_activate_cb(GtkMenuItem *widget,
                                              gpointer     data);
G_MODULE_EXPORT void     menu_copy_activate_cb(GtkMenuItem *widget,
                                               gpointer     data);
G_MODULE_EXPORT void     menu_paste_activate_cb(GtkMenuItem *widget,
                                                gpointer     data);
G_MODULE_EXPORT void     menu_delete_activate_cb(GtkMenuItem *widget,
                                                 gpointer     data);
G_MODULE_EXPORT void     menu_find_activate_cb(GtkMenuItem   *widget,
                                               gpointer       data);

G_MODULE_EXPORT void     menu_input_clear_cb(GtkMenuItem *widget,
                                             gpointer     data);
G_MODULE_EXPORT void     menu_input_prev_cb(GtkMenuItem *widget, gpointer data);
G_MODULE_EXPORT void     menu_input_next_cb(GtkMenuItem *widget, gpointer data);
G_MODULE_EXPORT void     menu_input_find_prev_cb(GtkMenuItem *widget,
                                                 gpointer     data);
G_MODULE_EXPORT void     menu_input_find_next_cb(GtkMenuItem *widget,
                                                 gpointer     data);


G_MODULE_EXPORT void     menu_preferences_open_cb(GtkMenuItem *widget,
                                                  gpointer data);
G_MODULE_EXPORT void     menu_disable_triggers_cb(GtkMenuItem *widget,
                                                  gpointer data);
G_MODULE_EXPORT void     menu_disable_aliases_cb(GtkMenuItem *widget,
                                                 gpointer     data);
G_MODULE_EXPORT void     menu_disable_macros_cb(GtkMenuItem *widget,
                                                gpointer     data);
G_MODULE_EXPORT void     menu_disable_timers_cb(GtkMenuItem *widget,
                                                gpointer     data);
G_MODULE_EXPORT void     menu_debug_matches_cb(GtkMenuItem *widget,
                                               gpointer     data);
G_MODULE_EXPORT void     menu_edit_default_world_cb(GtkMenuItem *widget,
                                                    gpointer     data);

G_MODULE_EXPORT void     menu_display_open_cb(GtkMenuItem *widget,
                                              gpointer     data);
G_MODULE_EXPORT void     menu_split_screen_cb(GtkMenuItem *widget,
                                              gpointer     data);

G_MODULE_EXPORT void     menu_manual_cb(GtkMenuItem *widget, gpointer data);
G_MODULE_EXPORT void     menu_about_cb(GtkMenuItem *widget, gpointer data);



GtkWidget*
create_main_window(WorldGUI *gui)
{
  GError     *error = NULL;
  GtkWidget  *window;
  GObject    *mnuReconnect;
  GObject    *mnuDisconnect;
  GObject    *mnuConnectAnother;
  GObject    *mnuSave;
  GObject    *mnuClose;
  GObject    *mnuPrevious;
  GObject    *mnuNext;
  GObject    *mnuQuit;

  main_builder = gtk_builder_new();
  if (!gtk_builder_add_from_resource(main_builder,
                                     "/ekalin/kildclient/wndmain.ui",
                                     &error)) {
    g_warning(_("Error loading UI from XML file: %s"), error->message);
    g_error_free(error);
    exit(1); /* No point in continuing */
  }
  gtk_builder_connect_signals(main_builder, NULL);

  window = GTK_WIDGET(gtk_builder_get_object(main_builder, "wndMain"));
#ifndef __MINGW32__
  gtk_window_set_default_icon_from_file(SYSDATADIR "/pixmaps/kildclient.png",
                                        NULL);
#else /* defined __MINGW32__ */
  gtk_window_set_default_icon_from_file(get_kildclient_installed_file("kildclient.png"),
                                        NULL);
#endif /* __MINGW32__ */

  /* User interface */
  ntbkWorlds = GTK_WIDGET(gtk_builder_get_object(main_builder, "ntbkWorlds"));
  gtk_notebook_set_tab_pos(GTK_NOTEBOOK(ntbkWorlds),
                           globalPrefs.tab_position);

  attach_world_gui(gui);

  gtk_widget_grab_focus(GTK_WIDGET(gui->cmbEntry));

  /* These signals apparently don't work from the XML file */
  mnuReconnect = gtk_builder_get_object(main_builder, "mnuReconnect");
  g_signal_connect(mnuReconnect, "activate",
                   G_CALLBACK(menu_perl_run_cb), "$world->reconnect()");
  mnuDisconnect = gtk_builder_get_object(main_builder, "mnuDisconnect");
  g_signal_connect(mnuDisconnect, "activate",
                   G_CALLBACK(menu_perl_run_cb), "$world->dc()");
  mnuConnectAnother = gtk_builder_get_object(main_builder, "mnuConnectAnother");
  g_signal_connect(mnuConnectAnother, "activate",
                   G_CALLBACK(menu_perl_run_cb), "$world->connectother()");
  mnuSave = gtk_builder_get_object(main_builder, "mnuSave");
  g_signal_connect(mnuSave, "activate",
                   G_CALLBACK(menu_perl_run_cb), "$world->save()");
  mnuClose = gtk_builder_get_object(main_builder, "mnuClose");
  g_signal_connect(mnuClose, "activate",
                   G_CALLBACK(menu_perl_run_cb), "$world->close()");
  mnuPrevious = gtk_builder_get_object(main_builder, "mnuPrevious");
  g_signal_connect(mnuPrevious, "activate",
                   G_CALLBACK(menu_perl_run_cb), "$world->prev()");
  mnuNext = gtk_builder_get_object(main_builder, "mnuNext");
  g_signal_connect(mnuNext, "activate",
                   G_CALLBACK(menu_perl_run_cb), "$world->next()");
  mnuQuit = gtk_builder_get_object(main_builder, "mnuQuit");
  g_signal_connect(mnuQuit, "activate",
                   G_CALLBACK(menu_perl_run_cb), "quit");

  return window;
}


void
set_main_window_size(int x, int y, int width, int height, WorldGUI *gui)
{
  if (x != -1) {
    restore_main_window_size(x, y, width, height, gui);
  } else {
    set_initial_window_size(gui);
  }
}


static
void
restore_main_window_size(int x, int y, int width, int height, WorldGUI *gui)
{
  gtk_window_move(GTK_WINDOW(wndMain), x, y);
  gtk_window_resize(GTK_WINDOW(wndMain), width, height);
}


static
void
set_initial_window_size(WorldGUI *gui)
{
  PangoLayout          *layout;
  PangoFontDescription *font;
  PangoRectangle        logical_rect;
  gint                  char_width, char_height;
  gint                  tv_width, tv_height;
  GtkRequisition        window_size;

  /* At this moment, calling ansitextview_get_char_size() results in an
     invalid value, probably because the GtkTextView is not yet display
     on screen. So we set the default font manually. */
  layout = gtk_widget_create_pango_layout(GTK_WIDGET(gui->txtView), "W");
  font = pango_font_description_from_string(DEFAULT_TERMINAL_FONT);
  pango_layout_set_font_description(layout, font);
  pango_layout_get_pixel_extents(layout, NULL, &logical_rect);
  pango_font_description_free(font);
  g_object_unref(layout);
  char_width  = logical_rect.width;
  char_height = logical_rect.height;
#ifndef __MINGW32__
  /* On Linux, I observed the result to be 1 pixel larger than the
     actual size, but this didn't happen under Window. */
  char_width  -= 1;
  char_height -= 1;
#endif

  /* Set a minimum size of 80x24 for the main MUD area */
  tv_width  = 80 * char_width  + 1;
  tv_height = 24 * char_height + 1;
  gtk_scrolled_window_set_min_content_width(gui->scrolled_win, tv_width);
  gtk_scrolled_window_set_min_content_height(gui->scrolled_win, tv_height);

  /* Find out what size the window would be with a MUD area that size */
  gtk_widget_get_preferred_size(wndMain, &window_size, NULL);

  /* Remove the constraint on the MUD area size, so that the user can
     shrink it if he wishes */
  gtk_scrolled_window_set_min_content_width(gui->scrolled_win, tv_width / 3);
  gtk_scrolled_window_set_min_content_height(gui->scrolled_win, tv_height / 3);

  /* But make the window start at the size it would have for a 80x24 area,
     with a little extra width (without it the size would be a couple
     pixels smaller than necessary_. */
  gtk_window_set_default_size(GTK_WINDOW(wndMain),
                              window_size.width + char_width,
                              window_size.height);
}


void
add_global_css()
{
  GtkCssProvider *provider;
  GdkDisplay     *display;
  GdkScreen      *screen;

  gchar *css =
    "menu {"              /* Use system font in popup menus, */
    " font: initial;"     /* instead of the mud output font. */
    "}";

  provider = gtk_css_provider_new();
  gtk_css_provider_load_from_data(provider, css, -1, NULL);

  display = gdk_display_get_default();
  screen = gdk_display_get_default_screen(display);
  gtk_style_context_add_provider_for_screen(screen,
                                            GTK_STYLE_PROVIDER(provider),
                                            GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
}


gboolean
exit_cb(GtkWidget *widget, GdkEvent *event, gpointer data)
{
  if (confirm_quit()) {
    gtk_main_quit();
  }

  /* Never let it propagate the event, since gtk_main_quit() deals with
     exiting. */
  return TRUE;
}


gboolean
window_focus_in_cb(GtkWidget     *widget,
                   GdkEventFocus *evt,
                   gpointer       data)
{
  if (!currentWorld)
    return FALSE;

  currentWorld->has_focus = TRUE;
  if (currentWorld->has_unread_text) {
    /* If in offline mode, then the tab is greyed. Let's not change it. */
    if (currentWorld->connected) {
      gtk_label_set_text(currentWorld->gui->lblNotebook,
                         currentWorld->display_name);
    }
    currentWorld->has_unread_text = FALSE;
    --worlds_with_new_text;
  }

  if (currentWorld->selection_start || currentWorld->selection_end) {
    simo_combo_box_select_region(currentWorld->gui->cmbEntry,
                                 currentWorld->selection_start,
                                 currentWorld->selection_end);
  }

  window_has_focus = TRUE;
  adjust_window_title();
  if (globalPrefs.urgency_hint) {
    gtk_window_set_urgency_hint(GTK_WINDOW(wndMain), FALSE);
  }

  if (currentWorld->connected) {
    execute_hook(currentWorld, "OnGetFocus", NULL);
  }

  return FALSE;
}


gboolean
window_focus_out_cb(GtkWidget     *widget,
                    GdkEventFocus *evt,
                    gpointer       data)
{
  if (!currentWorld)
    return FALSE;

  currentWorld->has_focus = FALSE;

  simo_combo_box_get_selection_bounds(currentWorld->gui->cmbEntry,
                                      &currentWorld->selection_start,
                                      &currentWorld->selection_end);

  window_has_focus = FALSE;
  adjust_window_title();

  if (currentWorld->connected) {
    execute_hook(currentWorld, "OnLoseFocus", NULL);
  }

  return FALSE;
}


void
adjust_window_title(void)
{
  gchar *worldname;
  gchar *title;

  if (currentWorld) {
    if (currentWorld->connected) {
      worldname = g_strdup_printf(" - %s", currentWorld->display_name);
    } else {
      worldname = g_strdup_printf(" - [%s]", currentWorld->display_name);
    }
  } else {
    worldname = g_strdup("");
  }

  if (window_has_focus || worlds_with_new_text == 0) {
    title = g_strdup_printf("KildClient%s", worldname);
  } else if (worlds_with_new_text == 1) {
    title = g_strdup_printf("(*) KildClient%s", worldname);
  } else {
    title = g_strdup_printf("(*)[%d] KildClient%s",
                            worlds_with_new_text, worldname);
  }

  gtk_window_set_title(GTK_WINDOW(wndMain), title);
  g_free(worldname);
  g_free(title);

  if (!window_has_focus) {
    if (globalPrefs.urgency_hint) {
      gtk_window_set_urgency_hint(GTK_WINDOW(wndMain), worlds_with_new_text);
    }
  }
}


void
notebook_page_changed_cb(GtkNotebook *notebook,
                         gpointer    *page,
                         guint        page_num,
                         gpointer     data)
{
  World *old_world = NULL;
  gint   old_page;

  old_page = gtk_notebook_get_current_page(notebook);
  if (old_page != - 1) {
    old_world = g_list_nth_data(open_worlds, old_page);
  }

  if (old_world) {
    old_world->has_focus      = FALSE;
    old_world->is_current_tab = FALSE;

    if(old_world->connected) {
      execute_hook(old_world, "OnLoseFocus", NULL);
    }
  }

  /* Updates currentWorld, handles focus, etc.
     Called as idle because the change may result from a call to a Perl
     function, and this changes the current Perl interpreter. */
  g_idle_add(update_world_with_focus, GINT_TO_POINTER(page_num));
}


void
notebook_page_reordered_cb(GtkNotebook *notebook,
                           GtkWidget   *widget,
                           guint        new_pos,
                           gpointer     data)
{
  World    *world;
  WorldGUI *gui;
  gint      old_pos;
  GList    *sucessor;

  gui = (WorldGUI *) g_object_get_data(G_OBJECT(widget), "gui");
  world = gui->world;

  old_pos = g_list_index(open_worlds, world);

  sucessor = g_list_nth(open_worlds, new_pos);
  if (old_pos < new_pos) {
    sucessor = g_list_next(sucessor);
  }

  open_worlds = g_list_remove(open_worlds, world);
  open_worlds = g_list_insert_before(open_worlds, sucessor, world);
}


static
gboolean
update_world_with_focus(gpointer data)
{
  gint n = GPOINTER_TO_INT(data);

  /* Now we make it point to the new world */
  currentWorld = (World *) g_list_nth_data(open_worlds, n);
  world_for_perl = currentWorld;
  if (currentWorld) {
    currentWorld->has_focus      = window_has_focus;
    currentWorld->is_current_tab = TRUE;
    if (currentWorld->has_unread_text) {
      gtk_label_set_text(currentWorld->gui->lblNotebook,
                         currentWorld->display_name);
      currentWorld->has_unread_text = FALSE;
      --worlds_with_new_text;
    }

    adjust_window_title();

    if (currentWorld->connected) {
      execute_hook(currentWorld, "OnGetFocus", NULL);
    }
  }

  /* Do not run again */
  return FALSE;
}


void
open_new_world_cb(GtkMenuItem *widget, gpointer data)
{
  open_new_world(NULL, NULL, FALSE, NULL);
}


gboolean
open_new_world(WorldGUI    *gui,
               const gchar *cmdline_arg,
               gboolean     can_offline,
               gboolean    *offline_selected)
{
  World    *world = NULL;
  gint      page = 0;
  gboolean  our_gui = FALSE;

  if (!gui) {
    gui = world_gui_new(TRUE);
    page = attach_world_gui(gui);
    our_gui = TRUE;
  }

  if (cmdline_arg) {
    world = get_world_from_cmdline(cmdline_arg);
  } else {
    world = get_world_to_connect();
  }

  if (world) {
    world->gui = gui;
    gui->world = world;
    ansitextview_update_color_tags(gui, gui->world);
    connect_to(world);
    set_focused_world(page);
    world->has_focus      = TRUE;
    world->is_current_tab = TRUE;
    gtk_widget_grab_focus(GTK_WIDGET(world->gui->cmbEntry));
    return TRUE;
  }

  if (our_gui) {
    detach_world_gui(gui);
    free_world_gui(gui);
  }
  return FALSE;
}


void
edit_this_world_cb(GtkMenuItem *widget, gpointer data)
{
  gboolean dummy;

  edit_world(&currentWorld, GTK_WINDOW(wndMain), &dummy, FALSE);

  gtk_label_set_text(currentWorld->gui->lblNotebook,
                     currentWorld->display_name);
}


void
menu_statistics_cb(GtkMenuItem *widget, gpointer data)
{
  GtkBuilder *ui_builder;
  GError     *error = NULL;
  GtkWidget  *dlgStatistics;
  GtkLabel   *lblName;
  GtkLabel   *lblHost;
  GtkLabel   *lblPort;
  GtkLabel   *lblProxyType;
  GtkLabel   *lblProxyHost;
  GtkLabel   *lblProxyPort;
  GtkLabel   *lblConnTime;
  GtkLabel   *lblIdleTime;
  GtkLabel   *lblCompression;
  GtkLabel   *lblRawBytes;
  GtkLabel   *lblBytes;
  GtkLabel   *lblCRatio;
  GtkLabel   *lblLines;
  gchar      *str;
  time_t      now;
  GString    *timestr;

  ui_builder = gtk_builder_new();
  if (!gtk_builder_add_from_resource(ui_builder,
                                     "/ekalin/kildclient/dlgStatistics.ui",
                                     &error)) {
    g_warning(_("Error loading UI from XML file: %s"), error->message);
    g_error_free(error);
    return;
  }

  dlgStatistics  = GTK_WIDGET(gtk_builder_get_object(ui_builder,
                                                    "dlgStatistics"));
  gtk_window_set_transient_for(GTK_WINDOW(dlgStatistics), GTK_WINDOW(wndMain));
  lblName        = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblName"));
  lblHost        = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblHost"));
  lblPort        = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblPort"));
  lblConnTime    = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblConnTime"));
  lblIdleTime    = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblIdleTime"));
  lblCompression = GTK_LABEL(gtk_builder_get_object(ui_builder,
                                                   "lblCompression"));
  lblRawBytes    = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblRawBytes"));
  lblBytes       = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblBytes"));
  lblCRatio      = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblCRatio"));
  lblLines       = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblLines"));

  /* Basic information */
  gtk_label_set_text(lblName, currentWorld->name);
  if (currentWorld->proxy_used.type == PROXY_NONE && currentWorld->host_ip) {
    str = g_strdup_printf("%s (%s)", currentWorld->host, currentWorld->host_ip);
  } else {
    str = g_strdup(currentWorld->host);
  }
  gtk_label_set_text(lblHost, str);
  g_free(str);
  gtk_label_set_text(lblPort, currentWorld->port);

  /* Proxy information */
  if (currentWorld->proxy_used.type != PROXY_NONE) {
    lblProxyType
      = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblProxyType"));
    lblProxyHost
      = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblProxyHost"));
    lblProxyPort
      = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblProxyPort"));

    if (currentWorld->proxy_used.type == PROXY_SOCKS4) {
      gtk_label_set_text(lblProxyType, "SOCKS4");
    } else if (currentWorld->proxy_used.type == PROXY_SOCKS5) {
      gtk_label_set_text(lblProxyType, "SOCKS5");
    }

    gtk_label_set_text(lblProxyHost, currentWorld->proxy_used.server);
    gtk_label_set_text(lblProxyPort, currentWorld->proxy_used.port);
  }

  if (currentWorld->connected) {
    now = time(NULL);
    timestr = g_string_new(NULL);
    format_hms_time(timestr, difftime(now, currentWorld->connected_time));
    gtk_label_set_text(lblConnTime, timestr->str);
    g_string_truncate(timestr, 0);
    format_hms_time(timestr, difftime(now, currentWorld->last_command_time));
    gtk_label_set_text(lblIdleTime, timestr->str);
    g_string_free(timestr, TRUE);
  } else {
    gtk_label_set_text(lblConnTime, _("Not connected"));
    gtk_label_set_text(lblIdleTime, _("Not connected"));
  }

  /* Compression Information */
  if (currentWorld->zstream) {
    str = g_strdup_printf("MCCP version %d", currentWorld->mccp_ver);
    gtk_label_set_text(lblCompression, str);
    g_free(str);
    str = g_strdup_printf("%ld", currentWorld->rawbytes);
    gtk_label_set_text(lblRawBytes, str);
    g_free(str);
    str = g_strdup_printf("%ld", currentWorld->bytes);
    gtk_label_set_text(lblBytes, str);
    g_free(str);
    str = g_strdup_printf("%.2f:1",
                          (double) currentWorld->bytes/(double) currentWorld->rawbytes);
    gtk_label_set_text(lblCRatio, str);
    g_free(str);
  } else {
    str = g_strdup_printf("%ld", currentWorld->rawbytes);
    gtk_label_set_text(lblBytes, str);
    g_free(str);
  }

  str = g_strdup_printf("%ld",
                        currentWorld->deleted_lines
                          + gtk_text_buffer_get_line_count(currentWorld->gui->txtBuffer));
  gtk_label_set_text(lblLines, str);
  g_free(str);

  /* TLS information */
  if (currentWorld->use_tls) {
    GtkLabel                  *lblTLS;
    GtkLabel                  *lblTLSCipher;
    GtkLabel                  *lblTLSMAC;
    GtkLabel                  *lblTLSKX;
    GtkLabel                  *lblTLSCertType;
    GtkLabel                  *lblTLSCertHostname;
    GtkLabel                  *lblTLSCertActivation;
    GtkLabel                  *lblTLSCertExpiration;
    GtkLabel                  *lblTLSCertFingerprint;
    gnutls_certificate_type_t  cert_type;
    const gnutls_datum_t      *cert_list;
    guint                      cert_list_size;
    gboolean                   hostname_matches = FALSE;
    time_t                     act_time;
    time_t                     exp_time;
    gchar                      digest[20];
    size_t                     digest_size;
    const gchar               *val;
    gchar                     *mval;
    gchar                      buf[MAX_BUFFER];
    guint                      i;

    lblTLS = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblTLS"));
    val = gnutls_protocol_get_name(gnutls_protocol_get_version(currentWorld->tls_session));
    gtk_label_set_text(lblTLS, val);

    lblTLSCipher
      = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblTLSCipher"));
    val = gnutls_cipher_get_name(gnutls_cipher_get(currentWorld->tls_session));
    gtk_label_set_text(lblTLSCipher, val);

    lblTLSMAC = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblTLSMAC"));
    val = gnutls_mac_get_name(gnutls_mac_get(currentWorld->tls_session));
    gtk_label_set_text(lblTLSMAC, val);

    lblTLSKX = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblTLSKX"));
    val = gnutls_kx_get_name(gnutls_kx_get(currentWorld->tls_session));
    gtk_label_set_text(lblTLSKX, val);

    lblTLSCertType = GTK_LABEL(gtk_builder_get_object(ui_builder,
                                                     "lblTLSCertType"));
    cert_type = gnutls_certificate_type_get(currentWorld->tls_session);
    val = gnutls_certificate_type_get_name(cert_type);
    gtk_label_set_text(lblTLSCertType, val);

    /* Certificate/key information */
    lblTLSCertHostname
      = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblTLSCertHostname"));
    lblTLSCertActivation
      = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblTLSCertActivation"));
    lblTLSCertExpiration
      = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblTLSCertExpiration"));
    lblTLSCertFingerprint
      = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblTLSCertFingerprint"));
    cert_list = gnutls_certificate_get_peers(currentWorld->tls_session,
                                             &cert_list_size);
    if (cert_type == GNUTLS_CRT_X509) {
      GtkLabel          *lblTLSCertSubDN;
      GtkLabel          *lblTLSCertIssDN;
      gnutls_x509_crt_t  crt;
      size_t             size;

      if (cert_list_size > 0) {   /* Note that we consider only the first */
        gnutls_x509_crt_init(&crt);
        gnutls_x509_crt_import(crt, &cert_list[0], GNUTLS_X509_FMT_DER);

        hostname_matches = gnutls_x509_crt_check_hostname(crt,
                                                          currentWorld->host);

        act_time = gnutls_x509_crt_get_activation_time(crt);
        exp_time = gnutls_x509_crt_get_expiration_time(crt);

        now = time(NULL);
        strftime(buf, MAX_BUFFER, "%c", localtime(&act_time));
        if (act_time > now) {
          strcat(buf, _(": Not yet active"));
        }
        gtk_label_set_text(lblTLSCertActivation, buf);
        strftime(buf, MAX_BUFFER, "%c", localtime(&exp_time));
        if (exp_time < now) {
          strcat(buf, _(": Expired"));
        }
        gtk_label_set_text(lblTLSCertExpiration, buf);

        digest_size = sizeof(digest);
        gnutls_x509_crt_get_fingerprint(crt, GNUTLS_DIG_MD5,
                                        digest, &digest_size);
        mval = buf;
        for (i = 0; i < digest_size; i++) {
          sprintf(mval, "%.2X:", (unsigned char) digest[i]);
          mval += 3;
        }
        mval[strlen(mval) - 1] = 0; /* Remove last : */
        gtk_label_set_text(lblTLSCertFingerprint, buf);

        size = sizeof(buf);
        gnutls_x509_crt_get_dn(crt, buf, &size);
        lblTLSCertSubDN
          = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblTLSCertSubDN"));
        gtk_label_set_text(lblTLSCertSubDN, buf);

        size = sizeof(buf);
        gnutls_x509_crt_get_issuer_dn(crt, buf, &size);
        lblTLSCertIssDN
          = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblTLSCertIssDN"));
        gtk_label_set_text(lblTLSCertIssDN, buf);
      }
    } else {
      gtk_label_set_text(lblTLSCertHostname, "Unknown certificate type");
      return;
    }

    if (hostname_matches) {
      mval = g_strdup_printf("Matches '%s'", currentWorld->host);
    } else {
      mval = g_strdup_printf("Does not match '%s'", currentWorld->host);
    }
    gtk_label_set_text(lblTLSCertHostname, mval);
    g_free(mval);
  } else {
    GtkLabel *lblTLS;
    lblTLS = GTK_LABEL(gtk_builder_get_object(ui_builder, "lblTLS"));
    gtk_label_set_text(lblTLS, _("None"));
  }

  /* Run the dialog */
  gtk_widget_show_all(dlgStatistics);
  gtk_dialog_run(GTK_DIALOG(dlgStatistics));
  gtk_widget_destroy(dlgStatistics);
  g_object_unref(ui_builder);
}


void
menu_edit_default_world_cb(GtkMenuItem *widget, gpointer data)
{
  gboolean dummy;

  edit_world(&default_world, GTK_WINDOW(wndMain), &dummy, FALSE);
}


void
menu_manual_cb(GtkMenuItem *widget, gpointer data)
{
  gchar *manualurl;

#ifndef __MINGW32__
  manualurl = g_strdup_printf("file://%s", MANUALDIR "/index.xhtml");
#else
  manualurl = g_strdup_printf(get_kildclient_installed_file("manual\\html\\index.xhtml"));
#endif
  menu_url_open(NULL, manualurl);
}



void
menu_about_cb(GtkMenuItem *widget, gpointer data)
{
  GError                *error = NULL;
  static GtkAboutDialog *dlgAbout = NULL;

  if (!dlgAbout) {
    if (!gtk_builder_add_from_resource(main_builder,
                                       "/ekalin/kildclient/dlgAbout.ui",
                                       &error)) {
      g_warning(_("Error loading UI from XML file: %s"), error->message);
      g_error_free(error);
      return;
    }
    dlgAbout = GTK_ABOUT_DIALOG(gtk_builder_get_object(main_builder,
                                                       "dlgAbout"));
    gtk_window_set_transient_for(GTK_WINDOW(dlgAbout), GTK_WINDOW(wndMain));
    gtk_about_dialog_set_version(dlgAbout, VERSION);
    /* TODO: Will not be necessary if future glade versions do not insist
       on setting this property. */
    gtk_about_dialog_set_logo(dlgAbout, NULL);

    gtk_builder_connect_signals(main_builder, NULL);
  }

  gtk_widget_show_all(GTK_WIDGET(dlgAbout));
  gtk_window_present(GTK_WINDOW(dlgAbout));
}


gboolean
about_dlg_url_hook(GtkAboutDialog *about, gchar *link, gpointer data)
{
  gchar *url = g_strdup(link);
  menu_url_open(NULL, url);
  return TRUE;
}



static
gint
attach_world_gui(WorldGUI *gui)
{
  gint page;

  page = gtk_notebook_append_page(GTK_NOTEBOOK(ntbkWorlds), gui->vbox,
                                  GTK_WIDGET(gui->hboxTab));
  gtk_widget_show_all(GTK_WIDGET(gui->hboxTab));
  gtk_notebook_set_tab_reorderable(GTK_NOTEBOOK(ntbkWorlds),
                                   gui->vbox,
                                   TRUE);

  if (globalPrefs.hide_single_tab) {
    gtk_notebook_set_show_tabs(GTK_NOTEBOOK(ntbkWorlds),
                               gtk_notebook_get_n_pages(GTK_NOTEBOOK(ntbkWorlds)) != 1);
  }

  gtk_widget_show_all(ntbkWorlds);

  return page;
}


static
void
detach_world_gui(WorldGUI *gui)
{
  gint old_page;
  gint new_page;

  old_page = gtk_notebook_page_num(GTK_NOTEBOOK(ntbkWorlds), gui->vbox);
  gtk_notebook_remove_page(GTK_NOTEBOOK(ntbkWorlds), old_page);

  if (globalPrefs.hide_single_tab) {
    gtk_notebook_set_show_tabs(GTK_NOTEBOOK(ntbkWorlds),
                               gtk_notebook_get_n_pages(GTK_NOTEBOOK(ntbkWorlds)) != 1);
  }

  new_page = gtk_notebook_get_current_page(GTK_NOTEBOOK(ntbkWorlds));
  update_world_with_focus(GINT_TO_POINTER(new_page));
}


void
set_focused_world(gint page)
{
  gtk_notebook_set_current_page(GTK_NOTEBOOK(ntbkWorlds), page);
}


gint
get_focused_world(void)
{
  return gtk_notebook_get_current_page(GTK_NOTEBOOK(ntbkWorlds));
}


void
remove_world(World *world, gboolean remove_gui)
{
  open_worlds = g_list_remove(open_worlds, world);

  if (world->connected || world->connecting) {
    g_free(close_connection(world, TRUE));
  }

  if (world->file) {
    save_world_to_file(world);
    save_command_history(world);
    save_permanent_variables(world);
  }

  if (remove_gui) {
    detach_world_gui(world->gui);
    free_world_gui(world->gui);
  }

  free_world(world);

  if (open_worlds == NULL) {
    gtk_main_quit();
  }
}


gboolean
remove_world_timer(World *world)
{
  remove_world(world, TRUE);

  /* Because it is called as an idle function */
  return FALSE;
}


void
set_notebook_tab_position(GtkPositionType pos)
{
  gtk_notebook_set_tab_pos(GTK_NOTEBOOK(ntbkWorlds), pos);
}


void
menu_world_open_cb(GtkMenuItem *widget, gpointer data)
{
  GtkWidget *mnuDisconnect = GTK_WIDGET(data);

  if (currentWorld && (currentWorld->connected || currentWorld->connecting)) {
    gtk_widget_set_sensitive(mnuDisconnect, TRUE);
  } else {
    gtk_widget_set_sensitive(mnuDisconnect, FALSE);
  }
}


void
menu_edit_open_cb(GtkMenuItem *widget, gpointer data)
{
  GtkWidget    *mnuCut;
  GtkWidget    *mnuCopy;
  GtkWidget    *mnuPaste;
  GtkWidget    *mnuDelete;
  GdkDisplay   *display;
  GtkClipboard *clipboard;
  gboolean      input_selected;
  gboolean      output_selected;

  /* Safety check */
  if (!currentWorld
      || !currentWorld->gui->cmbEntry || !currentWorld->gui->txtBuffer) {
    return;
  }

  mnuCut     = GTK_WIDGET(gtk_builder_get_object(main_builder, "mnuCut"));
  mnuCopy    = GTK_WIDGET(gtk_builder_get_object(main_builder, "mnuCopy"));
  mnuPaste   = GTK_WIDGET(gtk_builder_get_object(main_builder, "mnuPaste"));
  mnuDelete  = GTK_WIDGET(gtk_builder_get_object(main_builder, "mnuDelete"));

  /* We can paste when there is text in the clipboard */
  display = gtk_widget_get_display(wndMain);
  clipboard = gtk_clipboard_get_for_display(display, GDK_SELECTION_CLIPBOARD);
  gtk_widget_set_sensitive(mnuPaste,
                           gtk_clipboard_wait_is_text_available(clipboard));

  /* We can cut and delete if there is text selected in the input box */
  input_selected
    = simo_combo_box_get_selection_bounds(currentWorld->gui->cmbEntry,
                                          NULL, NULL);
  gtk_widget_set_sensitive(mnuCut,    input_selected);
  gtk_widget_set_sensitive(mnuDelete, input_selected);

  /* We can copy if there is a selection in the input box or the output
     window */
  output_selected
    = gtk_text_buffer_get_selection_bounds(currentWorld->gui->txtBuffer,
                                           NULL, NULL);
  gtk_widget_set_sensitive(mnuCopy, input_selected || output_selected);
}


void
menu_cut_activate_cb(GtkMenuItem *widget, gpointer data)
{
  simo_combo_box_cut_clipboard(currentWorld->gui->cmbEntry);
}


void
menu_copy_activate_cb(GtkMenuItem *widget, gpointer data)
{
  GtkTextIter start;
  GtkTextIter end;

  if (gtk_text_buffer_get_selection_bounds(currentWorld->gui->txtBuffer,
                                           &start, &end)) {
    GdkDisplay   *display;
    GtkClipboard *clipboard;

    display = gtk_widget_get_display(GTK_WIDGET(currentWorld->gui->txtView));

    clipboard = gtk_clipboard_get_for_display(display,
                                              GDK_SELECTION_CLIPBOARD);
    gtk_text_buffer_copy_clipboard(currentWorld->gui->txtBuffer, clipboard);
    /* The selection is unmarked after the previous step (possibly a issue
       with the non-focusability of the text buffer). So we set it again. */
    gtk_text_buffer_select_range(currentWorld->gui->txtBuffer, &start, &end);
  } else {
    simo_combo_box_copy_clipboard(currentWorld->gui->cmbEntry);
  }
}


void
menu_paste_activate_cb(GtkMenuItem *widget, gpointer data)
{
  simo_combo_box_paste_clipboard(currentWorld->gui->cmbEntry);
}


void
menu_delete_activate_cb(GtkMenuItem *widget, gpointer data)
{
  simo_combo_box_delete_selection(currentWorld->gui->cmbEntry);
}


void
menu_find_activate_cb(GtkMenuItem *widget, gpointer data)
{
  GtkTextIter search_start;

  if (!currentWorld || !currentWorld->gui || !currentWorld->gui->search_box) {
    return;
  }

  g_object_set(G_OBJECT(currentWorld->gui->search_box),
               "no-show-all", FALSE,
               NULL);
  gtk_widget_show_all(currentWorld->gui->search_box);

  gtk_text_buffer_get_start_iter(currentWorld->gui->txtBuffer, &search_start);
  if (!currentWorld->gui->txtmark_search_start) {
    currentWorld->gui->txtmark_search_start
      = gtk_text_buffer_create_mark(currentWorld->gui->txtBuffer,
                                    NULL,
                                    &search_start,
                                    FALSE);
    currentWorld->gui->txtmark_next_search_start
      = gtk_text_buffer_create_mark(currentWorld->gui->txtBuffer,
                                    NULL,
                                    &search_start,
                                    FALSE);
  }

  gtk_entry_set_text(GTK_ENTRY(currentWorld->gui->txtSearchTerm), "");
  gtk_widget_grab_focus(currentWorld->gui->txtSearchTerm);
}


void
menu_findnext_activate_cb(GtkMenuItem *widget, gpointer data)
{
  if (!currentWorld || !currentWorld->gui || !currentWorld->gui->search_box) {
    return;
  }

  if (!currentWorld->gui->txtmark_search_start) {
    /* No search started yet. */
    menu_find_activate_cb(NULL, NULL);
  } else {
    find_next_cb(NULL, currentWorld->gui);
  }
}


void
menu_input_clear_cb(GtkMenuItem *widget, gpointer data)
{
  if (currentWorld) {
    clear_button_cb(NULL, currentWorld->gui);
  }
}


void
menu_input_prev_cb(GtkMenuItem *widget, gpointer data)
{
  if (currentWorld) {
    prev_or_next_command(currentWorld, FALSE);
  }
}


void
menu_input_next_cb(GtkMenuItem *widget, gpointer data)
{
  if (currentWorld) {
    prev_or_next_command(currentWorld, TRUE);
  }
}


void
menu_input_find_prev_cb(GtkMenuItem *widget, gpointer data)
{
  if (currentWorld) {
    find_prev_or_next_command(currentWorld, FALSE);
  }
}


void
menu_input_find_next_cb(GtkMenuItem *widget, gpointer data)
{
  if (currentWorld) {
    find_prev_or_next_command(currentWorld, TRUE);
  }
}



void
menu_preferences_open_cb(GtkMenuItem *widget, gpointer data)
{
  GtkCheckMenuItem *mnuDisableTriggers;
  GtkCheckMenuItem *mnuDisableAliases;
  GtkCheckMenuItem *mnuDisableMacros;
  GtkCheckMenuItem *mnuDisableTimers;

  if (!currentWorld) {
    return;
  }

  mnuDisableTriggers
    = GTK_CHECK_MENU_ITEM(gtk_builder_get_object(main_builder,
                                                 "mnuDisTriggers"));
  mnuDisableAliases
    = GTK_CHECK_MENU_ITEM(gtk_builder_get_object(main_builder,
                                                 "mnuDisAliases"));
  mnuDisableMacros
    = GTK_CHECK_MENU_ITEM(gtk_builder_get_object(main_builder,
                                                 "mnuDisMacros"));
  mnuDisableTimers
    = GTK_CHECK_MENU_ITEM(gtk_builder_get_object(main_builder,
                                                 "mnuDisTimers"));

  gtk_check_menu_item_set_active(mnuDisableTriggers,
                                 currentWorld->disable_triggers);
  gtk_check_menu_item_set_active(mnuDisableAliases,
                                 currentWorld->disable_aliases);
  gtk_check_menu_item_set_active(mnuDisableMacros,
                                 currentWorld->disable_macros);
  gtk_check_menu_item_set_active(mnuDisableTimers,
                                 currentWorld->disable_timers);
}


void
menu_disable_triggers_cb(GtkMenuItem *widget, gpointer data)
{
  currentWorld->disable_triggers =
    gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(widget));
}


void
menu_disable_aliases_cb(GtkMenuItem *widget, gpointer data)
{
  currentWorld->disable_aliases =
    gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(widget));
}


void
menu_disable_macros_cb(GtkMenuItem *widget, gpointer data)
{
  currentWorld->disable_macros =
    gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(widget));
}


void
menu_disable_timers_cb(GtkMenuItem *widget, gpointer data)
{
  currentWorld->disable_timers =
    gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(widget));
}


void
menu_debug_matches_cb(GtkMenuItem *widget, gpointer data)
{
  debug_matches = gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(widget));
}


void
menu_display_open_cb(GtkMenuItem *widget, gpointer data)
{
  GtkCheckMenuItem *mnuSplitWindow;

  if (!currentWorld) {
    return;
  }

  mnuSplitWindow
    = GTK_CHECK_MENU_ITEM(gtk_builder_get_object(main_builder,
                                                 "mnuSplitWindow"));

  g_signal_handlers_block_by_func(G_OBJECT(mnuSplitWindow),
                                  menu_split_screen_cb,
                                  NULL);
  gtk_check_menu_item_set_active(mnuSplitWindow,
                                 gtk_paned_get_position(currentWorld->gui->split_pane) > 0);
  g_signal_handlers_unblock_by_func(G_OBJECT(mnuSplitWindow),
                                    menu_split_screen_cb,
                                    NULL);
}

void
menu_split_screen_cb(GtkMenuItem *widget, gpointer data)
{
  if (gtk_paned_get_position(currentWorld->gui->split_pane) == 0) {
    if (currentWorld->gui->last_split_position == 0) {
      GtkAllocation allocation;
      gtk_widget_get_allocation(GTK_WIDGET(currentWorld->gui->split_pane),
                                &allocation);
      currentWorld->gui->last_split_position = allocation.height * .65;
    }

    gtk_paned_set_position(currentWorld->gui->split_pane,
                           currentWorld->gui->last_split_position);
  } else {
    currentWorld->gui->last_split_position
      = gtk_paned_get_position(currentWorld->gui->split_pane);
    gtk_paned_set_position(currentWorld->gui->split_pane, 0);
  }
}


gboolean
update_times_cb(gpointer data)
{
  /* Updates the connection and idle timers in the status bar of the
     worlds */
  GList  *worldptr;
  World  *world;
  time_t  now;
  gchar  *timestr;

  now = time(NULL);

  worldptr = open_worlds;
  while (worldptr) {
    world = (World *) worldptr->data;

    if (world->connected
        && world->gui && world->gui->lblTime) {
      timestr = format_time_string(world,
                                   (int) difftime(now, world->connected_time),
                                   (int) difftime(now, world->last_command_time));
      gtk_label_set_text(world->gui->lblTime, timestr);
      g_free(timestr);
    }

    worldptr = worldptr->next;
  }

  return TRUE;
}


static
gchar *
format_time_string(World *world, int ctime, int itime)
{
  GString *str;

  str = g_string_new(NULL);

  if (world->ctime_type != NO) {
    g_string_append(str, "Conn: ");
    if (world->ctime_type == SEC) {
      g_string_append_printf(str, "%ds", ctime);
    } else {
      format_hms_time(str, ctime);
    }

    if (world->itime_type != NO) {
      g_string_append(str, ", ");;
    }
  }

  if (world->itime_type != NO) {
    g_string_append(str, "Idle: ");
    if (world->itime_type == SEC) {
      g_string_append_printf(str, "%ds", itime);
    } else {
      format_hms_time(str, itime);
    }
  }

  return g_string_free(str, FALSE);
}


static
void
format_hms_time(GString *str, int seconds)
{
  int hours;
  int minutes;

  hours   = (int) (seconds/3600);
  seconds %= 3600;
  minutes = (int) (seconds/60);
  seconds %= 60;

  if (hours) {
    g_string_append_printf(str, "%dh", hours);
  }
  if (minutes) {
    if (hours) {
      g_string_append_printf(str, "%02dm", minutes);
    } else {
      g_string_append_printf(str, "%dm", minutes);
    }
  }
  g_string_append_printf(str, "%02ds", seconds);
}
