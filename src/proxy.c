/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <stdint.h>
#include <sys/time.h>
#include "libintl-wrapper.h"
#include <locale.h>
#include <gtk/gtk.h>
#include <zlib.h>
#include <glib.h>

#ifndef __MINGW32__
#  include <fcntl.h>
#  include <sys/types.h>
#  include <sys/socket.h>
#  include <netdb.h>
#  include <arpa/inet.h>
#else /* defined __MINGW32__ */
#  include <winsock2.h>
#  include <ws2tcpip.h>
#endif

#include "kildclient.h"
#include "perlscript.h"
#include "net.h"


/*************************
 * File global variables *
 *************************/
static const char* proxy_socks5_rejection_reasons[] = {
  NULL, /* success */
  N_("general failure"),
  N_("connection not allowed by ruleset"),
  N_("network unreachable"),
  N_("host unreachable"),
  N_("connection refused by destination host"),
  N_("TTL expired"),
  N_("command not supported / protocol error"),
  N_("address type not supported"),
};


/**************
 * Data types *
 **************/


/***********************
 * SOCKS proxy defines *
 ***********************/
#define SOCKS_CONNECT             0x01

#define SOCKS4_VER                0x04
#define SOCKS4_REP_OK             0x5a
#define SOCKS4_REP_FAIL           0x5b
#define SOCKS4_REP_FAIL_NO_IDENTD 0x5c
#define SOCKS4_REP_FAIL_UNCONF    0x5d
/* SOCKS4 request: 1 byte ver, 1 byte command, 2 bytes port, 4 bytes IP,
   user name (variable), 1 byte terminating NULL */
#define SOCKS4_REQUEST_SIZE       (1+1+2+4+1)
/* SOCKS4 reply: 1 byte null, 1 byte status, 2 bytes arbitrary,
   4 bytes arbitrary */
#define SOCKS4_REPLY_SIZE         (1+1+2+4)

#define SOCKS5_VER                    0x05
#define SOCKS5_AUTH_NONE              0x00
#define SOCKS5_AUTH_USERNAME          0x02
#define SOCKS5_AUTH_REJECTED          0xFF
#define SOCKS5_AUTH_REP_SUCCESS       0x00
#define SOCKS5_AUTH_USERNAME_VER      0x01
#define SOCKS5_ADDR_IP4               0x01
#define SOCKS5_ADDR_NAME              0x03
#define SOCKS5_ADDR_IP6               0x04
#define SOCKS5_CONN_REP_SUCCESS       0x00
#define SOCKS5_CONN_MAX_FAILED_REASON 0x08
/* SOCKS5 initial request: 1 byte version, 1 byte number of auth
   methods, 1 byte for each method. Since we support at most two
   methods, that's two bytes. */
#define SOCKS5_AUTH_METHOD_REQUEST_SIZE (1+1+2)
/* SOCKS5 initial reply: 1 byte version, 1 byte method */
#define SOCKS5_AUTH_METHOD_REPLY_SIZE (1+1)
/* SOCKS5 auth reply: 1 byte version, 1 byte status */
#define SOCKS5_AUTH_REPLY_SIZE (1+1)
/* SOCKS5 connection request: 1 byte version, 1 byte command, 1 byte
   null, 1 byte address type, 1 byte domain length, domain (variable),
   2 bytes port */
#define SOCKS5_CONNECTION_REQUEST_SIZE (1+1+1+1+1+2)
/* Start of SOCKS5 connection request: 1 byte version, 1 byte status,
   1 byte null, 1 byte address type, and the first byte of the
   address */
#define SOCKS5_PARTIAL_CONNECTION_REPLY_SIZE (1+1+1+1+1)


/***********************
 * Function prototypes *
 ***********************/
static gboolean proxy_socks4_data_ready_cb(GIOChannel   *iochan,
                                           GIOCondition  cond,
                                           gpointer      data);
static gboolean proxy_socks5_data_ready_cb(GIOChannel   *iochan,
                                           GIOCondition  cond,
                                           gpointer      data);
static gboolean proxy_socks5_data_ready_initial(World *world);
static gboolean proxy_socks5_data_ready_auth(World *world);
static gboolean proxy_socks5_data_ready_connection(World *world);
static void     proxy_socks5_send_auth(World *world);
static void     proxy_socks5_send_connection_request(World *world);
static gboolean proxy_http_data_ready_cb(GIOChannel   *iochan,
                                         GIOCondition  cond,
                                         gpointer      data);
static gchar   *proxy_http_get_connection_string(World *world);

#define HAS_PROXY_HTTP_RESPONSE(w) ((w)->pbuf_size >= 4 && memcmp((w)->inbuffer + (w)->pbuf_size - 4, "\r\n\r\n", 4) == 0)



/***************************
 * General proxy functions *
 ***************************/
void
proxy_info_free(proxy_settings *s)
{
  /* Frees the contents of the structure, but not the structure itself. */
  if (s == NULL) {
    return;
  }

  g_free(s->server);
  g_free(s->port);
  g_free(s->user);
  g_free(s->password);
}


void
proxy_info_copy(proxy_settings *source, proxy_settings *dest)
{
  dest->type     = source->type;
  dest->server   = g_strdup(source->server   ? source->server   : "");
  dest->port     = g_strdup(source->port     ? source->port     : "");
  dest->user     = g_strdup(source->user     ? source->user     : "");
  dest->password = g_strdup(source->password ? source->password : "");
}


/**********
 * SOCKS4 *
 **********/
gboolean
proxy_socks4_resolved_real_host_cb(gpointer data)
{
  struct resolve_data_s *resolve_data = (struct resolve_data_s *) data;
  World *world = resolve_data->world;
  gchar *msg;
  GThread *resolver_thread;

  g_free(resolve_data->host);
  g_free(resolve_data->port);

  if (resolve_data->ret != 0) {
    /* It could happen that the world has been closed */
    if (g_list_index(open_worlds, world) != -1) {
      gchar *msg;
      gsize  len;

      msg = g_locale_to_utf8(gai_strerror(resolve_data->ret), -1,
                             NULL, &len, NULL);
      world->errmsg = g_strdup_printf(_("Could not resolve host %s: %s"),
                                      world->host, msg);
      g_free(msg);
      disconnect_world(world, FALSE);
    }
    g_free(resolve_data);

    /* Only run once */
    return FALSE;
  }

  /* FIXME: There must be a better way to get the IP address as a number. */
  memcpy(&(world->real_ip_for_socks4),
         resolve_data->addrinfo->ai_addr->sa_data+2, 4);
  g_free(resolve_data);

  msg = g_strdup_printf(_("Resolving proxy host %s..."),
                        world->proxy_used.server);
  ansitextview_append_string_with_fgcolor(world->gui, msg,
                                          globalPrefs.idxInfoMsgColor);
  ansitextview_append_string(world->gui, "\n");
  gtk_label_set_text(world->gui->lblStatus, msg);
  g_free(msg);

  resolve_data = g_new(struct resolve_data_s, 1);
  resolve_data->world     = world;
  resolve_data->host      = g_strdup(world->proxy_used.server);
  resolve_data->port      = g_strdup(world->proxy_used.port);
  resolve_data->ai_family = AF_UNSPEC;
  resolve_data->callback  = continue_connecting_cb;
  resolver_thread = g_thread_new("resolver",
                                 resolve_name_thread, resolve_data);
  g_thread_unref(resolver_thread);

  return FALSE;
}


gboolean
proxy_socks4_connected_to(World *world)
{
  gchar    *msg;
  guchar   *request;
  size_t    request_size;
  uint16_t  port;

  msg = g_strdup_printf(_("Connected to proxy server %s port %s, sending request..."),
                        world->proxy_used.server, world->proxy_used.port);
  ansitextview_append_string_with_fgcolor(world->gui, msg,
                                          globalPrefs.idxInfoMsgColor);
  ansitextview_append_string(world->gui, "\n");
  gtk_label_set_text(world->gui->lblStatus, msg);
  g_free(msg);

  /* Create SOCKS4 request */
  request_size = SOCKS4_REQUEST_SIZE + strlen(world->proxy_used.user);
  request = g_malloc(request_size);
  request[0] = SOCKS4_VER;
  request[1] = SOCKS_CONNECT;
  port = htons((uint16_t) atoi(world->port));
  memcpy(request + 2, &port, 2);
  memcpy(request + 4, &world->real_ip_for_socks4, 4);
  strcpy((gchar *) request + 8, world->proxy_used.user);

  /* We do not use kc_send because TLS is never used at this point */
  send(world->sock, request, request_size, 0);
  g_free(request);

  /* Set a callback to read the response */
  world->pbuf_size = 0; /* This counter will hold the number of bytes
                           read. So far, nothing has been read yet. */
  g_source_remove(world->io_watch_id);
  world->io_watch_id = g_io_add_watch_full(world->iochan,
                                           G_PRIORITY_HIGH,
                                           G_IO_IN | G_IO_HUP | G_IO_ERR,
                                           proxy_socks4_data_ready_cb,
                                           world,
                                           NULL);

  return TRUE;
}


static
gboolean
proxy_socks4_data_ready_cb(GIOChannel   *iochan,
                           GIOCondition  cond,
                           gpointer      data)
{
  World    *world;
  ssize_t   nread;
  gchar    *msg = NULL;

  world = (World *) data;

  /* We borrow world->inbuffer to hold the bytes, and world->pbuf_size
     to store to number of bytes that have been read. */
  nread = recv(world->sock,
               world->inbuffer + world->pbuf_size,
               SOCKS4_REPLY_SIZE - world->pbuf_size, 0);
  if (nread == -1) {
    world->errmsg = g_strdup(_("Error while reading from proxy host."));
    world->pbuf_size = 0; /* disconnect_world() flushes the buffer. */
    disconnect_world(world, FALSE);
    return FALSE;
  }

  world->pbuf_size += nread;
  if (world->pbuf_size < SOCKS4_REPLY_SIZE) {
    /* Continue reading... */
    return TRUE;
  }

  /* OK, so we have the whole 8-byte reply */
  world->pbuf_size = 0;
  switch (world->inbuffer[1]) {
  case SOCKS4_REP_OK:
    world->pbuf_size = 0;
    perform_tls_handshake(world);
    return FALSE; /* Not to be called again. */

  case SOCKS4_REP_FAIL:
    msg = _("request rejected or failed");
    break;

  case SOCKS4_REP_FAIL_NO_IDENTD:
    msg = _("request failed because identd could not be reached");
    break;

  case SOCKS4_REP_FAIL_UNCONF:
    msg = _("request failed because client's identd could not confirm the user ID string in the request");
    break;
  }

  world->errmsg = g_strdup_printf(_("Proxy request failed: %s."), msg);
  disconnect_world(world, FALSE);
  return FALSE;
}


/**********
 * SOCKS5 *
 **********/
gboolean
proxy_socks5_connected_to(World *world)
{
  gchar    *msg;
  gboolean  can_use_auth;
  size_t    request_size;
  guchar    request[SOCKS5_AUTH_METHOD_REQUEST_SIZE];

  msg = g_strdup_printf(_("Connected to proxy server %s port %s, negotiating authentication..."),
                        world->proxy_used.server, world->proxy_used.port);
  ansitextview_append_string_with_fgcolor(world->gui, msg,
                                          globalPrefs.idxInfoMsgColor);
  ansitextview_append_string(world->gui, "\n");
  gtk_label_set_text(world->gui->lblStatus, msg);
  g_free(msg);

  /* Send supported authentication methods */
  can_use_auth = (strcmp(world->proxy_used.user, "") != 0);
  request[0] = SOCKS5_VER;
  if (can_use_auth) {
    request[1] = 2;
    request_size = SOCKS5_AUTH_METHOD_REQUEST_SIZE;
  } else {
    request[1] = 1;
    request_size = SOCKS5_AUTH_METHOD_REQUEST_SIZE - 1;
  }
  request[2] = SOCKS5_AUTH_NONE;
  if (can_use_auth) {
    request[3] = SOCKS5_AUTH_USERNAME;
  }

  /* We do not use kc_send because TLS is never used at this point */
  send(world->sock, request, request_size, 0);

  /* Set a callback to read the response */
  world->pbuf_size = 0; /* This counter will hold the number of bytes
                           read. So far, nothing has been read yet. */
  world->proxy_used.state = SOCKS5_STATE_INITIAL;
  g_source_remove(world->io_watch_id);
  world->io_watch_id = g_io_add_watch_full(world->iochan,
                                           G_PRIORITY_HIGH,
                                           G_IO_IN | G_IO_HUP | G_IO_ERR,
                                           proxy_socks5_data_ready_cb,
                                           world,
                                           NULL);

  return TRUE;
}


static
gboolean
proxy_socks5_data_ready_cb(GIOChannel   *iochan,
                           GIOCondition  cond,
                           gpointer      data)
{
  World    *world;

  world = (World *) data;

  switch (world->proxy_used.state) {
  case SOCKS5_STATE_INITIAL:
    return proxy_socks5_data_ready_initial(world);

  case SOCKS5_STATE_AUTH:
    return proxy_socks5_data_ready_auth(world);

  case SOCKS5_STATE_CONNECTION:
    return proxy_socks5_data_ready_connection(world);
  }

  return FALSE;
}


static
gboolean
proxy_socks5_data_ready_initial(World *world)
{
  ssize_t nread;

  /* We borrow world->inbuffer to hold the bytes, and world->pbuf_size
     to store to number of bytes that have been read. */
  nread = recv(world->sock,
               world->inbuffer + world->pbuf_size,
               SOCKS5_AUTH_METHOD_REPLY_SIZE - world->pbuf_size, 0);
  if (nread == -1) {
    world->errmsg = g_strdup(_("Error while reading from proxy host."));
    world->pbuf_size = 0; /* disconnect_world() flushes the buffer. */
    disconnect_world(world, FALSE);
    return FALSE;
  }

  world->pbuf_size += nread;
  if (world->pbuf_size < SOCKS5_AUTH_METHOD_REPLY_SIZE) {
    /* Continue reading... */
    return TRUE;
  }

  /* See what was selected. */
  switch (world->inbuffer[1]) {
  case SOCKS5_AUTH_REJECTED:
    world->errmsg = _("No acceptable proxy authentication methods.");
    world->pbuf_size = 0; /* disconnect_world() flushes the buffer. */
    disconnect_world(world, FALSE);
    return FALSE;

  case SOCKS5_AUTH_NONE:
    proxy_socks5_send_connection_request(world);
    return TRUE;

  case SOCKS5_AUTH_USERNAME:
    proxy_socks5_send_auth(world);
    return TRUE;
  }

  return FALSE;
}


static
gboolean
proxy_socks5_data_ready_auth(World *world)
{
  ssize_t nread;

  /* We borrow world->inbuffer to hold the bytes, and world->pbuf_size
     to store to number of bytes that have been read. */
  nread = recv(world->sock,
               world->inbuffer + world->pbuf_size,
               SOCKS5_AUTH_REPLY_SIZE - world->pbuf_size, 0);
  if (nread == -1) {
    world->errmsg = g_strdup(_("Error while reading from proxy host."));
    world->pbuf_size = 0; /* disconnect_world() flushes the buffer. */
    disconnect_world(world, FALSE);
    return FALSE;
  }

  world->pbuf_size += nread;
  if (world->pbuf_size < SOCKS5_AUTH_REPLY_SIZE) {
    /* Continue reading... */
    return TRUE;
  }

  /* See what was selected. */
  if (world->inbuffer[1] == SOCKS5_AUTH_REP_SUCCESS) {
    proxy_socks5_send_connection_request(world);
    return TRUE;
  }

  world->errmsg = g_strdup(_("Proxy authentication failed."));
  world->pbuf_size = 0; /* disconnect_world() flushes the buffer. */
  disconnect_world(world, FALSE);
  return FALSE;
}


static
gboolean
proxy_socks5_data_ready_connection(World *world)
{
  ssize_t  size_to_read;
  ssize_t  nread;
  gchar   *msg;

  /* Things are more complicated here because the reply is variable in
     length, depending on the address type and its length.

     First, we read the address type and the first byte of the
     address, which will be the length of the domain name, or the
     first byte of an IP address. Then we calculate how many more
     bytes need to be read. */

  /* size_to_read will hold the total number of bytes that must be read */
  if (world->pbuf_size < SOCKS5_PARTIAL_CONNECTION_REPLY_SIZE) {
    size_to_read = SOCKS5_PARTIAL_CONNECTION_REPLY_SIZE;
  } else {
    int addr_type = world->inbuffer[3];
    switch (addr_type) {
    case SOCKS5_ADDR_IP4:
      size_to_read = SOCKS5_PARTIAL_CONNECTION_REPLY_SIZE - 1 + 4 + 2;
      break;

    case SOCKS5_ADDR_IP6:
      size_to_read = SOCKS5_PARTIAL_CONNECTION_REPLY_SIZE - 1 + 16 + 2;
      break;

    case SOCKS5_ADDR_NAME:
      size_to_read = SOCKS5_PARTIAL_CONNECTION_REPLY_SIZE
                     + world->inbuffer[4] + 2;
      break;

    default: /* Should never happen */
      world->errmsg = g_strdup(_("Unrecognized response from proxy host."));
      world->pbuf_size = 0; /* disconnect_world() flushes the buffer. */
      disconnect_world(world, FALSE);
      return FALSE;
    }
  }

  /* We borrow world->inbuffer to hold the bytes, and world->pbuf_size
     to store to number of bytes that have been read. */
  nread = recv(world->sock,
               world->inbuffer + world->pbuf_size,
               size_to_read - world->pbuf_size, 0);
  if (nread == -1) {
    world->errmsg = g_strdup(_("Error while reading from proxy host."));
    world->pbuf_size = 0; /* disconnect_world() flushes the buffer. */
    disconnect_world(world, FALSE);
    return FALSE;
  }

  world->pbuf_size += nread;
  if (world->pbuf_size < size_to_read
      || size_to_read <= SOCKS5_PARTIAL_CONNECTION_REPLY_SIZE) {
    /* Continue reading... */
    return TRUE;
  }

  /* Determine the reply. */
  world->pbuf_size = 0;
  if (world->inbuffer[1] == SOCKS5_CONN_REP_SUCCESS) {
    perform_tls_handshake(world);
    return FALSE; /* Not to be called again. */
  }

  if (world->inbuffer[1] > SOCKS5_CONN_MAX_FAILED_REASON) {
    msg = _("unknown reason code");
  } else {
    msg = _(proxy_socks5_rejection_reasons[world->inbuffer[1]]);
  }

  world->errmsg = g_strdup_printf(_("Proxy request failed: %s."), msg);
  disconnect_world(world, FALSE);
  return FALSE;
}


static
void
proxy_socks5_send_auth(World *world)
{
  gchar    *msg;
  guchar   *request;
  size_t    request_size;
  size_t    user_len;
  size_t    pass_len;

  msg = g_strdup_printf(_("Sending proxy authentication credentials..."));
  ansitextview_append_string_with_fgcolor(world->gui, msg,
                                          globalPrefs.idxInfoMsgColor);
  ansitextview_append_string(world->gui, "\n");
  gtk_label_set_text(world->gui->lblStatus, msg);
  g_free(msg);

  /* We could warn the user if the values are too big, but this is so
     unlikely to happen that we ignore it, just clamp the value to
     avoid a buffer overflow. */
  user_len = MIN(strlen(world->proxy_used.user), 255);
  pass_len = MIN(strlen(world->proxy_used.password), 255);
  request_size = 3 + user_len + pass_len;
  request = g_malloc(request_size);
  request[0] = SOCKS5_AUTH_USERNAME_VER;
  request[1] = (guint8) user_len;
  memcpy(request + 2, world->proxy_used.user, user_len);
  request[2 + user_len] = (guint8) pass_len;
  memcpy(request + 2 + user_len + 1, world->proxy_used.password, pass_len);

  /* We do not use kc_send because TLS is never used at this point */
  send(world->sock, request, request_size, 0);
  g_free(request);

  world->proxy_used.state = SOCKS5_STATE_AUTH;
  world->pbuf_size = 0;
}


static
void
proxy_socks5_send_connection_request(World *world)
{
  gchar    *msg;
  guchar   *request;
  size_t    request_size;
  size_t    host_name_len;
  uint16_t  port;

  msg = g_strdup_printf(_("Sending connection request..."));
  ansitextview_append_string_with_fgcolor(world->gui, msg,
                                          globalPrefs.idxInfoMsgColor);
  ansitextview_append_string(world->gui, "\n");
  gtk_label_set_text(world->gui->lblStatus, msg);
  g_free(msg);

  /* Create SOCKS5 connection request */
  host_name_len = MIN(strlen(world->host), 255);
  request_size = SOCKS5_CONNECTION_REQUEST_SIZE + host_name_len;
  request = g_malloc(request_size);
  request[0] = SOCKS5_VER;
  request[1] = SOCKS_CONNECT;
  request[2] = 0;
  request[3] = SOCKS5_ADDR_NAME;
  request[4] = (guint8) host_name_len;
  memcpy(request + 5, world->host, host_name_len);
  port = htons((uint16_t) atoi(world->port));
  memcpy(request + 5 + host_name_len, &port, 2);

  /* We do not use kc_send because TLS is never used at this point */
  send(world->sock, request, request_size, 0);
  g_free(request);

  world->proxy_used.state = SOCKS5_STATE_CONNECTION;
  world->pbuf_size = 0;
}


/********
 * HTTP *
 ********/
gboolean
proxy_http_connected_to(World *world)
{
  gchar *msg;
  gchar *request;

  msg = g_strdup_printf(_("Connected to proxy server %s port %s, sending request..."),
                        world->proxy_used.server, world->proxy_used.port);
  ansitextview_append_string_with_fgcolor(world->gui, msg,
                                          globalPrefs.idxInfoMsgColor);
  ansitextview_append_string(world->gui, "\n");
  gtk_label_set_text(world->gui->lblStatus, msg);
  g_free(msg);

  request = proxy_http_get_connection_string(world);

  /* We do not use kc_send because TLS is never used at this point */
  send(world->sock, request, strlen(request), 0);
  g_free(request);

  /* Set a callback to read the response */
  world->pbuf_size = 0; /* This counter will hold the number of bytes
                           read. So far, nothing has been read yet. */
  g_source_remove(world->io_watch_id);
  world->io_watch_id = g_io_add_watch_full(world->iochan,
                                           G_PRIORITY_HIGH,
                                           G_IO_IN | G_IO_HUP | G_IO_ERR,
                                           proxy_http_data_ready_cb,
                                           world,
                                           NULL);

  return TRUE;
}


static
gboolean
proxy_http_data_ready_cb(GIOChannel   *iochan,
                         GIOCondition  cond,
                         gpointer      data)
{
  World    *world;
  ssize_t   nread;

  world = (World *) data;

  /* Since there is no way to know how long the proxy response will be,
     we must read 1 byte at a time and check if it is complete in order
     not to read the contents of the actual connection. */
  nread = recv(world->sock,
               world->inbuffer + world->pbuf_size,
               1, 0);
  if (nread == -1) {
    world->errmsg = g_strdup(_("Error while reading from proxy host."));
    world->pbuf_size = 0; /* disconnect_world() flushes the buffer. */
    disconnect_world(world, FALSE);
    return FALSE;
  }
  ++world->pbuf_size;
  if (!HAS_PROXY_HTTP_RESPONSE(world)) {
    /* Continue reading... */
    return TRUE;
  }

  gchar *code_start = memchr(world->inbuffer, ' ', world->pbuf_size);
  if (code_start == NULL || memcmp(code_start, " 200", 4) != 0) {
    gchar *line_end = memchr(world->inbuffer, '\r', world->pbuf_size);
    *line_end = '\0';

    world->errmsg = g_strdup_printf(_("Proxy request failed: %s."),
                                    code_start + 1);
    world->pbuf_size = 0;
    disconnect_world(world, FALSE);
    return FALSE;
  }

  world->pbuf_size = 0;
  perform_tls_handshake(world);
  /* Not to be called again. */
  return FALSE;
}


static
gchar *
proxy_http_get_connection_string(World *world)
{
  if (strcmp(world->proxy_used.user, "") == 0) {
    return g_strdup_printf("CONNECT %s:%s HTTP/1.0\r\n\r\n",
                           world->host, world->port);
  } else {
    gchar *auth_string = g_strdup_printf("%s:%s",
                                         world->proxy_used.user,
                                         world->proxy_used.password);
    gchar *basic_auth_data = g_base64_encode((guchar *) auth_string,
                                             strlen(auth_string));
    g_free(auth_string);

    gchar *result = g_strdup_printf("CONNECT %s:%s HTTP/1.0\r\n"
                                    "Proxy-Authorization: Basic %s\r\n"
                                    "\r\n",
                                    world->host, world->port,
                                    basic_auth_data);
    g_free(basic_auth_data);
    return result;
  }
}

