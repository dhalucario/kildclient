/*
 * Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <kcconfig.h>
#endif

#include <string.h>
#include <errno.h>
#include <stdarg.h>
#include "libintl-wrapper.h"
#include <gtk/gtk.h>

#include "kildclient.h"
#include "perlscript.h"
#include "net.h"


/********************
 * Type definitions *
 ********************/
typedef enum {
  KC_START,
  KC_KCWORLD,
  KC_NAME,
  KC_HOST,
  KC_PORT,
  KC_CHARSET,
  KC_PROXY,
  KC_AUTO_LOGON,
  KC_CHARACTER,
  KC_PASSWORD,
  KC_DISPLAY,
  KC_TERMINALFONT,
  KC_ENTRYFONT,
  KC_USEBOLD,
  KC_WRAP,
  KC_WRAP_COLUMN,
  KC_TOOLTIPS,
  KC_STATUSFONT,
  KC_STATUSBAR,
  KC_DEFFORECOLOR,
  KC_DEFBACKCOLOR,
  KC_DEFBOLDCOLOR,
  KC_DEFBOLDBACKCOLOR,
  KC_ANSICOLOR,
  KC_BACKGROUND,
  KC_SCROLL,
  KC_NAMEDISPLAY,
  KC_LOG,
  KC_LOGFILE,
  KC_LOGTIMEFORMAT,
  KC_FLOOD_PREVENTION,
  KC_REPEAT_COMMANDS,
  KC_COMMAND_ECHO,
  KC_NEVER_HIDE_INPUT,
  KC_STORE_COMMANDS,
  KC_COMMAND_HISTORY,
  KC_AUTO_COMPLETION,
  KC_COMMAND_SEPARATOR,
  KC_PERL_CHARACTER,
  KC_INPUT_LINES,
  KC_INPUT_SPELL,
  KC_CONFIRM_DELETE,
  KC_PLUGIN_ITEMS,
  KC_SCRIPTFILE,
  KC_PLUGINS,
  KC_PLUGIN,
  KC_TRIGGERS,
  KC_TRIGGER,
  KC_PATTERN,
  KC_ACTION,
  KC_HIGHLIGHT,
  KC_ALIASES,
  KC_ALIAS,
  KC_SUBSTITUTION,
  KC_MACROS,
  KC_MACRO,
  KC_KEY,
  KC_TIMERS,
  KC_TIMER,
  KC_VARIABLES,
  KC_VARIABLE,
  KC_HOOKSV2,
  KC_HOOKLISTV2,
  KC_HOOKV2,
  KC_PROTOCOLS,
  KC_MCCP,
  KC_KEEP_ALIVE,
}  ParserState;
struct WorldParser_s
{
  ParserState  state;
  ParserState  previous_state;
  World       *world;
  gboolean     forWorldSelector;
  char        *currHookTrigger;
  char        *currHookName;
  gboolean     currHookEnabled;
  union {
    Trigger *currTrigger;
    Alias   *currAlias;
    Macro   *currMacro;
    Timer   *currTimer;
    char    *currVar;
    char    *currPluginFile;
    char    *currHookAction;
  };
  union {
    gchar      **currText;
    GdkRGBA     *currColor;
    GtkTreeIter  curr_logon_iter;
  };
};
typedef struct WorldParser_s WorldParser;


/***********************
 * Function prototypes *
 ***********************/
static void world_allocate_space_for_colors(World *world);
static void xml_start_element(GMarkupParseContext *context,
                              const gchar         *element_name,
                              const gchar        **attribute_names,
                              const gchar        **attribute_values,
                              gpointer             user_data,
                              GError             **error);
static void xml_end_element(GMarkupParseContext *context,
                            const gchar         *element_name,
                            gpointer             user_data,
                            GError             **error);
static void xml_text(GMarkupParseContext *context,
                     const gchar         *text,
                     gsize                text_len,
                     gpointer             user_data,
                     GError             **error);
static void xml_error_invalid_inside(GError              **error,
                                     GMarkupParseContext  *context);
static void xml_error_unknown_attribute(GError              **error,
                                        const gchar          *attribute_name,
                                        GMarkupParseContext  *context);
static void copy_text(char **dest, const char *orig, gsize len);
static void read_color(GdkRGBA *color, const char *text, gsize text_len,
                       GError **error);
static void write_color(GString *str, char *name, int idx, GdkRGBA *color);


/********************
 * Global variables *
 ********************/
World    *default_world = NULL;
GdkRGBA deffore     = { 0.74509804, 0.74509804, 0.74509804, 1 };
GdkRGBA defbold     = { 1,          1,          1,          1 };
GdkRGBA defback     = { 0,          0,          0         , 1 };
GdkRGBA defboldback = { 0.37254902, 0.37254902, 0.37254902, 1 };
GdkRGBA defansi[] = {
  { 0,          0,          0,          1 },        /* Black */
  { 0.54509804, 0,          0,          1 },        /* Red */
  { 0,          0.45098039, 0,          1 },        /* Green */
  { 0.54509804, 0.41176471, 0.07843137, 1 },        /* Yellow */
  { 0,          0,          0.54509804, 1 },        /* Blue */
  { 0.54509804, 0,          0.54509804, 1 },        /* Magenta */
  { 0,          0.54509804, 0.54509804, 1 },        /* Cyan */
  { 0.74509804, 0.74509804, 0.74509804, 1 },        /* White */
  { 0.37254902, 0.37254902, 0.37254902, 1 },        /* Bold Black */
  { 1,          0,          0,          1 },        /* Bold Red */
  { 0,          1,          0,          1 },        /* Bold Green */
  { 1,          1,          0,          1 },        /* Bold Yellow */
  { 0,          0,          1,          1 },        /* Bold Blue */
  { 1,          0,          1,          1 },        /* Bold Magenta */
  { 0,          1,          1,          1 },        /* Bold Cyan */
  { 1,          1,          1,          1 },        /* Bold White */
};


/*************************
 * File global variables *
 *************************/
static const GMarkupParser xmlWorldParser = {
  xml_start_element,
  xml_end_element,
  xml_text,
  NULL,                 /* passthrough */
  NULL                  /* error */
};



World *
create_new_world(gboolean load_defaults)
{
  World *world;

  world = g_new0(World, 1);
  world_allocate_space_for_colors(world);

  world->startup_plugins = GTK_TREE_MODEL(gtk_list_store_new(N_SPLUGIN_COLUMNS,
                                                             G_TYPE_STRING,
                                                             G_TYPE_STRING,
                                                             G_TYPE_STRING,
                                                             G_TYPE_STRING));

  world->logon_characters = GTK_TREE_MODEL(gtk_list_store_new(N_LOGON_COLUMNS,
                                                              G_TYPE_STRING,
                                                              G_TYPE_STRING));

  world->proxy.type = PROXY_USE_GLOBAL;

  g_rec_mutex_init(&world->perl_mutex);

  if (!load_defaults) {
    return world;
  }

  /* Create pbuffer */
  world->pbuffer = (guchar *) g_malloc(MAX_BUFFER);
  world->pbuf_alloc_size = MAX_BUFFER;

  /* Copy defaults */
  world->charset       = g_strdup(default_world->charset);
  world->charset_index = default_world->charset_index;

  world->terminalfont    = g_strdup(default_world->terminalfont);
  world->entryfont       = g_strdup(default_world->entryfont);
  world->statusfont      = g_strdup(default_world->statusfont);
  world->usebold         = default_world->usebold;
  world->wrap            = default_world->wrap;
  world->wrap_indent     = default_world->wrap_indent;
  world->use_wrap_column = default_world->use_wrap_column;
  world->wrap_column     = default_world->wrap_column;
  world->use_tooltips    = default_world->use_tooltips;

  memcpy(world->deffore,     default_world->deffore,     sizeof(GdkRGBA));
  memcpy(world->defbold,     default_world->defbold,     sizeof(GdkRGBA));
  memcpy(world->defback,     default_world->defback,     sizeof(GdkRGBA));
  memcpy(world->defboldback, default_world->defboldback, sizeof(GdkRGBA));
  memcpy(world->ansicolors,  default_world->ansicolors,  16*sizeof(GdkRGBA));
  world->name_display_style   = default_world->name_display_style;
  world->name_display_pattern = g_strdup(default_world->name_display_pattern);

  world->scrollOutput        = default_world->scrollOutput;
  world->buffer_lines        = default_world->buffer_lines;

  world->repeat_commands     = default_world->repeat_commands;
  world->cmd_echo            = default_world->cmd_echo;
  world->never_hide_input    = default_world->never_hide_input;
  world->store_commands      = default_world->store_commands;
  world->commands_to_save    = default_world->commands_to_save;
  world->input_n_lines       = default_world->input_n_lines;
  world->input_n_lines_saved = default_world->input_n_lines_saved;
  world->spell               = default_world->spell;
  if (default_world->spell_language)
    world->spell_language    = g_strdup(default_world->spell_language);
  world->autocompletion      = default_world->autocompletion;
  world->autocompl_minprefix = default_world->autocompl_minprefix;
  strcpy(world->command_separator, default_world->command_separator);
  world->perl_character      = default_world->perl_character;

  world->confirm_delete = default_world->confirm_delete;

  world->flood_prevention         = default_world->flood_prevention;
  world->max_equal_commands       = default_world->max_equal_commands;
  world->flood_prevention_command
    = g_strdup(default_world->flood_prevention_command);
  world->repeat_count             = 1;

  world->log_file_name  = g_strdup(default_world->log_file_name);
  world->log_timeformat = g_strdup(default_world->log_timeformat);
  world->log_add_time   = default_world->log_add_time;
  world->log_autostart  = default_world->log_autostart;

  world->keep_alive = default_world->keep_alive;

  /* Initialize command history list. */
  world->cmd_list = GTK_TREE_MODEL(gtk_list_store_new(N_CMDLIST_COLUMNS,
                                                      G_TYPE_STRING,
                                                      G_TYPE_STRING));
  gtk_list_store_append(GTK_LIST_STORE(world->cmd_list), &world->current_cmd);
  gtk_list_store_set(GTK_LIST_STORE(world->cmd_list),
                     &world->current_cmd,
                     CMDLIST_COL_COMMAND, NULL,
                     CMDLIST_COL_TENTATIVE, "",
                     -1);
  world->cmd_has_been_edited = 1;

  /* Create timer to hold the time since last writing of data */
  world->last_data_written = g_timer_new();

  return world;
}


void
create_default_world(void)
{
  const gchar *kilddir;
  gchar       *default_world_file;

  default_world = g_new0(World, 1);
  world_allocate_space_for_colors(default_world);

  /* Built-in defaults */
  default_world->charset = g_strdup("ISO-8859-1");
  default_world->charset_index = -1;

  default_world->terminalfont    = g_strdup(DEFAULT_TERMINAL_FONT);
  default_world->entryfont       = g_strdup(DEFAULT_TERMINAL_FONT);
  default_world->statusfont      = g_strdup("Sans 8");
  default_world->usebold         = TRUE;
  default_world->wrap            = TRUE;
  default_world->wrap_indent     = 0;
  default_world->wrap_column     = 40;
  default_world->use_wrap_column = FALSE;
  default_world->use_tooltips    = TRUE;

  memcpy(default_world->deffore,     &deffore,     sizeof(GdkRGBA));
  memcpy(default_world->defbold,     &defbold,     sizeof(GdkRGBA));
  memcpy(default_world->defback,     &defback,     sizeof(GdkRGBA));
  memcpy(default_world->defboldback, &defboldback, sizeof(GdkRGBA));
  memcpy(default_world->ansicolors,  &defansi,     16*sizeof(GdkRGBA));
  default_world->name_display_style   = NAME_DISPLAY_WORLD;
  default_world->name_display_pattern = g_strdup("%Kw [%Kc]");

  default_world->scrollOutput        = FALSE;
  default_world->buffer_lines        = 2000;

  default_world->repeat_commands     = FALSE;
  default_world->cmd_echo            = TRUE;
  default_world->never_hide_input    = FALSE;
  default_world->store_commands      = TRUE;
  default_world->commands_to_save    = 50;
  default_world->input_n_lines       = 1;
  default_world->input_n_lines_saved = 2;
  default_world->spell               = 1;
  default_world->autocompletion      = TRUE;
  default_world->autocompl_minprefix = 3;
  strcpy(default_world->command_separator, "%;");
  default_world->perl_character      = '/';

  default_world->confirm_delete = TRUE;

  default_world->flood_prevention         = 1;
  default_world->max_equal_commands       = 20;
  default_world->flood_prevention_command = g_strdup("look");

  default_world->log_timeformat = g_strdup(DEFAULT_LOGTIME_FORMAT);
  default_world->log_add_time   = TRUE;
  default_world->log_autostart  = FALSE;

  default_world->keep_alive = FALSE;

  /* Now load everything from the file */
  kilddir = get_kildclient_directory_path();
  default_world_file = g_build_filename(kilddir, "defworld.cfg", NULL);
  if (!load_world_from_file(default_world_file,
                            default_world,
                            FALSE,
                            NULL)) {
    default_world->file = default_world_file;
  } else {
    g_free(default_world_file);
  }
}


static
void
world_allocate_space_for_colors(World *world)
{
  world->deffore     = g_new0(GdkRGBA, 1);
  world->defbold     = g_new0(GdkRGBA, 1);
  world->defback     = g_new0(GdkRGBA, 1);
  world->defboldback = g_new0(GdkRGBA, 1);
  world->ansicolors  = g_new0(GdkRGBA, 16);
}


void
init_threads(World *world)
{
  g_mutex_init(&world->write_log_line_mutex);

  g_mutex_init(&world->wait_command_mutex);
  g_cond_init(&world->wait_command_cond);

  world->process_line_operations
    = g_async_queue_new_full(free_process_line_operation);
  world->process_line_thread = g_thread_new("process_line",
                                             process_line_thread, world);

  world->ansiparse_operations
    = g_async_queue_new_full(free_ansiparse_operation);
  world->ansiparse_thread = g_thread_new("ansiparse", ansiparse_thread, world);

  world->mainthread_operations
    = g_async_queue_new_full(free_mainthread_operation);
  world->mainthread_operations_idle_id
    = g_timeout_add(100, mainthread_operations_idle_cb, world);
}


void
free_world(World *world)
{
  if (world->has_unread_text) {
    --worlds_with_new_text;
    adjust_window_title();
  }

  if (world->ansiparse_thread) {
    AnsiParseOperation *ansi_op;
    ProcessLineOperation *process_line_op;

    g_source_remove(world->mainthread_operations_idle_id);
    g_async_queue_unref(world->mainthread_operations);

    g_async_queue_unref(world->ansiparse_operations);
    g_async_queue_unref(world->process_line_operations);

    world->to_be_destructed = TRUE;

    /* We add an operation so that the threads wakes up (if the queue
       is empty, it might never notice that it should be
       destroyed). */
    ansi_op = g_new(AnsiParseOperation, 1);
    ansi_op->action = DUMMY_ANSIPARSE;
    g_async_queue_push(world->ansiparse_operations, ansi_op);
    /* Wait for the thread to exit. */
    g_thread_join(world->ansiparse_thread);

    /* The same, for process_line_thread */
    process_line_op = g_new(ProcessLineOperation, 1);
    process_line_op->action = DUMMY_PROCESS_LINE;
    g_async_queue_push(world->process_line_operations, process_line_op);

    /* Notify a possibly waiting match trigger thread */
    g_mutex_lock(&world->wait_command_mutex);
    world->command_executed = TRUE;
    g_cond_signal(&world->wait_command_cond);
    g_mutex_unlock(&world->wait_command_mutex);

    /* Wait for the thread to exit */
    g_thread_join(world->process_line_thread);

    g_mutex_clear(&world->write_log_line_mutex);

    g_cond_clear(&world->wait_command_cond);
    g_mutex_clear(&world->wait_command_mutex);
  }

  g_free(world->name);
  g_free(world->display_name);
  g_free(world->new_text_name);
  g_free(world->file);
  g_free(world->charset);
  g_free(world->host);
  g_free(world->port);
  g_free(world->host_ip);

  g_free(world->pbuffer);
  g_free(world->server_data_buffer);

  if (world->character_used) {
    gtk_tree_row_reference_free(world->character_used);
  }
  g_object_unref(world->logon_characters);

  g_free(world->terminalfont);
  g_free(world->entryfont);
  g_free(world->statusfont);

  g_free(world->deffore);
  g_free(world->defbold);
  g_free(world->defback);
  g_free(world->defboldback);
  g_free(world->ansicolors);
  g_free(world->name_display_pattern);

  proxy_info_free(&world->proxy);
  proxy_info_free(&world->proxy_used);

  g_free(world->scriptfile);

  if (world->perl_interpreter) {
    GRAB_PERL(world);
  }

  g_slist_foreach(world->plugins,
                  (GFunc) free_plugin, world);
  g_slist_free(world->plugins);

  g_slist_foreach(world->triggers, (GFunc) free_trigger, NULL);
  g_slist_free(world->triggers);
  if (world->trigger_action_re) {
    SvREFCNT_dec(world->trigger_action_re);
  }

  g_slist_foreach(world->aliases, (GFunc) free_alias, NULL);
  g_slist_free(world->aliases);

  g_slist_foreach(world->macros, (GFunc) free_macro, NULL);
  g_slist_free(world->macros);

  g_slist_foreach(world->timers, (GFunc) free_timer, NULL);
  g_slist_free(world->timers);

  g_slist_foreach(world->hooks.OnConnect, (GFunc) free_hook, NULL);
  g_slist_free(world->hooks.OnConnect);
  g_slist_foreach(world->hooks.OnDisconnect, (GFunc) free_hook, NULL);
  g_slist_free(world->hooks.OnDisconnect);
  g_slist_foreach(world->hooks.OnReceivedText, (GFunc) free_hook, NULL);
  g_slist_free(world->hooks.OnReceivedText);
  g_slist_foreach(world->hooks.OnSentCommand, (GFunc) free_hook, NULL);
  g_slist_free(world->hooks.OnSentCommand);
  g_slist_foreach(world->hooks.OnGetFocus, (GFunc) free_hook, NULL);
  g_slist_free(world->hooks.OnGetFocus);
  g_slist_foreach(world->hooks.OnLoseFocus, (GFunc) free_hook, NULL);
  g_slist_free(world->hooks.OnLoseFocus);
  g_slist_foreach(world->hooks.OnCloseConnected, (GFunc) free_hook, NULL);
  g_slist_free(world->hooks.OnCloseConnected);
  g_slist_foreach(world->hooks.OnServerData, (GFunc) free_hook, NULL);
  g_slist_free(world->hooks.OnServerData);

  g_slist_foreach(world->permanent_variables,
                  (GFunc) free_permanent_variable, NULL);
  g_slist_free(world->permanent_variables);

  if (world->perl_interpreter) {
    RELEASE_PERL(world);
  }

  g_free(world->flood_prevention_command);
  g_free(world->last_command);
  if (world->cmd_list) {
    g_object_unref(world->cmd_list);
  }

  stop_log(world);
  g_free(world->log_file_name);
  g_free(world->log_timeformat);
  g_free(world->log_actual_file);

  if (world->connected_at_least_once) {
    destruct_perl_script(world->perl_interpreter);
  }

  if (world->dlgEditWorld) {
    gtk_widget_destroy(world->dlgEditWorld);

    g_object_unref(world->hooks.OnConnect_model_filter);
    g_object_unref(world->hooks.OnDisconnect_model_filter);
    g_object_unref(world->hooks.OnReceivedText_model_filter);
    g_object_unref(world->hooks.OnSentCommand_model_filter);
    g_object_unref(world->hooks.OnGetFocus_model_filter);
    g_object_unref(world->hooks.OnLoseFocus_model_filter);
    g_object_unref(world->hooks.OnCloseConnected_model_filter);
    g_object_unref(world->hooks.OnServerData_model_filter);

    g_slist_foreach(world->selected_OnConnect_hooks,
                    (GFunc) gtk_tree_row_reference_free, NULL);
    g_slist_free(world->selected_OnConnect_hooks);
    g_slist_foreach(world->selected_OnDisconnect_hooks,
                    (GFunc) gtk_tree_row_reference_free, NULL);
    g_slist_free(world->selected_OnDisconnect_hooks);
    g_slist_foreach(world->selected_OnReceivedText_hooks,
                    (GFunc) gtk_tree_row_reference_free, NULL);
    g_slist_free(world->selected_OnReceivedText_hooks);
    g_slist_foreach(world->selected_OnSentCommand_hooks,
                    (GFunc) gtk_tree_row_reference_free, NULL);
    g_slist_free(world->selected_OnSentCommand_hooks);
    g_slist_foreach(world->selected_OnGetFocus_hooks,
                    (GFunc) gtk_tree_row_reference_free, NULL);
    g_slist_free(world->selected_OnGetFocus_hooks);
    g_slist_foreach(world->selected_OnLoseFocus_hooks,
                    (GFunc) gtk_tree_row_reference_free, NULL);
    g_slist_free(world->selected_OnLoseFocus_hooks);
    g_slist_foreach(world->selected_OnCloseConnected_hooks,
                    (GFunc) gtk_tree_row_reference_free, NULL);
    g_slist_free(world->selected_OnCloseConnected_hooks);
    g_slist_foreach(world->selected_OnServerData_hooks,
                    (GFunc) gtk_tree_row_reference_free, NULL);
    g_slist_free(world->selected_OnServerData_hooks);
  }

  if (world->dlgMLSend) {
    gtk_widget_destroy(world->dlgMLSend);
  }

  if (world->dlgCmdHistoryFind) {
    gtk_widget_destroy(world->dlgCmdHistoryFind);
  }
  if (world->dlgCmdHistory) {
    gtk_widget_destroy(world->dlgCmdHistory);
  }
  g_free(world->cmdfind_string);
  if (world->cmdfind_row) {
    gtk_tree_row_reference_free(world->cmdfind_row);
  }

  if (world->dlgTestTriggers) {
    gtk_widget_destroy(world->dlgTestTriggers);
  }

  if (world->ui_builder) {
    g_object_unref(world->ui_builder);
  }

  g_object_unref(world->startup_plugins);

  if (world->last_data_written) {
    g_timer_destroy(world->last_data_written);
  }

  g_rec_mutex_clear(&world->perl_mutex);

  g_free(world);
}


void
remove_plugin_objects(World *world, Plugin *plugin)
{
  GSList *iter;
  GSList *next;

  for (iter = world->triggers; iter; iter = next) {
    /* Must do this here, since the list item may be deleted. */
    next = iter->next;

    Trigger *trigger = (Trigger *) iter->data;
    if (trigger->owner_plugin == plugin) {
      remove_trigger(world, iter);
    }
  }

  for (iter = world->aliases; iter; iter = next) {
    /* Must do this here, since the list item may be deleted. */
    next = iter->next;

    Alias *alias = (Alias *) iter->data;
    if (alias->owner_plugin == plugin) {
      remove_alias(world, iter);
    }
  }

  for (iter = world->macros; iter; iter = next) {
    /* Must do this here, since the list item may be deleted. */
    next = iter->next;

    Macro *macro = (Macro *) iter->data;
    if (macro->owner_plugin == plugin) {
      remove_macro(world, iter);
    }
  }

  for (iter = world->timers; iter; iter = next) {
    /* Must do this here, since the list item may be deleted. */
    next = iter->next;

    Timer *timer = (Timer *) iter->data;
    if (timer->owner_plugin == plugin) {
      remove_timer(world, iter);
    }
  }

  for (iter = world->hooks.OnConnect; iter; iter = next) {
    /* Must do this here, since the list item may be deleted. */
    next = iter->next;

    Hook *hook = (Hook *) iter->data;
    if (hook->owner_plugin == plugin) {
      delete_hook(world, "OnConnect", 0, iter);
    }
  }

  for (iter = world->hooks.OnDisconnect; iter; iter = next) {
    /* Must do this here, since the list item may be deleted. */
    next = iter->next;

    Hook *hook = (Hook *) iter->data;
    if (hook->owner_plugin == plugin) {
      delete_hook(world, "OnDisconnect", 0, iter);
    }
  }

  for (iter = world->hooks.OnReceivedText; iter; iter = next) {
    /* Must do this here, since the list item may be deleted. */
    next = iter->next;

    Hook *hook = (Hook *) iter->data;
    if (hook->owner_plugin == plugin) {
      delete_hook(world, "OnReceivedText", 0, iter);
    }
  }

  for (iter = world->hooks.OnSentCommand; iter; iter = next) {
    /* Must do this here, since the list item may be deleted. */
    next = iter->next;

    Hook *hook = (Hook *) iter->data;
    if (hook->owner_plugin == plugin) {
      delete_hook(world, "OnSentCommand", 0, iter);
    }
  }

  for (iter = world->hooks.OnGetFocus; iter; iter = next) {
    /* Must do this here, since the list item may be deleted. */
    next = iter->next;

    Hook *hook = (Hook *) iter->data;
    if (hook->owner_plugin == plugin) {
      delete_hook(world, "OnGetFocus", 0, iter);
    }
  }

  for (iter = world->hooks.OnLoseFocus; iter; iter = next) {
    /* Must do this here, since the list item may be deleted. */
    next = iter->next;

    Hook *hook = (Hook *) iter->data;
    if (hook->owner_plugin == plugin) {
      delete_hook(world, "OnLoseFocus", 0, iter);
    }
  }

  for (iter = world->hooks.OnCloseConnected; iter; iter = next) {
    /* Must do this here, since the list item may be deleted. */
    next = iter->next;

    Hook *hook = (Hook *) iter->data;
    if (hook->owner_plugin == plugin) {
      delete_hook(world, "OnCloseConnected", 0, iter);
    }
  }

  for (iter = world->hooks.OnServerData; iter; iter = next) {
    /* Must do this here, since the list item may be deleted. */
    next = iter->next;

    Hook *hook = (Hook *) iter->data;
    if (hook->owner_plugin == plugin) {
      delete_hook(world, "OnServerData", 0, iter);
    }
  }
}


void
create_world_from_parameters(World *world,
                             gchar *host,
                             gchar *port,
                             gchar *user,
                             gchar *password)

{
  world->name = g_strdup(_("Unnamed world"));
  world->host = host;
  if (port && strcmp(port, "") != 0) {
    world->port = port;
  } else {
    world->port = g_strdup("4000");
    g_free(port);
  }

  if (user && strcmp(user, "") != 0) {
    GtkTreePath *treepath;
    GtkTreeIter  iter;

    /* Currently there is no way of selecting another connection style */
    world->connection_style = DIKU;

    gtk_list_store_append(GTK_LIST_STORE(world->logon_characters), &iter);
    gtk_list_store_set(GTK_LIST_STORE(world->logon_characters), &iter,
                       LOGON_CHAR, user,
                       LOGON_PASS, password,
                       -1);
    treepath = gtk_tree_model_get_path(world->logon_characters, &iter);
    world->character_used = gtk_tree_row_reference_new(world->logon_characters,
                                                       treepath);
    gtk_tree_path_free(treepath);

    prepare_display_name(world);
  } else {
    world->character_used = NULL;    /* No auto-logon */
    /* Since there is never a character in this case, it makes no
       sense to use the default world settings for display name. */
    world->display_name = g_strdup(world->name);
    world->new_text_name =
      g_strdup_printf("<span foreground=\"red\">%s</span>",
                      world->display_name);
  }
  g_free(user);
  g_free(password);
}


gboolean
load_world_from_file(const char *file,
                     World      *world,
                     gboolean    forWorldSelector,
                     GError    **error)
{
  FILE                *fp;
  char                 buffer[MAX_BUFFER];
  gboolean             success;
  WorldParser         *parser;
  GMarkupParseContext *parseContext;

  fp = fopen(file, "r");
  if (!fp) {
    g_set_error(error,
                G_FILE_ERROR, G_FILE_ERROR_NOENT,
                _("Could not open file '%s': %s"), file, g_strerror(errno));
    return FALSE;
  }

  world->file = strdup(file);
  world->charset_index = -1;

  parser = g_new0(WorldParser, 1);
  parser->state = KC_START;
  parser->world = world;
  parser->forWorldSelector = forWorldSelector;

  parseContext = g_markup_parse_context_new(&xmlWorldParser,
                                            0,
                                            parser,
                                            NULL);

  success = FALSE;
  while (1) {
    if (fgets(buffer, MAX_BUFFER, fp)) {
      success = g_markup_parse_context_parse(parseContext,
                                             buffer,
                                             strlen(buffer),
                                             error);
    }

    if (feof(fp)) {
      success &= g_markup_parse_context_end_parse(parseContext,
                                                  error);
      break;
    }
    if (!success)
      break;
  }

  fclose(fp);

  g_markup_parse_context_free(parseContext);
  g_free(parser);

  return success;
}


static
void
xml_start_element(GMarkupParseContext *context,
                  const gchar         *element_name,
                  const gchar        **attribute_names,
                  const gchar        **attribute_values,
                  gpointer             user_data,
                  GError             **error)
{
  WorldParser *parser = (WorldParser *)user_data;

  switch (parser->state) {
  case KC_START:
    if (strcmp(element_name, "kcworld") == 0
        || strcmp(element_name, "kcworld-export") == 0) {
      parser->state = KC_KCWORLD;
    } else {
      g_set_error(error,
                  G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ELEMENT,
                  _("Invalid top-level element '%s'"), element_name);
    }
    break;

  case KC_KCWORLD:
    if (strcmp(element_name, "name") == 0) {
      parser->currText = &parser->world->name;
      parser->state = KC_NAME;
    } else if (strcmp(element_name, "host") == 0) {
      parser->currText = &parser->world->host;
      parser->state = KC_HOST;
      if (parser->forWorldSelector) {
        break;
      }

      while (*attribute_names) {
        if (strcmp(*attribute_names, "tls") == 0) {
          parser->world->use_tls = atoi(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }
        ++attribute_names;
        ++attribute_values;
      }
    } else if (strcmp(element_name, "port") == 0) {
      parser->currText = &parser->world->port;
      parser->state = KC_PORT;
    } else if (strcmp(element_name, "charset") == 0) {
      parser->currText = &parser->world->charset;
      parser->state = KC_CHARSET;
      g_free(parser->world->charset);
    } else if (strcmp(element_name, "proxy") == 0) {
      parser->state = KC_PROXY;
      if (parser->forWorldSelector) {
        break;
      }

      while (*attribute_names) {
        if (strcmp(*attribute_names, "type") == 0) {
          parser->world->proxy.type = atoi(*attribute_values);
        } else if (strcmp(*attribute_names, "server") == 0) {
          parser->world->proxy.server = g_strdup(*attribute_values);
        } else if (strcmp(*attribute_names, "port") == 0) {
          parser->world->proxy.port = g_strdup(*attribute_values);
        } else if (strcmp(*attribute_names, "user") == 0) {
          parser->world->proxy.user = g_strdup(*attribute_values);
        } else if (strcmp(*attribute_names, "password") == 0) {
          parser->world->proxy.password = g_strdup(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }
        ++attribute_names;
        ++attribute_values;
      }
    } else if (strcmp(element_name, "display") == 0) {
      parser->state = KC_DISPLAY;
    } else if (strcmp(element_name, "scroll") == 0) {
      parser->state = KC_SCROLL;
      if (parser->forWorldSelector) {
        break;
      }

      while (*attribute_names) {
        if (strcmp(*attribute_names, "on-output") == 0) {
          parser->world->scrollOutput = atoi(*attribute_values);
        } else if (strcmp(*attribute_names, "lines") == 0) {
          parser->world->buffer_lines = atol(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }
        ++attribute_names;
        ++attribute_values;
      }
    } else if (strcmp(element_name, "name-display") == 0) {
      parser->state = KC_NAMEDISPLAY;
      if (parser->forWorldSelector) {
        break;
      }

      while (*attribute_names) {
        if (strcmp(*attribute_names, "style") == 0) {
          parser->world->name_display_style = atoi(*attribute_values);
        } else if (strcmp(*attribute_names, "pattern") == 0) {
          parser->world->name_display_pattern = g_strdup(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }
        ++attribute_names;
        ++attribute_values;
      }
    } else if (strcmp(element_name, "log") == 0) {
      parser->state = KC_LOG;
      if (parser->forWorldSelector) {
        break;
      }

      while (*attribute_names) {
        if (strcmp(*attribute_names, "autostart") == 0) {
          parser->world->log_autostart = atoi(*attribute_values);
        } else if (strcmp(*attribute_names, "addtime") == 0) {
          parser->world->log_add_time = atoi(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }
        ++attribute_names;
        ++attribute_values;
      }
    } else if (strcmp(element_name, "auto-logon") == 0) {
      parser->state = KC_AUTO_LOGON;
      if (parser->forWorldSelector) {
        break;
      }

      while (*attribute_names) {
        if (strcmp(*attribute_names, "style") == 0) {
          parser->world->connection_style = atoi(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }
        ++attribute_names;
        ++attribute_values;
      }
    } else if (strcmp(element_name, "flood-prevention") == 0) {
      parser->currText = &parser->world->flood_prevention_command;
      parser->state = KC_FLOOD_PREVENTION;
      if (parser->forWorldSelector) {
        break;
      }

      g_free(parser->world->flood_prevention_command);

      while (*attribute_names) {
        if (strcmp(*attribute_names, "enabled") == 0) {
          parser->world->flood_prevention = atoi(*attribute_values);
        } else if (strcmp(*attribute_names, "max-repeat") == 0) {
          parser->world->max_equal_commands = atoi(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }
        ++attribute_names;
        ++attribute_values;
      }
    } else if (strcmp(element_name, "repeat-commands") == 0) {
      parser->state = KC_REPEAT_COMMANDS;
      if (parser->forWorldSelector) {
        break;
      }

      while (*attribute_names) {
        if (strcmp(*attribute_names, "enabled") == 0) {
          parser->world->repeat_commands = atoi(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }
        ++attribute_names;
        ++attribute_values;
      }
    } else if (strcmp(element_name, "command-echo") == 0) {
      parser->state = KC_COMMAND_ECHO;
      if (parser->forWorldSelector) {
        break;
      }

      while (*attribute_names) {
        if (strcmp(*attribute_names, "enabled") == 0) {
          parser->world->cmd_echo = atoi(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }
        ++attribute_names;
        ++attribute_values;
      }
    } else if (strcmp(element_name, "never-hide-input") == 0) {
      parser->state = KC_NEVER_HIDE_INPUT;
      if (parser->forWorldSelector) {
        break;
      }

      while (*attribute_names) {
        if (strcmp(*attribute_names, "enabled") == 0) {
          parser->world->never_hide_input = atoi(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }
        ++attribute_names;
        ++attribute_values;
      }
    } else if (strcmp(element_name, "store-commands") == 0) {
      parser->state = KC_STORE_COMMANDS;
      if (parser->forWorldSelector) {
        break;
      }

      while (*attribute_names) {
        if (strcmp(*attribute_names, "enabled") == 0) {
          parser->world->store_commands = atoi(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }
        ++attribute_names;
        ++attribute_values;
      }
    } else if (strcmp(element_name, "command-history") == 0) {
      parser->state = KC_COMMAND_HISTORY;
      if (parser->forWorldSelector) {
        break;
      }

      while (*attribute_names) {
        if (strcmp(*attribute_names, "size") == 0) {
          parser->world->commands_to_save = atoi(*attribute_values);
        } else if (strcmp(*attribute_names, "ignore-up-down-keys") == 0) {
          parser->world->ignore_up_down_keys = atoi(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }
        ++attribute_names;
        ++attribute_values;
      }
    } else if (strcmp(element_name, "auto-completion") == 0) {
      parser->state = KC_AUTO_COMPLETION;
      if (parser->forWorldSelector) {
        break;
      }

      while (*attribute_names) {
        if (strcmp(*attribute_names, "enabled") == 0) {
          parser->world->autocompletion = atoi(*attribute_values);
        } else if (strcmp(*attribute_names, "minprefix") == 0) {
          parser->world->autocompl_minprefix = atoi(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }
        ++attribute_names;
        ++attribute_values;
      }
    } else if (strcmp(element_name, "command-separator") == 0) {
      parser->state = KC_COMMAND_SEPARATOR;
      if (parser->forWorldSelector) {
        break;
      }

      while (*attribute_names) {
        if (strcmp(*attribute_names, "separator") == 0) {
          strcpy(parser->world->command_separator, *attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }
        ++attribute_names;
        ++attribute_values;
      }
    } else if (strcmp(element_name, "perl-character") == 0) {
      parser->state = KC_PERL_CHARACTER;
      if (parser->forWorldSelector) {
        break;
      }

      while (*attribute_names) {
        if (strcmp(*attribute_names, "char") == 0) {
          parser->world->perl_character = *attribute_values[0];
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }
        ++attribute_names;
        ++attribute_values;
      }
    } else if (strcmp(element_name, "input-lines") == 0) {
      parser->state = KC_INPUT_LINES;
      if (parser->forWorldSelector) {
        break;
      }

      while (*attribute_names) {
        if (strcmp(*attribute_names, "size") == 0) {
          parser->world->input_n_lines = atoi(*attribute_values);
        } else if (strcmp(*attribute_names, "save") == 0) {
          parser->world->input_n_lines_saved = atoi(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }
        ++attribute_names;
        ++attribute_values;
      }
    } else if (strcmp(element_name, "input-spell") == 0) {
      parser->state = KC_INPUT_SPELL;
      if (parser->forWorldSelector) {
        break;
      }

      while (*attribute_names) {
        if (strcmp(*attribute_names, "active") == 0) {
          parser->world->spell = atoi(*attribute_values);
        } else if (strcmp(*attribute_names, "language") == 0) {
          if (strcmp(*attribute_values, "") != 0) {
            parser->world->spell_language = g_strdup(*attribute_values);
          }
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }
        ++attribute_names;
        ++attribute_values;
      }
    } else if (strcmp(element_name, "confirm-delete") == 0) {
      parser->state = KC_CONFIRM_DELETE;
      if (parser->forWorldSelector) {
        break;
      }

      while (*attribute_names) {
        if (strcmp(*attribute_names, "confirm") == 0) {
          parser->world->confirm_delete = atoi(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }
        ++attribute_names;
        ++attribute_values;
      }
    } else if (strcmp(element_name, "plugin-items") == 0) {
      parser->state = KC_PLUGIN_ITEMS;
      if (parser->forWorldSelector) {
        break;
      }

      while (*attribute_names) {
        if (strcmp(*attribute_names, "show") == 0) {
          parser->world->show_plugin_items = atoi(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }
        ++attribute_names;
        ++attribute_values;
      }
    } else if (strcmp(element_name, "keep-alive") == 0) {
      parser->state = KC_KEEP_ALIVE;
      if (parser->forWorldSelector) {
        break;
      }

      while (*attribute_names) {
        if (strcmp(*attribute_names, "enabled") == 0) {
          parser->world->keep_alive = atoi(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }
        ++attribute_names;
        ++attribute_values;
      }
    } else if (strcmp(element_name, "scriptfile") == 0) {
      parser->currText = &parser->world->scriptfile;
      parser->state = KC_SCRIPTFILE;
    } else if (strcmp(element_name, "plugins") == 0) {
      parser->state = KC_PLUGINS;
    } else if (strcmp(element_name, "triggers") == 0) {
      parser->state = KC_TRIGGERS;
    } else if (strcmp(element_name, "aliases") == 0) {
      parser->state = KC_ALIASES;
    } else if (strcmp(element_name, "macros") == 0) {
      parser->state = KC_MACROS;
    } else if (strcmp(element_name, "timers") == 0) {
      parser->state = KC_TIMERS;
    } else if (strcmp(element_name, "variables") == 0) {
      parser->state = KC_VARIABLES;
    } else if (strcmp(element_name, "hooksv2") == 0) {
      parser->state = KC_HOOKSV2;
    } else if (strcmp(element_name, "protocols") == 0) {
      parser->state = KC_PROTOCOLS;
    } else {
      xml_error_invalid_inside(error, context);
    }
    break;

  case KC_DISPLAY:
    if (strcmp(element_name, "terminalfont") == 0) {
      parser->currText = &parser->world->terminalfont;
      parser->state = KC_TERMINALFONT;
      g_free(parser->world->terminalfont);
    } else if (strcmp(element_name, "entryfont") == 0) {
      parser->currText = &parser->world->entryfont;
      parser->state = KC_ENTRYFONT;
      g_free(parser->world->entryfont);
    } else if (strcmp(element_name, "usebold") == 0) {
      parser->state = KC_USEBOLD;
    } else if (strcmp(element_name, "wrap") == 0) {
      parser->state = KC_WRAP;
      if (parser->forWorldSelector) {
        break;
      }

      while (*attribute_names) {
        if (strcmp(*attribute_names, "enabled") == 0) {
          parser->world->wrap = atoi(*attribute_values);
        } else if (strcmp(*attribute_names, "indent") == 0) {
          parser->world->wrap_indent = atoi(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }

        ++attribute_names;
        ++attribute_values;
      }
    } else if (strcmp(element_name, "tooltips") == 0) {
      parser->state = KC_TOOLTIPS;
      if (parser->forWorldSelector) {
        break;
      }

      while (*attribute_names) {
        if (strcmp(*attribute_names, "enabled") == 0) {
          parser->world->use_tooltips = atoi(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }

        ++attribute_names;
        ++attribute_values;
      }
    } else if (strcmp(element_name, "statusfont") == 0) {
      parser->currText = &parser->world->statusfont;
      parser->state = KC_STATUSFONT;
      g_free(parser->world->statusfont);
    } else if (strcmp(element_name, "statusbar") == 0) {
      parser->state = KC_STATUSBAR;
      if (parser->forWorldSelector) {
        break;
      }

      while (*attribute_names) {
        if (strcmp(*attribute_names, "ctime") == 0) {
          parser->world->ctime_type = atoi(*attribute_values);
        } else if (strcmp(*attribute_names, "itime") == 0) {
          parser->world->itime_type = atoi(*attribute_values);
        } else if (strcmp(*attribute_names, "itime-behavior") == 0) {
          parser->world->itime_reset_activate = atoi(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }

        ++attribute_names;
        ++attribute_values;
      }
    } else if (strcmp(element_name, "defforecolor") == 0) {
      parser->currColor = parser->world->deffore;
      parser->state = KC_DEFFORECOLOR;
    } else if (strcmp(element_name, "defbackcolor") == 0) {
      parser->currColor = parser->world->defback;
      parser->state = KC_DEFBACKCOLOR;
    } else if (strcmp(element_name, "defboldcolor") == 0) {
      parser->currColor = parser->world->defbold;
      parser->state = KC_DEFBOLDCOLOR;
    } else if (strcmp(element_name, "defboldbackcolor") == 0) {
      parser->currColor = parser->world->defboldback;
      parser->state = KC_DEFBOLDBACKCOLOR;
    } else if (strcmp(element_name, "ansicolor") == 0) {
      parser->currColor = parser->world->ansicolors;
      parser->state = KC_ANSICOLOR;
    } else if (strcmp(element_name, "background") == 0) {
      parser->state = KC_BACKGROUND;
      /* Ignored, but kept for backwards compatibility. */
    } else {
      xml_error_invalid_inside(error, context);
    }
    break;

  case KC_WRAP:
    if (strcmp(element_name, "wrap-column") == 0) {
      parser->state = KC_WRAP_COLUMN;
      if (parser->forWorldSelector) {
        break;
      }

      while (*attribute_names) {
        if (strcmp(*attribute_names, "enabled") == 0) {
          parser->world->use_wrap_column = atoi(*attribute_values);
        } else if (strcmp(*attribute_names, "column") == 0) {
          parser->world->wrap_column = atoi(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }

        ++attribute_names;
        ++attribute_values;
      }
    } else {
      xml_error_invalid_inside(error, context);
    }
    break;

  case KC_AUTO_LOGON:
    if (strcmp(element_name, "character") == 0) {
      parser->state = KC_CHARACTER;
    } else if (strcmp(element_name, "password") == 0) {
      parser->state = KC_PASSWORD;
    } else {
      xml_error_invalid_inside(error, context);
    }
    break;

  case KC_LOG:
    if (strcmp(element_name, "logfile") == 0) {
      parser->currText = &parser->world->log_file_name;
      parser->state = KC_LOGFILE;
    } else if (strcmp(element_name, "logtimeformat") == 0) {
      g_free(parser->world->log_timeformat);
      parser->currText = &parser->world->log_timeformat;
      parser->state = KC_LOGTIMEFORMAT;
    } else {
      xml_error_invalid_inside(error, context);
    }
    break;

  case KC_PLUGINS:
    if (strcmp(element_name, "plugin") == 0) {
      parser->currText = &parser->currPluginFile;
      parser->state = KC_PLUGIN;
    } else {
      xml_error_invalid_inside(error, context);
    }
    break;

  case KC_TRIGGERS:
    if (strcmp(element_name, "trigger") == 0) {
      parser->state = KC_TRIGGER;
      if (parser->forWorldSelector) {
        break;
      }

      parser->currTrigger = new_trigger();

      while (*attribute_names) {
        if (strcmp(*attribute_names, "name") == 0) {
          parser->currTrigger->name = g_strdup(*attribute_values);
        } else if (strcmp(*attribute_names, "gag") == 0) {
          parser->currTrigger->gag_output = atoi(*attribute_values);
        } else if (strcmp(*attribute_names, "gaglog") == 0) {
          parser->currTrigger->gag_log = atoi(*attribute_values);
        } else if (strcmp(*attribute_names, "enabled") == 0) {
          parser->currTrigger->enabled = atoi(*attribute_values);
        } else if (strcmp(*attribute_names, "keepexecuting") == 0) {
          parser->currTrigger->keepexecuting = atoi(*attribute_values);
        } else if (strcmp(*attribute_names, "rewriter") == 0) {
          parser->currTrigger->rewriter = atoi(*attribute_values);
        } else if (strcmp(*attribute_names, "ignorecase") == 0) {
          parser->currTrigger->ignore_case = atoi(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }
        ++attribute_names;
        ++attribute_values;
      }
    } else {
      xml_error_invalid_inside(error, context);
    }
    break;

  case KC_ALIASES:
    if (strcmp(element_name, "alias") == 0) {
      parser->state = KC_ALIAS;
      if (parser->forWorldSelector) {
        break;
      }

      parser->currAlias = g_new0(Alias, 1);

      while (*attribute_names) {
        if (strcmp(*attribute_names, "name") == 0) {
          parser->currAlias->name = g_strdup(*attribute_values);
        } else if (strcmp(*attribute_names, "perleval") == 0) {
          parser->currAlias->perl_eval = atoi(*attribute_values);
        } else if (strcmp(*attribute_names, "enabled") == 0) {
          parser->currAlias->enabled = atoi(*attribute_values);
        } else if (strcmp(*attribute_names, "ignorecase") == 0) {
          parser->currAlias->ignore_case = atoi(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }
        ++attribute_names;
        ++attribute_values;
      }
    } else {
      xml_error_invalid_inside(error, context);
    }
    break;

  case KC_TIMERS:
    if (strcmp(element_name, "timer") == 0) {
      parser->state = KC_TIMER;
      if (parser->forWorldSelector) {
        break;
      }

      parser->currTimer = g_new0(Timer, 1);
      parser->currTimer->for_world = parser->world;
      parser->currText  = &parser->currTimer->action;

      while (*attribute_names) {
        if (strcmp(*attribute_names, "name") == 0) {
          parser->currTimer->name = g_strdup(*attribute_values);
        } else if (strcmp(*attribute_names, "interval") == 0) {
          parser->currTimer->interval = strtod(*attribute_values, NULL);
        } else if (strcmp(*attribute_names, "count") == 0) {
          parser->currTimer->count = atoi(*attribute_values);
        } else if (strcmp(*attribute_names, "enabled") == 0) {
          parser->currTimer->enabled = atoi(*attribute_values);
        } else if (strcmp(*attribute_names, "temporary") == 0) {
          parser->currTimer->temporary = atoi(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }
        ++attribute_names;
        ++attribute_values;
      }
    } else {
      xml_error_invalid_inside(error, context);
    }
    break;

  case KC_MACROS:
    if (strcmp(element_name, "macro") == 0) {
      parser->state = KC_MACRO;
      if (parser->forWorldSelector) {
        break;
      }

      parser->currMacro = g_new0(Macro, 1);

      while (*attribute_names) {
        if (strcmp(*attribute_names, "name") == 0) {
          parser->currMacro->name = g_strdup(*attribute_values);
        } else if (strcmp(*attribute_names, "enabled") == 0) {
          parser->currMacro->enabled = atoi(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }
        ++attribute_names;
        ++attribute_values;
      }
    } else {
      xml_error_invalid_inside(error, context);
    }
    break;

  case KC_VARIABLES:
    if (strcmp(element_name, "variable") == 0) {
      parser->state = KC_VARIABLE;
      parser->currText = &parser->currVar;
    } else {
      xml_error_invalid_inside(error, context);
    }
    break;

  case KC_HOOKSV2:
    if (strcmp(element_name, "hooklistv2") == 0) {
      parser->state = KC_HOOKLISTV2;
      if (parser->forWorldSelector) {
        break;
      }

      while (*attribute_names) {
        if (strcmp(*attribute_names, "for") == 0) {
          parser->currHookTrigger = g_strdup(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }
        ++attribute_names;
        ++attribute_values;
      }
    } else {
      xml_error_invalid_inside(error, context);
    }
    break;

  case KC_HOOKLISTV2:
    if (strcmp(element_name, "hookv2") == 0) {
      parser->state = KC_HOOKV2;
      parser->previous_state = KC_HOOKLISTV2;
      if (parser->forWorldSelector) {
        break;
      }

      while (*attribute_names) {
        if (strcmp(*attribute_names, "name") == 0) {
          parser->currHookName = g_strdup(*attribute_values);
        } else if (strcmp(*attribute_names, "enabled") == 0) {
          parser->currHookEnabled = atoi(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }
        ++attribute_names;
        ++attribute_values;
      }

      parser->currText = &parser->currHookAction;
    } else {
      xml_error_invalid_inside(error, context);
    }
    break;

  case KC_TRIGGER:
    if (strcmp(element_name, "pattern") == 0) {
      parser->currText = &parser->currTrigger->pattern;
      parser->state = KC_PATTERN;
      parser->previous_state = KC_TRIGGER;
    } else if (strcmp(element_name, "action") == 0) {
      parser->currText = &parser->currTrigger->action;
      parser->state = KC_ACTION;
    } else if (strcmp(element_name, "highlight") == 0) {
      parser->state = KC_HIGHLIGHT;
      if (parser->forWorldSelector) {
        break;
      }

      while (*attribute_names) {
        if (strcmp(*attribute_names, "enabled") == 0) {
          parser->currTrigger->highlight = atoi(*attribute_values);
        } else if (strcmp(*attribute_names, "target") == 0) {
          parser->currTrigger->high_target = atoi(*attribute_values);
        } else if (strcmp(*attribute_names, "fg") == 0) {
          parser->currTrigger->high_fg_color = atoi(*attribute_values);
        } else if (strcmp(*attribute_names, "bg") == 0) {
          parser->currTrigger->high_bg_color = atoi(*attribute_values);
        } else if (strcmp(*attribute_names, "italics") == 0) {
          parser->currTrigger->high_italic = atoi(*attribute_values);
        } else if (strcmp(*attribute_names, "strike") == 0) {
          parser->currTrigger->high_strike = atoi(*attribute_values);
        } else if (strcmp(*attribute_names, "underline") == 0) {
          parser->currTrigger->high_underline = atoi(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }
        ++attribute_names;
        ++attribute_values;
      }
     } else {
      xml_error_invalid_inside(error, context);
    }
    break;

  case KC_ALIAS:
    if (strcmp(element_name, "pattern") == 0) {
      parser->currText = &parser->currAlias->pattern;
      parser->state = KC_PATTERN;
      parser->previous_state = KC_ALIAS;
    } else if (strcmp(element_name, "substitution") == 0) {
      parser->currText = &parser->currAlias->substitution;
      parser->state = KC_SUBSTITUTION;
    } else {
      xml_error_invalid_inside(error, context);
    }
    break;

  case KC_MACRO:
    if (strcmp(element_name, "key") == 0) {
      parser->state = KC_KEY;
    } else if (strcmp(element_name, "action") == 0) {
      parser->currText = &parser->currMacro->action;
      parser->state = KC_ACTION;
      parser->previous_state = KC_MACRO;
    } else {
      xml_error_invalid_inside(error, context);
    }
    break;

  case KC_PROTOCOLS:
    if (strcmp(element_name, "mccp") == 0) {
      parser->state = KC_MCCP;
      if (parser->forWorldSelector) {
        break;
      }

      while (*attribute_names) {
        if (strcmp(*attribute_names, "behavior") == 0) {
          parser->world->mccp_behavior = atoi(*attribute_values);
        } else {
          xml_error_unknown_attribute(error, *attribute_names, context);
        }
        ++attribute_names;
        ++attribute_values;
      }
    } else {
      xml_error_invalid_inside(error, context);
    }
    break;

  default:
    xml_error_invalid_inside(error, context);
    break;
  }
}


static
void
xml_end_element(GMarkupParseContext *context,
                const gchar         *element_name,
                gpointer             user_data,
                GError             **error)
{
  WorldParser *parser = (WorldParser *)user_data;

  switch (parser->state) {
  case KC_START:
    g_set_error(error,
                G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
                _("Internal error: element '%s' ends when in START mode"),
                element_name);
    break;

  case KC_NAME:
  case KC_HOST:
  case KC_PORT:
  case KC_CHARSET:
  case KC_PROXY:
  case KC_AUTO_LOGON:
  case KC_DISPLAY:
  case KC_SCROLL:
  case KC_FLOOD_PREVENTION:
  case KC_REPEAT_COMMANDS:
  case KC_COMMAND_ECHO:
  case KC_NEVER_HIDE_INPUT:
  case KC_STORE_COMMANDS:
  case KC_COMMAND_HISTORY:
  case KC_AUTO_COMPLETION:
  case KC_COMMAND_SEPARATOR:
  case KC_PERL_CHARACTER:
  case KC_INPUT_LINES:
  case KC_INPUT_SPELL:
  case KC_CONFIRM_DELETE:
  case KC_NAMEDISPLAY:
  case KC_LOG:
  case KC_PLUGIN_ITEMS:
  case KC_SCRIPTFILE:
  case KC_PLUGINS:
  case KC_TRIGGERS:
  case KC_ALIASES:
  case KC_MACROS:
  case KC_TIMERS:
  case KC_VARIABLES:
  case KC_HOOKSV2:
  case KC_PROTOCOLS:
  case KC_KEEP_ALIVE:
    parser->state = KC_KCWORLD;
    break;

  case KC_CHARACTER:
  case KC_PASSWORD:
    parser->state = KC_AUTO_LOGON;
    break;

  case KC_TERMINALFONT:
  case KC_ENTRYFONT:
  case KC_USEBOLD:
  case KC_WRAP:
  case KC_TOOLTIPS:
  case KC_STATUSFONT:
  case KC_STATUSBAR:
  case KC_DEFFORECOLOR:
  case KC_DEFBACKCOLOR:
  case KC_DEFBOLDCOLOR:
  case KC_DEFBOLDBACKCOLOR:
  case KC_ANSICOLOR:
  case KC_BACKGROUND:
    parser->state = KC_DISPLAY;
    break;

  case KC_WRAP_COLUMN:
    parser->state = KC_WRAP;
    break;

  case KC_LOGFILE:
  case KC_LOGTIMEFORMAT:
    parser->state = KC_LOG;
    break;

  case KC_PLUGIN:
    if (!parser->forWorldSelector) {
      parser->world->startup_plugin_files
        = g_slist_append(parser->world->startup_plugin_files,
                         parser->currPluginFile);
      parser->currPluginFile = NULL;
    }
    parser->state = KC_PLUGINS;
    break;

  case KC_TRIGGER:
    if (!parser->forWorldSelector) {
      parser->world->triggers = g_slist_append(parser->world->triggers,
                                               parser->currTrigger);
      parser->currTrigger = NULL;
      ++parser->world->trigger_pos;
    }
    parser->state = KC_TRIGGERS;
    break;

  case KC_ALIAS:
    if (!parser->forWorldSelector) {
      parser->world->aliases = g_slist_append(parser->world->aliases,
                                              parser->currAlias);
      parser->currAlias = NULL;
      ++parser->world->alias_pos;
    }
    parser->state = KC_ALIASES;
    break;

  case KC_MACRO:
    if (!parser->forWorldSelector) {
      parser->world->macros = g_slist_append(parser->world->macros,
                                             parser->currMacro);
      parser->currMacro = NULL;
      ++parser->world->macro_pos;
    }
    parser->state = KC_MACROS;
    break;

  case KC_TIMER:
    if (!parser->forWorldSelector) {
      parser->world->timers = g_slist_append(parser->world->timers,
                                             parser->currTimer);
      parser->currTimer = NULL;
      ++parser->world->timer_pos;
    }
    parser->state = KC_TIMERS;
    break;

  case KC_VARIABLE:
    if (!parser->forWorldSelector) {
      parser->world->permanent_variables =
        g_slist_append(parser->world->permanent_variables,
                       parser->currVar);
      parser->currVar = NULL;
    }
    parser->state = KC_VARIABLES;
    break;

  case KC_HOOKV2:
    if (!parser->forWorldSelector) {
      connect_hook(parser->world,
                   parser->currHookTrigger,
                   -1,
                   parser->currHookAction,
                   parser->currHookName,
                   parser->currHookEnabled);
      parser->currHookName = NULL;
    }
    parser->state = parser->previous_state;
    break;

  case KC_HOOKLISTV2:
    if (!parser->forWorldSelector) {
      g_free(parser->currHookTrigger);
    }
    parser->state = KC_HOOKSV2;
    break;

  case KC_PATTERN:
  case KC_ACTION:
    parser->state = parser->previous_state;
    break;

  case KC_HIGHLIGHT:
    parser->state = KC_TRIGGER;
    break;

  case KC_SUBSTITUTION:
    parser->state = KC_ALIAS;
    break;

  case KC_KEY:
    parser->state = KC_MACRO;
    break;

  case KC_MCCP:
    parser->state = KC_PROTOCOLS;
    break;

  case KC_KCWORLD:
    parser->state = KC_START;
    break;
  }
}


static
void
xml_text(GMarkupParseContext *context,
         const gchar         *text,
         gsize                text_len,
         gpointer             user_data,
         GError             **error)
{
  WorldParser *parser = (WorldParser *)user_data;
  gchar       *tmp;

  if (parser->forWorldSelector
      && parser->state != KC_NAME
      && parser->state != KC_CHARACTER
      && parser->state != KC_PASSWORD) {
    return;
  }

  switch (parser->state) {
  case KC_NAME:
  case KC_HOST:
  case KC_PORT:
  case KC_CHARSET:
  case KC_TERMINALFONT:
  case KC_ENTRYFONT:
  case KC_STATUSFONT:
  case KC_FLOOD_PREVENTION:
  case KC_LOGFILE:
  case KC_LOGTIMEFORMAT:
  case KC_SCRIPTFILE:
  case KC_PLUGIN:
  case KC_PATTERN:
  case KC_ACTION:
  case KC_SUBSTITUTION:
  case KC_TIMER:
  case KC_VARIABLE:
  case KC_HOOKV2:
    copy_text(parser->currText, text, text_len);
    break;

  case KC_CHARACTER:
    /* When loading to get the names to compare against passed
       command-line options, characters need not be loaded. */
    if (parser->world->logon_characters) {
      copy_text(&tmp, text, text_len);
      gtk_list_store_append(GTK_LIST_STORE(parser->world->logon_characters),
                            &parser->curr_logon_iter);
      gtk_list_store_set(GTK_LIST_STORE(parser->world->logon_characters),
                         &parser->curr_logon_iter,
                         LOGON_CHAR, tmp,
                         -1);
      g_free(tmp);
    }
    break;

  case KC_PASSWORD:
    if (parser->world->logon_characters) {
      copy_text(&tmp, text, text_len);
      gtk_list_store_set(GTK_LIST_STORE(parser->world->logon_characters),
                         &parser->curr_logon_iter,
                         LOGON_PASS, tmp,
                         -1);
      g_free(tmp);
    }
    break;

  case KC_USEBOLD:
    parser->world->usebold = text[0] - '0';
    break;

  case KC_DEFFORECOLOR:
  case KC_DEFBACKCOLOR:
  case KC_DEFBOLDCOLOR:
  case KC_DEFBOLDBACKCOLOR:
  case KC_ANSICOLOR:
    read_color(parser->currColor, text, text_len, error);
    break;

  case KC_KEY: {
    guint            keyval;
    GdkModifierType  modifiers;
    gchar           *key;

    copy_text(&key, text, text_len);
    gtk_accelerator_parse(key, &keyval, &modifiers);
    if (keyval) {
      parser->currMacro->keyval    = keyval;
      parser->currMacro->modifiers = modifiers;
    } else {
      g_set_error(error,
                  G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT,
                  _("Invalid key code '%s'"),
                  key);
    }
    g_free(key);
    break;
  }

  default:
    /* Lets ignore text here */
    break;
  }
}


static void
xml_error_invalid_inside(GError **error, GMarkupParseContext *context)
{
  g_set_error(error,
              G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT,
              _("Element '%s' not allowed inside element '%s'"),
              (const gchar *) g_markup_parse_context_get_element_stack(context)->data,
              (const gchar *) g_markup_parse_context_get_element_stack(context)->next->data);
}


static void
xml_error_unknown_attribute(GError              **error,
                            const gchar          *attribute_name,
                            GMarkupParseContext  *context)
{
  g_set_error(error,
              G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ELEMENT,
              _("Invalid attribute '%s' for element '%s'"),
              attribute_name, g_markup_parse_context_get_element(context));
}

static
void
copy_text(char **dest, const char *orig, gsize len)
{
  *dest = g_malloc(len + 1);
  memcpy(*dest, orig, len);
  (*dest)[len] = '\0';
}


void
save_world_to_file(World *world)
{
  int          i;
  GtkTreeIter  iter;
  GString     *file_contents;
  GError      *error = NULL;

  if (!world->file) {
    fprintf(stderr, "Error: null filename\n");
    return;
  }

  file_contents = g_string_sized_new(4096);

  /* Header */
  g_string_append(file_contents,
                  "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                  "<!DOCTYPE kcworld SYSTEM \"kcworld.dtd\">\n\n"
                  "<kcworld>\n");

  /* General data */
  if (world != default_world) {
    kc_g_string_append_escaped(file_contents,
                               "  <name>%s</name>\n"
                               "  <host tls=\"%d\">%s</host>\n"
                               "  <port>%s</port>\n",
                               world->name,
                               world->use_tls,
                               world->host,
                               world->port);
  }
  kc_g_string_append_escaped(file_contents,
                             "  <charset>%s</charset>\n", world->charset);

  /* Proxy */
  g_string_append_printf(file_contents,
                         "  <proxy type=\"%d\" ", world->proxy.type);
  if (world->proxy.server && strcmp(world->proxy.server, "") != 0) {
    kc_g_string_append_escaped(file_contents,
                               "server=\"%s\" ", world->proxy.server);
  }
  if (world->proxy.port && strcmp(world->proxy.port, "") != 0) {
    kc_g_string_append_escaped(file_contents,
                               "port=\"%s\" ", world->proxy.port);
  }
  if (world->proxy.user && strcmp(world->proxy.user, "") != 0) {
    kc_g_string_append_escaped(file_contents,
                               "user=\"%s\" ", world->proxy.user);
  }
  if (world->proxy.password && strcmp(world->proxy.password, "") != 0) {
    kc_g_string_append_escaped(file_contents,
                               "password=\"%s\" ", world->proxy.password);
  }
  g_string_append(file_contents, "/>\n");

  /* Auto-logon */
  if (world != default_world) {
    g_string_append_printf(file_contents,
                           "  <auto-logon style=\"%d\">\n",
                           world->connection_style);
    if (gtk_tree_model_get_iter_first(world->logon_characters, &iter)) {
      do {
        gchar *character;
        gchar *password;

        gtk_tree_model_get(world->logon_characters, &iter,
                           LOGON_CHAR, &character,
                           LOGON_PASS,  &password,
                           -1);
        kc_g_string_append_escaped(file_contents,
                                   "    <character>%s</character>\n",
                                   character);
        kc_g_string_append_escaped(file_contents,
                                   "    <password>%s</password>\n",
                                   password);
        g_free(character);
        g_free(password);
      } while (gtk_tree_model_iter_next(world->logon_characters, &iter));
    }
    g_string_append(file_contents, "  </auto-logon>\n");
  }

  /* Fonts, Colors, Status Bar and background */
  g_string_append(file_contents, "  <display>\n");
  kc_g_string_append_escaped(file_contents,
                             "    <terminalfont>%s</terminalfont>\n"
                             "    <entryfont>%s</entryfont>\n"
                             "    <usebold>%d</usebold>\n"
                             "    <wrap enabled=\"%d\" indent=\"%d\">\n"
                             "      <wrap-column enabled=\"%d\" column=\"%d\"/>\n"
                             "    </wrap>\n"
                             "    <tooltips enabled=\"%d\"/>\n"
                             "    <statusfont>%s</statusfont>\n"
                             "    <statusbar ctime=\"%d\" itime=\"%d\" itime-behavior=\"%d\"/>\n",
                             world->terminalfont,
                             world->entryfont,
                             world->usebold,
                             world->wrap, world->wrap_indent,
                             world->use_wrap_column, world->wrap_column,
                             world->use_tooltips,
                             world->statusfont,
                             world->ctime_type,
                             world->itime_type,
                             world->itime_reset_activate);

  write_color(file_contents, "defforecolor",     0, world->deffore);
  write_color(file_contents, "defbackcolor",     0, world->defback);
  write_color(file_contents, "defboldcolor",     0, world->defbold);
  write_color(file_contents, "defboldbackcolor", 0, world->defboldback);
  for (i = 0; i < 16; ++i) {
    write_color(file_contents, "ansicolor", i, world->ansicolors);
  }

  g_string_append(file_contents, "  </display>\n");

  /* Command entry */
  kc_g_string_append_escaped(file_contents,
                             "  <repeat-commands enabled=\"%d\" />\n"
                             "  <command-echo enabled=\"%d\" />\n"
                             "  <never-hide-input enabled=\"%d\" />\n"
                             "  <store-commands enabled=\"%d\" />\n"
                             "  <command-history size=\"%d\" ignore-up-down-keys=\"%d\" />\n"
                             "  <auto-completion enabled=\"%d\" minprefix=\"%d\" />\n"
                             "  <command-separator separator=\"%s\" />\n"
                             "  <perl-character char=\"%c\" />\n"
                             "  <input-lines size=\"%d\" save=\"%d\" />\n",
                             world->repeat_commands,
                             world->cmd_echo,
                             world->never_hide_input,
                             world->store_commands,
                             world->commands_to_save,
                             world->ignore_up_down_keys,
                             world->autocompletion,
                             world->autocompl_minprefix,
                             world->command_separator,
                             world->perl_character,
                             world->input_n_lines,
                             world->input_n_lines_saved);
  g_string_append_printf(file_contents,
                         "  <input-spell active=\"%d\"", world->spell);
  if (world->spell_language) {
    kc_g_string_append_escaped(file_contents,
                               " language=\"%s\"", world->spell_language);
  }
  g_string_append(file_contents, " />\n");

  /* Anti flood */
  g_string_append_printf(file_contents,
                         "  <flood-prevention enabled=\"%d\" max-repeat=\"%d\">",
                         world->flood_prevention,
                         world->max_equal_commands);
  kc_g_string_append_escaped(file_contents,
                             "%s</flood-prevention>\n",
                             world->flood_prevention_command);

  /* Scrolling */
  g_string_append_printf(file_contents,
                         "  <scroll on-output=\"%d\" lines=\"%ld\" />\n",
                         world->scrollOutput, world->buffer_lines);

  /* Name display */
  g_string_append_printf(file_contents,
                         "  <name-display style=\"%d\" pattern=\"%s\" />\n",
                         world->name_display_style,
                         world->name_display_pattern);

  /* Logging */
  g_string_append_printf(file_contents,
                         "  <log autostart=\"%d\" addtime=\"%d\">\n",
                         world->log_autostart, world->log_add_time);
  if (world->log_file_name) {
    kc_g_string_append_escaped(file_contents,
                               "    <logfile>%s</logfile>\n",
                               world->log_file_name);
  }
  if (world->log_timeformat) {
    kc_g_string_append_escaped(file_contents,
                               "    <logtimeformat>%s</logtimeformat>\n",
                               world->log_timeformat);
  }
  g_string_append(file_contents, "  </log>\n");

  /* Delete confirmation in GUI Editors */
  g_string_append_printf(file_contents,
                         "  <confirm-delete confirm=\"%d\" />\n",
                         world->confirm_delete);

  /* Plugin items in GUI Editors */
  g_string_append_printf(file_contents,
                         "  <plugin-items show=\"%d\" />\n",
                         world->show_plugin_items);

  /* Script file */
  if (world->scriptfile && strcmp(world->scriptfile, "") != 0) {
    kc_g_string_append_escaped(file_contents,
                               "  <scriptfile>%s</scriptfile>\n",
                               world->scriptfile);
  }

  /* Startup plugins */
  if (world != default_world
      && gtk_tree_model_iter_n_children(world->startup_plugins, NULL)) {
    GtkTreeIter  iter;
    gchar       *file;

    g_string_append(file_contents, "  <plugins>\n");
    gtk_tree_model_get_iter_first(world->startup_plugins, &iter);
    do {
      gtk_tree_model_get(world->startup_plugins, &iter,
                         SPLUGIN_FILE, &file, -1);
      kc_g_string_append_escaped(file_contents,
                                 "    <plugin>%s</plugin>\n", file);
      g_free(file);
    } while (gtk_tree_model_iter_next(world->startup_plugins, &iter));
    g_string_append(file_contents, "  </plugins>\n");
  }

  /* Triggers */
  if (world->triggers) {
    GSList  *trigptr = world->triggers;
    Trigger *trigger;

    g_string_append(file_contents, "  <triggers>\n");

    while (trigptr) {
      trigger = (Trigger *) trigptr->data;
      trigptr = trigptr->next;

      if (trigger->owner_plugin) {
        continue;
      }

      save_trigger(file_contents, trigger);
    }

    g_string_append(file_contents, "  </triggers>\n");
  }

  /* Aliases */
  if (world->aliases) {
    GSList *aliasptr = world->aliases;
    Alias  *alias;

    g_string_append(file_contents, "  <aliases>\n");

    while (aliasptr) {
      alias = (Alias *) aliasptr->data;
      aliasptr = aliasptr->next;

      if (alias->owner_plugin) {
        continue;
      }

      save_alias(file_contents, alias);
    }

    g_string_append(file_contents, "  </aliases>\n");
  }

  /* Macros */
  if (world->macros) {
    GSList *macroptr = world->macros;
    Macro  *macro;

    g_string_append(file_contents, "  <macros>\n");

    while (macroptr) {
      macro = (Macro *) macroptr->data;
      macroptr = macroptr->next;

      if (macro->owner_plugin) {
        continue;
      }

      save_macro(file_contents, macro);
    }
    g_string_append(file_contents, "  </macros>\n");
  }

  /* Timers */
  if (world->timers) {
    GSList *timerptr = world->timers;
    Timer  *timer;

    g_string_append(file_contents, "  <timers>\n");

    while (timerptr) {
      timer = (Timer *) timerptr->data;
      timerptr = timerptr->next;

      if (timer->owner_plugin) {
        continue;
      }

      save_timer(file_contents, timer);
    }

    g_string_append(file_contents, "  </timers>\n");
  }

  /* Permanent variables */
  if (world->permanent_variables) {
    GSList *varptr = world->permanent_variables;

    g_string_append(file_contents, "  <variables>\n");

    while (varptr) {
      g_string_append_printf(file_contents, "    <variable>%s</variable>\n",
              (char *) varptr->data);
      varptr = varptr->next;
    }

    g_string_append(file_contents, "  </variables>\n");
  }

  /* Hooks */
  save_hooks(file_contents, world);

  /* Protocols */
  g_string_append(file_contents, "  <protocols>\n");
  g_string_append_printf(file_contents,
                         "    <mccp behavior=\"%d\"/>\n", world->mccp_behavior);
  g_string_append(file_contents, "  </protocols>\n");

  /* Keepalive */
  g_string_append_printf(file_contents,
                         "  <keep-alive enabled=\"%d\"/>\n", world->keep_alive);

  g_string_append(file_contents, "</kcworld>\n");

  create_kildclient_directory();
  if (!g_file_set_contents(world->file,
                           file_contents->str, file_contents->len,
                           &error)) {
    kc_error_dialog(GTK_WINDOW(wndMain),
                    _("Could not open file '%s': %s"),
                    world->file, error->message);
    g_error_free(error);
  }
  g_string_free(file_contents, TRUE);
}


void
load_command_history(World *world)
{
  FILE         *fp;
  gchar        *path;
  gchar         command[MAX_BUFFER];
  GtkListStore *liststore;
  GtkTreeIter   iter;

  if (!world->store_commands)
    return;

  if (!world->file) {
    return;
  }

  path = g_strdup_printf("%s.hst", world->file);

  fp = fopen(path, "r");
  g_free(path);
  if (!fp) {
    return;
  }

  liststore = GTK_LIST_STORE(world->cmd_list);

  while (fgets(command, MAX_BUFFER, fp)) {
    command[strlen(command) - 1] = '\0'; /* Strip final newline */

    gtk_list_store_append(liststore, &iter);
    gtk_list_store_set(liststore, &iter,
                       CMDLIST_COL_COMMAND, command,
                       CMDLIST_COL_TENTATIVE, NULL,
                       -1);
    ++world->cmd_list_size;
  }

  fclose(fp);
}


void
save_command_history(World *world)
{
  gchar       *path;
  GtkTreeIter  iter;
  gchar       *command;
  int          start_pos;
  GString     *file_contents;

  if (!world->store_commands)
    return;

  if (!world->file) {
    fprintf(stderr, "Error: null filename\n");
    return;
  }

  file_contents = g_string_sized_new(1024);

  /* Define start position. Most of the times saving starts at the
     second (pos 1) command because the first represents the command
     currently being entered, unless under some circunstances when
     using the Repeat Commands feature, in this case there is not this
     "being typed" command. */
  if (!world->repeat_commands || world->cmd_has_been_edited) {
    start_pos = 1;
  } else {
    start_pos = 0;
  }
  if (gtk_tree_model_iter_nth_child(world->cmd_list, &iter,
                                    NULL, start_pos)) {
    do {
      command = cmdhistory_get_command(world->cmd_list, &iter);
      g_string_append(file_contents, command);
      g_string_append_c(file_contents, '\n');
      g_free(command);
    } while (gtk_tree_model_iter_next(world->cmd_list, &iter));
  }

  path = g_strdup_printf("%s.hst", world->file);
  g_file_set_contents(path, file_contents->str, file_contents->len, NULL);
  g_free(path);
  g_string_free(file_contents, TRUE);
}


void
kc_g_string_append_escaped(GString *gstr, const char *template, ...)
{
  va_list  ap;
  char    *str;

  va_start(ap, template);
  str = g_markup_vprintf_escaped(template, ap);
  g_string_append(gstr, str);
  g_free(str);
}


static
void
read_color(GdkRGBA *color, const char *text, gsize text_len, GError **error)
{
  gchar    *colordesc;
  int       idx;
  guint16   red, green, blue;

  copy_text(&colordesc, text, text_len);

  if (sscanf(colordesc, "%d %hu %hu %hu", &idx, &red, &green, &blue) != 4) {
    g_set_error(error,
                G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
                "Invalid color specification in config file.");
  }

  color[idx].alpha = 1.0;
  color[idx].red   = red   / 65535.0;
  color[idx].green = green / 65535.0;
  color[idx].blue  = blue  / 65535.0;

  g_free(colordesc);
}


static
void
write_color(GString *str, char *name, int idx, GdkRGBA *color)
{
  g_string_append_printf(str, "    <%s>%d %d %d %d</%s>\n", name, idx,
                         (int) (color[idx].red   * 65535),
                         (int) (color[idx].green * 65535),
                         (int) (color[idx].blue  * 65535),
                         name);
}


void
mark_as_offline(World *world)
{
  gchar *str;

  str = g_strdup_printf("<span foreground=\"grey\">%s</span>",
                        world->display_name);
  gtk_label_set_markup(world->gui->lblNotebook, str);
  g_free(str);

  str = g_strdup_printf(_("Disconnected from world %s"), world->name);
  gtk_label_set_text(world->gui->lblStatus, str);
  g_free(str);

  currentWorld = world;
}


void
prepare_display_name(World *world)
{
  gchar *character;

  g_free(world->display_name);
  g_free(world->new_text_name);

  if (world->character_used == NULL || world->connection_style == NONE) {
    character = g_strdup("");
  } else {
    GtkTreePath *path;
    GtkTreeIter  iter;

    path = gtk_tree_row_reference_get_path(world->character_used);
    if (!path) {
      gtk_tree_row_reference_free(world->character_used);
      world->character_used = NULL;
      character = g_strdup("");
    }

    gtk_tree_model_get_iter(world->logon_characters,
                            &iter, path);
    gtk_tree_model_get(world->logon_characters, &iter,
                       LOGON_CHAR, &character,
                       -1);
  }

  switch (world->name_display_style) {
  case NAME_DISPLAY_WORLD:
    world->display_name = g_strdup(world->name);
    break;

  case NAME_DISPLAY_WORLD_CHAR:
    world->display_name = g_strdup_printf("%s - %s", world->name, character);
    break;

  case NAME_DISPLAY_CHAR_WORLD:
    world->display_name = g_strdup_printf("%s - %s", character, world->name);
    break;

  case NAME_DISPLAY_CUSTOM:
    world->display_name = replace_kc_escapes(world->name_display_pattern,
                                             world->name, character);
    break;
  }
  world->new_text_name = g_strdup_printf("<span foreground=\"red\">%s</span>",
                                         world->display_name);


  g_free(character);
}


gchar *
replace_kc_escapes(const gchar *pattern,
                   const gchar *name,
                   const gchar *character)
{
  static GRegex *reKw = NULL;
  static GRegex *reKc = NULL;
  gchar         *tmp1;
  gchar         *tmp2;

  /* If this is the first run, create the GRegex structure */
  if (!reKw) {
    reKw = g_regex_new("%Kw", 0, 0, NULL);
  }
  if (!reKc) {
    reKc = g_regex_new("%Kc", 0, 0, NULL);
  }

  /* Substitute KC specific strings:
     %Kw - World name
     %Kc - Character name */
  tmp1 = g_regex_replace_literal(reKw,
                                 pattern, -1,
                                 0,
                                 name,
                                 0, NULL);
  tmp2 = g_regex_replace_literal(reKc,
                                 tmp1, -1,
                                 0,
                                 character,
                                 0, NULL);

  g_free(tmp1);
  return tmp2;
}

