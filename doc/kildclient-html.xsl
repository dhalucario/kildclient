<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:import
    href="/usr/share/xml/docbook/stylesheet/docbook-xsl-ns/xhtml5/chunkfast.xsl"/>

<xsl:param name="html.ext">.xhtml</xsl:param>
<xsl:param name="use.id.as.filename" select="1"></xsl:param>
<xsl:param name="chunker.output.encoding">UTF-8</xsl:param>
<xsl:param name="chunk.quietly" select="1"></xsl:param>

<xsl:param name="html.stylesheet">kildclient.css</xsl:param>

<xsl:param name="chunk.first.sections" select="1"></xsl:param>

<xsl:param name="section.autolabel" select="1"/>
<xsl:param name="section.label.includes.component.label" select="1"/>
<xsl:param name="autotoc.label.in.hyperlink" select="0"></xsl:param>
<xsl:param name="toc.list.type">dl</xsl:param>

<xsl:param name="funcsynopsis.style">ansi</xsl:param>
<xsl:param name="funcsynopsis.decoration" select="0"></xsl:param>
<xsl:param name="abstract.notitle.enabled" select="1"></xsl:param>

</xsl:stylesheet>
