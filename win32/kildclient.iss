; Inno Setup script to generate KildClient installer.
; When generating the installer for a new version, the following must be
; changed/verified:
;   - Version number in #define section
;   - New files included in the package
;   - Make sure to run strip on the .exe before compiling!
;   - Generate html docs
;   - Make in po/ directory

;;; Some defines
#define AppName "KildClient"
#define AppId   "KildClient"
#define AppVer  "3.2.1b"
#define URL     "https://www.kildclient.org"

;;; Paths to the files to be included
; Directory where KildClient is built
#define BuildDir   "Z:\work"
; Root of mingw32 files installed as Fedora files
#define MingwDir   "Z:\mingw32\usr\i686-w64-mingw32\sys-root\mingw"
; Strawberry Perl directory
#define PerlDir    "Z:\mingw32\opt\strawberryperl\perl"
; Directory of Perl modules (not in the mingw root, used for JSON.pm)
#define SysPerlDir "Z:\mingw32\usr\share\perl5\vendor_perl"

[Setup]
AppId={#AppId}
AppName={#AppName}
AppVersion={#AppVer}
AppPublisher=Eduardo M Kalinowski
AppPublisherURL={#URL}
AppSupportURL={#URL}
AppUpdatesURL={#URL}
DefaultDirName={autopf}\KildClient
DefaultGroupName=KildClient
AllowNoIcons=true
LicenseFile={#BuildDir}\COPYING
OutputDir={#BuildDir}\win32
OutputBaseFilename=kildclient-{#AppVer}
Compression=lzma
SolidCompression=true
AppCopyright=This program is released under the GNU General Public License
ShowLanguageDialog=yes
RestartIfNeededByRun=yes

[Languages]
Name: eng; MessagesFile: compiler:Default.isl
Name: bra; MessagesFile: compiler:Languages\BrazilianPortuguese.isl

[Tasks]
Name: desktopicon; Description: {cm:CreateDesktopIcon}; GroupDescription: {cm:AdditionalIcons}; Flags: unchecked

[Files]
;;; Program and support files
Source: {#BuildDir}\src\kildclient.exe; DestDir: {app}; Flags: ignoreversion
Source: {#BuildDir}\src\*.ui; DestDir: {app}; Flags: ignoreversion
Source: {#BuildDir}\share\kildclient.pl; DestDir: {app}; Flags: ignoreversion
Source: {#BuildDir}\share\MSDP.pm; DestDir: {app}; Flags: ignoreversion
Source: {#BuildDir}\share\kildclient.png; DestDir: {app}; Flags: ignoreversion


;;; Plugins
Source: {#BuildDir}\share\plugins\*.pl; DestDir: {app}\plugins; Flags: ignoreversion

;;; Help files
Source: {#BuildDir}\NEWS; DestDir: {app}; Flags: ignoreversion
Source: {#BuildDir}\COPYING; DestDir: {app}; Flags: ignoreversion
Source: {#BuildDir}\share\kildclient.hlp; DestDir: {app}; Flags: ignoreversion

;;; The manual
Source: {#BuildDir}\doc\C\kildclient\*.xhtml; DestDir: {app}\manual\html; Flags: ignoreversion
Source: {#BuildDir}\doc\C\kildclient\*.css; DestDir: {app}\manual\html; Flags: ignoreversion
Source: {#BuildDir}\doc\C\images\*.png; DestDir: {app}\manual\html\images; Flags: ignoreversion

;;; Message catalogs
Source: {#BuildDir}\po\pt_BR.gmo; DestDir: {app}\locale\pt_BR\LC_MESSAGES; DestName: kildclient.mo; Tasks: ; Languages: ; Flags: ignoreversion
Source: {#BuildDir}\po\de.gmo; DestDir: {app}\locale\de\LC_MESSAGES; DestName: kildclient.mo; Tasks: ; Languages: ; Flags: ignoreversion
Source: {#BuildDir}\po\es.gmo; DestDir: {app}\locale\es\LC_MESSAGES; DestName: kildclient.mo; Tasks: ; Languages: ; Flags: ignoreversion
Source: {#BuildDir}\po\eo.gmo; DestDir: {app}\locale\eo\LC_MESSAGES; DestName: kildclient.mo; Tasks: ; Languages: ; Flags: ignoreversion
Source: {#BuildDir}\po\sv.gmo; DestDir: {app}\locale\sv\LC_MESSAGES; DestName: kildclient.mo; Tasks: ; Languages: ; Flags: ignoreversion

;;; Gtk+
Source: {#MingwDir}\bin\iconv.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libatk-1.0-0.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libbz2-1.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libcairo-2.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libcairo-gobject-2.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libepoxy-0.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libexpat-1.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libffi-8.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libfontconfig-1.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libfreetype-6.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libfribidi-0.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libgdk-3-0.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libgdk_pixbuf-2.0-0.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libgio-2.0-0.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libglib-2.0-0.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libgmodule-2.0-0.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libgobject-2.0-0.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libgnurx-0.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libgtk-3-0.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libharfbuzz-0.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libintl-8.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libjpeg-62.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libpango-1.0-0.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libpangocairo-1.0-0.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libpangoft2-1.0-0.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libpangowin32-1.0-0.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libpcre2-8-0.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libpng16-16.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libpixman-1-0.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libssp-0.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libtiff-5.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\zlib1.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\share\glib-2.0\schemas\*; DestDir: {app}\share\glib-2.0\schemas; Flags: ignoreversion
Source: {#MingwDir}\share\locale\pt_BR\LC_MESSAGES\*; DestDir: {app}\locale\pt_BR\LC_MESSAGES; Flags: ignoreversion
Source: {#MingwDir}\share\locale\de\LC_MESSAGES\*; DestDir: {app}\locale\de\LC_MESSAGES; Flags: ignoreversion
Source: {#MingwDir}\share\locale\es\LC_MESSAGES\*; DestDir: {app}\locale\es\LC_MESSAGES; Flags: ignoreversion
Source: {#MingwDir}\share\locale\eo\LC_MESSAGES\*; DestDir: {app}\locale\eo\LC_MESSAGES; Flags: ignoreversion
Source: {#MingwDir}\share\locale\sv\LC_MESSAGES\*; DestDir: {app}\locale\sv\LC_MESSAGES; Flags: ignoreversion
Source: {#MingwDir}\share\icons\Adwaita\index.theme; DestDir: {app}\share\icons\Adwaita\; Flags: ignoreversion
Source: {#MingwDir}\share\icons\Adwaita\16x16\actions\*; DestDir: {app}\share\icons\Adwaita\16x16\actions; Flags: ignoreversion
Source: {#MingwDir}\share\icons\Adwaita\16x16\devices\*; DestDir: {app}\share\icons\Adwaita\16x16\devices; Flags: ignoreversion
Source: {#MingwDir}\share\icons\Adwaita\16x16\mimetypes\*; DestDir: {app}\share\icons\Adwaita\16x16\devices; Flags: ignoreversion
Source: {#MingwDir}\share\icons\Adwaita\16x16\places\*; DestDir: {app}\share\icons\Adwaita\16x16\places; Flags: ignoreversion

;;; gnutls
Source: {#MingwDir}\bin\libgmp-10.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libgnutls-30.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libhogweed-6.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libnettle-8.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libp11-kit-0.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libtasn1-6.dll; DestDir: {app}\bin; Flags: ignoreversion

;;; gtk-spell
Source: {#MingwDir}\bin\libenchant-2.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libgcc_s_dw2-1.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libgtkspell3-3-0.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libhunspell-1.7-0.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libstdc++-6.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\bin\libwinpthread-1.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#MingwDir}\lib\enchant-2\enchant_hunspell.dll; DestDir: {app}\lib\enchant-2; Flags: ignoreversion

;;; Perl
Source: {#PerlDir}\bin\perl532.dll; DestDir: {app}\bin; Flags: ignoreversion
Source: {#PerlDir}\lib\*; DestDir: {app}\perl; Flags: ignoreversion recursesubdirs
Source: {#SysPerlDir}\JSON.pm; DestDir: {app}\lib; Flags: ignoreversion

[INI]
Filename: {app}\kildclient.url; Section: InternetShortcut; Key: URL; String: https://www.kildclient.org
Filename: {app}\manual.url; Section: InternetShortcut; Key: URL; String: {app}\manual\html\index.xhtml

[Icons]
Name: {group}\KildClient; Filename: {app}\kildclient.exe
Name: {group}\{cm:Manual}; Filename: {app}\manual.url
Name: {group}\{cm:ProgramOnTheWeb,KildClient}; Filename: {app}\kildclient.url
Name: {group}\{cm:UninstallProgram,KildClient}; Filename: {uninstallexe}
Name: {autodesktop}\KildClient; Filename: {app}\kildclient.exe; Tasks: desktopicon

[Registry]
Root: HKLM; Subkey: "SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\kildclient.exe"; ValueType: string; ValueName: ""; ValueData: "{app}\kildclient.exe"; Flags: uninsdeletekey
Root: HKLM; Subkey: "SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\kildclient.exe"; ValueType: string; ValueName: "Path"; ValueData: "{app};{app}\bin"; Flags: uninsdeletekey

[UninstallDelete]
Type: files; Name: {app}\kildclient.url
Type: files; Name: {app}\manual.url

[CustomMessages]
eng.Manual=Manual
bra.Manual=Manual

[Code]
procedure CurStepChanged(CurStep: TSetupStep);
var
  UninstallPath: String;
  UninstallString: String;
  ResultCode: Integer;
begin
  { Uninstall a previous version (if it is found) }
  if (CurStep = ssInstall) then
  begin
    UninstallPath   := 'Software\Microsoft\Windows\CurrentVersion\Uninstall\{#AppId}_is1';
    UninstallString := '';
    if RegQueryStringValue(HKLM, UninstallPath, 'UninstallString', UninstallString) then
    begin
      if UninstallString <> '' then
      begin
        UninstallString := RemoveQuotes(UninstallString);
        Exec(UninstallString, '/SILENT /NORESTART /SUPPRESSMSGBOXES', '',
             SW_HIDE, ewWaitUntilTerminated, ResultCode);
      end;
    end;
  end;
end;
