#!/usr/bin/perl

# Copyright (C) 2004-2023 Eduardo M Kalinowski <eduardo@kalinowski.com.br>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

use strict;
use warnings;

use XML::LibXML::Reader;
use Text::Wrap;


# Global variables
my $document;
my $xpc;

main();


sub main {
  print <<'EOF';
[index]
Use
   help 'functionname'
to get help on that function. The quotes are necessary.

EOF

  $document = XML::LibXML->load_xml(location => "../doc/C/kildclient.xml",
                                    no_blanks => 1);

  $xpc = XML::LibXML::XPathContext->new();
  $xpc->registerNs('db', 'http://docbook.org/ns/docbook');

  my @functions = $xpc->findnodes('//db:sect1[starts-with(@xml:id, "func_")]',
                                  $document);
  for my $function (@functions) {
    write_function($function);
  }
}


sub write_function {
  my ($root) = @_;

  write_title($root);

  my $description_start = $root->firstChild()->nextSibling();
  write_description($description_start);
}


sub write_title {
  my ($root) = @_;

  my $function = $xpc->findvalue("db:info/db:title/db:function", $root);
  print "[$function]";

  my $shortened = $function;
  $shortened =~ s/.*(::|>)//;
  if ($shortened ne $function) {
    print " [$shortened]";
  }

  print "\n";
}


sub write_description {
  my ($start) = @_;

  print get_texts($start);
}


sub get_texts {
  my ($start) = @_;

  my $i = $start;
  my $result = "";
  while (defined $i) {
    my $text = get_text($i);
    $result .= $text;

    $i = $i->nextNonBlankSibling();
  }

  return $result;
}


sub get_text {
  my ($node) = @_;

  my $nodename = $node->nodeName();
  if    ($nodename eq "para")           { return n_paragraph($node); }
  elsif ($nodename eq "abstract")       { return n_abstract($node); }
  elsif ($nodename eq "funcsynopsis")   { return n_funcsynopsis($node); }
  elsif ($nodename eq "itemizedlist")   { return n_list($node); }
  elsif ($nodename eq "screen")         { return n_code($node); }
  elsif ($nodename eq "programlisting") { return n_code($node); }
  elsif ($nodename eq "example")        { return n_example($node); }
  else  {
    return "Unknown element " . $node->nodeName() . "\n\n";
  }
}


sub n_paragraph {
  my ($node) = @_;

  for my $xref ($xpc->findnodes('db:xref', $node)) {
    my $new_text = XML::LibXML::Text->new(
                    get_xref_text($xref->getAttribute('linkend')));
    $xref->replaceNode($new_text);
  }

  for my $menuchoice ($xpc->findnodes('db:menuchoice', $node)) {
    my $guimenu = $menuchoice->firstChild();
    my $guimenuitem = $guimenu->nextSibling();
    my $new_text = XML::LibXML::Text->new($guimenu->textContent() . '->'
                                          . $guimenuitem->textContent());
    $menuchoice->replaceNode($new_text);
  }

  return wrap_text($node->textContent()) . "\n\n";
}


sub get_xref_text {
  my ($xref) = @_;

  if ($xref =~ /^func_/) {
    $xref =~ s/^func_//;
    $xref =~ s/$/()/;
  } else {
    my $sect = ($xpc->findnodes("//*[\@xml:id = '$xref']", $document))[0];
    my $title = $sect->firstChild->textContent();
    $xref = "section '$title' in the Manual";
  }

  return $xref;
}


sub wrap_text {
  my ($text) = @_;

  $text =~ s#\n\s+# #g;
  return fill("", "", $text)
}


sub n_abstract {
  my ($node) = @_;

  return "&D" . $node->firstChild()->textContent() . "\n\n";
}


sub n_funcsynopsis {
  my ($node) = @_;

  my $result = "";
  my $first = 1;

  foreach my $prototype ($xpc->findnodes('db:funcprototype', $node)) {
    my $funcdef = $prototype->firstChild();

    my $returnType = $funcdef->firstChild();
    if ($returnType->nodeValue() eq "void ") {
      $funcdef->removeChild($returnType);
    }

    $result .= $first ? "&cSyntax: " : "&c        ";
    my $func_call = $funcdef->textContent();
    $func_call =~ s/\n\s+/ /g;
    $result .= $func_call;
    $first = 0;

    my @parameters = $xpc->findnodes("db:paramdef", $prototype);
    $result .= "(" . join(', ', map { get_parameter($_) } @parameters) . ")\n";
  }

  $result .= "\n";
  return $result;
}


sub get_parameter {
  my ($node) = @_;

  my $parameter = $_->textContent();
  $parameter =~ s/\s//g;
  return $parameter;
}


sub n_list {
  my ($node) = @_;

  local $Text::Wrap::columns = $Text::Wrap::columns - 4;
  my $result;

  for my $item ($xpc->findnodes('db:listitem', $node)) {
    my $text = get_texts($item->firstNonBlankChild());
    $text =~ s#\n(.)#\n    $1#g;

    $result .= "  * $text";
  }

  return $result;
}


sub n_code {
  my ($node) = @_;

  my $text = remove_initial_nl($node->textContent());
  return $text . "\n";
}


sub n_example {
  my ($node) = @_;

  my $text = remove_initial_nl($xpc->findvalue('db:programlisting', $node));
  return $text . "\n";
}


sub remove_initial_nl {
  my ($str) = @_;

  $str =~ s/^\n//;
  return $str;
}
