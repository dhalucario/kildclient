package keypad;
#: Version: 1.0
#: Description: Allows use of the keypad for moving
#: Author: Eduardo M Kalinowski

$::world->macro('KP_Home',      'nw',        { name => 'keypad:keypad' });
$::world->macro('KP_Up',        'n',         { name => 'keypad:keypad' });
$::world->macro('KP_Page_Up',   'ne',        { name => 'keypad:keypad' });
$::world->macro('KP_Left',      'w',         { name => 'keypad:keypad' });
$::world->macro('KP_Right',     'e',         { name => 'keypad:keypad' });
$::world->macro('KP_End',       'sw',        { name => 'keypad:keypad' });
$::world->macro('KP_Down',      's',         { name => 'keypad:keypad' });
$::world->macro('KP_Page_Down', 'se',        { name => 'keypad:keypad' });
$::world->macro('KP_Subtract',  'down',      { name => 'keypad:keypad' });
$::world->macro('KP_Add',       'up',        { name => 'keypad:keypad' });

$::world->macro('KP_Begin',     'who',       { name => 'keypad:keypad' });
$::world->macro('KP_Insert',    'look',      { name => 'keypad:keypad' });
$::world->macro('KP_Divide',    'inventory', { name => 'keypad:keypad' });
$::world->macro('KP_Multiply',  'score',     { name => 'keypad:keypad' });

sub help {
  $::world->echonl("Allows use of the keypad for movement (and a few other",
                   "actions). NumLock must be off for this plugin to work.");
}
