package KCWin;
#: Version: 3.0.0
#: Description: Creates general-purpose windows with an input and output area
#: Author: Eduardo M Kalinowski

BEGIN {
  eval "use Gtk3";
  die "This plugin requires the gtk3-perl bindings" if $@;
}


use base 'Gtk3::Window';


sub help {
  $::world->echonl(<<'EOHELP');
    This plugin defines a new type, KCWin, which is a small window
with an area for output and an entry box for input. These windows do
nothing by themselves, but they can be used by other plugins when they
need a window for input and output. The output area supports ANSI
colors and thus is like a mini MUD window.

    Use KCWin->new to create a new window. KCWin derives from Gtk3::Window
so you can use all of its methods.

    The widgets are accessible for customization. If $kcw is a KCWin, the
following widgets are available:

- $kcw->{VBOX}, Gtk3::Box, the vertical box that contains the output and
  input areas.
- $kcw->{SCROLLWIN}, Gtk3::ScrolledWindow, this holds the TextView that
  is used for output.
- $kcw->{TEXTVIEW}, Gtk3::TextView, is the widget used for output.
- $kcw->{TEXTBUFFER}, Gtk3::TextBuffer, for convenience, the TextBuffer
  displayed in the window.
- $kcw->{CMDAREA}, Gtk3::Box, a box that holds a button to clear the
  input area, and the input area itself.
- $kcw->{BTNCLEAR}, Gtk3::Button, a button that clears the input entry
  widget when clicked.
- $kcw->{ENTRY}, Gtk3::Entry, the input entry widget. Connect to the
  activate signal of this widget to do something when the user presses
  ENTER in this widget.

    The widgets can be used, and the window can be customized (by adding
other widgets, for example). Three common actions have functions in
KCWin as a shortcut:

$text = KCWin::get_text
    Returns the text in the entry box.

KCWin::set_text($text)
    Sets the text in the entry box.

KCWin::feed($text)
    Appends text to the output terminal widget. $text can contain ANSI
    color sequences. The colorize function can be useful in conjuntion
    with this function.
EOHELP
}


sub new {
  my $class = shift;

  my ($windowPTR,
      $vboxPTR, $scrollwinPTR, $txtViewPTR, $txtBufferPTR,
      $cmdareaPTR, $btnclearPTR, $entryPTR,
      $guiPTR) = KCWin::_new;

  my $self = bless Glib::Object->new_from_pointer($windowPTR),
                   $class;
  $self->{VBOX}       = Glib::Object->new_from_pointer($vboxPTR);
  $self->{SCROLLWIN}  = Glib::Object->new_from_pointer($scrollwinPTR);
  $self->{TEXTVIEW}   = Glib::Object->new_from_pointer($txtViewPTR);
  $self->{TEXTBUFFER} = Glib::Object->new_from_pointer($txtBufferPTR);
  $self->{CMDAREA}    = Glib::Object->new_from_pointer($cmdareaPTR);
  $self->{BTNCLEAR}   = Glib::Object->new_from_pointer($btnclearPTR);
  $self->{ENTRY}      = Glib::Object->new_from_pointer($entryPTR);
  $self->{_GUIPTR}    = $guiPTR;

  return $self;
}


sub destroy {
  my $self = shift;

  $self->_destroy();
  $self->SUPER::destroy();
}


sub get_text {
  my $self = shift;

  return $self->{ENTRY}->get_text;
}


sub set_text {
  my $self = shift;
  my ($text) = @_;

  return $self->{ENTRY}->set_text($text);
}
