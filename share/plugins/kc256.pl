package kc256;
#: Version: 1.0.0
#: Description: Demonstrates the 256 color mode supported by KildClient
#: Author: Eduardo M Kalinowski

sub help {
  $::world->echonl(<<'EOHELP');
This plugin demonstrates a useful extension to the 16 ansi colors that
KildClient supports. This is the same 256-color extension supported by
xterm (if it is complied with support for that).

With this extension, you can specify any color in a 6x6x6 RGB color
cube, that is, you have 216 rgb colors at your disposal, plus 24
shades of gray. (The remaining 16 colors are the standard ansi
colors.)

There are two functions in this plugin:

kc256::showcolors Displays all 216 available RGB colors and the 24
                  shades of grey in nice tables, with the values that
                  you can use in colorize() to access them.

kc256::rainbowtext($string) Just a demonstration on how (not to) use
                            the colors. Displays $string, one character
                            in a different color.
EOHELP
}


sub showcolors {
  $::world->echonl("\nRGB Colors:");
  $::world->echonl("green 000000 000000 000000 000000 000000 000000");
  $::world->echonl("blue  012345 012345 012345 012345 012345 012345");
  foreach my $r (0..5) {
    $::world->echo($r == 0 ? "red " : "    ");
    $::world->echo("$r ");
    foreach my $g (0..5) {
      foreach my $b (0..5) {
        $::world->echo(::colorize("^$r$g${b}X "));
      }
      $::world->echo(" ");
    }
    $::world->echo("\n");
  }


  $::world->echonl("\nGreyscale:");
  $::world->echo("      ");
  foreach my $g (0..23) {
    $::world->echo(::colorize("^${g}G "));
  }
  $::world->echo("\n");
  $::world->echonl("      000000000011111111112222");
  $::world->echonl("      012345678901234567890123");
}


sub rainbowtext {
  my ($text) = shift;

  my ($r, $g, $b) = (0, 0, 0);
  my $length = length($text);

  for (my $i = 0; $i < $length; ++$i) {
    my $char = substr($text, $i, 1);

    $::world->echo(::colorize("&$r$g${b}X$char"));

    next if ($char =~ /[[:space:]]/);
    ++$b;
    if ($b == 6) {
      ++$g;
      $b = 0;
    }
    if ($g == 6) {
      ++$r;
      $g = 0;
    }
    if ($r == 6) {
      $r = 0;
    }
  }
}
