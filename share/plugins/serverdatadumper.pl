package serverdatadumper;
#: Version: 1.0.0
#: Description: Dumps server data
#: Author: Eduardo M Kalinowski

use Data::Dumper;

$::world->hook('OnServerData', '/serverdatadumper::dump()', { name => 'serverdatadump:dump' });

sub help {
  $::world->echonl("Dumps server data received via out-of-band protocols such as GMCP and MSDP.");
  $::world->echonl("Just load the plugin and whenever data is received, it will be");
  $::world->echonl("displayed (using Data::Dumper).");
}

sub dump {
  $::world->echonl("Got server data:");
  $::world->echo(Data::Dumper->Dump([\%::server_data], ['%::server_data']));
}
