package easypath;
#: Version: 1.0.1
#: Description: Adds a shortcut to paths: #<path>
#: Author: Eduardo M Kalinowski

$::world->alias('^#(.*)$', '/\$world->path("$1")', { name => 'easypath:path' });

sub help {
  $::world->echonl("Makes #<path> expand to the a path, as the $world->path()",
                   "function would do.");
}
